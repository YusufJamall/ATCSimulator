﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummyVisual
{
    public enum AircraftStatus
    {
        Taxing,
        Pushback,
        ReadyAircraft,
        Intersection
    }
    public enum AircraftCategory
    {
        Jet,
        Propeller,
        Helicopter
    }

    public enum UTurnInfo
    {
        Left,
        Right,
        Turn180
    }
    public enum FlightInfo
    {
        Departure,
        Arrival
    }
    public enum GroundSpeed
    {
        Slow,
        Normal,
        Expedite
    }
    public enum FlightRuleInfo //*penambahan
    {
        IFR,
        VFR,
        Local
    }
    public class AircraftData
    {
        public string callsign { get; set; }
        public string model { get; set; }
        public string livery { get; set; }
        public double fuel { get; set; }
        public FlightInfo flight { get; set; }
        public string position { get; set; }
        public FlightRouteModel flightRoutes { get; set; }
    }
    public class FlightRouteModel
    {
        public FlightRuleInfo flightRules { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
        public List<string> routes { get; set; }
    }
    public enum WeatherInfo
    {
        Clear,
        LightRain,
        ThunderStorm,
        Cloudy,
    }
    public class WeatherModel
    {
        public WeatherInfo weather { get; set; }
        public float windDirection { get; set; }
        public float temperature { get; set; }
        public float visibility { get; set; }
        public TimeData time { get; set; }
    }
    public class TimeData
    {
        public int hour { get; set; }
        public int minute { get; set; }
    }

    public class ExerciseData //*Perubahan
    {
        public string scenery_name { get; set; }
        public WeatherModel weather { get; set; }
        public Dictionary<string, bool> runway_lights { get; set; }
        public Dictionary<string, bool> approach_lights { get; set; }
        public bool taxi { get; set; }
        public bool papi { get; set; }
    }

    public class SimulationData
    {
        public ExerciseData exercise { get; set; }
        public List<AircraftData> aircrafts { get; set; }
    }

}
