﻿namespace ATCSimulator.DummyVisual
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FinishReady = new System.Windows.Forms.Button();
            this.FinishPushback = new System.Windows.Forms.Button();
            this.AircraftList = new System.Windows.Forms.ComboBox();
            this.HeadingValue = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TaxiCurrentPosition = new System.Windows.Forms.Button();
            this.TaxiRouteList = new System.Windows.Forms.ComboBox();
            this.FinishTaxi = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.HeadingAircraft = new System.Windows.Forms.Button();
            this.CrossRunway = new System.Windows.Forms.Button();
            this.UTurn = new System.Windows.Forms.Button();
            this.ReadySimulation = new System.Windows.Forms.Button();
            this.FinishTakeOff = new System.Windows.Forms.Button();
            this.CrashAircraft = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.FinishLanding = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.FinishParking = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.HeadingValue)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // FinishReady
            // 
            this.FinishReady.Location = new System.Drawing.Point(8, 6);
            this.FinishReady.Name = "FinishReady";
            this.FinishReady.Size = new System.Drawing.Size(78, 23);
            this.FinishReady.TabIndex = 0;
            this.FinishReady.Text = "Finish Ready";
            this.FinishReady.UseVisualStyleBackColor = true;
            this.FinishReady.Click += new System.EventHandler(this.FinishReady_Click);
            // 
            // FinishPushback
            // 
            this.FinishPushback.Location = new System.Drawing.Point(135, 35);
            this.FinishPushback.Name = "FinishPushback";
            this.FinishPushback.Size = new System.Drawing.Size(122, 23);
            this.FinishPushback.TabIndex = 1;
            this.FinishPushback.Text = "Finish Pushback";
            this.FinishPushback.UseVisualStyleBackColor = true;
            this.FinishPushback.Click += new System.EventHandler(this.FinishPushback_Click);
            // 
            // AircraftList
            // 
            this.AircraftList.FormattingEnabled = true;
            this.AircraftList.Location = new System.Drawing.Point(63, 12);
            this.AircraftList.Name = "AircraftList";
            this.AircraftList.Size = new System.Drawing.Size(88, 21);
            this.AircraftList.TabIndex = 2;
            this.AircraftList.SelectedIndexChanged += new System.EventHandler(this.AircraftList_SelectedIndexChanged);
            // 
            // HeadingValue
            // 
            this.HeadingValue.Location = new System.Drawing.Point(63, 41);
            this.HeadingValue.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.HeadingValue.Name = "HeadingValue";
            this.HeadingValue.Size = new System.Drawing.Size(58, 20);
            this.HeadingValue.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Heading";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "callsign";
            // 
            // TaxiCurrentPosition
            // 
            this.TaxiCurrentPosition.Location = new System.Drawing.Point(92, 6);
            this.TaxiCurrentPosition.Name = "TaxiCurrentPosition";
            this.TaxiCurrentPosition.Size = new System.Drawing.Size(125, 23);
            this.TaxiCurrentPosition.TabIndex = 6;
            this.TaxiCurrentPosition.Text = "Taxi Current Position";
            this.TaxiCurrentPosition.UseVisualStyleBackColor = true;
            this.TaxiCurrentPosition.Click += new System.EventHandler(this.TaxiCurrentPosition_Click);
            // 
            // TaxiRouteList
            // 
            this.TaxiRouteList.FormattingEnabled = true;
            this.TaxiRouteList.Location = new System.Drawing.Point(199, 42);
            this.TaxiRouteList.Name = "TaxiRouteList";
            this.TaxiRouteList.Size = new System.Drawing.Size(156, 21);
            this.TaxiRouteList.TabIndex = 7;
            // 
            // FinishTaxi
            // 
            this.FinishTaxi.Location = new System.Drawing.Point(223, 6);
            this.FinishTaxi.Name = "FinishTaxi";
            this.FinishTaxi.Size = new System.Drawing.Size(85, 23);
            this.FinishTaxi.TabIndex = 8;
            this.FinishTaxi.Text = "Finish Taxi";
            this.FinishTaxi.UseVisualStyleBackColor = true;
            this.FinishTaxi.Click += new System.EventHandler(this.FinishTaxi_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(134, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Taxi Route";
            // 
            // HeadingAircraft
            // 
            this.HeadingAircraft.Location = new System.Drawing.Point(8, 35);
            this.HeadingAircraft.Name = "HeadingAircraft";
            this.HeadingAircraft.Size = new System.Drawing.Size(121, 23);
            this.HeadingAircraft.TabIndex = 10;
            this.HeadingAircraft.Text = "Heading Aircraft";
            this.HeadingAircraft.UseVisualStyleBackColor = true;
            this.HeadingAircraft.Click += new System.EventHandler(this.HeadingAircraft_Click);
            // 
            // CrossRunway
            // 
            this.CrossRunway.Location = new System.Drawing.Point(260, 35);
            this.CrossRunway.Name = "CrossRunway";
            this.CrossRunway.Size = new System.Drawing.Size(125, 23);
            this.CrossRunway.TabIndex = 11;
            this.CrossRunway.Text = "Cross Runway";
            this.CrossRunway.UseVisualStyleBackColor = true;
            this.CrossRunway.Click += new System.EventHandler(this.CrossRunway_Click);
            // 
            // UTurn
            // 
            this.UTurn.Location = new System.Drawing.Point(389, 35);
            this.UTurn.Name = "UTurn";
            this.UTurn.Size = new System.Drawing.Size(85, 23);
            this.UTurn.TabIndex = 12;
            this.UTurn.Text = "Finish U Turn";
            this.UTurn.UseVisualStyleBackColor = true;
            this.UTurn.Click += new System.EventHandler(this.UTurn_Click);
            // 
            // ReadySimulation
            // 
            this.ReadySimulation.Location = new System.Drawing.Point(157, 10);
            this.ReadySimulation.Name = "ReadySimulation";
            this.ReadySimulation.Size = new System.Drawing.Size(114, 23);
            this.ReadySimulation.TabIndex = 13;
            this.ReadySimulation.Text = "Ready Simulation";
            this.ReadySimulation.UseVisualStyleBackColor = true;
            this.ReadySimulation.Click += new System.EventHandler(this.ReadySimulation_Click);
            // 
            // FinishTakeOff
            // 
            this.FinishTakeOff.Location = new System.Drawing.Point(314, 6);
            this.FinishTakeOff.Name = "FinishTakeOff";
            this.FinishTakeOff.Size = new System.Drawing.Size(108, 23);
            this.FinishTakeOff.TabIndex = 14;
            this.FinishTakeOff.Text = "Finish Take Off";
            this.FinishTakeOff.UseVisualStyleBackColor = true;
            this.FinishTakeOff.Click += new System.EventHandler(this.button1_Click);
            // 
            // CrashAircraft
            // 
            this.CrashAircraft.Location = new System.Drawing.Point(277, 10);
            this.CrashAircraft.Name = "CrashAircraft";
            this.CrashAircraft.Size = new System.Drawing.Size(85, 23);
            this.CrashAircraft.TabIndex = 15;
            this.CrashAircraft.Text = "Crash";
            this.CrashAircraft.UseVisualStyleBackColor = true;
            this.CrashAircraft.Click += new System.EventHandler(this.CrashAircraft_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 69);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(489, 162);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.FinishParking);
            this.tabPage1.Controls.Add(this.FinishTaxi);
            this.tabPage1.Controls.Add(this.FinishReady);
            this.tabPage1.Controls.Add(this.FinishTakeOff);
            this.tabPage1.Controls.Add(this.FinishPushback);
            this.tabPage1.Controls.Add(this.TaxiCurrentPosition);
            this.tabPage1.Controls.Add(this.UTurn);
            this.tabPage1.Controls.Add(this.HeadingAircraft);
            this.tabPage1.Controls.Add(this.CrossRunway);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(481, 136);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ground";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.FinishLanding);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(481, 136);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Flight";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // FinishLanding
            // 
            this.FinishLanding.Location = new System.Drawing.Point(6, 6);
            this.FinishLanding.Name = "FinishLanding";
            this.FinishLanding.Size = new System.Drawing.Size(99, 23);
            this.FinishLanding.TabIndex = 1;
            this.FinishLanding.Text = "Finish Landing";
            this.FinishLanding.UseVisualStyleBackColor = true;
            this.FinishLanding.Click += new System.EventHandler(this.FinishLanding_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(481, 136);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Other";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // FinishParking
            // 
            this.FinishParking.Location = new System.Drawing.Point(8, 64);
            this.FinishParking.Name = "FinishParking";
            this.FinishParking.Size = new System.Drawing.Size(108, 23);
            this.FinishParking.TabIndex = 15;
            this.FinishParking.Text = "Finish Parking";
            this.FinishParking.UseVisualStyleBackColor = true;
            this.FinishParking.Click += new System.EventHandler(this.FinishParking_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 236);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.CrashAircraft);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TaxiRouteList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ReadySimulation);
            this.Controls.Add(this.HeadingValue);
            this.Controls.Add(this.AircraftList);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.HeadingValue)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button FinishReady;
        private System.Windows.Forms.Button FinishPushback;
        private System.Windows.Forms.ComboBox AircraftList;
        private System.Windows.Forms.NumericUpDown HeadingValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button TaxiCurrentPosition;
        private System.Windows.Forms.ComboBox TaxiRouteList;
        private System.Windows.Forms.Button FinishTaxi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button HeadingAircraft;
        private System.Windows.Forms.Button CrossRunway;
        private System.Windows.Forms.Button UTurn;
        private System.Windows.Forms.Button ReadySimulation;
        private System.Windows.Forms.Button FinishTakeOff;
        private System.Windows.Forms.Button CrashAircraft;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button FinishLanding;
        private System.Windows.Forms.Button FinishParking;
    }
}

