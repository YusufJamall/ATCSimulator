﻿using SmartFoxClientAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummyVisual
{
    public class SmartFox 
    {
        private static SmartFoxClient smartFox;
        public static SmartFoxClient Connection
        {
            get { return smartFox; }
            set { smartFox = value; }
        }

        public static bool IsInitialized()
        {
            if (smartFox != null)
            {
                return true;
            }
            return false;
        }
    }
}
