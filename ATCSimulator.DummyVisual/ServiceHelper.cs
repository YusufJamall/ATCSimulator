﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummyVisual
{
    public class ServiceHelper
    {
        #region Ground
        public static string FINISH_READY_AIRCRAFT
        {
            get { return "FinishReadyAircraft"; }
        }
        public static string FINISH_PUSHBACK
        {
            get { return "FinishPushback"; }
        }
        public static string UPDATE_TAXI_POSITION
        {
            get { return "TaxiPosition"; }
        }
        public static string FINISH_TAXI
        {
            get { return "FinishTaxi"; }
        }
        public static string HOLD_TAXI
        {
            get { return "HoldTaxi"; }
        }
        public static string REQUEST_CROSS_RUNWAY
        {
            get { return "RequestCrossRunway"; }
        }
        public static string FINISH_UTURN
        {
            get { return "FinishUTurn"; }
        }
        public static string FINISH_HOLD_TAKE_OFF
        {
            get { return "FinishHoldTakeOff"; }
        }
        public static string FINISH_TAKE_OFF
        {
            get { return "FinishTakeOff"; }
        }
        public static string CRASH_AIRCRAFT
        {
            get { return "CrashAircraft"; }
        }
        public static string FINISH_LANDING
        {
            get { return "FinishLanding"; }
        }
        public static string FINISH_PARKING
        {
            get { return "FinishParking"; }
        }
        #endregion

        #region Other
        public static string READY_SIMULATION
        {
            get { return "ReadySimulation"; }
        }

        #endregion
        public static string SEND_TO_SYSTEM
        {
            get { return "SendToSystem"; }
        }
    }
}
