﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATCSimulator.DummyVisual.Services;

namespace ATCSimulator.DummyVisual
{
    public partial class MainForm : Form
    {
        static MainForm Instance;
        Network network;
        public Dictionary<string, ClientTaxiModel> aircraft_data;
        string current_aircraft;
        public MainForm()
        {
            InitializeComponent();
            Instance = this;
            network = new Network(new VisualConfig()
                {
                host = System.Configuration.ConfigurationManager.AppSettings["SfsHost"].ToString(),
                port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SfsPort"]),
                room_name = System.Configuration.ConfigurationManager.AppSettings["SfsRoom"].ToString(),
                user_name = System.Configuration.ConfigurationManager.AppSettings["SfsUser"].ToString(),
                zone_name = System.Configuration.ConfigurationManager.AppSettings["SfsZone"].ToString()
                });
            network.Connect();
            aircraft_data = new Dictionary<string, ClientTaxiModel>();
        }
        public T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static void StartSimulation(SimulationData data)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    Instance.LoadSimulation(data);
                }));
            }
            else
            {
                Instance.LoadSimulation(data);
            }
        }
        public void LoadSimulation(SimulationData data)
        {
            foreach (AircraftData a in data.aircrafts)
            {
                AircraftList.Items.Add(a.callsign);
                aircraft_data.Add(a.callsign, new ClientTaxiModel() { taxi = new List<string>() });
            }
            FinishPushback.Enabled = true;
            FinishReady.Enabled = true;
        }
        public void SetTaxiRoute(string callsign,List<string> routes)
        {
            foreach(var a in aircraft_data)
            {
                if (callsign.Equals(a.Key))
                {
                    a.Value.taxi = routes;
                }
            }
            if (current_aircraft.Equals(callsign))
                SetTaxi(routes);
        }
        private void SetTaxi(List<string> routes)
        {
            TaxiRouteList.Items.Clear();
            foreach (string s in routes)
            {
                TaxiRouteList.Items.Add(s);
            }
        }

        private void FinishReady_Click(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1)
                return;
            VisualService.FinishReadyAircraft(AircraftList.Text);
        }

        private void FinishPushback_Click(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1)
                return;
            VisualService.FinishPushback(AircraftList.Text, (float)HeadingValue.Value);
        }

        private void HeadingAircraft_Click(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1)
                return;
            VisualService.HoldTaxi(AircraftList.Text, (float)HeadingValue.Value);
        }

        private void TaxiCurrentPosition_Click(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1 || TaxiRouteList.SelectedIndex == -1)
                return;
            VisualService.UpdateTaxiPosition(AircraftList.Text, TaxiRouteList.Text, (float)HeadingValue.Value);
        }

        private void FinishTaxi_Click(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1 || TaxiRouteList.SelectedIndex == -1)
                return;
            VisualService.FinishTaxi(AircraftList.Text, TaxiRouteList.Text, (float)HeadingValue.Value,TaxiInfo.Intersection);
        }

        private void CrossRunway_Click(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1)
                return;
            VisualService.RequestCrossRunway(AircraftList.Text, (float)HeadingValue.Value);
        }

        private void UTurn_Click(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1)
                return;
            VisualService.FinishUTurn(AircraftList.Text, (float)HeadingValue.Value);
        }

        private void AircraftList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AircraftList.SelectedIndex == -1)
                return;
            current_aircraft = AircraftList.Text;
            foreach(var a in aircraft_data)
            {
                if (current_aircraft.Equals(a.Key))
                {
                    SetTaxi(a.Value.taxi);
                    break;
                }
            }
        }

        private void ReadySimulation_Click(object sender, EventArgs e)
        {
            VisualService.ReadySimulation();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VisualService.FinishTakeOff(AircraftList.Text);
        }

        private void CrashAircraft_Click(object sender, EventArgs e)
        {
            VisualService.CrashAircraft(AircraftList.Text);
        }

        private void FinishLanding_Click(object sender, EventArgs e)
        {
            VisualService.FinishLanding(AircraftList.Text,360,"16L");
        }

        private void FinishParking_Click(object sender, EventArgs e)
        {
            VisualService.FinishParking(AircraftList.Text, "A1");
        }
        
    }
    public class ClientTaxiModel
    {
        public List<string> taxi { get; set; }
    }
}
