﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ATCSimulator.DummyVisual.SimulationService;
using System.ServiceModel;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace ATCSimulator.DummyVisual
{
     [CallbackBehavior(
       ConcurrencyMode = ConcurrencyMode.Multiple,
       UseSynchronizationContext = false)]
    public class Connection : SimulationServiceCallback
    {
        MainForm main_form;
        private SynchronizationContext _uiSyncContext = null;
        private SimulationServiceClient _atcService = null;
        UserComputerInfo user_info;
        public Connection(MainForm form)
        {
            main_form = form;
            user_info = GetComputerInfo();
            OpenConnection();
            Connect();
        }
        #region Open Connection
        private bool OpenConnection()
        {
            try
            {
                if (_atcService == null || !_atcService.State.ToString().Equals("Opened"))
                {
                    _uiSyncContext = SynchronizationContext.Current;
                    _atcService = new SimulationServiceClient(new InstanceContext(this), "TcpBinding");
                    _atcService.Open();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        public StatusResponse Connect()
        {
            if (OpenConnection())
            {

            }
            StatusResponse result = new StatusResponse();
            result = _atcService.Connect(new SimulationService.UserComputerInfo()
            {
                ip_address = user_info.ip_address,
                pc_name = user_info.pc_name,
                mac_address = user_info.mac_address,
                role_pc = user_info.role_pc,
                application = "Visual"
            });
            return result;
        }
        public void FinishReadyDepartureCallback(string callsign)
        {
            if (OpenConnection())
            {

            }
            _atcService.FinishReadyDepartureCallback(callsign);
        }
        public void FinishPushbackCallback(string callsign, double heading)
        {
            if (OpenConnection())
            {

            }
            _atcService.FinishPushbackCallback(callsign,heading);
        }
        public void UpdateTaxiPositionCallback(string callsign,string position, double heading)
        {
            if (OpenConnection())
            {

            }
            _atcService.UpdateTaxiPositionCallback(callsign,position, heading);
        }

        public void FinishTaxiCallback(string callsign,string position, double heading)
        {
            if (OpenConnection())
            {

            }
            _atcService.FinishTaxiCallback(callsign,position, heading);
        }
        public void HeadingAircraftCallback(string callsign, double heading)
        {
            if (OpenConnection())
            {

            }
            _atcService.HeadingAircraftCallback(callsign, heading);
        }
        public void RequestCrossRunwayCallback(string callsign, double heading)
        {
            if (OpenConnection())
            {

            }
            _atcService.RequestCrossRunwayCallback(callsign, heading);
        }
        public void FinishUTurnCallback(string callsign, double heading)
        {
            if (OpenConnection())
            {

            }
            _atcService.FinishUTurnCallback(callsign, heading);
        }
        public void StartSimulation(ExerciseSimulationModel exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_route)
        {
            main_form.StartSimulation(exercise, aircrafts, flight_route);
        }

        #region User Info
        private UserComputerInfo GetComputerInfo()
        {
            // Get Local IP Address and PC Name
            UserComputerInfo info = new UserComputerInfo();
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            info.pc_name = host.HostName.ToString();
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    info.ip_address = ip.ToString();
                }
            }

            //Get Machine Address
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            info.mac_address = sMacAddress;
            info.role_pc = "Instructor";
            return info;
        }
        #endregion


        public void ReadyAircraftCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void ChangeRunwayCallback(string callsign, string runway, bool is_ground)
        {
            throw new NotImplementedException();
        }

        public void StartEnigneCallback(string callsign, bool engine)
        {
            throw new NotImplementedException();
        }

        public void PushbackCallback(string callsign, PushbackInfo pushback, string pushback_name)
        {
            throw new NotImplementedException();
        }

        public void ChangeFlightRoutesCallback(string callsign, FlightRouteListModel routes)
        {
            throw new NotImplementedException();
        }

        public void StartTaxiCallback(string callsign, List<string> routes)
        {
            main_form.SetTaxiRoute(callsign,routes);
        }

        public void HoldTaxiCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void ContinueTaxiCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void ChangeSpeedTaxiCallback(string callsign, GroundSpeed speed)
        {
            throw new NotImplementedException();
        }

        public void CrossRunwayCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void UturnCallback(string callsign, UturnInfo uturn)
        {
            throw new NotImplementedException();
        }

        public void RockingWingCallback(string callsign, bool value)
        {
            throw new NotImplementedException();
        }

        public void TakeOffCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void RunwayHoldCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void IntersectionSpeedChangeCallback(string callsign, GroundSpeed speed)
        {
            throw new NotImplementedException();
        }

        public void FinishReadyDeparture(string callsign)
        {
            throw new NotImplementedException();
        }

        public void FinishPushback(string callsign, double heading)
        {
            throw new NotImplementedException();
        }

        public void UpdateTaxiPosition(string callsign, string position, double heading)
        {
            throw new NotImplementedException();
        }

        public void FinishTaxi(string callsign, string position, double heading)
        {
            throw new NotImplementedException();
        }

        public void HeadingAircraft(string callsign, double heading)
        {
            throw new NotImplementedException();
        }

        public void RequestCrossRunway(string callsign, double heading)
        {
            throw new NotImplementedException();
        }

        public void FinishUTurn(string callsign, double heading)
        {
            throw new NotImplementedException();
        }


        public void IncidentBirdCallback(string runway)
        {
            throw new NotImplementedException();
        }

        public void IncidentAnimalCallback(string runway)
        {
            throw new NotImplementedException();
        }

        public void RemoveAircraftCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void LandingGearJamCallback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void EngineFailureCallback(string callsign, int engine)
        {
            throw new NotImplementedException();
        }

        public void ChangeFuelCallback(string callsign, TimeSpan fuel)
        {
            throw new NotImplementedException();
        }

        public void ChangeWeatherCallback(WeatherInfo weather)
        {
            throw new NotImplementedException();
        }

        public void ChangeWindDirectionCallback(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeWindSpeedCallback(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeTemperatureCallback(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeVisibilityCallback(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeTimeSimulationCallback(TimeSpan time)
        {
            throw new NotImplementedException();
        }


        public void CrashAircraft(string callsign)
        {
            throw new NotImplementedException();
        }
    }
}
