﻿using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummyVisual.Services
{
    public class Network
    {
        public static Network instance;
        private string host;
        private int port;
        private string zoneName;
        private string roomName;
        private string userName;
        private SmartFox sfs;

        VisualService visual;

        public Network(VisualConfig visual_config)
        {
            instance = this;
            this.host = visual_config.host;
            this.port = visual_config.port;
            this.zoneName = visual_config.zone_name;
            this.roomName = visual_config.room_name;
            this.userName = visual_config.user_name;
            sfs = new SmartFox();
            sfs.ThreadSafeMode = false;
            visual = new VisualService();
        }
        public void Connect()
        {
            sfs.Connect(host, port);
            sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
            sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
            sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoom);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);
            sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
        }
        public void Disconnect()
        {
            if (sfs.IsConnected)
            {
                sfs.Disconnect();
                sfs.RemoveAllEventListeners();
            }
        }

        public void Send(string command, ISFSObject data)
        {
            sfs.Send(new ExtensionRequest(command, data));
        }

        #region Callback
        void OnConnection(BaseEvent e)
        {
            if ((bool)e.Params["success"])
            {
                //MainForm.LogMessage("Connected to " + host + ":" + port);

                sfs.Send(new LoginRequest(userName, "", zoneName));
            }
            else
            {
               // MainForm.LogMessage("Connection error!");
            }
        }

        void OnLogin(BaseEvent e)
        {
            //MainForm.LogMessage("Login success : " + e.Params["user"]);
            sfs.Send(new JoinRoomRequest(roomName));
        }

        void OnLoginError(BaseEvent e)
        {
            //MainForm.LogMessage(e.Params["errorMessage"].ToString());
        }

        void OnJoinRoom(BaseEvent e)
        {
            //MainForm.LogMessage("JoinRoomm Success: " + e.Params["room"]);
        }

        void OnJoinRoomError(BaseEvent e)
        {
            //MainForm.LogMessage(e.Params["errorMessage"].ToString());
        }
        void OnExtensionResponse(BaseEvent e)
        {
            string cmd = (string)e.Params["cmd"];
            ISFSObject data = (SFSObject)e.Params["params"];
            //MainForm.LogMessage("Call form Visual : " + cmd + ", " + data.GetUtfString("callsign"));
            switch (cmd)
            {
                case "StartSimulation":
                    visual.StartSimulation(data);
                    break;
            }
        }
        #endregion
    }
}