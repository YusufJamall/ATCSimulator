﻿using Newtonsoft.Json;
using Sfs2X.Entities.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummyVisual.Services
{
    public class VisualService
    {

        public void StartSimulation(ISFSObject data)
        {
            string _data = data.GetUtfString("data");
            SimulationData simulation = JsonConvert.DeserializeObject<SimulationData>(_data);
            MainForm.StartSimulation(simulation);
        }

        #region Send

        #region Ground Command
        public static void FinishReadyAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_READY_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void FinishPushback(string callsign, float heading)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_PUSHBACK);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("heading", heading);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void UpdateTaxiPosition(string callsign, string position, float heading)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.UPDATE_TAXI_POSITION);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("position", position);
            data.PutFloat("heading", heading);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void FinishTaxi(string callsign, string position, float heading, TaxiInfo status)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_TAXI);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("position", position);
            data.PutFloat("heading", heading);
            data.PutInt("status", (int)status);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void HoldTaxi(string callsign, float heading)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.HOLD_TAXI);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("heading", heading);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void RequestCrossRunway(string callsign, float heading)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.REQUEST_CROSS_RUNWAY);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("heading", heading);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void FinishUTurn(string callsign, float heading)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_UTURN);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("heading", heading);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void FinishHoldTakeOff(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_HOLD_TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }

        public static void FinishTakeOff(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void CrashAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.CRASH_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void FinishParking(string callsign, string appron)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_PARKING);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("appron", appron);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }
        public static void FinishLanding(string callsign,float heading, string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.FINISH_LANDING);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("heading", heading);
            data.PutUtfString("runway",runway);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }

        #endregion

        #region Flight Command
        //public static void FinishReadyAircraft(string callsign)
        //{
        //    ISFSObject data = new SFSObject();
        //    data.PutUtfString("cmd", ServiceHelper.FINISH_READY_AIRCRAFT);
        //    data.PutUtfString("callsign", callsign);
        //    Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        //}

        #endregion

        #region Other Command

        public static void ReadySimulation()
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ServiceHelper.READY_SIMULATION);
            Network.instance.Send(ServiceHelper.SEND_TO_SYSTEM, data);
        }

        #endregion

        #endregion
    }
}
