﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartFoxClientAPI;
using SmartFoxClientAPI.Data;
using SmartFoxClientAPI.Util;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;

namespace ATCSimulator.DummyVisual
{
    public partial class VIsualForm : Form
    {
        private SmartFoxClient smartFox;
        private bool debug = true;
        private string loginErrorMessage = "";
        const string serverIP = "127.0.0.1";
        const int serverPort = 9339;
        public delegate void Koneksi(bool success, string error);
        public VIsualForm()
        {
            InitializeComponent();
        }

        private void VIsualForm_Load(object sender, EventArgs e)
        {
            if (SmartFox.IsInitialized())
            {
                smartFox = SmartFox.Connection;
            }
            else
            {
                try
                {
                    smartFox = new SmartFoxClient(debug);
                    smartFox.runInQueueMode = true;
                }
                catch (Exception ex)
                {
                    loginErrorMessage = e.ToString();
                }
            }

            // Register callback delegate
            SFSEvent.onConnectionLost += OnConnectionLost;
            SFSEvent.onLogin += OnLogin;
            SFSEvent.onRoomListUpdate += OnRoomList;
            SFSEvent.onDebugMessage += OnDebugMessage;
            smartFox.Connect(serverIP, serverPort);

        }
        private void UnregisterSFSSceneCallbacks()
        {
            // This should be called when switching scenes, so callbacks from the backend do not trigger code in this scene
            SFSEvent.onConnection -= OnConnection;
            SFSEvent.onConnectionLost -= OnConnectionLost;
            SFSEvent.onLogin -= OnLogin;
            SFSEvent.onRoomListUpdate -= OnRoomList;
            SFSEvent.onDebugMessage -= OnDebugMessage;
            SFSEvent.onPublicMessage -= OnPublicMessage;
        }
        public void OnConnection(bool success, string error)
        {
            MessageBox.Show(success.ToString());
            if (success)
            {
                SmartFox.Connection = smartFox;
                
            }
            else
            {
                loginErrorMessage = error;
            }
        }

        void OnConnectionLost()
        {
            loginErrorMessage = "Connection lost / no connection to server";
        }

        public void OnDebugMessage(string message)
        {
            //Debug.Log("[SFS DEBUG] " + message);
        }

        public void OnLogin(bool success, string name, string error)
        {
            if (success)
            {
                // Lets wait for the room list

            }
            else
            {
                // Login failed - lets display the error message sent to us
                loginErrorMessage = error;
            }
        }

        void OnRoomList(Hashtable roomList)
        {
            // When room list is updated we are ready to move on to the lobby
            //UnregisterSFSSceneCallbacks();
            //Application.LoadLevel("lobby");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            smartFox.Login("sftris", textBox1.Text, "");
            button1.Enabled = false;
        }
        public void OnPublicMessage(string message, User sender, int roomId)
        {
            textBox2.Text = sender.GetName() + " said " + message;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SFSEvent.onPublicMessage += OnPublicMessage;
            if (smartFox.GetActiveRoom() == null)
            {
                smartFox.AutoJoin();
            }
            Room currentActiveRoom = smartFox.GetActiveRoom();

            if (currentActiveRoom == null)
            {
                // Wait until active room has been set up in the API before drawing anything
                return;
            }
            foreach (User user in currentActiveRoom.GetUserList().Values)
            {
                string s = user.GetName();
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            smartFox.SendPublicMessage(textBox2.Text, smartFox.GetActiveRoom().GetId());
        }

    }
}
