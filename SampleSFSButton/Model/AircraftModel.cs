﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleSFSButton.Model
{
    public enum PushbackInfo
    {
        None,
        Left,
        Right
    }
    public enum GroundStatusInfo
    {
        Departure,
        Parking
    }
    public enum PilotCommandMenu
    {
        DepartureReady,
        Ground,
        Flight
    }
    public enum UturnInfo
    {
        UTurnLeft,
        UturnRight,
        Turn180,
        None
    }
    public enum AircraftInfoStatus
    {
        Disable,
        Enable,
        Blink
    }
    public enum AircraftStatus
    {
        NONE,
        ACC,
        APP,
        Form,
        Holding,
        Landing,
        Pushback,
        TakeOff,
        Taxing,
        Parking,
        Circuit
    }
    public enum CommandStatus
    {
        None,
        Process,
        Finish
    }
    public enum TaxiStatus
    {
        None,
        Process,
        Finish,
        Holding,
        CrossRunway
    }
    public enum GroundSpeed
    {
        Slow,
        Normal,
        Expedite
    }
    public enum FlightRuleInfo
    {
        IFR,
        VFR,
        Local
    }
    public class AircraftData
    {
        public string callsign { get; set; }
        public bool is_departure { get; set; }
        public bool is_enabled { get; set; }
        public double heading { get; set; }
        public AircraftStatus status { get; set; }
        public int engine_failure { get; set; }
        public string from_position { get; set; }
        public string current_position { get; set; }
        public TimeSpan fuel { get; set; }
        public TimeSpan departure_time { get; set; }
        public TimeSpan start_time { get; set; }
        public TimeSpan estimate_time { get; set; }
        public PilotCommandProperties pilot_properties { get; set; }
        public AircraftProperties properties { get; set; }
        public UserPilotModel user { get; set; }

        public bool pkppk { get; set; }
        public bool incident_bird { get; set; }
        public bool crashed { get; set; }
        
    }
    public class UserPilotModel
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string home_base { get; set; }
        public string pc_role { get; set; }
        public string pc_name { get; set; }
        public string mac_address { get; set; }
        public string ip_address { get; set; }
    }
    public class PilotCommandProperties
    {
        public PilotCommandMenu status { get; set; }
        public PilotDepartureReadyCommand ready_departure { get; set; }
        public PilotGroundCommand ground { get; set; }
        public PilotFlightCommand flight { get; set; }
    }
    
    public class PilotDepartureReadyCommand
    {
        public bool is_enabled { get; set; }
    }
    public class PilotGroundCommand
    {
        public GroundStatusInfo status { get; set; }
        public PilotRunwayCommand runway { get; set; }
        public bool engine { get; set; }
        public bool engine_enabled { get; set; }
        public PilotParkingInfo parking { get; set; }
        public PilotPushbackCommand pushback { get; set; }
        public PilotTaxiCommand taxi { get; set; }
        public PilotOtherCommand other { get; set; }
        public PilotIntersectionCommand intersection { get; set; }
    }

    public class PilotParkingInfo
    {
        public int parking_id { get; set; }
        public string terminal_name { get; set; }
        public string appron_name { get; set; }

    }
    public class PilotIntersectionCommand
    {
        public bool is_show { get; set; }
        public bool take_off { get; set; }
        public bool runway_hold { get; set; }
        public CommandStatus status { get; set; }
        public GroundSpeed speed { get; set; }
    }
    public class PilotRunwayCommand
    {
        public string runway { get; set; }
        public bool is_show { get; set; }
    }
    public class PilotOtherCommand
    {
        public bool u_turn { get; set; }
        public UturnInfo u_turn_status { get; set; }
        public bool rocking_wing { get; set; }
    }
    public class PilotPushbackCommand
    {
        public CommandStatus status { get; set; }
        public bool is_show { get; set; }
        public bool pushback_left { get; set; }
        public bool pushback_right { get; set; }
        public bool pushback_left_enabled { get; set; }
        public bool pushback_right_enabled { get; set; }
        public string pushback_left_alias { get; set; }
        public string pushback_right_alias { get; set; }
    }
    public class PilotTaxiCommand
    {
        public bool is_show { get; set; }
        public bool taxi_route_enabled { get; set; }
        public bool taxi_route { get; set; }
        public bool cross_runway { get; set; }
        public TaxiStatus status { get; set; }
        public GroundSpeed speed { get; set; }
        public List<string> routes { get; set; }

    }
    public class AircraftProperties
    {
        public string category { get; set; }
        public string livery { get; set; }
        public string model { get; set; }
        public int engine { get; set; }
        public string type { get; set; }
        public int person { get; set; }
        public string flight_level_type { get; set; }
        public int flight_level { get; set; }
        public string flight_type { get; set; }
        public double true_speed { get; set; }
        public string livery_strips { get; set; }
    }
    public class PilotFlightCommand
    {
        public FlightRouteListModel flight_route { get; set; }
    }

}
