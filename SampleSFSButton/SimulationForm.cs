﻿using SampleSFSButton.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SampleSFSButton.Model;

namespace SampleSFSButton
{
    public partial class SimulationForm : Form
    {
        VisualNetwork network;
        public SimulationForm(VisualNetwork network)
        {
            InitializeComponent();
            this.network = network;

        }

        private void PlayBtn_Click(object sender, EventArgs e)
        {
            string file_path = System.IO.Directory.GetCurrentDirectory() + "\\Asset\\sample_data.json";
            string data = string.Empty;

            using (StreamReader reader = new StreamReader(file_path))
            {
                data = reader.ReadToEnd();
            }
            PauseBtn.Enabled = true;
            StopBtn.Enabled = true;
            //PlayBtn.Enabled = false;
            MainPanel.Enabled = true;
            OtherService.CreateSimulation(data);
        }

        private void TaxiLightBtn_Click(object sender, EventArgs e)
        {
            OtherService.TaxiLight(TaxiLightChk.Checked);
        }

        private void PapiLightBtn_Click(object sender, EventArgs e)
        {
            OtherService.PapiLight(PapiLightChk.Checked);
        }

        private void PapiBtn_Click(object sender, EventArgs e)
        {
            OtherService.ApproachLight(RunwayLight.Text,RunwayLightChk.Checked);
        }

        private void RunwayLightBtn_Click(object sender, EventArgs e)
        {
            OtherService.RunwayLight(RunwayLight.Text, RunwayLightChk.Checked);
        }

        private void WeatherBtn_Click(object sender, EventArgs e)
        {
            OtherService.Weather(WeatherList.SelectedIndex);
        }

        private void TimeBtn_Click(object sender, EventArgs e)
        {
            DateTime date = dateTimePicker1.Value;
            OtherService.TimeSimulation(date.Hour, date.Minute);
        }

        private void EngineBtn_Click(object sender, EventArgs e)
        {
            GroundService.EngineStart(Callsign.Text,EngineChk.Checked);
        }

        private void PBLeftBtn_Click(object sender, EventArgs e)
        {
            GroundService.Pushback(Callsign.Text, PushbackInfo.Left);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GroundService.Pushback(Callsign.Text, PushbackInfo.Right);
        }

        private void StartTaxiBtn_Click(object sender, EventArgs e)
        {
            //GroundService.StartTaxi
        }

        private void HoldTaxiBtn_Click(object sender, EventArgs e)
        {
            GroundService.HoldTaxi(Callsign.Text);
        }

        private void CrossRunwayBtn_Click(object sender, EventArgs e)
        {
            GroundService.CrossRunway(Callsign.Text);
        }

        private void AnimalCross_Click(object sender, EventArgs e)
        {
            OtherService.AnimalCrossing(RunwayTxt.Text);
        }

        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            OtherService.RemoveAircraft(Callsign.Text);
        }

        private void TakeOffBtn_Click(object sender, EventArgs e)
        {
            //GroundService.TakeOff()
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OtherService.BirdAttack(Callsign.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GroundService.PKPPK(Callsign.Text);
        }

        private void SelectBtn_Click(object sender, EventArgs e)
        {
            OtherService.SelectAircraft(Callsign.Text, "Instructor");
        }

        private void ReadyAircraft_Click(object sender, EventArgs e)
        {
            GroundService.ReadyAircraft(Callsign.Text);
        }

    }
}
