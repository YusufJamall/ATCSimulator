﻿namespace SampleSFSButton
{
    partial class SimulationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlayBtn = new System.Windows.Forms.Button();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.SelectBtn = new System.Windows.Forms.Button();
            this.RemoveBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.AnimalCross = new System.Windows.Forms.Button();
            this.RWHold = new System.Windows.Forms.Button();
            this.RunwayTxt = new System.Windows.Forms.TextBox();
            this.TakeOffBtn = new System.Windows.Forms.Button();
            this.CrossRunwayBtn = new System.Windows.Forms.Button();
            this.HoldTaxiBtn = new System.Windows.Forms.Button();
            this.StartTaxiBtn = new System.Windows.Forms.Button();
            this.TaxiText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.PBLeftBtn = new System.Windows.Forms.Button();
            this.EngineChk = new System.Windows.Forms.CheckBox();
            this.EngineBtn = new System.Windows.Forms.Button();
            this.TimeBtn = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.WeatherList = new System.Windows.Forms.ComboBox();
            this.WeatherBtn = new System.Windows.Forms.Button();
            this.PapiBtn = new System.Windows.Forms.Button();
            this.RunwayLightChk = new System.Windows.Forms.CheckBox();
            this.RunwayLight = new System.Windows.Forms.TextBox();
            this.RunwayLightBtn = new System.Windows.Forms.Button();
            this.PapiLightChk = new System.Windows.Forms.CheckBox();
            this.PapiLightBtn = new System.Windows.Forms.Button();
            this.TaxiLightChk = new System.Windows.Forms.CheckBox();
            this.TaxiLightBtn = new System.Windows.Forms.Button();
            this.PauseBtn = new System.Windows.Forms.Button();
            this.StopBtn = new System.Windows.Forms.Button();
            this.Callsign = new System.Windows.Forms.TextBox();
            this.ReadyAircraft = new System.Windows.Forms.Button();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PlayBtn
            // 
            this.PlayBtn.Location = new System.Drawing.Point(2, 3);
            this.PlayBtn.Name = "PlayBtn";
            this.PlayBtn.Size = new System.Drawing.Size(52, 23);
            this.PlayBtn.TabIndex = 0;
            this.PlayBtn.Text = "Play";
            this.PlayBtn.UseVisualStyleBackColor = true;
            this.PlayBtn.Click += new System.EventHandler(this.PlayBtn_Click);
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.ReadyAircraft);
            this.MainPanel.Controls.Add(this.SelectBtn);
            this.MainPanel.Controls.Add(this.RemoveBtn);
            this.MainPanel.Controls.Add(this.button3);
            this.MainPanel.Controls.Add(this.button2);
            this.MainPanel.Controls.Add(this.AnimalCross);
            this.MainPanel.Controls.Add(this.RWHold);
            this.MainPanel.Controls.Add(this.RunwayTxt);
            this.MainPanel.Controls.Add(this.TakeOffBtn);
            this.MainPanel.Controls.Add(this.CrossRunwayBtn);
            this.MainPanel.Controls.Add(this.HoldTaxiBtn);
            this.MainPanel.Controls.Add(this.StartTaxiBtn);
            this.MainPanel.Controls.Add(this.TaxiText);
            this.MainPanel.Controls.Add(this.button1);
            this.MainPanel.Controls.Add(this.PBLeftBtn);
            this.MainPanel.Controls.Add(this.EngineChk);
            this.MainPanel.Controls.Add(this.EngineBtn);
            this.MainPanel.Controls.Add(this.TimeBtn);
            this.MainPanel.Controls.Add(this.dateTimePicker1);
            this.MainPanel.Controls.Add(this.WeatherList);
            this.MainPanel.Controls.Add(this.WeatherBtn);
            this.MainPanel.Controls.Add(this.PapiBtn);
            this.MainPanel.Controls.Add(this.RunwayLightChk);
            this.MainPanel.Controls.Add(this.RunwayLight);
            this.MainPanel.Controls.Add(this.RunwayLightBtn);
            this.MainPanel.Controls.Add(this.PapiLightChk);
            this.MainPanel.Controls.Add(this.PapiLightBtn);
            this.MainPanel.Controls.Add(this.TaxiLightChk);
            this.MainPanel.Controls.Add(this.TaxiLightBtn);
            this.MainPanel.Enabled = false;
            this.MainPanel.Location = new System.Drawing.Point(2, 32);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(278, 253);
            this.MainPanel.TabIndex = 1;
            // 
            // SelectBtn
            // 
            this.SelectBtn.Location = new System.Drawing.Point(112, 226);
            this.SelectBtn.Name = "SelectBtn";
            this.SelectBtn.Size = new System.Drawing.Size(75, 23);
            this.SelectBtn.TabIndex = 27;
            this.SelectBtn.Text = "SelectAircraft";
            this.SelectBtn.UseVisualStyleBackColor = true;
            this.SelectBtn.Click += new System.EventHandler(this.SelectBtn_Click);
            // 
            // RemoveBtn
            // 
            this.RemoveBtn.Location = new System.Drawing.Point(214, 3);
            this.RemoveBtn.Name = "RemoveBtn";
            this.RemoveBtn.Size = new System.Drawing.Size(54, 23);
            this.RemoveBtn.TabIndex = 26;
            this.RemoveBtn.Text = "Delete";
            this.RemoveBtn.UseVisualStyleBackColor = true;
            this.RemoveBtn.Click += new System.EventHandler(this.RemoveBtn_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(174, 197);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(56, 23);
            this.button3.TabIndex = 25;
            this.button3.Text = "PKPK";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(112, 197);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "Bird";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AnimalCross
            // 
            this.AnimalCross.Location = new System.Drawing.Point(147, 142);
            this.AnimalCross.Name = "AnimalCross";
            this.AnimalCross.Size = new System.Drawing.Size(121, 23);
            this.AnimalCross.TabIndex = 23;
            this.AnimalCross.Text = "Animal Cross";
            this.AnimalCross.UseVisualStyleBackColor = true;
            this.AnimalCross.Click += new System.EventHandler(this.AnimalCross_Click);
            // 
            // RWHold
            // 
            this.RWHold.Location = new System.Drawing.Point(193, 168);
            this.RWHold.Name = "RWHold";
            this.RWHold.Size = new System.Drawing.Size(75, 23);
            this.RWHold.TabIndex = 22;
            this.RWHold.Text = "RW Hold";
            this.RWHold.UseVisualStyleBackColor = true;
            // 
            // RunwayTxt
            // 
            this.RunwayTxt.Location = new System.Drawing.Point(112, 142);
            this.RunwayTxt.Name = "RunwayTxt";
            this.RunwayTxt.Size = new System.Drawing.Size(29, 20);
            this.RunwayTxt.TabIndex = 21;
            this.RunwayTxt.Text = "34L";
            // 
            // TakeOffBtn
            // 
            this.TakeOffBtn.Location = new System.Drawing.Point(112, 168);
            this.TakeOffBtn.Name = "TakeOffBtn";
            this.TakeOffBtn.Size = new System.Drawing.Size(75, 23);
            this.TakeOffBtn.TabIndex = 20;
            this.TakeOffBtn.Text = "Take Off";
            this.TakeOffBtn.UseVisualStyleBackColor = true;
            this.TakeOffBtn.Click += new System.EventHandler(this.TakeOffBtn_Click);
            // 
            // CrossRunwayBtn
            // 
            this.CrossRunwayBtn.Location = new System.Drawing.Point(112, 113);
            this.CrossRunwayBtn.Name = "CrossRunwayBtn";
            this.CrossRunwayBtn.Size = new System.Drawing.Size(156, 23);
            this.CrossRunwayBtn.TabIndex = 19;
            this.CrossRunwayBtn.Text = "Cross Runway";
            this.CrossRunwayBtn.UseVisualStyleBackColor = true;
            this.CrossRunwayBtn.Click += new System.EventHandler(this.CrossRunwayBtn_Click);
            // 
            // HoldTaxiBtn
            // 
            this.HoldTaxiBtn.Location = new System.Drawing.Point(193, 84);
            this.HoldTaxiBtn.Name = "HoldTaxiBtn";
            this.HoldTaxiBtn.Size = new System.Drawing.Size(75, 23);
            this.HoldTaxiBtn.TabIndex = 18;
            this.HoldTaxiBtn.Text = "Hold";
            this.HoldTaxiBtn.UseVisualStyleBackColor = true;
            this.HoldTaxiBtn.Click += new System.EventHandler(this.HoldTaxiBtn_Click);
            // 
            // StartTaxiBtn
            // 
            this.StartTaxiBtn.Location = new System.Drawing.Point(112, 84);
            this.StartTaxiBtn.Name = "StartTaxiBtn";
            this.StartTaxiBtn.Size = new System.Drawing.Size(75, 23);
            this.StartTaxiBtn.TabIndex = 17;
            this.StartTaxiBtn.Text = "Taxi";
            this.StartTaxiBtn.UseVisualStyleBackColor = true;
            this.StartTaxiBtn.Click += new System.EventHandler(this.StartTaxiBtn_Click);
            // 
            // TaxiText
            // 
            this.TaxiText.Location = new System.Drawing.Point(112, 58);
            this.TaxiText.Name = "TaxiText";
            this.TaxiText.Size = new System.Drawing.Size(156, 20);
            this.TaxiText.TabIndex = 16;
            this.TaxiText.Text = "WP231;WP231";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(193, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "PBRight";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PBLeftBtn
            // 
            this.PBLeftBtn.Location = new System.Drawing.Point(112, 32);
            this.PBLeftBtn.Name = "PBLeftBtn";
            this.PBLeftBtn.Size = new System.Drawing.Size(75, 23);
            this.PBLeftBtn.TabIndex = 14;
            this.PBLeftBtn.Text = "PBLeft";
            this.PBLeftBtn.UseVisualStyleBackColor = true;
            this.PBLeftBtn.Click += new System.EventHandler(this.PBLeftBtn_Click);
            // 
            // EngineChk
            // 
            this.EngineChk.AutoSize = true;
            this.EngineChk.Location = new System.Drawing.Point(112, 8);
            this.EngineChk.Name = "EngineChk";
            this.EngineChk.Size = new System.Drawing.Size(15, 14);
            this.EngineChk.TabIndex = 13;
            this.EngineChk.UseVisualStyleBackColor = true;
            // 
            // EngineBtn
            // 
            this.EngineBtn.Location = new System.Drawing.Point(133, 3);
            this.EngineBtn.Name = "EngineBtn";
            this.EngineBtn.Size = new System.Drawing.Size(75, 23);
            this.EngineBtn.TabIndex = 12;
            this.EngineBtn.Text = "Engine";
            this.EngineBtn.UseVisualStyleBackColor = true;
            this.EngineBtn.Click += new System.EventHandler(this.EngineBtn_Click);
            // 
            // TimeBtn
            // 
            this.TimeBtn.Location = new System.Drawing.Point(10, 227);
            this.TimeBtn.Name = "TimeBtn";
            this.TimeBtn.Size = new System.Drawing.Size(89, 23);
            this.TimeBtn.TabIndex = 11;
            this.TimeBtn.Text = "Time";
            this.TimeBtn.UseVisualStyleBackColor = true;
            this.TimeBtn.Click += new System.EventHandler(this.TimeBtn_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker1.Location = new System.Drawing.Point(10, 201);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(89, 20);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // WeatherList
            // 
            this.WeatherList.FormattingEnabled = true;
            this.WeatherList.Items.AddRange(new object[] {
            "Clear",
            "LightRain",
            "ThunderStorm",
            "Cloud"});
            this.WeatherList.Location = new System.Drawing.Point(10, 145);
            this.WeatherList.Name = "WeatherList";
            this.WeatherList.Size = new System.Drawing.Size(89, 21);
            this.WeatherList.TabIndex = 9;
            // 
            // WeatherBtn
            // 
            this.WeatherBtn.Location = new System.Drawing.Point(10, 172);
            this.WeatherBtn.Name = "WeatherBtn";
            this.WeatherBtn.Size = new System.Drawing.Size(89, 23);
            this.WeatherBtn.TabIndex = 8;
            this.WeatherBtn.Text = "Weather";
            this.WeatherBtn.UseVisualStyleBackColor = true;
            this.WeatherBtn.Click += new System.EventHandler(this.WeatherBtn_Click);
            // 
            // PapiBtn
            // 
            this.PapiBtn.Location = new System.Drawing.Point(10, 87);
            this.PapiBtn.Name = "PapiBtn";
            this.PapiBtn.Size = new System.Drawing.Size(89, 23);
            this.PapiBtn.TabIndex = 7;
            this.PapiBtn.Text = "Papi";
            this.PapiBtn.UseVisualStyleBackColor = true;
            this.PapiBtn.Click += new System.EventHandler(this.PapiBtn_Click);
            // 
            // RunwayLightChk
            // 
            this.RunwayLightChk.AutoSize = true;
            this.RunwayLightChk.Location = new System.Drawing.Point(84, 64);
            this.RunwayLightChk.Name = "RunwayLightChk";
            this.RunwayLightChk.Size = new System.Drawing.Size(15, 14);
            this.RunwayLightChk.TabIndex = 6;
            this.RunwayLightChk.UseVisualStyleBackColor = true;
            // 
            // RunwayLight
            // 
            this.RunwayLight.Location = new System.Drawing.Point(10, 61);
            this.RunwayLight.Name = "RunwayLight";
            this.RunwayLight.Size = new System.Drawing.Size(68, 20);
            this.RunwayLight.TabIndex = 5;
            this.RunwayLight.Text = "[\"34L\"]";
            // 
            // RunwayLightBtn
            // 
            this.RunwayLightBtn.Location = new System.Drawing.Point(10, 116);
            this.RunwayLightBtn.Name = "RunwayLightBtn";
            this.RunwayLightBtn.Size = new System.Drawing.Size(89, 23);
            this.RunwayLightBtn.TabIndex = 4;
            this.RunwayLightBtn.Text = "Runway";
            this.RunwayLightBtn.UseVisualStyleBackColor = true;
            this.RunwayLightBtn.Click += new System.EventHandler(this.RunwayLightBtn_Click);
            // 
            // PapiLightChk
            // 
            this.PapiLightChk.AutoSize = true;
            this.PapiLightChk.Location = new System.Drawing.Point(10, 37);
            this.PapiLightChk.Name = "PapiLightChk";
            this.PapiLightChk.Size = new System.Drawing.Size(15, 14);
            this.PapiLightChk.TabIndex = 3;
            this.PapiLightChk.UseVisualStyleBackColor = true;
            // 
            // PapiLightBtn
            // 
            this.PapiLightBtn.Location = new System.Drawing.Point(31, 32);
            this.PapiLightBtn.Name = "PapiLightBtn";
            this.PapiLightBtn.Size = new System.Drawing.Size(75, 23);
            this.PapiLightBtn.TabIndex = 2;
            this.PapiLightBtn.Text = "Papi Light";
            this.PapiLightBtn.UseVisualStyleBackColor = true;
            this.PapiLightBtn.Click += new System.EventHandler(this.PapiLightBtn_Click);
            // 
            // TaxiLightChk
            // 
            this.TaxiLightChk.AutoSize = true;
            this.TaxiLightChk.Location = new System.Drawing.Point(10, 8);
            this.TaxiLightChk.Name = "TaxiLightChk";
            this.TaxiLightChk.Size = new System.Drawing.Size(15, 14);
            this.TaxiLightChk.TabIndex = 1;
            this.TaxiLightChk.UseVisualStyleBackColor = true;
            // 
            // TaxiLightBtn
            // 
            this.TaxiLightBtn.Location = new System.Drawing.Point(31, 3);
            this.TaxiLightBtn.Name = "TaxiLightBtn";
            this.TaxiLightBtn.Size = new System.Drawing.Size(75, 23);
            this.TaxiLightBtn.TabIndex = 0;
            this.TaxiLightBtn.Text = "Taxi Light";
            this.TaxiLightBtn.UseVisualStyleBackColor = true;
            this.TaxiLightBtn.Click += new System.EventHandler(this.TaxiLightBtn_Click);
            // 
            // PauseBtn
            // 
            this.PauseBtn.Enabled = false;
            this.PauseBtn.Location = new System.Drawing.Point(60, 3);
            this.PauseBtn.Name = "PauseBtn";
            this.PauseBtn.Size = new System.Drawing.Size(48, 23);
            this.PauseBtn.TabIndex = 2;
            this.PauseBtn.Text = "Pause";
            this.PauseBtn.UseVisualStyleBackColor = true;
            // 
            // StopBtn
            // 
            this.StopBtn.Enabled = false;
            this.StopBtn.Location = new System.Drawing.Point(114, 3);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(48, 23);
            this.StopBtn.TabIndex = 3;
            this.StopBtn.Text = "Stop";
            this.StopBtn.UseVisualStyleBackColor = true;
            // 
            // Callsign
            // 
            this.Callsign.Location = new System.Drawing.Point(170, 5);
            this.Callsign.Name = "Callsign";
            this.Callsign.Size = new System.Drawing.Size(100, 20);
            this.Callsign.TabIndex = 4;
            this.Callsign.Text = "GIA 0001";
            // 
            // ReadyAircraft
            // 
            this.ReadyAircraft.Location = new System.Drawing.Point(193, 226);
            this.ReadyAircraft.Name = "ReadyAircraft";
            this.ReadyAircraft.Size = new System.Drawing.Size(75, 23);
            this.ReadyAircraft.TabIndex = 28;
            this.ReadyAircraft.Text = "ReadyAircraft";
            this.ReadyAircraft.UseVisualStyleBackColor = true;
            this.ReadyAircraft.Click += new System.EventHandler(this.ReadyAircraft_Click);
            // 
            // SimulationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 297);
            this.Controls.Add(this.Callsign);
            this.Controls.Add(this.StopBtn);
            this.Controls.Add(this.PauseBtn);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.PlayBtn);
            this.Name = "SimulationForm";
            this.Text = "SimualtionForm";
            this.TopMost = true;
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PlayBtn;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Button PauseBtn;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.CheckBox PapiLightChk;
        private System.Windows.Forms.Button PapiLightBtn;
        private System.Windows.Forms.CheckBox TaxiLightChk;
        private System.Windows.Forms.Button TaxiLightBtn;
        private System.Windows.Forms.CheckBox RunwayLightChk;
        private System.Windows.Forms.TextBox RunwayLight;
        private System.Windows.Forms.Button RunwayLightBtn;
        private System.Windows.Forms.Button PapiBtn;
        private System.Windows.Forms.Button WeatherBtn;
        private System.Windows.Forms.ComboBox WeatherList;
        private System.Windows.Forms.Button TimeBtn;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button EngineBtn;
        private System.Windows.Forms.CheckBox EngineChk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button PBLeftBtn;
        private System.Windows.Forms.TextBox TaxiText;
        private System.Windows.Forms.Button StartTaxiBtn;
        private System.Windows.Forms.Button HoldTaxiBtn;
        private System.Windows.Forms.Button CrossRunwayBtn;
        private System.Windows.Forms.TextBox RunwayTxt;
        private System.Windows.Forms.Button TakeOffBtn;
        private System.Windows.Forms.Button RWHold;
        private System.Windows.Forms.Button AnimalCross;
        private System.Windows.Forms.TextBox Callsign;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button RemoveBtn;
        private System.Windows.Forms.Button SelectBtn;
        private System.Windows.Forms.Button ReadyAircraft;
    }
}