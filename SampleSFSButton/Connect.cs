﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SampleSFSButton.Services;
using SampleSFSButton.Model;
namespace SampleSFSButton
{
    public partial class Connect : Form
    {
        public Connect()
        {
            InitializeComponent();
        }

        private void ConnectBtn_Click(object sender, EventArgs e)
        {
            VisualConfig config = new VisualConfig()
            {
                host = IpAddress.Text,
                port = 9933,
                room_name = "simulation",
                user_name = User.Text,
                zone_name = "ATCSimulator"
            };
            VisualNetwork visual = new VisualNetwork(config);
            visual.Connect();
            ConnectBtn.Enabled = false;
            SimulationForm simulation = new SimulationForm(visual);
            simulation.Show();
        }
    }
}
