﻿using SampleSFSButton.Model;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSFSButton.Services
{
    public class VisualNetwork
    {
        public static VisualNetwork instance;
        private string host;
        private int port;
        private string zoneName;
        private string roomName;
        private string userName;
        private SmartFox sfs;

        GroundService ground;
        FlightService flight;
        OtherService other;

        public VisualNetwork(VisualConfig visual_config)
        {
            instance = this;
            this.host = visual_config.host;
            this.port = visual_config.port;
            this.zoneName = visual_config.zone_name;
            this.roomName = visual_config.room_name;
            this.userName = visual_config.user_name;
            sfs = new SmartFox();
            sfs.ThreadSafeMode = false;
            ground = new GroundService();
            flight = new FlightService();
            other = new OtherService();
        }
        public void Connect()
        {
            sfs.Connect(host, port);
            sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
            sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
            sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoom);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);
            sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
            sfs.AddEventListener(SFSEvent.CONNECTION_LOST, Reconnect);
        }

        public void Reconnect(BaseEvent e)
        {
            sfs.Connect();
        }
        public void Disconnect()
        {
            if (sfs.IsConnected)
            {
                sfs.Disconnect();
                sfs.RemoveAllEventListeners();
            }
        }

        public void Send(string command, ISFSObject data)
        {
            sfs.Send(new ExtensionRequest(command, data));
        }

        #region Callback
        void OnConnection(BaseEvent e)
        {
            if ((bool)e.Params["success"])
            {
                Console.WriteLine("Connected to " + host + ":" + port);

                sfs.Send(new LoginRequest(userName, "", zoneName));
            }
            else
            {
                Console.WriteLine("Connection error!");
            }
        }

        void OnLogin(BaseEvent e)
        {
            Console.WriteLine("Login success : " + e.Params["user"]);
            sfs.Send(new JoinRoomRequest(roomName));
        }

        void OnLoginError(BaseEvent e)
        {
            Console.WriteLine(e.Params["errorMessage"].ToString());
        }

        void OnJoinRoom(BaseEvent e)
        {
            Console.WriteLine("JoinRoomm Success: " + e.Params["room"]);
        }

        void OnJoinRoomError(BaseEvent e)
        {
            Console.WriteLine(e.Params["errorMessage"].ToString());
        }
        void OnExtensionResponse(BaseEvent e)
        {
            string cmd = (string)e.Params["cmd"];
            ISFSObject data = (SFSObject)e.Params["params"];
            Console.WriteLine("Call form Visual : " + cmd + ", " + data.GetUtfString("callsign"));
            switch (cmd)
            {
                #region Ground Callback

                case "FinishReadyAircraft":
                    ground.FinishReadyCallback(data.GetUtfString("callsign"));
                    break;
                case "FinishPushback":
                    ground.FinishPushbackCallback(data.GetUtfString("callsign"), data.GetFloat("heading"));
                    break;
                case "TaxiPosition":
                    ground.TaxiPositionCallback(data.GetUtfString("callsign"), data.GetUtfString("position"), data.GetFloat("heading"));
                    break;
                case "FinishTaxi":
                    ground.FinishTaxiCallback(data.GetUtfString("callsign"), data.GetUtfString("position"), data.GetFloat("heading"), data.GetInt("status"));
                    break;
                case "HoldTaxi":
                    ground.HoldTaxiCallback(data.GetUtfString("callsign"), data.GetFloat("heading"));
                    break;
                case "RequestCrossRunway":
                    ground.RequestCrossRunwayCallback(data.GetUtfString("callsign"), data.GetFloat("heading"));
                    break;
                case "FinishUTurn":
                    ground.FinishUTurnCallback(data.GetUtfString("callsign"), data.GetFloat("heading"));
                    break;
                case "FinishHoldTakeOff":
                    ground.FinishHoldTakeOffCallback(data.GetUtfString("callsign"));
                    break;
                case "FinishTakeOff":
                    ground.FinishTakeOffCallback(data.GetUtfString("callsign"));
                    break;
                case "CrashAircraft":
                    ground.CrashAircraftCallback(data.GetUtfString("callsign"));
                    break;

                #endregion

                #region Flight Callback

                case "UpdateFlightRoute":
                    flight.UpdateFlightRoute(data.GetUtfString("callsign"), data.GetUtfString("position"));
                    break;
                case "FinishFlightRoute":
                    flight.FinishFlightRoute(data.GetUtfString("callsign"), data.GetUtfString("position"));
                    break;
                case "CollideAircraft":
                    flight.CollideAircraft(data.GetUtfString("callsign1"), data.GetUtfString("callsign2"));
                    break;

                #endregion

                #region Aircraft Callback
                case "EngineFailure":
                    other.EngineFailureCallback(data.GetUtfString("callsign"), data.GetInt("engine"));
                    break;
                case "DeniedAircraftMethod":
                    other.DeniedAircraftMethod(data.GetUtfString("callsign"), data.GetUtfString("method"));
                    break;
                #endregion

                #region Other Callback

                case "ReadySimulation":
                    other.ReadySimulation();
                    break;
                case "WeatherUpdate":
                    other.WeatherUpdate(data.GetUtfString("data"));
                    break;

                #endregion
            }
        }
        #endregion
    }
}
