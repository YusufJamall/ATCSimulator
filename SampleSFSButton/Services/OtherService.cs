﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Newtonsoft.Json;

namespace SampleSFSButton.Services
{
    public class OtherService
    {
        #region Response
        public void ReadySimulation()
        {
            SimulationService.ReadySimulation();
            Console.WriteLine("Simulation is Begin");
        }

        public void WeatherUpdate(string data)
        {
           
        }
        public void EngineFailureCallback(string callsign, int engine)
        {
            
        }

        public void DeniedAircraftMethod(string callsign, string method)
        {

        }

        #endregion

        #region Send

        #region Other Command

        #region Create Simulation
        public static void CreateSimulation(string data)
        {
            //string json = JsonConvert.SerializeObject(data);
            ISFSObject send = new SFSObject();
            send.PutUtfString("cmd", VisualServiceHelper.START_SIMULATION);
            send.PutUtfString("data", data);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, send);
        }
        #endregion

        #region Pause Simulation
        public static void PauseSimulation(bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PAUSE_SIMULATION);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Stop Simulation
        public static void StopSimulation()
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.STOP_SIMULATION);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Bird Attack
        public static void BirdAttack(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.BIRD_ATTACK);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Animal Crossing
        public static void AnimalCrossing(string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ANIMAL_CROSSING);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Taxi Light
        public static void TaxiLight( bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TAXI_LIGHT);
            data.PutBool("value",value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Papi Light
        public static void PapiLight( bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PAPI_LIGHT);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Runway Light
        public static void RunwayLight(string runway, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.RUNWAY_LIGHT);
            data.PutUtfString("runway", runway);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Approach Light
        public static void ApproachLight(string runway, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.APPROACH_LIGHT);
            data.PutUtfString("runway", runway);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Weather
        public static void Weather(int weather)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.WEATHER);
            data.PutInt("weather", weather);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Wind Direction
        public static void WindDirection(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.WIND_DIRECTION);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Wind Speed
        public static void WindSpeed(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.WIND_SPEED);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Temperature
        public static void Temperature(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TEMPERATURE);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Visibility
        public static void Visibility(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.VISIBILITY);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Time Simulation
        public static void TimeSimulation(int hour, int minute)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TIME_SIMULATION);
            data.PutInt("hour", hour);
            data.PutInt("minute", minute);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion


        #endregion

        #region Aircraft Command

        #region Select Aircraft
        public static void SelectAircraft(string callsign, string SendToTarget)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.SELECT_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("target", SendToTarget);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_TARGET, data);
            Console.WriteLine("Select aircraft callsign " + callsign + " target " + SendToTarget);
        }
        #endregion

        #region Remove Aircraft
        public static void RemoveAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.REMOVE_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Engine Failure
        public static void EngineFailure(string callsign, int engine)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ENGINE_FAILURE);
            data.PutUtfString("callsign", callsign);
            data.PutInt("engine", engine);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Change Fuel
        public static void ChangeFuel(string callsign, double fuel)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_FUEL);
            data.PutUtfString("callsign", callsign);
            data.PutDouble("fuel", fuel);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #endregion

        #endregion
    }
}
