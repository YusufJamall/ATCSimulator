﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Newtonsoft.Json;
using SampleSFSButton.Model;

namespace SampleSFSButton.Services
{
    public class GroundService
    {
        #region Response
        public void FinishReadyCallback(string callsign)
        {
            //SimulationService.FinishReadyDepartureCallback(callsign);
            Console.WriteLine("Call form Visual : Finish Ready " + callsign);
        }

        public void FinishPushbackCallback(string callsign, float heading)
        {
           // SimulationService.FinishPushbackCallback(callsign, heading);
            Console.WriteLine("Call form Visual : Finish Pushback " +callsign + " heading " + heading);
        }
        public void TaxiPositionCallback(string callsign, string position, float heading)
        {
          //  SimulationService.UpdateTaxiPositionCallback(callsign, position, heading);
            Console.WriteLine("Call form Visual : Taxi Position " + callsign + " heading " + heading +" position "+position);
        }

        public void FinishTaxiCallback(string callsign, string position, float heading, int status)
        {
          //  SimulationService.FinishTaxiCallback(callsign,position, heading,status);
            Console.WriteLine("Call form Visual : Finish Taxi " + callsign + " heading " + heading + " position " + position + " status " + (GroundStatusInfo)status);
        }

        public void HoldTaxiCallback(string callsign, float heading)
        {
           // SimulationService.HeadingAircraftCallback(callsign, heading);
            Console.WriteLine("Call form Visual : Hold Taxi " +callsign + " heading " + heading);
        }

        public void RequestCrossRunwayCallback(string callsign, float heading)
        {
            //SimulationService.RequestCrossRunwayCallback(callsign, heading);
            Console.WriteLine("Call form Visual : Request Cross Runway " + callsign + " heading " + heading);
        }
        public void FinishUTurnCallback(string callsign, float heading)
        {
          //  SimulationService.FinishUTurnCallback(callsign, heading);
            Console.WriteLine("Call form Visual : Finish U-Turn " + callsign + " heading " + heading);
        }

        public void FinishHoldTakeOffCallback(string callsign)
        {
          //  SimulationService.FinishHoldTakeOffCallback(callsign);
            Console.WriteLine("Call form Visual : Finish Runway Hold " + callsign);
        }

        public void FinishTakeOffCallback(string callsign)
        {
           // SimulationService.FinishTakeOffCallback(callsign);
            Console.WriteLine("Call form Visual : Finish Take Off " + callsign);
        }

        public void CrashAircraftCallback(string callsign)
        {
           // SimulationService.CrashAircraftCallback(callsign);
            Console.WriteLine("Call form Visual : Crash Aircraft " + callsign);
        }

        #endregion

        #region Send

        #region Ready Aircraft
        public static void ReadyAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.READY_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Engine Start
        public static void EngineStart(string callsign, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ENGINE_START);
            data.PutUtfString("callsign", callsign);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Pushback
        public static void Pushback(string callsign, PushbackInfo pushback)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PUSHBACK);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction", pushback == PushbackInfo.Left ? 0 : 1);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Start Taxi
        public static void StartTaxi(string callsign, List<string> routes, string target, int status)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.START_TAXI);
            data.PutUtfString("callsign", callsign);
            data.PutUtfStringArray("routes", routes.ToArray());
            data.PutUtfString("target", target);
            data.PutInt("status", status);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Taxi Speed
        public static void TaxiSpeed(string callsign, int speed)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TAXI_SPEED);
            data.PutUtfString("callsign", callsign);
            data.PutInt("speed", speed);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Hold Taxi
        public static void HoldTaxi(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLD_TAXI);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Continue Taxi
        public static void ContinueTaxi(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CONTINUE_TAXI);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Cross Runway
        public static void CrossRunway(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CROSS_RUNWAY);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Uturn
        public static void Uturn(string callsign, UturnInfo uturn)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.U_TURN);         
            data.PutUtfString("callsign", callsign);
            data.PutInt("uturn", (int)uturn);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Rocking The Wing
        public static void GroundRockingWing(string callsign, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.G_ROCKING_WING);
            data.PutUtfString("callsign", callsign);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }

        #endregion

        #region Hold Take Off
        public static void HoldTakeOff(string callsign, VisualFlightRouteModel routes, string runway)
        {
            string _routes = Newtonsoft.Json.JsonConvert.SerializeObject(routes);
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLD_TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("routes", _routes);
            data.PutUtfString("runway", runway);

            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Take Off
        public static void TakeOff(string callsign, VisualFlightRouteModel routes, string runway)
        {
            string _routes = JsonConvert.SerializeObject(routes);
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("routes", _routes);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Intersection Speed
        public static void IntersectionSpeed(string callsign, int speed)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.INTERSECTION_SPEED);
            data.PutUtfString("callsign", callsign);
            data.PutInt("speed", speed);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region PKPPK
        public static void PKPPK(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PKPPK);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }


        #endregion

        #endregion
    }
}
