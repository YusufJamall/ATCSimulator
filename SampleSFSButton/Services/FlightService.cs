﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Newtonsoft.Json;
using SampleSFSButton.Model;

namespace SampleSFSButton.Services
{
    public class FlightService
    {
        #region Response
        public void UpdateFlightRoute(string callsign, string position)
        {
            
        }

        public void FinishFlightRoute(string callsign, string position)
        {

        }

        public void CollideAircraft(string callsign1, string callsign2)
        {

        }

        #endregion

        #region Send

        #region Active Aircraft
        public static void ActiveAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ACTIVE_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Holding Now
        public static void HoldingNow(string callsign, int direction)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLDING_NOW);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction", direction);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Holding Position
        public static void HoldingPosition(string callsign,string position)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLDING_POSITION);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("position", position);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Stop Holding
        public static void StopHolding(string callsign, int direction)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.STOP_HOLDING);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction",direction);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Continue Route
        public static void ContinueRoute(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CONTINUE_ROUTE);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Direct Go
        public static void DirectGo(string callsign, string position)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.DIRECT_GO);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("position", position);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Change Heading
        public static void ChangeHeading(string callsign, int direction,float heading)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_HEADING);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction", direction);
            data.PutFloat("heading", heading);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Change Route
        public static void ChangeRoute(string callsign, VisualFlightRouteModel routes)
        {
            // json convert routes
            string _routes = JsonConvert.SerializeObject(routes);
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_ROUTE);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("routes", _routes);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Altitude
        public static void Altitude(string callsign, float altitude)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_HEADING);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("altitude", altitude);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Speed
        public static void Speed(string callsign, float speed)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.SPEED);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("speed", speed);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Approach
        public static void Approach(string callsign, string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.APPROACH);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Circuit
        public static void Circuit(string callsign, string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CIRCUIT);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Missed Approach
        public static void MissedApproach(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.MISSED_APPROACH);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Touch Go
        public static void TouchGo(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TOUCH_GO);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Flypass
        public static void Flypass(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.FLYPASS);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Flight Rocking Wing
        public static void FlightRockingWing(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.FLIGHT_ROCKING_WING);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Extend Downwind
        public static void ExtendDownwind(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.EXTEND_DOWNWIND);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Orbit
        public static void Orbit(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ORBIT);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Landing
        public static void Landing(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.LANDING);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Return Base
        public static void ReturnBase(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.RETURN_BASE);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Aborted Take Off
        public static void AbortedTakeOff(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ABORTED_TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Change Runway
        public static void ChangeRunway(string callsign, string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_RUNWAY);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #region Landing Gear Jam
        public static void LandingGearJam(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.LANDING_GEAR_JAM);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_VISUAL, data);
        }
        #endregion

        #endregion
    }
}
