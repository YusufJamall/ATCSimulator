﻿namespace TesReplay
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CounterTxt = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.mediaSlider19 = new MediaSlider.MediaSlider();
            this.Speed2 = new System.Windows.Forms.Button();
            this.Speed3 = new System.Windows.Forms.Button();
            this.SpeedNormal = new System.Windows.Forms.Button();
            this.Slow2 = new System.Windows.Forms.Button();
            this.Slow3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CounterTxt
            // 
            this.CounterTxt.Location = new System.Drawing.Point(12, 12);
            this.CounterTxt.Name = "CounterTxt";
            this.CounterTxt.Size = new System.Drawing.Size(100, 20);
            this.CounterTxt.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(105, 120);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // mediaSlider19
            // 
            this.mediaSlider19.Animated = true;
            this.mediaSlider19.AnimationSize = 0.2F;
            this.mediaSlider19.AnimationSpeed = MediaSlider.MediaSlider.AnimateSpeed.Normal;
            this.mediaSlider19.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.mediaSlider19.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.mediaSlider19.BackColor = System.Drawing.Color.Black;
            this.mediaSlider19.BackGroundImage = null;
            this.mediaSlider19.ButtonAccentColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(205)))), ((int)(((byte)(229)))));
            this.mediaSlider19.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(114)))), ((int)(((byte)(166)))));
            this.mediaSlider19.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(84)))), ((int)(((byte)(152)))));
            this.mediaSlider19.ButtonCornerRadius = ((uint)(6u));
            this.mediaSlider19.ButtonSize = new System.Drawing.Size(20, 10);
            this.mediaSlider19.ButtonStyle = MediaSlider.MediaSlider.ButtonType.RoundedRectInline;
            this.mediaSlider19.ContextMenuStrip = null;
            this.mediaSlider19.LargeChange = 2;
            this.mediaSlider19.Location = new System.Drawing.Point(33, 305);
            this.mediaSlider19.Margin = new System.Windows.Forms.Padding(0);
            this.mediaSlider19.Maximum = 3600;
            this.mediaSlider19.Minimum = 0;
            this.mediaSlider19.Name = "mediaSlider19";
            this.mediaSlider19.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.mediaSlider19.ShowButtonOnHover = false;
            this.mediaSlider19.Size = new System.Drawing.Size(691, 24);
            this.mediaSlider19.SliderFlyOut = MediaSlider.MediaSlider.FlyOutStyle.None;
            this.mediaSlider19.SmallChange = 1;
            this.mediaSlider19.SmoothScrolling = true;
            this.mediaSlider19.TabIndex = 14;
            this.mediaSlider19.TickColor = System.Drawing.Color.DarkGray;
            this.mediaSlider19.TickStyle = System.Windows.Forms.TickStyle.None;
            this.mediaSlider19.TickType = MediaSlider.MediaSlider.TickMode.Standard;
            this.mediaSlider19.TrackBorderColor = System.Drawing.Color.White;
            this.mediaSlider19.TrackDepth = 4;
            this.mediaSlider19.TrackFillColor = System.Drawing.Color.Transparent;
            this.mediaSlider19.TrackProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(101)))), ((int)(((byte)(188)))));
            this.mediaSlider19.TrackShadow = true;
            this.mediaSlider19.TrackShadowColor = System.Drawing.Color.DarkGray;
            this.mediaSlider19.TrackStyle = MediaSlider.MediaSlider.TrackType.Progress;
            this.mediaSlider19.Value = 1;
            this.mediaSlider19.ValueChanged += new MediaSlider.MediaSlider.ValueChangedDelegate(this.mediaSlider19_ValueChanged);
            this.mediaSlider19.Click += new System.EventHandler(this.mediaSlider19_Click);
            // 
            // Speed2
            // 
            this.Speed2.Location = new System.Drawing.Point(420, 356);
            this.Speed2.Name = "Speed2";
            this.Speed2.Size = new System.Drawing.Size(75, 23);
            this.Speed2.TabIndex = 15;
            this.Speed2.Text = "2x";
            this.Speed2.UseVisualStyleBackColor = true;
            this.Speed2.Click += new System.EventHandler(this.Speed2_Click);
            // 
            // Speed3
            // 
            this.Speed3.Location = new System.Drawing.Point(513, 356);
            this.Speed3.Name = "Speed3";
            this.Speed3.Size = new System.Drawing.Size(75, 23);
            this.Speed3.TabIndex = 16;
            this.Speed3.Text = "3x";
            this.Speed3.UseVisualStyleBackColor = true;
            this.Speed3.Click += new System.EventHandler(this.Speed3_Click);
            // 
            // SpeedNormal
            // 
            this.SpeedNormal.Location = new System.Drawing.Point(339, 356);
            this.SpeedNormal.Name = "SpeedNormal";
            this.SpeedNormal.Size = new System.Drawing.Size(75, 23);
            this.SpeedNormal.TabIndex = 17;
            this.SpeedNormal.Text = "1x";
            this.SpeedNormal.UseVisualStyleBackColor = true;
            this.SpeedNormal.Click += new System.EventHandler(this.SpeedNormal_Click);
            // 
            // Slow2
            // 
            this.Slow2.Location = new System.Drawing.Point(420, 385);
            this.Slow2.Name = "Slow2";
            this.Slow2.Size = new System.Drawing.Size(75, 23);
            this.Slow2.TabIndex = 18;
            this.Slow2.Text = "-2x";
            this.Slow2.UseVisualStyleBackColor = true;
            this.Slow2.Click += new System.EventHandler(this.Slow2_Click);
            // 
            // Slow3
            // 
            this.Slow3.Location = new System.Drawing.Point(513, 385);
            this.Slow3.Name = "Slow3";
            this.Slow3.Size = new System.Drawing.Size(75, 23);
            this.Slow3.TabIndex = 19;
            this.Slow3.Text = "-3x";
            this.Slow3.UseVisualStyleBackColor = true;
            this.Slow3.Click += new System.EventHandler(this.Slow3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 716);
            this.Controls.Add(this.Slow3);
            this.Controls.Add(this.Slow2);
            this.Controls.Add(this.SpeedNormal);
            this.Controls.Add(this.Speed3);
            this.Controls.Add(this.Speed2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.CounterTxt);
            this.Controls.Add(this.mediaSlider19);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox CounterTxt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private MediaSlider.MediaSlider mediaSlider19;
        private System.Windows.Forms.Button Speed2;
        private System.Windows.Forms.Button Speed3;
        private System.Windows.Forms.Button SpeedNormal;
        private System.Windows.Forms.Button Slow2;
        private System.Windows.Forms.Button Slow3;
    }
}

