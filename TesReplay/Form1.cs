﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
using System.Net.Sockets;
using System.Net;

namespace TesReplay
{
    public partial class Form1 : Form
    {
        int counter;
        string lineData;
        StreamReader file;
        UdpClient udp;
        IPEndPoint remoteEndPointVisual;
        string[] lines;
        int playSpeed = 1;
        public Form1()
        {
            InitializeComponent();
            file = new StreamReader("E:\\update.bin");
            remoteEndPointVisual = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8015);
            udp = new UdpClient();
            lines =  File.ReadLines("E:\\update.bin").ToArray();
            mediaSlider19.Maximum = lines.Count();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter = counter+playSpeed;
            CounterTxt.Text = counter.ToString();
            mediaSlider19.Value = (int)counter;
            lineData = lines[counter];
            SendUDP(lineData);
        }


        public void SendUDP(string message)
        {
            try
            {
                byte[] data = Encoding.ASCII.GetBytes(message);
                udp.Send(data, data.Length, remoteEndPointVisual);
            }
            catch (Exception err)
            {
                //error
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            file.Close();

        }

        private void mediaSlider19_ValueChanged(object sender, EventArgs e)
        {
            int value = mediaSlider19.Value;
            counter = value;
        }

        private void mediaSlider19_Click(object sender, EventArgs e)
        {
            int value = mediaSlider19.Value;
            counter = value;
        }

        private void SpeedNormal_Click(object sender, EventArgs e)
        {
            playSpeed = 1;
            timer1.Interval = 1;
        }

        private void Speed2_Click(object sender, EventArgs e)
        {
            playSpeed = 2;
            timer1.Interval = 1;
        }

        private void Speed3_Click(object sender, EventArgs e)
        {
            playSpeed = 3;
            timer1.Interval = 1;
        }

        private void Slow2_Click(object sender, EventArgs e)
        {
            playSpeed = 1;
            timer1.Interval = 30;
        }
        private void Slow3_Click(object sender, EventArgs e)
        {
            playSpeed = 1;
            timer1.Interval = 60;
        }

    }
}
