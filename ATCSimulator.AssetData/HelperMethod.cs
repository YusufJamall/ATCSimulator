﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.AssetData.Model;

namespace ATCSimulator.AssetData
{
        public class HelperMethod
        {
            private const double LATITUDE = -0.000008993222222222;
            private const double LONGITUDE = -0.000009033544444444;
            private const double CENTER_LATITUDE = -6.22491;
            private const double CENTER_LONGITUDE = 106.660782;

            public static GeolocationModel ConvertGeolocation(CartesianModel location)
            {
                double lat = -1 * ((location.y * LATITUDE) - CENTER_LATITUDE);
                double lon = -1 * ((location.x * LONGITUDE) - CENTER_LONGITUDE);
                return new GeolocationModel()
                {
                    latitude = lat,
                    longitude = lon
                };
            }

            public static Vector3D ConvertCartesian(double latitude, double longitude)
            {
                double y = (CENTER_LATITUDE - latitude) / LATITUDE;
                double x = (CENTER_LONGITUDE - longitude) / LONGITUDE;
                return new Vector3D()
                {
                    X = x,
                    Y = 0,
                    Z = y
                };
            }
        }
        public class GeolocationModel
        {
            public double latitude { get; set; }
            public double longitude { get; set; }
        }
        public class CartesianModel
        {
            public double x { get; set; }
            public double y { get; set; }
        }

}
