﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATCSimulator.AssetData.Model
{
    public class TaxiModel
    {
        public int id { get; set; }
        public string from { get; set; }
        public string current { get; set; }
        public string to { get; set; }
        public bool IsIntersection { get; set; }
        public bool IsParking { get; set; }

    }
    public class IntersectionModel
    {
        public int id { get; set; }
        public string intersection { get; set; }
        public string runway { get; set; }
        public bool isHelipad { get; set; }
    }
    public class ParkingModel
    {
        public int id { get; set; }
        public string current { get; set; }
        public string parking { get; set; }
    }
    public class GroundRouteModel
    {
        public List<ParkingModel> parking { get; set; }
        public List<TaxiModel> taxi { get; set; }
        public List<IntersectionModel> intersection { get; set; }
    }
}
