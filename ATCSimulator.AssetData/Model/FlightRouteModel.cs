﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATCSimulator.AssetData.Model
{
    public class AirportModel
    {
        public string code { get; set; }
        public string name { get; set; }
    }
    public class FlightRouteModel
    {
        public int id { get; set; }
        public bool is_departure { get; set; }
        public List<string> runways { get; set; }
        public int flight_rules_id { get; set; }
        public string flight_rules { get; set; }
        public AirportModel origin { get; set; }
        public AirportModel destination { get; set; }
        public AirportModel alternate { get; set; }
        public List<string> routes { get; set; }
        public string posititon { get; set; }
        public double total_distance { get; set; }
    }
}
