//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ATCSimulator.AssetData.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Base_Scenery
    {
        public Base_Scenery()
        {
            this.Base_Appron = new HashSet<Base_Appron>();
            this.Base_Exercise = new HashSet<Base_Exercise>();
            this.Base_ExerciseArrival = new HashSet<Base_ExerciseArrival>();
            this.Base_ExerciseDeparture = new HashSet<Base_ExerciseDeparture>();
            this.Base_Airport = new HashSet<Base_Airport>();
            this.Base_Terminal = new HashSet<Base_Terminal>();
            this.Waypoint_ArrivalRoute = new HashSet<Waypoint_ArrivalRoute>();
            this.Waypoint_DepartureRoute = new HashSet<Waypoint_DepartureRoute>();
            this.Waypoint_Ground = new HashSet<Waypoint_Ground>();
        }
    
        public int ID { get; set; }
        public int AirportID { get; set; }
        public string Name { get; set; }
        public string RunwayLight { get; set; }
        public bool Taxi { get; set; }
        public bool Papi { get; set; }
        public string ApproachLight { get; set; }
    
        public virtual ICollection<Base_Appron> Base_Appron { get; set; }
        public virtual ICollection<Base_Exercise> Base_Exercise { get; set; }
        public virtual ICollection<Base_ExerciseArrival> Base_ExerciseArrival { get; set; }
        public virtual ICollection<Base_ExerciseDeparture> Base_ExerciseDeparture { get; set; }
        public virtual ICollection<Base_Airport> Base_Airport { get; set; }
        public virtual Base_Airport Base_Airport1 { get; set; }
        public virtual ICollection<Base_Terminal> Base_Terminal { get; set; }
        public virtual ICollection<Waypoint_ArrivalRoute> Waypoint_ArrivalRoute { get; set; }
        public virtual ICollection<Waypoint_DepartureRoute> Waypoint_DepartureRoute { get; set; }
        public virtual ICollection<Waypoint_Ground> Waypoint_Ground { get; set; }
    }
}
