﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.AssetData.Model;
using ATCSimulator.AssetData.Data;
using Newtonsoft.Json;

namespace ATCSimulator.AssetData.Controller
{
    public class CreateData
    {
        public static List<DataAppronModel> AppronData(int scenery_id)
        {
            List<DataAppronModel> result = new List<DataAppronModel>();
            using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
            {
                List<Base_Appron> data = entity.Base_Appron.Where(d => d.SceneryID.Equals(scenery_id)).ToList();
                foreach (Base_Appron p in data)
                {
                    DataAppronModel appron = new DataAppronModel()
                    {
                        appron_name = p.AppronName,
                        can_pushback = p.CanPushback,
                        id = p.ID,
                        terminal_name = p.TerminalName,
                        pushback_left = p.PushbackLeft,
                        pushback_right = p.PushbackRight
                    };
                    result.Add(appron);
                }
            }
            return result;
        }

        #region Flight Route List
        public static List<FlightRouteModel> FlightRouteList(int scenery_id)
        {
            List<FlightRouteModel> result = new List<FlightRouteModel>();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<FlightRouteModel> departure = entity.Waypoint_DepartureRoute.Where(d => d.SceneryID.Equals(scenery_id)).AsEnumerable()
                        .Select(d => new FlightRouteModel()
                        {
                            alternate = new AirportModel()
                            {
                                code = d.AlternateAirportCode,
                                name = d.AlternateAirportName
                            },
                            origin = new AirportModel()
                            {
                                code = d.OriginAirportCode,
                                name = d.OriginAirportName
                            },
                            destination = new AirportModel()
                            {
                                code = d.DestAirportCode,
                                name = d.DestAirportName
                            },
                            posititon = d.EndPointName,
                            routes = JsonConvert.DeserializeObject<List<string>>(d.RoutesName),
                            flight_rules = d.FlightRules,
                            flight_rules_id = d.FlightRulesID,
                            id = d.ID,
                            is_departure = true,
                            runways = JsonConvert.DeserializeObject<List<string> >(d.RunwayUsed),
                            total_distance = d.TotalDistance
                        }).ToList();
                    List<FlightRouteModel> arrival = entity.Waypoint_ArrivalRoute.Where(d => d.SceneryID.Equals(scenery_id)).AsEnumerable()
                        .Select(d => new FlightRouteModel()
                        {
                            alternate = new AirportModel()
                            {
                                code = d.AlternateAirportCode,
                                name = d.AlternateAirportName
                            },
                            origin = new AirportModel()
                            {
                                code = d.OriginAirportCode,
                                name = d.OriginAirportName
                            },
                            destination = new AirportModel()
                            {
                                code = d.DestinationAirportCode,
                                name = d.DestinationAirportName
                            },
                            posititon = d.StartPointName,
                            routes = JsonConvert.DeserializeObject<List<string>>(d.RoutesName),
                            flight_rules = d.FlightRules,
                            flight_rules_id = d.FlightRulesID,
                            id = d.ID,
                            is_departure = false,
                            runways = JsonConvert.DeserializeObject<List<string>>(d.RunwayUsed),
                            total_distance = d.TotalDistance
                        }).ToList();
                    result = departure;
                    result.AddRange(arrival);
                }
            }
            catch (Exception ex)
            {
                
            }
            return result;
        }
        #endregion
    }
}
