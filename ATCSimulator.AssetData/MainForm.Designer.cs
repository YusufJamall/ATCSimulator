﻿namespace ATCSimulator.AssetData
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SceneryList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AppronBtn = new System.Windows.Forms.Button();
            this.FlightRuteList = new System.Windows.Forms.Button();
            this.TaxiBtn = new System.Windows.Forms.Button();
            this.RunwayBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SceneryList
            // 
            this.SceneryList.FormattingEnabled = true;
            this.SceneryList.Location = new System.Drawing.Point(80, 12);
            this.SceneryList.Name = "SceneryList";
            this.SceneryList.Size = new System.Drawing.Size(121, 25);
            this.SceneryList.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Scenery";
            // 
            // AppronBtn
            // 
            this.AppronBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.AppronBtn.Location = new System.Drawing.Point(14, 57);
            this.AppronBtn.Name = "AppronBtn";
            this.AppronBtn.Size = new System.Drawing.Size(121, 30);
            this.AppronBtn.TabIndex = 2;
            this.AppronBtn.Text = "Appron";
            this.AppronBtn.UseVisualStyleBackColor = true;
            this.AppronBtn.Click += new System.EventHandler(this.AppronBtn_Click);
            // 
            // FlightRuteList
            // 
            this.FlightRuteList.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.FlightRuteList.Location = new System.Drawing.Point(141, 57);
            this.FlightRuteList.Name = "FlightRuteList";
            this.FlightRuteList.Size = new System.Drawing.Size(121, 30);
            this.FlightRuteList.TabIndex = 4;
            this.FlightRuteList.Text = "Flight Route List";
            this.FlightRuteList.UseVisualStyleBackColor = true;
            this.FlightRuteList.Click += new System.EventHandler(this.FlightRuteList_Click);
            // 
            // TaxiBtn
            // 
            this.TaxiBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.TaxiBtn.Location = new System.Drawing.Point(268, 57);
            this.TaxiBtn.Name = "TaxiBtn";
            this.TaxiBtn.Size = new System.Drawing.Size(121, 30);
            this.TaxiBtn.TabIndex = 5;
            this.TaxiBtn.Text = "Taxi Route";
            this.TaxiBtn.UseVisualStyleBackColor = true;
            this.TaxiBtn.Click += new System.EventHandler(this.TaxiBtn_Click);
            // 
            // RunwayBtn
            // 
            this.RunwayBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.RunwayBtn.Location = new System.Drawing.Point(221, 15);
            this.RunwayBtn.Name = "RunwayBtn";
            this.RunwayBtn.Size = new System.Drawing.Size(121, 30);
            this.RunwayBtn.TabIndex = 6;
            this.RunwayBtn.Text = "Runway";
            this.RunwayBtn.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.button1.Location = new System.Drawing.Point(348, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 30);
            this.button1.TabIndex = 7;
            this.button1.Text = "ParkingRoute";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 95);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.RunwayBtn);
            this.Controls.Add(this.TaxiBtn);
            this.Controls.Add(this.FlightRuteList);
            this.Controls.Add(this.AppronBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SceneryList);
            this.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "AssetForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SceneryList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AppronBtn;
        private System.Windows.Forms.Button FlightRuteList;
        private System.Windows.Forms.Button TaxiBtn;
        private System.Windows.Forms.Button RunwayBtn;
        private System.Windows.Forms.Button button1;
    }
}

