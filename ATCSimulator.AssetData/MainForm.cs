﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATCSimulator.AssetData.Controller;
using ATCSimulator.AssetData.Model;
using Newtonsoft.Json;
using ATCSimulator.AssetData.Data;
using System.IO;
using Excel;

namespace ATCSimulator.AssetData
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void AppronBtn_Click(object sender, EventArgs e)
        {
            if (SceneryList.SelectedIndex == -1)
            {
                return;
            }
            try
            {
                ComboboxItem scenery = SceneryList.SelectedItem as ComboboxItem;
                List<DataAppronModel> appron = CreateData.AppronData((int)scenery.Value);
                string text = JsonConvert.SerializeObject(appron);
                string file_path = System.IO.Directory.GetCurrentDirectory() + "\\Asset\\" + scenery.Text + "\\Appron.json";
                System.IO.File.WriteAllText(file_path, text);
                MessageBox.Show("Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
            {
                List<Base_Scenery> sceneries = entity.Base_Scenery.ToList();
                foreach (Base_Scenery s in sceneries)
                {
                    ComboboxItem item = new ComboboxItem()
                    {
                        Text = s.Name,
                        Value = s.ID
                    };
                    SceneryList.Items.Add(item);
                }
            }
        }
        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void FlightRuteList_Click(object sender, EventArgs e)
        {
            if (SceneryList.SelectedIndex == -1)
            {
                return;
            }
            try
            {
                ComboboxItem scenery = SceneryList.SelectedItem as ComboboxItem;
                List<FlightRouteModel> routes = CreateData.FlightRouteList((int)scenery.Value);
                string text = JsonConvert.SerializeObject(routes);
                string file_path = System.IO.Directory.GetCurrentDirectory() + "\\Asset\\" + scenery.Text + "\\FlightRoute.json";
                System.IO.File.WriteAllText(file_path, text);
                MessageBox.Show("Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void TaxiBtn_Click(object sender, EventArgs e)
        {
            if (SceneryList.SelectedIndex == -1)
            {
                return;
            }
            try
            {
                ComboboxItem scenery = SceneryList.SelectedItem as ComboboxItem;
                string path = @"E:\ATCSimulator\Dokumen\RouteData\";
                GroundRouteModel routes = new GroundRouteModel();
                routes.taxi = GetTaxiRoute(path+"TaxiRoute.xlsx");
                routes.intersection = GetIntersectionRoute(path + "Intersection Route.xlsx");
                routes.parking = GetParkingRoute(path + "Parking Route.xlsx");
                string text = JsonConvert.SerializeObject(routes);
                string file_path = System.IO.Directory.GetCurrentDirectory() + "\\Asset\\" + scenery.Text + "\\GroundRoute.json";
                System.IO.File.WriteAllText(file_path, text);
                MessageBox.Show("Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
            
        }

        private List<TaxiModel> GetTaxiRoute(string path)
        {
            List<TaxiModel> returnData = new List<TaxiModel>();
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            DataSet result = excelReader.AsDataSet();

            foreach (DataRow dataRow in result.Tables[0].Rows)
            {
                TaxiModel t = new TaxiModel()
                {
                    id = int.Parse(dataRow[0].ToString()),
                    from = dataRow[1].ToString(),
                    current = dataRow[2].ToString(),
                    to = dataRow[3].ToString(),
                    IsIntersection = bool.Parse(dataRow[4].ToString()),
                    IsParking = bool.Parse(dataRow[5].ToString())
                };
                returnData.Add(t);
            }
            excelReader.Close();
            return returnData;
        }
        private List<IntersectionModel> GetIntersectionRoute(string path)
        {
            List<IntersectionModel> returnData = new List<IntersectionModel>();
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            DataSet result = excelReader.AsDataSet();

            foreach (DataRow dataRow in result.Tables[0].Rows)
            {
                IntersectionModel t = new IntersectionModel()
                {
                    id = int.Parse(dataRow[0].ToString()),
                    intersection = dataRow[1].ToString(),
                    runway = dataRow[2].ToString(),
                    isHelipad = bool.Parse(dataRow[3].ToString())
                };
                returnData.Add(t);
            }
            excelReader.Close();
            return returnData;
        }
        private List<ParkingModel> GetParkingRoute(string path)
        {
            List<ParkingModel> returnData = new List<ParkingModel>();
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            DataSet result = excelReader.AsDataSet();

            foreach (DataRow dataRow in result.Tables[0].Rows)
            {
                ParkingModel t = new ParkingModel()
                {
                    id = int.Parse(dataRow[0].ToString()),
                    current = dataRow[1].ToString(),
                    parking = dataRow[2].ToString()
                };
                returnData.Add(t);
            }
            excelReader.Close();
            return returnData;
        }
    }
}
