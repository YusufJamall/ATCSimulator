﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using ATCSimulator.DummySystem.Model;

namespace ATCSimulator.DummySystem.Controller
{
    class GroundController
    {

        #region Constant Variable

        const string SEND_TO_VISUAL = "SendToVisual";

        const string READY_AIRCRAFT = "ReadyAircraft";
        const string ENGINE_START = "Engine";
        const string PUSHBACK = "Pushback";
        const string START_TAXI = "StartTaxi";
        const string TAXI_SPEED = "TaxiSpeed";
        const string HOLD_TAXI = "HoldTaxi";
        const string CONTINUE_TAXI = "ContinueTaxi";
        const string CROSS_RUNWAY = "CrossRunway";
        const string U_TURN = "UTurn";
        const string G_ROCKING_WING = "GroundRockingWing";
        const string HOLD_TAKE_OFF = "HoldTakeOff";
        const string TAKE_OFF = "TakeOff";
        const string INTERSECTION_SPEED = "IntersectionSpeed";


        #endregion

        private SmartFox sfs;
        public GroundController(SmartFox sfs)
        {
            this.sfs = sfs;
        }

        #region Send Request To Visual

        #region Ready Aircraft
        public void ReadyAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", READY_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Engine Start
        public void EngineStart(string callsign, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", ENGINE_START);
            data.PutUtfString("callsign", callsign);
            data.PutBool("value", value);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Pushback
        public void Pushback(string callsign, PushbackInfo pushback)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", PUSHBACK);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction", pushback == PushbackInfo.Left ? 0 : 1);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Taxi

        #region Start Taxi
        public void StartTaxi(string callsign, List<string> routes)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", START_TAXI);
            data.PutUtfString("callsign", callsign);
            data.PutUtfStringArray("routes", routes.ToArray());
            data.PutInt("status", 0);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Taxi Speed
        public void TaxiSpeed(string callsign, int speed)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", TAXI_SPEED);
            data.PutUtfString("callsign", callsign);
            data.PutInt("speed", speed);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Hold Taxi
        public void HoldTaxi(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", HOLD_TAXI);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Continue Taxi
        public void ContinueTaxi(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", CONTINUE_TAXI);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Cross Runway
        public void CrossRunway(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", CROSS_RUNWAY);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #endregion

        #region U-Turn and Rocking Wing

        #region Uturn
        public void Uturn(string callsign, UturnInfo uturn)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", U_TURN);
            data.PutInt("uturn", (int)uturn);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Rocking The Wing
        public void GroundRockingWing(string callsign, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", G_ROCKING_WING);
            data.PutUtfString("callsign", callsign);
            data.PutBool("value", value);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }

        #endregion

        #endregion

        #region Take Off

        #region Hold Take Off
        public void HoldTakeOff(string callsign, List<string> routes, string runway, int rules)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", HOLD_TAKE_OFF);
            data.PutUtfStringArray("routes", routes.ToArray());
            data.PutUtfString("runway", runway);
            data.PutInt("rules", rules);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Take Off
        public void TakeOff(string callsign, List<string> routes, string runway, int rules)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", TAKE_OFF);
            data.PutUtfStringArray("routes", routes.ToArray());
            data.PutUtfString("runway", runway);
            data.PutInt("rules", rules);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Intersection Speed
        public void IntersectionSpeed(string callsign, int speed)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", INTERSECTION_SPEED);
            data.PutUtfString("callsign", callsign);
            data.PutInt("speed", speed);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #endregion

        #endregion

        #region Response From Visual

        #endregion
    }
}
