﻿using ATCSimulator.DummySystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummySystem.Helper
{
    public class InitData
    {
        public static ExerciseSimulationModel CreateExerciseData()
        {
            ExerciseSimulationModel data = new ExerciseSimulationModel()
            {
                approach_lights = new Dictionary<string, bool>(),
                desc_exercise = "",
                exercise_name = "",
                id = 0,
                papi = true,
                play_time = new TimeSpan(0,30,0),
                runway_lights = new Dictionary<string,bool>(),
                scenery_id = 0,
                scenery_name = "SEAHORSE",
                taxi = true,
                temperature = 10,
                time_simulation = new TimeSpan(14,30,0),
                traffic_exercise = "",
                visibility = 1000,
                weather = WeatherInfo.LightRain,
                wind_direction = 30,
                wind_speed = 100
            };
            data.approach_lights.Add("07", true);
            data.approach_lights.Add("25",true);
            data.approach_lights.Add("34L",true);
            data.approach_lights.Add("34R",true);
            data.approach_lights.Add("16R",true);
            data.approach_lights.Add("16L",true);

            data.runway_lights.Add("34R-16L", true);
            data.runway_lights.Add("34L-16R",true);
            data.runway_lights.Add("07-25",true);
            return data;
        }
        public static List<AircraftData> CreateDataAircrafts(FlightRouteListModel flight_route)
        {
            List<AircraftData> aircrafts = new List<AircraftData>();
            #region Aircraft 1 
            AircraftData aircraft1 = new AircraftData()
            {
                callsign = "LA 001",
                current_position = "A1",
                departure_time = new TimeSpan(0, 0, 0),
                engine_failure = 0,
                fuel = new TimeSpan(1, 30, 0),
                heading = 0,
                is_departure = true,
                is_enabled = true,
                pilot_properties = new PilotCommandProperties()
                {
                    flight = new PilotFlightCommand()
                    {
                        flight_route = flight_route
                    },
                    ground = new PilotGroundCommand()
                    {
                        engine = false,
                        engine_enabled = false,
                        intersection = new PilotIntersectionCommand()
                        {
                            is_show = false,
                            runway_hold = false,
                            speed = GroundSpeed.Normal,
                            status = CommandStatus.None,
                            take_off = false
                        },
                        other = new PilotOtherCommand()
                        {
                            rocking_wing = false,
                            u_turn = false,
                            u_turn_status = UturnInfo.None
                        },
                        pushback = new PilotPushbackCommand()
                        {
                            is_show = false,
                            pushback_left = false,
                            pushback_left_enabled = false,
                            pushback_left_alias = "",
                            pushback_right_alias = "",
                            pushback_right = false,
                            pushback_right_enabled = false,
                            status = CommandStatus.None
                        },
                        runway = new PilotRunwayCommand()
                        {
                            is_show = false,
                            runway = flight_route.runways.FirstOrDefault(),
                        },
                        status = GroundStatusInfo.Departure,
                        taxi = new PilotTaxiCommand()
                        {
                            cross_runway = false,
                            is_show = false,
                            routes = new List<string>(),
                            speed = GroundSpeed.Normal,
                            status = TaxiStatus.None,
                            taxi_route = false,
                            taxi_route_enabled = false
                        },
                    },
                    ready_departure = new PilotDepartureReadyCommand()
                    {
                        is_enabled = false
                    },
                    status = PilotCommandMenu.DepartureReady
                },
                properties = new AircraftProperties()
                {
                    category = "",
                    engine = 2,
                    flight_level = 20,
                    flight_level_type = "A",
                    flight_type = "A",
                    livery = "LION AIR",
                    livery_strips = "",
                    model = "A310",
                    person = 100,
                    true_speed = 100,
                    type = "Jet"
                },
                squawk = "",
                start_time = new TimeSpan(0, 0, 0),
                status = AircraftStatus.NONE,
                user = new UserPilotModel()
            };
            aircrafts.Add(aircraft1);
            #endregion
            return aircrafts;
        }
    }
}
