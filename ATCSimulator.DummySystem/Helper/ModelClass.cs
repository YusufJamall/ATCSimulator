﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummySystem.Helper
{
    public class VisualConfig
    {
        public string host { get; set; }
        public int port { get; set; }
        public string zone_name { get; set; }
        public string room_name { get; set; }
        public string user_name { get; set; }
    }
}
