﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using ATCSimulator.DummySystem.Model;
using ATCSimulator.DummySystem.Controller;

namespace ATCSimulator.DummySystem.Helper
{
    class VisualNetwork
    {
        #region Constant Variable

        const string SEND_TO_VISUAL = "SendToVisual";

        const string SELECT_AIRCRAFT = "SelectAircraft";
        const string REMOVE_AIRCRAFT = "RemoveAircraft";


        #endregion

        public static VisualNetwork instance;
        private string host;
        private int port;
        private string zoneName;
        private string roomName;
        private string userName;
        private SmartFox sfs;

        public GroundController ground;
        public FlightController flight;

        public VisualNetwork(VisualConfig visual_config)
        {
            instance = this;
            this.host = visual_config.host;
            this.port = visual_config.port;
            this.zoneName = visual_config.zone_name;
            this.roomName = visual_config.room_name;
            this.userName = visual_config.user_name;
            sfs = new SmartFox();
            sfs.ThreadSafeMode = false;
            ground = new GroundController(sfs);
            flight = new FlightController(sfs);
        }
        public void Connect()
        {
            sfs.Connect(host, port);
            sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
            sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
            sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoom);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);
            sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
        }
        public void Disconnect()
        {
            if (sfs.IsConnected)
            {
                sfs.Disconnect();
                sfs.RemoveAllEventListeners();
            }
        }

        #region Callback
        void OnConnection(BaseEvent e)
        {
            if ((bool)e.Params["success"])
            {
                MainForm.LogMessage("Connected to " + host + ":" + port);

                sfs.Send(new LoginRequest(userName, "", zoneName));
            }
            else
            {
                MainForm.LogMessage("Connection error!");
            }
        }

        void OnLogin(BaseEvent e)
        {
            MainForm.LogMessage("Login success : " + e.Params["user"]);
            sfs.Send(new JoinRoomRequest(roomName));
        }

        void OnLoginError(BaseEvent e)
        {
            MainForm.LogMessage(e.Params["errorMessage"].ToString());
        }

        void OnJoinRoom(BaseEvent e)
        {
            MainForm.LogMessage("JoinRoomm Success: " + e.Params["room"]);
        }

        void OnJoinRoomError(BaseEvent e)
        {
            MainForm.LogMessage(e.Params["errorMessage"].ToString());
        }
        void OnExtensionResponse(BaseEvent e)
        {
            string cmd = (string)e.Params["cmd"];
            ISFSObject data = (SFSObject)e.Params["params"];
            MainForm.LogMessage("Call form Visual : " + cmd + ", " + data.GetUtfString("callsign"));
            switch (cmd)
            {
                case "FinishReadyAircraft":
                    //SelectAircraft(data);
                    break;
                case "FinishPushback":
                    //ReadyAircraft(data);
                    break;
                case "TaxiIntersection":
                    //EngineStart(data);
                    break;
                case "TaxiCrossRunway":
                    //Pushback(data);
                    break;
                case "FinishUTurn":
                    //StartTaxi(data);
                    break;
                case "Finish180":
                    //RemoveAircraft(data);
                    break;
            }
        }
        #endregion

        #region Send Request To Visual

        #region Create Simulation
        public void CreateSimulation(ExerciseSimulationModel exercise, List<AircraftData> aircrafts)
        {

            List<VisualAircraftData> visual_aircrafts = new List<VisualAircraftData>();
            foreach (AircraftData ac in aircrafts)
            {
                VisualAircraftData aircraft = new VisualAircraftData()
                {
                    callsign = ac.callsign,
                    flight = ac.is_departure ? VisualFlight.Departure : VisualFlight.Arrivals,
                    flight_routes = ac.pilot_properties.flight.flight_route.routes_name,
                    flight_rules = ac.pilot_properties.flight.flight_route.flight_rules,
                    fuel = ac.fuel.TotalSeconds,
                    livery = ac.properties.livery,
                    model = ac.properties.model,
                    position = ac.current_position,
                    start_time = new TimeVisual()
                    {
                        hour = ac.start_time.Hours,
                        minutes = ac.start_time.Minutes
                    }
                };
                visual_aircrafts.Add(aircraft);
            };
            VisualSimulationData data = new VisualSimulationData()
            {
                exercise = new VisualExerciseData()
                {
                    approach_lights = exercise.approach_lights,
                    papi = exercise.papi,
                    aircrafts = visual_aircrafts,
                    runway_lights = exercise.runway_lights,
                    scenery_name = exercise.scenery_name,
                    taxi = exercise.taxi,
                    temperature = exercise.temperature,
                    time_simulation = new TimeVisual()
                    {
                        hour = exercise.time_simulation.Hours,
                        minutes = exercise.time_simulation.Minutes
                    },
                    visibility = exercise.visibility,
                    weather = exercise.weather,
                    wind_direction = exercise.wind_direction,
                    wind_speed = exercise.wind_speed
                }
            };
        }
        #endregion

        #region Select Aircraft
        public void SelectAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", SELECT_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #region Remove Aircraft
        public void RemoveAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", REMOVE_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            sfs.Send(new ExtensionRequest(SEND_TO_VISUAL, data));
        }
        #endregion

        #endregion
    }
}