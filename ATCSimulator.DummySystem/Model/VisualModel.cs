﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummySystem.Model
{

    public enum VisualFlight
    {
        Departure,
        Arrivals
    }
    public class VisualAircraftData
    {
        public string callsign { get; set; }
        public string model { get; set; }
        public string livery { get; set; }
        public double fuel { get; set; }
        public VisualFlight flight { get; set; }
        public string position { get; set; }
        public TimeVisual start_time { get; set; }
        public List<string> flight_routes { get; set; }
        public string flight_rules { get; set; }
    }
    public class TimeVisual
    {
        public int hour { get; set; }
        public int minutes { get; set; }

    }
    public class VisualExerciseData
    {
        public string scenery_name { get; set; }
        public WeatherInfo weather { get; set; }
        public int wind_direction { get; set; }
        public int wind_speed { get; set; }
        public int temperature { get; set; }
        public int visibility { get; set; }
        public TimeVisual time_simulation { get; set; }
        public Dictionary<string, bool> runway_lights { get; set; }
        public Dictionary<string, bool> approach_lights { get; set; }
        public bool taxi { get; set; }
        public bool papi { get; set; }
        public List<VisualAircraftData> aircrafts { get; set; }
    }

    public class VisualSimulationData
    {
        public VisualExerciseData exercise { get; set; }
    }
}
