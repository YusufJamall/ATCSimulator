﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.DummySystem.Model
{
    public enum WeatherInfo
    {
        Clear,
        LightRain,
        ThunderStorm,
        Cloudy,
    }
    public class ExerciseSimulationModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public string scenery_name { get; set; }
        public TimeSpan play_time { get; set; }
        public string exercise_name { get; set; }
        public string traffic_exercise { get; set; }
        public string desc_exercise { get; set; }
        public WeatherInfo weather { get; set; }
        public int wind_direction { get; set; }
        public int wind_speed { get; set; }
        public int temperature { get; set; }
        public int visibility { get; set; }
        public TimeSpan time_simulation { get; set; }
        public Dictionary<string, bool> runway_lights { get; set; }
        public Dictionary<string, bool> approach_lights { get; set; }
        public bool taxi { get; set; }
        public bool papi { get; set; }
    }
}
