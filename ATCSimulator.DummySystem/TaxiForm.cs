﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using Newtonsoft.Json;

namespace ATCSimulator.DummySystem
{
    public partial class TaxiForm : Form
    {
        private const string FIND_TAXING = "FindTaxing";
        private const string RESET_POINT = "ResetPointData";
        private const string SHOW_LEGEND = "ShowLegend";
        private const string REQUEST_ROUTES = "GetRoute";

        List<string> routes;
        private MainForm pilot;
        public TaxiForm(MainForm pilot)
        {
            InitializeComponent();
            this.pilot = pilot;
        }
        public void Init(string scenery_name)
        {
            string file_path = System.IO.Directory.GetCurrentDirectory() + "\\Asset\\Pilot.swf";
            FlashContainer.Movie = file_path;
            FlashContainer.Play();
            routes = new List<string>();
        }
        public void TaxiToIntersection(string current_position,double heading,string runway)
        {
            StatusTaxi.Text = "Find Intersection";
            TargetLabel.Text = runway;
            string send_data = string.Format("{0}&{1}", current_position,heading);
            SendStringToFlash(FIND_TAXING, send_data);
        }
        private void SendStringToFlash(string function_name, string sendstring)
        {
            FlashContainer.CallFunction("<invoke name=\"" + function_name + "\" returntype=\"xml\"><arguments><string>" + sendstring + "</string></arguments></invoke>");
        }
        private void FlashContainer_FlashCall(object sender, AxShockwaveFlashObjects._IShockwaveFlashEvents_FlashCallEvent e)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(e.request);
            XmlAttributeCollection attributes = document.FirstChild.Attributes;
            String command = attributes.Item(0).InnerText;
            XmlNodeList list = document.GetElementsByTagName("arguments");
            String arg = list.Item(0).InnerText;
            switch (command)
            {
                case "GetRouteData":
                    routes = JsonConvert.DeserializeObject<List<string>>(arg);
                    if (routes.Count>0)
                    {
                        pilot.SetTaxiRoute(routes);
                        this.Hide();
                    }
                    else
                        MessageBox.Show("Taxi Route must be defined at least 1 ");
                break;
            }
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void ResetBtn_Click(object sender, EventArgs e)
        {
            SendStringToFlash(RESET_POINT, "");
            routes = new List<string>();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            SendStringToFlash(REQUEST_ROUTES, "");
        }

        private void LegendBtn_CheckedChanged(object sender, EventArgs e)
        {
            SendStringToFlash(SHOW_LEGEND, LegendBtn.Checked.ToString());
        }
    }
}
