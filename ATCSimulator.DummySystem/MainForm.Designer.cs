﻿namespace ATCSimulator.DummySystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem18 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem19 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem20 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem21 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem22 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem23 = new Telerik.WinControls.UI.RadListDataItem();
            this.AircraftCommandView = new Telerik.WinControls.UI.RadPageView();
            this.GroundPageView = new Telerik.WinControls.UI.RadPageViewPage();
            this.ReadyAircraft = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button8 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radDropDownList7 = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.radDropDownList3 = new Telerik.WinControls.UI.RadDropDownList();
            this.TakeOffGroup = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.ChangeSpeedIntersection = new System.Windows.Forms.Button();
            this.IntersectionSpeedList = new Telerik.WinControls.UI.RadDropDownList();
            this.TakeOff = new System.Windows.Forms.Button();
            this.RunwayHold = new System.Windows.Forms.Button();
            this.FlightRouteGroup = new System.Windows.Forms.GroupBox();
            this.Runway = new System.Windows.Forms.GroupBox();
            this.RunwayList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.FlightRouteList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DestinationList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.FlightRulesList = new Telerik.WinControls.UI.RadDropDownList();
            this.TaxiGroup = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ChangeSpeedTaxi = new System.Windows.Forms.Button();
            this.TaxiSpeedList = new Telerik.WinControls.UI.RadDropDownList();
            this.button2 = new System.Windows.Forms.Button();
            this.StartTaxi = new System.Windows.Forms.Button();
            this.CrossRW = new System.Windows.Forms.Button();
            this.TaxiRoute = new System.Windows.Forms.Button();
            this.OtherCommandGroup = new System.Windows.Forms.GroupBox();
            this.RockingWing = new System.Windows.Forms.CheckBox();
            this.OneEighty = new System.Windows.Forms.Button();
            this.UTurnRight = new System.Windows.Forms.Button();
            this.UTurnLeft = new System.Windows.Forms.Button();
            this.Engine = new System.Windows.Forms.CheckBox();
            this.PushbackGroup = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.FlightPageView = new Telerik.WinControls.UI.RadPageViewPage();
            this.AbortTakeOff = new System.Windows.Forms.Button();
            this.ClearLand = new System.Windows.Forms.Button();
            this.ReturnBase = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.Orbit = new System.Windows.Forms.Button();
            this.ExtendDownwind = new System.Windows.Forms.Button();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.ChangeSpeed = new System.Windows.Forms.Button();
            this.SpeedBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.ChangeAltitude = new System.Windows.Forms.Button();
            this.AltitudeBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.RockingWing2 = new System.Windows.Forms.Button();
            this.Flypass = new System.Windows.Forms.Button();
            this.TouchGo = new System.Windows.Forms.Button();
            this.MissedApproach = new System.Windows.Forms.Button();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.RunwayList3 = new Telerik.WinControls.UI.RadDropDownList();
            this.InstrumentApp = new System.Windows.Forms.Button();
            this.JoinCircuit = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.RunwayList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.RunwayBtn = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.FlightRouteList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.FlightRulesList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.DirectGoList = new Telerik.WinControls.UI.RadDropDownList();
            this.DirectBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.AbsoluteChange = new System.Windows.Forms.Button();
            this.AbsoluteBox = new Telerik.WinControls.UI.RadSpinEditor();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.RelativeChange = new System.Windows.Forms.Button();
            this.RelativeBox = new Telerik.WinControls.UI.RadSpinEditor();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ContDir = new System.Windows.Forms.Button();
            this.ContRoute = new System.Windows.Forms.Button();
            this.Holding = new System.Windows.Forms.Button();
            this.HoldingRight = new System.Windows.Forms.Button();
            this.HoldList = new Telerik.WinControls.UI.RadDropDownList();
            this.label3 = new System.Windows.Forms.Label();
            this.HoldingLeft = new System.Windows.Forms.Button();
            this.OtherPageView = new Telerik.WinControls.UI.RadPageViewPage();
            this.IncidentCommand = new System.Windows.Forms.GroupBox();
            this.IncidentAnimal = new System.Windows.Forms.GroupBox();
            this.IncidentBird = new System.Windows.Forms.GroupBox();
            this.LightsGroup = new System.Windows.Forms.GroupBox();
            this.ApproachGroup = new System.Windows.Forms.GroupBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.PapiLight = new System.Windows.Forms.CheckBox();
            this.TaxiLight = new System.Windows.Forms.CheckBox();
            this.RunwayGroup = new System.Windows.Forms.GroupBox();
            this.EnvironmentPageView = new Telerik.WinControls.UI.RadPageViewPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.ChangeTime = new System.Windows.Forms.Button();
            this.ChangeVisibility = new System.Windows.Forms.Button();
            this.ChangeTemperatue = new System.Windows.Forms.Button();
            this.ChangeWindSpeed = new System.Windows.Forms.Button();
            this.ChangeWindDir = new System.Windows.Forms.Button();
            this.ChangeWeather = new System.Windows.Forms.Button();
            this.EnvTime = new Telerik.WinControls.UI.RadTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Visibility = new Telerik.WinControls.UI.RadSpinEditor();
            this.Temperature = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindSpeed = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindDirection = new Telerik.WinControls.UI.RadSpinEditor();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Weather = new Telerik.WinControls.UI.RadDropDownList();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.AircraftList = new Telerik.WinControls.UI.RadDropDownList();
            this.label1 = new System.Windows.Forms.Label();
            this.StartSimulation = new Telerik.WinControls.UI.RadButton();
            this.PauseSimulation = new Telerik.WinControls.UI.RadButton();
            this.AircraftCommandGroup = new Telerik.WinControls.UI.RadGroupBox();
            this.FuelValue = new Telerik.WinControls.UI.RadTimePicker();
            this.radSpinEditor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.FuelBtn = new Telerik.WinControls.UI.RadButton();
            this.EngineFailureValue = new Telerik.WinControls.UI.RadSpinEditor();
            this.EngineFailure = new Telerik.WinControls.UI.RadButton();
            this.RemoveAircraft = new Telerik.WinControls.UI.RadButton();
            this.SelectAircraft = new Telerik.WinControls.UI.RadButton();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.LogText = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCommandView)).BeginInit();
            this.AircraftCommandView.SuspendLayout();
            this.GroundPageView.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList7)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).BeginInit();
            this.TakeOffGroup.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IntersectionSpeedList)).BeginInit();
            this.FlightRouteGroup.SuspendLayout();
            this.Runway.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationList)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList)).BeginInit();
            this.TaxiGroup.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaxiSpeedList)).BeginInit();
            this.OtherCommandGroup.SuspendLayout();
            this.PushbackGroup.SuspendLayout();
            this.FlightPageView.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedBox)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AltitudeBox)).BeginInit();
            this.groupBox20.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList3)).BeginInit();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList2)).BeginInit();
            this.groupBox13.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList2)).BeginInit();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList2)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DirectGoList)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AbsoluteBox)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RelativeBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HoldList)).BeginInit();
            this.OtherPageView.SuspendLayout();
            this.IncidentCommand.SuspendLayout();
            this.LightsGroup.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.EnvironmentPageView.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartSimulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PauseSimulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCommandGroup)).BeginInit();
            this.AircraftCommandGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FuelValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuelBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineFailureValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineFailure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemoveAircraft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectAircraft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogText)).BeginInit();
            this.SuspendLayout();
            // 
            // AircraftCommandView
            // 
            this.AircraftCommandView.Controls.Add(this.GroundPageView);
            this.AircraftCommandView.Controls.Add(this.FlightPageView);
            this.AircraftCommandView.Controls.Add(this.OtherPageView);
            this.AircraftCommandView.Controls.Add(this.EnvironmentPageView);
            this.AircraftCommandView.Enabled = false;
            this.AircraftCommandView.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.AircraftCommandView.Location = new System.Drawing.Point(2, 51);
            this.AircraftCommandView.Name = "AircraftCommandView";
            this.AircraftCommandView.SelectedPage = this.GroundPageView;
            this.AircraftCommandView.Size = new System.Drawing.Size(576, 340);
            this.AircraftCommandView.TabIndex = 0;
            this.AircraftCommandView.Text = "Ground";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.AircraftCommandView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // GroundPageView
            // 
            this.GroundPageView.Controls.Add(this.ReadyAircraft);
            this.GroundPageView.Controls.Add(this.label2);
            this.GroundPageView.Controls.Add(this.checkBox1);
            this.GroundPageView.Controls.Add(this.groupBox3);
            this.GroundPageView.Controls.Add(this.TakeOffGroup);
            this.GroundPageView.Controls.Add(this.FlightRouteGroup);
            this.GroundPageView.Controls.Add(this.TaxiGroup);
            this.GroundPageView.Controls.Add(this.OtherCommandGroup);
            this.GroundPageView.Controls.Add(this.Engine);
            this.GroundPageView.Controls.Add(this.PushbackGroup);
            this.GroundPageView.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.GroundPageView.Location = new System.Drawing.Point(10, 40);
            this.GroundPageView.Name = "GroundPageView";
            this.GroundPageView.Size = new System.Drawing.Size(555, 289);
            this.GroundPageView.Text = "Ground Menu";
            // 
            // ReadyAircraft
            // 
            this.ReadyAircraft.BackColor = System.Drawing.Color.DodgerBlue;
            this.ReadyAircraft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ReadyAircraft.FlatAppearance.BorderSize = 0;
            this.ReadyAircraft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.ReadyAircraft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.ReadyAircraft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReadyAircraft.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ReadyAircraft.ForeColor = System.Drawing.Color.White;
            this.ReadyAircraft.Location = new System.Drawing.Point(3, 3);
            this.ReadyAircraft.Name = "ReadyAircraft";
            this.ReadyAircraft.Size = new System.Drawing.Size(90, 25);
            this.ReadyAircraft.TabIndex = 114;
            this.ReadyAircraft.Text = "Ready";
            this.ReadyAircraft.UseVisualStyleBackColor = false;
            this.ReadyAircraft.Click += new System.EventHandler(this.ReadyAircraft_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 19);
            this.label2.TabIndex = 113;
            this.label2.Text = "Parking";
            // 
            // checkBox1
            // 
            this.checkBox1.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox1.BackColor = System.Drawing.Color.Tomato;
            this.checkBox1.FlatAppearance.BorderSize = 0;
            this.checkBox1.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.checkBox1.ForeColor = System.Drawing.Color.White;
            this.checkBox1.Location = new System.Drawing.Point(187, 33);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 25);
            this.checkBox1.TabIndex = 112;
            this.checkBox1.Text = "IsParking";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.groupBox11);
            this.groupBox3.Controls.Add(this.groupBox10);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox3.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox3.Location = new System.Drawing.Point(272, -2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox3.Size = new System.Drawing.Size(280, 60);
            this.groupBox3.TabIndex = 111;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Appron Parking";
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.DodgerBlue;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(207, 23);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(66, 30);
            this.button8.TabIndex = 114;
            this.button8.Text = "Parking";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radDropDownList7);
            this.groupBox11.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox11.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox11.Location = new System.Drawing.Point(127, 13);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox11.Size = new System.Drawing.Size(74, 44);
            this.groupBox11.TabIndex = 113;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Bay";
            // 
            // radDropDownList7
            // 
            this.radDropDownList7.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList7.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radDropDownList7.Location = new System.Drawing.Point(6, 19);
            this.radDropDownList7.Name = "radDropDownList7";
            this.radDropDownList7.NullText = "Choose Runway";
            this.radDropDownList7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radDropDownList7.Size = new System.Drawing.Size(62, 21);
            this.radDropDownList7.TabIndex = 111;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.radDropDownList3);
            this.groupBox10.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox10.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox10.Location = new System.Drawing.Point(6, 14);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox10.Size = new System.Drawing.Size(120, 43);
            this.groupBox10.TabIndex = 112;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Appron";
            // 
            // radDropDownList3
            // 
            this.radDropDownList3.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radDropDownList3.Location = new System.Drawing.Point(3, 18);
            this.radDropDownList3.Name = "radDropDownList3";
            this.radDropDownList3.NullText = "Choose Runway";
            this.radDropDownList3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radDropDownList3.Size = new System.Drawing.Size(114, 21);
            this.radDropDownList3.TabIndex = 111;
            // 
            // TakeOffGroup
            // 
            this.TakeOffGroup.Controls.Add(this.groupBox5);
            this.TakeOffGroup.Controls.Add(this.TakeOff);
            this.TakeOffGroup.Controls.Add(this.RunwayHold);
            this.TakeOffGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.TakeOffGroup.ForeColor = System.Drawing.Color.Tomato;
            this.TakeOffGroup.Location = new System.Drawing.Point(0, 188);
            this.TakeOffGroup.Name = "TakeOffGroup";
            this.TakeOffGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TakeOffGroup.Size = new System.Drawing.Size(204, 96);
            this.TakeOffGroup.TabIndex = 110;
            this.TakeOffGroup.TabStop = false;
            this.TakeOffGroup.Text = "Intersection";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ChangeSpeedIntersection);
            this.groupBox5.Controls.Add(this.IntersectionSpeedList);
            this.groupBox5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox5.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox5.Location = new System.Drawing.Point(6, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox5.Size = new System.Drawing.Size(94, 78);
            this.groupBox5.TabIndex = 108;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Speed";
            // 
            // ChangeSpeedIntersection
            // 
            this.ChangeSpeedIntersection.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeSpeedIntersection.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ChangeSpeedIntersection.FlatAppearance.BorderSize = 0;
            this.ChangeSpeedIntersection.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.ChangeSpeedIntersection.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.ChangeSpeedIntersection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeSpeedIntersection.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ChangeSpeedIntersection.ForeColor = System.Drawing.Color.White;
            this.ChangeSpeedIntersection.Location = new System.Drawing.Point(4, 46);
            this.ChangeSpeedIntersection.Name = "ChangeSpeedIntersection";
            this.ChangeSpeedIntersection.Size = new System.Drawing.Size(85, 25);
            this.ChangeSpeedIntersection.TabIndex = 107;
            this.ChangeSpeedIntersection.Text = "Change";
            this.ChangeSpeedIntersection.UseVisualStyleBackColor = false;
            this.ChangeSpeedIntersection.Click += new System.EventHandler(this.ChangeSpeedIntersection_Click);
            // 
            // IntersectionSpeedList
            // 
            this.IntersectionSpeedList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IntersectionSpeedList.Font = new System.Drawing.Font("Segoe UI", 9F);
            radListDataItem1.Text = "Slow";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Medium";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Expedite";
            radListDataItem3.TextWrap = true;
            this.IntersectionSpeedList.Items.Add(radListDataItem1);
            this.IntersectionSpeedList.Items.Add(radListDataItem2);
            this.IntersectionSpeedList.Items.Add(radListDataItem3);
            this.IntersectionSpeedList.Location = new System.Drawing.Point(4, 19);
            this.IntersectionSpeedList.Name = "IntersectionSpeedList";
            this.IntersectionSpeedList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.IntersectionSpeedList.Size = new System.Drawing.Size(85, 21);
            this.IntersectionSpeedList.TabIndex = 106;
            // 
            // TakeOff
            // 
            this.TakeOff.BackColor = System.Drawing.Color.DodgerBlue;
            this.TakeOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TakeOff.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.TakeOff.ForeColor = System.Drawing.Color.White;
            this.TakeOff.Location = new System.Drawing.Point(103, 27);
            this.TakeOff.Name = "TakeOff";
            this.TakeOff.Size = new System.Drawing.Size(95, 25);
            this.TakeOff.TabIndex = 1;
            this.TakeOff.Text = "Take Off";
            this.TakeOff.UseVisualStyleBackColor = false;
            this.TakeOff.Click += new System.EventHandler(this.TakeOff_Click);
            // 
            // RunwayHold
            // 
            this.RunwayHold.BackColor = System.Drawing.Color.DodgerBlue;
            this.RunwayHold.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RunwayHold.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.RunwayHold.ForeColor = System.Drawing.Color.White;
            this.RunwayHold.Location = new System.Drawing.Point(102, 58);
            this.RunwayHold.Name = "RunwayHold";
            this.RunwayHold.Size = new System.Drawing.Size(96, 25);
            this.RunwayHold.TabIndex = 0;
            this.RunwayHold.Text = "Runway Hold";
            this.RunwayHold.UseVisualStyleBackColor = false;
            this.RunwayHold.Click += new System.EventHandler(this.RunwayHold_Click);
            // 
            // FlightRouteGroup
            // 
            this.FlightRouteGroup.Controls.Add(this.Runway);
            this.FlightRouteGroup.Controls.Add(this.groupBox8);
            this.FlightRouteGroup.Controls.Add(this.groupBox7);
            this.FlightRouteGroup.Controls.Add(this.groupBox6);
            this.FlightRouteGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.FlightRouteGroup.ForeColor = System.Drawing.Color.Tomato;
            this.FlightRouteGroup.Location = new System.Drawing.Point(207, 175);
            this.FlightRouteGroup.Name = "FlightRouteGroup";
            this.FlightRouteGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FlightRouteGroup.Size = new System.Drawing.Size(345, 115);
            this.FlightRouteGroup.TabIndex = 109;
            this.FlightRouteGroup.TabStop = false;
            this.FlightRouteGroup.Text = "Flight Route and Rules";
            // 
            // Runway
            // 
            this.Runway.Controls.Add(this.RunwayList);
            this.Runway.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Runway.ForeColor = System.Drawing.Color.Tomato;
            this.Runway.Location = new System.Drawing.Point(262, 63);
            this.Runway.Name = "Runway";
            this.Runway.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Runway.Size = new System.Drawing.Size(76, 46);
            this.Runway.TabIndex = 115;
            this.Runway.TabStop = false;
            this.Runway.Text = "Runway";
            // 
            // RunwayList
            // 
            this.RunwayList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RunwayList.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.RunwayList.Location = new System.Drawing.Point(4, 18);
            this.RunwayList.Name = "RunwayList";
            this.RunwayList.NullText = "Choose Runway";
            this.RunwayList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RunwayList.Size = new System.Drawing.Size(64, 21);
            this.RunwayList.TabIndex = 41;
            this.RunwayList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RunwayList_SelectedIndexChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.FlightRouteList);
            this.groupBox8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox8.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox8.Location = new System.Drawing.Point(6, 63);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox8.Size = new System.Drawing.Size(250, 46);
            this.groupBox8.TabIndex = 111;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Flight Route";
            // 
            // FlightRouteList
            // 
            this.FlightRouteList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRouteList.Font = new System.Drawing.Font("Segoe UI", 9F);
            radListDataItem4.Text = "Slow";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "Medium";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "Expedite";
            radListDataItem6.TextWrap = true;
            this.FlightRouteList.Items.Add(radListDataItem4);
            this.FlightRouteList.Items.Add(radListDataItem5);
            this.FlightRouteList.Items.Add(radListDataItem6);
            this.FlightRouteList.Location = new System.Drawing.Point(6, 18);
            this.FlightRouteList.Name = "FlightRouteList";
            this.FlightRouteList.NullText = "Choose Runway";
            this.FlightRouteList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FlightRouteList.Size = new System.Drawing.Size(238, 21);
            this.FlightRouteList.TabIndex = 111;
            this.FlightRouteList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.FlightRouteList_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.DestinationList);
            this.groupBox7.Font = new System.Drawing.Font("Corbel", 8.25F);
            this.groupBox7.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox7.Location = new System.Drawing.Point(93, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox7.Size = new System.Drawing.Size(245, 40);
            this.groupBox7.TabIndex = 110;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Destination";
            // 
            // DestinationList
            // 
            this.DestinationList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DestinationList.Font = new System.Drawing.Font("Segoe UI", 9F);
            radListDataItem7.Text = "Slow";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "Medium";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "Expedite";
            radListDataItem9.TextWrap = true;
            this.DestinationList.Items.Add(radListDataItem7);
            this.DestinationList.Items.Add(radListDataItem8);
            this.DestinationList.Items.Add(radListDataItem9);
            this.DestinationList.Location = new System.Drawing.Point(6, 15);
            this.DestinationList.Name = "DestinationList";
            this.DestinationList.NullText = "Choose Runway";
            this.DestinationList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DestinationList.Size = new System.Drawing.Size(231, 21);
            this.DestinationList.TabIndex = 111;
            this.DestinationList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.DestinationList_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.FlightRulesList);
            this.groupBox6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox6.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox6.Location = new System.Drawing.Point(8, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox6.Size = new System.Drawing.Size(79, 40);
            this.groupBox6.TabIndex = 109;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Flight Rule";
            // 
            // FlightRulesList
            // 
            this.FlightRulesList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRulesList.Font = new System.Drawing.Font("Segoe UI", 9F);
            radListDataItem10.Text = "IFR";
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "VFR";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "Local";
            radListDataItem12.TextWrap = true;
            this.FlightRulesList.Items.Add(radListDataItem10);
            this.FlightRulesList.Items.Add(radListDataItem11);
            this.FlightRulesList.Items.Add(radListDataItem12);
            this.FlightRulesList.Location = new System.Drawing.Point(6, 16);
            this.FlightRulesList.Name = "FlightRulesList";
            this.FlightRulesList.NullText = "Choose Runway";
            this.FlightRulesList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FlightRulesList.Size = new System.Drawing.Size(67, 21);
            this.FlightRulesList.TabIndex = 111;
            this.FlightRulesList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.FlightRulesList_SelectedIndexChanged);
            // 
            // TaxiGroup
            // 
            this.TaxiGroup.Controls.Add(this.button1);
            this.TaxiGroup.Controls.Add(this.groupBox4);
            this.TaxiGroup.Controls.Add(this.button2);
            this.TaxiGroup.Controls.Add(this.StartTaxi);
            this.TaxiGroup.Controls.Add(this.CrossRW);
            this.TaxiGroup.Controls.Add(this.TaxiRoute);
            this.TaxiGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.TaxiGroup.ForeColor = System.Drawing.Color.Tomato;
            this.TaxiGroup.Location = new System.Drawing.Point(209, 60);
            this.TaxiGroup.Name = "TaxiGroup";
            this.TaxiGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TaxiGroup.Size = new System.Drawing.Size(343, 114);
            this.TaxiGroup.TabIndex = 108;
            this.TaxiGroup.TabStop = false;
            this.TaxiGroup.Text = "Taxi";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(103, 83);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 25);
            this.button1.TabIndex = 109;
            this.button1.Text = "Continue Taxi";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ChangeSpeedTaxi);
            this.groupBox4.Controls.Add(this.TaxiSpeedList);
            this.groupBox4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox4.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox4.Location = new System.Drawing.Point(218, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox4.Size = new System.Drawing.Size(116, 89);
            this.groupBox4.TabIndex = 107;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Speed";
            // 
            // ChangeSpeedTaxi
            // 
            this.ChangeSpeedTaxi.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeSpeedTaxi.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ChangeSpeedTaxi.FlatAppearance.BorderSize = 0;
            this.ChangeSpeedTaxi.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.ChangeSpeedTaxi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.ChangeSpeedTaxi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeSpeedTaxi.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ChangeSpeedTaxi.ForeColor = System.Drawing.Color.White;
            this.ChangeSpeedTaxi.Location = new System.Drawing.Point(5, 52);
            this.ChangeSpeedTaxi.Name = "ChangeSpeedTaxi";
            this.ChangeSpeedTaxi.Size = new System.Drawing.Size(105, 30);
            this.ChangeSpeedTaxi.TabIndex = 107;
            this.ChangeSpeedTaxi.Text = "Change";
            this.ChangeSpeedTaxi.UseVisualStyleBackColor = false;
            this.ChangeSpeedTaxi.Click += new System.EventHandler(this.ChangeSpeedTaxi_Click);
            // 
            // TaxiSpeedList
            // 
            this.TaxiSpeedList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TaxiSpeedList.Font = new System.Drawing.Font("Segoe UI", 9F);
            radListDataItem13.Text = "Slow";
            radListDataItem13.TextWrap = true;
            radListDataItem14.Text = "Medium";
            radListDataItem14.TextWrap = true;
            radListDataItem15.Text = "Expedite";
            radListDataItem15.TextWrap = true;
            this.TaxiSpeedList.Items.Add(radListDataItem13);
            this.TaxiSpeedList.Items.Add(radListDataItem14);
            this.TaxiSpeedList.Items.Add(radListDataItem15);
            this.TaxiSpeedList.Location = new System.Drawing.Point(5, 24);
            this.TaxiSpeedList.Name = "TaxiSpeedList";
            this.TaxiSpeedList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TaxiSpeedList.Size = new System.Drawing.Size(105, 21);
            this.TaxiSpeedList.TabIndex = 106;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DodgerBlue;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(7, 83);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 25);
            this.button2.TabIndex = 108;
            this.button2.Text = "Hold Taxi";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // StartTaxi
            // 
            this.StartTaxi.BackColor = System.Drawing.Color.DodgerBlue;
            this.StartTaxi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.StartTaxi.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.StartTaxi.ForeColor = System.Drawing.Color.White;
            this.StartTaxi.Location = new System.Drawing.Point(6, 53);
            this.StartTaxi.Name = "StartTaxi";
            this.StartTaxi.Size = new System.Drawing.Size(186, 25);
            this.StartTaxi.TabIndex = 0;
            this.StartTaxi.Text = "Start Taxi";
            this.StartTaxi.UseVisualStyleBackColor = false;
            this.StartTaxi.Click += new System.EventHandler(this.StartTaxi_Click);
            // 
            // CrossRW
            // 
            this.CrossRW.BackColor = System.Drawing.Color.DodgerBlue;
            this.CrossRW.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.CrossRW.FlatAppearance.BorderSize = 0;
            this.CrossRW.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.CrossRW.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.CrossRW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CrossRW.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.CrossRW.ForeColor = System.Drawing.Color.White;
            this.CrossRW.Location = new System.Drawing.Point(102, 22);
            this.CrossRW.Name = "CrossRW";
            this.CrossRW.Size = new System.Drawing.Size(90, 25);
            this.CrossRW.TabIndex = 6;
            this.CrossRW.Text = "Cross Runway";
            this.CrossRW.UseVisualStyleBackColor = false;
            this.CrossRW.Click += new System.EventHandler(this.CrossRW_Click);
            // 
            // TaxiRoute
            // 
            this.TaxiRoute.BackColor = System.Drawing.Color.DodgerBlue;
            this.TaxiRoute.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.TaxiRoute.FlatAppearance.BorderSize = 0;
            this.TaxiRoute.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.TaxiRoute.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.TaxiRoute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaxiRoute.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.TaxiRoute.ForeColor = System.Drawing.Color.White;
            this.TaxiRoute.Location = new System.Drawing.Point(6, 22);
            this.TaxiRoute.Name = "TaxiRoute";
            this.TaxiRoute.Size = new System.Drawing.Size(90, 25);
            this.TaxiRoute.TabIndex = 1;
            this.TaxiRoute.Text = "Taxi Route";
            this.TaxiRoute.UseVisualStyleBackColor = false;
            this.TaxiRoute.Click += new System.EventHandler(this.TaxiRoute_Click);
            // 
            // OtherCommandGroup
            // 
            this.OtherCommandGroup.Controls.Add(this.RockingWing);
            this.OtherCommandGroup.Controls.Add(this.OneEighty);
            this.OtherCommandGroup.Controls.Add(this.UTurnRight);
            this.OtherCommandGroup.Controls.Add(this.UTurnLeft);
            this.OtherCommandGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.OtherCommandGroup.ForeColor = System.Drawing.Color.Tomato;
            this.OtherCommandGroup.Location = new System.Drawing.Point(3, 93);
            this.OtherCommandGroup.Name = "OtherCommandGroup";
            this.OtherCommandGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.OtherCommandGroup.Size = new System.Drawing.Size(200, 87);
            this.OtherCommandGroup.TabIndex = 107;
            this.OtherCommandGroup.TabStop = false;
            this.OtherCommandGroup.Text = "Other Command";
            // 
            // RockingWing
            // 
            this.RockingWing.Appearance = System.Windows.Forms.Appearance.Button;
            this.RockingWing.BackColor = System.Drawing.Color.Tomato;
            this.RockingWing.FlatAppearance.BorderSize = 0;
            this.RockingWing.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.RockingWing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RockingWing.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.RockingWing.ForeColor = System.Drawing.Color.White;
            this.RockingWing.Location = new System.Drawing.Point(103, 53);
            this.RockingWing.Name = "RockingWing";
            this.RockingWing.Size = new System.Drawing.Size(90, 25);
            this.RockingWing.TabIndex = 106;
            this.RockingWing.Text = "Rocking Wing";
            this.RockingWing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RockingWing.UseVisualStyleBackColor = false;
            this.RockingWing.CheckedChanged += new System.EventHandler(this.RockingWing_CheckedChanged);
            // 
            // OneEighty
            // 
            this.OneEighty.BackColor = System.Drawing.Color.DodgerBlue;
            this.OneEighty.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OneEighty.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.OneEighty.ForeColor = System.Drawing.Color.White;
            this.OneEighty.Location = new System.Drawing.Point(7, 53);
            this.OneEighty.Name = "OneEighty";
            this.OneEighty.Size = new System.Drawing.Size(90, 25);
            this.OneEighty.TabIndex = 18;
            this.OneEighty.Text = "Turn 180";
            this.OneEighty.UseVisualStyleBackColor = false;
            this.OneEighty.Click += new System.EventHandler(this.OneEighty_Click);
            // 
            // UTurnRight
            // 
            this.UTurnRight.BackColor = System.Drawing.Color.DodgerBlue;
            this.UTurnRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UTurnRight.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.UTurnRight.ForeColor = System.Drawing.Color.White;
            this.UTurnRight.Location = new System.Drawing.Point(102, 22);
            this.UTurnRight.Name = "UTurnRight";
            this.UTurnRight.Size = new System.Drawing.Size(90, 25);
            this.UTurnRight.TabIndex = 17;
            this.UTurnRight.Text = "U-Turn Right";
            this.UTurnRight.UseVisualStyleBackColor = false;
            this.UTurnRight.Click += new System.EventHandler(this.UTurnRight_Click);
            // 
            // UTurnLeft
            // 
            this.UTurnLeft.BackColor = System.Drawing.Color.DodgerBlue;
            this.UTurnLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UTurnLeft.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.UTurnLeft.ForeColor = System.Drawing.Color.White;
            this.UTurnLeft.Location = new System.Drawing.Point(6, 22);
            this.UTurnLeft.Name = "UTurnLeft";
            this.UTurnLeft.Size = new System.Drawing.Size(90, 25);
            this.UTurnLeft.TabIndex = 0;
            this.UTurnLeft.Text = "U-Turn Left";
            this.UTurnLeft.UseVisualStyleBackColor = false;
            this.UTurnLeft.Click += new System.EventHandler(this.UTurnLeft_Click);
            // 
            // Engine
            // 
            this.Engine.Appearance = System.Windows.Forms.Appearance.Button;
            this.Engine.BackColor = System.Drawing.Color.Tomato;
            this.Engine.FlatAppearance.BorderSize = 0;
            this.Engine.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.Engine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Engine.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Engine.ForeColor = System.Drawing.Color.White;
            this.Engine.Location = new System.Drawing.Point(95, 3);
            this.Engine.Name = "Engine";
            this.Engine.Size = new System.Drawing.Size(80, 25);
            this.Engine.TabIndex = 105;
            this.Engine.Text = "Engine";
            this.Engine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Engine.UseVisualStyleBackColor = false;
            this.Engine.Click += new System.EventHandler(this.Engine_Click);
            // 
            // PushbackGroup
            // 
            this.PushbackGroup.Controls.Add(this.button3);
            this.PushbackGroup.Controls.Add(this.button4);
            this.PushbackGroup.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.PushbackGroup.ForeColor = System.Drawing.Color.Tomato;
            this.PushbackGroup.Location = new System.Drawing.Point(3, 34);
            this.PushbackGroup.Name = "PushbackGroup";
            this.PushbackGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PushbackGroup.Size = new System.Drawing.Size(178, 53);
            this.PushbackGroup.TabIndex = 104;
            this.PushbackGroup.TabStop = false;
            this.PushbackGroup.Text = "Pushback";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DodgerBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(92, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 25);
            this.button3.TabIndex = 19;
            this.button3.Text = "Right";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DodgerBlue;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(6, 19);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 25);
            this.button4.TabIndex = 18;
            this.button4.Text = "Left";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // FlightPageView
            // 
            this.FlightPageView.Controls.Add(this.AbortTakeOff);
            this.FlightPageView.Controls.Add(this.ClearLand);
            this.FlightPageView.Controls.Add(this.ReturnBase);
            this.FlightPageView.Controls.Add(this.groupBox14);
            this.FlightPageView.Controls.Add(this.groupBox21);
            this.FlightPageView.Controls.Add(this.groupBox22);
            this.FlightPageView.Controls.Add(this.groupBox20);
            this.FlightPageView.Controls.Add(this.groupBox19);
            this.FlightPageView.Controls.Add(this.groupBox18);
            this.FlightPageView.Controls.Add(this.groupBox13);
            this.FlightPageView.Controls.Add(this.groupBox12);
            this.FlightPageView.Controls.Add(this.groupBox2);
            this.FlightPageView.Controls.Add(this.groupBox9);
            this.FlightPageView.Controls.Add(this.groupBox1);
            this.FlightPageView.Location = new System.Drawing.Point(10, 40);
            this.FlightPageView.Name = "FlightPageView";
            this.FlightPageView.Size = new System.Drawing.Size(555, 289);
            this.FlightPageView.Text = "Flight Menu";
            // 
            // AbortTakeOff
            // 
            this.AbortTakeOff.BackColor = System.Drawing.Color.DodgerBlue;
            this.AbortTakeOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AbortTakeOff.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.AbortTakeOff.ForeColor = System.Drawing.Color.White;
            this.AbortTakeOff.Location = new System.Drawing.Point(391, 256);
            this.AbortTakeOff.Name = "AbortTakeOff";
            this.AbortTakeOff.Size = new System.Drawing.Size(159, 28);
            this.AbortTakeOff.TabIndex = 164;
            this.AbortTakeOff.Text = "Aborted Take Off";
            this.AbortTakeOff.UseVisualStyleBackColor = false;
            // 
            // ClearLand
            // 
            this.ClearLand.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClearLand.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ClearLand.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.ClearLand.ForeColor = System.Drawing.Color.White;
            this.ClearLand.Location = new System.Drawing.Point(391, 226);
            this.ClearLand.Name = "ClearLand";
            this.ClearLand.Size = new System.Drawing.Size(76, 28);
            this.ClearLand.TabIndex = 163;
            this.ClearLand.Text = "Clear Land";
            this.ClearLand.UseVisualStyleBackColor = false;
            // 
            // ReturnBase
            // 
            this.ReturnBase.BackColor = System.Drawing.Color.DodgerBlue;
            this.ReturnBase.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ReturnBase.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.ReturnBase.ForeColor = System.Drawing.Color.White;
            this.ReturnBase.Location = new System.Drawing.Point(471, 226);
            this.ReturnBase.Name = "ReturnBase";
            this.ReturnBase.Size = new System.Drawing.Size(79, 28);
            this.ReturnBase.TabIndex = 162;
            this.ReturnBase.Text = "Return Base";
            this.ReturnBase.UseVisualStyleBackColor = false;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.Orbit);
            this.groupBox14.Controls.Add(this.ExtendDownwind);
            this.groupBox14.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox14.ForeColor = System.Drawing.Color.White;
            this.groupBox14.Location = new System.Drawing.Point(258, 212);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox14.Size = new System.Drawing.Size(127, 77);
            this.groupBox14.TabIndex = 161;
            this.groupBox14.TabStop = false;
            // 
            // Orbit
            // 
            this.Orbit.BackColor = System.Drawing.Color.DodgerBlue;
            this.Orbit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Orbit.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.Orbit.ForeColor = System.Drawing.Color.White;
            this.Orbit.Location = new System.Drawing.Point(9, 44);
            this.Orbit.Name = "Orbit";
            this.Orbit.Size = new System.Drawing.Size(108, 28);
            this.Orbit.TabIndex = 108;
            this.Orbit.Text = "Orbit";
            this.Orbit.UseVisualStyleBackColor = false;
            // 
            // ExtendDownwind
            // 
            this.ExtendDownwind.BackColor = System.Drawing.Color.DodgerBlue;
            this.ExtendDownwind.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ExtendDownwind.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.ExtendDownwind.ForeColor = System.Drawing.Color.White;
            this.ExtendDownwind.Location = new System.Drawing.Point(9, 14);
            this.ExtendDownwind.Name = "ExtendDownwind";
            this.ExtendDownwind.Size = new System.Drawing.Size(108, 28);
            this.ExtendDownwind.TabIndex = 7;
            this.ExtendDownwind.Text = "Extend Downind";
            this.ExtendDownwind.UseVisualStyleBackColor = false;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.ChangeSpeed);
            this.groupBox21.Controls.Add(this.SpeedBox);
            this.groupBox21.Controls.Add(this.label4);
            this.groupBox21.Controls.Add(this.label9);
            this.groupBox21.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox21.ForeColor = System.Drawing.Color.White;
            this.groupBox21.Location = new System.Drawing.Point(121, 160);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox21.Size = new System.Drawing.Size(133, 69);
            this.groupBox21.TabIndex = 160;
            this.groupBox21.TabStop = false;
            // 
            // ChangeSpeed
            // 
            this.ChangeSpeed.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeSpeed.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeSpeed.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.ChangeSpeed.ForeColor = System.Drawing.Color.White;
            this.ChangeSpeed.Location = new System.Drawing.Point(5, 37);
            this.ChangeSpeed.Name = "ChangeSpeed";
            this.ChangeSpeed.Size = new System.Drawing.Size(119, 28);
            this.ChangeSpeed.TabIndex = 152;
            this.ChangeSpeed.Text = "Change Speed";
            this.ChangeSpeed.UseVisualStyleBackColor = false;
            // 
            // SpeedBox
            // 
            this.SpeedBox.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.SpeedBox.Location = new System.Drawing.Point(51, 12);
            this.SpeedBox.Mask = "d";
            this.SpeedBox.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.SpeedBox.Name = "SpeedBox";
            this.SpeedBox.Size = new System.Drawing.Size(31, 19);
            this.SpeedBox.TabIndex = 143;
            this.SpeedBox.TabStop = false;
            this.SpeedBox.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label4.ForeColor = System.Drawing.Color.Tomato;
            this.label4.Location = new System.Drawing.Point(88, 14);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(37, 16);
            this.label4.TabIndex = 144;
            this.label4.Text = "Knots";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label9.ForeColor = System.Drawing.Color.Tomato;
            this.label9.Location = new System.Drawing.Point(6, 14);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(44, 16);
            this.label9.TabIndex = 103;
            this.label9.Text = "Speed";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.ChangeAltitude);
            this.groupBox22.Controls.Add(this.AltitudeBox);
            this.groupBox22.Controls.Add(this.label5);
            this.groupBox22.Controls.Add(this.label23);
            this.groupBox22.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox22.ForeColor = System.Drawing.Color.White;
            this.groupBox22.Location = new System.Drawing.Point(121, 221);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox22.Size = new System.Drawing.Size(131, 68);
            this.groupBox22.TabIndex = 159;
            this.groupBox22.TabStop = false;
            // 
            // ChangeAltitude
            // 
            this.ChangeAltitude.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeAltitude.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeAltitude.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.ChangeAltitude.ForeColor = System.Drawing.Color.White;
            this.ChangeAltitude.Location = new System.Drawing.Point(6, 34);
            this.ChangeAltitude.Name = "ChangeAltitude";
            this.ChangeAltitude.Size = new System.Drawing.Size(119, 28);
            this.ChangeAltitude.TabIndex = 152;
            this.ChangeAltitude.Text = "Change Altitude";
            this.ChangeAltitude.UseVisualStyleBackColor = false;
            // 
            // AltitudeBox
            // 
            this.AltitudeBox.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.AltitudeBox.Location = new System.Drawing.Point(51, 12);
            this.AltitudeBox.Mask = "d4";
            this.AltitudeBox.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.AltitudeBox.Name = "AltitudeBox";
            this.AltitudeBox.Size = new System.Drawing.Size(29, 19);
            this.AltitudeBox.TabIndex = 143;
            this.AltitudeBox.TabStop = false;
            this.AltitudeBox.Text = "0000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label5.ForeColor = System.Drawing.Color.Tomato;
            this.label5.Location = new System.Drawing.Point(80, 15);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(42, 16);
            this.label5.TabIndex = 144;
            this.label5.Text = "x100 ft";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label23.ForeColor = System.Drawing.Color.Tomato;
            this.label23.Location = new System.Drawing.Point(6, 15);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label23.Size = new System.Drawing.Size(49, 16);
            this.label23.TabIndex = 103;
            this.label23.Text = "Altitude";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.RockingWing2);
            this.groupBox20.Controls.Add(this.Flypass);
            this.groupBox20.Controls.Add(this.TouchGo);
            this.groupBox20.Controls.Add(this.MissedApproach);
            this.groupBox20.Font = new System.Drawing.Font("Century Gothic", 1F);
            this.groupBox20.ForeColor = System.Drawing.Color.White;
            this.groupBox20.Location = new System.Drawing.Point(274, 132);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox20.Size = new System.Drawing.Size(277, 79);
            this.groupBox20.TabIndex = 158;
            this.groupBox20.TabStop = false;
            // 
            // RockingWing2
            // 
            this.RockingWing2.BackColor = System.Drawing.Color.DodgerBlue;
            this.RockingWing2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RockingWing2.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.RockingWing2.ForeColor = System.Drawing.Color.White;
            this.RockingWing2.Location = new System.Drawing.Point(133, 48);
            this.RockingWing2.Name = "RockingWing2";
            this.RockingWing2.Size = new System.Drawing.Size(135, 28);
            this.RockingWing2.TabIndex = 110;
            this.RockingWing2.Text = "Rocking Wing";
            this.RockingWing2.UseVisualStyleBackColor = false;
            // 
            // Flypass
            // 
            this.Flypass.BackColor = System.Drawing.Color.DodgerBlue;
            this.Flypass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Flypass.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.Flypass.ForeColor = System.Drawing.Color.White;
            this.Flypass.Location = new System.Drawing.Point(6, 48);
            this.Flypass.Name = "Flypass";
            this.Flypass.Size = new System.Drawing.Size(121, 28);
            this.Flypass.TabIndex = 109;
            this.Flypass.Text = "Flypass / Go Round";
            this.Flypass.UseVisualStyleBackColor = false;
            // 
            // TouchGo
            // 
            this.TouchGo.BackColor = System.Drawing.Color.DodgerBlue;
            this.TouchGo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TouchGo.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.TouchGo.ForeColor = System.Drawing.Color.White;
            this.TouchGo.Location = new System.Drawing.Point(133, 14);
            this.TouchGo.Name = "TouchGo";
            this.TouchGo.Size = new System.Drawing.Size(135, 28);
            this.TouchGo.TabIndex = 108;
            this.TouchGo.Text = "Touch and Go";
            this.TouchGo.UseVisualStyleBackColor = false;
            // 
            // MissedApproach
            // 
            this.MissedApproach.BackColor = System.Drawing.Color.DodgerBlue;
            this.MissedApproach.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.MissedApproach.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.MissedApproach.ForeColor = System.Drawing.Color.White;
            this.MissedApproach.Location = new System.Drawing.Point(6, 14);
            this.MissedApproach.Name = "MissedApproach";
            this.MissedApproach.Size = new System.Drawing.Size(121, 28);
            this.MissedApproach.TabIndex = 7;
            this.MissedApproach.Text = "Missed Approach";
            this.MissedApproach.UseVisualStyleBackColor = false;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label8);
            this.groupBox19.Controls.Add(this.RunwayList3);
            this.groupBox19.Controls.Add(this.InstrumentApp);
            this.groupBox19.Controls.Add(this.JoinCircuit);
            this.groupBox19.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.groupBox19.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox19.Location = new System.Drawing.Point(0, 164);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox19.Size = new System.Drawing.Size(120, 124);
            this.groupBox19.TabIndex = 157;
            this.groupBox19.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label8.ForeColor = System.Drawing.Color.Tomato;
            this.label8.Location = new System.Drawing.Point(7, 20);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(53, 16);
            this.label8.TabIndex = 110;
            this.label8.Text = "Runway";
            // 
            // RunwayList3
            // 
            this.RunwayList3.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RunwayList3.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.RunwayList3.Location = new System.Drawing.Point(59, 20);
            this.RunwayList3.Name = "RunwayList3";
            this.RunwayList3.NullText = "Choose Runway";
            this.RunwayList3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RunwayList3.Size = new System.Drawing.Size(53, 22);
            this.RunwayList3.TabIndex = 109;
            // 
            // InstrumentApp
            // 
            this.InstrumentApp.BackColor = System.Drawing.Color.DodgerBlue;
            this.InstrumentApp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.InstrumentApp.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.InstrumentApp.ForeColor = System.Drawing.Color.White;
            this.InstrumentApp.Location = new System.Drawing.Point(11, 48);
            this.InstrumentApp.Name = "InstrumentApp";
            this.InstrumentApp.Size = new System.Drawing.Size(101, 28);
            this.InstrumentApp.TabIndex = 108;
            this.InstrumentApp.Text = "Instrument App";
            this.InstrumentApp.UseVisualStyleBackColor = false;
            // 
            // JoinCircuit
            // 
            this.JoinCircuit.BackColor = System.Drawing.Color.DodgerBlue;
            this.JoinCircuit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.JoinCircuit.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.JoinCircuit.ForeColor = System.Drawing.Color.White;
            this.JoinCircuit.Location = new System.Drawing.Point(11, 82);
            this.JoinCircuit.Name = "JoinCircuit";
            this.JoinCircuit.Size = new System.Drawing.Size(101, 28);
            this.JoinCircuit.TabIndex = 7;
            this.JoinCircuit.Text = "Join Circuit";
            this.JoinCircuit.UseVisualStyleBackColor = false;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.RunwayList2);
            this.groupBox18.Controls.Add(this.RunwayBtn);
            this.groupBox18.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.groupBox18.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox18.Location = new System.Drawing.Point(130, 115);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(130, 49);
            this.groupBox18.TabIndex = 156;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Change Runway";
            // 
            // RunwayList2
            // 
            this.RunwayList2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RunwayList2.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.RunwayList2.Location = new System.Drawing.Point(11, 16);
            this.RunwayList2.Name = "RunwayList2";
            this.RunwayList2.NullText = "Choose Runway";
            this.RunwayList2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RunwayList2.Size = new System.Drawing.Size(69, 22);
            this.RunwayList2.TabIndex = 107;
            // 
            // RunwayBtn
            // 
            this.RunwayBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.RunwayBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.RunwayBtn.FlatAppearance.BorderSize = 0;
            this.RunwayBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.RunwayBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.RunwayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RunwayBtn.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.RunwayBtn.ForeColor = System.Drawing.Color.White;
            this.RunwayBtn.Location = new System.Drawing.Point(86, 13);
            this.RunwayBtn.Name = "RunwayBtn";
            this.RunwayBtn.Size = new System.Drawing.Size(38, 28);
            this.RunwayBtn.TabIndex = 108;
            this.RunwayBtn.Text = "Ok";
            this.RunwayBtn.UseVisualStyleBackColor = false;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.groupBox15);
            this.groupBox13.Controls.Add(this.groupBox17);
            this.groupBox13.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox13.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox13.Location = new System.Drawing.Point(274, 71);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox13.Size = new System.Drawing.Size(276, 59);
            this.groupBox13.TabIndex = 155;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Flight Route and Rules";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.FlightRouteList2);
            this.groupBox15.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox15.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox15.Location = new System.Drawing.Point(84, 15);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox15.Size = new System.Drawing.Size(184, 41);
            this.groupBox15.TabIndex = 111;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Flight Route";
            // 
            // FlightRouteList2
            // 
            this.FlightRouteList2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRouteList2.Font = new System.Drawing.Font("Segoe UI", 8F);
            radListDataItem16.Text = "Slow";
            radListDataItem16.TextWrap = true;
            radListDataItem17.Text = "Medium";
            radListDataItem17.TextWrap = true;
            radListDataItem18.Text = "Expedite";
            radListDataItem18.TextWrap = true;
            this.FlightRouteList2.Items.Add(radListDataItem16);
            this.FlightRouteList2.Items.Add(radListDataItem17);
            this.FlightRouteList2.Items.Add(radListDataItem18);
            this.FlightRouteList2.Location = new System.Drawing.Point(6, 15);
            this.FlightRouteList2.Name = "FlightRouteList2";
            this.FlightRouteList2.NullText = "Choose Runway";
            this.FlightRouteList2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.FlightRouteList2.RootElement.StretchVertically = true;
            this.FlightRouteList2.Size = new System.Drawing.Size(172, 21);
            this.FlightRouteList2.TabIndex = 111;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.FlightRulesList2);
            this.groupBox17.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox17.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox17.Location = new System.Drawing.Point(3, 15);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox17.Size = new System.Drawing.Size(79, 40);
            this.groupBox17.TabIndex = 109;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Flight Rule";
            // 
            // FlightRulesList2
            // 
            this.FlightRulesList2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRulesList2.Font = new System.Drawing.Font("Segoe UI", 8F);
            radListDataItem19.Text = "IFR";
            radListDataItem19.TextWrap = true;
            radListDataItem20.Text = "VFR";
            radListDataItem20.TextWrap = true;
            radListDataItem21.Text = "Local";
            radListDataItem21.TextWrap = true;
            this.FlightRulesList2.Items.Add(radListDataItem19);
            this.FlightRulesList2.Items.Add(radListDataItem20);
            this.FlightRulesList2.Items.Add(radListDataItem21);
            this.FlightRulesList2.Location = new System.Drawing.Point(6, 16);
            this.FlightRulesList2.Name = "FlightRulesList2";
            this.FlightRulesList2.NullText = "Choose Runway";
            this.FlightRulesList2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.FlightRulesList2.RootElement.StretchVertically = true;
            this.FlightRulesList2.Size = new System.Drawing.Size(67, 21);
            this.FlightRulesList2.TabIndex = 111;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.DirectGoList);
            this.groupBox12.Controls.Add(this.DirectBtn);
            this.groupBox12.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.groupBox12.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox12.Location = new System.Drawing.Point(0, 115);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(126, 49);
            this.groupBox12.TabIndex = 154;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Direct Go to";
            // 
            // DirectGoList
            // 
            this.DirectGoList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DirectGoList.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.DirectGoList.Location = new System.Drawing.Point(11, 16);
            this.DirectGoList.Name = "DirectGoList";
            this.DirectGoList.NullText = "Choose Runway";
            this.DirectGoList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DirectGoList.Size = new System.Drawing.Size(66, 22);
            this.DirectGoList.TabIndex = 107;
            // 
            // DirectBtn
            // 
            this.DirectBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.DirectBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.DirectBtn.FlatAppearance.BorderSize = 0;
            this.DirectBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.DirectBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.DirectBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DirectBtn.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.DirectBtn.ForeColor = System.Drawing.Color.White;
            this.DirectBtn.Location = new System.Drawing.Point(81, 13);
            this.DirectBtn.Name = "DirectBtn";
            this.DirectBtn.Size = new System.Drawing.Size(40, 28);
            this.DirectBtn.TabIndex = 108;
            this.DirectBtn.Text = "Ok";
            this.DirectBtn.UseVisualStyleBackColor = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.AbsoluteChange);
            this.groupBox2.Controls.Add(this.AbsoluteBox);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(274, -2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox2.Size = new System.Drawing.Size(132, 72);
            this.groupBox2.TabIndex = 151;
            this.groupBox2.TabStop = false;
            // 
            // AbsoluteChange
            // 
            this.AbsoluteChange.BackColor = System.Drawing.Color.DodgerBlue;
            this.AbsoluteChange.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AbsoluteChange.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.AbsoluteChange.ForeColor = System.Drawing.Color.White;
            this.AbsoluteChange.Location = new System.Drawing.Point(6, 39);
            this.AbsoluteChange.Name = "AbsoluteChange";
            this.AbsoluteChange.Size = new System.Drawing.Size(119, 28);
            this.AbsoluteChange.TabIndex = 151;
            this.AbsoluteChange.Text = "Change Heading";
            this.AbsoluteChange.UseVisualStyleBackColor = false;
            // 
            // AbsoluteBox
            // 
            this.AbsoluteBox.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.AbsoluteBox.Location = new System.Drawing.Point(69, 13);
            this.AbsoluteBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.AbsoluteBox.Name = "AbsoluteBox";
            this.AbsoluteBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.AbsoluteBox.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.AbsoluteBox.Size = new System.Drawing.Size(56, 25);
            this.AbsoluteBox.TabIndex = 150;
            this.AbsoluteBox.TabStop = false;
            this.AbsoluteBox.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.AbsoluteBox.Value = new decimal(new int[] {
            360,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label11.ForeColor = System.Drawing.Color.Tomato;
            this.label11.Location = new System.Drawing.Point(6, 18);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label11.Size = new System.Drawing.Size(65, 16);
            this.label11.TabIndex = 103;
            this.label11.Text = "Absolute H";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.RelativeChange);
            this.groupBox9.Controls.Add(this.RelativeBox);
            this.groupBox9.Controls.Add(this.label13);
            this.groupBox9.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox9.ForeColor = System.Drawing.Color.White;
            this.groupBox9.Location = new System.Drawing.Point(419, -2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox9.Size = new System.Drawing.Size(131, 72);
            this.groupBox9.TabIndex = 152;
            this.groupBox9.TabStop = false;
            // 
            // RelativeChange
            // 
            this.RelativeChange.BackColor = System.Drawing.Color.DodgerBlue;
            this.RelativeChange.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RelativeChange.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.RelativeChange.ForeColor = System.Drawing.Color.White;
            this.RelativeChange.Location = new System.Drawing.Point(4, 39);
            this.RelativeChange.Name = "RelativeChange";
            this.RelativeChange.Size = new System.Drawing.Size(119, 28);
            this.RelativeChange.TabIndex = 153;
            this.RelativeChange.Text = "Change Heading";
            this.RelativeChange.UseVisualStyleBackColor = false;
            // 
            // RelativeBox
            // 
            this.RelativeBox.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.RelativeBox.Location = new System.Drawing.Point(67, 13);
            this.RelativeBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.RelativeBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.RelativeBox.Name = "RelativeBox";
            this.RelativeBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RelativeBox.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.RelativeBox.Size = new System.Drawing.Size(56, 25);
            this.RelativeBox.TabIndex = 152;
            this.RelativeBox.TabStop = false;
            this.RelativeBox.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.RelativeBox.Value = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label13.ForeColor = System.Drawing.Color.Tomato;
            this.label13.Location = new System.Drawing.Point(6, 18);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(64, 16);
            this.label13.TabIndex = 103;
            this.label13.Text = "Relative H";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.ContDir);
            this.groupBox1.Controls.Add(this.ContRoute);
            this.groupBox1.Controls.Add(this.Holding);
            this.groupBox1.Controls.Add(this.HoldingRight);
            this.groupBox1.Controls.Add(this.HoldList);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.HoldingLeft);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.groupBox1.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox1.Location = new System.Drawing.Point(-2, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(262, 116);
            this.groupBox1.TabIndex = 150;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Holding";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label6.ForeColor = System.Drawing.Color.Tomato;
            this.label6.Location = new System.Drawing.Point(6, 57);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(63, 16);
            this.label6.TabIndex = 112;
            this.label6.Text = "Holding At";
            // 
            // ContDir
            // 
            this.ContDir.BackColor = System.Drawing.Color.DodgerBlue;
            this.ContDir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ContDir.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.ContDir.ForeColor = System.Drawing.Color.White;
            this.ContDir.Location = new System.Drawing.Point(122, 82);
            this.ContDir.Name = "ContDir";
            this.ContDir.Size = new System.Drawing.Size(132, 28);
            this.ContDir.TabIndex = 111;
            this.ContDir.Text = "Continue Direction";
            this.ContDir.UseVisualStyleBackColor = false;
            // 
            // ContRoute
            // 
            this.ContRoute.BackColor = System.Drawing.Color.DodgerBlue;
            this.ContRoute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ContRoute.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.ContRoute.ForeColor = System.Drawing.Color.White;
            this.ContRoute.Location = new System.Drawing.Point(9, 82);
            this.ContRoute.Name = "ContRoute";
            this.ContRoute.Size = new System.Drawing.Size(105, 28);
            this.ContRoute.TabIndex = 110;
            this.ContRoute.Text = "Continue Route";
            this.ContRoute.UseVisualStyleBackColor = false;
            // 
            // Holding
            // 
            this.Holding.BackColor = System.Drawing.Color.DodgerBlue;
            this.Holding.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Holding.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.Holding.ForeColor = System.Drawing.Color.White;
            this.Holding.Location = new System.Drawing.Point(174, 51);
            this.Holding.Name = "Holding";
            this.Holding.Size = new System.Drawing.Size(80, 28);
            this.Holding.TabIndex = 109;
            this.Holding.Text = "Holding";
            this.Holding.UseVisualStyleBackColor = false;
            // 
            // HoldingRight
            // 
            this.HoldingRight.BackColor = System.Drawing.Color.DodgerBlue;
            this.HoldingRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HoldingRight.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.HoldingRight.ForeColor = System.Drawing.Color.White;
            this.HoldingRight.Location = new System.Drawing.Point(122, 19);
            this.HoldingRight.Name = "HoldingRight";
            this.HoldingRight.Size = new System.Drawing.Size(132, 28);
            this.HoldingRight.TabIndex = 108;
            this.HoldingRight.Text = "Holding Right";
            this.HoldingRight.UseVisualStyleBackColor = false;
            // 
            // HoldList
            // 
            this.HoldList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.HoldList.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.HoldList.Location = new System.Drawing.Point(75, 53);
            this.HoldList.Name = "HoldList";
            this.HoldList.NullText = "Choose Runway";
            this.HoldList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.HoldList.Size = new System.Drawing.Size(93, 25);
            this.HoldList.TabIndex = 106;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(10, 63);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(93, 21);
            this.label3.TabIndex = 107;
            this.label3.Text = "Holding At";
            // 
            // HoldingLeft
            // 
            this.HoldingLeft.BackColor = System.Drawing.Color.DodgerBlue;
            this.HoldingLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HoldingLeft.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.HoldingLeft.ForeColor = System.Drawing.Color.White;
            this.HoldingLeft.Location = new System.Drawing.Point(9, 19);
            this.HoldingLeft.Name = "HoldingLeft";
            this.HoldingLeft.Size = new System.Drawing.Size(105, 28);
            this.HoldingLeft.TabIndex = 7;
            this.HoldingLeft.Text = "Holding Left";
            this.HoldingLeft.UseVisualStyleBackColor = false;
            // 
            // OtherPageView
            // 
            this.OtherPageView.Controls.Add(this.IncidentCommand);
            this.OtherPageView.Controls.Add(this.LightsGroup);
            this.OtherPageView.Location = new System.Drawing.Point(10, 40);
            this.OtherPageView.Name = "OtherPageView";
            this.OtherPageView.Size = new System.Drawing.Size(555, 289);
            this.OtherPageView.Text = "Other Menu";
            // 
            // IncidentCommand
            // 
            this.IncidentCommand.Controls.Add(this.IncidentAnimal);
            this.IncidentCommand.Controls.Add(this.IncidentBird);
            this.IncidentCommand.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.IncidentCommand.ForeColor = System.Drawing.Color.Tomato;
            this.IncidentCommand.Location = new System.Drawing.Point(3, 3);
            this.IncidentCommand.Name = "IncidentCommand";
            this.IncidentCommand.Size = new System.Drawing.Size(271, 280);
            this.IncidentCommand.TabIndex = 159;
            this.IncidentCommand.TabStop = false;
            this.IncidentCommand.Text = "Incident";
            // 
            // IncidentAnimal
            // 
            this.IncidentAnimal.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.IncidentAnimal.ForeColor = System.Drawing.Color.Tomato;
            this.IncidentAnimal.Location = new System.Drawing.Point(19, 148);
            this.IncidentAnimal.Name = "IncidentAnimal";
            this.IncidentAnimal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IncidentAnimal.Size = new System.Drawing.Size(248, 108);
            this.IncidentAnimal.TabIndex = 155;
            this.IncidentAnimal.TabStop = false;
            this.IncidentAnimal.Text = "Animal Crossing";
            // 
            // IncidentBird
            // 
            this.IncidentBird.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.IncidentBird.ForeColor = System.Drawing.Color.Tomato;
            this.IncidentBird.Location = new System.Drawing.Point(19, 38);
            this.IncidentBird.Name = "IncidentBird";
            this.IncidentBird.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IncidentBird.Size = new System.Drawing.Size(248, 108);
            this.IncidentBird.TabIndex = 2;
            this.IncidentBird.TabStop = false;
            this.IncidentBird.Text = "Bird Attack";
            // 
            // LightsGroup
            // 
            this.LightsGroup.Controls.Add(this.ApproachGroup);
            this.LightsGroup.Controls.Add(this.groupBox23);
            this.LightsGroup.Controls.Add(this.RunwayGroup);
            this.LightsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.LightsGroup.ForeColor = System.Drawing.Color.Tomato;
            this.LightsGroup.Location = new System.Drawing.Point(276, 3);
            this.LightsGroup.Name = "LightsGroup";
            this.LightsGroup.Size = new System.Drawing.Size(276, 280);
            this.LightsGroup.TabIndex = 158;
            this.LightsGroup.TabStop = false;
            this.LightsGroup.Text = "Lights Properties";
            // 
            // ApproachGroup
            // 
            this.ApproachGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ApproachGroup.ForeColor = System.Drawing.Color.Tomato;
            this.ApproachGroup.Location = new System.Drawing.Point(20, 153);
            this.ApproachGroup.Name = "ApproachGroup";
            this.ApproachGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ApproachGroup.Size = new System.Drawing.Size(245, 108);
            this.ApproachGroup.TabIndex = 4;
            this.ApproachGroup.TabStop = false;
            this.ApproachGroup.Text = "Approach";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.PapiLight);
            this.groupBox23.Controls.Add(this.TaxiLight);
            this.groupBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox23.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox23.Location = new System.Drawing.Point(19, 93);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox23.Size = new System.Drawing.Size(246, 60);
            this.groupBox23.TabIndex = 3;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Taxi / Papi";
            // 
            // PapiLight
            // 
            this.PapiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.PapiLight.BackColor = System.Drawing.Color.Tomato;
            this.PapiLight.FlatAppearance.BorderSize = 0;
            this.PapiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.PapiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PapiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PapiLight.ForeColor = System.Drawing.Color.White;
            this.PapiLight.Location = new System.Drawing.Point(126, 22);
            this.PapiLight.Name = "PapiLight";
            this.PapiLight.Size = new System.Drawing.Size(108, 30);
            this.PapiLight.TabIndex = 108;
            this.PapiLight.Text = "Papi";
            this.PapiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PapiLight.UseVisualStyleBackColor = false;
            // 
            // TaxiLight
            // 
            this.TaxiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.TaxiLight.BackColor = System.Drawing.Color.Tomato;
            this.TaxiLight.FlatAppearance.BorderSize = 0;
            this.TaxiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.TaxiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaxiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiLight.ForeColor = System.Drawing.Color.White;
            this.TaxiLight.Location = new System.Drawing.Point(15, 22);
            this.TaxiLight.Name = "TaxiLight";
            this.TaxiLight.Size = new System.Drawing.Size(105, 30);
            this.TaxiLight.TabIndex = 107;
            this.TaxiLight.Text = "Taxi";
            this.TaxiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TaxiLight.UseVisualStyleBackColor = false;
            // 
            // RunwayGroup
            // 
            this.RunwayGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RunwayGroup.ForeColor = System.Drawing.Color.Tomato;
            this.RunwayGroup.Location = new System.Drawing.Point(19, 32);
            this.RunwayGroup.Name = "RunwayGroup";
            this.RunwayGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RunwayGroup.Size = new System.Drawing.Size(246, 60);
            this.RunwayGroup.TabIndex = 2;
            this.RunwayGroup.TabStop = false;
            this.RunwayGroup.Text = "Runway";
            // 
            // EnvironmentPageView
            // 
            this.EnvironmentPageView.Controls.Add(this.groupBox16);
            this.EnvironmentPageView.Location = new System.Drawing.Point(10, 40);
            this.EnvironmentPageView.Name = "EnvironmentPageView";
            this.EnvironmentPageView.Size = new System.Drawing.Size(555, 289);
            this.EnvironmentPageView.Text = "Environment Menu";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.ChangeTime);
            this.groupBox16.Controls.Add(this.ChangeVisibility);
            this.groupBox16.Controls.Add(this.ChangeTemperatue);
            this.groupBox16.Controls.Add(this.ChangeWindSpeed);
            this.groupBox16.Controls.Add(this.ChangeWindDir);
            this.groupBox16.Controls.Add(this.ChangeWeather);
            this.groupBox16.Controls.Add(this.EnvTime);
            this.groupBox16.Controls.Add(this.label12);
            this.groupBox16.Controls.Add(this.label7);
            this.groupBox16.Controls.Add(this.Visibility);
            this.groupBox16.Controls.Add(this.Temperature);
            this.groupBox16.Controls.Add(this.WindSpeed);
            this.groupBox16.Controls.Add(this.WindDirection);
            this.groupBox16.Controls.Add(this.label10);
            this.groupBox16.Controls.Add(this.label14);
            this.groupBox16.Controls.Add(this.label15);
            this.groupBox16.Controls.Add(this.Weather);
            this.groupBox16.Controls.Add(this.label16);
            this.groupBox16.Controls.Add(this.label17);
            this.groupBox16.Controls.Add(this.label19);
            this.groupBox16.Controls.Add(this.label20);
            this.groupBox16.Controls.Add(this.label18);
            this.groupBox16.Controls.Add(this.label25);
            this.groupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBox16.ForeColor = System.Drawing.Color.Tomato;
            this.groupBox16.Location = new System.Drawing.Point(3, 3);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(500, 280);
            this.groupBox16.TabIndex = 115;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Environment Properties";
            // 
            // ChangeTime
            // 
            this.ChangeTime.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeTime.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeTime.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ChangeTime.ForeColor = System.Drawing.Color.White;
            this.ChangeTime.Location = new System.Drawing.Point(378, 207);
            this.ChangeTime.Name = "ChangeTime";
            this.ChangeTime.Size = new System.Drawing.Size(107, 30);
            this.ChangeTime.TabIndex = 178;
            this.ChangeTime.Text = "Change";
            this.ChangeTime.UseVisualStyleBackColor = false;
            // 
            // ChangeVisibility
            // 
            this.ChangeVisibility.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeVisibility.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeVisibility.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ChangeVisibility.ForeColor = System.Drawing.Color.White;
            this.ChangeVisibility.Location = new System.Drawing.Point(378, 172);
            this.ChangeVisibility.Name = "ChangeVisibility";
            this.ChangeVisibility.Size = new System.Drawing.Size(107, 30);
            this.ChangeVisibility.TabIndex = 177;
            this.ChangeVisibility.Text = "Change";
            this.ChangeVisibility.UseVisualStyleBackColor = false;
            // 
            // ChangeTemperatue
            // 
            this.ChangeTemperatue.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeTemperatue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeTemperatue.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ChangeTemperatue.ForeColor = System.Drawing.Color.White;
            this.ChangeTemperatue.Location = new System.Drawing.Point(378, 135);
            this.ChangeTemperatue.Name = "ChangeTemperatue";
            this.ChangeTemperatue.Size = new System.Drawing.Size(107, 30);
            this.ChangeTemperatue.TabIndex = 176;
            this.ChangeTemperatue.Text = "Change";
            this.ChangeTemperatue.UseVisualStyleBackColor = false;
            // 
            // ChangeWindSpeed
            // 
            this.ChangeWindSpeed.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeWindSpeed.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeWindSpeed.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ChangeWindSpeed.ForeColor = System.Drawing.Color.White;
            this.ChangeWindSpeed.Location = new System.Drawing.Point(378, 101);
            this.ChangeWindSpeed.Name = "ChangeWindSpeed";
            this.ChangeWindSpeed.Size = new System.Drawing.Size(107, 30);
            this.ChangeWindSpeed.TabIndex = 175;
            this.ChangeWindSpeed.Text = "Change";
            this.ChangeWindSpeed.UseVisualStyleBackColor = false;
            // 
            // ChangeWindDir
            // 
            this.ChangeWindDir.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeWindDir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeWindDir.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ChangeWindDir.ForeColor = System.Drawing.Color.White;
            this.ChangeWindDir.Location = new System.Drawing.Point(378, 65);
            this.ChangeWindDir.Name = "ChangeWindDir";
            this.ChangeWindDir.Size = new System.Drawing.Size(107, 30);
            this.ChangeWindDir.TabIndex = 174;
            this.ChangeWindDir.Text = "Change";
            this.ChangeWindDir.UseVisualStyleBackColor = false;
            // 
            // ChangeWeather
            // 
            this.ChangeWeather.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeWeather.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeWeather.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ChangeWeather.ForeColor = System.Drawing.Color.White;
            this.ChangeWeather.Location = new System.Drawing.Point(378, 26);
            this.ChangeWeather.Name = "ChangeWeather";
            this.ChangeWeather.Size = new System.Drawing.Size(107, 30);
            this.ChangeWeather.TabIndex = 173;
            this.ChangeWeather.Text = "Change";
            this.ChangeWeather.UseVisualStyleBackColor = false;
            // 
            // EnvTime
            // 
            this.EnvTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.EnvTime.Location = new System.Drawing.Point(238, 211);
            this.EnvTime.Name = "EnvTime";
            this.EnvTime.Size = new System.Drawing.Size(122, 24);
            this.EnvTime.Step = 30;
            this.EnvTime.TabIndex = 172;
            this.EnvTime.TabStop = false;
            this.EnvTime.Text = "radTimePicker1";
            this.EnvTime.TimeTables = Telerik.WinControls.UI.TimeTables.HoursAndMinutesInOneTable;
            this.EnvTime.Value = new System.DateTime(2014, 8, 16, 11, 30, 4, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label12.ForeColor = System.Drawing.Color.Tomato;
            this.label12.Location = new System.Drawing.Point(338, 178);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 17);
            this.label12.TabIndex = 118;
            this.label12.Text = "m";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label7.ForeColor = System.Drawing.Color.Tomato;
            this.label7.Location = new System.Drawing.Point(338, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 117;
            this.label7.Text = "o";
            // 
            // Visibility
            // 
            this.Visibility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Visibility.Location = new System.Drawing.Point(282, 174);
            this.Visibility.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.Visibility.Name = "Visibility";
            // 
            // 
            // 
            this.Visibility.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Visibility.Size = new System.Drawing.Size(56, 24);
            this.Visibility.TabIndex = 116;
            this.Visibility.TabStop = false;
            this.Visibility.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Visibility.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // Temperature
            // 
            this.Temperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Temperature.Location = new System.Drawing.Point(282, 138);
            this.Temperature.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Temperature.Name = "Temperature";
            // 
            // 
            // 
            this.Temperature.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Temperature.Size = new System.Drawing.Size(56, 24);
            this.Temperature.TabIndex = 115;
            this.Temperature.TabStop = false;
            this.Temperature.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Temperature.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // WindSpeed
            // 
            this.WindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindSpeed.Location = new System.Drawing.Point(282, 103);
            this.WindSpeed.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.WindSpeed.Name = "WindSpeed";
            // 
            // 
            // 
            this.WindSpeed.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindSpeed.Size = new System.Drawing.Size(56, 24);
            this.WindSpeed.TabIndex = 114;
            this.WindSpeed.TabStop = false;
            this.WindSpeed.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // WindDirection
            // 
            this.WindDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindDirection.Location = new System.Drawing.Point(282, 68);
            this.WindDirection.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.WindDirection.Name = "WindDirection";
            // 
            // 
            // 
            this.WindDirection.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindDirection.Size = new System.Drawing.Size(56, 24);
            this.WindDirection.TabIndex = 113;
            this.WindDirection.TabStop = false;
            this.WindDirection.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindDirection.Value = new decimal(new int[] {
            359,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.ForeColor = System.Drawing.Color.Tomato;
            this.label10.Location = new System.Drawing.Point(343, 142);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 112;
            this.label10.Text = "C";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label14.ForeColor = System.Drawing.Color.Tomato;
            this.label14.Location = new System.Drawing.Point(338, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 17);
            this.label14.TabIndex = 111;
            this.label14.Text = "kn";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label15.ForeColor = System.Drawing.Color.Tomato;
            this.label15.Location = new System.Drawing.Point(338, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 109;
            this.label15.Text = "o";
            // 
            // Weather
            // 
            this.Weather.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Weather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Weather.ForeColor = System.Drawing.Color.DodgerBlue;
            radListDataItem22.Text = "Clear";
            radListDataItem22.TextWrap = true;
            radListDataItem23.Text = "Rain";
            radListDataItem23.TextWrap = true;
            this.Weather.Items.Add(radListDataItem22);
            this.Weather.Items.Add(radListDataItem23);
            this.Weather.Location = new System.Drawing.Point(238, 29);
            this.Weather.Name = "Weather";
            this.Weather.Size = new System.Drawing.Size(124, 24);
            this.Weather.TabIndex = 108;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label16.ForeColor = System.Drawing.Color.Tomato;
            this.label16.Location = new System.Drawing.Point(11, 213);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 20);
            this.label16.TabIndex = 24;
            this.label16.Text = "Time";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.ForeColor = System.Drawing.Color.Tomato;
            this.label17.Location = new System.Drawing.Point(11, 178);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 20);
            this.label17.TabIndex = 14;
            this.label17.Text = "Visibility";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label19.ForeColor = System.Drawing.Color.Tomato;
            this.label19.Location = new System.Drawing.Point(11, 141);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 20);
            this.label19.TabIndex = 12;
            this.label19.Text = "Temperature";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label20.ForeColor = System.Drawing.Color.Tomato;
            this.label20.Location = new System.Drawing.Point(11, 71);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(112, 20);
            this.label20.TabIndex = 9;
            this.label20.Text = "Wind Direction";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label18.ForeColor = System.Drawing.Color.Tomato;
            this.label18.Location = new System.Drawing.Point(11, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 20);
            this.label18.TabIndex = 7;
            this.label18.Text = "Weather";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label25.ForeColor = System.Drawing.Color.Tomato;
            this.label25.Location = new System.Drawing.Point(11, 107);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "Wind Speed";
            // 
            // AircraftList
            // 
            this.AircraftList.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.AircraftList.Location = new System.Drawing.Point(240, 23);
            this.AircraftList.Name = "AircraftList";
            this.AircraftList.Size = new System.Drawing.Size(98, 21);
            this.AircraftList.TabIndex = 1;
            this.AircraftList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.AircraftList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(237, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Callsign";
            // 
            // StartSimulation
            // 
            this.StartSimulation.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.StartSimulation.Location = new System.Drawing.Point(5, 5);
            this.StartSimulation.Name = "StartSimulation";
            this.StartSimulation.Size = new System.Drawing.Size(110, 40);
            this.StartSimulation.TabIndex = 3;
            this.StartSimulation.Text = "Start Simulation";
            this.StartSimulation.Click += new System.EventHandler(this.StartSimulation_Click);
            // 
            // PauseSimulation
            // 
            this.PauseSimulation.Enabled = false;
            this.PauseSimulation.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.PauseSimulation.Location = new System.Drawing.Point(121, 5);
            this.PauseSimulation.Name = "PauseSimulation";
            this.PauseSimulation.Size = new System.Drawing.Size(110, 40);
            this.PauseSimulation.TabIndex = 4;
            this.PauseSimulation.Text = "Pause Simulation";
            // 
            // AircraftCommandGroup
            // 
            this.AircraftCommandGroup.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.AircraftCommandGroup.Controls.Add(this.FuelValue);
            this.AircraftCommandGroup.Controls.Add(this.radSpinEditor2);
            this.AircraftCommandGroup.Controls.Add(this.FuelBtn);
            this.AircraftCommandGroup.Controls.Add(this.EngineFailureValue);
            this.AircraftCommandGroup.Controls.Add(this.EngineFailure);
            this.AircraftCommandGroup.Controls.Add(this.RemoveAircraft);
            this.AircraftCommandGroup.Controls.Add(this.SelectAircraft);
            this.AircraftCommandGroup.Enabled = false;
            this.AircraftCommandGroup.HeaderText = "Aircraft Command";
            this.AircraftCommandGroup.Location = new System.Drawing.Point(344, 5);
            this.AircraftCommandGroup.Name = "AircraftCommandGroup";
            this.AircraftCommandGroup.Size = new System.Drawing.Size(483, 47);
            this.AircraftCommandGroup.TabIndex = 5;
            this.AircraftCommandGroup.Text = "Aircraft Command";
            // 
            // FuelValue
            // 
            this.FuelValue.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.FuelValue.Location = new System.Drawing.Point(394, 20);
            this.FuelValue.Name = "FuelValue";
            this.FuelValue.Size = new System.Drawing.Size(84, 21);
            this.FuelValue.TabIndex = 0;
            this.FuelValue.TabStop = false;
            this.FuelValue.Text = "radTimePicker1";
            this.FuelValue.Value = new System.DateTime(2015, 2, 15, 8, 58, 0, 0);
            // 
            // radSpinEditor2
            // 
            this.radSpinEditor2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.radSpinEditor2.Location = new System.Drawing.Point(489, 18);
            this.radSpinEditor2.Name = "radSpinEditor2";
            this.radSpinEditor2.Size = new System.Drawing.Size(44, 23);
            this.radSpinEditor2.TabIndex = 5;
            this.radSpinEditor2.TabStop = false;
            // 
            // FuelBtn
            // 
            this.FuelBtn.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.FuelBtn.Location = new System.Drawing.Point(340, 17);
            this.FuelBtn.Name = "FuelBtn";
            this.FuelBtn.Size = new System.Drawing.Size(50, 25);
            this.FuelBtn.TabIndex = 4;
            this.FuelBtn.Text = "Fuel";
            // 
            // EngineFailureValue
            // 
            this.EngineFailureValue.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.EngineFailureValue.Location = new System.Drawing.Point(293, 18);
            this.EngineFailureValue.Name = "EngineFailureValue";
            this.EngineFailureValue.Size = new System.Drawing.Size(44, 23);
            this.EngineFailureValue.TabIndex = 3;
            this.EngineFailureValue.TabStop = false;
            // 
            // EngineFailure
            // 
            this.EngineFailure.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.EngineFailure.Location = new System.Drawing.Point(197, 17);
            this.EngineFailure.Name = "EngineFailure";
            this.EngineFailure.Size = new System.Drawing.Size(90, 25);
            this.EngineFailure.TabIndex = 2;
            this.EngineFailure.Text = "Engine Failure";
            // 
            // RemoveAircraft
            // 
            this.RemoveAircraft.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.RemoveAircraft.Location = new System.Drawing.Point(101, 17);
            this.RemoveAircraft.Name = "RemoveAircraft";
            this.RemoveAircraft.Size = new System.Drawing.Size(90, 25);
            this.RemoveAircraft.TabIndex = 1;
            this.RemoveAircraft.Text = "Remove Aircraft";
            // 
            // SelectAircraft
            // 
            this.SelectAircraft.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.SelectAircraft.Location = new System.Drawing.Point(5, 17);
            this.SelectAircraft.Name = "SelectAircraft";
            this.SelectAircraft.Size = new System.Drawing.Size(90, 25);
            this.SelectAircraft.TabIndex = 0;
            this.SelectAircraft.Text = "Select Aircraft";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.LogText);
            this.radGroupBox2.HeaderText = "Log";
            this.radGroupBox2.Location = new System.Drawing.Point(582, 58);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(245, 323);
            this.radGroupBox2.TabIndex = 6;
            this.radGroupBox2.Text = "Log";
            // 
            // LogText
            // 
            this.LogText.AutoSize = false;
            this.LogText.Location = new System.Drawing.Point(5, 21);
            this.LogText.Multiline = true;
            this.LogText.Name = "LogText";
            this.LogText.Size = new System.Drawing.Size(235, 296);
            this.LogText.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(830, 388);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.AircraftCommandGroup);
            this.Controls.Add(this.PauseSimulation);
            this.Controls.Add(this.StartSimulation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AircraftList);
            this.Controls.Add(this.AircraftCommandView);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "Dummy System";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ChildFormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCommandView)).EndInit();
            this.AircraftCommandView.ResumeLayout(false);
            this.GroundPageView.ResumeLayout(false);
            this.GroundPageView.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList7)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).EndInit();
            this.TakeOffGroup.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IntersectionSpeedList)).EndInit();
            this.FlightRouteGroup.ResumeLayout(false);
            this.Runway.ResumeLayout(false);
            this.Runway.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationList)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList)).EndInit();
            this.TaxiGroup.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaxiSpeedList)).EndInit();
            this.OtherCommandGroup.ResumeLayout(false);
            this.PushbackGroup.ResumeLayout(false);
            this.FlightPageView.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedBox)).EndInit();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AltitudeBox)).EndInit();
            this.groupBox20.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList3)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList2)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList2)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList2)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DirectGoList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AbsoluteBox)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RelativeBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HoldList)).EndInit();
            this.OtherPageView.ResumeLayout(false);
            this.IncidentCommand.ResumeLayout(false);
            this.LightsGroup.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.EnvironmentPageView.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartSimulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PauseSimulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCommandGroup)).EndInit();
            this.AircraftCommandGroup.ResumeLayout(false);
            this.AircraftCommandGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FuelValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuelBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineFailureValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineFailure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemoveAircraft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectAircraft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LogText)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView AircraftCommandView;
        private Telerik.WinControls.UI.RadPageViewPage GroundPageView;
        private Telerik.WinControls.UI.RadPageViewPage FlightPageView;
        private Telerik.WinControls.UI.RadPageViewPage OtherPageView;
        private Telerik.WinControls.UI.RadDropDownList AircraftList;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton StartSimulation;
        private Telerik.WinControls.UI.RadButton PauseSimulation;
        private Telerik.WinControls.UI.RadGroupBox AircraftCommandGroup;
        private Telerik.WinControls.UI.RadTimePicker FuelValue;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor2;
        private Telerik.WinControls.UI.RadButton FuelBtn;
        private Telerik.WinControls.UI.RadSpinEditor EngineFailureValue;
        private Telerik.WinControls.UI.RadButton EngineFailure;
        private Telerik.WinControls.UI.RadButton RemoveAircraft;
        private Telerik.WinControls.UI.RadButton SelectAircraft;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadTextBox LogText;
        private System.Windows.Forms.CheckBox Engine;
        private System.Windows.Forms.GroupBox PushbackGroup;
        private System.Windows.Forms.GroupBox OtherCommandGroup;
        private System.Windows.Forms.CheckBox RockingWing;
        private System.Windows.Forms.Button OneEighty;
        private System.Windows.Forms.Button UTurnRight;
        private System.Windows.Forms.Button UTurnLeft;
        private System.Windows.Forms.GroupBox TaxiGroup;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button ChangeSpeedTaxi;
        private Telerik.WinControls.UI.RadDropDownList TaxiSpeedList;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button StartTaxi;
        private System.Windows.Forms.Button CrossRW;
        private System.Windows.Forms.Button TaxiRoute;
        private System.Windows.Forms.GroupBox FlightRouteGroup;
        private System.Windows.Forms.GroupBox groupBox8;
        private Telerik.WinControls.UI.RadDropDownList FlightRouteList;
        private System.Windows.Forms.GroupBox groupBox7;
        private Telerik.WinControls.UI.RadDropDownList DestinationList;
        private System.Windows.Forms.GroupBox groupBox6;
        private Telerik.WinControls.UI.RadDropDownList FlightRulesList;
        private System.Windows.Forms.GroupBox Runway;
        private Telerik.WinControls.UI.RadDropDownList RunwayList;
        private System.Windows.Forms.GroupBox TakeOffGroup;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button ChangeSpeedIntersection;
        private Telerik.WinControls.UI.RadDropDownList IntersectionSpeedList;
        private System.Windows.Forms.Button TakeOff;
        private System.Windows.Forms.Button RunwayHold;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.GroupBox groupBox11;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList7;
        private System.Windows.Forms.GroupBox groupBox10;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ReadyAircraft;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ContDir;
        private System.Windows.Forms.Button ContRoute;
        private System.Windows.Forms.Button Holding;
        private System.Windows.Forms.Button HoldingRight;
        private Telerik.WinControls.UI.RadDropDownList HoldList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button HoldingLeft;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadSpinEditor AbsoluteBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox9;
        private Telerik.WinControls.UI.RadSpinEditor RelativeBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox12;
        private Telerik.WinControls.UI.RadDropDownList DirectGoList;
        private System.Windows.Forms.Button DirectBtn;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox15;
        private Telerik.WinControls.UI.RadDropDownList FlightRouteList2;
        private System.Windows.Forms.GroupBox groupBox17;
        private Telerik.WinControls.UI.RadDropDownList FlightRulesList2;
        private System.Windows.Forms.GroupBox groupBox18;
        private Telerik.WinControls.UI.RadDropDownList RunwayList2;
        private System.Windows.Forms.Button RunwayBtn;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadDropDownList RunwayList3;
        private System.Windows.Forms.Button InstrumentApp;
        private System.Windows.Forms.Button JoinCircuit;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Button RockingWing2;
        private System.Windows.Forms.Button Flypass;
        private System.Windows.Forms.Button TouchGo;
        private System.Windows.Forms.Button MissedApproach;
        private System.Windows.Forms.GroupBox groupBox21;
        private Telerik.WinControls.UI.RadMaskedEditBox SpeedBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox22;
        private Telerik.WinControls.UI.RadMaskedEditBox AltitudeBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button Orbit;
        private System.Windows.Forms.Button ExtendDownwind;
        private System.Windows.Forms.Button AbortTakeOff;
        private System.Windows.Forms.Button ClearLand;
        private System.Windows.Forms.Button ReturnBase;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ChangeSpeed;
        private System.Windows.Forms.Button AbsoluteChange;
        private System.Windows.Forms.Button RelativeChange;
        private System.Windows.Forms.Button ChangeAltitude;
        private Telerik.WinControls.UI.RadPageViewPage EnvironmentPageView;
        private System.Windows.Forms.GroupBox LightsGroup;
        private System.Windows.Forms.GroupBox ApproachGroup;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.CheckBox PapiLight;
        private System.Windows.Forms.CheckBox TaxiLight;
        private System.Windows.Forms.GroupBox RunwayGroup;
        private System.Windows.Forms.GroupBox IncidentCommand;
        private System.Windows.Forms.GroupBox IncidentAnimal;
        private System.Windows.Forms.GroupBox IncidentBird;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button ChangeTime;
        private System.Windows.Forms.Button ChangeVisibility;
        private System.Windows.Forms.Button ChangeTemperatue;
        private System.Windows.Forms.Button ChangeWindSpeed;
        private System.Windows.Forms.Button ChangeWindDir;
        private System.Windows.Forms.Button ChangeWeather;
        private Telerik.WinControls.UI.RadTimePicker EnvTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadSpinEditor Visibility;
        private Telerik.WinControls.UI.RadSpinEditor Temperature;
        private Telerik.WinControls.UI.RadSpinEditor WindSpeed;
        private Telerik.WinControls.UI.RadSpinEditor WindDirection;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private Telerik.WinControls.UI.RadDropDownList Weather;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label25;
    }
}

