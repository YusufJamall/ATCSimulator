﻿namespace ATCSimulator.DummySystem
{
    partial class TaxiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaxiForm));
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.TargetLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.StatusTaxi = new System.Windows.Forms.Label();
            this.OKBtn = new System.Windows.Forms.Button();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.LegendBtn = new System.Windows.Forms.CheckBox();
            this.ResetBtn = new System.Windows.Forms.Button();
            this.FlashContainer = new AxShockwaveFlashObjects.AxShockwaveFlash();
            this.MenuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlashContainer)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuPanel
            // 
            this.MenuPanel.BackColor = System.Drawing.Color.Black;
            this.MenuPanel.Controls.Add(this.TargetLabel);
            this.MenuPanel.Controls.Add(this.label2);
            this.MenuPanel.Controls.Add(this.StatusTaxi);
            this.MenuPanel.Controls.Add(this.OKBtn);
            this.MenuPanel.Controls.Add(this.CloseBtn);
            this.MenuPanel.Controls.Add(this.LegendBtn);
            this.MenuPanel.Controls.Add(this.ResetBtn);
            this.MenuPanel.Location = new System.Drawing.Point(0, 600);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(1080, 40);
            this.MenuPanel.TabIndex = 4;
            // 
            // TargetLabel
            // 
            this.TargetLabel.AutoSize = true;
            this.TargetLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.TargetLabel.ForeColor = System.Drawing.Color.White;
            this.TargetLabel.Location = new System.Drawing.Point(656, 10);
            this.TargetLabel.Name = "TargetLabel";
            this.TargetLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TargetLabel.Size = new System.Drawing.Size(64, 21);
            this.TargetLabel.TabIndex = 112;
            this.TargetLabel.Text = "RW 34L";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label2.ForeColor = System.Drawing.Color.Tomato;
            this.label2.Location = new System.Drawing.Point(580, 10);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(70, 21);
            this.label2.TabIndex = 111;
            this.label2.Text = "Target :";
            // 
            // StatusTaxi
            // 
            this.StatusTaxi.AutoSize = true;
            this.StatusTaxi.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.StatusTaxi.ForeColor = System.Drawing.Color.White;
            this.StatusTaxi.Location = new System.Drawing.Point(153, 9);
            this.StatusTaxi.Name = "StatusTaxi";
            this.StatusTaxi.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StatusTaxi.Size = new System.Drawing.Size(65, 22);
            this.StatusTaxi.TabIndex = 110;
            this.StatusTaxi.Text = "Status";
            // 
            // OKBtn
            // 
            this.OKBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.OKBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.OKBtn.ForeColor = System.Drawing.Color.White;
            this.OKBtn.Location = new System.Drawing.Point(736, 4);
            this.OKBtn.Name = "OKBtn";
            this.OKBtn.Size = new System.Drawing.Size(110, 30);
            this.OKBtn.TabIndex = 108;
            this.OKBtn.Text = "OK";
            this.OKBtn.UseVisualStyleBackColor = false;
            this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // CloseBtn
            // 
            this.CloseBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.CloseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CloseBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.CloseBtn.ForeColor = System.Drawing.Color.White;
            this.CloseBtn.Location = new System.Drawing.Point(967, 4);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(110, 30);
            this.CloseBtn.TabIndex = 107;
            this.CloseBtn.Text = "Close";
            this.CloseBtn.UseVisualStyleBackColor = false;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // LegendBtn
            // 
            this.LegendBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.LegendBtn.BackColor = System.Drawing.Color.Tomato;
            this.LegendBtn.Checked = true;
            this.LegendBtn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.LegendBtn.FlatAppearance.BorderSize = 0;
            this.LegendBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.LegendBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LegendBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.LegendBtn.ForeColor = System.Drawing.Color.White;
            this.LegendBtn.Location = new System.Drawing.Point(7, 4);
            this.LegendBtn.Name = "LegendBtn";
            this.LegendBtn.Size = new System.Drawing.Size(140, 30);
            this.LegendBtn.TabIndex = 106;
            this.LegendBtn.Text = "Legend";
            this.LegendBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LegendBtn.UseVisualStyleBackColor = false;
            this.LegendBtn.CheckedChanged += new System.EventHandler(this.LegendBtn_CheckedChanged);
            // 
            // ResetBtn
            // 
            this.ResetBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.ResetBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ResetBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.ResetBtn.ForeColor = System.Drawing.Color.White;
            this.ResetBtn.Location = new System.Drawing.Point(852, 4);
            this.ResetBtn.Name = "ResetBtn";
            this.ResetBtn.Size = new System.Drawing.Size(110, 30);
            this.ResetBtn.TabIndex = 105;
            this.ResetBtn.Text = "Reset";
            this.ResetBtn.UseVisualStyleBackColor = false;
            this.ResetBtn.Click += new System.EventHandler(this.ResetBtn_Click);
            // 
            // FlashContainer
            // 
            this.FlashContainer.Enabled = true;
            this.FlashContainer.Location = new System.Drawing.Point(0, 0);
            this.FlashContainer.Name = "FlashContainer";
            this.FlashContainer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("FlashContainer.OcxState")));
            this.FlashContainer.Size = new System.Drawing.Size(1080, 600);
            this.FlashContainer.TabIndex = 5;
            this.FlashContainer.FlashCall += new AxShockwaveFlashObjects._IShockwaveFlashEvents_FlashCallEventHandler(this.FlashContainer_FlashCall);
            // 
            // TaxiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1080, 640);
            this.Controls.Add(this.FlashContainer);
            this.Controls.Add(this.MenuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TaxiForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaxiForm";
            this.MenuPanel.ResumeLayout(false);
            this.MenuPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlashContainer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.Button ResetBtn;
        private System.Windows.Forms.CheckBox LegendBtn;
        private System.Windows.Forms.Button OKBtn;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.Label TargetLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label StatusTaxi;
        private AxShockwaveFlashObjects.AxShockwaveFlash FlashContainer;


    }
}