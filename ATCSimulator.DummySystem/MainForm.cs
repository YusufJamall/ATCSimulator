﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.DummySystem.Helper;
using ATCSimulator.DummySystem.Model;
using System.IO;
using Newtonsoft.Json;

namespace ATCSimulator.DummySystem
{
    public partial class MainForm : Form
    {
        VisualNetwork visual_network;

        VisualConfig visual_config;
        List<AircraftData> aircrafts;
        ExerciseSimulationModel exercise;
        List<FlightRouteListModel> flight_routes;
        TaxiForm taxi_form;
        AircraftData current_aircraft;
        public enum FlightRuleInfo
        {
            IFR,
            VFR,
            Local
        }

        public static MainForm Instance { get; private set; }
        public MainForm()
        {
            InitializeComponent();
            Instance = this;
        }
        public static void LogMessage(string message)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    Instance.LogText.Text += "\n" + message;
                }));
            }
            else
            {
                Instance.LogText.Text += "\n" + message;
            }
        }
        public void ChildFormClosed(object sender, FormClosedEventArgs e)
        {
            visual_network.Disconnect();
            Application.Exit();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            visual_config = new VisualConfig()
            {
                host = System.Configuration.ConfigurationManager.AppSettings["SfsHost"].ToString(),
                port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SfsPort"]),
                room_name = System.Configuration.ConfigurationManager.AppSettings["SfsRoom"].ToString(),
                user_name = System.Configuration.ConfigurationManager.AppSettings["SfsUser"].ToString(),
                zone_name = System.Configuration.ConfigurationManager.AppSettings["SfsZone"].ToString()
            };
            visual_network = new VisualNetwork(visual_config);
            taxi_form = new TaxiForm(this);
            taxi_form.Init("");
        }


        #region Ground Controller

        #region Button Handler

        #region Start Simulation

        private void StartSimulation_Click(object sender, EventArgs e)
        {
            visual_network.Connect();
            flight_routes = new List<FlightRouteListModel>();
            string file_path = Directory.GetCurrentDirectory() + "\\Asset\\";
            using (StreamReader reader = new StreamReader(file_path + "FlightRouteList.json"))
            {
                string flight_route_list = reader.ReadToEnd();
                flight_routes = JsonConvert.DeserializeObject<List<FlightRouteListModel>>(flight_route_list);
            }
            aircrafts = InitData.CreateDataAircrafts(flight_routes.FirstOrDefault());
            exercise = InitData.CreateExerciseData();
            visual_network.CreateSimulation(exercise, aircrafts);

            PauseSimulation.Enabled = true;
            StartSimulation.Enabled = false;
            AircraftCommandGroup.Enabled = true;

            foreach (AircraftData ac in aircrafts)
            {
                AircraftList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(ac.callsign, ac));
            }
        }

        #endregion

        #region Selected List

        private void FlightRulesList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (FlightRulesList.SelectedIndex == -1)
                return;
            DestinationList.SelectedIndex = -1;
            DestinationList.Items.Clear();
            FlightRouteList.SelectedIndex = -1;
            FlightRouteList.Items.Clear();
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            List<FlightRouteListModel> _airports = flight_routes.Where(d => d.is_departure == true && d.flight_rules.ToLower().Equals(FlightRulesList.Text.ToLower())).GroupBy(d => d.destination_airport_id).Select(d => d.First()).ToList();
            foreach (FlightRouteListModel d in _airports)
            {
                DestinationList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(d.destination_airport_name, d));
            }
        }

        private void DestinationList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if ((FlightRulesList.SelectedIndex == -1) || (DestinationList.SelectedIndex == -1))
                return;
            FlightRouteList.SelectedIndex = -1;
            FlightRouteList.Items.Clear();
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            FlightRouteListModel dest_data = GetDataDropDown<FlightRouteListModel>(DestinationList);
            List<FlightRouteListModel> routes = flight_routes.Where(d => d.is_departure == true && d.flight_rules_id.Equals(dest_data.flight_rules_id)
                && d.destination_airport_id.Equals(dest_data.destination_airport_id)).ToList();
            foreach (FlightRouteListModel s in routes)
            {
                string route = s.routes_name.Aggregate((i, j) => i + " " + j);
                FlightRouteList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(route, s));
            }
        }

        private void FlightRouteList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (FlightRouteList.SelectedIndex == -1)
                return;
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            FlightRouteListModel routes = GetDataDropDown<FlightRouteListModel>(FlightRouteList);
            foreach (string s in routes.runways)
            {
                RunwayList.Items.Add(s);
            }
        }

        private void RunwayList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (RunwayList.SelectedIndex == -1 || FlightRouteList.SelectedIndex == -1)
                return;
            string rw = RunwayList.Items[RunwayList.SelectedIndex].Text;
        }

        private void AircraftList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (AircraftList.SelectedIndex == -1)
                return;
            AircraftCommandView.Enabled = true;
            current_aircraft = GetDataDropDown<AircraftData>(AircraftList);
            RockingWing.Checked = current_aircraft.pilot_properties.ground.other.rocking_wing;
            Engine.Checked = current_aircraft.pilot_properties.ground.engine;

            FlightRulesList.SelectedIndex = FlightRulesList.Items.IndexOf(current_aircraft.pilot_properties.flight.flight_route.flight_rules);
            DestinationList.SelectedIndex = DestinationList.Items.IndexOf(current_aircraft.pilot_properties.flight.flight_route.destination_airport_name);
            string route = current_aircraft.pilot_properties.flight.flight_route.routes_name.Aggregate((i, j) => i + " " + j);
            FlightRouteList.SelectedIndex = FlightRouteList.Items.IndexOf(route);
            RunwayList.SelectedIndex = RunwayList.Items.IndexOf(current_aircraft.pilot_properties.ground.runway.runway);
        }

        #endregion

        #region Taxi

        public void SetTaxiRoute(List<string> routes)
        {
            current_aircraft.pilot_properties.ground.taxi.routes = routes;
        }
        private void TaxiRoute_Click(object sender, EventArgs e)
        {
            taxi_form.Show();
            taxi_form.TaxiToIntersection(current_aircraft.current_position, current_aircraft.heading, RunwayList.Text);
        }

        private void CrossRW_Click(object sender, EventArgs e)
        {
            visual_network.ground.CrossRunway(current_aircraft.callsign);
        }

        private void StartTaxi_Click(object sender, EventArgs e)
        {
            visual_network.ground.StartTaxi(current_aircraft.callsign, current_aircraft.pilot_properties.ground.taxi.routes);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            visual_network.ground.HoldTaxi(current_aircraft.callsign);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            visual_network.ground.ContinueTaxi(current_aircraft.callsign);
        }
        private void ChangeSpeedTaxi_Click(object sender, EventArgs e)
        {
            if (TaxiSpeedList.SelectedIndex == -1)
                return;
            visual_network.ground.TaxiSpeed(current_aircraft.callsign, TaxiSpeedList.SelectedIndex);
        }

        #endregion

        #region Ready and Engine

        private void ReadyAircraft_Click(object sender, EventArgs e)
        {
            visual_network.ground.ReadyAircraft(current_aircraft.callsign);
        }
        private void Engine_Click(object sender, EventArgs e)
        {
            visual_network.ground.EngineStart(current_aircraft.callsign, Engine.Checked);
        }

        #endregion

        #region Pusback

        private void button4_Click(object sender, EventArgs e)
        {
           visual_network.ground.Pushback(current_aircraft.callsign, PushbackInfo.Left);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            visual_network.ground.Pushback(current_aircraft.callsign, PushbackInfo.Right);
        }

        #endregion

        #region U-Turn and Ground Rocking Wing

        private void UTurnLeft_Click(object sender, EventArgs e)
        {
            visual_network.ground.Uturn(current_aircraft.callsign, UturnInfo.Left);
        }

        private void UTurnRight_Click(object sender, EventArgs e)
        {
            visual_network.ground.Uturn(current_aircraft.callsign, UturnInfo.Right);
        }

        private void OneEighty_Click(object sender, EventArgs e)
        {
            visual_network.ground.Uturn(current_aircraft.callsign, UturnInfo.Turn180);
        }

        private void RockingWing_CheckedChanged(object sender, EventArgs e)
        {
            visual_network.ground.GroundRockingWing(current_aircraft.callsign, RockingWing.Checked);
        }

        #endregion

        #region Intersection

        private void TakeOff_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                FlightRouteListModel _routes = GetDataDropDown<FlightRouteListModel>(FlightRouteList);
                List<String> routes = new List<string>();
                foreach (string item in _routes.routes_name)
                    routes.Add(item);
                routes.Add(_routes.destination_airport_name);
                FlightRuleInfo rule = (FlightRuleInfo)Enum.Parse(typeof(FlightRuleInfo), FlightRulesList.Text);
                visual_network.ground.TakeOff(current_aircraft.callsign, routes, RunwayList.Text, (int)rule);
            }
        }

        private void RunwayHold_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                FlightRouteListModel _routes = GetDataDropDown<FlightRouteListModel>(FlightRouteList);
                List<String> routes = new List<string>();
                foreach (string item in _routes.routes_name)
                    routes.Add(item);
                routes.Add(_routes.destination_airport_name);
                FlightRuleInfo rule = (FlightRuleInfo)Enum.Parse(typeof(FlightRuleInfo), FlightRulesList.Text);
                visual_network.ground.HoldTakeOff(current_aircraft.callsign, routes, RunwayList.Text, (int)rule);
            }
        }

        private void ChangeSpeedIntersection_Click(object sender, EventArgs e)
        {
            if (IntersectionSpeedList.SelectedIndex == -1)
                return;
            visual_network.ground.IntersectionSpeed(current_aircraft.callsign, IntersectionSpeedList.SelectedIndex);
        }

        #endregion

        #endregion

        #region Helper

        public T GetDataDropDown<T>(Telerik.WinControls.UI.RadDropDownList item) where T : class, new()
        {
            T returnData = null;
            if (item.SelectedIndex > -1)
            {
                Telerik.WinControls.UI.RadListDataItem data = item.Items[item.SelectedIndex];
                returnData = (T)data.Value;
            }
            return returnData;
        }

        public bool ValidateData()
        {
            if (FlightRulesList.SelectedIndex == -1)
            {
                MessageBox.Show("Flight Rules Cannot Be Empty");
                return false;
            }
            if (DestinationList.SelectedIndex == -1)
            {
                MessageBox.Show("Destination Cannot Be Empty");
                return false;
            }
            if (RunwayList.SelectedIndex == -1)
            {
                MessageBox.Show("Runway Cannot Be Empty");
                return false;
            }
            if (FlightRouteList.SelectedIndex == -1)
            {
                MessageBox.Show("Flight Route Cannot Be Empty");
                return false;
            }

            return true;
        }

        #endregion

        #endregion

        #region Flight Controller


        #endregion

    }
}
