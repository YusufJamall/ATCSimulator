﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ATCSimulator.SampleUDP
{
    public partial class Sample : Form
    {
        string broadcastIP = "192.168.1.255";
        int portUDP = 2222;
        IPEndPoint remoteEndPoint;
        UdpClient client;
        public Sample()
        {
            InitializeComponent();
        }

        private void Sample_Load(object sender, EventArgs e)
        {
            remoteEndPoint = new IPEndPoint(IPAddress.Parse(broadcastIP), portUDP);
            client = new UdpClient();
        }
        private void SendUDP(string message)
        {
            try
            {
                byte[] data = Encoding.UTF8.GetBytes(message);
                client.Send(data, data.Length, remoteEndPoint);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void SendBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SendTextBox.Text))
            {
                SendUDP(SendTextBox.Text);
            }
        }
    }
}
