USE [master]
GO
/****** Object:  Database [ATCSimulatorDB]    Script Date: 04/28/2015 16:27:29 ******/
CREATE DATABASE [ATCSimulatorDB] ON  PRIMARY 
( NAME = N'ATCSimulatorDB_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ATCSimulatorDB_Data.mdf' , SIZE = 9152KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'ATCSimulatorDB_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ATCSimulatorDB_log.ldf' , SIZE = 7168KB , MAXSIZE = 2048GB , FILEGROWTH = 1024KB )
GO
ALTER DATABASE [ATCSimulatorDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ATCSimulatorDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ATCSimulatorDB] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET ARITHABORT OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ATCSimulatorDB] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ATCSimulatorDB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ATCSimulatorDB] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET  DISABLE_BROKER
GO
ALTER DATABASE [ATCSimulatorDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ATCSimulatorDB] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ATCSimulatorDB] SET  READ_WRITE
GO
ALTER DATABASE [ATCSimulatorDB] SET RECOVERY FULL
GO
ALTER DATABASE [ATCSimulatorDB] SET  MULTI_USER
GO
ALTER DATABASE [ATCSimulatorDB] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ATCSimulatorDB] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'ATCSimulatorDB', N'ON'
GO
USE [ATCSimulatorDB]
GO
/****** Object:  ForeignKey [FK_Base.SimulationPlayer_Base.Simulation]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base.SimulationPlayer] DROP CONSTRAINT [FK_Base.SimulationPlayer_Base.Simulation]
GO
/****** Object:  ForeignKey [FK_Waypoint.Ground_Base.Scenery]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Waypoint.Ground] DROP CONSTRAINT [FK_Waypoint.Ground_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Airport_Base.Scenery]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base_Airport] DROP CONSTRAINT [FK_Base.Airport_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Terminal_Base.Scenery]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base.Terminal] DROP CONSTRAINT [FK_Base.Terminal_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Simulation_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Simulation] DROP CONSTRAINT [FK_Base.Simulation_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Exercise_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Exercise] DROP CONSTRAINT [FK_Base.Exercise_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Appron_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Appron] DROP CONSTRAINT [FK_Base.Appron_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Scenery_Base.Airport]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Scenery] DROP CONSTRAINT [FK_Base.Scenery_Base.Airport]
GO
/****** Object:  ForeignKey [FK_Waypoint.DepartureRoute_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.DepartureRoute] DROP CONSTRAINT [FK_Waypoint.DepartureRoute_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Waypoint.DepartureRoute_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.DepartureRoute] DROP CONSTRAINT [FK_Waypoint.DepartureRoute_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Waypoint.ArrivalRoute_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.ArrivalRoute] DROP CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Waypoint.ArrivalRoute_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.ArrivalRoute] DROP CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Appron]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Appron]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Simulation]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Simulation]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.SimulationPlayer]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.SimulationPlayer]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Waypoint.DepartureRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Waypoint.DepartureRoute]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.Simulation]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.Simulation]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Waypoint.ArrivalRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Waypoint.ArrivalRoute]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Appron]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Appron]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Exercise]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Exercise]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Waypoint.DepartureRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Waypoint.DepartureRoute]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.Exercise]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.Exercise]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Waypoint.ArrivalRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Waypoint.ArrivalRoute]
GO
/****** Object:  ForeignKey [FK_Base.Aircraft_Base.AircraftProperties]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Aircraft] DROP CONSTRAINT [FK_Base.Aircraft_Base.AircraftProperties]
GO
/****** Object:  ForeignKey [FK_Prefix.User_Prefix.UserRole]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.User] DROP CONSTRAINT [FK_Prefix.User_Prefix.UserRole]
GO
/****** Object:  ForeignKey [FK_Prefix.UserProfile_Prefix.User]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.UserProfile] DROP CONSTRAINT [FK_Prefix.UserProfile_Prefix.User]
GO
/****** Object:  ForeignKey [FK_Prefix.ListPC_Prefix.User]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.ListPC] DROP CONSTRAINT [FK_Prefix.ListPC_Prefix.User]
GO
/****** Object:  Table [dbo].[Prefix.ListPC]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.ListPC] DROP CONSTRAINT [FK_Prefix.ListPC_Prefix.User]
GO
DROP TABLE [dbo].[Prefix.ListPC]
GO
/****** Object:  Table [dbo].[Prefix.UserProfile]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.UserProfile] DROP CONSTRAINT [FK_Prefix.UserProfile_Prefix.User]
GO
DROP TABLE [dbo].[Prefix.UserProfile]
GO
/****** Object:  Table [dbo].[Prefix.User]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.User] DROP CONSTRAINT [FK_Prefix.User_Prefix.UserRole]
GO
DROP TABLE [dbo].[Prefix.User]
GO
/****** Object:  Table [dbo].[Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Aircraft] DROP CONSTRAINT [FK_Base.Aircraft_Base.AircraftProperties]
GO
DROP TABLE [dbo].[Base.Aircraft]
GO
/****** Object:  Table [dbo].[Base.ExerciseArrival]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.Aircraft]
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.AircraftLivery]
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.Exercise]
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightRules]
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightType]
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Base.Scenery]
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] DROP CONSTRAINT [FK_Base.ExerciseArrival_Waypoint.ArrivalRoute]
GO
DROP TABLE [dbo].[Base.ExerciseArrival]
GO
/****** Object:  Table [dbo].[Base.ExerciseDeparture]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Aircraft]
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.AircraftLivery]
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Appron]
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Exercise]
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightRules]
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightType]
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Base.Scenery]
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] DROP CONSTRAINT [FK_Base.ExerciseDeparture_Waypoint.DepartureRoute]
GO
DROP TABLE [dbo].[Base.ExerciseDeparture]
GO
/****** Object:  Table [dbo].[Base.SimulationArrival]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.Aircraft]
GO
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.AircraftLivery]
GO
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.FlightRules]
GO
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.FlightType]
GO
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.Scenery]
GO
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Base.Simulation]
GO
ALTER TABLE [dbo].[Base.SimulationArrival] DROP CONSTRAINT [FK_Base.SimulationArrival_Waypoint.ArrivalRoute]
GO
DROP TABLE [dbo].[Base.SimulationArrival]
GO
/****** Object:  Table [dbo].[Base.SimulationDeparture]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Aircraft]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.AircraftLivery]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Appron]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightRules]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightType]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Scenery]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.Simulation]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Base.SimulationPlayer]
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] DROP CONSTRAINT [FK_Base.SimulationDeparture_Waypoint.DepartureRoute]
GO
DROP TABLE [dbo].[Base.SimulationDeparture]
GO
/****** Object:  Table [dbo].[Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
DROP TABLE [dbo].[Base.AircraftLivery]
GO
/****** Object:  Table [dbo].[Base.AircraftProperties]    Script Date: 04/28/2015 16:27:30 ******/
DROP TABLE [dbo].[Base.AircraftProperties]
GO
/****** Object:  Table [dbo].[Prefix.UserRole]    Script Date: 04/28/2015 16:27:30 ******/
DROP TABLE [dbo].[Prefix.UserRole]
GO
/****** Object:  Table [dbo].[Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
DROP TABLE [dbo].[Base.FlightRules]
GO
/****** Object:  Table [dbo].[Waypoint.ArrivalRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.ArrivalRoute] DROP CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.FlightRules]
GO
ALTER TABLE [dbo].[Waypoint.ArrivalRoute] DROP CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.Scenery]
GO
DROP TABLE [dbo].[Waypoint.ArrivalRoute]
GO
/****** Object:  Table [dbo].[Waypoint.DepartureRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.DepartureRoute] DROP CONSTRAINT [FK_Waypoint.DepartureRoute_Base.FlightRules]
GO
ALTER TABLE [dbo].[Waypoint.DepartureRoute] DROP CONSTRAINT [FK_Waypoint.DepartureRoute_Base.Scenery]
GO
DROP TABLE [dbo].[Waypoint.DepartureRoute]
GO
/****** Object:  Table [dbo].[Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
DROP TABLE [dbo].[Base.FlightType]
GO
/****** Object:  Table [dbo].[Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Scenery] DROP CONSTRAINT [FK_Base.Scenery_Base.Airport]
GO
DROP TABLE [dbo].[Base.Scenery]
GO
/****** Object:  Table [dbo].[Base.Appron]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Appron] DROP CONSTRAINT [FK_Base.Appron_Base.Scenery]
GO
DROP TABLE [dbo].[Base.Appron]
GO
/****** Object:  Table [dbo].[Base.Exercise]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Exercise] DROP CONSTRAINT [FK_Base.Exercise_Base.Scenery]
GO
DROP TABLE [dbo].[Base.Exercise]
GO
/****** Object:  Table [dbo].[Base.Simulation]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Simulation] DROP CONSTRAINT [FK_Base.Simulation_Base.Scenery]
GO
DROP TABLE [dbo].[Base.Simulation]
GO
/****** Object:  Table [dbo].[Base.Terminal]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base.Terminal] DROP CONSTRAINT [FK_Base.Terminal_Base.Scenery]
GO
DROP TABLE [dbo].[Base.Terminal]
GO
/****** Object:  Table [dbo].[Base_Airport]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base_Airport] DROP CONSTRAINT [FK_Base.Airport_Base.Scenery]
GO
DROP TABLE [dbo].[Base_Airport]
GO
/****** Object:  Table [dbo].[Waypoint.Ground]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Waypoint.Ground] DROP CONSTRAINT [FK_Waypoint.Ground_Base.Scenery]
GO
DROP TABLE [dbo].[Waypoint.Ground]
GO
/****** Object:  Table [dbo].[Base.SimulationPlayer]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base.SimulationPlayer] DROP CONSTRAINT [FK_Base.SimulationPlayer_Base.Simulation]
GO
DROP TABLE [dbo].[Base.SimulationPlayer]
GO
/****** Object:  Table [dbo].[Base.SimulationPlayer]    Script Date: 04/28/2015 16:27:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base.SimulationPlayer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SimulationID] [int] NOT NULL,
	[RolePlayer] [nvarchar](50) NOT NULL,
	[UserID] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Point] [int] NULL,
	[Grade] [nvarchar](50) NULL,
	[EditedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Base.SimulationPlayer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Base.SimulationPlayer] ON
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (60, 1, N'Pseudopilot1', 7, N'Asmiranda ', 46, N'Deficient', N'teguhnoor')
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (61, 1, N'Pseudopilot2', 6, N'Aliah S Winarsih', 46, N'Deficient', N'teguhnoor')
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (62, 1, N'APPController1', 8, N'Tegar Amal Firmansyah', 60, N'Sufficient', N'teguhnoor')
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (63, 2, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (64, 2, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (65, 2, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (66, 3, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (67, 3, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (68, 3, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (69, 4, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (70, 4, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (71, 4, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (72, 5, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (73, 5, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (74, 5, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (75, 6, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (76, 6, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (77, 6, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (78, 7, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (79, 7, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (80, 7, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (81, 8, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (82, 8, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (83, 8, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (84, 9, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (85, 9, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (86, 9, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (87, 10, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (88, 10, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (89, 10, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (90, 11, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (91, 11, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (92, 11, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (93, 12, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (94, 12, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (95, 12, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (96, 13, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (97, 13, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (98, 13, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (99, 14, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (100, 14, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (101, 14, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (102, 15, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (103, 15, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (104, 15, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (105, 16, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (106, 16, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (107, 16, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (108, 17, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (109, 17, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (110, 17, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (111, 18, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (112, 18, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (113, 18, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (114, 19, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (115, 19, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (116, 19, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (117, 20, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (118, 20, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (119, 20, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (120, 21, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (121, 21, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (122, 21, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (123, 22, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (124, 22, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (125, 22, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (126, 23, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (127, 23, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (128, 23, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (129, 24, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (130, 24, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (131, 24, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (132, 25, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (133, 25, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (134, 25, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (135, 26, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (136, 26, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (137, 26, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (138, 27, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (139, 27, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (140, 27, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (141, 28, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (142, 28, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (143, 28, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (144, 29, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (145, 29, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (146, 29, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (147, 30, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (148, 30, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (149, 30, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (150, 31, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (151, 31, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (152, 31, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (153, 32, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (154, 32, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (155, 32, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (156, 33, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (157, 33, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (158, 33, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (159, 34, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (160, 34, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (161, 34, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (162, 35, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (163, 35, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (164, 35, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (165, 36, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (166, 36, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (167, 36, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (168, 37, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (169, 37, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (170, 37, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (171, 38, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (172, 38, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (173, 38, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (174, 39, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (175, 39, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (176, 39, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (177, 40, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (178, 40, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (179, 40, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (180, 41, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (181, 41, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (182, 41, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (183, 42, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (184, 42, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (185, 42, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (186, 43, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (187, 43, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (188, 43, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (189, 44, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (190, 44, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (191, 44, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (192, 45, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (193, 45, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (194, 45, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (195, 46, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (196, 46, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (197, 46, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (198, 47, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (199, 47, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (200, 47, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (201, 48, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (202, 48, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (203, 48, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (204, 49, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (205, 49, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (206, 49, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (207, 50, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (208, 50, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (209, 50, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (210, 51, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (211, 51, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (212, 51, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (213, 52, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (214, 52, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (215, 52, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (216, 53, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (217, 53, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (218, 53, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (219, 54, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (220, 54, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (221, 54, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (222, 55, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (223, 55, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (224, 55, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (225, 56, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (226, 56, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (227, 56, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (228, 57, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (229, 57, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (230, 57, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (231, 58, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (232, 58, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (233, 58, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (234, 59, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (235, 59, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (236, 59, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (237, 60, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (238, 60, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (239, 60, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (240, 61, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (241, 61, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (242, 61, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (243, 62, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (244, 62, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (245, 62, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (246, 63, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (247, 63, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (248, 63, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (249, 64, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (250, 64, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (251, 64, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (252, 65, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (253, 65, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (254, 65, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (255, 66, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (256, 66, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (257, 66, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (258, 67, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (259, 67, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (260, 67, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (261, 68, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (262, 68, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (263, 68, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (264, 69, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (265, 69, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (266, 69, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (267, 70, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (268, 70, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (269, 70, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (270, 71, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (271, 71, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (272, 71, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (273, 72, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (274, 72, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (275, 72, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (276, 73, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (277, 73, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (278, 73, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (279, 74, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (280, 74, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (281, 74, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (282, 75, N'Pseudopilot1', 7, N'Asmiranda ', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (283, 75, N'Pseudopilot2', 6, N'Aliah S Winarsih', NULL, NULL, NULL)
INSERT [dbo].[Base.SimulationPlayer] ([ID], [SimulationID], [RolePlayer], [UserID], [UserName], [Point], [Grade], [EditedBy]) VALUES (284, 75, N'APPController1', 8, N'Tegar Amal Firmansyah', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Base.SimulationPlayer] OFF
/****** Object:  Table [dbo].[Waypoint.Ground]    Script Date: 04/28/2015 16:27:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Waypoint.Ground](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsIntersection] [bit] NOT NULL,
	[IsParking] [bit] NOT NULL,
	[IsCrossRunway] [bit] NOT NULL,
 CONSTRAINT [PK_Waypoint.Ground] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Waypoint.Ground] ON
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (1, 1, N'0', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (2, 1, N'1', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (3, 1, N'2', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (4, 1, N'3', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (5, 1, N'4', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (6, 1, N'5', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (7, 1, N'6', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (8, 1, N'7', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (9, 1, N'8', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (10, 1, N'9', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (11, 1, N'10', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (12, 1, N'11', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (13, 1, N'12', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (14, 1, N'13', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (15, 1, N'14', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (16, 1, N'15', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (17, 1, N'16', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (18, 1, N'17', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (19, 1, N'18', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (20, 1, N'19', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (21, 1, N'20', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (22, 1, N'21', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (23, 1, N'22', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (24, 1, N'23', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (25, 1, N'24', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (26, 1, N'25', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (27, 1, N'26', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (28, 1, N'27', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (29, 1, N'28', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (30, 1, N'29', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (31, 1, N'30', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (32, 1, N'31', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (33, 1, N'32', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (34, 1, N'33', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (35, 1, N'34', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (36, 1, N'35', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (37, 1, N'36', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (38, 1, N'37', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (39, 1, N'38', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (40, 1, N'39', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (41, 1, N'40', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (42, 1, N'41', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (43, 1, N'42', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (44, 1, N'43', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (45, 1, N'44', 0, 0, 0)
INSERT [dbo].[Waypoint.Ground] ([ID], [SceneryID], [Name], [IsIntersection], [IsParking], [IsCrossRunway]) VALUES (46, 1, N'45', 0, 0, 0)
SET IDENTITY_INSERT [dbo].[Waypoint.Ground] OFF
/****** Object:  Table [dbo].[Base_Airport]    Script Date: 04/28/2015 16:27:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Airport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[AirportCode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Base.Airport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Airport] ON
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (1, 1, N'SEAHORSE', N'SASH')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (2, 1, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (3, 1, N'EASTTOWN', N'SAEN')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (4, 1, N'NORTHSTAR', N'SANS')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (5, 1, N'SANDSHARK', N'SASK')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (6, 1, N'SEAGULL', N'SAGL')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (7, 1, N'SOUTHVIEW', N'SAUW')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (9, 1, N'STARFISH', N'SATF')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (13, 1, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Base_Airport] ([ID], [SceneryID], [Name], [AirportCode]) VALUES (14, 1, N'WESTPOINT', N'SAPT')
SET IDENTITY_INSERT [dbo].[Base_Airport] OFF
/****** Object:  Table [dbo].[Base.Terminal]    Script Date: 04/28/2015 16:27:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.Terminal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Base.Terminal] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.Terminal] ON
INSERT [dbo].[Base.Terminal] ([ID], [SceneryID], [Name]) VALUES (1, 1, N'International')
INSERT [dbo].[Base.Terminal] ([ID], [SceneryID], [Name]) VALUES (2, 1, N'Domestic')
INSERT [dbo].[Base.Terminal] ([ID], [SceneryID], [Name]) VALUES (3, 1, N'East')
INSERT [dbo].[Base.Terminal] ([ID], [SceneryID], [Name]) VALUES (4, 1, N'West')
INSERT [dbo].[Base.Terminal] ([ID], [SceneryID], [Name]) VALUES (5, 1, N'Military')
SET IDENTITY_INSERT [dbo].[Base.Terminal] OFF
/****** Object:  Table [dbo].[Base.Simulation]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base.Simulation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExerciseName] [nvarchar](50) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[SceneryName] [nvarchar](150) NOT NULL,
	[PlayTime] [time](7) NOT NULL,
	[ElapsedTime] [time](7) NULL,
	[Weather] [nvarchar](50) NOT NULL,
	[WindDirection] [int] NOT NULL,
	[WindSpeed] [int] NOT NULL,
	[Temperature] [int] NOT NULL,
	[Visibility] [int] NOT NULL,
	[TimeSimulation] [time](7) NOT NULL,
	[RunwayLights] [text] NOT NULL,
	[Taxi] [bit] NOT NULL,
	[Papi] [bit] NOT NULL,
	[Approach] [text] NOT NULL,
	[TotalArrival] [int] NOT NULL,
	[TotalDeparture] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_BaseSimulation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Base.Simulation] ON
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (1, N'dummy stop exercise', 1, N'SEAHORSE', CAST(0x0700BCA065010000 AS Time), CAST(0x07D532F001000000 AS Time), N'ThunderStorm', 0, 0, 0, 0, CAST(0x070006E297610000 AS Time), N'{"34R-16L":false,"34L-16R":false,"07-25":false}', 0, 0, N'{"34L":false,"34R":false,"16R":false,"16L":false,"07":false,"25":false}', 0, 2, CAST(0x0000A46C00F1C2D4 AS DateTime), N'teguhnoor')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (2, N'dummy stop exercise', 1, N'SEAHORSE', CAST(0x0700BCA065010000 AS Time), NULL, N'ThunderStorm', 0, 0, 0, 0, CAST(0x070006E297610000 AS Time), N'{"34R-16L":false,"34L-16R":false,"07-25":false}', 0, 0, N'{"34L":false,"34R":false,"16R":false,"16L":false,"07":false,"25":false}', 0, 2, CAST(0x0000A46E00DC34A0 AS DateTime), N'teguhnoor')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (3, N'dummy stop exercise', 1, N'SEAHORSE', CAST(0x0700BCA065010000 AS Time), NULL, N'ThunderStorm', 0, 0, 0, 0, CAST(0x070006E297610000 AS Time), N'{"34R-16L":false,"34L-16R":false,"07-25":false}', 0, 0, N'{"34L":false,"34R":false,"16R":false,"16L":false,"07":false,"25":false}', 0, 2, CAST(0x0000A46E00DD1A22 AS DateTime), N'teguhnoor')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (4, N'dummy stop exercise', 1, N'SEAHORSE', CAST(0x0700BCA065010000 AS Time), CAST(0x07BFCECA65010000 AS Time), N'ThunderStorm', 0, 0, 0, 0, CAST(0x070006E297610000 AS Time), N'{"34R-16L":false,"34L-16R":false,"07-25":false}', 0, 0, N'{"34L":false,"34R":false,"16R":false,"16L":false,"07":false,"25":false}', 0, 2, CAST(0x0000A46E00DDE5D2 AS DateTime), N'teguhnoor')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (5, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E0105D11B AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (6, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E0106122A AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (7, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E010BC46A AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (8, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E010C9AC3 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (9, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E010F367B AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (10, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x076130143B000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E01128804 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (11, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E01131335 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (12, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x070AA02924940000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46E01172E99 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (13, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F00AF5926 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (14, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0744682E62080000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F00B0F78E AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (15, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F0103C9B0 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (16, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F0104AF93 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (17, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07400FF730000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F0105E892 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (18, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x075C8CDA1B000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F0106573C AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (19, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07DD26672C000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F01069892 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (20, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x071E4F3504000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F01071C6B AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (21, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F01105EEF AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (22, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07B84FCA59000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F01123A03 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (23, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07CFF6C048000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F011C20B4 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (24, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F011D7F11 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (25, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07989C5450000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F01223FE3 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (26, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07C9A53C26000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F01245806 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (27, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0783C7C70C000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F01251C84 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (28, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0709E3F70A000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F0127217A AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (29, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x070A595B22000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F0127AB09 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (30, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 10000, CAST(0x070074053F470000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 1, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F012C2363 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (31, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0709F70D3F010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x07806DC9BF510000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A46F012DB520 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (32, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x072FF2D626000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780DB89D0710000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A471016CCDFE AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (33, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x078C85A517000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070092DE8F720000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A471016E4498 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (34, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x0700402BBD720000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A471016E9028 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (35, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07124DA1E1000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700724F02730000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A471016F1982 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (36, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x078D3016D3000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780A9BFED730000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4710170E9A8 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (37, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x0700C71823750000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4710173A9C4 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (38, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0731122BA8010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700B3D3A6770000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47101783F51 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (39, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0727895D28010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780478FB1790000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A471017C3C53 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (40, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x078AE91DFB000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x070084F2E07A0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A471017E9274 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (41, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0786DBEC14010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780A3826E7C0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4710181A008 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (42, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x076BAB0053000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780D39C8C7D0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4710183D599 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (43, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x0780E3FAEB7D0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47101848D0D AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (44, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0796C9FD08020000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700FBAE187E0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4710184FBE6 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (45, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0736654EC6000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700F61537800000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4710189161B AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (46, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07A9ED9641010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x07006D780C810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A471018AC055 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (47, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07B7FF9A76010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700863760820000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4720001D8EB AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (48, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x0780C4D1E7830000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4720004CEB6 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (49, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x079549E907000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700E48EA6860000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A472000A40E0 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (50, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07D91FE082000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780FEFEBE860000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A472000A64AD AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (51, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x073151B2DB000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700DAD54F870000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A472000B81B2 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (52, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x07006E8CE2B30000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200632265 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (53, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x070B639B86000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x078085400FB40000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200637B1B AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (54, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x0700DA839CB40000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200649028 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (55, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x070090D95BB70000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A4720069F917 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (56, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x07807F0390BC0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200743225 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (57, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x07003D55BABC0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A472007484B3 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (58, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x071616ED02000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700EC8D52010000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A472008F922E AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (59, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07695ADF0A000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x07805DA481010000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A472008FF246 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (60, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07084CA86A000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x07800835C3010000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200907129 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (61, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0742174109000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780149B22120000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200B0A329 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (62, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x0711695F84000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700DD5768120000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200B12B21 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (63, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07E4FEAF22000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700793C0F130000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200B277B6 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (64, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x073AF3820D000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700AE1C40130000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200B2E285 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (65, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x078315A420010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x07004745FB130000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200B443FF AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (66, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07E9FEA966020000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780C813EC160000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200BA0A37 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (67, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x079BB4B32D000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700768E4A1A0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200C0C3E3 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (68, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07119434F4010000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x07004E508D1A0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200C12D80 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (69, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x079B6F7D3F000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780998C881C0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200C5124A AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (70, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x07008F7ACF1C0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200C5B23C AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (71, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x0700D790992B0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200E2B754 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (72, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), NULL, N'Clear', 0, 0, 0, 0, CAST(0x0700F022BC2B0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200E2FA46 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (73, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x072A9BC03C000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x07804466492C0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200E40E30 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (74, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07AE59AA17000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0780F7A5BA2C0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200E516B3 AS DateTime), N'ilyas')
INSERT [dbo].[Base.Simulation] ([ID], [ExerciseName], [SceneryID], [SceneryName], [PlayTime], [ElapsedTime], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreatedDate], [CreatedBy]) VALUES (75, N'abc', 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), CAST(0x07CDFC2B5C000000 AS Time), N'Clear', 0, 0, 0, 0, CAST(0x0700C062002D0000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A47200E578B5 AS DateTime), N'ilyas')
SET IDENTITY_INSERT [dbo].[Base.Simulation] OFF
/****** Object:  Table [dbo].[Base.Exercise]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.Exercise](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[SceneryName] [varchar](150) NOT NULL,
	[PlayTime] [time](7) NOT NULL,
	[ExerciseName] [varchar](150) NOT NULL,
	[TrafficExercise] [varchar](250) NULL,
	[DescExercise] [varchar](250) NULL,
	[Weather] [varchar](50) NOT NULL,
	[WindDirection] [int] NOT NULL,
	[WindSpeed] [int] NOT NULL,
	[Temperature] [int] NOT NULL,
	[Visibility] [int] NOT NULL,
	[TimeSimulation] [time](7) NOT NULL,
	[RunwayLights] [text] NOT NULL,
	[Taxi] [bit] NOT NULL,
	[Papi] [bit] NOT NULL,
	[Approach] [text] NOT NULL,
	[TotalArrival] [int] NOT NULL,
	[TotalDeparture] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [varchar](50) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [varchar](50) NULL,
 CONSTRAINT [PK_Base.Exercise] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.Exercise] ON
INSERT [dbo].[Base.Exercise] ([ID], [SceneryID], [SceneryName], [PlayTime], [ExerciseName], [TrafficExercise], [DescExercise], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (10, 1, N'SEAHORSE', CAST(0x070068C461080000 AS Time), N'abc', N'asd', N'asd', N'Clear', 0, 0, 0, 0, CAST(0x070099247A810000 AS Time), N'{"34R-16L":true,"34L-16R":false,"07-25":false}', 0, 1, N'{"34L":false,"34R":false,"16R":true,"16L":true,"07":false,"25":true}', 0, 3, CAST(0x0000A42D0054976D AS DateTime), N'ilyas', CAST(0x0000A46300AD8947 AS DateTime), N'teguhnoor')
INSERT [dbo].[Base.Exercise] ([ID], [SceneryID], [SceneryName], [PlayTime], [ExerciseName], [TrafficExercise], [DescExercise], [Weather], [WindDirection], [WindSpeed], [Temperature], [Visibility], [TimeSimulation], [RunwayLights], [Taxi], [Papi], [Approach], [TotalArrival], [TotalDeparture], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (12, 1, N'SEAHORSE', CAST(0x0700BCA065010000 AS Time), N'dummy stop exercise', N'departure', N'abcd', N'ThunderStorm', 0, 0, 0, 0, CAST(0x070006E297610000 AS Time), N'{"34R-16L":false,"34L-16R":false,"07-25":false}', 0, 0, N'{"34L":false,"34R":false,"16R":false,"16L":false,"07":false,"25":false}', 0, 2, CAST(0x0000A46C0007EF14 AS DateTime), N'teguhnoor', CAST(0x0000A46C009F6E5B AS DateTime), N'teguhnoor')
SET IDENTITY_INSERT [dbo].[Base.Exercise] OFF
/****** Object:  Table [dbo].[Base.Appron]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.Appron](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[TerminalName] [varchar](50) NOT NULL,
	[AppronName] [varchar](50) NOT NULL,
	[CanPushback] [bit] NOT NULL,
	[PushbackLeft] [bit] NOT NULL,
	[PushbackRight] [bit] NOT NULL,
 CONSTRAINT [PK_Base.Appron] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.Appron] ON
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (1, 1, N'International', N'A1', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (2, 1, N'International', N'A2', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (3, 1, N'International', N'A3', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (4, 1, N'International', N'A4', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (5, 1, N'International', N'A5', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (6, 1, N'International', N'A6', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (7, 1, N'International', N'A7', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (8, 1, N'International', N'A8', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (9, 1, N'Domestic', N'B1', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (10, 1, N'Domestic', N'B2', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (11, 1, N'Domestic', N'B3', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (12, 1, N'Domestic', N'B4', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (13, 1, N'Domestic', N'B5', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (14, 1, N'Domestic', N'B6', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (15, 1, N'Domestic', N'B7', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (16, 1, N'Domestic', N'B8', 1, 1, 1)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (17, 1, N'East', N'E1', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (18, 1, N'East', N'E2', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (19, 1, N'East', N'E3', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (20, 1, N'East', N'E4', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (21, 1, N'East', N'E5', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (22, 1, N'East', N'E6', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (23, 1, N'East', N'E7', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (24, 1, N'West', N'W1', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (25, 1, N'West', N'W2', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (26, 1, N'West', N'W3', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (27, 1, N'West', N'W4', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (28, 1, N'West', N'W5', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (29, 1, N'West', N'W6', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (30, 1, N'West', N'W7', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (31, 1, N'Military', N'M1', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (32, 1, N'Military', N'M2', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (33, 1, N'Military', N'M3', 0, 0, 0)
INSERT [dbo].[Base.Appron] ([ID], [SceneryID], [TerminalName], [AppronName], [CanPushback], [PushbackLeft], [PushbackRight]) VALUES (34, 1, N'Military', N'M4', 0, 0, 0)
SET IDENTITY_INSERT [dbo].[Base.Appron] OFF
/****** Object:  Table [dbo].[Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.Scenery](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AirportID] [int] NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[RunwayLight] [text] NOT NULL,
	[Taxi] [bit] NOT NULL,
	[Papi] [bit] NOT NULL,
	[ApproachLight] [text] NOT NULL,
 CONSTRAINT [PK_Base.Scenery] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.Scenery] ON
INSERT [dbo].[Base.Scenery] ([ID], [AirportID], [Name], [RunwayLight], [Taxi], [Papi], [ApproachLight]) VALUES (1, 1, N'SEAHORSE', N'["34R-16L","34L-16R","07-25"]', 1, 1, N'["34L","34R","16R","16L","07","25"]')
SET IDENTITY_INSERT [dbo].[Base.Scenery] OFF
/****** Object:  Table [dbo].[Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.FlightType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](150) NOT NULL,
 CONSTRAINT [PK_Base.FlightType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.FlightType] ON
INSERT [dbo].[Base.FlightType] ([ID], [Name], [Description]) VALUES (1, N'S', N'Scheduled Services')
INSERT [dbo].[Base.FlightType] ([ID], [Name], [Description]) VALUES (2, N'N', N'Non-Scheduled')
INSERT [dbo].[Base.FlightType] ([ID], [Name], [Description]) VALUES (3, N'G', N'General Aviation')
INSERT [dbo].[Base.FlightType] ([ID], [Name], [Description]) VALUES (4, N'M', N'Military')
INSERT [dbo].[Base.FlightType] ([ID], [Name], [Description]) VALUES (5, N'X', N'Other')
SET IDENTITY_INSERT [dbo].[Base.FlightType] OFF
/****** Object:  Table [dbo].[Waypoint.DepartureRoute]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Waypoint.DepartureRoute](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[RunwayUsed] [varchar](150) NOT NULL,
	[FlightRulesID] [int] NOT NULL,
	[FlightRules] [varchar](50) NOT NULL,
	[OriginAirportID] [int] NOT NULL,
	[OriginAirportCode] [varchar](10) NOT NULL,
	[OriginAirportName] [varchar](50) NOT NULL,
	[DestAirportID] [int] NOT NULL,
	[DestAirportName] [varchar](50) NOT NULL,
	[DestAirportCode] [varchar](50) NOT NULL,
	[EndPointName] [varchar](50) NOT NULL,
	[IsLocalFlight] [bit] NOT NULL,
	[IsEndPointAirport] [bit] NOT NULL,
	[RoutesName] [text] NOT NULL,
	[TotalDistance] [float] NOT NULL,
	[AlternateAirportID] [int] NOT NULL,
	[AlternateAirportName] [varchar](50) NOT NULL,
	[AlternateAirportCode] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Waypoint.DepartureRoute] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Waypoint.DepartureRoute] ON
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (1, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 14, N'WESTPOINT', N'SAPT', N'AMBER', 0, 0, N'["VOR","AMBER","PRADA"]', 398.53678669641704, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (2, 1, N'["16R"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 14, N'WESTPOINT', N'SAPT', N'AMBER', 0, 0, N'["SH","BT","AMBER","PRADA"]', 396.63290264466445, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (3, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 7, N'SOUTHVIEW', N'SAUW', N'POLAR', 0, 0, N'["VOR","BT"]', 373.76965828262485, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (4, 1, N'["16R"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 7, N'SOUTHVIEW', N'SAUW', N'POLAR', 0, 0, N'["SH","BT"]', 347.80875110520753, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (5, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 5, N'SANDSHARK', N'SASK', N'ECTOR', 0, 0, N'["VOR","BT","ECTOR","MAHAR","SIMON"]', 705.7853094994249, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (6, 1, N'["16R"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 5, N'SANDSHARK', N'SASK', N'ECTOR', 0, 0, N'["SH","BT","ECTOR","MAHAR","SIMON"]', 679.82440232200759, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (7, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 6, N'SEAGULL', N'SAGL', N'DORIN', 0, 0, N'["VOR","DORIN","CANEE","GOLAN"]', 768.30468778622844, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (8, 1, N'["16R"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 6, N'SEAGULL', N'SAGL', N'DORIN', 0, 0, N'["SH","DORIN","CANEE","GOLAN"]', 730.78562485756015, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (9, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 3, N'EASTTOWN', N'SAEN', N'CORAL', 0, 0, N'["VOR","TF"]', 343.19423477455427, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (10, 1, N'["16R"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 3, N'EASTTOWN', N'SAEN', N'CORAL', 0, 0, N'["SH","TF"]', 323.53533308851053, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (11, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 9, N'STARFISH', N'SATF', N'COMET', 0, 0, N'["VOR","COMET","ARDEE"]', 426.9328955071328, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (12, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 9, N'STARFISH', N'SATF', N'COMET', 0, 0, N'["VOR","TF","COMET","ARDEE"]', 447.32492711449368, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (13, 1, N'["16R"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 9, N'STARFISH', N'SATF', N'COMET', 0, 0, N'["SH","TF","COMET","ARDEE"]', 427.66602542844987, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (14, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 4, N'NORTHSTAR', N'SANS', N'BURSA', 0, 0, N'["VOR","BURSA","AMINA","NIKEL"]', 663.10267935481784, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (15, 1, N'["34L"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 4, N'NORTHSTAR', N'SANS', N'BURSA', 0, 0, N'["VOR","TF","BURSA","AMINA","NIKEL"]', 729.265310282758, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (16, 1, N'["16R"]', 1, N'IFR', 1, N'SASH', N'SEAHORSE', 4, N'NORTHSTAR', N'SANS', N'BURSA', 0, 0, N'["SH","TF","BURSA","AMINA","NIKEL"]', 709.60640859671412, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (17, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 14, N'WESTPOINT', N'SAPT', N'AMBER', 0, 0, N'["ALPHA","WEBER","AMBER","PRADA"]', 385.63305376733513, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (18, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 14, N'WESTPOINT', N'SAPT', N'AMBER', 0, 0, N'["ALPHA","BT","WEBER","AMBER","PRADA"]', 450.27578877992994, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (19, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 7, N'SOUTHVIEW', N'SAUW', N'POLAR', 0, 0, N'["ALPHA","BT"]', 342.51117370339875, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (20, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 5, N'SANDSHARK', N'SASK', N'ECTOR', 0, 0, N'["ALPHA","BT","ECTOR","MAHAR","SIMON"]', 674.52571940351845, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (21, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 6, N'SEAGULL', N'SAGL', N'DORIN', 0, 0, N'["BRAVO","TF","CANEE","GOLAN"]', 745.75120165400767, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (22, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 6, N'SEAGULL', N'SAGL', N'DORIN', 0, 0, N'["BRAVO","ESTER","CANEE","GOLAN"]', 724.41478776385145, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (23, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 3, N'EASTTOWN', N'SAEN', N'CORAL', 0, 0, N'["BRAVO","TF","CORAL"]', 316.95781228054159, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (24, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 9, N'STARFISH', N'SATF', N'COMET', 0, 0, N'["BRAVO","TF","COMET","ARDEE"]', 421.08843877506865, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (25, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 4, N'NORTHSTAR', N'SANS', N'BURSA', 0, 0, N'["BRAVO","TF","BURSA","AMINA","NIKEL"]', 703.028821943333, 13, N'TIGERFORT', N'SAFT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (26, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 4, N'NORTHSTAR', N'SANS', N'BURSA', 0, 0, N'["ALPHA","BT","WEBER","BURSA","AMINA","NIKEL"]', 713.32628518362253, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (27, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 1, N'SASH', N'SEAHORSE', 4, N'NORTHSTAR', N'SANS', N'BURSA', 0, 0, N'["ALPHA","WEBER","BURSA","AMINA","NIKEL"]', 707.62388705126887, 2, N'BRIGHTSTAR', N'SABT')
INSERT [dbo].[Waypoint.DepartureRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestAirportID], [DestAirportName], [DestAirportCode], [EndPointName], [IsLocalFlight], [IsEndPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportName], [AlternateAirportCode]) VALUES (36, 1, N'["34L","34R","16R","16L","07","25"]', 3, N'Local', 1, N'SASH', N'SEAHORSE', 1, N'SEAHORSE', N'SASH', N'LOCAL', 1, 1, N'["LOCAL"]', 0, 1, N'SEAHORSE', N'SASH')
SET IDENTITY_INSERT [dbo].[Waypoint.DepartureRoute] OFF
/****** Object:  Table [dbo].[Waypoint.ArrivalRoute]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Waypoint.ArrivalRoute](
	[ID] [int] NOT NULL,
	[SceneryID] [int] NOT NULL,
	[RunwayUsed] [varchar](150) NOT NULL,
	[FlightRulesID] [int] NOT NULL,
	[FlightRules] [varchar](50) NOT NULL,
	[OriginAirportID] [int] NOT NULL,
	[OriginAirportCode] [varchar](10) NOT NULL,
	[OriginAirportName] [varchar](50) NOT NULL,
	[DestinationAirportID] [int] NOT NULL,
	[DestinationAirportCode] [varchar](10) NOT NULL,
	[DestinationAirportName] [varchar](50) NOT NULL,
	[IsDestinationScenery] [bit] NOT NULL,
	[StartPointName] [varchar](50) NOT NULL,
	[IsStartPointAirport] [bit] NOT NULL,
	[RoutesName] [text] NOT NULL,
	[TotalDistance] [float] NOT NULL,
	[AlternateAirportID] [int] NOT NULL,
	[AlternateAirportCode] [varchar](10) NOT NULL,
	[AlternateAirportName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Waypoint.ArrivalRoute] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (1, 1, N'["16R"]', 1, N'IFR', 14, N'SAPT', N'WESTPOINT', 1, N'SASH', N'SEAHORSE', 1, N'AMBER', 0, N'["PRADA","AMBER","VOR"]', 398.53678669641704, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (2, 1, N'["34L"]', 1, N'IFR', 14, N'SAPT', N'WESTPOINT', 1, N'SASH', N'SEAHORSE', 1, N'AMBER', 0, N'["PRADA","AMBER","BT","SH"]', 396.63290264466445, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (3, 1, N'["16R"]', 1, N'IFR', 7, N'SAUW', N'SOUTHVIEW', 1, N'SASH', N'SEAHORSE', 1, N'POLAR', 0, N'["BT","VOR"]', 373.76965828262485, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (4, 1, N'["34L"]', 1, N'IFR', 7, N'SAUW', N'SOUTHVIEW', 1, N'SASH', N'SEAHORSE', 1, N'POLAR', 0, N'["BT","SH"]', 347.80875110520753, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (5, 1, N'["16R"]', 1, N'IFR', 5, N'SASK', N'SANDSHARK', 1, N'SASH', N'SEAHORSE', 1, N'ECTOR', 0, N'["SIMON","MAHAR","ECTOR","BT","VOR"]', 705.7853094994249, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (6, 1, N'["34L"]', 1, N'IFR', 5, N'SASK', N'SANDSHARK', 1, N'SASH', N'SEAHORSE', 1, N'ECTOR', 0, N'["SIMON","MAHAR","ECTOR","SH"]', 643.54179898068378, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (7, 1, N'["16R"]', 1, N'IFR', 6, N'SAGL', N'SEAGULL', 1, N'SASH', N'SEAHORSE', 1, N'DORIN', 0, N'["GOLAN","CANEE","DORIN","VOR"]', 768.30468778622833, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (8, 1, N'["34L"]', 1, N'IFR', 6, N'SAGL', N'SEAGULL', 1, N'SASH', N'SEAHORSE', 1, N'DORIN', 0, N'["GOLAN","CANEE","DORIN","SH"]', 730.78562485756, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (9, 1, N'["16R"]', 1, N'IFR', 3, N'SAEN', N'EASTTOWN', 1, N'SASH', N'SEAHORSE', 1, N'CORAL', 0, N'["TF","VOR"]', 343.19423477455433, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (10, 1, N'["34L"]', 1, N'IFR', 3, N'SAEN', N'EASTTOWN', 1, N'SASH', N'SEAHORSE', 1, N'CORAL', 0, N'["TF","SH"]', 323.53533308851053, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (11, 1, N'["16R"]', 1, N'IFR', 9, N'SATF', N'STARFISH', 1, N'SASH', N'SEAHORSE', 1, N'COMET', 0, N'["ARDEE","COMET","VOR"]', 426.9328955071328, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (12, 1, N'["16R"]', 1, N'IFR', 9, N'SATF', N'STARFISH', 1, N'SASH', N'SEAHORSE', 1, N'COMET', 0, N'["ARDEE","COMET","TF","VOR"]', 447.32492711449368, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (13, 1, N'["34L"]', 1, N'IFR', 9, N'SATF', N'STARFISH', 1, N'SASH', N'SEAHORSE', 1, N'COMET', 0, N'["ARDEE","COMET","TF","SH"]', 427.66602542844987, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (14, 1, N'["16R"]', 1, N'IFR', 4, N'SANS', N'NORTHSTAR', 1, N'SASH', N'SEAHORSE', 1, N'BURSA', 0, N'["NIKEL","AMINA","BURSA","VOR"]', 663.102679354818, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (15, 1, N'["16R"]', 1, N'IFR', 4, N'SANS', N'NORTHSTAR', 1, N'SASH', N'SEAHORSE', 1, N'BURSA', 0, N'["NIKEL","AMINA","BURSA","TF","VOR"]', 729.26531028275792, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (16, 1, N'["34L"]', 1, N'IFR', 4, N'SANS', N'NORTHSTAR', 1, N'SASH', N'SEAHORSE', 1, N'BURSA', 0, N'["NIKEL","AMINA","BURSA","TF","SH"]', 709.60640859671412, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (17, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 14, N'SAPT', N'WESTPOINT', 1, N'SASH', N'SEAHORSE', 1, N'AMBER', 0, N'["PRADA","AMBER","WEBER","ALPHA"]', 385.63327401348965, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (18, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 14, N'SAPT', N'WESTPOINT', 1, N'SASH', N'SEAHORSE', 1, N'AMBER', 0, N'["PRADA","AMBER","WEBER","BT","ALPHA"]', 391.33567214584326, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (19, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 7, N'SAUW', N'SOUTHVIEW', 1, N'SASH', N'SEAHORSE', 1, N'POLAR', 0, N'["BT","ALPHA"]', 342.51117370339875, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (20, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 5, N'SASK', N'SANDSHARK', 1, N'SASH', N'SEAHORSE', 1, N'ECTOR', 0, N'["SIMON","MAHAR","ECTOR","BT","ALPHA"]', 674.52682492019881, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (21, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 6, N'SAGL', N'SEAGULL', 1, N'SASH', N'SEAHORSE', 1, N'DORIN', 0, N'["GOLAN","CANEE","DORIN","TF","BRAVO"]', 748.03278257941679, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (22, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 6, N'SAGL', N'SEAGULL', 1, N'SASH', N'SEAHORSE', 1, N'DORIN', 0, N'["GOLAN","CANEE","DORIN","ESTER","BRAVO"]', 725.94876489111209, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (23, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 3, N'SAEN', N'EASTTOWN', 1, N'SASH', N'SEAHORSE', 1, N'CORAL', 0, N'["CORAL","TF","BRAVO"]', 316.95781228054159, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (24, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 9, N'SATF', N'STARFISH', 1, N'SASH', N'SEAHORSE', 1, N'COMET', 0, N'["ARDEE","COMET","TF","BRAVO"]', 421.08843877506865, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (25, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 4, N'SANS', N'NORTHSTAR', 1, N'SASH', N'SEAHORSE', 1, N'BURSA', 0, N'["NIKEL","AMINA","BURSA","TF","BRAVO"]', 703.028821943333, 13, N'SAFT', N'TIGERFORT')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (26, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 4, N'SANS', N'NORTHSTAR', 1, N'SASH', N'SEAHORSE', 1, N'BURSA', 0, N'["NIKEL","AMINA","BURSA","KOSER","WEBER","BT","ALPHA"]', 713.63911233454837, 2, N'SABT', N'BRIGHTSTAR')
INSERT [dbo].[Waypoint.ArrivalRoute] ([ID], [SceneryID], [RunwayUsed], [FlightRulesID], [FlightRules], [OriginAirportID], [OriginAirportCode], [OriginAirportName], [DestinationAirportID], [DestinationAirportCode], [DestinationAirportName], [IsDestinationScenery], [StartPointName], [IsStartPointAirport], [RoutesName], [TotalDistance], [AlternateAirportID], [AlternateAirportCode], [AlternateAirportName]) VALUES (27, 1, N'["34L","34R","16R","16L","07","25"]', 2, N'VFR', 4, N'SANS', N'NORTHSTAR', 1, N'SASH', N'SEAHORSE', 1, N'BURSA', 0, N'["NIKEL","AMINA","BURSA","KOSER","WEBER","ALPHA"]', 707.93671420219482, 2, N'SABT', N'BRIGHTSTAR')
/****** Object:  Table [dbo].[Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.FlightRules](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FlightRules] [varchar](5) NOT NULL,
 CONSTRAINT [PK_Base.FlightRules] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.FlightRules] ON
INSERT [dbo].[Base.FlightRules] ([ID], [FlightRules]) VALUES (1, N'IFR')
INSERT [dbo].[Base.FlightRules] ([ID], [FlightRules]) VALUES (2, N'VFR')
INSERT [dbo].[Base.FlightRules] ([ID], [FlightRules]) VALUES (3, N'Local')
SET IDENTITY_INSERT [dbo].[Base.FlightRules] OFF
/****** Object:  Table [dbo].[Prefix.UserRole]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prefix.UserRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](100) NOT NULL,
	[Constant] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Prefix.UserRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Prefix.UserRole] ON
INSERT [dbo].[Prefix.UserRole] ([ID], [RoleName], [Constant]) VALUES (3, N'Instructor', N'instructor')
INSERT [dbo].[Prefix.UserRole] ([ID], [RoleName], [Constant]) VALUES (4, N'Student', N'student')
SET IDENTITY_INSERT [dbo].[Prefix.UserRole] OFF
/****** Object:  Table [dbo].[Base.AircraftProperties]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base.AircraftProperties](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BagCar] [bit] NOT NULL,
	[Bus] [bit] NOT NULL,
	[Catering] [bit] NOT NULL,
	[Lavatory] [bit] NOT NULL,
	[StairCarFront] [bit] NOT NULL,
	[StairCarBack] [bit] NOT NULL,
	[FuelCar] [bit] NOT NULL,
	[Garbarata] [bit] NOT NULL,
 CONSTRAINT [PK_Base.AircraftProperties] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Base.AircraftProperties] ON
INSERT [dbo].[Base.AircraftProperties] ([ID], [BagCar], [Bus], [Catering], [Lavatory], [StairCarFront], [StairCarBack], [FuelCar], [Garbarata]) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Base.AircraftProperties] OFF
/****** Object:  Table [dbo].[Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.AircraftLivery](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AircraftID] [int] NOT NULL,
	[AircraftModel] [varchar](50) NOT NULL,
	[Livery] [varchar](150) NOT NULL,
	[Callsign] [varchar](10) NOT NULL,
	[AircraftStrips] [varchar](150) NOT NULL,
 CONSTRAINT [PK_Base.AircraftLivery] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.AircraftLivery] ON
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (1, 1, N'A310', N'LION AIR', N'LA', N'Red/White')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (3, 2, N'A330', N'GARUDA INDONESIA', N'GIA', N'Blue/White')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (9, 1, N'A330-200', N'Batavia Air', N'BTV', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (10, 1, N'A330-200', N'Garuda Indonesia', N'GIA', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (11, 2, N'A320-200', N'Batavia Air', N'BTV', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (12, 2, N'A320-200', N'Batik Air', N'GIA', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (13, 2, N'A320-200', N'Air Asia', N'AXM', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (14, 2, N'A320-200', N'Lion Air', N'LNI', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (15, 2, N'A320-200', N'Mandala Air', N'MDL', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (16, 2, N'A320-200', N'Citilink', N'-', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (17, 3, N'B737-300', N'Batavia Air', N'BTV', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (18, 3, N'B737-300', N'Citilink', N'-', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (19, 3, N'B737-300', N'Lion Air', N'LNI', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (20, 3, N'B737-300', N'Merpati Nusantara', N'MNA', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (21, 3, N'B737-300', N'Sriwijaya Air', N'SJY', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (22, 3, N'B737-300', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (23, 4, N'B737-400', N'Batavia Air', N'BTV', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (24, 4, N'B737-400', N'Citilink', N'-', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (25, 4, N'B737-400', N'Lion Air', N'LNI', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (26, 4, N'B737-400', N'Merpati Nusantara', N'MNA', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (27, 4, N'B737-400', N'Sriwijaya Air', N'SJY', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (28, 4, N'B737-400', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (29, 5, N'B737-500', N'Batavia Air', N'BTV', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (30, 5, N'B737-500', N'Merpati Nusantara', N'MNA', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (31, 5, N'B737-500', N'Sriwijaya Air', N'SJY', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (32, 5, N'B737-500', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (33, 6, N'B737-800', N'Garuda Indonesia', N'GIA', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (34, 6, N'B737-800', N'Lion Air', N'LNI', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (35, 6, N'B737-800', N'Sriwijaya Air', N'SJY', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (36, 7, N'CN-235', N'Merpati Nusantara', N'MNA', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (37, 7, N'CN-235', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (38, 8, N'Fokker 28', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (39, 8, N'Fokker 28', N'Pelita Air', N'-', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (40, 9, N'ATR 72-500', N'Pelita Air', N'-', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (41, 9, N'ATR 72-500', N'Wings Air', N'-', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (42, 10, N'AS/SA 292 Bravo', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (43, 11, N'F-16', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (44, 12, N'SU-30MK', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (45, 13, N'Lockhead C130', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (46, 14, N'Bell 412', N'Angkatan Udara', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (47, 14, N'Bell 412', N'SAR', N'SAR', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (48, 15, N'NAS 322 Super Puma', N'Pelita Air', N'AU', N'-')
INSERT [dbo].[Base.AircraftLivery] ([ID], [AircraftID], [AircraftModel], [Livery], [Callsign], [AircraftStrips]) VALUES (49, 15, N'NAS 322 Super Puma', N'Angkatan Udara', N'AU', N'-')
SET IDENTITY_INSERT [dbo].[Base.AircraftLivery] OFF
/****** Object:  Table [dbo].[Base.SimulationDeparture]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base.SimulationDeparture](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[SimulationID] [int] NOT NULL,
	[AircraftID] [int] NOT NULL,
	[AircraftLiveryID] [int] NOT NULL,
	[RouteID] [int] NOT NULL,
	[FlightRulesID] [int] NOT NULL,
	[ParkingID] [int] NOT NULL,
	[FlightTypeID] [int] NOT NULL,
	[FlightLevelType] [nvarchar](5) NOT NULL,
	[FlightLevel] [int] NOT NULL,
	[Callsign] [nvarchar](50) NOT NULL,
	[PersonOnBoard] [int] NOT NULL,
	[DepartureTime] [time](7) NOT NULL,
	[EstimateTime] [time](7) NOT NULL,
	[Fuel] [time](7) NOT NULL,
	[PlayerID] [int] NOT NULL,
 CONSTRAINT [PK_Base.SimulationDeparture] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Base.SimulationDeparture] ON
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (62, 1, 1, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0001', 1, CAST(0x070046C323000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 60)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (63, 1, 1, 2, 3, 1, 1, 2, 1, N'M', 3, N'GIA 0002', 1, CAST(0x070046C323000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 61)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (64, 1, 2, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0001', 1, CAST(0x070046C323000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 63)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (65, 1, 2, 2, 3, 1, 1, 2, 1, N'M', 3, N'GIA 0002', 1, CAST(0x070046C323000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 64)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (66, 1, 3, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0001', 1, CAST(0x070046C323000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 66)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (67, 1, 3, 2, 3, 1, 1, 2, 1, N'M', 3, N'GIA 0002', 1, CAST(0x070046C323000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 67)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (68, 1, 4, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0001', 1, CAST(0x070046C323000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 69)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (69, 1, 4, 2, 3, 1, 1, 2, 1, N'M', 3, N'GIA 0002', 1, CAST(0x070046C323000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 70)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (70, 1, 5, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 72)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (71, 1, 5, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 72)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (72, 1, 5, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 72)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (73, 1, 6, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 75)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (74, 1, 6, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 75)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (75, 1, 6, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 75)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (76, 1, 7, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 78)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (77, 1, 7, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 78)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (78, 1, 7, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 79)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (79, 1, 8, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 81)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (80, 1, 8, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 81)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (81, 1, 8, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 81)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (82, 1, 9, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 84)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (83, 1, 9, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 84)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (84, 1, 9, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 84)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (85, 1, 10, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 87)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (86, 1, 10, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 87)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (87, 1, 10, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 87)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (88, 1, 11, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 90)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (89, 1, 11, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 90)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (90, 1, 11, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 91)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (91, 1, 12, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 93)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (92, 1, 12, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 93)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (93, 1, 12, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 93)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (94, 1, 13, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 96)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (95, 1, 13, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 96)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (96, 1, 13, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 96)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (97, 1, 14, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 99)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (98, 1, 14, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 99)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (99, 1, 14, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 99)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (100, 1, 15, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 102)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (101, 1, 15, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 102)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (102, 1, 15, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 102)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (103, 1, 16, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 106)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (104, 1, 16, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 105)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (105, 1, 16, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 105)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (106, 1, 17, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 108)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (107, 1, 17, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 108)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (108, 1, 17, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 108)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (109, 1, 18, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 111)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (110, 1, 18, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 111)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (111, 1, 18, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 111)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (112, 1, 19, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 114)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (113, 1, 19, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 114)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (114, 1, 19, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 114)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (115, 1, 20, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 117)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (116, 1, 20, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 117)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (117, 1, 20, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 117)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (118, 1, 21, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 120)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (119, 1, 21, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 120)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (120, 1, 21, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 120)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (121, 1, 22, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 123)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (122, 1, 22, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 123)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (123, 1, 22, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 123)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (124, 1, 23, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 126)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (125, 1, 23, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 126)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (126, 1, 23, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 126)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (127, 1, 24, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 129)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (128, 1, 24, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 129)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (129, 1, 24, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 129)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (130, 1, 25, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 132)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (131, 1, 25, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 132)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (132, 1, 25, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 132)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (133, 1, 26, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 136)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (134, 1, 26, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 135)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (135, 1, 26, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 135)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (136, 1, 27, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 138)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (137, 1, 27, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 138)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (138, 1, 27, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 138)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (139, 1, 28, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 141)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (140, 1, 28, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 141)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (141, 1, 28, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 141)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (142, 1, 29, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 144)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (143, 1, 29, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 144)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (144, 1, 29, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 144)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (145, 1, 30, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 147)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (146, 1, 30, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 147)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (147, 1, 30, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 147)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (148, 1, 31, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 150)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (149, 1, 31, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 150)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (150, 1, 31, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 150)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (151, 1, 32, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 153)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (152, 1, 32, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 153)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (153, 1, 32, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 154)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (154, 1, 33, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 156)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (155, 1, 33, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 156)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (156, 1, 33, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 156)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (157, 1, 34, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 159)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (158, 1, 34, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 159)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (159, 1, 34, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 159)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (160, 1, 35, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 162)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (161, 1, 35, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 162)
GO
print 'Processed 100 total records'
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (162, 1, 35, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 162)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (163, 1, 36, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 165)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (164, 1, 36, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 165)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (165, 1, 36, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 165)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (166, 1, 37, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 168)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (167, 1, 37, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 168)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (168, 1, 37, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 168)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (169, 1, 38, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 171)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (170, 1, 38, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 171)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (171, 1, 38, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 171)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (172, 1, 39, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 174)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (173, 1, 39, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 174)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (174, 1, 39, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 174)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (175, 1, 40, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 177)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (176, 1, 40, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 177)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (177, 1, 40, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 177)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (178, 1, 41, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 180)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (179, 1, 41, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 180)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (180, 1, 41, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 180)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (181, 1, 42, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 183)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (182, 1, 42, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 183)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (183, 1, 42, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 183)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (184, 1, 43, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 186)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (185, 1, 43, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 186)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (186, 1, 43, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 186)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (187, 1, 44, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 189)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (188, 1, 44, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 189)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (189, 1, 44, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 189)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (190, 1, 45, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 192)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (191, 1, 45, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 192)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (192, 1, 45, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 192)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (193, 1, 46, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 195)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (194, 1, 46, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 195)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (195, 1, 46, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 195)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (196, 1, 47, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 198)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (197, 1, 47, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 198)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (198, 1, 47, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 198)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (199, 1, 48, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 201)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (200, 1, 48, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 201)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (201, 1, 48, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 201)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (202, 1, 49, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 204)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (203, 1, 49, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 204)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (204, 1, 49, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 204)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (205, 1, 50, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 207)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (206, 1, 50, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 207)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (207, 1, 50, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 207)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (208, 1, 51, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 210)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (209, 1, 51, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 210)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (210, 1, 51, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 210)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (211, 1, 52, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 213)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (212, 1, 52, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 213)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (213, 1, 52, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 213)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (214, 1, 53, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 216)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (215, 1, 53, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 216)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (216, 1, 53, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 216)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (217, 1, 54, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 219)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (218, 1, 54, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 219)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (219, 1, 54, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 220)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (220, 1, 55, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 222)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (221, 1, 55, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 222)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (222, 1, 55, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 222)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (223, 1, 56, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 225)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (224, 1, 56, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 225)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (225, 1, 56, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 225)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (226, 1, 57, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 228)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (227, 1, 57, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 228)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (228, 1, 57, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 228)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (229, 1, 58, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 232)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (230, 1, 58, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 232)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (231, 1, 58, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 231)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (232, 1, 59, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 234)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (233, 1, 59, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 234)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (234, 1, 59, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 235)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (235, 1, 60, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 237)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (236, 1, 60, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 237)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (237, 1, 60, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 237)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (238, 1, 61, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 240)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (239, 1, 61, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 240)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (240, 1, 61, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 240)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (241, 1, 62, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 243)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (242, 1, 62, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 243)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (243, 1, 62, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 243)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (244, 1, 63, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 246)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (245, 1, 63, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 246)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (246, 1, 63, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 246)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (247, 1, 64, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 249)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (248, 1, 64, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 249)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (249, 1, 64, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 249)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (250, 1, 65, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 252)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (251, 1, 65, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 252)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (252, 1, 65, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 252)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (253, 1, 66, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 255)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (254, 1, 66, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 255)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (255, 1, 66, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 255)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (256, 1, 67, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 258)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (257, 1, 67, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 258)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (258, 1, 67, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 258)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (259, 1, 68, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 261)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (260, 1, 68, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 261)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (261, 1, 68, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 261)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (262, 1, 69, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 264)
GO
print 'Processed 200 total records'
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (263, 1, 69, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 264)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (264, 1, 69, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 264)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (265, 1, 70, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 267)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (266, 1, 70, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 267)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (267, 1, 70, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 267)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (268, 1, 71, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 270)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (269, 1, 71, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 270)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (270, 1, 71, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 270)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (271, 1, 72, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 273)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (272, 1, 72, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 273)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (273, 1, 72, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 273)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (274, 1, 73, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 276)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (275, 1, 73, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 276)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (276, 1, 73, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 276)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (277, 1, 74, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 279)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (278, 1, 74, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 279)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (279, 1, 74, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 279)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (280, 1, 75, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time), 282)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (281, 1, 75, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time), 282)
INSERT [dbo].[Base.SimulationDeparture] ([ID], [SceneryID], [SimulationID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel], [PlayerID]) VALUES (282, 1, 75, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time), 282)
SET IDENTITY_INSERT [dbo].[Base.SimulationDeparture] OFF
/****** Object:  Table [dbo].[Base.SimulationArrival]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base.SimulationArrival](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[SimulationID] [int] NOT NULL,
	[AircraftID] [int] NOT NULL,
	[AircraftLiveryID] [int] NOT NULL,
	[RouteID] [int] NOT NULL,
	[FlightRulesID] [int] NOT NULL,
	[FlightTypeID] [int] NOT NULL,
	[FlightLevelType] [nvarchar](5) NOT NULL,
	[FlightLevel] [int] NOT NULL,
	[Callsign] [nvarchar](50) NOT NULL,
	[PersonOnBoard] [int] NOT NULL,
	[StartPointTime] [time](7) NOT NULL,
	[DepartureTime] [time](7) NOT NULL,
	[EstimateTime] [time](7) NOT NULL,
	[Fuel] [time](7) NOT NULL,
	[PlayerID] [int] NOT NULL,
 CONSTRAINT [PK_Base.SimulationArrival] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Base.ExerciseDeparture]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.ExerciseDeparture](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[ExerciseID] [int] NOT NULL,
	[AircraftID] [int] NOT NULL,
	[AircraftLiveryID] [int] NOT NULL,
	[RouteID] [int] NOT NULL,
	[FlightRulesID] [int] NOT NULL,
	[ParkingID] [int] NOT NULL,
	[FlightTypeID] [int] NOT NULL,
	[FlightLevelType] [varchar](5) NOT NULL,
	[FlightLevel] [int] NOT NULL,
	[Callsign] [varchar](50) NOT NULL,
	[PersonOnBoard] [int] NOT NULL,
	[DepartureTime] [time](7) NOT NULL,
	[EstimateTime] [time](7) NOT NULL,
	[Fuel] [time](7) NOT NULL,
 CONSTRAINT [PK_Base.ExerciseDeparture] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base.ExerciseDeparture] ON
INSERT [dbo].[Base.ExerciseDeparture] ([ID], [SceneryID], [ExerciseID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel]) VALUES (123, 1, 10, 2, 3, 1, 1, 2, 1, N'A', 2, N'GIA 0001', 1, CAST(0x0700000000000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time))
INSERT [dbo].[Base.ExerciseDeparture] ([ID], [SceneryID], [ExerciseID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel]) VALUES (124, 1, 10, 2, 3, 2, 2, 3, 1, N'F', 4, N'GIA 0002', 1, CAST(0x0700000000000000 AS Time), CAST(0x0700209DB4060000 AS Time), CAST(0x0700547FE50A0000 AS Time))
INSERT [dbo].[Base.ExerciseDeparture] ([ID], [SceneryID], [ExerciseID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel]) VALUES (125, 1, 10, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0003', 1, CAST(0x07008C8647000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time))
INSERT [dbo].[Base.ExerciseDeparture] ([ID], [SceneryID], [ExerciseID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel]) VALUES (128, 1, 12, 2, 3, 36, 3, 1, 1, N'A', 2, N'GIA 0001', 1, CAST(0x070046C323000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x070034E230040000 AS Time))
INSERT [dbo].[Base.ExerciseDeparture] ([ID], [SceneryID], [ExerciseID], [AircraftID], [AircraftLiveryID], [RouteID], [FlightRulesID], [ParkingID], [FlightTypeID], [FlightLevelType], [FlightLevel], [Callsign], [PersonOnBoard], [DepartureTime], [EstimateTime], [Fuel]) VALUES (129, 1, 12, 2, 3, 1, 1, 2, 1, N'M', 3, N'GIA 0002', 1, CAST(0x070046C323000000 AS Time), CAST(0x07006660D8060000 AS Time), CAST(0x07009A42090B0000 AS Time))
SET IDENTITY_INSERT [dbo].[Base.ExerciseDeparture] OFF
/****** Object:  Table [dbo].[Base.ExerciseArrival]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.ExerciseArrival](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SceneryID] [int] NOT NULL,
	[ExerciseID] [int] NOT NULL,
	[AircraftID] [int] NOT NULL,
	[AircraftLiveryID] [int] NOT NULL,
	[RouteID] [int] NOT NULL,
	[FlightRulesID] [int] NOT NULL,
	[FlightTypeID] [int] NOT NULL,
	[FlightLevelType] [varchar](5) NOT NULL,
	[FlightLevel] [int] NOT NULL,
	[Callsign] [varchar](50) NOT NULL,
	[PersonOnBoard] [int] NOT NULL,
	[StartPointTime] [time](7) NOT NULL,
	[DepartureTime] [time](7) NOT NULL,
	[EstimateTime] [time](7) NOT NULL,
	[Fuel] [time](7) NOT NULL,
 CONSTRAINT [PK_Base.ExerciseArrival] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base.Aircraft](
	[ID] [int] NOT NULL,
	[AircraftPropertiesID] [int] NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Category] [varchar](10) NOT NULL,
	[Type] [varchar](10) NOT NULL,
	[Engine] [int] NOT NULL,
	[TrueSpeed] [float] NOT NULL,
	[MaximumPerson] [int] NOT NULL,
	[FuelConsumption] [float] NOT NULL,
	[Weight] [float] NOT NULL,
	[Thrust] [float] NOT NULL,
	[AccelerateSpeed] [float] NOT NULL,
	[ClimbSpeed] [text] NOT NULL,
	[DescendSpeed] [text] NOT NULL,
	[CircuitHeight] [float] NOT NULL,
	[MaximumAltitude] [float] NOT NULL,
 CONSTRAINT [PK_Base.Aircraft_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (1, 1, N'A330-200', N'Heavy', N'Jet', 2, 567, 500, 0, 200000, 316000, 1.58, N'0', N'0', 457.2, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (2, 1, N'A320-200', N'Heavy', N'Jet', 2, 487, 500, 0, 60300, 120000, 1.99, N'0', N'0', 457.2, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (3, 1, N'B737-300', N'Medium', N'Jet', 3, 520, 500, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (4, 1, N'B737-400', N'Medium', N'Jet', 2, 509, 500, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (5, 1, N'B737-500', N'Medium', N'Jet', 3, 520, 500, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (6, 1, N'B737-800', N'-', N'-', 0, 0, 0, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (7, 1, N'CN-235', N'Medium', N'Propeller', 2, 245, 500, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (8, 1, N'Fokker 28', N'Medium', N'Jet', 2, 485, 500, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (9, 1, N'ATR 72-500', N'Medium', N'Propeller', 2, 294, 500, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (10, 1, N'AS/SA 292 Bravo', N'-', N'-', 0, 0, 0, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (11, 1, N'F-16', N'Medium', N'Jet', 2, 615, 1, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (12, 1, N'SU-30MK', N'-', N'-', 0, 0, 0, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (13, 1, N'Lockhead C130', N'Heavy', N'Jet', 3, 562, 1, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (14, 1, N'Bell 412', N'Light', N'Helicopter', 2, 133, 2, 0, 0, 0, 0, N'0', N'0', 0, 4000)
INSERT [dbo].[Base.Aircraft] ([ID], [AircraftPropertiesID], [Model], [Category], [Type], [Engine], [TrueSpeed], [MaximumPerson], [FuelConsumption], [Weight], [Thrust], [AccelerateSpeed], [ClimbSpeed], [DescendSpeed], [CircuitHeight], [MaximumAltitude]) VALUES (15, 1, N'NAS 322 Super Puma', N'Medium', N'Helicopter', 2, 102, 2, 4000, 0, 0, 0, N'0', N'0', 0, 4000)
/****** Object:  Table [dbo].[Prefix.User]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prefix.User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](max) NOT NULL,
	[LastLogin] [datetime] NULL,
 CONSTRAINT [PK_Prefix.User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Prefix.User] ON
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (1, 3, N'ilyas', N'qPc17MAOKsEdMfrYAWuORg==', CAST(0x0000A47200E4DB7D AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (2, 4, N'student1', N'jVy5OARaKZs=', CAST(0x0000A46B01758010 AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (3, 4, N'student2', N'K3s2/M1KCfQ=', CAST(0x0000A45C00C37789 AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (4, 4, N'student3', N'qPc17MAOKsEdMfrYAWuORg==', CAST(0x0000A45C00C62AB1 AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (5, 3, N'teguhnoor', N'qPc17MAOKsEdMfrYAWuORg==', CAST(0x0000A48801011780 AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (6, 4, N'aliahsw', N'miT7Q49T2P0=', CAST(0x0000A46C009EF477 AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (7, 4, N'asmirandah', N'u4HfFCGWFPU11AObE/ICeA==', CAST(0x0000A46C009EEDD6 AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (8, 4, N'tegar', N'Gqh+OeJPaR8dMfrYAWuORg==', CAST(0x0000A4690182E5B5 AS DateTime))
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (9, 4, N'coba', N'RCrfULTeKFI=', NULL)
INSERT [dbo].[Prefix.User] ([ID], [RoleID], [Username], [Password], [LastLogin]) VALUES (11, 4, N'mardika', N'DSpjgZOfJro=', CAST(0x0000A46C001C4C1E AS DateTime))
SET IDENTITY_INSERT [dbo].[Prefix.User] OFF
/****** Object:  Table [dbo].[Prefix.UserProfile]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prefix.UserProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Rolename] [varchar](100) NOT NULL,
	[Firstname] [varchar](150) NOT NULL,
	[Lastname] [varchar](150) NULL,
	[UserIdentity] [varchar](100) NOT NULL,
	[Gender] [varchar](15) NOT NULL,
	[Birthdate] [date] NULL,
	[Phone] [varchar](25) NULL,
	[Email] [varchar](150) NULL,
	[Address] [text] NULL,
	[Photo] [image] NULL,
	[IsStudent] [bit] NOT NULL,
	[Class] [varchar](250) NULL,
	[ClassYear] [int] NULL,
	[Score] [float] NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](50) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateBy] [varchar](50) NOT NULL,
	[isOnline] [bit] NOT NULL,
 CONSTRAINT [PK_Prefix.UserProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Prefix.UserProfile] ON
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (1, 1, N'ilyas', N'Instructor', N'Ilyasin', N'Hafid', N'-', N'Male', CAST(0x38130B00 AS Date), N'+6285722315553', N'ilyas@media-it.co.id', N'Pelanduk 18B, Bandung', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108004B006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00E952A640474A5451D854DB4E3815ED1E328319F377A0900FAD53D7975587C3D7973A55A4D7378ABB61489377CC4F5C7B75AF3CF063F8CF55D5AEACF517BB48E2C195EE73188F24F000009271C61801835CD2C5414DC126DAFF0087EA6D1C2CA51E66EC8F500D97099018F20679A98AE054567A45BD9731A1690FDE91CE589F5CFAD56D55356B18A4BBD3E35BF551B9ACE43B58FF00B8E3BFB1073EA2B7BD95D99FB2D6C8B8C4118A410679AE17FE16BE9705CDB4779A7CD047281E63870C62CFAAE33FE7A57A2A245756C92C4EB24522864746E1811C104511A9197C2C7EC64B59228CED05AC465B8952288757918281F89A42AAEA1A221948C820E411543C53E0CB7D56DAC9EFEF26B4B14946FF002E2525B7703E623239207248E7A56345AF5A7FC2C3B5D2EC6E9E589E078A40CE48CA8DCA7D33C119F7AE4A78B752A38452B26D377D9E9F79BCB0D18C399BD5EDA1D2B44C7AD412400F6AD66894D30C09DC8AEEBA39FD918C6D467A515A8D1479EA28A2E2F628D08ECE11C0E4D5B4B242B8005528EE0EEC81D6AEA4B26D04719ED58BB9D89216DAE041AC3692ADB435B7DA588EBB776D3FAE3F5A82DECADD2EA5F29362C873C9EF5CAF87FC4D16B1F1035B643BA2B4B55B58CFA90E771FA647E95D4C63ED3BE3DC54382B9538233DC1AF2E786E7C42ACDEB1DBE7B9D31A96A7C8B665D4B0CB7DE14FFB122B7279AE074CF17788B4F8766BDA742CB1B146B886E111B8E39562013F43F856B45E39D2AEF544D3EDA769246566797A2A05193F5AF4154BABDCE7B743CFFE2F782B4B80C9AED9491C77721027B7538DC7FE7A003BFAFAF5F5AE93E09DC9BCF0A3584AC4FD9A7DB1E4F215B9C7E79FCEBCBBE2078826D535A98472B1B656C2F3E9C56BFC36F108D0EF4ACF70B14773132F27186C654FD78C7E359462EEE4B71CA7B27B1EE9F11BC83E03D66060430B3778F69C6D641B94FE0541AF9C7C1774D6FE3ED12EA65DC2797CACB7ABA9407F36AF7AD6E5935AD267859B26681A3CFAE4115F33ADF5C695A9C1E644C93D9CE8FB5B82AC8DD3F4ACA8D2749CB9BAD8A9BE66AC8FAACDA2ED24F6AA8F680B7278AD01776D736905CDBC9BA299164423BA91907F2348CF14BF74600EB5DAA4D91CA8CC3690E7AD1574DBDBB1CEF3453E60E545A4D267DBC22AD66EBBF69D2B45BBBAC85744C464F4DEC76AFEA457611C9838241158DE2448EF2E746B2201596F964917B6D8D59F9F6DC17F3AE4F6D2B9AF2AB1E61E0EF0D0F0A5DDF452EE33CDE5B3173961F206C1F7CB1AB5E2AF1AC7E12D39A645592F26CAC11B74CF763EC38FD2B735C3BF51D4A78D490974006ED8F2D063F353F9D711AD785A2F126BBA7DEDDDC62D2DC625831CC9CE719EC0F7A7CCB9056D4C6F0BF85FC53E3D9D752B8221B46258DD5CA9C4849E7628EA3DF81EF53F8AFC243E1EFEF9AFD6EAE6F91829552BE5AE41200C9EBC73EDD057BD682122D22011C6A91608455180AA0E0003D38AF1BF8F170BFDB765167212D4363D0966FF000A98C9B7626515157478A5E5C3CF3124F7CD588FED12C626891D92251B8A8C85C9E2B3DCEE91BDEBD9FE1A78160F12784B50B7B87684DD4595995726360C0A1C77E509C7A1ABE6693686E2B4465782BE233DBF97A66AEDBA03858E72794F66F51EFDBF9707E28B8377E29D4E70D95370F83EA03103F95767E3DF01CFE1F70E9224B3C1127DA0C6BB44BF28CB81D8F5CD79C13BA4C7AD373E68845599F4FF00C2C32EB3F0DF4B983296855ADDBDB631007FDF3B6BB086CBECCDF3E189F415E59FB3A6A921B6D734776CA46F1DCC633D0B02ADFF00A0A57B818D73920564EB497BACBE54F54511042C33E50FCA8ABDB94718145473B1F2A33A2B9B7C61648FFEFAACFB878BFE121B2B86913CA8EDA75C93C6F668B1FA2B7E75E6A92F1C54DF680885C9FBA3268E60B1BDACEB10C1A2FD98042F33BCD2B818F9C926B90B1B87D4F5286CAD64506470BBD8E00E7A9F6AC2F106B1E6DC416ADB849E486D801CE00C9CFD2B53C2B1021EE49C81C29F7AD65A2B18F3734EC7B7DAB5ADADA476E93A308542E4B75C77AF9E3E355DA5D78D265593291431AE01E33B73FD6BD044BCE2BCB7C716C6EB50D56F1B8512C31839F48871FA0A9A7AB2EA688F3B8977CDB40C9CD7D65F0D2DA1D1FC1F6B048E8B70DC382707818FE60FE75F2E6848FF00DBB66CA33B6E131FF7D0AFA1210D1C11A67255793EA6AA4D286A0B599BBE3ED286A7689A8DA94964B705658D4E4B27A8F5C735F396A5A4C163E22B5F2D58D8DCC8BC631B72D865FC3A8FAD7B53DF85DC024DF29E7119FD3D6B91F19471DB4697A11712B6DE78DB2763F8FF00315106AE8734D26D14FE0BEA51E85F136E74D9A5056EA192DD5BB1752181FC95BF3AFA45EEE21D6441F8D7C676375259F8AEDAF590EF4BA0580F73C8CFAE0D7B53DCC847DF6E7B6689A4A409DE28F5DFB643FF003D53F3A2BC6CDCC99FF58DF9D1537431A920F4A903F190DB4F6354518F1CD4AA4ED15170389B0D41344D6350BBD5E6BA7BCB888A44F246A40F51C13EDD874353F83BC4F0697A31B3D4D66DE246688A2798369C71C679CE7AFB57658E9F5A685563F3229E7BA8A6A57DC2C427C4F608D17D9F73C67EF3125028FA35701ADEAF0EB1AEB59FDA163B47BB2EC49206DDA83AF3E8D8F4AF46FB3C2EBF34319CFAA8A468A257DA23400F6DA2852B6A85BEE79B5DDEE97A5F8AB4E6B18E25B5876798EA4B63E620B1E392060D76ABE2ED359448B7F34B81C0488819FCB35AA74FB3F318FD961E87F8052C76B6E14E208FFEF91494AFB8356D8C1B8F16E9E5B3B6EC8CE4FEF48EDD7033F9562F883C45A7DFE953D9C56D74CD2600760CDDC1EFD3047E95DC98635FBA8A3F0FAD42D146AA0845049F4A7703CD34A9A1FB15D477B6C64334BE66E7462571F77182307AF3EF5B87C40B228412DC44C176EEF2C93F80DD8AEAD8938FF67A7B544490C807420FF4AA4EFB92E2FB9C99C5CFEF1AEEFC9E848B72DFAD15D4A9C8C9A29732EC4D99FFD9, 0, N'B', 0, NULL, CAST(0x0000A46C009AFC2B AS DateTime), N'teguhnoor', CAST(0x0000A46C009AFC2B AS DateTime), N'teguhnoor', 0)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (2, 2, N'student1', N'Student', N'Ilyasin', N'Student1', N'123', N'Male', CAST(0x38130B00 AS Date), N'+6285722315553', N'ilyas@media-it.co.id', N'Pelanduk 18B, Bandung', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108004B006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F2565A8596ACB0A85857D64A2793165665A89855965A8996B9E51378B2BB0A8C8A9D96A36158491AC590114DA948A61159346A98DA28A2A4614514A013D05031A68A5239EB452B858E91AA16A91DAA2077381EA6BDCAAD462E4F647994E2E4D243A2B59EE33E4C4CF8F4144D632C081A55C13DBD3EBEFED5D91B792CB4E89215C610EE7E9CD5AF0BE8F6FE24B8BC8EF24982DB212C7EF386F41C0FA57C9C739AB56F51457227F33DEFECFA70B45CBDEFC0F3A68D82EE2001EA540A81953B83F874AF68B8F85DA449BD26BFBA8A60A08C90E07B74154A2F86961A7C11CF744DCA8605DCB15007A6DEFF00E7E95D31CC68C95F5FB8C65869C5D958F1F6507A0CFE351B281D41FCEBD3758F095AEADA7A5CE8DA54B6B28C7989B8940A3B824F2C41538FF0AC497C2DA579400D4FC931E16598E0A06C8EA339048278FF00035AAAF06AE67C8D33893B7D0FE7463D80FAD6F6A9E1CB8D3EDFED50CD6D776C3EF490364A738F9876AC13F8534D495D06C1BB1D290927A9A38A38F7A6171327D68A4A2968237DCD244035CA2B742714E75A858115EED7A7CF0943BAB1C1465CB252EC7B568D636BAC5840CEAAF04A9861CF23A11EDD2BAEB3F0D68D63325F5959C5E7840AD21E00007A9FA7E9CD78F7843C5CFA744D6B2B111B9CEE07946F5FA1AEC575FBD7808594344C0F0C7E523FA8AFCA6AC2BE06BCA94EE95CFAF8F26220A71DCDFD65AEE4D4623653BC913A6E53146BB3241C12CC791F41F87A4D6D15BDF958A7D4048EF80888C99DE064F18F5FC6BCD7C49E34B9B7B28ACE190A4A492CD1300C0673E871CD708353BA8EE96E60B8962995B7AB83C86F5C8EF5EFE032EAB88A5CFF000A7B338B13888529725EED6E7B7DEEA57514F2CD04027B06077007EEB2E7B0EB9F6FFEBD72775E0ED1FC676336A3A3432D8DE292CF16C387F9B93B3D719C63DAB82B3D7751B0BAFB44533392DBDD1D8B239FF68679EB5E83A77C52D26DED6269EC2E9274DA1A3888DAE7BB06C823E9835DD3C1D7C3A4E96BFD7539155A555FBFA193A17853481F6AB76D4A5963BB85D629215FDE465490C0C6339CF1E86B9EBDD1F47D1F519B4CB95373712CA123712347E429E01390324E73D315E8F6BAA697E2DB2B9BAB1B18E192DDDBCA0E4230DD8DC495FBB9CE723A915CEEA5E1D7D52DE5B5B99A39A4B58BCE8AE4002668C1C32B72031C743D3BD670AB3E77ED1B5E412A71B7BA705AC7876F3489660CD1CF144DB5E488E76E7A6EF4CD6357A2681E1BB7BBBD0743BB96E6CA480ADE25CC653823E6008E0918CFB1039AF3D6037103D7F3AEBA55149B57D518CA36D46E28A31456E49D43C755DE3AD478AA078ABE96503C4855334A1539190477153C77D770C663495829EA3247F2A95A2A89A2AE3AF85A55BF8914FD51D94F13387C2EC5572D2396739269BB6AC98E98528E451565B0B9EEEEC808A6367BF35332D46C2B0A8AE5C644BA7EAB79A4DC19ACA7685D9769C7208F423D2BA7B6F88F3411C425D2AD9E655DAD3C64AB9E793DFB62B8D6151B0E3DC579F5B0F4EA7C4AE7553AB28ECCF4EB3F8ADA6DAA311A24F1BE7858EE176B73CE7E51FD6B0758F12685E268E63796B2D8CE5C3C6D1A2BF7E7E618E31EDDAB8B3EFCD30815C51C05284B9E09A7EACE97889C95A5AA3A0B9F0F69A276F27C4B61244794660EA48F71B7834573B83EB456DECE5FCCFF000FF223997F29DF34750B47571AA2615F6928A3E4E3365268EA268EAE30A858573C91BC66CA8D1D44D1D5C6150B0AC251378CD94D92A264AB6C2A16AE79451BC645464A8597156DAA07EB5CD389D119159979A8C8A9DEA26EB5CD246F1645453A8ACEC59FFFD9, 1, N'A', 0, 0, CAST(0x0000A4330182FEAF AS DateTime), N'ilyas', CAST(0x0000A4330182FEAF AS DateTime), N'ilyas', 0)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (3, 3, N'student2', N'Student', N'Ilyasin', N'Student2', N'234', N'Male', CAST(0x38130B00 AS Date), N'+6285722315553', N'ilyas@media-it.co.id', N'Pelanduk 18B, Bandung', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108004B006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00E952A640474A54503A0A9B69C702BDA3C6506306EA0900D53D7975587C3D7973A55A4D737AABB61489377CC4F5C7B75AF3CF063F8CF55D5AEACF517BB48E2C195EE73188F24F000009271C61801835CD2C5414DC126DAFF87EA6D1C2CA51E66EC8F500D970990188C819E6A62B815159E916F65CC685A43F7A4739627D73EB55B554D5AC6392EF4F8D6FD546E6B290ED63FEE381D7D981CFA8ADEED2BB33F65AD9171882314820CF35C28F8AFA5C1736D1DE69F3411CA0798E1C318B3EAB8CFF005F6AF45448AEAD92589D648A450C8E8DC30238208A235232F858FD8C96B24529DA0B589A5B89522897ABC8C140FC4D34AABA868C865232083904550F14F832DF55B6B17BFBC9AD2C5251BFCB89096DDC0F988C8E481C9239E958D16BD69FF0B0ED74BB1BA7962781E290339232A3729F4CF0467DEB92962DD4AAE114AC9B4DDF67DBCCDE5868C61CCDEAF6D3FE09D2B42C7AD432400F6AD56894D30C083A915DD7473FB2318DA8CF4A2B54C5183D68A2E2F628BF1D9C2381C9AB6964857000AA51DC12D903AD5D49640A08E33DAB1773B1242DB5C883586D255B686B6FB4B11D76EEDA7F5C7EB505BD95BC7752F948516439E4F7AE57C3FE268B58F881ADB21DD15A5AADAC67D4873B8FD323F4AEA631F69DF1EE2A1C15CA9C119EE0D7973C373E21566F58EDF3DCE98D4B53E45B32F25865BEF0A77D8915B93CD703A5F8BBC45A7C3B35ED3A1658D8A35C437088DC71CAB1009FA1FC2B5A2F1CE9577AA269F6D3B49232B33CBD15028C93EF5E82A8DABDCE7B743CFF00E2F78274B80C9AED94891DE48409EDD4E371FF009E800EFEBEBD7D6BA4F825726F3C28D612B122DA7DB1E4F215B9C7E79FCEBCBBE2078826D535A98472B1B656C2F3E9C56BFC36F108D0EF4ACF70B14773132F27186C654FD78C7E359462EEDADC729EC9EC7BA7C46103780F5980821859BBC7B4E36B20DCA7F02A0D7CE3E0BBA6B7F1F6877532EE13CBE565BD5D4A03F9B57BD6B72C9AD6933C0CD93340D1E7D7208FEB5F33ADF5C693A9C1E644C93D9CE8FB5B82AC8DD3F4ACA8D2749CB9BAD8A9BE66AC8FAACDA2ED24F6AA8F680B7278AD01776D736905CDBC9BA299164423BA91907F2348CF14BF77803AD76A9323951986D21CF5FD68ABAD6F6EC73BCD14F983951693499F6F08AB59BAE9B9D2B45BBBAC85744C464F4DEC76AFEA457611C9838241158DE2448EF2E746B2201596F964917B6C8D59F9F6DC17F3AE4F6D2B9AF2AB1E61E0EF0D0F0A5DDF452EE33CDE5B3173923E40D83EF96356BC55E368FC25A734C8AB25E4C4AC11B74CF763EC38FD2B735C3BF51D4A78D5884BA00376C796831F9A9FCEB88D6BC2D178935DD3AF6EEE31696E312C18E64E738CF607BD3E65C82B6A63785BC2FE29F1E4EBA94E44368C4B1BAB9538909393B14751EFC0F7A9FC57E121F0F7F7CD7EB75737C8C14AA95F2D72090064F5E39F6E82BDEB4109169102C71AA45821154602A838000F4E2BC6FE3C5CAFF6DD9459C84B50DF4259BFC2A6326E56267151574789DE5C34F2924F19CD598FED12C626891D92251B8A8C85C9E2B3DCEE91BDEBD9BE1A78160F12F84F50B7B87684DD4595995726360C0A1C77E509C7A1ABE6693686E2B4465F82BE233DBF97A66AEDBA03858E72794F66F51EFDBF9707E28B8377E29D4E70D95370F83EA03103F95767E3EF01CFE1F70E9224B3C1127DA0C6BB44BF28CB81D8F5CD79C13BA4C7AD373E68845599F4FF00C2C32EB3F0DF4B983296855ADDBDB631007FDF3B6BB086CBECCC37E189F415E59FB3A6A921B6D734773948DE3B98C67A16055BFF00414AF7031AE7240CD64EAC97BACBE54F54515821600F963F2A2AF6E51C605151CEC7CA8CE8AE6DF185923FFBEAB3EE248BFE121B1B86913CA8EDA75C96E37B3458FD15BF3AF358E5E38A9BED0110B9FE1193473058DED6758860D17ECC021799DE695C0C7CE4935C858DC3EA7A943656B22832385DEC70073D4FB561788359F36E20B56DC24F2436C00E70064E7E95A9E158410F727903853EF5ACB45631E6BCEC7B7DAB5A5ADA476E93A308542E4B7271DEBE78F8D576975E3499564CA450C6B8078CEDCFF5AF4112F38AF2DF1C5B1BAD4355BC6E144B0C60E7D221C7E82A69EB22EA688F3B8537CDB40C9CD7D65F0D2DA1D1FC1F6B048E8B70C30E09C1C818FE60FE75F2E6848FFDBB66CA3256E131DFF8857D090868A08D3392ABC9F535526943505ACCDDF1FE94353B34D46D4A4B2DB82B2C6A72593D47AE39AF9CB52D260B1F115AF96AC6C6E645C0C636E5B0CBF8751F5AF6A7BF0BB8049BE53CE233FA7AD723E328E3B68D2F422E256DBCF1B64EC7F1FE62A20D5D0E69A4DA29FC17D4A3D0BE26DCE9B34A0ADD4325BAB762EA4303F92B7E75F48BDDC63AC883F1AF8CEC6EA4B2F15DB5EB21DE9740B01EE7919F5C1AF6A7B9908FBEDCF6CD13494869DE28F5DFB643FF003D53F3A2BC6CDCC99FF58DF9D15374035241E95207C0C86DA7B1AA28C46306A552768A8B8CE26C3504D1358D42EF579AE9AF2E222913C91AB01EA3E527DBB0E86A7F07789E0D2F46367A9ACDBC48CD1144F306D38E38CF39CF5F6AECF1D3EB4C0AAE7E6453CFF745352B8AC427C4F608D17D9F73C67EF3125028FA37E35C06B7ABC3AC6BAD69F6958ED1EECBB12481B76A0EBCF3C363D2BD1BECF0BAFCD0C673EA80D0D0C4AFB446801EA368A39ADAA12D773CD2EEF74BD2FC55A7358C712DAC3B3CC7525B1F31058F1C90306BB65F1769ACA245BF9A5C0E024440CFE59AD5FECEB3F309FB2C39C1FE01EB4B1DADB856FDC47FF7C8A4A57DC1AB6C60DC78B74F2D9DB7646727F7A57B75C0CFE558BE21F1169F7FA54F6715B5D3349801D83377078CF4C11FA5772618D7EEA28FC3EB50B451AA821141279E28B86A79A695343F62BA8EF6D8C86697CCDCE8C4AE3EEE30460F5E7DEB70F8816450825B88982EDDDE5927F01BB15D631CE3FD93C7B542490C807420E7F4AB4EFB92E2FB9C99C5CFEF1AEEFF00774245B16FD68AEA50E5413D68A5CCBB13667FFFD9, 1, N'A', 0, 0, CAST(0x0000A4330181C597 AS DateTime), N'ilyas', CAST(0x0000A4330181C597 AS DateTime), N'ilyas', 0)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (4, 4, N'student3', N'Student', N'Ilyasin', N'Student3', N'345', N'Male', CAST(0x38130B00 AS Date), N'+6285722315553', N'ilyas@media-it.co.id', N'Pelanduk 18B, Bandung', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108004B006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00C111803359F3A031F7DDBBAE6B54AE518019E2BB0F09E9F67A469F36B97F0F993A822DA36E467D71DFF2AE28EACEA5A2287877C09792E991DD6A2E2CE290E555972EC0F703FC6BA46B0F0E69D6D1C1F60378DDE491D91C9F6C6062BCF3C45E2CD727958CBE6AB33642F01B1EA7B81E9FA566F87B56B8D46F8C370EEC3A74DD8FA9EBFAD6CA28C99E92356F07C36CF0DDE972DB3A9F99BCC2D827A73FA551D67406B30B776DFBDB197FD5C8BC81EC4D62EA77D636C12DE573322E1A42995650B838C1E9CE39C9E2B5BC09AECDA9DC4D672AAFD91325D08C82BD0641EFC512826813D4E9BC3BA3C569A635D4C06F619E6B9ED42E05EEA813B2B7CA73C1F6AEFA7BAD19EDBECC0B2A95E81C0E3F5E95CE3787B47BB9956CEFA788F56F3143E0FD41158BA6EC6AE5756471B7508498C609217819155CC05B85524FB57752F81E5994CB06A16D28031F3654E7F2359B2CD6FE1CBC8ED22861BABD7EAE4E557D401DCE334A309688C9A398B6B276B98F7215527A914EBD2EF23233650138C74AECA4F17DA47132CF3C70301CFD9E156D83D8E301B9E464E2AB43A9E95ACA0374C851C6CDC5151FD9B23BF3EF9CFE15A7B2D6F711C23C5F3515D75F7836ED2EDC5A346F01E519DD738F7A2A6CFB058E6AD4033C609C0DDD6BB593544FECA3109D830E3248C1FA639AE25E29AD6511CB1B236324118E2BA1D174A7991A4B89E1491C011412B7217AE47604D10DCDBEC9C678B2EC14D900288E70D230C16FA53740B23696DFDA1BA3431838C310C7DB1FFD7AD4F106948B74C65256353866272EC7FBA3D2AEB5B5943A6A34512AFC9D00EFEE7B9AE8460D9E7573737DE20D7C58C424595DF6B71C229EBC7AE7FA57B2F84BC26DA769EB18DE8C53716724306F52011EF8FAD627C37F07ABDE5E6AD2EC2266D8189C8451F788FAFF004AF4CBCD4C2CC6DA08D57040D800E077CF1F41436115D4CDFEC5FB759FEFA590F0371071CF5EA31D7AFE559D3692D641C41E6146F9FBFD303F0E78C74F7AD98B54B645993192FC30CE067A62A17BA177B18663623A05C903D3F2FD691699C96A7ACDF69D6F89E501002467AF7EA0F3E9E95E757FE214BBF14DBDCCD1B9B531819DBBB1CE7A11CF23F9D7A1788E08EF4BEFDC218D0E4641E391C1E98C8FAD78DEA71F91AC0B5B79711960D1E074F6FCE9C513267A16ADE23D20DAA40F1C11C93A12588CE0770063FCFB5731A5DEA89BC9460208D8BA9E801F607A76FCAB76CFC202FB4BF3A589269C271183871F43D2B1A1D2E159C5BCF13A12FB4639FC0D368516AC7A96937D147A55B1B976123203856E31D077F414554D30CE9611ADA4A0C1FC23938F51D38E73C51480E4EDFED0FA499AE247794A6E6773D06338ACEF0E78CB4E8A086D1ECE69E7761E7CEF27CD9EFB3D056C78CAFE1B586F2D21E15D76E3DB15E3D1C8F6F7292AE41460463DAB2A2AE9B37AFEEB491F55EA7E1EB3D574C8350898CB6AD180E54729EBC7E99AC71A658C9751D922F9A878CA9E9593F0EFE223DA69E23BC1BE0238040CE3DEBD1AD23D1B526FB7D8158A775DCC9FC24F5E3D0D12BF4221CAF7394F106A23C23069DA7436EF2C778E6058D549C9E3278236924F5E7A5693DF5B68510F36296E6E48DB14110DCCC38EA4F41EE6B2758BFD4AFF005A3636D1A79E253E5E7927A703E838CFF91CF78ABC5B158EB535B99556789BCB900E704707F0CE6B448972474635FB7BFBA7B79ED1AC6ED06F10BBAB061DF691D6B2EFF56920BA2B6A43B1396C81D3EA7B0F5AE5C6A49A94B04DE6EFB90FF2153C8E39FC3155A4BB86F6C5AD2194457AE06D908CE3DAAAC4A773B77BB59E392694C72CACA1CB44E09E0F2720FA0E9CD70DAAE8F6D6FA8349711E111C9240CE7273918AE8B48B29AC74A8BED33FDA2F327CC61CEFDDC60FD33EBEB526A764B7091273F2FCBCE7F5FD68D84CB3A3489E4836EC1311E4646E0CBD31EDD38AE7AC629353D4265601DF7178E65C82467047F9F7AEAF4BFB31B159622018C9B797073EC4E292D20B4D1C15800DC739E3BF20FF004A4E690D41B35ED65B3D3EDD609E46120EAC07DEED9FD3F4A2B9B7959DB7375A2B1F6ACD7D923CDFC4D7725D5CC92303824FE55B365E1482F7C256D6F32013B7EFC374209ED9FA629B6BE1A9FC416EF38B886144E144848DE78C81E9F8D76C91A2E11700280A2A5B692B1ABB36CE634CD1D6202255DA53829E95DE691A65D5841F6C8DCB4601CA8E48AE3175DB593C4125A2150570A1F3F788EB5E8DE1DD4912658E46251BA669C9BB19452B99D71732693AC49A8A0549D61253CC190BC1EDFE7A57CF7AE43769A83CF732F98F3BB31727049CF391D6BEADD7F454BFD29AE6D901923E553FBCA7A8AF24F11E9FA6EAC6379A011DE42A239372E0B60601FCAB484EEAE44A3676391F8796925C6B85AE5FCB8E38CB97623E5191EBF5AE9BC410DA25D2CDA6EE504859542E03F5E476AE72F45A4768FA5D960C933032C8A7A63D4FAD5E92F2448E0873BC460063C64E055F310D1D1E9D279768099599CA8270A179FA7E5536A7A8C763A73CCE49609F201D77E38E2B9FB5BD5B7B43349370063E7FE1F6AC0D535D6D46E238E1256DE27DD93FC473827FCFAD1B88E87C3DAB269B332BEF78EE1C3306E30707271F8FE95D5DC292F90DB91B94607823B1AF31B6461B25DCC7073C939F4AEDBC3DA87DAEC8DBB9CBC38239CE01EDF98ACEAC6EAE6B4A5AD8D3DA68AB214628AE737B9C74B16A96FE21834486D24586271CB8DBBCE79249FE55D26AD1BE9B0986598C7772FCA831C03F5AF67B7D36C6E13CE9AD207943603320247D3D2B84F889A1E9C756B5B8FB39F3723912301F96715BB57D4CE2F5B1E1DAFE9F359B40F1B033EE1B42FDECF6AED74AD52EAD255B7BA1B664001F4C9AA66CADE6F13DB7991EFD8415C9270735A1AE469E644FB46ECE33529B692654D24EE8F4B835F8A3D2259EE18948C6481D5B8E00AF22F13F8D61BEBC97CBD3551C7F179B93ED9C0AD7D527963F0DB15720E6BCD75A002C328E24671B98704E45694D18D46549B51BCDCCC2255CD20BFD43E565C21C1E4024D5EB7F9A18B3CFC8FD7DBA53E451C70338EBDFAD6B6465CCCA6567BA03ED53E501C805801F5C669C912C7300C42823A81D41EE2AD46001C765047D79AA11B17196249DCBDE81974388DC9DDF29E0A8E82BA0F0B5CB45792052595D7919EA78E6B988D143A8C0C16E73CF6ADCF0A803510074DA4FEB513F85951DCF45127145530C71D68AE63A4FFFD9, 1, N'B', 0, 0, CAST(0x0000A43301819FB6 AS DateTime), N'ilyas', CAST(0x0000A43301819FB6 AS DateTime), N'ilyas', 0)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (5, 5, N'teguhnoor', N'Instructor', N'Teguh', N'Noor Pramudya', N'123456789', N'Male', CAST(0xCD170B00 AS Date), N'085722844244', N'teguh@sahaware.com', N'Cimahi', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC00011080085006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F4C83518D082634C7B0AD5B6D6AD98EC7076F62C735C6AB63A669E2423D6BD69508C8F3A35A48EDDF53B774222619CF4DA39A58EF265CFDCD83D4F4AE23ED4EBD339140D4A51C6F6159FD555AC8AFAC3EA76B25ECA4EE0E838E3DAAA92D748DBA552C0F35C8BEA6C3F889A80EA9276CD5C70AD6C4CB11DCEB26B128C76CA8703B9AAB2DBB2A80F22951D0135CDFF00694F83C900FBD44F7D2B75727F1ADA3427D5993AD17D0DA793ECEEC4156CF18038AA925CF964B2FCBEC0565B5E3E3FFAF4C7BF761B4818FA56D1A4CC5D42C3334F26E7195CF3814C98C0B2E413B7B0A805EB0CF03F1A64977D8633ED5AA8B33722E79ABFC238FA5159BF6A90719A29F232798D7DEE0F4FCA977BFD2B7174857381736FF893FE1528F0FB374B8B43FF006D2BCDFACC0F57EAF239DCBB771F9D34A6EE338AE90F8765C7135A9FFB6950BE832A93F35B9C75FDE0A6B150EE278797639FF240396614D65519C026B75F449D78C447FDD901FE550BE913A677C6463AE455AC4C3B92F0D2EC61B039E053086F4ADB3A5B639DA29A34A07F8C0FC3FF00AF56B130EE43C2CBB184431E98A6F94D9CF15D3C1A124AD82653EC91E7FAD5893C3F6A91EE31DE67BE5140FE749E369A76B87D526CE3CC4BFC4C7F0A694894E4E48F4AE95F4DB25E364BEF92291EC34EC709700FD01ABFAD448FAAC8E68ECCF0871F5A2B7CE9FA7E7A4DF90A29FD6A1E62FAAC8DF3E22B7272D67093FE7DAA1975EB760716912FD335C91B803F8A986E87F7AB856029FF004D9DEF14CE925D5E261F2C4ABEE09AACDA98AC06BB1EB503DC93FC55BC70914672C533A06D531DE84D6DA23959594FFB2D8AE65A5C9EB5199BE95AFD520D6A8CBEB73475E3C5932104B6FC7F7998FF005A90F8CEE08F97C907FDDAE24CD8F4A8DAE3E952F0149EE86B1B50ECE5F165FC9C79C00F4502B3A6D6279092D21FCEB90BED5EDF4F81A5B89422804819E4FD2B2E3F1C694F16F6B92847F0B2927F4A6B0F87A6EDA20F6F5A6B4B9DD3EA329FE2A89B519F1F7EB9D83558EF2113412EF8DBA11C50D393DEBAA3422D1CCEB493374EA1367FD6515CF79C7D4D157EC224FB791D019D075029BF688FFBA3F2ACB7D4909F9401F4A61D45B1F7B8F7AE748E935BED11FF00707E54D6B94FEE2FE558ED7FF434C37C4D52892CD9FB5C7FF3CD3F2A3ED11778D3F2AC5FB5E693ED431CD5589364DC438C796BF95676AF73E56997135B9549225DE3E5CE40E48FC7A55437447AE2A1B875B8B69217E55D4A907BD292D34086FA9E6D7D7179AB4D25E4892382D8CA825507A7B55092278DB0CA4679191D457B7E87A5DAE87616F2DBCC803C797C8032C060F3D4F273CFA541E2DF0F6349967BE314CFE5B794DB7E604F4E493FA62BE4E58CBD47CC8FA5580B4134CE13C117E4DEBDA4F293194FDDAFBD7752411B0215F69EDDEB87D03499F4CD4A66B921248D7CB650C09563838C82474C7E75D37DA47A9FCEBE9B05293A49B67CF62A29546AC583A54CC73F6B3FF7CFFF005E8A83ED5FED1FCE8AEDE697739F95143FB417A2A63EA690DE6EE4902B9F13B8EE7F3A905C377E7F0AE1533AF94DAFB564F06A417631D7358AB3FB53D6760DC63F1AAE71729B2B739E94EFB411D6B2566DD825BF2E2A2BED505A5BF9832C49DAA33D4D0EAA8ABB17237A236BED231C9C55886379363153B18FF0F5C7AD70D77AA4B716EA98D9BB9600F5AEE7C308D73A441E5F28AA149C67E6EE2BCAC6E652847F76BE67A182C12A934A66B5B48F00489A5C13900609F5E38F407F3ACCD7B5B9AF2E604776318755F315B9247A63A722A3F146B706936C909432DC37DD407031FED1FC7A77AC8B1BB66D31B52D54A421B02CE251FEB064EE6C7271D07E75E2D2A72A9355647B388AF0A707493D6C5B9260AE493F3312C49EA49EE6A33700551D3251AC5E4C0B040A06D04F5C9E49AAF7B27D92F25B7690028D8193D7BD7D1E1B1D06FD93DD1F39570D24B9D6C6AFDA57DE8AC6171C7DFF00D68AF43DAA39F90A3B8F5DE703DA9E6738E1863DEB2DAE1C824B77A8C4CDEA6BC9F6C76F21AFE7B13B8373F5A5FB5337058D6479C7DE9CB3BE786E94FDB0B90D6370C063278AAB721AE235DCD844604F3557CE6047CD4A5F72EC049CB649ACAAD56E362E10B32511898492745FE115674AF106A7A219A2B4976ACA30C8C3233EA3D0D5649B647B78233C0AAD34A49C76AE2694B468E85271778BD4BF36A324F02473B0B8DCFE63799D4367D73E9FCEA482FA39AE337FF3C780075F940E8063A0F6ACA5E31CFBD0CC70055244799B1657D0DB6A65AD959236051B736E0467E9914CD6A7136A20B81CA21359F6C0862E0A8207F1556DECCDB892493DCD4A8AF69CE537EE729ADF685A2B33CC6F5A2BD3F6C72F20ABB1D4F453E99A6671FE34F44F314C8C02A8E981D692770E7EE81B40CE3BD70A96A6C47B8D48992477350671CD4B11C1C74C9FD2A9C82C3DB25B03F2A40FB0E0D216FBA46324F34EF959DB71383CD26F40248636B85999368112191B27B640FE645560D9FA9A9554AAB80D852B827D79CD311003C67A77ACCA1474341E694F038A68EB8A0096072A251FF4CC8AAC383532B05DD9EEA4544BD79A16E37B085B9345349A2B5E62470C85CE78CF4A7373183DFA522F2319E2A4D836EC0D9E33CF15170116312229242FBD0A364A54F3C7F4A606E319E294E0B123D003400EDA0ED3F7403934E73C0239C0C1A41CC6DF4A6A6769C770690132B2ECE7B1FD68D87613DC9A83CCDB91EF4F597E519349A18E61B723D2998A0B71D69BBF140C09F5A4CF0706B734DD262B9B67B8BA0F1C64610A9C127D79AC6BA8961B992347DEAAD80D8C6694649B69742E74A518A93D9905145156644AEBB1F19CD2E4EC0D9E4514525B00D182D823BD19C8E94514C096139908232369FF001A5450173FED6DFC334514864120E41F5A65145310B4F880320C8C81DA8A281ADCBD26A53242B04676C78CE09CF359E7919A28A98A4B62AA49B7A8023D3F5A28A2A883FFD9, 0, N'a', 2013, NULL, CAST(0x0000A45600CBA551 AS DateTime), N'ilyas', CAST(0x0000A45600CBA551 AS DateTime), N'ilyas', 0)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (6, 6, N'aliahsw', N'Student', N'Aliah', N'S Winarsih', N'345676653', N'Female', CAST(0x9D190B00 AS Date), N'', N'aliahsw@gmail.com', N'', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC00011080064006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00D4D5358B5D134E7BBBA6C2838451F79DBD05798DD78EFC45797135C5B4EB6D6F08DC63540401DB2482493FE71535FC977E35F1C9D3ECB2D0A318E3C74C0EADF89FE95DC5DFC186FEC078E3BB6FB4961237A120600AC7DD8EFB951A6E4AE707A77C45F104770AD3CB0CD1F7468C01F98E6BD3FC3FE22B5D7A1F9479570A32D1139FC41EE2BC93FB125D2AF2682E14A4D1F055BA1C7F9CD57D37C45368FE21B69D880B1CA012A3864CE083F855B8A96C438B47D051C191D2A39A32A7A568C2B94071D45655DDEC81E654B77262709FEF719C8FCEB225A055C12CC7000C927B5792F887C6173AE5E5CADACAD16956E08454EB39E996F63D8576FE38D42E6C3C27A84F06003179649EA3790BC7E75C77C32F0E37883565B2299B68104974C4752DD17F2FEB5A46C95C210BB38EBAB02967667E6227804CC4E4904B11D2ADF86FC5D79E15BF4572F269F21FDEC24E78FEF2FA1FE75F4AEB1F0F746D4B4FF23ECCB1B247B6374182BE95F2F78D741BEF0EEBB2E9D7ABD32F1381C3AFA8AA4EFA48D1C16E8FA06DA58AF2D22B9B770F0CA81D187704641A9523CE722B98F85B2BCFE02B3DE7718DE441F4DC48FE75D76DC13593566458CE953121145599532E4E28A5603CFF00E082DA41757DAA5C2932CB218A320AFCA3A9E339EE3B57BADE6AE9670A32C48E186773BED03F435E15F0EF45091EA50BD93C0F0DC2C918994957C03B581E32393F9D7AC698B7BA969B6E9A9B884B44935B8840DBB4FBE3391F5EE29BBDDD8EB514E2AE64F887C39A2F8FF4F79ECEEAD23D4231C4B03E707D1862BE69D7EC2EB4AD566B3B928CD1B901E33956F706BEAE8BC3502CE96425786D4A3BCB0C2E406C15C67D01C9FAE0D79A7C56D27C390F91631C5E46A0CABE4A469C6CDDCB31F5EBEF4E2DA76EE138C5A767B1DDF84AE6E353F09E997B74856796DD4BE463271D7F1EBF8D4F2C180ED8E59CE7F97F4AD8B2B78ADF4E82288058A38D5500E8140E2AB7945AD919BAB2EE3F53CD558E3679DFC4A423C0D78A07DE9221FF009116A8FC30D3B531E1FBBB9B3B6B4925B8B8DE0DC0278000007A71939ADFF8871C63C1F74B27765DBEEDB862A7F879A9436361A6D8CA42BDCD8A4CA0FF00115F91B1F929FC6896891B61D5D3677D0FDA974A29BC232E00C7615E6DE34F0B5CEB9A7DD3EB1E4490C2ACF6F32B1F3138FCB9F4AEF6E351B5F9A26B809BDC7DE520E7D0579DFC5CF15C76DA4C7A359B9FB45E70D8FE18C75FCFA7E35166F446F6E557921BF0B34C9B4FF04C693821E499E4DA7B038C7F2CFE35D8F979AE13C37E2C6D2BC176F717C8B2C51B791184E1D88E83DF8ADED07C6FA4EB930814C96D704E047300371F620E0D5F23776724958D8787E6A2AE98C67A514AC4DCF9EADBC71A8DAEBB0DE30CDBC7F2C90A93874EE3926BE80F0DF88AC35BB584C76B1C90B8CC4ECF903F31C7D3DABE62FB2BBCB222919E6BB9F87BE2AD67C397074E8618A7B5B8977796FF00795B38CA9FE95AD5A76D51D14AA2D62CFA46436F696FF246880F3841C135F367C46D49B50F88B7386FF8F789211EC705BFF66AF5FD57C5125BE952DDCD1B19427C8817183E95F37C97D2DDEB179772B169A477773EF58D3F7A5709FB91F53DE3C19F1034DD72DE2D0E67306A490AA0573C4A3681953DCF7C57733AF15F28DD5B48F7765750E51D154920F41DBFCFB57ACF83FC7DA8B5C5B69BAA30B98A4611ACEC70E99E064F71FAFBD74FB26D5D1CAE449F166E7C8D2F4F833812DD0CFD003FE229DE03F17785750B3B1D0B5A11C37D6647912484A7CDECDFE7347C5DB1F3F46B2BB192209F1C7A91C1FF00C77F5AF07BEBAF32E37639F5A971BC51746563EBFF0013DBBDB6966E6C951D80E094C9FC08AF9A3C57797B7BE24916F10A3C081429ED939CD7D05F0EEF5F5EF85B61BF3E7431185B272494E01FC460D7CD9E25BDB987C517EB749B6713B248339C60E3FA54423EFDCDA527CB623B6D4EE6465B47909821762ABE85B19FE42B6ED1719F639FC6B1ECAD519A4950E43E4E6B6F4D1E606CB64EE2315DF08D918DCF5CF0BF8B6C6E7448C6AB75047751318C998F2E0630DFAFE60D15E553C23CCE9DA8A5EC913A1E910F80FC3A58B7F6772DD4F9F27FF154971E14F0DE8A3FB4DAD0C46DC870FF006890E0E78E0B60F35D15ADFC0447D4798FE5AE4756C67F9035C3FC60D49ACF4CD32D5588171333360F50A07F5615E7DDC9D9B29239FF0015F8EB59B80D6F06CB7B5C1007960923DCE3F962B8CD1E148E45BE36DF68843149A162C073EE0E791D0E6B7B46D36E7C452D8D9229796690A827B2F524FE19AFA0744F09E85A6E95FD9D0C7148E07EF43A60B9F5C1A6DF2E9136514D7BC79E69DA2786B59D321BBB6B3CC4C3A79AE0A91C107E6EA2B493C31A4C3103159853D8EF63FCCD41A7E9F1683E36D5B49B523EC53C6B771206C88DB3B5C7FE835D1B8F93151ED25D1B225049D8CDBB53AA5A358DE0F3A0620943C720E7A8E7AD614DE00F0C962D2E98B9EA4F9F20FF00D9ABA5B74C4C7EB59BE306923D0A658DB6BCEE9003E9BDC293F913494A5DC4A2AFA1CACBF12DBC2360DA77856DD56CE272CED282E849E38CE4F3F5FC2B9993C43A278835992FF5CF0CA4B24E4192686E64539C63254103B7B57BBC3E19D06D7408EC5AC2392DE44C1C05E78EA49239AF29F19FC3EB8F0B4125F5944F71A79195914E4C40F638EDEF57195F735953ECCE934CF0AF85EE6CA39ACAC4794C38FDF4991EC7E6ABF0F83B43841F2EC02E4E4FEF5FFF008AAE27E11EB72DDEA3A869B3CDBFE512C618F3C1C1FE62BD7C4436D3E79A76BB306730DE14D198E4D973FF005D5FFC68AE8CC63345573CFB93A0C5B75F2D1B68CAB820E3DF15E6FF001B6C8C96FA24C081B2678FFEFA00FF00ECB5EAC63DB6AEC072AA48FA8AF35F8C7A9598874EB012235C0732940725571804FD79A94B52D6E6CFC1ED0616B1B8B9972B285F29594E081DF07F2AF44B6D1E3B086E717534AC55B6F9B21723A9EA4D79B7C24D7A1FECE9A09580DB31527A60F6FC08C7E46BB9F16EA0DA36892DE1B8416B126F90C881B03DBD4FA542B3BDCEAB372493B26732DA3CD0F8945F3CEF2AB5B9450F8C8CB67B01E82B49E3F97A5799D9FC54BABE91A44B189D4B9015D8A955CF009E727183D05771E1EF13DAEBE1E211986E635CB464E411EAA7BD1ECE4A37B19D47795CB28A44CDEC6A2D5EC9EF6C4468E5184B1B6E07046181FE95A42102663DAB2AFBC51A05A24826D4ED81419203826928B6669D99D8697A6DA5D68F1D94E1AE1549DC651BBF33DF8AD1BED3ED468D259AC11ADB142A51570A011E95E5FE02F885A2EADA95DA4F72F6D28931046F230F353B1001EBC722BBFD4F57F3F4B9EE236C5A471B3B39E0B003F4A6DA51B3DCDE516E49A7A1E3BE0FD0B4CB2F89F7CDA64A67B54B6628E0150AFB941183F523F035E9F7D1BB58CCB1B9472A76B0EA0F6AF21F87DAC456DE3187ED122A0BB8DD0963C024E47EA31F8D7B65C460C4A7B165C7E757630A8FDE6624FA73CAE1BED0EBC018CD15B0D173D28A2C6772A7883533A1F86AFF00520A19A084B203D0B7419FC48AF94AFAF2E2F6F5A6964792563CB31C935F5478C2C9B50F066AB6A98DEF6EC547A91F301FA57CAB21FB3BB6470DDF1D2B68AF7469EA76FF000CEF676D62748C82A5144A84F51CF3F81E3F1AA7F12FC453DE6B0DA643792B5B4002C912C87CBDFF004E9C573BA06A37BA5CB3CD6C230D2C663323AE4A83D703D7EB55469A669FA90A79350A849CB98E8755F272A35FC3E9B2DC1FEF9DC6BB2F0EDDC96BE26D35E3C92D3A4647B31DA7F435CA69308552413B41C2E7D0715D4F8717FE2A7B261B4F924CD83DF1D3F522BADAB419114E4F956E7A378EB53974BD312180ED92E98A16F45C738FD0578DEA50AC969344A3E6DA76E6BD23C7B7CB7BA4E9D2492462E125759117F87238FD05797EA323265C93B082A7DAA6825C84B8B849C65BA31BC3B7B1D9F88ACA463B15275F9FD39AFA17E25EA175A47C3D668B60FB5011EE53D8F53F957CE5268EE1D5A37DD1B743DEBBD9BC5D71A87821BC3BA940F7891AE2DE73211244C071BB3D40FE5584E849BBD8B84ECAC7137329D8AC8FC05C641FCFF9D7A4F82FE2A5FDA5B5958EAB1A5CDBC4E019B244817A64F50719FD2BCB63B5B853E5BE30A3A0AD2B38B69201E57AFD6B6853BEE8C65A9F598682455955CB248032B237041E8451583E00B8924F02E94CEECCC2365CFB07603F4028AC9D3D48B97B59761A35DB0382236C11F435F30BC6B711BBCA373312C49EE68A2B5A3B308FC455B550136F6C9FE75666FDDA1DBC6EEB4515B2D8D4E92CADE34B7450BC63BD5DF0C9C78CB60036881B03F11451455F819D185FE2C7D4EA7C5F0C6746965DA37ACD1B03EFD3F9579D6A51AB5B4991FC268A2B2C37F0D1AE3FF88BD114ADCEE50A40C7A54B2A8478D97AB1DA7DC5145741C2413011C7232819E6A6B38516DC10392BB89F7A28A68967D11E0D510F83B4954E01B756FC4F27F534514572BDCC4FFFD9, 1, N'B', 2015, NULL, CAST(0x0000A46C0021EEC0 AS DateTime), N'teguhnoor', CAST(0x0000A46C0021EEC0 AS DateTime), N'teguhnoor', 0)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (7, 7, N'asmirandah', N'Student', N'Asmiranda', N'', N'4635645765', N'Female', CAST(0x00000000 AS Date), N'', N'', N'', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC00011080052006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F5D14A2900A5A4028A2802968012B84F1B7C4FD2FC245ACE15FB6EA78FF508D858FF00DF3DBE9D7E956BE23F8C57C23E1C6785C7F685CE63B65F43DDBF0FE78AF98E469AE6679E676795C9666639249F534203A4D73E24F8AB5B99DA5D4A4B7849E21B626351EDC727F135474EF1D788B4CC88354B82ACC1996490B838FAD61BA33100027D2B7EC7C15AACF68D7725BB226DCA8239343B2DC6A2DEC7A7783BE3125D5CADA6B50A42CE7093459DA4FA107A1AF58B5D46DEF23592170CAC3239AF8E9A392D6768E405483822BD77E1FF0088A79EC00321F3A020139EA2A5E9A858F73A4AA5A65F2DF5AAC8BD7B8F435769884A28A2981252D252E6800A09C024F4A2A86BAFE5F87F5271208CADAC84393F77E53CD203E65F88DE286F1378BEEAE11C9B4B7630DB0EDB4756FC4E4D728D39202A8A6DD4BE75D4B200177B96C0E8327A5747A1F84F569618B541A72CF0EECAC52F01C7BF238A77B0D26F63B7F87DE05173047A96A50FCBF7A2561D7DF15E997567125B940A00C74C566F872692DACBCB28F14689B8233642FB027B5646B7E2D16D2AC73EA22DCC848545B6321CF1C647D4562FDE674AF751E71F1174E86D2FA39A350A5C9DD8AA7E01BDFB36BBE431C2CE98FC474AB1E32BB9AF0C724D2473464663963180C3D715CCE99706CF51B79C1C147073ED5A25EE98CFE23E92F0BDE18AF7C863F2B8FD6BB4AF2DD26F36CB6D72A78386AF4F471246AE3A30C8A5164C87514945512494B4D14B400E155355D3E2D5B49BBD3E62445731344C57A80462AD0350DDDD45656935CCEC122890BBB1EC00CD007C8F0E90F63E331A55D286682ECC2E08E1B6B63FA57D05A5DE599B55880C151C281D2BE7FD675C92FBC5736BE836BC9746755F4C3640FC80AF62D2EED753D323BDB171BF0180EC41EC6B3A97BA3A6835668DF79945ACB282BF336CDA0F34DBCD134DBFB759EE218DCE33C806B1E599D9D5E4B15964FF00618FF2349A8DECD6B6AC5A4D921192A5B216B3D8DDC6DB9E6DE3D961172967688A91C209C0E302B86329247D2AEEB77A6FB539A60E5937601F5F7ACF51F30E32335D1156471549294AE8F6EF0ECFE7E81612FAC2B9FAF7AF55D0AE0DC6930B13CA8DB5E39E0F2DFF0008B596E1D9B1F4DC715EA9E1373FD9AEBE8FFD2B38FC40F63A0CD14DDD4568413034B51960A0927815CA7893C67A7694E2DA4D422B773F7989DC40FA0E6803AD795231976007B9AF26F8CBE32860D123D16C2E51E5BB63E798DB25507638E993FA66B8FF00157C405BB91A2D3E69CA0FF969280C5FF03D3F5AF3BBABA9AF25DF73233B7FB47A0A10EC44FCC4A2BD13E1CDFDDDA4122440491EFC1463C74CD79D38DAE57D0D7A37C3D75640898DD9E477A99EC5D3F88F40BBD7648E0C9D2E5F388E366083FAD795F8CF58D4C9104E44025C931AB65B1EE6BD76ED985B96DB8C0EF5E11E289A5BBD5E59E42482C427D01ACE0AECD6A49F298982413DAA4802B32C6C7019864D354601CD203839AE8398F71D2E28A1B182188623440ABF4AF47F0F5B35AE9A0B8C3487763D076AF29F0ADEFDA34CB490F242807F0AF62B597CC82361D0A8358C772D96734547BA8AB24E5FC7DE26B8D034E862B3C0B8B92C039E76281C9C7AF22BE7CD6BCE79DAE1D9A432365989C9CD7A5FC51BF33F88D6DF77CB6F105C7A13C9FE62BCF2F17CE4318EA7A55A5A137D4CA306CC93C815459D84A781927BD6F98259A14811332B9E40EA6AAEB9A3C9A5CEF0CD8F39305B6E704100E7F5C50C68C76CEF27D7935D07866FE6D3AE05CC6708080D58392546DEFC62BB6F0B68D3816EB3ED8A1BD2D0B175CE3D3F911F8D44B634A6B53D66495F50D2E39A05056540C3F1AF17F1158CE2EA6DD095585CA67B673CFF004AF67B05FECBF0F2A265B6652207F88EEC01F9D721E2ED3DA2D11D8619832966C75E793F893514D6A5D56923C8DD7008EF516306AC5E065958540D93B7E95A981DB78475592CC2DB48C3CB278CF626BDC747D5A192C6250DF381822BE76B4211133D40AED7C3DE2136E0453484303C313D6A651B6A869DF467B72CCBB47CC28AE2E0D7FF0072BF3515371D8F3DF1BB3378A751CB13FBD6EA6B99FF0096ABF4A28AD96C66745E1A553E20B7CA83F7BA8FF64D5BF8A88A2285B68DD94E71CD14527B0D6E798DA7FC7E41FEFAFF003AF68BE554F0D698554021E3C6063F88514565337A7B1D45C01F66D28638F33FF64358DE3103FE11EB8E3B0FFD0A8A2AE96C6757E23C4B52FF00583FDE35553EFC3451544A35A3FBD5722277753D68A299275562EFF644F99BF3A28A2B32CFFFD9, 1, N'B', 2015, 0, CAST(0x0000A44B00619AA2 AS DateTime), N'ilyas', CAST(0x0000A44B00619AA2 AS DateTime), N'ilyas', 1)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (8, 8, N'tegar', N'Student', N'Tegar', N'Amal Firmansyah', N'34653547645', N'Male', CAST(0x00000000 AS Date), N'', N'', N'', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108004B006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00D65C71C552D4B59D374A8DA4BBB88D197A216F98FD075AB89DABCFB55B3B7D5BC561EE14BA07D98071900E2AC81350F1F6A17EC61D2A028A780D8E6B1A4D2F54BD613EA370C0123E563C9AF64D57C2361A669F04F6914505B48A368418278EE7935C65FC30C4802ED196EA4F2695C96CF41D1D42E93663FE98A7F2AD2502A869E36E9F6C3D225FE557D682C9540A905440D3C1F7A4048314BB80A654174B2496F2246DB5C8F94FBD0059F3076349BAB3ECE19608996675662D9CA8C62ACEFC50B55A812EFA2A02FCD1401C9860A858F619AE021667BF6917861CE4FD6BB5BE97CAD3AE5FB889BF95713A7FCD23B76CE2A988DF5BA9A60A2595D940E06EAAF7DB446A70323A934B11F947D2AB6A9992D9D14F254807DF152413EA7F166D34D75B4D3ACFED662015A567DAB903B70735B3E11F89567E23BD163716E6CEED86631BF72C9EC0E3AD790698F040E3300697693F741E8335A76FA983A95ADD476CB15C5B481D1D8E327D381593ABADAC74FB1BADCFA295B34FDD5C8691E2B179708B3A08D1D140E7386FF03C57522418EB5719292BA32945C5D99386A4635187069ACD8A621CCD5196A6B3D465A98C796E68A84BFBD14C4717AE4A23D12E4FA803F322B93D3B884B7A9ADCF144DB346C67EFC807F33FD2B02C0816CBEB4D899B0870A7E9515C3FEF235C80A48CFE748AF806B9DF17DECD6F6491C5C094ED66F41E9504F528EBB7DA7DBF882E7FB1A512C21C956DBC027A81EA3AD635DEA73798A50ED65F6E3F2ACE80A87F9DB031D69D2C8AE42A7DD1DFD6B3E45CC7573BE4B5CEA7C3BAD5F0B7BB779E168ED62326257C339ECAA3A935BBE15F8A3756B75243ACEF9EDA4398D907CD17B7B8AF3A82431923B7AD2160242471F4A6A2A2DD9156528ABB3E9CD1B5FB0D72D7CFB19B785E1D4F0CA7DC56897CD7CE3E1CF11DE787EFC5D5B3E411B648DBA3AD7BA68FAD5B6B5A747796CF9561865EE8DDC1A69935A8F27BD1D8D667F7A85A4E6985F8A8D9EACC097CCCD155F7D1401E7BE2E971636E9FDE727F21FF00D7ACCB3E208FDC0A97C5F2E64B68F3D149C7E5FE155229308004638A6C96696F000C1E335E7FAFEA8752BD6DA7F731FCA83D7DEAE6AFE209CDC35BDA9F2D10E19B1C93DEB9E3CB7D6A59708D95C18700D011B6EEDA76838CE2BB5F0158D95C4D733DDC514862DBB7CD19551CE4F3591E30BA82E7C4B726DD6310A054531F46000C1A0773081AD3D2F42BED6A2B87B14591A0DBBA3DD8620E7919FA565135D3781F50367AF2C458AC772A50F3DC723FC3F1A077B1467F0F6B566374DA65CAAFA842C3F4AEF3E1643A8C37178F2C32C764D1800C80805F3C633ED9AECEDE41C64E71EB57165F4A490FDA4AD62DB495196F7A85A4F7A6992A8CC977628A83CCCD140B53CCFC59216D5234FEEC607EA6A2F37CAB77761F2AA927150F88D98EB8F93D36E3F4A8EF188D2A720F3E59FE545C4D6A718EE6495E43D589269B9E7347F0D276A46DB236745D78E911CC9E409964182A5B00FD6B36EEE05D5DC932C4912B9C88D070A3D0557A282433535B4ED6F7514E9F7A370E3EA0E6A1A55EB40CF76B1BA59ADE3954E55D430FA115A092722B92F0E3B3685624924F922BA38C9DABCD224D0DF9028DD508276D3D4F14D8AC3F75151127D68A9B8EC7FFD9, 1, N'a', 2014, 0, CAST(0x0000A43400108E6C AS DateTime), N'ilyas', CAST(0x0000A43400108E6C AS DateTime), N'ilyas', 1)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (9, 9, N'coba', N'Student', N'asda', N'sadas', N'sadasd', N'Male', CAST(0x00000000 AS Date), N'', N'', N'', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108004B006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00D7583DAA54833DAA7BA636B6735C089A5F2D0B6C520138FAD71EDE24D613528EF23D30CF6AC9B1A189DBE519CEE27A679F4AF4278B8C1A8BEA72D3C34AA26D743AF5B73E9532DB9F4AB56605CDAC3388DD04881C2BE32B919C1C77AB8B6DED57ED8CF90CE5B7F6A996DFDAB452D7DAA75B4F6A97547C866ADBFB54AB6FED5A6B69ED52ADA7B54BAA3503316D8FA54AB6DED5A8B6BED4F16DED52EA94A2658B6F6A78B6F6AD416FED4EF22A3DA95CA65FD9BDA8AD5F20514BDA0729E576DA76B5A5E8BA7E9F7F796D706E55A1DF186CAA950A393D48273FA525C783AFB4F4665D55993CA64D8015193D0F5ED5B5258EA73DCC2CD0C8618A44280B0F946D5CE39F506B4F57B9677F2D61262D849933C86CF0318FAF35F33571129F2C9CB547D0D2A3185E0968CE013C5D7F617091C11C37514712C6CA64C1C85C0208E3AFB54F61F1360916E5658D7CE8A30046582FCE38639E723358F7961058BB3476E212CE776D5E08C0C7F5AF38D445F69D797139431A4CECAAFC7209CFF2AF6E33E6829459E354A7CB369A3E8DF05F8893C41A62F9AC3ED899DEB8C646700FA7E44D75A918F4AF953C25E2DBDF0FEB70DC5B9CA9CA32919041F6EF5F45E8DE2FB2D5A79E0898EF802163D03646738EA39CF06A2755C7708D2E6D8E99631E9532C62B34EA90A0059D54640E4E39A906A710FE2159FD64BF60CD10829DB05677F6A45FDE141D522FEF0A5F582BEAECD1D8290A8ACD3AAC7FDEA636AD1FF7A97D60A587669E0515907574CFDEA2A7EB03FABB2969D75F68B2472474AA9A9C91C71BBBB05551924F402BCD6CBE27D9585B30920B9755E309B49CFE75CA6BBE259758BEB9B92D7102DDC6311873C463B1C1EF81F9D799EC272766AC8F5155847DE4CDDD73C4F6D337FA3DB3BA6376F7217233D8726B94F125C5BDF68E1A3E248A45668C9C300411F9722B067BD7F306C76000E01E8476A82F2EDAE5A369300A0DBC0EDD47F3AF62846708F2BD8F2B11285497375123D3AF6496316F048ECD820229273F8569DA6A3AA69B70D37DA2E60938462AE5738E0027DAB434BBA692CA248A2F9D5C296EFC81DFF0A875AC5CDC821B1186C84CF03D6B3559F3F2491A7B05C9CD163AF35EBFBD82DFED17924BF6721A10C436DF73DEB5ACBE226A96B7D2DE4D0C7319502B00E4038EFD4E2B8AF3981DBB4673D0714E372E5B950B8EC2BA7917639D4DAEA7A38F8AF7070059027D379FF0ABD1FC4D85F6878A58C91CEE0303F5AF26F39988CA8C8E9C53CDD96508E5B238CF6A1421D87ED67DCF5DB9F1A6A1BD45B44BB0F3973F7BE9CD38F8C6E8C2C5A3292E385EA335E422E190A9590E41C8C1C62AD5B6B37D6EC4A5CBB03D55C961FAD68A14BAC49F6B57B9EA5178C2E767EF636DF9E7674A2BCBE4F10EA0CE4F9A83DB65157C947B0BDAD5EE518259ADA41323A861C8EF568EA21E24C67CD008C9E719CE6B294B746E9EB4FE8C9B4E0743C562E9C5BBB295492D112B9578C11C6D240F5AAD2B9190473DA9F801B24B668601B0ADBB3EB9AAB11726B7BC92025E39183E07DD6208C54A2F1E5DA4CAC48F5AAE91ABB85192C780054C6D9EDDC12B9C7254FA54B8C6FA94A525B156E09DEC77739CE69CC4ECCEEFC6A5656BA724B041EC3F955868D16211C51877231B88E2AC932CBB062377E5522B304C904834B7313DB4857F886376DE9491DC807326E2718CD201013DB81EF4FCB2E3A7E74E8E54C160DB71EBD6A540195991810383DB228B815D98EEE868A94B0071CD145C081B6061F37029A58F6FCEA7D8A48E077A6B80AA00E053023DCCCBB768CFB726A582CE4B86DBD1B2073D2AF59431FCA76F7FE86A7B97286255C01B98E001594AA34EC8D634D35764D69670DA484C6C92C8A40723A7BE2A39BCC9E491D2364C020003EF71CF34E878DB8FE27E7DF038AB32B157545385C138AE6726A573A14538D8C7B881A14C330F300076E0E47D6A909D9385CF078E2B7ADD41B994E391FE159B7A07DA7EEAF53D8574C27CCECCE79C3955D1147303BDA4DA4B773D735112928E7033D2A17F94B01C7352443815A246435EDF69C06EBD0114C2AEB18CE703A548CC5653838A9473C9EB8269D8440B71220C649EFD28A9300761451619FFD9, 1, N'B', 2014, NULL, CAST(0x0000A46C009AC02E AS DateTime), N'teguhnoor', CAST(0x0000A46C009AC02E AS DateTime), N'teguhnoor', 0)
INSERT [dbo].[Prefix.UserProfile] ([ID], [UserID], [Username], [Rolename], [Firstname], [Lastname], [UserIdentity], [Gender], [Birthdate], [Phone], [Email], [Address], [Photo], [IsStudent], [Class], [ClassYear], [Score], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [isOnline]) VALUES (11, 11, N'mardika', N'Student', N'Mardika', N'Reza', N'1234', N'Male', CAST(0x30170B00 AS Date), N'', N'mardikareza@mit.com', N'', 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108004B006403012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00E952A640474A5451D854DB4E3815ED1E328319F377A0900FAD53D7975587C3D7973A55A4D7378ABB61489377CC4F5C7B75AF3CF063F8CF55D5AEACF517BB48E2C195EE73188F24F000009271C61801835CD2C5414DC126DAFF0087EA6D1C2CA51E66EC8F500D97099018F20679A98AE054567A45BD9731A1690FDE91CE589F5CFAD56D55356B18A4BBD3E35BF551B9ACE43B58FF00B8E3BFB1073EA2B7BD95D99FB2D6C8B8C4118A410679AE17FE16BE9705CDB4779A7CD047281E63870C62CFAAE33FE7A57A2A245756C92C4EB24522864746E1811C104511A9197C2C7EC64B59228CED05AC465B8952288757918281F89A42AAEA1A221948C820E411543C53E0CB7D56DAC9EFEF26B4B14946FF002E2525B7703E623239207248E7A56345AF5A7FC2C3B5D2EC6E9E589E078A40CE48CA8DCA7D33C119F7AE4A78B752A38452B26D377D9E9F79BCB0D18C399BD5EDA1D2B44C7AD412400F6AD66894D30C09DC8AEEBA39FD918C6D467A515A8D1479EA28A2E2F628D08ECE11C0E4D5B4B242B8005528EE0EEC81D6AEA4B26D04719ED58BB9D89216DAE041AC3692ADB435B7DA588EBB776D3FAE3F5A82DECADD2EA5F29362C873C9EF5CAF87FC4D16B1F1035B643BA2B4B55B58CFA90E771FA647E95D4C63ED3BE3DC54382B9538233DC1AF2E786E7C42ACDEB1DBE7B9D31A96A7C8B665D4B0CB7DE14FFB122B7279AE074CF17788B4F8766BDA742CB1B146B886E111B8E39562013F43F856B45E39D2AEF544D3EDA769246566797A2A05193F5AF4154BABDCE7B743CFFE2F782B4B80C9AED9491C77721027B7538DC7FE7A003BFAFAF5F5AE93E09DC9BCF0A3584AC4FD9A7DB1E4F215B9C7E79FCEBCBBE2078826D535A98472B1B656C2F3E9C56BFC36F108D0EF4ACF70B14773132F27186C654FD78C7E359462EEE4B71CA7B27B1EE9F11BC83E03D66060430B3778F69C6D641B94FE0541AF9C7C1774D6FE3ED12EA65DC2797CACB7ABA9407F36AF7AD6E5935AD267859B26681A3CFAE4115F33ADF5C695A9C1E644C93D9CE8FB5B82AC8DD3F4ACA8D2749CB9BAD8A9BE66AC8FAACDA2ED24F6AA8F680B7278AD01776D736905CDBC9BA299164423BA91907F2348CF14BF74600EB5DAA4D91CA8CC3690E7AD1574DBDBB1CEF3453E60E545A4D267DBC22AD66EBBF69D2B45BBBAC85744C464F4DEC76AFEA457611C9838241158DE2448EF2E746B2201596F964917B6D8D59F9F6DC17F3AE4F6D2B9AF2AB1E61E0EF0D0F0A5DDF452EE33CDE5B3173961F206C1F7CB1AB5E2AF1AC7E12D39A645592F26CAC11B74CF763EC38FD2B735C3BF51D4A78D490974006ED8F2D063F353F9D711AD785A2F126BBA7DEDDDC62D2DC625831CC9CE719EC0F7A7CCB9056D4C6F0BF85FC53E3D9D752B8221B46258DD5CA9C4849E7628EA3DF81EF53F8AFC243E1EFEF9AFD6EAE6F91829552BE5AE41200C9EBC73EDD057BD682122D22011C6A91608455180AA0E0003D38AF1BF8F170BFDB765167212D4363D0966FF000A98C9B7626515157478A5E5C3CF3124F7CD588FED12C626891D92251B8A8C85C9E2B3DCEE91BDEBD9FE1A78160F12784B50B7B87684DD4595995726360C0A1C77E509C7A1ABE6693686E2B4465782BE233DBF97A66AEDBA03858E72794F66F51EFDBF9707E28B8377E29D4E70D95370F83EA03103F95767E3DF01CFE1F70E9224B3C1127DA0C6BB44BF28CB81D8F5CD79C13BA4C7AD373E68845599F4FF00C2C32EB3F0DF4B983296855ADDBDB631007FDF3B6BB086CBECCDF3E189F415E59FB3A6A921B6D734776CA46F1DCC633D0B02ADFF00A0A57B818D73920564EB497BACBE54F54511042C33E50FCA8ABDB94718145473B1F2A33A2B9B7C61648FFEFAACFB878BFE121B2B86913CA8EDA75C93C6F668B1FA2B7E75E6A92F1C54DF680885C9FBA3268E60B1BDACEB10C1A2FD98042F33BCD2B818F9C926B90B1B87D4F5286CAD64506470BBD8E00E7A9F6AC2F106B1E6DC416ADB849E486D801CE00C9CFD2B53C2B1021EE49C81C29F7AD65A2B18F3734EC7B7DAB5ADADA476E93A308542E4B75C77AF9E3E355DA5D78D265593291431AE01E33B73FD6BD044BCE2BCB7C716C6EB50D56F1B8512C31839F48871FA0A9A7AB2EA688F3B8977CDB40C9CD7D65F0D2DA1D1FC1F6B048E8B70DC382707818FE60FE75F2E6848FF00DBB66CA33B6E131FF7D0AFA1210D1C11A67255793EA6AA4D286A0B599BBE3ED286A7689A8DA94964B705658D4E4B27A8F5C735F396A5A4C163E22B5F2D58D8DCC8BC631B72D865FC3A8FAD7B53DF85DC024DF29E7119FD3D6B91F19471DB4697A11712B6DE78DB2763F8FF00315106AE8734D26D14FE0BEA51E85F136E74D9A5056EA192DD5BB1752181FC95BF3AFA45EEE21D6441F8D7C676375259F8AEDAF590EF4BA0580F73C8CFAE0D7B53DCC847DF6E7B6689A4A409DE28F5DFB643FF003D53F3A2BC6CDCC99FF58DF9D1537431A920F4A903F190DB4F6354518F1CD4AA4ED15170389B0D41344D6350BBD5E6BA7BCB888A44F246A40F51C13EDD874353F83BC4F0697A31B3D4D66DE246688A2798369C71C679CE7AFB57658E9F5A685563F3229E7BA8A6A57DC2C427C4F608D17D9F73C67EF3125028FA35701ADEAF0EB1AEB59FDA163B47BB2EC49206DDA83AF3E8D8F4AF46FB3C2EBF34319CFAA8A468A257DA23400F6DA2852B6A85BEE79B5DDEE97A5F8AB4E6B18E25B5876798EA4B63E620B1E392060D76ABE2ED359448B7F34B81C0488819FCB35AA74FB3F318FD961E87F8052C76B6E14E208FFEF91494AFB8356D8C1B8F16E9E5B3B6EC8CE4FEF48EDD7033F9562F883C45A7DFE953D9C56D74CD2600760CDDC1EFD3047E95DC98635FBA8A3F0FAD42D146AA0845049F4A7703CD34A9A1FB15D477B6C64334BE66E7462571F77182307AF3EF5B87C40B228412DC44C176EEF2C93F80DD8AEAD8938FF67A7B544490C807420FF4AA4EFB92E2FB9C99C5CFEF1AEEFC9E848B72DFAD15D4A9C8C9A29732EC4D99FFD9, 1, N'a', 2015, 0, CAST(0x0000A46B017445B2 AS DateTime), N'teguhnoor', CAST(0x0000A46B017445B2 AS DateTime), N'teguhnoor', 0)
SET IDENTITY_INSERT [dbo].[Prefix.UserProfile] OFF
/****** Object:  Table [dbo].[Prefix.ListPC]    Script Date: 04/28/2015 16:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prefix.ListPC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PCName] [varchar](150) NOT NULL,
	[IPAddress] [varchar](30) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsOnline] [bit] NOT NULL,
	[MacAddress] [varchar](max) NULL,
	[RolePC] [varchar](50) NOT NULL,
	[LoggedUser] [int] NULL,
	[RoleUserAccess] [varchar](50) NULL,
	[Group] [int] NULL,
 CONSTRAINT [PK_Prefix.ListPC] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Prefix.ListPC] ON
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (1, N'MIT-1', N'192.168.1.119', 1, 0, N'FCAA14552EAE', N'Instructor', NULL, N'1;2;3', 5)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (2, N'Teguh-PC', N'192.168.1.104', 0, 0, N'5A9423C6056B', N'Supervisor', 4, N'1;2;3;4', 2)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (3, N'Teguh-PC', N'192.168.1.106', 1, 1, N'5A9423C6056B', N'Pseudopilot1', 7, N'1;2;3;4', 4)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (4, N'MIT-1', N'192.168.1.119', 1, 1, N'FCAA14552EAE', N'Pseudopilot2', 6, N'1;2;3;4', 4)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (5, N'Pseudo Pilot 3', N'192.168.1.120', 0, 0, N'AC8112050723', N'Pseudopilot3', NULL, N'1;2;3;4', 4)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (6, N'Pseudo Pilot 4', N'192.168.1.120', 0, 0, N'AC8112050723', N'Pseudopilot4', NULL, N'1;2;3;4', 4)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (7, N'Teguh-PC', N'192.168.1.118', 0, 0, N'5A9423C6056B', N'TWRController1', NULL, N'1;2;3;4', 2)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (8, N'ADC Controller 2', N'192.168.1.120', 0, 0, N'AC8112050723', N'TWRController2', NULL, N'1;2;3;4', 2)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (9, N'Teguh-PC', N'192.168.1.118', 1, 1, N'5A9423C6056B', N'APPController1', 8, N'1;2;3;4', 3)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (10, N'APP Controller 2', N'192.168.1.120', 0, 0, N'AC8112050723', N'APPController2', NULL, N'1;2;3;4', 3)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (11, N'', N'', 0, 0, N'', N'Visual1', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (12, N'Visual 2', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual2', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (13, N'Visual 3', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual3', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (14, N'Visual 4', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual4', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (15, N'Visual 5', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual5', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (16, N'Visual 6', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual6', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (17, N'Visual 7', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual7', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (18, N'Visual 8', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual8', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (19, N'Visual 9', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual9', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (20, N'Visual 10', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual10', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (21, N'Visual 11', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual11', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (23, N'Visual 12', N'192.168.1.120', 0, 0, N'AC8112050723', N'Visual12', NULL, NULL, 1)
INSERT [dbo].[Prefix.ListPC] ([ID], [PCName], [IPAddress], [IsActive], [IsOnline], [MacAddress], [RolePC], [LoggedUser], [RoleUserAccess], [Group]) VALUES (24, N'Zoom', N'192.168.1.120', 0, 0, N'AC8112050723', N'Zoom', NULL, NULL, 6)
SET IDENTITY_INSERT [dbo].[Prefix.ListPC] OFF
/****** Object:  ForeignKey [FK_Base.SimulationPlayer_Base.Simulation]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base.SimulationPlayer]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationPlayer_Base.Simulation] FOREIGN KEY([SimulationID])
REFERENCES [dbo].[Base.Simulation] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.SimulationPlayer] CHECK CONSTRAINT [FK_Base.SimulationPlayer_Base.Simulation]
GO
/****** Object:  ForeignKey [FK_Waypoint.Ground_Base.Scenery]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Waypoint.Ground]  WITH CHECK ADD  CONSTRAINT [FK_Waypoint.Ground_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Waypoint.Ground] CHECK CONSTRAINT [FK_Waypoint.Ground_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Airport_Base.Scenery]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base_Airport]  WITH CHECK ADD  CONSTRAINT [FK_Base.Airport_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base_Airport] CHECK CONSTRAINT [FK_Base.Airport_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Terminal_Base.Scenery]    Script Date: 04/28/2015 16:27:29 ******/
ALTER TABLE [dbo].[Base.Terminal]  WITH CHECK ADD  CONSTRAINT [FK_Base.Terminal_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base.Terminal] CHECK CONSTRAINT [FK_Base.Terminal_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Simulation_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Simulation]  WITH CHECK ADD  CONSTRAINT [FK_Base.Simulation_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.Simulation] CHECK CONSTRAINT [FK_Base.Simulation_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Exercise_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Exercise]  WITH CHECK ADD  CONSTRAINT [FK_Base.Exercise_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base.Exercise] CHECK CONSTRAINT [FK_Base.Exercise_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Appron_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Appron]  WITH CHECK ADD  CONSTRAINT [FK_Base.Appron_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base.Appron] CHECK CONSTRAINT [FK_Base.Appron_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.Scenery_Base.Airport]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Scenery]  WITH CHECK ADD  CONSTRAINT [FK_Base.Scenery_Base.Airport] FOREIGN KEY([AirportID])
REFERENCES [dbo].[Base_Airport] ([ID])
GO
ALTER TABLE [dbo].[Base.Scenery] CHECK CONSTRAINT [FK_Base.Scenery_Base.Airport]
GO
/****** Object:  ForeignKey [FK_Waypoint.DepartureRoute_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.DepartureRoute]  WITH CHECK ADD  CONSTRAINT [FK_Waypoint.DepartureRoute_Base.FlightRules] FOREIGN KEY([FlightRulesID])
REFERENCES [dbo].[Base.FlightRules] ([ID])
GO
ALTER TABLE [dbo].[Waypoint.DepartureRoute] CHECK CONSTRAINT [FK_Waypoint.DepartureRoute_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Waypoint.DepartureRoute_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.DepartureRoute]  WITH CHECK ADD  CONSTRAINT [FK_Waypoint.DepartureRoute_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Waypoint.DepartureRoute] CHECK CONSTRAINT [FK_Waypoint.DepartureRoute_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Waypoint.ArrivalRoute_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.ArrivalRoute]  WITH CHECK ADD  CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.FlightRules] FOREIGN KEY([FlightRulesID])
REFERENCES [dbo].[Base.FlightRules] ([ID])
GO
ALTER TABLE [dbo].[Waypoint.ArrivalRoute] CHECK CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Waypoint.ArrivalRoute_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Waypoint.ArrivalRoute]  WITH CHECK ADD  CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Waypoint.ArrivalRoute] CHECK CONSTRAINT [FK_Waypoint.ArrivalRoute_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.Aircraft] FOREIGN KEY([AircraftID])
REFERENCES [dbo].[Base.Aircraft] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.AircraftLivery] FOREIGN KEY([AircraftLiveryID])
REFERENCES [dbo].[Base.AircraftLivery] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Appron]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.Appron] FOREIGN KEY([ParkingID])
REFERENCES [dbo].[Base.Appron] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.Appron]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightRules] FOREIGN KEY([FlightRulesID])
REFERENCES [dbo].[Base.FlightRules] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightType] FOREIGN KEY([FlightTypeID])
REFERENCES [dbo].[Base.FlightType] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.Simulation]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.Simulation] FOREIGN KEY([SimulationID])
REFERENCES [dbo].[Base.Simulation] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.Simulation]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Base.SimulationPlayer]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Base.SimulationPlayer] FOREIGN KEY([PlayerID])
REFERENCES [dbo].[Base.SimulationPlayer] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Base.SimulationPlayer]
GO
/****** Object:  ForeignKey [FK_Base.SimulationDeparture_Waypoint.DepartureRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationDeparture_Waypoint.DepartureRoute] FOREIGN KEY([RouteID])
REFERENCES [dbo].[Waypoint.DepartureRoute] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationDeparture] CHECK CONSTRAINT [FK_Base.SimulationDeparture_Waypoint.DepartureRoute]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationArrival_Base.Aircraft] FOREIGN KEY([AircraftID])
REFERENCES [dbo].[Base.Aircraft] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.SimulationArrival] CHECK CONSTRAINT [FK_Base.SimulationArrival_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationArrival_Base.AircraftLivery] FOREIGN KEY([AircraftLiveryID])
REFERENCES [dbo].[Base.AircraftLivery] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationArrival] CHECK CONSTRAINT [FK_Base.SimulationArrival_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationArrival_Base.FlightRules] FOREIGN KEY([FlightRulesID])
REFERENCES [dbo].[Base.FlightRules] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationArrival] CHECK CONSTRAINT [FK_Base.SimulationArrival_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationArrival_Base.FlightType] FOREIGN KEY([FlightTypeID])
REFERENCES [dbo].[Base.FlightType] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationArrival] CHECK CONSTRAINT [FK_Base.SimulationArrival_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationArrival_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationArrival] CHECK CONSTRAINT [FK_Base.SimulationArrival_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Base.Simulation]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationArrival_Base.Simulation] FOREIGN KEY([SimulationID])
REFERENCES [dbo].[Base.Simulation] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.SimulationArrival] CHECK CONSTRAINT [FK_Base.SimulationArrival_Base.Simulation]
GO
/****** Object:  ForeignKey [FK_Base.SimulationArrival_Waypoint.ArrivalRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.SimulationArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.SimulationArrival_Waypoint.ArrivalRoute] FOREIGN KEY([RouteID])
REFERENCES [dbo].[Waypoint.ArrivalRoute] ([ID])
GO
ALTER TABLE [dbo].[Base.SimulationArrival] CHECK CONSTRAINT [FK_Base.SimulationArrival_Waypoint.ArrivalRoute]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Base.Aircraft] FOREIGN KEY([AircraftID])
REFERENCES [dbo].[Base.Aircraft] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Base.AircraftLivery] FOREIGN KEY([AircraftLiveryID])
REFERENCES [dbo].[Base.AircraftLivery] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Appron]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Base.Appron] FOREIGN KEY([ParkingID])
REFERENCES [dbo].[Base.Appron] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Base.Appron]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Exercise]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Base.Exercise] FOREIGN KEY([ExerciseID])
REFERENCES [dbo].[Base.Exercise] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Base.Exercise]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightRules] FOREIGN KEY([FlightRulesID])
REFERENCES [dbo].[Base.FlightRules] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightType] FOREIGN KEY([FlightTypeID])
REFERENCES [dbo].[Base.FlightType] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseDeparture_Waypoint.DepartureRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseDeparture]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseDeparture_Waypoint.DepartureRoute] FOREIGN KEY([RouteID])
REFERENCES [dbo].[Waypoint.DepartureRoute] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseDeparture] CHECK CONSTRAINT [FK_Base.ExerciseDeparture_Waypoint.DepartureRoute]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.Aircraft]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseArrival_Base.Aircraft] FOREIGN KEY([AircraftID])
REFERENCES [dbo].[Base.Aircraft] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] CHECK CONSTRAINT [FK_Base.ExerciseArrival_Base.Aircraft]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.AircraftLivery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseArrival_Base.AircraftLivery] FOREIGN KEY([AircraftLiveryID])
REFERENCES [dbo].[Base.AircraftLivery] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] CHECK CONSTRAINT [FK_Base.ExerciseArrival_Base.AircraftLivery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.Exercise]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseArrival_Base.Exercise] FOREIGN KEY([ExerciseID])
REFERENCES [dbo].[Base.Exercise] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] CHECK CONSTRAINT [FK_Base.ExerciseArrival_Base.Exercise]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.FlightRules]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightRules] FOREIGN KEY([FlightRulesID])
REFERENCES [dbo].[Base.FlightRules] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] CHECK CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightRules]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.FlightType]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightType] FOREIGN KEY([FlightTypeID])
REFERENCES [dbo].[Base.FlightType] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] CHECK CONSTRAINT [FK_Base.ExerciseArrival_Base.FlightType]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Base.Scenery]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseArrival_Base.Scenery] FOREIGN KEY([SceneryID])
REFERENCES [dbo].[Base.Scenery] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] CHECK CONSTRAINT [FK_Base.ExerciseArrival_Base.Scenery]
GO
/****** Object:  ForeignKey [FK_Base.ExerciseArrival_Waypoint.ArrivalRoute]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.ExerciseArrival]  WITH CHECK ADD  CONSTRAINT [FK_Base.ExerciseArrival_Waypoint.ArrivalRoute] FOREIGN KEY([RouteID])
REFERENCES [dbo].[Waypoint.ArrivalRoute] ([ID])
GO
ALTER TABLE [dbo].[Base.ExerciseArrival] CHECK CONSTRAINT [FK_Base.ExerciseArrival_Waypoint.ArrivalRoute]
GO
/****** Object:  ForeignKey [FK_Base.Aircraft_Base.AircraftProperties]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Base.Aircraft]  WITH CHECK ADD  CONSTRAINT [FK_Base.Aircraft_Base.AircraftProperties] FOREIGN KEY([AircraftPropertiesID])
REFERENCES [dbo].[Base.AircraftProperties] ([ID])
GO
ALTER TABLE [dbo].[Base.Aircraft] CHECK CONSTRAINT [FK_Base.Aircraft_Base.AircraftProperties]
GO
/****** Object:  ForeignKey [FK_Prefix.User_Prefix.UserRole]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.User]  WITH CHECK ADD  CONSTRAINT [FK_Prefix.User_Prefix.UserRole] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Prefix.UserRole] ([ID])
GO
ALTER TABLE [dbo].[Prefix.User] CHECK CONSTRAINT [FK_Prefix.User_Prefix.UserRole]
GO
/****** Object:  ForeignKey [FK_Prefix.UserProfile_Prefix.User]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.UserProfile]  WITH CHECK ADD  CONSTRAINT [FK_Prefix.UserProfile_Prefix.User] FOREIGN KEY([UserID])
REFERENCES [dbo].[Prefix.User] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Prefix.UserProfile] CHECK CONSTRAINT [FK_Prefix.UserProfile_Prefix.User]
GO
/****** Object:  ForeignKey [FK_Prefix.ListPC_Prefix.User]    Script Date: 04/28/2015 16:27:30 ******/
ALTER TABLE [dbo].[Prefix.ListPC]  WITH CHECK ADD  CONSTRAINT [FK_Prefix.ListPC_Prefix.User] FOREIGN KEY([LoggedUser])
REFERENCES [dbo].[Prefix.UserProfile] ([ID])
GO
ALTER TABLE [dbo].[Prefix.ListPC] CHECK CONSTRAINT [FK_Prefix.ListPC_Prefix.User]
GO
