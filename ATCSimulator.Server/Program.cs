﻿using ATCSimulator.Server.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ATCSimulator.Server
{
    class Program
    {
        static Services.Simulation.VisualNetwork visual_network;
        static ServiceHost simulation_service;
        static ServiceHost system_service;
        static int port_server;
        static VisualConfig visual_config;
        static string broadcastIP;
        static int portUDP;
        static IPEndPoint remoteEndPoint;
        static UdpClient client;
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            broadcastIP = System.Configuration.ConfigurationManager.AppSettings["BroadcastIP"].ToString();
            portUDP = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PortUDP"]);
            remoteEndPoint = new IPEndPoint(IPAddress.Parse(broadcastIP), portUDP);
            client = new UdpClient();
            visual_config = new VisualConfig()
            {
                host = System.Configuration.ConfigurationManager.AppSettings["SfsHost"].ToString(),
                port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SfsPort"]),
                room_name = System.Configuration.ConfigurationManager.AppSettings["SfsRoom"].ToString(),
                user_name = System.Configuration.ConfigurationManager.AppSettings["SfsUser"].ToString(),
                zone_name = System.Configuration.ConfigurationManager.AppSettings["SfsZone"].ToString()
            };

            
            simulation_service = new ServiceHost(typeof(Services.Simulation.SimulationService));
            system_service = new ServiceHost(typeof(Services.System.SystemService));
            visual_network = new Services.Simulation.VisualNetwork(visual_config);
            

            Open(true);
            Console.WriteLine("==========================");
            Console.WriteLine("Command -stop to Stop service and -start to Open Service and -exit to close Service");
            Console.WriteLine();
            while (true)
            {
                Console.Write("Command > ");
                string command = Console.ReadLine();
                if (command == "-start")
                    Open(true);
                else if (command == "-stop")
                    Open(false);
                else if (command == "-exit")
                    break;

            }
        }
        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Open(false);
        }
        static void Open(bool isStart)
        {
            if (isStart)
            {
                visual_network.Connect();
                simulation_service.Open();
                system_service.Open();
                Services.System.SystemService.Init();
                SendMessage("Service Is Up and Running");
            }
            else
            {
                visual_network.Disconnect();
                simulation_service.Close();
                system_service.Close();
                SendMessage("Service Is Stop");
            }
        }
        public static void SendMessage(string args)
        {
            Console.WriteLine(args);
        }
        public static void SendUDPMessage(string message)
        {
            try
            {
                byte[] data = Encoding.UTF8.GetBytes(message);
                client.Send(data, data.Length, remoteEndPoint);
            }
            catch (Exception err)
            {
                Console.WriteLine("UDP Send Message Error : " + err.Message);
            }

        }
    }
    public class VisualConfig
    {
        public string host{get;set;}
        public int port{get;set;}
        public string zone_name{get;set;}
        public string room_name{get;set;}
        public string user_name{get;set;}
    }
}
