﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model;
using System.Runtime.Serialization;

namespace ATCSimulator.Server.Contract.Simulation
{
    [DataContract(Name = "WeatherInfo")]
    public enum WeatherInfo
    {
        [EnumMember]
        Clear,
        [EnumMember]
        LightRain,
        [EnumMember]
        ThunderStorm,
        [EnumMember]
        Cloudy,
    }

    [DataContract]
    public class PlaySimulationContract
    {
        [DataMember]
        public List<AircraftSimulationContract> pilots { get; set; }
        [DataMember]
        public ExerciseSimulationModel exercise { get; set; }
        [DataMember]
        public List<PlayerModel> players { get; set; }
        [DataMember]
        public string InstructorName { get; set; }
    }

    [DataContract]
    public class PlayerModel
    {
        [DataMember]
        public string role { get; set; }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string name { get; set; }
    }
    [DataContract]
    public class ExerciseSimulationModel
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int scenery_id { get; set; }
        [DataMember]
        public string scenery_name { get; set; }
        [DataMember]
        public TimeSpan play_time { get; set; }
        [DataMember]
        public string exercise_name { get; set; }
        [DataMember]
        public string traffic_exercise { get; set; }
        [DataMember]
        public string desc_exercise { get; set; }
        [DataMember]
        public WeatherInfo weather { get; set; }
        [DataMember]
        public int wind_direction { get; set; }
        [DataMember]
        public int wind_speed { get; set; }
        [DataMember]
        public int temperature { get; set; }
        [DataMember]
        public int visibility { get; set; }
        [DataMember]
        public TimeSpan time_simulation { get; set; }
        [DataMember]
        public Dictionary<string, bool> runway_lights { get; set; }
        [DataMember]
        public Dictionary<string, bool> approach_lights { get; set; }
        [DataMember]
        public bool taxi { get; set; }
        [DataMember]
        public bool papi { get; set; }
    }

    [DataContract]
    public class AircraftSimulationContract
    {
        [DataMember]
        public UserSimulationModel pilot { get; set; }
        [DataMember]
        public int aircraft_id { get; set; }
        [DataMember]
        public int livery_id { get; set; }
        [DataMember]
        public string livery_name { get; set; }
        [DataMember]
        public string livery_strips { get; set; }
        [DataMember]
        public int route_id { get; set; }
        [DataMember]
        public int flight_rules_id { get; set; }
        [DataMember]
        public string flight_rule_name { get; set; }
        [DataMember]
        public int flight_type_id { get; set; }
        [DataMember]
        public string flight_type { get; set; }
        [DataMember]
        public string flight_level_type { get; set; }
        [DataMember]
        public int flight_level { get; set; }
        [DataMember]
        public string callsign { get; set; }
        [DataMember]
        public int person_on_board { get; set; }
        [DataMember]
        public TimeSpan departure_time { get; set; }
        [DataMember]
        public TimeSpan estimate_time { get; set; }
        [DataMember]
        public TimeSpan fuel { get; set; }
        [DataMember]
        public bool is_departure { get; set; }
        [DataMember]
        public int? parking_id { get; set; }
        [DataMember]
        public TimeSpan? start_time { get; set; }
        [DataMember]
        public string destination { get; set; }
        [DataMember]
        public string origin { get; set; }
        [DataMember]
        public string aircraft_model { get; set; }
        [DataMember]
        public string runway { get; set; }
    }
    [DataContract]
    public class UserSimulationModel
    {
        [DataMember]
        public int user_id { get; set; }
        [DataMember]
        public string first_name { get; set; }
        [DataMember]
        public string last_name { get; set; }
        [DataMember]
        public string user_identity_id { get; set; }
        [DataMember]
        public string gender { get; set; }
        [DataMember]
        public DateTime birthdate { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string pc_role { get; set; }
        [DataMember]
        public string pc_name { get; set; }
        [DataMember]
        public string mac_address { get; set; }
        [DataMember]
        public string ip_address { get; set; }


    }
}
