﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCSimulator.Server.Contract.System
{
    [DataContract]
    public class FlightRouteListContract
    {
        [DataMember]
        public int scenery_id { get; set; }
        [DataMember]
        public bool is_departure { get; set; }
    }
}
