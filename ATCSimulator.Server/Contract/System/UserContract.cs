﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCSimulator.Server.Contract.System
{
    #region User Login
    [DataContract]
    public class UserLoginContract
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string ip_address { get; set; }
        [DataMember]
        public string pc_role { get; set; }
    }
    #endregion

    #region User Register
    [DataContract]
    public class UserRegisterContract
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public int role_id { get; set; }
        [DataMember]
        public string role_name { get; set; }
        [DataMember]
        public UserProfileContract profile { get; set; }
    }
    #endregion

    #region User Update
    [DataContract]
    public class UserUpdateContract
    {
        [DataMember]
        public int user_id { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public int role_id { get; set; }
        [DataMember]
        public string role_name { get; set; }
        [DataMember]
        public UserProfileContract profile { get; set; }
    }
    #endregion

    #region User Remote Login
    [DataContract]
    public class UserRemoteContract
    {
        [DataMember]
        public int user_id { get; set; }
        [DataMember]
        public int pc_id { get; set; }
        [DataMember]
        public string ip_address { get; set; }
        [DataMember]
        public string pc_role { get; set; }
    }
    #endregion

    #region User Profile
    [DataContract]
    public class UserProfileContract
    {
        [DataMember]
        public string first_name { get; set; }
        [DataMember]
        public string last_name { get; set; }
        [DataMember]
        public string user_identity_id { get; set; }
        [DataMember]
        public string gender { get; set; }
        [DataMember]
        public DateTime birthdate { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public byte[] photo { get; set; }
        [DataMember]
        public bool isStudent { get; set; }
        [DataMember]
        public StudentInfoContract student_info { get; set; }
        [DataMember]
        public string create_by { get; set; }
    }
    [DataContract]
    public class StudentInfoContract
    {
        [DataMember]
        public string @class { get; set; }
        [DataMember]
        public int class_year { get; set; }
    }
    #endregion
}
