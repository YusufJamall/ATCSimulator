﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCSimulator.Server.Contract.System
{

    #region ConnectPC
    [DataContract]
    public class PCContract
    {
        [DataMember]
        public string pc_name { get; set; }
        [DataMember]
        public string ip_address { get; set; }
        [DataMember]
        public string mac_address { get; set; }
        [DataMember]
        public string role_pc { get; set; }
    }
    #endregion
    
    #region DisconnectPC
    [DataContract]
    public class DisconnectPCContract
    {
        [DataMember]
        public string role_pc { get; set; }
        [DataMember]
        public string mac_address { get; set; }
    }
    #endregion

    #region ShutDownRestartPCContract
    [DataContract]
    public class ShutdownRestartPCContract
    {
        [DataMember]
        public string command { get; set; }
        [DataMember]
        public string role_pc { get; set; }
        [DataMember]
        public string mac_address { get; set; }
        [DataMember]
        public string ip_address { get; set; }

    }
    #endregion
    
}
