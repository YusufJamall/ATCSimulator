﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCSimulator.Server.Contract.System
{
    [DataContract]
    public class PointContract
    {
        [DataMember]
        public int simulationID { get; set; }
        [DataMember]
        public int playerID { get; set; }
        [DataMember]
        public int point { get; set; }
        [DataMember]
        public string grade { get; set; }
        [DataMember]
        public string instructorName { get; set; }
    }
}
