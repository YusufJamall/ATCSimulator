﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCSimulator.Server.Contract.System
{
    #region Scenery Add
    [DataContract]
    public class SceneryDetailContract
    {
        [DataMember]
        public int airport_id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public List<string> runway_light { get; set; }
        [DataMember]
        public bool taxi { get; set; }
        [DataMember]
        public bool papi { get; set; }
        [DataMember]
        public List<string> approach_light { get; set; }
    }
    #endregion

    #region Scenery Update
    [DataContract]
    public class SceneryUpdateContract : SceneryDetailContract
    {
        [DataMember]
        public int id { get; set; }
    }
    #endregion
}
