﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ATCSimulator.Server.Model.System;

namespace ATCSimulator.Server.Contract.System
{

    #region Exercise Update
    [DataContract]
    public class ExerciseUpdateContract : ExerciseDetailContract
    {
        [DataMember]
        public int exercise_id { get; set; }
        [DataMember]
        public string update_by { get; set; }
    }
    #endregion

    #region Exercise Add
    [DataContract]
    public class ExerciseAddContract : ExerciseDetailContract
    {
        [DataMember]
        public string create_by { get; set; }
    }
    #endregion

    #region Exercise Detail
    [DataContract]
    public class ExerciseDetailContract
    {
        [DataMember]
        public int scenery_id { get; set; }
        [DataMember]
        public string scenery_name { get; set; }
        [DataMember]
        public TimeSpan play_time { get; set; }
        [DataMember]
        public string exercise_name { get; set; }
        [DataMember]
        public string traffic_exercise { get; set; }
        [DataMember]
        public string desc_exercise { get; set; }
        [DataMember]
        public string weather { get; set; }
        [DataMember]
        public int wind_direction { get; set; }
        [DataMember]
        public int wind_speed { get; set; }
        [DataMember]
        public int temperature { get; set; }
        [DataMember]
        public int visibility { get; set; }
        [DataMember]
        public TimeSpan time_simulation { get; set; }
        [DataMember]
        public bool taxi { get; set; }
        [DataMember]
        public bool papi { get; set; }
        [DataMember]
        public List<AircraftExerciseDetailContract> aircrafts { get; set; }
        [DataMember]
        public Dictionary<string, bool> runway_lights { get; set; }
        [DataMember]
        public Dictionary<string,bool> approach { get; set; }
    }
    #endregion

    #region Aircraft Exercise Detail
    [DataContract]
    public class AircraftExerciseDetailContract
    {
        [DataMember]
        public int aircraft_id { get; set; }
        [DataMember]
        public int aircraft_livery_id { get; set; }
        [DataMember]
        public int route_id { get; set; }
        [DataMember]
        public int flight_rules_id { get; set; }
        [DataMember]
        public int flight_type_id { get; set; }
        [DataMember]
        public string flight_level_type { get; set; }
        [DataMember]
        public int flight_level { get; set; }
        [DataMember]
        public string callsign { get; set; }
        [DataMember]
        public int person_on_board { get; set; }
        [DataMember]
        public TimeSpan departure_time { get; set; }
        [DataMember]
        public TimeSpan estimate_time { get; set; }
        [DataMember]
        public TimeSpan fuel { get; set; }
        [DataMember]
        public AircraftExerciseDepartureContract departure { get; set; }
        [DataMember]
        public AircraftExerciseArrivalContract arrival { get; set; }
    }
    #endregion

    #region Aircraft Exercise Departure Detail
    [DataContract]
    public class AircraftExerciseDepartureContract
    {
        [DataMember]
        public int parking_id{get;set;}
    }
    #endregion

    #region Aircraft Exercise Arrival Detail
    [DataContract]
    public class AircraftExerciseArrivalContract
    {
        [DataMember]
        public TimeSpan start_time { get; set; }
        [DataMember]
        public string runway { get; set; }
    }
    #endregion

}
