﻿using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Services.System
{
    [ServiceContract(
        Name = "SystemService",
        Namespace = "http://ilyas:8001/ATC/SystemService/",
        SessionMode = SessionMode.Required,
        CallbackContract = typeof(ISystemCallback))]

    public interface ISystemService
    {
        #region PC Service
        [OperationContract(IsOneWay = false)]
        StatusResponse ConnectPC(PCContract user);

        [OperationContract(IsOneWay = false)]
        DetailPCResponse DetailPC(int pc_id);

        [OperationContract(IsOneWay = false)]
        ListPCResponse ListPC();

        [OperationContract(IsOneWay = false)]
        StatusResponse DisconnectPC(DisconnectPCContract user);

        [OperationContract(IsOneWay = false)]
        StatusResponse ShutdownRestartPC(ShutdownRestartPCContract user);
        [OperationContract(IsOneWay = false)]
        bool Ping();

        #endregion

        #region User Service
        [OperationContract(IsOneWay = false)]
        UserLoginResponse UserLogin(UserLoginContract user);

        [OperationContract(IsOneWay = false)]
        StatusResponse UserRemove(int user_id);

        [OperationContract(IsOneWay = false)]
        StatusResponse UserRegister(UserRegisterContract user);

        [OperationContract(IsOneWay = false)]
        StatusResponse UserUpdate(UserUpdateContract user);

        [OperationContract(IsOneWay = false)]
        UserListResponse UserList(int page, int offset, int type);

        [OperationContract(IsOneWay = false)]
        UserDetailResponse UserDetail(int user_id);

        [OperationContract(IsOneWay = false)]
        StatusResponse UserLogout(int user_id, string role_pc);

        [OperationContract(IsOneWay = false)]
        UserDetailResponse UserRemoteLogin(UserRemoteContract user);

        [OperationContract(IsOneWay = false)]
        UserStudentSimulationResponse UserStudentSimulation();

        [OperationContract(IsOneWay = false)]
        UserRolesResponse GetUserRoles();

        [OperationContract(IsOneWay = false)]
        StatusResponse ResetPassword(int user_id);
        #endregion

        #region Scenery Service
        [OperationContract(IsOneWay = false)]
        StatusResponse SceneryAdd(SceneryDetailContract scenery);

        [OperationContract(IsOneWay = false)]
        StatusResponse SceneryRemove(int scenery_id);

        [OperationContract(IsOneWay = false)]
        StatusResponse SceneryUpdate(SceneryUpdateContract scenery);

        [OperationContract(IsOneWay = false)]
        SceneryDetailResponse SceneryDetail(int scenery_id);

        [OperationContract(IsOneWay = false)]
        SceneryListResponse SceneryList();
        #endregion

        #region Exercise Service
        [OperationContract(IsOneWay = false)]
        StatusResponse ExerciseAdd(ExerciseAddContract exercise);

        [OperationContract(IsOneWay = false)]
        StatusResponse ExerciseUpdate(ExerciseUpdateContract exercise);

        [OperationContract(IsOneWay = false)]
        StatusResponse ExerciseRemove(int exercise_id);

        [OperationContract(IsOneWay = false)]
        ExerciseDetailResponse ExerciseDetail(int exercise_id);

        [OperationContract(IsOneWay = false)]
        ExerciseListResponse ExerciseList();
        #endregion

        #region History Simulation Service

        [OperationContract(IsOneWay = false)]
        StatusResponse SimulationRemove(int simulation_id);

        [OperationContract(IsOneWay = false)]
        SimulationListResponse SimulationList(int page, int pageSize);
        [OperationContract(IsOneWay = false)]
        AircraftHistoryListResponse AircraftHistoryList(int simulation_id);

        [OperationContract(IsOneWay = false)]
        PlayerListResponse PlayerList(int simulation_id);
        [OperationContract(IsOneWay = false)]
        StatusResponse SetPoint(PointContract point);
        [OperationContract(IsOneWay = false)]
        HistoryPlayerResponse GetHistoryPlayer(int userID);
        [OperationContract(IsOneWay = false)]
        HistoryInstructorResponse GetHistoryInstructor(string instructorName);
        #endregion

        #region Aircraft Service
        [OperationContract(IsOneWay = false)]
        AircraftDetailResponse AircraftDetail(int aircraft_id);

        [OperationContract(IsOneWay = false)]
        AircraftListResponse AircraftList();
        #endregion

        #region Flight Rule Service
        [OperationContract(IsOneWay = false)]
        FlightRuleListResponse FlightRuleList();
        #endregion

        #region Flight Type Service
        [OperationContract(IsOneWay = false)]
        FlightTypeListResponse FlightTypeList();
        #endregion

        #region Livery Service
        [OperationContract(IsOneWay = false)]
        LiveryListResponse LiveryList(int aircraft_id);

        [OperationContract(IsOneWay = false)]
        LiveryDetailResponse LiveryDetail(int id);
        #endregion

        #region Airport Service
        [OperationContract(IsOneWay = false)]
        AirportAppronListResponse AirportAppronList(int scenery_id);

        [OperationContract(IsOneWay = false)]
        AirportListResponse AirportList(int scenery_id);
        [OperationContract(IsOneWay = false)]
        RunwayListResponse RunwayList(int scenery_id);
        #endregion

        #region Flight Route Service
        [OperationContract(IsOneWay = false)]
        FlightRouteListResponse FlightRouteList(FlightRouteListContract request);
        #endregion

        #region Flight Route Detail Service
        [OperationContract(IsOneWay = false)]
        FlightRouteDetailResponse FlightRouteDetail(int route_id, bool is_departure);
        #endregion

    }
}
