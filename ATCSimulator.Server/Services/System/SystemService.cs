﻿using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Controller.System;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Model.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Diagnostics;
using System.Timers;

namespace ATCSimulator.Server.Services.System
{
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerCall)]
    public class SystemService : ISystemService
    {
        private static Dictionary<UserComputerInfo, ISystemCallback> _callbackUser = new Dictionary<UserComputerInfo, ISystemCallback>();
        public static void Init()
        {
            // Timer For Ping to Client
            Timer timer = new Timer
            {
                Interval = 1000
            };
            timer.Elapsed += new ElapsedEventHandler(StartTimerEvent);
            timer.Start();
        }
        public static void RemoveCallbackUser(UserComputerInfo info)
        {
            foreach (var c in _callbackUser)
            {
                if (c.Key.role_pc.Equals(info.role_pc))
                {
                    _callbackUser.Remove(c.Key);
                    break;
                }
            }
        }

        #region PC Service

        #region Connect PC
        public StatusResponse ConnectPC(PCContract user)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = PCRepository.ConnectPC(user);
            if (result.status.code == 0)
            {
                ISystemCallback client = OperationContext.Current.GetCallbackChannel<ISystemCallback>();
                UserComputerInfo _user = new UserComputerInfo()
                {
                    ip_address = user.ip_address,
                    mac_address = user.mac_address,
                    pc_name = user.pc_name,
                    role_pc = user.role_pc
                };
                foreach (var c in _callbackUser)
                {
                    if (c.Key.role_pc.Equals(_user.role_pc))
                    {
                        _callbackUser.Remove(c.Key);
                        Simulation.SimulationService.RemoveCallbackUser(c.Key);
                        break;
                    }
                }
                _callbackUser.Add(_user, client);

                // send callback to instructor
                foreach (var c in _callbackUser)
                {
                    if (c.Key.role_pc.ToLower().Equals("instructor"))
                    {
                        _callbackUser[c.Key].PCConnect(_user);
                        break;
                    }
                }
            }
            return result;
        }
        #endregion

        #region Detail PC
        public DetailPCResponse DetailPC(int pc_id)
        {
            return PCRepository.DetailPC(pc_id);
        }
        #endregion

        #region List PC
        public ListPCResponse ListPC()
        {
            return PCRepository.ListPC();
        }
        #endregion

        #region Disconnect PC
        public StatusResponse DisconnectPC(DisconnectPCContract user)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = PCRepository.DisconnectPC(user.role_pc);
            if (result.status.code == 0)
            {
                foreach (var c in _callbackUser)
                {
                    if (c.Key.mac_address.Equals(user.mac_address))
                    {
                        _callbackUser.Remove(c.Key);
                        Simulation.SimulationService.RemoveCallbackUser(c.Key);
                        break;
                    }
                }
            }
            return result;
        }
        #endregion

        #region Shutdown Restart PC
        public StatusResponse ShutdownRestartPC(ShutdownRestartPCContract user) //powerpccontract
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = PCRepository.DisconnectPC(user.role_pc);
            if (result.status.code == 0)
            {
                foreach (var c in _callbackUser)
                {
                    if (c.Key.mac_address.Equals(user.mac_address))
                    {
                        _callbackUser[c.Key].ShutdownRestart(user.command);
                        _callbackUser.Remove(c.Key);
                        Simulation.SimulationService.RemoveCallbackUser(c.Key);
                        break;
                    }
                }
            }
            return result;
        }
        #endregion

        #region Ping Service

        public bool Ping()
        {
            return true;
        }
        private static void StartTimerEvent(object source, ElapsedEventArgs e)
        {
            foreach (var c in _callbackUser)
            {
                bool status = false;
                try
                {
                    status = c.Value.PingClient();
                }
                catch (Exception ex)
                {
                    status = false;
                    Program.SendMessage("user " + c.Key.pc_name + "is not found and disconected from server with error : " + ex.Message);
                }
                if (!status)
                {
                    _callbackUser.Remove(c.Key);
                    Simulation.SimulationService.RemoveCallbackUser(c.Key);
                }
            }
        }

        #endregion

        #endregion

        #region User Service

        #region User Login
        public UserLoginResponse UserLogin(UserLoginContract user)
        {
            UserLoginResponse result = UserRepository.UserLogin(user);
            if (result.status.code == 0)
            {
                //send data player to simulation
                if (Simulation.SimulationService.LoginPlayer.ContainsKey(user.pc_role))
                    Simulation.SimulationService.LoginPlayer[user.pc_role] = result.user.ID;
                else
                    Simulation.SimulationService.LoginPlayer.Add(user.pc_role, result.user.ID);
                // send callback to instructor
                foreach (var c in _callbackUser)
                {
                    if (c.Key.role_pc.ToLower().Equals("instructor"))
                    {
                        _callbackUser[c.Key].ConnectUpdate();
                        break;
                    }
                }
            }
            return result;
        }
        #endregion

        #region User Remove
        public StatusResponse UserRemove(int user_id)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = UserRepository.UserRemove(user_id);
            return result;
        }
        #endregion

        #region User Register
        public StatusResponse UserRegister(UserRegisterContract user)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = UserRepository.UserRegister(user);
            return result;
        }
        #endregion

        #region User Update
        public StatusResponse UserUpdate(UserUpdateContract user)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = UserRepository.UserUpdate(user);
            return result;
        }
        #endregion

        #region User List
        public UserListResponse UserList(int page, int offset, int type)
        {
            return UserRepository.UserList(page, offset, type);
        }
        #endregion

        #region User Detail
        public UserDetailResponse UserDetail(int user_id)
        {
            return UserRepository.UserDetail(user_id);
        }
        #endregion

        #region User Logout
        public StatusResponse UserLogout(int user_id, string role_pc)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = UserRepository.UserLogout(user_id, role_pc);
            if (result.status.code == 0)
            {
                // send callback to instructor
                foreach (var c in _callbackUser)
                {
                    if (c.Key.role_pc.ToLower().Equals("instructor"))
                    {
                        _callbackUser[c.Key].ConnectUpdate();
                        break;
                    }
                }
            }
            return result;
        }
        #endregion

        #region User Remote Login
        public UserDetailResponse UserRemoteLogin(UserRemoteContract user)
        {
            UserDetailResponse result = UserRepository.UserRemoteLogin(user);
            if (result.status.code == 0)
            {
                //send data player to simulation
                if (Simulation.SimulationService.LoginPlayer.ContainsKey(user.pc_role))
                    Simulation.SimulationService.LoginPlayer[user.pc_role] = result.user.ID;
                else
                    Simulation.SimulationService.LoginPlayer.Add(user.pc_role, result.user.ID);
                foreach (var c in _callbackUser)
                {
                    if (c.Key.ip_address.Equals(user.ip_address))
                    {
                        _callbackUser[c.Key].RemoteLogin(result.user);
                        result.status = ServiceResponse.OK;
                        break;
                    }
                }
            }
            return result;
        }
        #endregion

        #region User StudentSimulation
        public UserStudentSimulationResponse UserStudentSimulation()
        {
            return UserRepository.UserStudentSimulation();
        }
        #endregion

        #region Get User Role
        public UserRolesResponse GetUserRoles()
        {
            return UserRepository.GetUserRoles();
        }

        #endregion

        #region Reset Password
        public StatusResponse ResetPassword(int user_id)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = UserRepository.ResetPassword(user_id);
            return result;
        }

        #endregion

        #endregion

        #region Scenery Service

        #region Scenery ADD
        public StatusResponse SceneryAdd(SceneryDetailContract scenery)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = SceneryRepository.SceneryAdd(scenery);
            return result;
        }
        #endregion

        #region SceneryRemove
        public StatusResponse SceneryRemove(int scenery_id)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = SceneryRepository.SceneryRemove(scenery_id);
            return result;
        }
        #endregion

        #region SceneryUpdate
        public StatusResponse SceneryUpdate(SceneryUpdateContract scenery)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = SceneryRepository.SceneryUpdate(scenery);
            return result;
        }
        #endregion

        #region SceneryUpdate
        public SceneryDetailResponse SceneryDetail(int scenery_id)
        {
            return SceneryRepository.SceneryDetail(scenery_id);
        }
        #endregion

        #region SceneryList
        public SceneryListResponse SceneryList()
        {
            return SceneryRepository.SceneryList();
        }
        #endregion

        #endregion

        #region Exercise Service
        public StatusResponse ExerciseAdd(ExerciseAddContract exercise)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = ExerciseRepository.ExerciseAdd(exercise);
            return result;
        }

        public StatusResponse ExerciseUpdate(ExerciseUpdateContract exercise)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = ExerciseRepository.ExerciseUpdate(exercise);
            return result;
        }

        public StatusResponse ExerciseRemove(int exercise_id)
        {
            StatusResponse result = new StatusResponse();
            result.status = ServiceResponse.BadRequest;
            result.status = ExerciseRepository.ExerciseRemove(exercise_id);
            return result;
        }

        public ExerciseDetailResponse ExerciseDetail(int exercise_id)
        {
            return ExerciseRepository.ExerciseDetail(exercise_id);
        }

        public ExerciseListResponse ExerciseList()
        {
            return ExerciseRepository.ExerciseList();
        }
        #endregion

        #region History Simulation Service

        public SimulationListResponse SimulationList(int page, int pageSize)
        {
            return HistorySimulationRepository.SimulationList(page, pageSize);
        }

        public StatusResponse SimulationRemove(int simulation_id)
        {
            return HistorySimulationRepository.RemoveSimulation(simulation_id);
        }
        public AircraftHistoryListResponse AircraftHistoryList(int simulation_id)
        {
            return HistorySimulationRepository.AircraftHistoryList(simulation_id);
        }

        public PlayerListResponse PlayerList(int simulation_id)
        {
            return HistorySimulationRepository.PlayerList(simulation_id);
        }

        public StatusResponse SetPoint(PointContract point)
        {
            return HistorySimulationRepository.SetPoint(point);
        }
        public HistoryPlayerResponse GetHistoryPlayer(int userID)
        {
            return HistorySimulationRepository.GetHistoryPlayer(userID);
        }

        public HistoryInstructorResponse GetHistoryInstructor(string instructorName)
        {
            return HistorySimulationRepository.GetHistoryInstructor(instructorName);
        }


        #endregion

        #region Aircraft Service
        public AircraftDetailResponse AircraftDetail(int aircraft_id)
        {
            return AircraftRepository.AircraftDetail(aircraft_id);
        }

        public AircraftListResponse AircraftList()
        {
            return AircraftRepository.AircraftList();
        }
        #endregion

        #region Flight Rule Service
        public FlightRuleListResponse FlightRuleList()
        {
            return AircraftRepository.FlightRuleList();
        }
        #endregion

        #region Flight Type Service
        public FlightTypeListResponse FlightTypeList()
        {
            return AircraftRepository.FlightTypeList();
        }
        #endregion

        #region Livery Service
        public LiveryDetailResponse LiveryDetail(int id)
        {
            return LiveryRepository.LiveryDetail(id);
        }

        public LiveryListResponse LiveryList(int aircraft_id)
        {
            return LiveryRepository.LiveryList(aircraft_id);
        }
        #endregion

        #region Airport Service
        public AirportListResponse AirportList(int scenery_id)
        {
            return AirportRepository.AirportList(scenery_id);
        }

        public AirportAppronListResponse AirportAppronList(int scenery_id)
        {
            return AirportRepository.AirportAppronList(scenery_id);
        }
        public RunwayListResponse RunwayList(int scenery_id)
        {
            return AirportRepository.RunwayList(scenery_id);
        }
        #endregion

        #region Flight Route Service
        public FlightRouteListResponse FlightRouteList(FlightRouteListContract request)
        {
            return FlightRouteRepository.FlightRouteList(request);
        }
        #endregion

        #region Flight Route Detail Service
        public FlightRouteDetailResponse FlightRouteDetail(int route_id, bool is_departure)
        {
            return FlightRouteRepository.FlightRouteDetail(route_id, is_departure);
        }
        #endregion
    }
}
