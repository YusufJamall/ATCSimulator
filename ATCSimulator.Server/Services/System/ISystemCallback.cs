﻿using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ATCSimulator.Server.Services.System
{
    interface ISystemCallback
    {
        [OperationContract(IsOneWay = true)]
        void RemoteLogin(UserProfileModel user);
        [OperationContract(IsOneWay = true)]
        void ShutdownRestart(string command);
        [OperationContract(IsOneWay = true)]
        void PCConnect(UserComputerInfo user);
        [OperationContract(IsOneWay = true)]
        void ConnectUpdate();
        [OperationContract(IsOneWay = false)]
        bool PingClient();
    }
}
