﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Diagnostics;
using System.Timers;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Controller.Simulation;
using ATCSimulator.Server.Contract.Simulation;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Controller.Simulation.VisualRepository;
using Newtonsoft.Json;

namespace ATCSimulator.Server.Services.Simulation
{
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerCall)]
    public class SimulationService : ISimulationService
    {
        public static SimulationService Instance { get; private set; }
        private static Dictionary<UserComputerInfo, ISimulationCallback> _callbackUser = new Dictionary<UserComputerInfo, ISimulationCallback>();
        private static Dictionary<AircraftData, ISimulationCallback> pilots = new Dictionary<AircraftData, ISimulationCallback>();
        private static ISimulationCallback instructor;
        public static SimulationRepository simulation;
        public static ExerciseSimulationModel oldExerciseData;
        private static Stopwatch simulation_timer;
        private static TimeSpan simulation_time;
        private static Timer timer;
        private static int playedSimulationID;
        private static bool isPlayed;
        private static bool isPaused;
        public static Dictionary<string, int> LoginPlayer = new Dictionary<string, int>();

        public SimulationService()
        {
            Instance = this;
        }
        public static void RemoveCallbackUser(UserComputerInfo info)
        {
            foreach (var c in _callbackUser)
            {
                if (c.Key.role_pc.Equals(info.role_pc))
                {
                    _callbackUser.Remove(c.Key);
                    break;
                }
            }
        }

        #region Connect
        public SimulationConnectResponse Connect(UserComputerInfo user)
        {
            SimulationConnectResponse result = new SimulationConnectResponse();
            try
            {
                ISimulationCallback client = OperationContext.Current.GetCallbackChannel<ISimulationCallback>();
                UserComputerInfo _user = user;
                foreach (var c in _callbackUser)
                {
                    if (c.Key.role_pc.Equals(_user.role_pc) && c.Key.application.Equals(_user.application))
                    {
                        _callbackUser.Remove(c.Key);
                        System.SystemService.RemoveCallbackUser(c.Key);
                        break;
                    }
                }
                _callbackUser.Add(_user, client);
                result.status = ServiceResponse.OK;
            }
            catch (Exception ex)
            {
                Program.SendMessage(ex.Message);
                result.status = ServiceResponse.InternalServerError;
            }
            result.isPlay = isPlayed;
            return result;
        }
        #endregion



        #region Instructor Command

        #region Simulation Control

        #region Play Simulation
        public StatusResponse SimulationPlay(PlaySimulationContract data)
        {
            StatusResponse result = new StatusResponse();
            if (data != null)
            {
                DataSimulationModel _database = DatabaseSimulation.CreateData(data.exercise.scenery_id);
                List<AircraftData> _aircrafts = DatabaseSimulation.CreateAircraftData(data.pilots, _database);
                SimulationResponse response = DatabaseSimulation.CreateSimulationData(data);

                if (response.status.code == 0)
                {
                    simulation = new SimulationRepository(this, data.exercise, _database, _aircrafts);
                    ResponseData response2 = VCCSRepository.InsertDataStartSimulation(simulation.aircrafts);
                    if (response2.code == 0)
                    {
                        #region Create Callback User

                        foreach (var p in _callbackUser)
                        {
                            if (p.Key.application.Equals("System"))
                            {
                                if (p.Key.role_pc.ToLower().Equals("instructor"))
                                {
                                    instructor = p.Value;
                                    break;
                                }
                                if (p.Key.role_pc.ToLower().Contains("pseudopilot"))
                                {
                                    foreach (AircraftSimulationContract pi in data.pilots)
                                    {
                                        AircraftData ac = _aircrafts.Where(d => d.callsign.Equals(pi.callsign)).FirstOrDefault();
                                        if (ac != null)
                                            pilots.Add(ac, p.Value);
                                    }
                                }
                            }
                        }
                        #endregion

                        oldExerciseData = data.exercise;
                        simulation_time = data.exercise.play_time;
                        playedSimulationID = response.simulationID;
                        isPlayed = true;
                        isPaused = false;
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = response2;
                    }
                }
                else
                {
                    result.status = ServiceResponse.BadRequest;
                }
            }
            return result;
        }
        #endregion

        #region Start Simulation
        public void StartSimulation()
        {
            Program.SendUDPMessage("#S1:" + playedSimulationID + "");
            Program.SendMessage("S1 dikirim");
            foreach (var c in _callbackUser)
            {
                c.Value.StartSimulationCallback(simulation.exercise, simulation.aircrafts, simulation.database.flight_route_list, simulation.database.approns);
            }
            OtherService.CreateSimulation(simulation.exercise, simulation.aircrafts, playedSimulationID);
        }
        #endregion

        #region Continue Simulation
        public bool ContinueSimulation(UserComputerInfo user)
        {
            bool result = false;
            try
            {
                foreach (var c in _callbackUser)
                {
                    if (c.Key.role_pc.Equals(user.role_pc))
                    {
                        c.Value.ContinueSimulationCallback(simulation.exercise, simulation.aircrafts, simulation.database.flight_route_list, simulation.database.approns, LoginPlayer[user.role_pc], isPaused);
                        result = true;
                        break;
                    }
                }
                return result;
            }
            catch
            {
                return result;
            }
        }
        #endregion

        #region Pause / Resume Simulation
        public void PauseSimulation(bool value)
        {
            OtherService.PauseSimulation(value);
            if (value)
            {
                simulation_timer.Stop();
                Program.SendUDPMessage("#S2");
            }
            else
            {
                simulation_timer.Start();
                Program.SendUDPMessage("#S3");
            }
            isPaused = value;
            foreach (var c in _callbackUser)
            {
                c.Value.PauseSimulationCallback(value);
            }
        }

        #endregion

        #region Stop Simulation
        public void StopSimulation()
        {
            if (isPlayed)
            {

                StatusResponse response = DatabaseSimulation.EndSimulation(playedSimulationID, simulation_timer.Elapsed);
                if (response.status.code == 0)
                {
                    foreach (var c in _callbackUser)
                    {
                        c.Value.StopSimulationCallback();
                    }
                    simulation_timer.Stop();
                    //Reset All Data Simulation
                    simulation_timer.Reset();
                    timer.Elapsed -= new ElapsedEventHandler(StartTimerEvent);
                    isPlayed = false;
                    oldExerciseData = null;
                    simulation = null;
                    playedSimulationID = 0;
                    Program.SendUDPMessage("#S0");

                    OtherService.StopSimulation();
                }
                else
                {
                    Program.SendMessage("Failed to Stop Simulation");
                }
            }
        }

        #endregion

        #endregion

        #region Incident
        public void IncidentBird(string callsign)
        {
            OtherService.BirdAttack(callsign);
            simulation.IncidentBird(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.BirdAttackCallback(callsign);
                }
            }
        }
        public void PKPPK(string callsign)
        {
            GroundService.PKPPK(callsign);
            simulation.PKPPK(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.PKPPKCallback(callsign);
                }
            }
        }

        public void IncidentAnimal(string runway)
        {
            OtherService.AnimalCrossing(runway);
            simulation.AnimalCrossing(false);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.IncidentAnimalCallback(false, runway);
                }
            }
        }

        #endregion

        #region Aircraft
        public void SelectAircraft(string callsign, string SendToTarget, string pc_sender)
        {
            OtherService.SelectAircraft(callsign, SendToTarget);
            if (SendToTarget.ToLower().Contains("pseudopilot"))
                Program.SendUDPMessage("@" + callsign + ":" + pc_sender);
        }
        public void RemoveAircraft(string callsign)
        {
            simulation.RemoveAircraft(callsign);
            OtherService.RemoveAircraft(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.RemoveAircraftCallback(callsign);
                }
            }
        }

        public void LandingGearJam(string callsign)
        {
            FlightService.LandingGearJam(callsign);
            simulation.LandingGearJam(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.LandingGearJamCallback(callsign);
                }
            }
        }

        public void EngineFailure(string callsign, int engine)
        {
            OtherService.EngineFailure(callsign, engine);
            simulation.EngineFailure(callsign, engine);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.EngineFailureCallback(callsign, engine);
                }
            }
        }

        public void ChangeFuel(string callsign, TimeSpan fuel)
        {
            OtherService.ChangeFuel(callsign, fuel.TotalSeconds);
            simulation.ChangeFuel(callsign, fuel);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeFuelCallback(callsign, fuel);
                }
            }
        }
        #endregion

        #region Environment
        public void ChangeWeather(WeatherInfo weather)
        {
            OtherService.Weather((int)weather);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeWeatherCallback(weather);
                }
            }
        }

        public void ChangeWindDirection(float value)
        {
            OtherService.WindDirection(value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeWindDirectionCallback(value);
                }
            }
        }

        public void ChangeWindSpeed(float value)
        {
            OtherService.WindSpeed(value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeWindSpeedCallback(value);
                }
            }
        }

        public void ChangeTemperature(float value)
        {
            OtherService.Temperature(value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeTemperatureCallback(value);
                }
            }
        }

        public void ChangeVisibility(float value)
        {
            OtherService.Visibility(value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeVisibilityCallback(value);
                }
            }
        }

        public void ChangeTimeSimulation(TimeSpan time)
        {
            OtherService.TimeSimulation(time.Hours, time.Minutes);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeTimeSimulationCallback(time);
                }
            }
        }
        #endregion

        #region Lights
        public void LightRunway(string runway, bool value)
        {
            OtherService.RunwayLight(runway, value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.RunwayLightCallback(runway, value);
                }
            }
        }

        public void LightApproach(string runway, bool value)
        {
            OtherService.ApproachLight(runway, value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ApproachLightCallback(runway, value);
                }
            }
        }

        public void LightTaxi(bool value)
        {
            OtherService.TaxiLight(value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.TaxiLightCallback(value);
                }
            }
        }

        public void LightPapi(bool value)
        {
            OtherService.PapiLight(value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.PapiLightCallback(value);
                }
            }
        }
        #endregion

        #endregion

        #region Ground Command

        #region Change Ground Status
        public void ChangeGroundStatus(string callsign, int status)
        {
            simulation.ChangeGroundStatus(callsign, status);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeGroundStatusCallback(callsign, status);
                }
            }
        }

        #endregion

        #region Change Flight Route, Rules, and Runway
        public void ChangeFlightRoute(string callsign, FlightRouteListModel routes, string runway)
        {
            //simulation.ChangeGroundStatus(callsign, status);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeFlightRouteCallback(callsign, routes, runway);
                }
            }
        }

        #endregion

        #region Change Approns and Terminal
        public void ChangeParkingAppron(string callsign, DataAppronModel terminal)
        {
            //simulation.ChangeGroundStatus(callsign, status);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeParkingAppronCallback(callsign, terminal);
                }
            }
        }

        #endregion

        #region Ready Aircraft
        public void ReadyAircraft(string callsign)
        {
            simulation.ReadyAircraft(callsign);
            GroundService.ReadyAircraft(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ReadyAircraftCallback(callsign);
                }
            }
        }

        #endregion

        #region Engine Start
        public void StartEnigne(string callsign, bool engine)
        {
            simulation.StartEnigne(callsign, engine);
            GroundService.EngineStart(callsign, engine);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.StartEnigneCallback(callsign, engine);
                }
            }
        }

        #endregion

        #region Pushback
        public void Pushback(string callsign, DirectionInfo pushback)
        {
            string pb_name = simulation.Pushback(callsign, pushback);
            GroundService.Pushback(callsign, pushback);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.PushbackCallback(callsign, pushback, pb_name);

                }
            }
        }

        #endregion

        #region Start Taxi
        public void StartTaxi(string callsign, List<string> routes, string target, int status)
        {
            simulation.StartTaxi(callsign, routes);
            GroundService.StartTaxi(callsign, routes, target, status);
            foreach (var p in _callbackUser)
            {

                if (p.Key.application.Equals("System"))
                {
                    p.Value.StartTaxiCallback(callsign, routes, status);
                }
            }
        }

        #endregion

        #region Hold Taxi
        public void HoldTaxi(string callsign)
        {
            simulation.HoldTaxi(callsign);
            GroundService.HoldTaxi(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.HoldTaxiCallback(callsign);
                }
            }
        }

        #endregion

        #region Continue Taxi
        public void ContinueTaxi(string callsign)
        {
            simulation.ContinueTaxi(callsign);
            GroundService.ContinueTaxi(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ContinueTaxiCallback(callsign);
                }
            }
        }

        #endregion

        #region Change Speed Taxi
        public void ChangeSpeedTaxi(string callsign, GroundSpeed speed)
        {
            simulation.ChangeSpeedTaxi(callsign, speed);
            GroundService.TaxiSpeed(callsign, (int)speed);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeSpeedTaxiCallback(callsign, speed);
                }
            }
        }

        #endregion

        #region Cross Runway
        public void CrossRunway(string callsign)
        {
            simulation.CrossRunway(callsign);
            GroundService.CrossRunway(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.CrossRunwayCallback(callsign);
                }
            }
        }

        #endregion

        #region UTurn
        public void Uturn(string callsign, UturnInfo uturn)
        {
            simulation.Uturn(callsign, uturn);
            GroundService.Uturn(callsign, uturn);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.UturnCallback(callsign, uturn);
                }
            }
        }

        #endregion

        #region Ground Rocking Wing
        public void RockingWing(string callsign, bool value)
        {
            simulation.RockingWing(callsign, value);
            GroundService.GroundRockingWing(callsign, value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.RockingWingCallback(callsign, value);
                }
            }
        }

        #endregion

        #region Take Off
        public void TakeOff(string callsign, VisualFlightRouteModel routes, string runway)
        {
            simulation.TakeOff(callsign);
            GroundService.TakeOff(callsign, routes, runway);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.TakeOffCallback(callsign);
                }
            }
        }

        #endregion

        #region Runway Hold
        public void RunwayHold(string callsign, VisualFlightRouteModel routes, string runway)
        {
            simulation.RunwayHold(callsign);
            GroundService.HoldTakeOff(callsign, routes, runway);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.RunwayHoldCallback(callsign);
                }
            }
        }

        #endregion

        #region AbortTakeOff
        public void AbortTakeOff(string callsign)
        {
            //not implement
            //simulation.AbortTakeOff(callsign);
            //GroundService.abor(callsign, routes, runway);
            //foreach (var p in _callbackUser)
            //{
            //    if (p.Key.application.Equals("System"))
            //    {
            //        if (p.Key.role_pc.ToLower().Equals("instructor") || p.Key.role_pc.ToLower().Contains("pseudopilot") || p.Key.role_pc.ToLower().Contains("supervisor"))
            //            p.Value.RunwayHoldCallback(callsign);
            //    }
            //}
        }

        #endregion

        #endregion

        #region Flight Command

        #region Active Aircraft
        public void ActiveAircraft(string callsign, FlightRouteListModel routes)
        {
            simulation.ActiveAircraft(callsign);
            FlightService.ActiveAircraft(callsign, routes);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ActiveAircraftCallback(callsign);
                }
            }
            Program.SendMessage("Active Aircraft " + callsign);
        }
        #endregion

        #region Holding Now
        public void HoldingNow(string callsign, DirectionInfo direction)
        {
            simulation.HoldingNow(callsign, direction);
            FlightService.HoldingNow(callsign, (int)direction);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.HoldingNowCallback(callsign, direction);
                }
            }
        }
        #endregion

        #region Holding Position
        public void HoldingPosition(string callsign, string position)
        {
            simulation.HoldingPosition(callsign, position);
            FlightService.HoldingPosition(callsign, position);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.HoldingPositionCallback(callsign, position);
                }
            }
        }
        #endregion

        #region Continue Route
        public void ContinueRoute(string callsign)
        {
            simulation.ContinueRoute(callsign);
            FlightService.ContinueRoute(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ContinueRouteCallback(callsign);
                }
            }
        }
        #endregion

        #region Continue Direction
        public void ContinueDirection(string callsign)
        {
            simulation.ContinueDirection(callsign);
            FlightService.ContinueDirection(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ContinueDirectionCallback(callsign);
                }
            }
        }
        #endregion

        #region Change Heading
        public void ChangeHeading(string callsign, HeadingInfo direction, float heading)
        {
            simulation.ChangeHeading(callsign, direction, heading);
            FlightService.ChangeHeading(callsign, (int)direction, heading);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeHeadingCallback(callsign, direction, heading);
                }
            }
        }
        #endregion

        #region Change Route
        public void ChangeRoute(string callsign, FlightRouteListModel routes)
        {
            simulation.ChangeRoute(callsign, routes);
            FlightService.ChangeRoute(callsign, routes);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeRouteCallback(callsign, routes);
                }
            }
        }
        #endregion

        #region Direct Go
        public void DirectGo(string callsign, string position, bool isILS)
        {
            simulation.DirectGo(callsign, position, isILS);
            FlightService.DirectGo(callsign, position, isILS);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.DirectGoCallback(callsign, position, isILS);
                }
            }
        }
        #endregion

        #region Approach
        public void Approach(string callsign, string runway)
        {
            simulation.InstrumentApproach(callsign, runway);
            FlightService.Approach(callsign, runway);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ApproachCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Circuit
        public void Circuit(string callsign, string runway)
        {
            simulation.JoinCircuit(callsign, runway);
            FlightService.Circuit(callsign, runway);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.CircuitCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Missed Approach
        public void MissedApproach(string callsign)
        {
            simulation.MissedApproach(callsign);
            FlightService.MissedApproach(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.MissedApproachCallback(callsign);
                }
            }
        }
        #endregion

        #region Touch Go
        public void TouchGo(string callsign)
        {
            simulation.TouchGo(callsign);
            FlightService.TouchGo(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.TouchGoCallback(callsign);
                }
            }
        }
        #endregion

        #region Flypass
        public void Flypass(string callsign)
        {
            simulation.Flypass(callsign);
            FlightService.Flypass(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FlypassCallback(callsign);
                }
            }
        }
        #endregion

        #region Extend Downwind
        public void ExtendDownwind(string callsign)
        {
            simulation.ExtendDownwind(callsign);
            FlightService.ExtendDownwind(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ExtendDownwindCallback(callsign);
                }
            }
        }
        #endregion

        #region Orbit
        public void Orbit(string callsign)
        {
            simulation.Orbit(callsign);
            FlightService.Orbit(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.OrbitCallback(callsign);
                }
            }
        }
        #endregion

        #region Landing
        public void Landing(string callsign)
        {
            simulation.Landing(callsign, "");
            FlightService.Landing(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.LandingCallback(callsign, "");
                }
            }
        }
        #endregion

        #region ILS Landing
        public void ILSLanding(string callsign, string runway)
        {
            simulation.Landing(callsign, runway);
            FlightService.ILSLanding(callsign, runway);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.LandingCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Altitude
        public void Altitude(string callsign, float altitude)
        {
            simulation.Altitude(callsign, altitude);
            FlightService.Altitude(callsign, altitude);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.AltitudeCallback(callsign, altitude);
                }
            }
        }
        #endregion

        #region Speed
        public void Speed(string callsign, float speed)
        {
            simulation.Speed(callsign, speed);
            FlightService.Speed(callsign, speed);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.SpeedCallback(callsign, speed);
                }
            }
        }
        #endregion

        #region Flight Rocking Wing
        public void FlightRockingWing(string callsign)
        {
            FlightService.FlightRockingWing(callsign);
        }
        #endregion

        #region Change ILS Mode
        public void ChangeILSMode(string callsign, bool value)
        {
            simulation.ChangeILSMode(callsign, value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ChangeILSModeCallback(callsign, value);
                }
            }
        }

        #endregion


        #region Return Base
        public void ReturnBase(string callsign)
        {
            FlightService.ReturnBase(callsign);
            Program.SendMessage("Return Base " + callsign);
        }
        #endregion

        #region Aborted Take Off
        public void AbortedTakeOff(string callsign)
        {
            FlightService.AbortedTakeOff(callsign);
            Program.SendMessage("Abort Take Off " + callsign);
        }
        #endregion

        #region Change Runway
        public void ChangeRunway(string callsign, string runway)
        {
            FlightService.ChangeRunway(callsign, runway);
            Program.SendMessage("Change Runway " + callsign + " " + runway);
        }
        #endregion

        #endregion

        #region Callback

        #region Other Callback

        #region Ready Simulation
        public static void ReadySimulation()
        {
            // Timer in server
            simulation_timer = new Stopwatch();
            timer = new Timer
            {
                Interval = 1000
            };
            timer.Elapsed += new ElapsedEventHandler(StartTimerEvent);
            simulation_timer.Start();
            timer.Start();

            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ReadySimulationCallback();
                }
            }

        }

        private static void StartTimerEvent(object source, ElapsedEventArgs e)
        {
            // Stop simulation if simulation time is over
            //else send update time to client
            if (simulation_timer.Elapsed > simulation_time)
                Instance.StopSimulation();
            else
                SendTimer();

        }

        // send timer and ping client, if client not found it will deleted from callback list
        public static void SendTimer()
        {
            foreach (var c in _callbackUser)
            {
                bool status = false;
                try
                {
                    c.Value.UpdateTimeSimulation(simulation_timer.Elapsed);
                    status = true;
                }
                catch (Exception ex)
                {
                    status = false;
                    Program.SendMessage("user " + c.Key.pc_name + "is not found and disconected from server with error : " + ex.Message);
                }
                if (!status)
                {
                    _callbackUser.Remove(c.Key);
                    System.SystemService.RemoveCallbackUser(c.Key);

                }
            }
        }

        #endregion

        #region Bird Attack Value
        public static void BirdAttackValue(string callsign, bool value)
        {
            simulation.BirdAttackValue(callsign, value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.role_pc.ToLower().Equals("instructor"))
                {
                    p.Value.BirdAttackValue(callsign, value);
                }
            }
        }
        #endregion

        #region Engine Failure Callback
        public static void EngineFailureCallback(string callsign, int engine)
        {
            simulation.EngineFailure(callsign, engine);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.EngineFailureCallback(callsign, engine);
                }
            }
        }
        #endregion

        #region Landing Gear Jam Value
        public static void LandingGearJamValue(string callsign, bool value)
        {
            simulation.LandingGearJamValue(callsign, value);
            foreach (var p in _callbackUser)
            {
                if (p.Key.role_pc.ToLower().Equals("instructor"))
                {
                    p.Value.LandingGearJamValue(callsign, value);
                }
            }
        }
        #endregion

        #region Animal Crossing Enable
        public static void AnimalCrossingEnable(string runway)
        {
            simulation.AnimalCrossing(true);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.IncidentAnimalCallback(true, runway);
                }
            }
        }

        #endregion

        #endregion

        #region Ground Callback

        #region Finish Ready
        public static void FinishReadyDepartureCallback(string callsign)
        {
            simulation.FinishReadyAircraft(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishReadyDeparture(callsign);
                }
            }
        }

        #endregion

        #region Finish Pushback
        public static void FinishPushbackCallback(string callsign, double heading)
        {
            simulation.FinishPushback(callsign, heading);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishPushback(callsign, heading);
                }
            }
        }

        #endregion

        #region Taxi Position
        public static void UpdateTaxiPositionCallback(string callsign, string position, double heading)
        {
            simulation.UpdateTaxiPosition(callsign, position, heading);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.UpdateTaxiPosition(callsign, position, heading);
                }
            }
        }

        #endregion

        #region Finish Taxi
        public static void FinishTaxiCallback(string callsign, string position, double heading, int status)
        {
            simulation.FinishTaxi(callsign, position, heading, status);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishTaxi(callsign, position, heading, status);
                }
            }
        }

        #endregion

        #region Heading Aircraft
        public static void HeadingAircraftCallback(string callsign, double heading)
        {
            simulation.HeadingAircraft(callsign, heading);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.HeadingAircraft(callsign, heading);
                }
            }
        }

        #endregion

        #region Request Cross Runway
        public static void RequestCrossRunwayCallback(string callsign, double heading)
        {
            simulation.RequestCrossRunway(callsign, heading);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.RequestCrossRunway(callsign, heading);
                }
            }
        }

        #endregion

        #region Finish UTurn
        public static void FinishUTurnCallback(string callsign, double heading)
        {
            simulation.FinishUTurn(callsign, heading);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishUTurn(callsign, heading);
                }
            }
        }

        #endregion

        #region Finish Hold Take Off
        public static void FinishHoldTakeOffCallback(string callsign)
        {
            simulation.FinishHoldTakeOff(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishHoldTakeOff(callsign);
                }
            }
        }

        #endregion

        #region Finish Take Off
        public static void FinishTakeOffCallback(string callsign)
        {
            Program.SendUDPMessage("@[" + callsign + "]:1");
            simulation.FinishTakeOff(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishTakeOff(callsign);
                }
            }
        }

        #endregion

        #region Crash Aircraft
        public static void CrashAircraftCallback(string callsign)
        {
            simulation.CrashAircraft(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.CrashAircraft(callsign);
                }
            }
        }

        #endregion

        #region Finish Parking
        public static void FinishParkingCallback(string callsign, string appron)
        {
            AircraftData aircraft = simulation.FinishParking(callsign, appron);
            if (aircraft != null)
            {
                foreach (var p in _callbackUser)
                {
                    if (p.Key.application.Equals("System"))
                    {
                        p.Value.FinishParking(aircraft);
                    }
                }
            }
            else
            {
                Program.SendMessage("Finish Parking Callsign " + callsign + " is failed");
            }
        }

        #endregion

        #region Backtrack Callback
        public static void BacktrackCallback(string callsign)
        {
            simulation.Backtrack(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.Backtrack(callsign);
                }
            }
        }

        #endregion

        #endregion

        #region Flight Callback

        #region Cannot Change Speed
        public static void CannotChangeSpeed(string callsign)
        {
            simulation.CannotChangeSpeed(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.CannotChangeSpeed(callsign);
                }
            }
        }
        #endregion

        #region Cannot Change Altitude
        public static void CannotChangeAltitude(string callsign)
        {
            simulation.CannotChangeAltitude(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.CannotChangeAltitude(callsign);
                }
            }
        }
        #endregion

        #region Update Performance
        public static void UpdatePerformanceCallback(string callsign, float speed, float altitude)
        {
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.UpdatePerformance(callsign, speed, altitude);
                }
            }
        }

        #endregion

        #region Update Holding Point
        public static void UpdateHoldingPoint(List<string> holding)
        {
            simulation.UpdateHoldingPoint(holding);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.UpdateHoldingPoint(holding);
                }
            }
        }
        #endregion

        #region Cannot Holding
        public static void CannotHolding(string callsign)
        {
            simulation.CannotHolding(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.CannotHolding(callsign);
                }
            }
        }
        #endregion

        #region Holding At Callback
        public static void HoldingAtCallback(string callsign, string position, bool isInstrument)
        {
            simulation.HoldingAtCallback(callsign, position, isInstrument);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.HoldingAtCallback(callsign, position, isInstrument);
                }
            }
        }
        #endregion

        #region Route Update Callback
        public static void RouteUpdateCallback(string callsign, string position)
        {
            FlightRouteListModel route = simulation.RouteUpdateCallback(callsign, position);
            if (route != null)
            {
                foreach (var p in _callbackUser)
                {
                    if (p.Key.application.Equals("System"))
                    {
                        p.Value.RouteUpdateCallback(callsign, route);
                    }
                }
            }
        }
        #endregion

        #region Ready Join Circuit
        public static void ReadyJoinCircuit(string callsign)
        {
            simulation.ReadyJoinCircuit(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.ReadyJoinCircuit(callsign);
                }
            }
        }
        #endregion

        #region Join Circuit Callback
        public static void JoinCircuitCallback(string callsign, string runway)
        {
            simulation.JoinCircuitCallback(callsign, runway);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.JoinCircuitCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Start Approach
        public static void StartApproach(string callsign)
        {
            simulation.StartApproach(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.StartApproach(callsign);
                }
            }
        }
        #endregion

        #region Start Go Round
        public static void StartGoRound(string callsign, bool isFlypass, bool isIfr)
        {
            simulation.StartGoRound(callsign, isFlypass, isIfr);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.StartGoRound(callsign, isFlypass, isIfr);
                }
            }
        }
        #endregion

        #region Finish Go Round
        public static void FinishGoRound(string callsign)
        {
            simulation.FinishGoRound(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishGoRound(callsign);
                }
            }
        }
        #endregion

        #region Start Landing
        public static void CannotLanding(string callsign)
        {
            simulation.CannotLanding(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.CannotLanding(callsign);
                }
            }
        }
        #endregion

        #region Start Landing
        public static void StartLanding(string callsign)
        {
            simulation.StartLanding(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.StartLanding(callsign);
                }
            }
        }
        #endregion

        #region Touch Ground
        public static void TouchGround(string callsign)
        {
            simulation.TouchGround(callsign);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.TouchGround(callsign);
                }
            }
        }
        #endregion

        #region Finish Landing
        public static void FinishLandingCallback(string callsign, float heading, string from, string position)
        {
            simulation.FinishLanding(callsign, heading, from, position);
            foreach (var p in _callbackUser)
            {
                if (p.Key.application.Equals("System"))
                {
                    p.Value.FinishLanding(callsign, heading, from, position);
                }
            }
        }

        #endregion

        #endregion


        #endregion


    }

}
