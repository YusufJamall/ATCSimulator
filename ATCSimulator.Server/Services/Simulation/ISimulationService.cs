﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Contract.Simulation;
using ATCSimulator.Server.Model.Simulation;

namespace ATCSimulator.Server.Services.Simulation
{
    [ServiceContract(
        Name = "SimulationService",
        Namespace = "http://ilyas:8001/ATC/SimulationService/",
        SessionMode = SessionMode.Required,
        CallbackContract = typeof(ISimulationCallback))]
    public interface ISimulationService
    {
        #region System
        [OperationContract(IsOneWay = false)]
        SimulationConnectResponse Connect(UserComputerInfo user);

        [OperationContract(IsOneWay = false)]
        StatusResponse SimulationPlay(PlaySimulationContract data);
        [OperationContract(IsOneWay = false)]
        void StartSimulation();

        [OperationContract(IsOneWay = true)]
        void PauseSimulation(bool value);

        [OperationContract(IsOneWay = true)]
        void StopSimulation();
        [OperationContract(IsOneWay = false)]
        bool ContinueSimulation(UserComputerInfo user);
        #endregion

        #region Ground Command

        [OperationContract(IsOneWay = true)]
        void ChangeGroundStatus(string callsign, int status);

        [OperationContract(IsOneWay = true)]
        void ChangeFlightRoute(string callsign, FlightRouteListModel routes, string runway);

        [OperationContract(IsOneWay = true)]
        void ChangeParkingAppron(string callsign, DataAppronModel terminal);

        [OperationContract(IsOneWay = true)]
        void ReadyAircraft(string callsign);

        [OperationContract(IsOneWay = true)]
        void StartEnigne(string callsign, bool engine);

        [OperationContract(IsOneWay = true)]
        void Pushback(string callsign, DirectionInfo pushback);

        [OperationContract(IsOneWay = true)]
        void StartTaxi(string callsign, List<string> routes, string target, int status);

        [OperationContract(IsOneWay = true)]
        void HoldTaxi(string callsign);

        [OperationContract(IsOneWay = true)]
        void ContinueTaxi(string callsign);

        [OperationContract(IsOneWay = true)]
        void ChangeSpeedTaxi(string callsign, GroundSpeed speed);

        [OperationContract(IsOneWay = true)]
        void CrossRunway(string callsign);

        [OperationContract(IsOneWay = true)]
        void Uturn(string callsign, UturnInfo uturn);

        [OperationContract(IsOneWay = true)]
        void RockingWing(string callsign, bool value);

        [OperationContract(IsOneWay = true)]
        void TakeOff(string callsign, VisualFlightRouteModel routes, string runway);

        [OperationContract(IsOneWay = true)]
        void RunwayHold(string callsign, VisualFlightRouteModel routes, string runway);

        [OperationContract(IsOneWay = true)]
        void PKPPK(string callsign);

        #endregion

        #region Flight Command

        [OperationContract(IsOneWay = true)]
        void ActiveAircraft(string callsign, FlightRouteListModel routes);
        [OperationContract(IsOneWay = true)]
        void HoldingNow(string callsign, DirectionInfo direction);
        [OperationContract(IsOneWay = true)]
        void HoldingPosition(string callsign, string position);
        [OperationContract(IsOneWay = true)]
        void ContinueRoute(string callsign);
        [OperationContract(IsOneWay = true)]
        void ContinueDirection(string callsign);
        [OperationContract(IsOneWay = true)]
        void ChangeHeading(string callsign, HeadingInfo direction, float heading);
        [OperationContract(IsOneWay = true)]
        void DirectGo(string callsign, string position, bool isILS);
        [OperationContract(IsOneWay = true)]
        void ChangeRoute(string callsign, FlightRouteListModel routes);
        [OperationContract(IsOneWay = true)]
        void Altitude(string callsign, float altitude);
        [OperationContract(IsOneWay = true)]
        void Speed(string callsign, float speed);
        [OperationContract(IsOneWay = true)]
        void Approach(string callsign, string runway);
        [OperationContract(IsOneWay = true)]
        void Circuit(string callsign, string runway);
        [OperationContract(IsOneWay = true)]
        void MissedApproach(string callsign);
        [OperationContract(IsOneWay = true)]
        void TouchGo(string callsign);
        [OperationContract(IsOneWay = true)]
        void Flypass(string callsign);
        [OperationContract(IsOneWay = true)]
        void FlightRockingWing(string callsign);
        [OperationContract(IsOneWay = true)]
        void ExtendDownwind(string callsign);
        [OperationContract(IsOneWay = true)]
        void Orbit(string callsign);
        [OperationContract(IsOneWay = true)]
        void Landing(string callsign);
        [OperationContract(IsOneWay = true)]
        void ILSLanding(string callsign,string runway);
        [OperationContract(IsOneWay = true)]
        void ReturnBase(string callsign);
        [OperationContract(IsOneWay = true)]
        void AbortedTakeOff(string callsign);
        [OperationContract(IsOneWay = true)]
        void ChangeRunway(string callsign, string runway);
        [OperationContract(IsOneWay = true)]
        void ChangeILSMode(string callsign, bool value);

        #endregion

        #region Instructor Command

        #region Aircraft
        [OperationContract(IsOneWay = true)]
        void SelectAircraft(string callsign, string SendToTarget, string pc_sender);

        [OperationContract(IsOneWay = true)]
        void RemoveAircraft(string callsign);

        [OperationContract(IsOneWay = true)]
        void LandingGearJam(string callsign);

        [OperationContract(IsOneWay = true)]
        void EngineFailure(string callsign, int engine);

        [OperationContract(IsOneWay = true)]
        void ChangeFuel(string callsign, TimeSpan fuel);

        [OperationContract(IsOneWay = true)]
        void IncidentBird(string runway);

        [OperationContract(IsOneWay = true)]
        void IncidentAnimal(string runway);
        #endregion

        #region Environment
        [OperationContract(IsOneWay = true)]
        void ChangeWeather(WeatherInfo weather);

        [OperationContract(IsOneWay = true)]
        void ChangeWindDirection(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeWindSpeed(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeTemperature(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeVisibility(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeTimeSimulation(TimeSpan time);
        #endregion

        #region Light
        [OperationContract(IsOneWay = true)]
        void LightRunway(string runway, bool value);

        [OperationContract(IsOneWay = true)]
        void LightApproach(string approach, bool value);

        [OperationContract(IsOneWay = true)]
        void LightTaxi(bool value);

        [OperationContract(IsOneWay = true)]
        void LightPapi(bool value);
        #endregion
        #endregion


    }
}
