﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Contract.Simulation;

namespace ATCSimulator.Server.Services.Simulation
{
    interface ISimulationCallback
    {
        #region System
        [OperationContract(IsOneWay = true)]
        void StartSimulationCallback(ExerciseSimulationModel exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_route, List<DataAppronModel> aapprons);
        [OperationContract(IsOneWay = true)]
        void ReadySimulationCallback();
        [OperationContract(IsOneWay = true)]
        void UpdateTimeSimulation(TimeSpan time);
        [OperationContract(IsOneWay = true)]
        void PauseSimulationCallback(bool value);
        [OperationContract(IsOneWay = true)]
        void StopSimulationCallback();
        [OperationContract(IsOneWay = true)]
        void ContinueSimulationCallback(ExerciseSimulationModel exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_route, List<DataAppronModel> aapprons, int ID_User, bool isPaused);
        #endregion

        #region Ground Callback
        [OperationContract(IsOneWay = true)]
        void ChangeGroundStatusCallback(string callsign, int status);

        [OperationContract(IsOneWay = true)]
        void ChangeFlightRouteCallback(string callsign, FlightRouteListModel routes, string runway);

        [OperationContract(IsOneWay = true)]
        void ChangeParkingAppronCallback(string callsign, DataAppronModel terminal);

        [OperationContract(IsOneWay = true)]
        void ReadyAircraftCallback(string callsign);

        [OperationContract(IsOneWay = true)]
        void StartEnigneCallback(string callsign, bool engine);

        [OperationContract(IsOneWay = true)]
        void PushbackCallback(string callsign, DirectionInfo pushback, string pushback_name);

        [OperationContract(IsOneWay = true)]
        void StartTaxiCallback(string callsign, List<string> routes, int status);

        [OperationContract(IsOneWay = true)]
        void HoldTaxiCallback(string callsign);

        [OperationContract(IsOneWay = true)]
        void ContinueTaxiCallback(string callsign);

        [OperationContract(IsOneWay = true)]
        void ChangeSpeedTaxiCallback(string callsign, GroundSpeed speed);

        [OperationContract(IsOneWay = true)]
        void CrossRunwayCallback(string callsign);

        [OperationContract(IsOneWay = true)]
        void UturnCallback(string callsign, UturnInfo uturn);

        [OperationContract(IsOneWay = true)]
        void RockingWingCallback(string callsign, bool value);

        [OperationContract(IsOneWay = true)]
        void TakeOffCallback(string callsign);

        [OperationContract(IsOneWay = true)]
        void RunwayHoldCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void AbortTakeOffCallback(string callsign);
        #endregion

        #region Flight Callback

        [OperationContract(IsOneWay = true)]
        void ActiveAircraftCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void HoldingNowCallback(string callsign, DirectionInfo direction);
        [OperationContract(IsOneWay = true)]
        void HoldingPositionCallback(string callsign, string position);
        [OperationContract(IsOneWay = true)]
        void ContinueRouteCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void ContinueDirectionCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void ChangeHeadingCallback(string callsign, HeadingInfo direction, float heading);
        [OperationContract(IsOneWay = true)]
        void ChangeRouteCallback(string callsign, FlightRouteListModel routes);
        [OperationContract(IsOneWay = true)]
        void DirectGoCallback(string callsign, string position, bool isILS);
        [OperationContract(IsOneWay = true)]
        void ApproachCallback(string callsign, string runway);
        [OperationContract(IsOneWay = true)]
        void CircuitCallback(string callsign, string runway);
        [OperationContract(IsOneWay = true)]
        void MissedApproachCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void TouchGoCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void FlypassCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void ExtendDownwindCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void OrbitCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void LandingCallback(string callsign,string runway);
        [OperationContract(IsOneWay = true)]
        void AltitudeCallback(string callsign, float altitude);
        [OperationContract(IsOneWay = true)]
        void SpeedCallback(string callsign, float speed);
        [OperationContract(IsOneWay = true)]
        void ChangeILSModeCallback(string callsign, bool value);

        #endregion

        #region Visual Callback

        #region Ground

        [OperationContract(IsOneWay = true)]
        void FinishReadyDeparture(string callsign);

        [OperationContract(IsOneWay = true)]
        void FinishPushback(string callsign, double heading);

        [OperationContract(IsOneWay = true)]
        void UpdateTaxiPosition(string callsign, string position, double heading);

        [OperationContract(IsOneWay = true)]
        void FinishTaxi(string callsign, string position, double heading, int status);

        [OperationContract(IsOneWay = true)]
        void HeadingAircraft(string callsign, double heading);

        [OperationContract(IsOneWay = true)]
        void RequestCrossRunway(string callsign, double heading);

        [OperationContract(IsOneWay = true)]
        void FinishUTurn(string callsign, double heading);

        [OperationContract(IsOneWay = true)]
        void FinishHoldTakeOff(string callsign);

        [OperationContract(IsOneWay = true)]
        void FinishTakeOff(string callsign);

        [OperationContract(IsOneWay = true)]
        void CrashAircraft(string callsign);
        [OperationContract(IsOneWay = true)]
        void FinishParking(AircraftData aircraft);
        [OperationContract(IsOneWay = true)]
        void Backtrack(string callsign);

        #endregion

        #region Flight
        [OperationContract(IsOneWay = true)]
        void CannotChangeSpeed(string callsign);
        [OperationContract(IsOneWay = true)]
        void CannotChangeAltitude(string callsign);
        [OperationContract(IsOneWay = true)]
        void UpdatePerformance(string callsign, float speed, float altitude);
        [OperationContract(IsOneWay = true)]
        void UpdateHoldingPoint(List<string> holding);
        [OperationContract(IsOneWay = true)]
        void CannotHolding(string callsign);
        [OperationContract(IsOneWay = true)]
        void HoldingAtCallback(string callsign, string position, bool isInstrument);
        [OperationContract(IsOneWay = true)]
        void RouteUpdateCallback(string callsign, FlightRouteListModel route);
        [OperationContract(IsOneWay = true)]
        void ReadyJoinCircuit(string callsign);
        [OperationContract(IsOneWay = true)]
        void JoinCircuitCallback(string callsign, string runway);
        [OperationContract(IsOneWay = true)]
        void StartApproach(string callsign);
        [OperationContract(IsOneWay = true)]
        void StartGoRound(string callsign, bool isFlypass, bool isIfr);
        [OperationContract(IsOneWay = true)]
        void FinishGoRound(string callsign);
        [OperationContract(IsOneWay = true)]
        void CannotLanding(string callsign);
        [OperationContract(IsOneWay = true)]
        void StartLanding(string callsign);
        [OperationContract(IsOneWay = true)]
        void TouchGround(string callsign);
        [OperationContract(IsOneWay = true)]
        void FinishLanding(string callsign, float heading,string from, string position);



        #endregion

        #endregion

        #region Instructor Command

        #region Aircraft
        [OperationContract(IsOneWay = true)]
        void RemoveAircraftCallback(string callsign);

        [OperationContract(IsOneWay = true)]
        void LandingGearJamCallback(string callsign);

        [OperationContract(IsOneWay = true)]
        void EngineFailureCallback(string callsign, int engine);

        [OperationContract(IsOneWay = true)]
        void ChangeFuelCallback(string callsign, TimeSpan fuel);
        [OperationContract(IsOneWay = true)]
        void BirdAttackValue(string callsign, bool value);
        [OperationContract(IsOneWay = true)]
        void LandingGearJamValue(string callsign, bool value);
        #endregion

        #region Environment
        [OperationContract(IsOneWay = true)]
        void ChangeWeatherCallback(WeatherInfo weather);

        [OperationContract(IsOneWay = true)]
        void ChangeWindDirectionCallback(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeWindSpeedCallback(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeTemperatureCallback(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeVisibilityCallback(float value);

        [OperationContract(IsOneWay = true)]
        void ChangeTimeSimulationCallback(TimeSpan time);
        #endregion

        #region Light
        [OperationContract(IsOneWay = true)]
        void TaxiLightCallback(bool value);

        [OperationContract(IsOneWay = true)]
        void PapiLightCallback(bool value);

        [OperationContract(IsOneWay = true)]
        void RunwayLightCallback(string runway, bool value);

        [OperationContract(IsOneWay = true)]
        void ApproachLightCallback(string runway, bool value);
        #endregion

        #region Incident
        [OperationContract(IsOneWay = true)]
        void IncidentAnimalCallback(bool value, string runway);
        [OperationContract(IsOneWay = true)]
        void PKPPKCallback(string callsign);
        [OperationContract(IsOneWay = true)]
        void BirdAttackCallback(string callsign);
        #endregion

        #endregion
    }
}
