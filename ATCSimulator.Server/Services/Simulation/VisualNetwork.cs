﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using ATCSimulator.Server.Services;
using ATCSimulator.Server.Controller.Simulation.VisualRepository;

namespace ATCSimulator.Server.Services.Simulation
{
    public class VisualNetwork
    {
        public static VisualNetwork instance;
        private string host;
        private int port;
        private string zoneName;
        private string roomName;
        private string userName;
        private SmartFox sfs;

        GroundService ground;
        FlightService flight;
        OtherService other;

        public VisualNetwork(VisualConfig visual_config)
        {
            instance = this;
            this.host = visual_config.host;
            this.port = visual_config.port;
            this.zoneName = visual_config.zone_name;
            this.roomName = visual_config.room_name;
            this.userName = visual_config.user_name;
            sfs = new SmartFox();
            sfs.ThreadSafeMode = false;
            ground = new GroundService();
            flight = new FlightService();
            other = new OtherService();
        }
        public void Connect()
        {
            sfs.Connect(host, port);
            sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
            sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
            sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoom);
            sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);
            sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
            sfs.AddEventListener(SFSEvent.CONNECTION_LOST, Reconnect);
        }

        public void Reconnect(BaseEvent e)
        {
            sfs.RemoveAllEventListeners();
            sfs.Connect();
        }
        public void Disconnect()
        {
            if (sfs.IsConnected)
            {
                sfs.Disconnect();
                sfs.RemoveAllEventListeners();
            }
        }

        public void Send(string command, ISFSObject data)
        {
            sfs.Send(new ExtensionRequest(command, data));
        }

        #region Callback
        void OnConnection(BaseEvent e)
        {
            if ((bool)e.Params["success"])
            {
                Program.SendMessage("Connected to " + host + ":" + port);

                sfs.Send(new LoginRequest(userName, "", zoneName));
            }
            else
            {
                Program.SendMessage("Connection error!");
            }
        }

        void OnLogin(BaseEvent e)
        {
            Program.SendMessage("Login success : " + e.Params["user"]);
            sfs.Send(new JoinRoomRequest(roomName));
        }

        void OnLoginError(BaseEvent e)
        {
            Program.SendMessage(e.Params["errorMessage"].ToString());
        }

        void OnJoinRoom(BaseEvent e)
        {
            Program.SendMessage("JoinRoomm Success: " + e.Params["room"]);
        }

        void OnJoinRoomError(BaseEvent e)
        {
            Program.SendMessage(e.Params["errorMessage"].ToString());
        }
        void OnExtensionResponse(BaseEvent e)
        {
            string cmd = (string)e.Params["cmd"];
            ISFSObject data = (SFSObject)e.Params["params"];
            Program.SendMessage("Call form Visual : " + cmd + ", " + data.GetUtfString("callsign"));
            switch (cmd)
            {
                #region Ground Callback

                case "FinishReadyAircraft":
                    ground.FinishReadyCallback(data.GetUtfString("callsign"));
                    break;
                case "FinishPushback":
                    ground.FinishPushbackCallback(data.GetUtfString("callsign"), data.GetFloat("heading"));
                    break;
                case "TaxiPosition":
                    ground.TaxiPositionCallback(data.GetUtfString("callsign"), data.GetUtfString("position"), data.GetFloat("heading"));
                    break;
                case "FinishTaxi":
                    ground.FinishTaxiCallback(data.GetUtfString("callsign"),data.GetUtfString("position"),data.GetFloat("heading"),data.GetInt("status"));
                    break;
                case "HoldTaxi":
                    ground.HoldTaxiCallback(data.GetUtfString("callsign"),data.GetFloat("heading"));
                    break;
                case "RequestCrossRunway":
                    ground.RequestCrossRunwayCallback(data.GetUtfString("callsign"),data.GetFloat("heading"));
                    break;
                case "FinishUTurn":
                    ground.FinishUTurnCallback(data.GetUtfString("callsign"),data.GetFloat("heading"));
                    break;
                case "FinishHoldTakeOff":
                    ground.FinishHoldTakeOffCallback(data.GetUtfString("callsign"));
                    break;
                case "FinishTakeOff":
                    ground.FinishTakeOffCallback(data.GetUtfString("callsign"),data.GetFloat("speed"),data.GetFloat("altitude"));
                    break;
                case "CrashAircraft":
                    ground.CrashAircraftCallback(data.GetUtfString("callsign"));
                    break;
                case "FinishParking":
                    ground.FinishParkingCallback(data.GetUtfString("callsign"), data.GetUtfString("appron"));
                    break;
                case "BacktrackCallback":
                    ground.BacktrackCallback(data.GetUtfString("callsign"));
                    break;

                #endregion

                #region Flight Callback
                case "CannotChangeSpeed":
                    flight.CannotChangeSpeed(data.GetUtfString("callsign"));
                    break;
                case "CannotChangeAltitude":
                    flight.CannotChangeAltitude(data.GetUtfString("callsign"));
                    break;
                case "TakeOffCallback":
                    flight.TakeOffCallback(data.GetUtfString("callsign"));
                    break;
                case "UpdatePerformance":
                    flight.UpdatePerformanceCallback(data.GetUtfString("callsign"), data.GetFloat("speed"), data.GetFloat("altitude"));
                    break;
                case "UpdateHoldingPoint":
                    flight.UpdateHoldingPoint(data.GetUtfStringArray("holding").ToList());
                    break;
                case "CannotHolding":
                    flight.CannotHolding(data.GetUtfString("callsign"));
                    break;
                case "HoldingAtCallback":
                    flight.HoldingAtCallback(data.GetUtfString("callsign"),data.GetUtfString("position"),data.GetBool("isInstrument"));
                    break;
                case "RouteUpdateCallback":
                    flight.RouteUpdateCallback(data.GetUtfString("callsign"), data.GetUtfString("position"));
                    break;
                case "ReadyJoinCircuit":
                    flight.ReadyJoinCircuit(data.GetUtfString("callsign"));
                    break;
                case "JoinCircuitCallback":
                    flight.JoinCircuitCallback(data.GetUtfString("callsign"),data.GetUtfString("runway"));
                    break;
                case "StartApproach":
                    flight.StartApproach(data.GetUtfString("callsign"));
                    break;
                case "StartGoRound":
                    flight.StartGoRound(data.GetUtfString("callsign"), data.GetBool("isFlypass"), data.GetBool("isIfr"));
                    break;
                case "FinishGoRound":
                    flight.FinishGoRound(data.GetUtfString("callsign"));
                    break;
                case "CannotLanding":
                    flight.CannotLanding(data.GetUtfString("callsign"));
                    break;
                case "StartLanding":
                    flight.StartLanding(data.GetUtfString("callsign"));
                    break;
                case "TouchGround":
                    flight.TouchGround(data.GetUtfString("callsign"));
                    break;
                case "FinishLanding":
                    flight.FinishLandingCallback(data.GetUtfString("callsign"), data.GetFloat("heading"), data.GetUtfString("from"), data.GetUtfString("position"));
                    break;

                #endregion

                #region Aircraft Callback
                case "LandingGearJamValue":
                    other.LandingGearJamValue(data.GetUtfString("callsign"), data.GetBool("value"));
                    break;
                case "EngineFailureCallback":
                    other.EngineFailureCallback(data.GetUtfString("callsign"),data.GetInt("engine"));
                    break;
                case "BirdAttackValue":
                    other.BirdAttackValue(data.GetUtfString("callsign"), data.GetBool("value"));
                    break;
                case "AnimalCrossingEnable":
                    other.AnimalCrossingEnable(data.GetUtfString("runway"));
                    break;
                #endregion

                #region Other Callback

                case "ReadySimulation":
                    other.ReadySimulation();
                    break;

                #endregion
            }
        }
        #endregion
    }
}