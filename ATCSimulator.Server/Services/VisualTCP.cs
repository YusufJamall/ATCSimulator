﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.Server.Services
{
    public class VisualTCP
    {
        private Socket _serverSocket;
        private Socket _clientSocket;
        private byte[] _buffer;
        private int port;
        private ATCService service = new ATCService();
        public static VisualTCP Instance { get; private set; }

        public VisualTCP() { Instance = this; }
        public void ServerStart(int _port)
        {
            try
            {
                this.port = _port;
                _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _serverSocket.Bind(new IPEndPoint(IPAddress.Any, port));
                _serverSocket.Listen(100);//hanya 1 connection ke IGIntegrator
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
                Program.SendMessage("The server is up and running at port " + port);
            }
            catch (Exception ex)
            {
                Program.SendMessage(ex.Message);
            }
        }
        public void StopServer()
        {
            if (_clientSocket != null)
            {
                _clientSocket.Close();
            }
            _serverSocket.Close();
        }

        private void AcceptCallback(IAsyncResult AR)
        {
            try
            {
                _clientSocket = _serverSocket.EndAccept(AR);
                _buffer = new byte[_clientSocket.ReceiveBufferSize];
                _clientSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
                _serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            }
            catch (Exception ex)
            {
                Program.SendMessage(ex.Message);
            }
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            try
            {
                int received = _clientSocket.EndReceive(AR);

                if (received == 0)
                {
                    return;
                }
                Array.Resize(ref _buffer, received); // Shrink buffer to trim null characters
                string text = Encoding.ASCII.GetString(_buffer);
                Array.Resize(ref _buffer, _clientSocket.ReceiveBufferSize); // Regrow buffer
                //if (text.Equals("-play"))
                //    service.StartSimulation();
                //else
                //    service.ReceviceDataUnity(text);
                Program.SendMessage("Client says: " + text);

                // Start receiving data again
                _clientSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
            }
            catch (Exception ex)
            {
                Program.SendMessage(ex.Message);
            }
        }

        public static void SendData(string text)
        {
            try
            {
                Program.SendMessage(text);
                // Serialize the textBoxes text before sending
                byte[] buffer = Encoding.ASCII.GetBytes(text);
                Instance._clientSocket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(Instance.SendCallback), null);
            }
            catch (Exception ex)
            {
                Program.SendMessage(ex.Message);
            }
        }

        private void SendCallback(IAsyncResult AR)
        {
            try
            {
                _clientSocket.EndSend(AR);
            }
            catch (Exception ex)
            {
                Program.SendMessage(ex.Message);
            }
        }
    }
}
