//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ATCSimulator.Server.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Base_SimulationPlayer
    {
        public Base_SimulationPlayer()
        {
            this.Base_SimulationDeparture = new HashSet<Base_SimulationDeparture>();
        }
    
        public int ID { get; set; }
        public int SimulationID { get; set; }
        public string RolePlayer { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public Nullable<int> Point { get; set; }
        public string Grade { get; set; }
        public string EditedBy { get; set; }
    
        public virtual Base_Simulation Base_Simulation { get; set; }
        public virtual ICollection<Base_SimulationDeparture> Base_SimulationDeparture { get; set; }
    }
}
