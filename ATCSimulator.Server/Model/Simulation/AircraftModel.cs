﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATCSimulator.Server.Model.Simulation
{
    #region Enum
    public enum DirectionInfo
    {
        Left,
        Right
    }

    public enum HeadingInfo
    {
        Absolute,
        Relative
    }
    public enum GroundStatusInfo
    {
        Departure,
        Parking
    }
    public enum PilotCommandMenu
    {
        DepartureReady,
        FlightReady,
        Ground,
        Flight
    }
    public enum UturnInfo
    {
        UTurnLeft,
        UturnRight,
        Turn180,
        None
    }
    public enum AircraftInfoStatus
    {
        Disable,
        Enable,
        Blink
    }
    public enum AircraftStatus
    {
        NONE,
        ACC,
        APP,
        Form,
        Pushback,
        TakeOff,
        Taxing,
        Parking,
        Landing,
        FlyingToRoute,
        FlyingToDirection,
        Holding,
        Circuit,
        Approach,
        MissedApproach,
        Flypass,
        TouchGo,
        ExtendDownwind,
        Orbit

    }
    public enum CommandStatus
    {
        None,
        Process,
        Finish
    }
    public enum TaxiStatus
    {
        None,
        Process,
        Finish,
        Holding,
        CrossRunway
    }
    public enum GroundSpeed
    {
        Slow,
        Normal,
        Expedite
    }
    public enum FlightRuleInfo
    {
        IFR,
        VFR,
        Local
    }

    #endregion

    #region General Data

    public class AircraftData
    {
        public string callsign { get; set; }
        public bool is_departure { get; set; }
        public bool is_enabled { get; set; }
        public double heading { get; set; }
        public float speed { get; set; }
        public bool can_change_speed { get;set;}
        public float altitude { get; set; }
        public bool can_change_altitude { get; set; }
        public AircraftStatus status { get; set; }
        public int engine_failure { get; set; }
        public string from_position { get; set; }
        public string current_position { get; set; }
        public TimeSpan fuel { get; set; }
        public TimeSpan departure_time { get; set; }
        public TimeSpan start_time { get; set; }
        public TimeSpan estimate_time { get; set; }
        public PilotCommandProperties pilot_properties { get; set; }
        public AircraftProperties properties { get; set; }
        public UserPilotModel user { get; set; }

        public bool pkppk { get; set; }
        public bool incident_bird { get; set; }
        public bool incident_bird_enable { get; set; }
        public bool crashed { get; set; }
        public bool removed { get; set; }
        public bool landing_gear_jam { get; set; }
        public bool landing_gear_jam_enabled { get; set; }
        public bool animal_crossing_enabled { get; set; }

    }
    public class AircraftProperties
    {
        public string category { get; set; }
        public string livery { get; set; }
        public string model { get; set; }
        public int engine { get; set; }
        public string type { get; set; }
        public int person { get; set; }
        public string flight_level_type { get; set; }
        public int flight_level { get; set; }
        public string flight_type { get; set; }
        public double true_speed { get; set; }
        public double maximum_altitude { get; set; }
        public string livery_strips { get; set; }
    }
    public class UserPilotModel
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string home_base { get; set; }
        public string pc_role { get; set; }
        public string pc_name { get; set; }
        public string mac_address { get; set; }
        public string ip_address { get; set; }
    }
    public class PilotCommandProperties
    {
        public PilotCommandMenu status { get; set; }
        public PilotReadyCommand ready_departure { get; set; }
        public PilotReadyCommand ready_flight { get; set; }
        public PilotGroundCommand ground { get; set; }
        public PilotFlightCommand flight { get; set; }
    }

    #endregion

    #region Ground Model
    public class PilotReadyCommand
    {
        public bool is_enabled { get; set; }
    }
    public class PilotGroundCommand
    {
        public GroundStatusInfo status { get; set; }
        public PilotRunwayCommand runway { get; set; }
        public bool engine { get; set; }
        public bool engine_enabled { get; set; }
        public PilotParkingInfo parking { get; set; }
        public PilotPushbackCommand pushback { get; set; }
        public PilotTaxiCommand taxi { get; set; }
        public PilotOtherCommand other { get; set; }
        public PilotIntersectionCommand intersection { get; set; }
    }

    public class PilotParkingInfo
    {
        public int parking_id { get; set; }
        public string terminal_name { get; set; }
        public string appron_name { get; set; }

    }
    public class PilotIntersectionCommand
    {
        public bool is_show { get; set; }
        public bool take_off { get; set; }
        public bool runway_hold { get; set; }
        public CommandStatus status { get; set; }
    }
    public class PilotRunwayCommand
    {
        public string runway { get; set; }
        public bool is_show { get; set; }
    }
    public class PilotOtherCommand
    {
        public bool u_turn { get; set; }
        public UturnInfo u_turn_status { get; set; }
        public bool rocking_wing { get; set; }
    }
    public class PilotPushbackCommand
    {
        public CommandStatus status { get; set; }
        public bool is_show { get; set; }
        public bool pushback_left { get; set; }
        public bool pushback_right { get; set; }
        public bool pushback_left_enabled { get; set; }
        public bool pushback_right_enabled { get; set; }
        public string pushback_left_alias { get; set; }
        public string pushback_right_alias { get; set; }
    }
    public class PilotTaxiCommand
    {
        public bool is_show { get; set; }
        public bool taxi_route_enabled { get; set; }
        public bool taxi_route { get; set; }
        public bool cross_runway { get; set; }
        public TaxiStatus status { get; set; }
        public GroundSpeed speed { get; set; }
        public List<string> routes { get; set; }

    }

    #endregion

    #region Flight model
    public class PilotFlightCommand
    {
        public bool ILSmode { get; set; }
        public string runwayDefault { get; set; }
        public List<string> holdingPoint { get; set; }
        public FlightRouteListModel flight_route { get; set; }
        public bool can_changeRoute { get; set; }
        public PilotHoldingCommand holding { get; set; }
        public PilotHeadingCommand heading { get; set; }
        public PilotCircuitApproachCommand circuit_approarch { get; set; }
        public PilotDirectGoCommand direct_go { get; set; }
        public PilotDownwindOrbitCommand downwind_orbit { get; set; }
        public PilotChangeRunwayCommand change_runway { get; set; }
        public PilotFlightOtherCommand other { get; set; }
    }

    public class PilotHoldingCommand
    {
        public bool holding_left { get; set; }
        public bool holding_right { get; set; }
        public bool holdingAt { get; set; }
        public bool continue_route { get; set; }
        public bool continue_direction { get; set; }
        public string position_holding { get; set; }
        public bool holding_left_enabled { get; set; }
        public bool holding_right_enabled { get; set; }
        public bool holdingAt_enabled { get; set; }
        public bool continue_route_enabled { get; set; }
        public bool continue_direction_enabled { get; set; }
    }

    public class PilotHeadingCommand
    {
        public bool is_enabled { get; set; }
        public bool absolute_heading { get; set; }
        public bool relative_heading { get; set; }
        public float absolute_heading_value { get; set; }
        public float relative_heading_value { get; set; }
    }

    public class PilotCircuitApproachCommand
    {
        public bool instrument_approach { get; set; }
        public bool join_circuit { get; set; }
        public bool instrument_approach_enabled { get; set; }
        public bool join_circuit_enabled { get; set; }
        public string runway { get; set; }
    }

    public class PilotDirectGoCommand
    {
        public bool is_enabled { get; set; }
        public bool directGo { get; set; }
        public string position { get; set; }
        public string positionILS { get; set; }
    }
    public class PilotDownwindOrbitCommand
    {
        public bool downwind { get; set; }
        public bool orbit { get; set; }
        public bool downwind_enabled { get; set; }
        public bool orbit_enabled { get; set; }
    }

    public class PilotChangeRunwayCommand
    {
        public bool is_enabled { get; set; }
        public string runway { get; set; }
    }
    public class PilotFlightOtherCommand
    {
        public bool missed_approach { get; set; }
        public bool touch_go { get; set; }
        public bool flypass { get; set; }
        public bool flight_rocking_wing { get; set; }
        public bool return_base { get; set; }
        public bool abort_takeoff { get; set; }
        public bool missed_approach_enabled { get; set; }
        public bool touch_go_enabled { get; set; }
        public bool flypass_enabled { get; set; }
        public bool flight_rocking_wing_enabled { get; set; }
        public bool landing { get; set; }
        public string landingRunway { get; set; }
        public bool landing_enabled { get; set; }
        public bool return_base_isVisible{ get; set; }
        public bool return_base_enabled { get; set; }
        public bool abort_takeoff_isVisible { get; set; }
        public bool abort_takeoff_enabled { get; set; }
    }

    #endregion

}
