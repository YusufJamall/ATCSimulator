﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.Server.Model.Simulation
{
    #region Appron
    public class DataAppronModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public string terminal_name { get; set; }
        public string appron_name { get; set; }
        public DataPushbackModel pushback_left { get; set; }
        public DataPushbackModel pushback_right { get; set; }
        public bool can_pushback { get; set; }

    }
    public class DataPushbackModel
    {
        public string name { get; set; }
        public string alias { get; set; }
    }
    #endregion

    #region Taxi
    public class DataTaxiModel
    {
        public int id { get; set; }
        public string current { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public bool can_holding { get; set; }
        public bool is_parking { get; set; }
        public bool is_intersection { get; set; }
        public bool is_cross_rw { get; set; }
        public List<Vector3D> waypoints { get; set; }
    }
    #endregion

    //public class CorridorModel
    //{
    //    public int id { get; set; }
    //    public string name { get; set; }
    //    public CartesianModel cartesian { get; set; }
    //    public string waypoint_type { get; set; }
    //    public DecimalModel geolocation { get; set; }
    //}

    public class DataSimulationModel
    {
        public List<DataAppronModel> approns { get; set; }
        public List<FlightRouteListModel> flight_route_list { get; set; }
        public List<DataTaxiModel> taxi { get; set; }
    }
    public class Vector3D
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }

}
