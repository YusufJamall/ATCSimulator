﻿using ATCSimulator.Server.Contract.Simulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.Server.Model.Simulation
{
    public enum VisualFlight
    {
        Departure,
        Arrivals
    }
    public class WeatherModel
    {
        public WeatherInfo weather { get; set; }
        public float windDirection { get; set; }
        public float temperature { get; set; }
        public float visibility { get; set; }
        public TimeData time { get; set; }
    }
    public class TimeData
    {
        public int hour { get; set; }
        public int minute { get; set; }
    }

    public class VisualExerciseData //*Perubahan
    {
        public string scenery_name { get; set; }
        public WeatherModel weather { get; set; }
        public Dictionary<string, bool> runway_lights { get; set; }
        public Dictionary<string, bool> approach_lights { get; set; }
        public bool taxi { get; set; }
        public bool papi { get; set; }
    }

    public class VisualSimulationData
    {
        public int simulation_id { get; set; }
        public VisualExerciseData exercise { get; set; }
        public List<VisualAircraftData> aircrafts { get; set; }
    }

    public class VisualAircraftData
    {
        public string callsign { get; set; }
        public string model { get; set; }
        public string livery { get; set; }
        public double fuel { get; set; }
        public VisualFlight flight { get; set; }
        public string position { get; set; }
        public VisualFlightRouteModel flightRoutes { get; set; }
    }
    public class VisualFlightRouteModel
    {
        public FlightRuleInfo flightRules { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
        public string runway { get; set; }
        public List<string> routes { get; set; }
    }
}
