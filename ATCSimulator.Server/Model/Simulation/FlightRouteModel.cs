﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATCSimulator.Server.Model.Simulation
{
    public class DataFlightRouteModel
    {
        public List<DepartureRouteModel> departures { get; set; }
        public List<ArrivalRouteModel> arrivals { get; set; }
    }
    public class DepartureRouteModel : FlightRouteModel
    {
        public bool is_local_flight { get; set; }
        public bool is_end_point_airport { get; set; }
    }
    public class ArrivalRouteModel : FlightRouteModel
    {
        public bool is_destination_scenery { get; set; }
        public bool is_start_point_airport { get; set; }
    }
    public class AirportModel
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public RunwayProperties arrival { get; set; }
        public RunwayProperties departure { get; set; }
    }
    public class RunwayProperties
    {
        public Vector3D location { get; set; }
        public float heading { get; set; }
    }
    public class FlightRouteModel
    {
        public int id { get; set; }
        public List<string> runway_can_used { get; set; }
        public int flight_rules_id { get; set; }
        public string flight_rules { get; set; }
        public AirportModel origin { get; set; }
        public AirportModel destination { get; set; }
        public AirportModel alternate { get; set; }
        public List<FlightWaypointModel> routes { get; set; }
        public double total_distance { get; set; }
    }
    public class FlightWaypointModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public Vector3D position { get; set; }
    }

    #region Flight Route Detail Model
    public class FlightRouteListModel
    {
        public int id { get; set; }
        public bool is_departure { get; set; }
        public List<string> runways { get; set; }
        public int flight_rules_id { get; set; }
        public string flight_rules { get; set; }
        public int origin_airport_id { get; set; }
        public string origin_airport_name { get; set; }
        public string origin_airport_code { get; set; }
        public int destination_airport_id { get; set; }
        public string destination_airport_name { get; set; }
        public string destination_airport_code { get; set; }
        public int alternate_airport_id { get; set; }
        public string alternate_airport_name { get; set; }
        public string alternate_airport_code { get; set; }
        public string start_point_name { get; set; }
        public string end_point_name { get; set; }
        public List<string> routes_name { get; set; }
    }
    #endregion
}
