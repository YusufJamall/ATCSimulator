﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    public class DetailPCResponse
    {
        public DetailPCModel pc { get; set; }
        public ResponseData status { get; set; }
    }

    public class ListPCResponse
    {
        public List<DetailPCModel> pc { get; set; }
        public int count { get; set; }
        public ResponseData status { get; set; }
    }

    public class DetailPCModel
    {
        public int id { get; set; }
        public string pc_name { get; set; }
        public string ip_address { get; set; }
        public bool is_active { get; set; }
        public bool is_online { get; set; }
        public string mac_address { get; set; }
        public string role_pc { get; set; }
        public int? logged_user { get; set; }
        public List<int> role_user_access { get; set; }
        public int group { get; set; }
    }
}
