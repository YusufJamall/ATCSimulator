﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    #region Aircraft Detail Response
    public class FlightRuleListResponse
    {
        public List<FlightRuleDetailModel> flight_rules { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Aircraft Detail Model
    public class FlightRuleDetailModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    #endregion
}
