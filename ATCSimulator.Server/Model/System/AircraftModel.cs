﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{

    #region Aircraft Detail Response
    public class AircraftDetailResponse
    {
        public AircraftModel aircraft { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Aircraft List Response
    public class AircraftListResponse
    {
        public List<AircraftModel> aircrafts { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Aircraft Model
    public class AircraftModel
    {
        public int id { get; set; }
        public string model { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public double maximum_altitude { get; set; }
        public double true_speed { get; set; }
        public int engine { get; set; }
        public int maximum_person { get; set; }
        public double fuel_consumption { get; set; }
        public double circuit_height { get; set; }
        public string available_scenery { get; set; }
        public string notAvailable_airport { get; set; }
    }
    #endregion
}
