﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    #region Aircraft Detail Response
    public class FlightTypeListResponse
    {
        public List<FlightTypeDetailModel> flight_types { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Aircraft Detail Model
    public class FlightTypeDetailModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
    #endregion
}
