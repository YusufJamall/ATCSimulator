﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Contract.System;

namespace ATCSimulator.Server.Model.System
{
    #region Exercise List Response
    public class ExerciseListResponse
    {
        public List<ExerciseListModel> exercises { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Exercise List Model
    public class ExerciseListModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public string scenery_name { get; set; }
        public TimeSpan play_time { get; set; }
        public string exercise_name { get; set; }
        public string traffic_exercise { get; set; }
        public string weather { get; set; }
        public TimeSpan time_simulation { get; set; }
        public int total_arrival { get; set; }
        public int total_departure { get; set; }
    }
    #endregion

    #region Exercise Details Response
    public class ExerciseDetailResponse
    {
        public ExerciseDetailModel exercise { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Exercise Detail Model
    public class ExerciseDetailModel : ExerciseModel
    {

        public List<ListAircraftExerciseModel> aircrafts { get; set; }
    }
    public class ExerciseModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public string scenery_name { get; set; }
        public TimeSpan play_time { get; set; }
        public string exercise_name { get; set; }
        public string traffic_exercise { get; set; }
        public string desc_exercise { get; set; }
        public string weather { get; set; }
        public int wind_direction { get; set; }
        public int wind_speed { get; set; }
        public int temperature { get; set; }
        public int visibility { get; set; }
        public TimeSpan time_simulation { get; set; }
        public Dictionary<string, bool> runway_lights { get; set; }
        public Dictionary<string, bool> approach_lights { get; set; }
        public bool taxi { get; set; }
        public bool papi { get; set; }
    }
    #endregion

    #region List Airaft Exercise Model
    public class ListAircraftExerciseModel
    {
        public int aircraft_id { get; set; }
        public int livery_id { get; set; }
        public string livery_name { get; set; }
        public string livery_strips { get; set; }
        public int route_id { get; set; }
        public int flight_rules_id { get; set; }
        public string flight_rule_name { get; set; }
        public int flight_type_id { get; set; }
        public string flight_type { get; set; }
        public string flight_level_type { get; set; }
        public int flight_level { get; set; }
        public string callsign { get; set; }
        public int person_on_board { get; set; }
        public TimeSpan departure_time { get; set; }
        public TimeSpan estimate_time { get; set; }
        public TimeSpan fuel { get; set; }
        public bool is_departure { get; set; }
        public int? parking_id { get; set; }
        public TimeSpan? start_time { get; set; }
        public string runway { get; set; }
        public string destination { get; set; }
        public string origin { get; set; }
        public string aircraft_model { get; set; }
    }

    #endregion
}
