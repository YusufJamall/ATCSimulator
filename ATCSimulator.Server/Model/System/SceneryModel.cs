﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    #region Scenery Details Response
    public class SceneryDetailResponse
    {
        public SceneryDetailModel scenery { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region List Scenery Response
    public class SceneryListResponse
    {
        public List<SceneryListModel> sceneries { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region List Scenery model
    public class SceneryListModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    #endregion

    #region SceneryDetailModel
    public class SceneryDetailModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<string> runway_light { get; set; }
        public List<string> approach_light { get; set; }
        public bool taxi { get; set; }
        public bool papi { get; set; }
    }
    #endregion
}
