﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    #region Flight Route Response
    public class FlightRouteListResponse
    {
        public List<FlightRouteDetailModel> flight_routes { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Flight Route Response
    public class FlightRouteDetailResponse
    {
        public FlightRouteDetailModel flight_route { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Flight Route Detail Model
    public class FlightRouteDetailModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public int flight_rules_id { get; set; }
        public string flight_rules { get; set; }
        public int origin_airport_id { get; set; }
        public string origin_airport_name { get; set; }
        public string origin_airport_code { get; set; }
        public int destination_airport_id { get; set; }
        public string destination_airport_name { get; set; }
        public string destination_airport_code { get; set; }
        public int alternate_airport_id { get; set; }
        public string alternate_airport_name { get; set; }
        public string alternate_airport_code { get; set; }
        public string waypoint_name { get; set; }
        public List<string> routes_name { get; set; }
        public List<string> runways { get; set; }
        public double total_distance { get; set; }
    }	
    #endregion
}
