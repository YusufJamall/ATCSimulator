﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    #region Livery List Response
    public class LiveryListResponse
    {
        public List<LiveryDetailModel> liveries { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Livery Detail Response
    public class LiveryDetailResponse
    {
        public LiveryDetailModel livery { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Livery Detail Model
    public class LiveryDetailModel
    {
        public int id { get; set; }
        public int aircraft_id { get; set; }
        public string aircraft_model { get; set; }
        public string livery { get; set; }
        public string callsign { get; set; }
        public string aircraft_strips { get; set; }
    }
    #endregion
}
