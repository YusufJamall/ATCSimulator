﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    #region User Login
    public class UserLoginResponse
    {
        public UserProfileModel user { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region User List Response
    public class UserListResponse
    {
        public List<UserProfileModel> users { get; set; }
        public int count { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region User Details Response
    public class UserDetailResponse
    {
        public UserProfileModel user { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region User Profile
    public class UserProfileModel
    {
        public int ID { get; set; }
        public int user_ID { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public UserRoleModel role { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string user_identity_id { get; set; }
        public string gender { get; set; }
        public DateTime? birthdate { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public byte[] photo { get; set; }
        public bool isStudent { get; set; }
        public StudentInfoModel student_info { get; set; }
        public string create_by { get; set; }
        public DateTime create_date { get; set; }
        public string update_by { get; set; }
        public DateTime update_date { get; set; }
    }

    public class StudentInfoModel
    {
        public string @class { get; set; }
        public int? class_year { get; set; }
        public double? score { get; set; }
    }
    public class UserRoleModel
    {
        public int role_id { get; set; }
        public string role_name { get; set; }
        public string constant_variable { get; set; }
    }

    #endregion

    #region User List Student
    public class UserStudentSimulationResponse
    {
        public List<UserStudentSimulationModel> users { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region User Student Simulation Model
    public class UserStudentSimulationModel
    {
        public int profile_id { get; set; }
        public int user_ID { get; set; }
        public string username { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string user_identity_id { get; set; }
        public string gender { get; set; }
        public byte[] photo { get; set; }
        public string @class { get; set; }
        public int class_year { get; set; }
        public double? score { get; set; }
    }
    #endregion

    #region User Roles Response
    public class UserRolesResponse
    {
        public List<UserRoleModel> roles { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion
}
