﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Model.System
{
    #region Airport List Response
    public class AirportListResponse
    {
        public List<AirportDetailModel> airports { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Airport Detail Model
    public class AirportDetailModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public string airport_name { get; set; }
        public string airport_code { get; set; }
    }
    #endregion

    #region Airport Appron List Response
    public class AirportAppronListResponse
    {
        public List<AirportAppronModel> approns { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Airport Appron Detail Model
    public class AirportAppronModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public string terminal_name { get; set; }
        public string appron_name { get; set; }
    }
    #endregion

    #region Runway List Response
    public class RunwayListResponse
    {
        public List<RunwayDetailModel> runways { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Runway Detail Model
    public class RunwayDetailModel
    {
        public int id { get; set; }
        public int scenery_id { get; set; }
        public string runway_name { get; set; }
        public bool is_helicopter { get; set; }
    }
    #endregion


}
