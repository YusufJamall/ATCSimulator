﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Contract.System;

namespace ATCSimulator.Server.Model.System
{
    #region History Player Response
    public class HistoryPlayerResponse
    {
        public List<HistoryPlayerModel> history { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region History Instructor Response
    public class HistoryInstructorResponse
    {
        public List<SimulationListModel> simulations { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region History List Model
    public class HistoryPlayerModel
    {
        public SimulationPlayerModel simulation { get; set; }
        public PlayerListModel player { get; set; }
    }

    #endregion

    #region Simulation List Response
    public class SimulationListResponse : HistoryInstructorResponse
    {
        public int count { get; set; }
    }
    #endregion

    #region Simulation Model
    public class SimulationPlayerModel
    {
        public int id { get; set; }
        public string exercise_name { get; set; }
        public string scenery_name { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
    }

    public class SimulationListModel : SimulationPlayerModel
    {

        public int scenery_id { get; set; }
        public TimeSpan play_time { get; set; }
        public TimeSpan? elapsed_time { get; set; }
        public string weather { get; set; }
        public int wind_direction { get; set; }
        public int wind_speed { get; set; }
        public int temperature { get; set; }
        public int visibility { get; set; }
        public TimeSpan time_simulation { get; set; }
        public string runway_lights { get; set; }
        public bool taxi { get; set; }
        public bool papi { get; set; }
        public string approach { get; set; }

        public int total_arrival { get; set; }
        public int total_departure { get; set; }

        public List<AircraftHistoryData> list_aircraft { get; set; }



    }
    #endregion

    #region Aircraft Simulation
    public class AircraftHistoryData
    {
        public string callsign { get; set; }
        public bool is_departure { get; set; }
        public string category { get; set; }
        public string livery { get; set; }
        public string model { get; set; }
        public int engine { get; set; }
        public string type { get; set; }
        public int person { get; set; }
        public string flight_level_type { get; set; }
        public int flight_level { get; set; }
        public double true_speed { get; set; }
        public double maximum_altitude { get; set; }
        public string livery_strips { get; set; }
        public TimeSpan eet { get; set; }
        public TimeSpan departure_time { get; set; }
        public TimeSpan fuel { get; set; }
        public int route_id { get; set; }
        public string route { get; set; }
        public int flight_rules_id { get; set; }
        public string flight_rules { get; set; }
        public int origin_airport_id { get; set; }
        public string origin_airport_name { get; set; }
        public string origin_airport_code { get; set; }
        public int destination_airport_id { get; set; }
        public string destination_airport_name { get; set; }
        public string destination_airport_code { get; set; }
        public int alternate_airport_id { get; set; }
        public string alternate_airport_name { get; set; }
        public string alternate_airport_code { get; set; }
        public string start_point_name { get; set; }
        public string end_point_name { get; set; }
        public int pilotId { get; set; }
    }
    public class PilotInfoModel
    {
        public string name { get; set; }
        public string address_city { get; set; }
        public string phone { get; set; }
        public string airport_city { get; set; }
    }
    #endregion

    #region Aircraft Simulation List Response
    public class AircraftHistoryListResponse
    {
        public List<AircraftHistoryData> aircrafts { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Simulation Player List Response
    public class PlayerListResponse
    {
        public List<PlayerListModel> players { get; set; }
        public ResponseData status { get; set; }
    }
    #endregion

    #region Simulation Player List Model
    public class PlayerListModel
    {
        public int id { get; set; }
        public string role_player { get; set; }
        public int user_ID { get; set; }
        public string fullname { get; set; }
        public string pilot { get; set; }
        public string point { get; set; }
        public string grade { get; set; }
        public string edited_by { get; set; }
        public byte[] photo_image { get; set; }
    }
    #endregion
}
