﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATCSimulator.Server.Helper
{
    public class ResponseData
    {
        public int code { get; set; }
        public string message { get; set; }
        public string description { get; set; }
    }
    public class StatusResponse
    {
        public ResponseData status { get; set; }
    }
    public class SimulationConnectResponse : StatusResponse
    {
        public bool isPlay { get; set; }
    }
    public class UserComputerInfo
    {
        public string pc_name { get; set; }
        public string ip_address { get; set; }
        public string mac_address { get; set; }
        public string role_pc { get; set; }
        public string application { get; set; }
    }

    public class SimulationResponse
    {
        public ResponseData status { get; set; }
        public int simulationID { get; set; }
    }
}
