﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATCSimulator.Server.Helper
{
    public class VisualServiceHelper
    {
        #region Ground
        public static string READY_AIRCRAFT
        {
            get { return "ReadyAircraft"; }
        }
        public static string ENGINE_START
        {
            get { return "Engine"; }
        }
        public static string PUSHBACK
        {
            get { return "Pushback"; }
        }
        public static string START_TAXI
        {
            get { return "StartTaxi"; }
        }
        public static string TAXI_SPEED
        {
            get { return "TaxiSpeed"; }
        }
        public static string HOLD_TAXI
        {
            get { return "HoldTaxi"; }
        }
        public static string CONTINUE_TAXI
        {
            get { return "ContinueTaxi"; }
        }
        public static string CROSS_RUNWAY
        {
            get { return "CrossRunway"; }
        }
        public static string U_TURN
        {
            get { return "UTurn"; }
        }
        public static string G_ROCKING_WING
        {
            get { return "GroundRockingWing"; }
        }
        public static string HOLD_TAKE_OFF
        {
            get { return "HoldTakeOff"; }
        }
        public static string TAKE_OFF
        {
            get { return "TakeOff"; }
        }
        public static string PKPPK
        {
            get { return "PKPPK"; }
        }
        #endregion

        #region Flight

        public static string ACTIVE_AIRCRAFT
        {
            get { return "ActiveAircraft"; }
        }
        public static string HOLDING_NOW
        {
            get { return "HoldingNow"; }
        }
        public static string HOLDING_POSITION
        {
            get { return "HoldingPosition"; }
        }
        public static string STOP_HOLDING
        {
            get { return "StopHolding"; }
        }
        public static string CONTINUE_ROUTE
        {
            get { return "ContinueRoute"; }
        }
        public static string CONTINUE_DIRECTION
        {
            get { return "ContinueDirection"; }
        }
        public static string DIRECT_GO
        {
            get { return "DirectGo"; }
        }
        public static string ILS_DIRECT_GO
        {
            get { return "ILSDirectGo"; }
        }
        public static string CHANGE_HEADING
        {
            get { return "ChangeHeading"; }
        }
        public static string CHANGE_ALTITUDE
        {
            get { return "ChangeAltitude"; }
        }
        public static string CHANGE_ROUTE
        {
            get { return "ChangeRoute"; }
        }
        public static string ALTITUDE
        {
            get { return "Altitude"; }
        }
        public static string SPEED
        {
            get { return "Speed"; }
        }
        public static string APPROACH
        {
            get { return "Approach"; }
        }
        public static string CIRCUIT
        {
            get { return "Circuit"; }
        }
        public static string MISSED_APPROACH
        {
            get { return "MissedApproach"; }
        }
        public static string TOUCH_GO
        {
            get { return "TouchGo"; }
        }
        public static string FLYPASS
        {
            get { return "Flypass"; }
        }
        public static string FLIGHT_ROCKING_WING
        {
            get { return "FlightRockingWing"; }
        }
        public static string EXTEND_DOWNWIND
        {
            get { return "ExtendDownwind"; }
        }
        public static string ORBIT
        {
            get { return "Orbit"; }
        }
        public static string LANDING
        {
            get { return "Landing"; }
        }
        public static string ILS_LANDING
        {
            get { return "ILSLanding"; }
        }
        public static string RETURN_BASE
        {
            get { return "ReturnBase"; }
        }
        public static string ABORTED_TAKE_OFF
        {
            get { return "AbortedTakeOff"; }
        }
        public static string CHANGE_RUNWAY
        {
            get { return "ChangeRunway"; }
        }
        public static string LANDING_GEAR_JAM
        {
            get { return "LandingGearJam"; }
        }
        #endregion

        #region Aircraft
        public static string REMOVE_AIRCRAFT
        {
            get { return "RemoveAircraft"; }
        }
        public static string SELECT_AIRCRAFT
        {
            get { return "SelectAircraft"; }
        }
        public static string ENGINE_FAILURE
        {
            get { return "EngineFailure"; }
        }
        public static string CHANGE_FUEL
        {
            get { return "ChangeFuel"; }
        }
        #endregion

        #region Other

        public static string START_SIMULATION
        {
            get { return "StartSimulation"; }
        }
        public static string PAUSE_SIMULATION
        {
            get { return "PauseSimulation"; }
        }
        public static string STOP_SIMULATION
        {
            get { return "StopSimulation"; }
        }
        public static string BIRD_ATTACK
        {
            get { return "BirdAttack"; }
        }
        public static string ANIMAL_CROSSING
        {
            get { return "AnimalCrossing"; }
        }
        public static string TAXI_LIGHT
        {
            get { return "TaxiLight"; }
        }
        public static string PAPI_LIGHT
        {
            get { return "PapiLight"; }
        }
        public static string RUNWAY_LIGHT
        {
            get { return "RunwayLight"; }
        }
        public static string APPROACH_LIGHT
        {
            get { return "ApproachLight"; }
        }
        public static string WEATHER
        {
            get { return "Weather"; }
        }
        public static string WIND_DIRECTION
        {
            get { return "WindDirection"; }
        }
        public static string WIND_SPEED
        {
            get { return "WindSpeed"; }
        }
        public static string TEMPERATURE
        {
            get { return "Temperature"; }
        }
        public static string VISIBILITY
        {
            get { return "Visibility"; }
        }
        public static string TIME_SIMULATION
        {
            get { return "TimeSimulation"; }
        }

        #endregion

        #region Visual

        #endregion

        #region Send Config
        public static string SEND_TO_VISUAL
        {
            get { return "SendToVisual"; }
        }
        public static string SEND_TO_VISUAL_COMPUTER
        {
            get { return "SendToVisualComputer"; }
        }
        public static string SEND_TO_TARGET
        {
            get { return "SendToTarget"; }
        }
        public static string SEND_TO_ALL_EXCEPT_ME
        {
            get { return "SendToExceptMe"; }
        }
        #endregion
    }
}
