﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Contract.Simulation;
using ATCSimulator.Server.Services.Simulation;
using Newtonsoft.Json;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.Simulation
{
    public class SimulationRepository
    {
        public ExerciseSimulationModel exercise { get; private set; }
        public List<AircraftData> aircrafts { get; private set; }
        public DataSimulationModel database { get; private set; }
        private SimulationService service;

        private Dictionary<int, AircraftData> aircraft_active = new Dictionary<int, AircraftData>();
        public SimulationRepository(SimulationService _service, ExerciseSimulationModel _exercise, DataSimulationModel _database, List<AircraftData> _aircrafts)
        {
            this.service = _service;
            this.exercise = _exercise;
            this.database = _database;
            this.aircrafts = _aircrafts;
        }
        #region Aircraft Command

        #region Instructor

        #region incident
        public void IncidentBird(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.incident_bird = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        public void BirdAttackValue(string callsign, bool value)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.incident_bird_enable = value;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }

        public void LandingGearJamValue(string callsign, bool value)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.landing_gear_jam_enabled = value;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }

        public void AnimalCrossingValue(bool value, string runway)
        {
            foreach (var a in aircrafts)
            {
                a.animal_crossing_enabled = value;
            }
        }

        public void PKPPK(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pkppk = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        public void AnimalCrossing(bool enabled)
        {
            foreach (var a in aircrafts)
            {
                a.animal_crossing_enabled = enabled;
            }
        }
        #endregion

        #region Aircraft

        public void RemoveAircraft(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.removed = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        public void LandingGearJam(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.landing_gear_jam = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }

        public void EngineFailure(string callsign, int engine)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.engine_failure = engine;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }

        }

        public void ChangeFuel(string callsign, TimeSpan fuel)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.fuel = fuel;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Environment
        public void ChangeWeather(WeatherInfo weather)
        {
            throw new NotImplementedException();
        }

        public void ChangeWindDirection(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeWindSpeed(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeTemperature(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeVisibility(double value)
        {
            throw new NotImplementedException();
        }

        public void ChangeTimeSimulation(TimeSpan time)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Lights
        public void LightRunway(string runway, bool value)
        {
            throw new NotImplementedException();
        }

        public void LightApproach(string approach, bool value)
        {
            throw new NotImplementedException();
        }

        public void LightTaxi(bool value)
        {
            throw new NotImplementedException();
        }

        public void LightPapi(bool value)
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion

        #region Ground

        #region Change Ground Status
        public void ChangeGroundStatus(string callsign, int status)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;
                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.None;
                aircraft.pilot_properties.ground.taxi.routes = new List<string>();
                aircraft.pilot_properties.ground.taxi.taxi_route = false;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Ready Aircraft
        public void ReadyAircraft(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ready_departure.is_enabled = false;
                aircraft.status = AircraftStatus.Form;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Change Runway
        public void ChangeRunway(string callsign, string runway, bool is_ground)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                if (is_ground)
                    aircraft.pilot_properties.ground.runway.runway = runway;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Start Engine
        public void StartEnigne(string callsign, bool engine)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.engine = engine;
                Dictionary<string, string> _data_visual = new Dictionary<string, string>();
                _data_visual.Add("command", "AEngine");
                _data_visual.Add("callsign", callsign);
                _data_visual.Add("engine", engine.ToString());
                string send_data_visual = string.Join("&", _data_visual.Select(x => x.Key + "=" + x.Value).ToArray());
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Pushback
        public string Pushback(string callsign, DirectionInfo pushback)
        {
            AircraftData aircraft = FindAircraft(callsign);
            string result = "";
            if (aircraft != null)
            {
                DataAppronModel appron = database.approns.Where(d => d.appron_name.Equals(aircraft.current_position)).FirstOrDefault();
                DataPushbackModel pushback_data = new DataPushbackModel();
                if (pushback.Equals(DirectionInfo.Left))
                    pushback_data = appron.pushback_left;
                else if (pushback.Equals(DirectionInfo.Right))
                    pushback_data = appron.pushback_right;

                if (pushback_data != null)
                {
                    aircraft.pilot_properties.ground.pushback.is_show = false;
                    aircraft.pilot_properties.ground.pushback.status = CommandStatus.Process;
                    aircraft.pilot_properties.ground.pushback.pushback_left = pushback.Equals(DirectionInfo.Left) ? true : false;
                    aircraft.pilot_properties.ground.pushback.pushback_right = pushback.Equals(DirectionInfo.Right) ? true : false;
                    aircraft.from_position = aircraft.current_position;
                    aircraft.current_position = pushback_data.name;
                    result = pushback_data.name;
                }
                else
                {
                    Program.SendMessage("Cant find Pushback Data");
                }
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
            return result;
        }
        #endregion

        #region Change Flight Route
        public void ChangeFlightRoutes(string callsign, FlightRouteListModel routes)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.flight.flight_route = routes;
                aircraft.pilot_properties.ground.runway.runway = routes.runways.FirstOrDefault();
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Start Taxi
        public void StartTaxi(string callsign, List<string> routes)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                aircraft.pilot_properties.ground.taxi.taxi_route = true;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                aircraft.pilot_properties.ground.taxi.routes = routes;
                aircraft.pilot_properties.ground.taxi.cross_runway = false;
                aircraft.pilot_properties.ground.runway.is_show = false;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Hold Taxi
        public void HoldTaxi(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Holding;
                aircraft.pilot_properties.ground.taxi.taxi_route = true;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                aircraft.pilot_properties.ground.taxi.cross_runway = false;
                aircraft.pilot_properties.ground.runway.is_show = true;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Continue Taxi
        public void ContinueTaxi(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                aircraft.pilot_properties.ground.taxi.taxi_route = true;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                aircraft.pilot_properties.ground.taxi.cross_runway = false;
                aircraft.pilot_properties.ground.runway.is_show = false;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Change Speed Taxi
        public void ChangeSpeedTaxi(string callsign, GroundSpeed speed)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.taxi.speed = speed;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Cross Runway
        public void CrossRunway(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                aircraft.pilot_properties.ground.taxi.taxi_route = true;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                aircraft.pilot_properties.ground.taxi.cross_runway = false;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region UTurn
        public void Uturn(string callsign, UturnInfo uturn)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.other.u_turn_status = uturn;
                aircraft.pilot_properties.ground.other.u_turn = false;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Rocking Wing
        public void RockingWing(string callsign, bool value)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.other.rocking_wing = value;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region TakeOff
        public void TakeOff(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.intersection.runway_hold = false;
                aircraft.pilot_properties.ground.intersection.take_off = false;
                aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                aircraft.pilot_properties.ground.other.u_turn = false;
                aircraft.pilot_properties.ground.engine_enabled = false;
                aircraft.pilot_properties.ground.runway.is_show = false;
                aircraft.pilot_properties.ground.other.u_turn = false;
                aircraft.pilot_properties.ground.intersection.is_show = false;
                aircraft.pilot_properties.ground.intersection.take_off = true;
                aircraft.status = AircraftStatus.TakeOff;
                aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Runway Hold
        public void RunwayHold(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.intersection.runway_hold = false;
                aircraft.pilot_properties.ground.intersection.take_off = false;
                aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                aircraft.status = AircraftStatus.TakeOff;
                aircraft.pilot_properties.status = PilotCommandMenu.Flight;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion




        #region Finish Ready Aircraft
        public void FinishReadyAircraft(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
                aircraft.pilot_properties.status = PilotCommandMenu.Ground;
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Finish Pushback
        public void FinishPushback(string callsign, double heading)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.pushback.status = CommandStatus.Finish;
                aircraft.pilot_properties.ground.engine_enabled = true;
                aircraft.pilot_properties.ground.runway.is_show = true;
                aircraft.pilot_properties.ground.taxi.is_show = true;
                aircraft.pilot_properties.ground.other.u_turn = true;
                aircraft.pilot_properties.ground.intersection.is_show = false;
                aircraft.heading = heading;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Update Taxi Position
        public void UpdateTaxiPosition(string callsign, string position, double heading)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.from_position = aircraft.current_position;
                aircraft.current_position = position;
                aircraft.heading = heading;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Finish Taxi
        public void FinishTaxi(string callsign, string position, double heading, int status)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.engine_enabled = true;
                aircraft.pilot_properties.ground.runway.is_show = true;
                aircraft.pilot_properties.ground.other.u_turn = true;

                aircraft.heading = heading;
                aircraft.from_position = aircraft.current_position;
                aircraft.current_position = position;
                aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;

                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Finish;
                aircraft.pilot_properties.ground.taxi.taxi_route = false;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                aircraft.pilot_properties.ground.taxi.cross_runway = false;
                aircraft.pilot_properties.ground.intersection.is_show = true;
                aircraft.pilot_properties.ground.intersection.runway_hold = true;
                aircraft.pilot_properties.ground.intersection.take_off = true;
                aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Heading Aircraft
        public void HeadingAircraft(string callsign, double heading)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.heading = heading;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Request Cross Runway
        public void RequestCrossRunway(string callsign, double heading)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.engine_enabled = true;
                aircraft.pilot_properties.ground.runway.is_show = true;
                aircraft.pilot_properties.ground.other.u_turn = true;
                aircraft.pilot_properties.ground.intersection.is_show = true;

                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.CrossRunway;
                aircraft.pilot_properties.ground.taxi.taxi_route = true;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                aircraft.pilot_properties.ground.taxi.cross_runway = true;

                aircraft.heading = heading;

            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Finish UTurn
        public void FinishUTurn(string callsign, double heading)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.engine_enabled = true;
                aircraft.pilot_properties.ground.runway.is_show = true;
                aircraft.pilot_properties.ground.taxi.is_show = true;
                aircraft.pilot_properties.ground.intersection.is_show = false;

                aircraft.pilot_properties.ground.other.u_turn_status = UturnInfo.None;
                aircraft.pilot_properties.ground.other.u_turn = true;
                aircraft.pilot_properties.ground.taxi.status = TaxiStatus.None;
                aircraft.pilot_properties.ground.taxi.taxi_route = false;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                aircraft.from_position = "";
                aircraft.heading = heading;

            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Finish Hold Take Off
        public void FinishHoldTakeOff(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.intersection.runway_hold = false;
                aircraft.pilot_properties.ground.intersection.take_off = true;
                aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;

            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Finish Take Off
        public void FinishTakeOff(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.status = AircraftStatus.TakeOff;
                aircraft.pilot_properties.status = PilotCommandMenu.Flight;
                aircraft.pilot_properties.ground.intersection.status = CommandStatus.Finish;

            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Crash Aircraft
        public void CrashAircraft(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.crashed = true;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #region Finish Parking
        public AircraftData FinishParking(string callsign, string appron)
        {
            AircraftData aircraft = FindAircraft(callsign);
            DataAppronModel appron_data = database.approns.Where(d => d.appron_name.ToLower().Equals(appron.ToLower())).FirstOrDefault();
            if (aircraft != null && appron_data != null)
            {

                aircraft.current_position = appron_data.appron_name;
                aircraft.from_position = appron_data.appron_name;
                aircraft.is_enabled = true;
                aircraft.status = AircraftStatus.NONE;
                aircraft.heading = 0;
                aircraft.pilot_properties.ground.status = GroundStatusInfo.Departure;
                aircraft.pilot_properties.ground.engine = false;
                aircraft.pilot_properties.ground.engine_enabled = true;
                aircraft.pilot_properties.ground.pushback = new PilotPushbackCommand()
                {
                    status = CommandStatus.None,
                    is_show = appron_data.can_pushback,
                    pushback_left_enabled = appron_data.pushback_left != null ? true : false,
                    pushback_right_enabled = appron_data.pushback_right != null ? true : false,
                    pushback_left = false,
                    pushback_right = false,
                    pushback_left_alias = appron_data.pushback_left != null ? appron_data.pushback_left.alias : "-",
                    pushback_right_alias = appron_data.pushback_right != null ? appron_data.pushback_right.alias : "-"
                };
                aircraft.pilot_properties.ground.taxi = new PilotTaxiCommand()
                {
                    is_show = !appron_data.can_pushback,
                    cross_runway = false,
                    routes = new List<string>(),
                    speed = GroundSpeed.Normal,
                    status = TaxiStatus.None,
                    taxi_route = false,
                    taxi_route_enabled = true
                };
                aircraft.pilot_properties.ground.other = new PilotOtherCommand()
                {
                    rocking_wing = false,
                    u_turn = false,
                    u_turn_status = UturnInfo.None
                };
                aircraft.pilot_properties.ground.intersection = new PilotIntersectionCommand()
                {
                    is_show = false,
                    runway_hold = false,
                    status = CommandStatus.None,
                    take_off = false
                };
                aircraft.pilot_properties.ground.parking = new PilotParkingInfo
                {
                    parking_id = appron_data.id,
                    terminal_name = appron_data.terminal_name,
                    appron_name = appron_data.appron_name
                };
                aircraft.pilot_properties.ready_departure = new PilotReadyCommand()
                {
                    is_enabled = true,
                };
                aircraft.pilot_properties.status = PilotCommandMenu.DepartureReady;

            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);

            return aircraft;
        }

        #endregion

        #region Backtrack
        public void Backtrack(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.ground.intersection.is_show = true;
                aircraft.pilot_properties.ground.intersection.runway_hold = false;
                aircraft.pilot_properties.ground.intersection.take_off = false;
                aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;

            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion

        #endregion

        #region Flight

        #region Active Aircraft
        public void ActiveAircraft(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.holding.continue_route = true;
                aircraft.pilot_properties.ready_flight.is_enabled = false;
                aircraft.status = AircraftStatus.FlyingToRoute;
                aircraft.pilot_properties.status = PilotCommandMenu.Flight;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Holding Now
        public void HoldingNow(string callsign, DirectionInfo direction)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                if (direction.Equals(DirectionInfo.Left))
                    aircraft.pilot_properties.flight.holding.holding_left = true;
                else
                    aircraft.pilot_properties.flight.holding.holding_right = true;
                aircraft.status = AircraftStatus.Holding;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = true;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = true;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Holding Position
        public void HoldingPosition(string callsign, string position)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.flight.holding.holdingAt = true;
                aircraft.pilot_properties.flight.holding.position_holding = position;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Continue Route
        public void ContinueRoute(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.FlyingToRoute;
                aircraft.pilot_properties.flight.holding.continue_route = true;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Continue Direction
        public void ContinueDirection(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.FlyingToDirection;
                aircraft.pilot_properties.flight.holding.continue_direction = true;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Change Heading
        public void ChangeHeading(string callsign, HeadingInfo direction, float heading)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.FlyingToDirection;
                if (direction.Equals(HeadingInfo.Absolute))
                {
                    aircraft.pilot_properties.flight.heading.absolute_heading = true;
                    aircraft.pilot_properties.flight.heading.absolute_heading_value = heading;
                }
                else
                {
                    aircraft.pilot_properties.flight.heading.relative_heading = true;
                    aircraft.pilot_properties.flight.heading.relative_heading_value = heading;
                }
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Change Route
        public void ChangeRoute(string callsign, FlightRouteListModel routes)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.FlyingToRoute;
                aircraft.pilot_properties.flight.flight_route = routes;
                aircraft.pilot_properties.flight.holding.continue_route = true;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Direct Go
        public void DirectGo(string callsign, string position, bool isILS)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.FlyingToDirection;
                aircraft.pilot_properties.flight.direct_go.directGo = true;
                if (isILS)
                    aircraft.pilot_properties.flight.direct_go.positionILS = position;
                else
                    aircraft.pilot_properties.flight.direct_go.position = position;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Instrument Approach
        public void InstrumentApproach(string callsign, string runway)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.Approach;
                aircraft.pilot_properties.flight.circuit_approarch.instrument_approach = true;
                aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                aircraft.pilot_properties.flight.can_changeRoute = false;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.heading.is_enabled = false;
                aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                aircraft.pilot_properties.flight.other.flypass_enabled = false;
                aircraft.pilot_properties.flight.change_runway.is_enabled = false;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Join Circuit
        public void JoinCircuit(string callsign, string runway)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.Circuit;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit = true;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                aircraft.pilot_properties.flight.can_changeRoute = false;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.heading.is_enabled = false;
                aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                aircraft.pilot_properties.flight.other.missed_approach_enabled = false;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Missed Approach
        public void MissedApproach(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.other.missed_approach = true;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Touch Go
        public void TouchGo(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.other.touch_go = true;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Flypass
        public void Flypass(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.other.flypass = true;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Extend Downwind
        public void ExtendDownwind(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.downwind_orbit.downwind = true;
                aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                aircraft.pilot_properties.flight.other.landing_enabled = true;
                aircraft.pilot_properties.flight.other.flypass_enabled = false;
                aircraft.pilot_properties.flight.other.touch_go_enabled = false;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Orbit
        public void Orbit(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.downwind_orbit.orbit = true;
                aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                aircraft.pilot_properties.flight.other.landing_enabled = true;
                aircraft.pilot_properties.flight.other.flypass_enabled = false;
                aircraft.pilot_properties.flight.other.touch_go_enabled = false;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Landing
        public void Landing(string callsign,string runway)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                if (runway.Equals(""))
                {
                    aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                    aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    aircraft.pilot_properties.flight.other.flypass_enabled = false;
                }

                aircraft.pilot_properties.flight.other.landing = true;
                aircraft.pilot_properties.flight.other.landingRunway = runway;
                aircraft.pilot_properties.flight.other.landing_enabled = false;
                aircraft.pilot_properties.flight.heading.is_enabled = false;
                aircraft.pilot_properties.flight.direct_go.directGo = false;

            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Altitude
        public void Altitude(string callsign, float altitude)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.altitude = altitude;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Speed
        public void Speed(string callsign, float speed)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.speed = speed;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Change ILS Mode
        public void ChangeILSMode(string callsign, bool value)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.flight.ILSmode = value;
            }
            else
                Program.SendMessage("Cant Find Aircraft " + callsign);
        }
        #endregion




        #region Cannot Change Speed
        public void CannotChangeSpeed(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.can_change_speed = false;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Cannot Change Altitude
        public void CannotChangeAltitude(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.can_change_altitude = false;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Update Holding Point
        public void UpdateHoldingPoint(List<string> holding)
        {
            foreach (var a in aircrafts)
            {
                a.pilot_properties.flight.holdingPoint = holding;
            }
        }
        #endregion

        #region Cannot Holding
        public void CannotHolding(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.flight.holding.holdingAt = false;
                aircraft.pilot_properties.flight.holding.position_holding = "";
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Holding At Callback
        public void HoldingAtCallback(string callsign, string position, bool isInstrument)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.Holding;
                aircraft.pilot_properties.flight.holding.holdingAt = true;
                aircraft.pilot_properties.flight.holding.position_holding = position;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = true;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = !isInstrument;
                if (isInstrument)
                {
                    aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = true;
                }
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Route Update Callback
        public FlightRouteListModel RouteUpdateCallback(string callsign, string position)
        {
            AircraftData aircraft = FindAircraft(callsign);
            FlightRouteListModel route = new FlightRouteListModel();
            if (aircraft != null)
            {
                route = database.flight_route_list.Where(d => d.is_departure.Equals(aircraft.is_departure) && d.routes_name.Where(e => e.ToLower().Contains(position.ToLower())).Count() > 0).FirstOrDefault();
                if (route != null)
                {
                    aircraft.pilot_properties.flight.flight_route = route;
                    Program.SendMessage(route.routes_name.Aggregate((i, j) => i + " " + j));
                }
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
            return route;
        }
        #endregion

        #region Ready Join Circuit
        public void ReadyJoinCircuit(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Join Circuit Callback
        public void JoinCircuitCallback(string callsign, string runway)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.Circuit;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit = true;
                aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                aircraft.pilot_properties.flight.can_changeRoute = false;
                aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                aircraft.pilot_properties.flight.heading.is_enabled = false;
                aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = true;
                aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                aircraft.pilot_properties.flight.other.flypass_enabled = true;
                aircraft.pilot_properties.flight.other.landing_enabled = true;
                aircraft.pilot_properties.flight.change_runway.is_enabled = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Start Approach
        public void StartApproach(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.pilot_properties.flight.other.missed_approach_enabled = true;
                aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                aircraft.pilot_properties.flight.other.landing_enabled = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Start Go Round
        public void StartGoRound(string callsign, bool isFlypass, bool isIfr)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                if (isFlypass)
                {
                    if (isIfr)
                    {
                        aircraft.status = AircraftStatus.MissedApproach;
                        aircraft.pilot_properties.flight.other.missed_approach = true;
                    }
                    else
                    {
                        aircraft.status = AircraftStatus.Flypass;
                        aircraft.pilot_properties.flight.other.flypass = true;
                    }
                }
                else
                {
                    aircraft.status = AircraftStatus.TouchGo;
                    aircraft.pilot_properties.flight.other.touch_go = true;
                }
                aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                aircraft.pilot_properties.flight.other.flypass_enabled = false;
                aircraft.pilot_properties.flight.other.landing_enabled = false;
                aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Finish Go Round
        public void FinishGoRound(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                aircraft.pilot_properties.flight.other.flypass_enabled = true;
                aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = true;
                aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Cannot Landing
        public void CannotLanding(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.pilot_properties.flight.other.landingRunway = "";
                aircraft.pilot_properties.flight.other.landing_enabled = true;
                aircraft.pilot_properties.flight.heading.is_enabled = true;
                aircraft.pilot_properties.flight.direct_go.directGo = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Start Landing
        public void StartLanding(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.status = AircraftStatus.Landing;
                aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                aircraft.pilot_properties.flight.other.flypass_enabled = false;
                aircraft.pilot_properties.flight.other.landing = true;
                aircraft.pilot_properties.flight.other.landing_enabled = false;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Touch Ground
        public void TouchGround(string callsign)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                ResetFlightStatus(aircraft);
                aircraft.is_departure = true;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                aircraft.pilot_properties.ground.status = GroundStatusInfo.Parking;
                aircraft.pilot_properties.status = PilotCommandMenu.Ground;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #region Finish Landing
        public void FinishLanding(string callsign, float heading,string from, string position)
        {
            AircraftData aircraft = FindAircraft(callsign);
            if (aircraft != null)
            {
                aircraft.heading = heading;
                aircraft.current_position = position;
                aircraft.from_position = from;
                aircraft.status = AircraftStatus.Parking;
                aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
            }
            else
            {
                Program.SendMessage("Cant Find Aircraft " + callsign);
            }
        }
        #endregion

        #endregion

        #region Flight Helper
        public AircraftData FindAircraft(string callsign)
        {
            AircraftData result = null;
            foreach (AircraftData a in aircrafts)
            {
                if (a.callsign.Equals(callsign))
                {
                    result = a;
                    break;
                }
            }
            return result;
        }
        private AircraftData ResetFlightStatus(AircraftData aircraft)
        {
            aircraft.pilot_properties.flight.circuit_approarch.instrument_approach = false;
            aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
            aircraft.pilot_properties.flight.direct_go.directGo = false;
            aircraft.pilot_properties.flight.downwind_orbit.downwind = false;
            aircraft.pilot_properties.flight.downwind_orbit.orbit = false;
            aircraft.pilot_properties.flight.holding.continue_direction = false;
            aircraft.pilot_properties.flight.holding.continue_route = false;
            aircraft.pilot_properties.flight.holding.holding_left = false;
            aircraft.pilot_properties.flight.holding.holding_right = false;
            aircraft.pilot_properties.flight.holding.holdingAt = false;
            aircraft.pilot_properties.flight.heading.absolute_heading = false;
            aircraft.pilot_properties.flight.heading.relative_heading = false;
            aircraft.pilot_properties.flight.other.abort_takeoff = false;
            aircraft.pilot_properties.flight.other.landing = false;
            aircraft.pilot_properties.flight.other.missed_approach = false;
            aircraft.pilot_properties.flight.other.flypass = false;
            aircraft.pilot_properties.flight.other.touch_go = false;
            aircraft.pilot_properties.flight.other.return_base = false;
            return aircraft;
        }
        #endregion

        #endregion
    }
}
