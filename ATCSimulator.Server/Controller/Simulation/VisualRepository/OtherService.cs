﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Contract.Simulation;
using ATCSimulator.Server.Services.Simulation;
using ATCSimulator.Server.Helper;
using Newtonsoft.Json;

namespace ATCSimulator.Server.Controller.Simulation.VisualRepository
{
    public class OtherService
    {
        #region Response
        public void ReadySimulation()
        {
            SimulationService.ReadySimulation();
            Program.SendMessage("Simulation is Begin");
        }

        public void LandingGearJamValue(string callsign, bool value)
        {
            SimulationService.LandingGearJamValue(callsign, value);
        }

        public void EngineFailureCallback(string callsign, int engine)
        {
            SimulationService.EngineFailureCallback(callsign, engine);
        }

        public void BirdAttackValue(string callsign, bool value)
        {
            SimulationService.BirdAttackValue(callsign,value);
        }

        public void AnimalCrossingEnable(string runway)
        {
            SimulationService.AnimalCrossingEnable(runway);
        }

        #endregion

        #region Send

        #region Other Command

        #region Create Simulation
        public static void CreateSimulation(ExerciseSimulationModel exercise, List<AircraftData> aircrafts, int simulation_id)
        {

            List<VisualAircraftData> visual_aircrafts = new List<VisualAircraftData>();
            foreach (AircraftData ac in aircrafts)
            {
                VisualAircraftData aircraft = new VisualAircraftData
                {
                    callsign = ac.callsign,
                    model = ac.properties.model,
                    livery = ac.properties.livery,
                    fuel = ac.fuel.TotalSeconds,
                    flight = ac.is_departure ? VisualFlight.Departure : VisualFlight.Arrivals,
                    position = ac.current_position,
                    flightRoutes = new VisualFlightRouteModel
                    {
                        flightRules = (FlightRuleInfo)Enum.Parse(typeof(FlightRuleInfo), ac.pilot_properties.flight.flight_route.flight_rules),
                        origin = ac.pilot_properties.flight.flight_route.origin_airport_name,
                        destination = ac.pilot_properties.flight.flight_route.destination_airport_name,
                        routes = ac.pilot_properties.flight.flight_route.routes_name,
                        runway = ac.pilot_properties.flight.runwayDefault
                    }
                };
                visual_aircrafts.Add(aircraft);
            };
            VisualSimulationData data = new VisualSimulationData
            {
                simulation_id = simulation_id,
                exercise = new VisualExerciseData
                {
                    scenery_name = exercise.scenery_name,
                    weather = new WeatherModel
                    {
                        weather = exercise.weather,
                        windDirection = exercise.wind_direction,
                        temperature = exercise.temperature,
                        visibility = exercise.visibility,
                        time = new TimeData
                        {
                            hour = exercise.time_simulation.Hours,
                            minute = exercise.time_simulation.Hours
                        }
                    },
                    runway_lights = exercise.runway_lights,
                    approach_lights = exercise.approach_lights,
                    taxi = exercise.taxi,
                    papi = exercise.papi
                },
                aircrafts = visual_aircrafts
            };

            string json = JsonConvert.SerializeObject(data);
            ISFSObject send = new SFSObject();
            send.PutUtfString("cmd", VisualServiceHelper.START_SIMULATION);
            send.PutUtfString("data", json);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, send);
        }
        #endregion

        #region Pause Simulation
        public static void PauseSimulation(bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PAUSE_SIMULATION);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Stop Simulation
        public static void StopSimulation()
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.STOP_SIMULATION);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Bird Attack
        public static void BirdAttack(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.BIRD_ATTACK);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Animal Crossing
        public static void AnimalCrossing(string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ANIMAL_CROSSING);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Taxi Light
        public static void TaxiLight( bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TAXI_LIGHT);
            data.PutBool("value",value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Papi Light
        public static void PapiLight( bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PAPI_LIGHT);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Runway Light
        public static void RunwayLight(string runway, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.RUNWAY_LIGHT);
            data.PutUtfString("runway", runway);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Approach Light
        public static void ApproachLight(string runway, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.APPROACH_LIGHT);
            data.PutUtfString("runway", runway);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Weather
        public static void Weather(int weather)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.WEATHER);
            data.PutInt("weather", weather);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Wind Direction
        public static void WindDirection(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.WIND_DIRECTION);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Wind Speed
        public static void WindSpeed(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.WIND_SPEED);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Temperature
        public static void Temperature(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TEMPERATURE);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Visibility
        public static void Visibility(float value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.VISIBILITY);
            data.PutFloat("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Time Simulation
        public static void TimeSimulation(int hour, int minute)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TIME_SIMULATION);
            data.PutInt("hour", hour);
            data.PutInt("minute", minute);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion


        #endregion

        #region Aircraft Command

        #region Select Aircraft
        public static void SelectAircraft(string callsign, string SendToTarget)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.SELECT_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("target", SendToTarget);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_TARGET, data);
            Program.SendMessage("Select aircraft callsign " + callsign + " target " + SendToTarget);
        }
        #endregion

        #region Remove Aircraft
        public static void RemoveAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.REMOVE_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Engine Failure
        public static void EngineFailure(string callsign, int engine)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ENGINE_FAILURE);
            data.PutUtfString("callsign", callsign);
            data.PutInt("engine", engine);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Change Fuel
        public static void ChangeFuel(string callsign, double fuel)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_FUEL);
            data.PutUtfString("callsign", callsign);
            data.PutDouble("fuel", fuel);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #endregion

        #endregion
    }
}
