﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Services.Simulation;
using ATCSimulator.Server.Helper;
using Newtonsoft.Json;

namespace ATCSimulator.Server.Controller.Simulation.VisualRepository
{
    public class GroundService
    {
        #region Response
        public void FinishReadyCallback(string callsign)
        {
            SimulationService.FinishReadyDepartureCallback(callsign);
        }

        public void FinishPushbackCallback(string callsign, float heading)
        {
            SimulationService.FinishPushbackCallback(callsign, heading);
        }
        public void TaxiPositionCallback(string callsign, string position, float heading)
        {
            SimulationService.UpdateTaxiPositionCallback(callsign, position, heading);
        }

        public void FinishTaxiCallback(string callsign, string position, float heading, int status)
        {
            SimulationService.FinishTaxiCallback(callsign, position, heading, status);
        }

        public void HoldTaxiCallback(string callsign, float heading)
        {
            SimulationService.HeadingAircraftCallback(callsign, heading);
        }

        public void RequestCrossRunwayCallback(string callsign, float heading)
        {
            SimulationService.RequestCrossRunwayCallback(callsign, heading);
        }
        public void FinishUTurnCallback(string callsign, float heading)
        {
            SimulationService.FinishUTurnCallback(callsign, heading);
        }

        public void FinishHoldTakeOffCallback(string callsign)
        {
            SimulationService.FinishHoldTakeOffCallback(callsign);
        }

        public void FinishTakeOffCallback(string callsign, float speed, float altitude)
        {
            SimulationService.FinishTakeOffCallback(callsign);
        }

        public void CrashAircraftCallback(string callsign)
        {
            SimulationService.CrashAircraftCallback(callsign);
        }
        public void FinishParkingCallback(string callsign, string appron)
        {
            SimulationService.FinishParkingCallback(callsign, appron);
        }
        public void BacktrackCallback(string callsign)
        {
            SimulationService.BacktrackCallback(callsign);
        }

        #endregion

        #region Send

        #region Ready Aircraft
        public static void ReadyAircraft(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.READY_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Engine Start
        public static void EngineStart(string callsign, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ENGINE_START);
            data.PutUtfString("callsign", callsign);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Pushback
        public static void Pushback(string callsign, DirectionInfo pushback)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PUSHBACK);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction", (int)pushback);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Start Taxi
        public static void StartTaxi(string callsign, List<string> routes, string target, int status)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.START_TAXI);
            data.PutUtfString("callsign", callsign);
            data.PutUtfStringArray("routes", routes.ToArray());
            data.PutUtfString("target", target);
            data.PutInt("status", status);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Taxi Speed
        public static void TaxiSpeed(string callsign, int speed)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TAXI_SPEED);
            data.PutUtfString("callsign", callsign);
            data.PutInt("speed", speed);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Hold Taxi
        public static void HoldTaxi(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLD_TAXI);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Continue Taxi
        public static void ContinueTaxi(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CONTINUE_TAXI);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Cross Runway
        public static void CrossRunway(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CROSS_RUNWAY);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Uturn
        public static void Uturn(string callsign, UturnInfo uturn)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.U_TURN);
            data.PutUtfString("callsign", callsign);
            data.PutInt("uturn", (int)uturn);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Rocking The Wing
        public static void GroundRockingWing(string callsign, bool value)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.G_ROCKING_WING);
            data.PutUtfString("callsign", callsign);
            data.PutBool("value", value);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }

        #endregion

        #region Hold Take Off
        public static void HoldTakeOff(string callsign, VisualFlightRouteModel routes, string runway)
        {
            string _routes = Newtonsoft.Json.JsonConvert.SerializeObject(routes);
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLD_TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("routes", _routes);
            data.PutUtfString("runway", runway);

            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Take Off
        public static void TakeOff(string callsign, VisualFlightRouteModel routes, string runway)
        {
            string _routes = JsonConvert.SerializeObject(routes);
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("routes", _routes);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region PKPPK
        public static void PKPPK(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.PKPPK);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }


        #endregion

        #endregion
    }
}
