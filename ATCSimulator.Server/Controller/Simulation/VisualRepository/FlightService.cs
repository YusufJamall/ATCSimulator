﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Services.Simulation;
using ATCSimulator.Server.Helper;
using Newtonsoft.Json;

namespace ATCSimulator.Server.Controller.Simulation.VisualRepository
{
    public class FlightService
    {
        #region Response
        public void CannotChangeSpeed(string callsign)
        {
        }
        public void CannotChangeAltitude(string callsign)
        {
        }
        public void TakeOffCallback(string callsign)
        {
        }
        public void UpdatePerformanceCallback(string callsign, float speed, float altitude)
        {
        }
        public void UpdateHoldingPoint(List<string> holding)
        {
            SimulationService.UpdateHoldingPoint(holding);
        }
        public void CannotHolding(string callsign)
        {
            SimulationService.CannotHolding(callsign);
        }
        public void HoldingAtCallback(string callsign,string position, bool isInstrument)
        {
            SimulationService.HoldingAtCallback(callsign,position,isInstrument);
        }
        public void RouteUpdateCallback(string callsign, string position)
        {
            SimulationService.RouteUpdateCallback(callsign, position);
        }
        public void ReadyJoinCircuit(string callsign)
        {
            SimulationService.ReadyJoinCircuit(callsign);
        }
        public void JoinCircuitCallback(string callsign,string runway)
        {
            SimulationService.JoinCircuitCallback(callsign,runway);
        }
        public void StartApproach(string callsign)
        {
            SimulationService.StartApproach(callsign);
        }
        public void StartGoRound(string callsign,bool isFlypass,bool isIfr)
        {
            SimulationService.StartGoRound(callsign, isFlypass,isIfr);
        }
        public void FinishGoRound(string callsign)
        {
            SimulationService.FinishGoRound(callsign);
        }
        public void CannotLanding(string callsign)
        {
            SimulationService.CannotLanding(callsign);
        }
        public void StartLanding(string callsign)
        {
            SimulationService.StartLanding(callsign);
        }
        public void TouchGround(string callsign)
        {
            SimulationService.TouchGround(callsign);
        }
        public void FinishLandingCallback(string callsign, float heading, string from, string position)
        {
            SimulationService.FinishLandingCallback(callsign, heading,from, position);
        }
        #endregion

        #region Send

        #region Active Aircraft
        public static void ActiveAircraft(string callsign, FlightRouteListModel routes)
        {
            string _routes = JsonConvert.SerializeObject(routes);
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ACTIVE_AIRCRAFT);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("routes", _routes);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Holding Now
        public static void HoldingNow(string callsign, int direction)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLDING_NOW);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction", direction);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Holding Position
        public static void HoldingPosition(string callsign,string position)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.HOLDING_POSITION);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("position", position);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Stop Holding
        public static void StopHolding(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.STOP_HOLDING);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Continue Route
        public static void ContinueRoute(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CONTINUE_ROUTE);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Continue Direction
        public static void ContinueDirection(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CONTINUE_DIRECTION);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Direct Go
        public static void DirectGo(string callsign, string position,bool isILS)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", isILS ? VisualServiceHelper.ILS_DIRECT_GO : VisualServiceHelper.DIRECT_GO);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("position", position);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Change Heading
        public static void ChangeHeading(string callsign, int direction,float heading)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_HEADING);
            data.PutUtfString("callsign", callsign);
            data.PutInt("direction", direction);
            data.PutFloat("heading", heading);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Change Route
        public static void ChangeRoute(string callsign, FlightRouteListModel routes)
        {
            // json convert routes
            string _routes = JsonConvert.SerializeObject(routes);
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_ROUTE);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("routes", _routes);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Altitude
        public static void Altitude(string callsign, float altitude)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_ALTITUDE);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("altitude", altitude);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Speed
        public static void Speed(string callsign, float speed)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.SPEED);
            data.PutUtfString("callsign", callsign);
            data.PutFloat("speed", speed);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Approach
        public static void Approach(string callsign, string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.APPROACH);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Circuit
        public static void Circuit(string callsign, string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CIRCUIT);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Missed Approach
        public static void MissedApproach(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.MISSED_APPROACH);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Touch Go
        public static void TouchGo(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.TOUCH_GO);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Flypass
        public static void Flypass(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.FLYPASS);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Flight Rocking Wing
        public static void FlightRockingWing(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.FLIGHT_ROCKING_WING);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Extend Downwind
        public static void ExtendDownwind(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.EXTEND_DOWNWIND);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Orbit
        public static void Orbit(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ORBIT);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Landing
        public static void Landing(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.LANDING);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region ILS Landing
        public static void ILSLanding(string callsign,string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ILS_LANDING);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Return Base
        public static void ReturnBase(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.RETURN_BASE);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Aborted Take Off
        public static void AbortedTakeOff(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.ABORTED_TAKE_OFF);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Change Runway
        public static void ChangeRunway(string callsign, string runway)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.CHANGE_RUNWAY);
            data.PutUtfString("callsign", callsign);
            data.PutUtfString("runway", runway);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #region Landing Gear Jam
        public static void LandingGearJam(string callsign)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("cmd", VisualServiceHelper.LANDING_GEAR_JAM);
            data.PutUtfString("callsign", callsign);
            VisualNetwork.instance.Send(VisualServiceHelper.SEND_TO_ALL_EXCEPT_ME, data);
        }
        #endregion

        #endregion
    }
}
