﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Data;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.Simulation
{
    public class VCCSRepository
    {
        #region VCCS Start Simulation
        public static ResponseData InsertDataStartSimulation(List<AircraftData> aircraft)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    //optioan code for refresh table
                    var temp = entity.tmpsimulasis.ToList();
                    if (temp.Count > 0)
                    {
                        foreach (var a in temp)
                        {
                            entity.tmpsimulasis.Attach(a);
                            entity.tmpsimulasis.Remove(a);
                            entity.SaveChanges();
                        }
                    }

                    foreach (var b in aircraft)
                    {
                        tmpsimulasi data = new tmpsimulasi
                        {
                            namapc = b.user.pc_name,
                            callsign = b.callsign,
                            posisipesawat = b.is_departure ? "0" : "1"
                        };
                        entity.tmpsimulasis.Add(data);
                        entity.SaveChanges();
                    }
                    result = ServiceResponse.OK;
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("Something Wrong when add vccs data");
            }
            return result;
        }
        #endregion
    }
}
