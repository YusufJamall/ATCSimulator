﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ATCSimulator.Server.Model.Simulation;
using ATCSimulator.Server.Contract.Simulation;
using System.IO;
using Newtonsoft.Json;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Data;

namespace ATCSimulator.Server.Controller.Simulation
{
    public class DatabaseSimulation
    {
        #region Create Data
        public static DataSimulationModel CreateData(int scenery_id)
        {
            DataSimulationModel result = new DataSimulationModel();
            result.approns = new List<DataAppronModel>();
            result.flight_route_list = new List<FlightRouteListModel>();
            using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
            {
                List<Base_Appron> approns = entity.Base_Appron.Where(d => d.SceneryID.Equals(scenery_id)).ToList();
                List<Waypoint_DepartureRoute> departure = entity.Waypoint_DepartureRoute.Where(d => d.SceneryID.Equals(scenery_id)).ToList();
                List<Waypoint_ArrivalRoute> arrival = entity.Waypoint_ArrivalRoute.Where(d => d.SceneryID.Equals(scenery_id)).ToList();

                foreach (Base_Appron appron in approns)
                {
                    DataAppronModel data = new DataAppronModel()
                    {
                        id = appron.ID,
                        appron_name = appron.AppronName,
                        can_pushback = appron.CanPushback,
                        scenery_id = appron.SceneryID,
                        terminal_name = appron.TerminalName
                    };
                    if (appron.PushbackLeft)
                    {
                        data.pushback_left = new DataPushbackModel()
                        {
                            alias = appron.PushbackLeftAlias,
                            name = appron.AppronName + "LEFT"
                        };
                    }
                    if (appron.PushbackRight)
                    {
                        data.pushback_right = new DataPushbackModel()
                        {
                            alias = appron.PushbackRightAlias,
                            name = appron.AppronName + "RIGHT"
                        };
                    }
                    result.approns.Add(data);
                }

                foreach (var item in departure)
                {
                    result.flight_route_list.Add(new FlightRouteListModel
                {
                    id = item.ID,
                    is_departure = true,
                    alternate_airport_code = item.AlternateAirportCode,
                    alternate_airport_id = item.AlternateAirportID,
                    alternate_airport_name = item.AlternateAirportName,
                    destination_airport_code = item.DestAirportCode,
                    destination_airport_id = item.DestAirportID,
                    destination_airport_name = item.DestAirportName,
                    origin_airport_code = item.OriginAirportCode,
                    origin_airport_id = item.OriginAirportID,
                    origin_airport_name = item.OriginAirportName,
                    flight_rules = item.FlightRules,
                    flight_rules_id = item.FlightRulesID,
                    routes_name = JsonConvert.DeserializeObject<List<string>>(item.RoutesName),
                    runways = JsonConvert.DeserializeObject<List<string>>(item.RunwayUsed)
                });
                }
                foreach (var item in arrival)
                {
                    result.flight_route_list.Add(new FlightRouteListModel
                    {
                        id = item.ID,
                        is_departure = false,
                        alternate_airport_code = item.AlternateAirportCode,
                        alternate_airport_id = item.AlternateAirportID,
                        alternate_airport_name = item.AlternateAirportName,
                        destination_airport_code = item.DestinationAirportCode,
                        destination_airport_id = item.DestinationAirportID,
                        destination_airport_name = item.DestinationAirportName,
                        origin_airport_code = item.OriginAirportCode,
                        origin_airport_id = item.OriginAirportID,
                        origin_airport_name = item.OriginAirportName,
                        flight_rules = item.FlightRules,
                        flight_rules_id = item.FlightRulesID,
                        start_point_name = item.StartPointName,
                        end_point_name = item.EndPointName,
                        routes_name = JsonConvert.DeserializeObject<List<string>>(item.RoutesName),
                        runways = JsonConvert.DeserializeObject<List<string>>(item.RunwayUsed)

                    });
                }
            }


            return result;
        }

        #endregion

        #region Create Aircraft Data
        public static List<AircraftData> CreateAircraftData(List<AircraftSimulationContract> _aircraft, DataSimulationModel data)
        {
            List<AircraftData> result = new List<AircraftData>();
            try
            {
                foreach (AircraftSimulationContract a in _aircraft)
                {
                    Model.System.AircraftDetailResponse aircraft_data = Controller.System.AircraftRepository.AircraftDetail(a.aircraft_id);
                    if (aircraft_data.status.code == ServiceResponse.OK.code)
                    {
                        AircraftData aircraft = new AircraftData()
                        {
                            callsign = a.callsign,
                            departure_time = a.departure_time,
                            estimate_time = a.estimate_time,
                            is_departure = a.is_departure,
                            engine_failure = 0,
                            fuel = a.fuel,
                            pkppk = false,
                            landing_gear_jam = false,
                            landing_gear_jam_enabled = false,
                            incident_bird = false,
                            crashed = false,
                            removed = false,
                            animal_crossing_enabled = true,
                            altitude = HelperMethod.GetAltitude(a.flight_level_type,a.flight_level),
                            speed = (float)aircraft_data.aircraft.true_speed,
                            can_change_altitude = true,
                            can_change_speed = true,
                            properties = new AircraftProperties()
                            {
                                category = aircraft_data.aircraft.category,
                                engine = aircraft_data.aircraft.engine,
                                flight_level = a.flight_level,
                                flight_level_type = a.flight_level_type,
                                livery = a.livery_name,
                                model = a.aircraft_model,
                                person = a.person_on_board,
                                type = aircraft_data.aircraft.type,
                                flight_type = a.flight_type,
                                livery_strips = a.livery_strips,
                                true_speed = aircraft_data.aircraft.true_speed,
                                maximum_altitude = aircraft_data.aircraft.maximum_altitude                                
                            },
                            user = new UserPilotModel()
                            {
                                address = a.pilot.address,
                                first_name = a.pilot.first_name,
                                home_base = "Atang Sanjaya",
                                last_name = a.pilot.last_name,
                                phone = a.pilot.phone,
                                pc_role = a.pilot.pc_role,
                                pc_name = a.pilot.pc_name,
                                ip_address = a.pilot.ip_address,
                                mac_address = a.pilot.mac_address
                            }
                        };

                        if (a.is_departure)
                        {
                            aircraft.incident_bird_enable = false;
                            DataAppronModel appron_data = data.approns.Where(d => d.id.Equals(a.parking_id.Value)).FirstOrDefault();
                            FlightRouteListModel flight_route_data = data.flight_route_list.Where(d => d.id.Equals(a.route_id) && d.is_departure).FirstOrDefault();
                            aircraft.current_position = appron_data.appron_name;
                            aircraft.from_position = appron_data.appron_name;
                            aircraft.is_enabled = true;
                            aircraft.start_time = a.departure_time;
                            aircraft.status = AircraftStatus.NONE;
                            aircraft.heading = 0;
                            aircraft.pilot_properties = new PilotCommandProperties()
                            {
                                ground = new PilotGroundCommand()
                                {
                                    status = GroundStatusInfo.Departure,
                                    engine = false,
                                    engine_enabled = true,
                                    pushback = new PilotPushbackCommand()
                                    {
                                        status = CommandStatus.None,
                                        is_show = appron_data.can_pushback,
                                        pushback_left_enabled = appron_data.pushback_left != null ? true : false,
                                        pushback_right_enabled = appron_data.pushback_right != null ? true : false,
                                        pushback_left = false,
                                        pushback_right = false,
                                        pushback_left_alias = appron_data.pushback_left != null ? appron_data.pushback_left.alias : "-",
                                        pushback_right_alias = appron_data.pushback_right != null ? appron_data.pushback_right.alias : "-"
                                    },
                                    runway = new PilotRunwayCommand()
                                    {
                                        is_show = true,
                                        runway = flight_route_data.runways.FirstOrDefault(),
                                    },
                                    taxi = new PilotTaxiCommand()
                                    {
                                        is_show = !appron_data.can_pushback,
                                        cross_runway = false,
                                        routes = new List<string>(),
                                        speed = GroundSpeed.Normal,
                                        status = TaxiStatus.None,
                                        taxi_route = false,
                                        taxi_route_enabled = true
                                    },
                                    other = new PilotOtherCommand()
                                    {
                                        rocking_wing = false,
                                        u_turn = false,
                                        u_turn_status = UturnInfo.None
                                    },
                                    intersection = new PilotIntersectionCommand()
                                    {
                                        is_show = false,
                                        runway_hold = false,
                                        status = CommandStatus.None,
                                        take_off = false
                                    },
                                    parking = new PilotParkingInfo
                                    {
                                        parking_id = appron_data.id,
                                        terminal_name = appron_data.terminal_name,
                                        appron_name = appron_data.appron_name
                                    }
                                },
                                ready_departure = new PilotReadyCommand()
                                {
                                    is_enabled = true,
                                },
                                flight = new PilotFlightCommand()
                                {
                                    ILSmode = false,
                                    runwayDefault = a.runway,
                                    flight_route = flight_route_data,
                                    can_changeRoute = false,
                                    change_runway = new PilotChangeRunwayCommand
                                    {
                                        is_enabled = false,
                                        runway = a.runway
                                    },
                                    circuit_approarch = new PilotCircuitApproachCommand
                                    {
                                        instrument_approach = false,
                                        instrument_approach_enabled = false,
                                        join_circuit = false,
                                        join_circuit_enabled = false,
                                        runway = a.runway
                                    },
                                    direct_go = new PilotDirectGoCommand
                                    {
                                        is_enabled = false,
                                        directGo = false,
                                        position = ""
                                    },
                                    downwind_orbit = new PilotDownwindOrbitCommand
                                    {
                                        downwind = false,
                                        downwind_enabled = false,
                                        orbit = false,
                                        orbit_enabled = false
                                    },
                                    heading = new PilotHeadingCommand
                                    {
                                        is_enabled = false,
                                        absolute_heading = false,
                                        absolute_heading_value = 0,
                                        relative_heading = false,
                                        relative_heading_value = 0
                                    },
                                    holding = new PilotHoldingCommand
                                    {
                                        continue_direction = false,
                                        continue_direction_enabled = false,
                                        continue_route = false,
                                        continue_route_enabled = false,
                                        holding_left = false,
                                        holding_left_enabled = false,
                                        holding_right = false,
                                        holding_right_enabled = false,
                                        holdingAt = false,
                                        holdingAt_enabled = false,
                                        position_holding = ""
                                    },
                                    other = new PilotFlightOtherCommand
                                    {
                                        abort_takeoff = false,
                                        abort_takeoff_enabled = true,
                                        flight_rocking_wing = false,
                                        flight_rocking_wing_enabled = false,
                                        flypass = false,
                                        flypass_enabled = false,
                                        landing = false,
                                        landing_enabled = false,
                                        missed_approach = false,
                                        missed_approach_enabled = false,
                                        return_base = false,
                                        return_base_enabled = false,
                                        touch_go = false,
                                        touch_go_enabled = false,
                                        return_base_isVisible= true,
                                        abort_takeoff_isVisible = true,
                                    }
                                },
                                status = PilotCommandMenu.DepartureReady
                            };
                        }
                        else
                        {
                            aircraft.incident_bird = true;
                            aircraft.incident_bird_enable = false;
                            FlightRouteListModel flight_route_data = data.flight_route_list.Where(d => d.id.Equals(a.route_id) && !d.is_departure).FirstOrDefault();
                            aircraft.current_position = data.flight_route_list.Where(d => d.id.Equals(a.route_id) && !d.is_departure).Select(d => d.start_point_name).FirstOrDefault();
                            aircraft.from_position = data.flight_route_list.Where(d => d.id.Equals(a.route_id) && !d.is_departure).Select(d => d.start_point_name).FirstOrDefault();
                            aircraft.is_enabled = true;
                            aircraft.start_time = a.start_time.HasValue ? a.start_time.Value : new TimeSpan();
                            aircraft.status = AircraftStatus.NONE;
                            aircraft.heading = 0;
                            aircraft.pilot_properties = new PilotCommandProperties()
                            {
                                ground = new PilotGroundCommand()
                                {
                                    status = GroundStatusInfo.Departure,
                                    engine = true,
                                    engine_enabled = true,
                                    pushback = new PilotPushbackCommand()
                                    {
                                        status = CommandStatus.None,
                                        is_show = false,
                                        pushback_left_enabled = false,
                                        pushback_right_enabled = false,
                                        pushback_left = false,
                                        pushback_right = false,
                                        pushback_left_alias = "-",
                                        pushback_right_alias = "-"
                                    },
                                    runway = new PilotRunwayCommand()
                                    {
                                        is_show = true,
                                        runway = flight_route_data.runways.FirstOrDefault(),
                                    },
                                    taxi = new PilotTaxiCommand()
                                    {
                                        is_show = true,
                                        cross_runway = false,
                                        routes = new List<string>(),
                                        speed = GroundSpeed.Expedite,
                                        status = TaxiStatus.None,
                                        taxi_route = false,
                                        taxi_route_enabled = true
                                    },
                                    other = new PilotOtherCommand()
                                    {
                                        rocking_wing = false,
                                        u_turn = false,
                                        u_turn_status = UturnInfo.None
                                    },
                                    intersection = new PilotIntersectionCommand()
                                    {
                                        is_show = false,
                                        runway_hold = false,
                                        status = CommandStatus.None,
                                        take_off = false
                                    },
                                    parking = new PilotParkingInfo
                                    {
                                        
                                    }
                                },
                                ready_flight = new PilotReadyCommand
                                {
                                    is_enabled = true
                                },
                                ready_departure = new PilotReadyCommand()
                                {
                                    is_enabled = false,
                                },
                                flight = new PilotFlightCommand()
                                {
                                    ILSmode = false,
                                    runwayDefault = a.runway,
                                    flight_route = flight_route_data,
                                    can_changeRoute = true,
                                    change_runway = new PilotChangeRunwayCommand
                                    {
                                        is_enabled = false,
                                        runway = a.runway
                                    },
                                    circuit_approarch = new PilotCircuitApproachCommand
                                    {
                                        instrument_approach = false,
                                        instrument_approach_enabled = false,
                                        join_circuit = false,
                                        join_circuit_enabled = false,
                                        runway = a.runway
                                    },
                                    direct_go = new PilotDirectGoCommand
                                    {
                                        is_enabled = true,
                                        directGo = false,
                                        position = ""
                                    },
                                    downwind_orbit = new PilotDownwindOrbitCommand
                                    {
                                        downwind = false,
                                        downwind_enabled = false,
                                        orbit = false,
                                        orbit_enabled = false
                                    },
                                    heading = new PilotHeadingCommand
                                    {
                                        is_enabled = true,
                                        absolute_heading = false,
                                        absolute_heading_value = 0,
                                        relative_heading = false,
                                        relative_heading_value = 0
                                    },
                                    holding = new PilotHoldingCommand
                                    {
                                        continue_direction = false,
                                        continue_direction_enabled = false,
                                        continue_route = false,
                                        continue_route_enabled = false,
                                        holding_left = false,
                                        holding_left_enabled = true,
                                        holding_right = false,
                                        holding_right_enabled = true,
                                        holdingAt = false,
                                        holdingAt_enabled = true,
                                        position_holding = ""
                                    },
                                    other = new PilotFlightOtherCommand
                                    {
                                        abort_takeoff = false,
                                        abort_takeoff_enabled = false,
                                        flight_rocking_wing = false,
                                        flight_rocking_wing_enabled = true,
                                        flypass = false,
                                        flypass_enabled = false,
                                        landing = false,
                                        landing_enabled = false,
                                        missed_approach = false,
                                        missed_approach_enabled = false,
                                        return_base = false,
                                        return_base_enabled = false,
                                        touch_go = false,
                                        touch_go_enabled = false,
                                        return_base_isVisible = false,
                                        abort_takeoff_isVisible = false,
                                    }
                                },
                                status = PilotCommandMenu.FlightReady
                            };
                        }
                        result.Add(aircraft);
                    }
                }
            }
            catch (Exception ex)
            {
                Program.SendMessage(ex.Message);
            }
            return result;
        }

        #endregion

        #region Create Simulation Data
        public static SimulationResponse CreateSimulationData(PlaySimulationContract data)
        {
            SimulationResponse result = new SimulationResponse();
            result.status = ServiceResponse.BadRequest;
            if (data != null)
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    try
                    {
                        Base_Simulation simulation = new Base_Simulation
                        {
                            ExerciseName = data.exercise.exercise_name,
                            SceneryID = data.exercise.scenery_id,
                            SceneryName = data.exercise.scenery_name,
                            PlayTime = data.exercise.play_time,
                            Weather = data.exercise.weather.ToString(),
                            WindDirection = data.exercise.wind_direction,
                            WindSpeed = data.exercise.wind_speed,
                            Temperature = data.exercise.temperature,
                            Visibility = data.exercise.visibility,
                            TimeSimulation = data.exercise.time_simulation,
                            RunwayLights = JsonConvert.SerializeObject(data.exercise.runway_lights),
                            Taxi = data.exercise.taxi,
                            Papi = data.exercise.papi,
                            Approach = JsonConvert.SerializeObject(data.exercise.approach_lights),
                            TotalArrival = data.pilots.Where(d => !d.is_departure).Count(),
                            TotalDeparture = data.pilots.Where(d => d.is_departure).Count(),
                            CreatedDate = DateTime.Now,
                            CreatedBy = data.InstructorName,
                        };
                        entity.Base_Simulation.Add(simulation);
                        entity.SaveChanges();

                        //save player simulation
                        foreach (var _player in data.players)
                        {
                            Base_SimulationPlayer player = new Base_SimulationPlayer
                            {
                                SimulationID = simulation.ID,
                                RolePlayer = _player.role,
                                UserID = _player.ID,
                                UserName = _player.name
                            };
                            entity.Base_SimulationPlayer.Add(player);
                            entity.SaveChanges();
                        }
                        // save aircraft
                        foreach (var aircraft in data.pilots)
                        {
                            if (aircraft.is_departure)
                            {
                                Base_SimulationDeparture departure = new Base_SimulationDeparture
                                {
                                    SceneryID = simulation.SceneryID,
                                    SimulationID = simulation.ID,
                                    AircraftID = aircraft.aircraft_id,
                                    AircraftLiveryID = aircraft.livery_id,
                                    RouteID = aircraft.route_id,
                                    FlightRulesID = aircraft.flight_rules_id,
                                    ParkingID = aircraft.parking_id.Value,
                                    FlightTypeID = aircraft.flight_type_id,
                                    FlightLevelType = aircraft.flight_level_type,
                                    FlightLevel = aircraft.flight_level,
                                    Callsign = aircraft.callsign,
                                    PersonOnBoard = aircraft.person_on_board,
                                    DepartureTime = aircraft.departure_time,
                                    EstimateTime = aircraft.estimate_time,
                                    Fuel = aircraft.fuel,
                                    PlayerID = entity.Base_SimulationPlayer.Where(d => d.UserID.Equals(aircraft.pilot.user_id) && d.SimulationID.Equals(simulation.ID)).Select(d => d.ID).FirstOrDefault()
                                };
                                entity.Base_SimulationDeparture.Add(departure);
                            }
                            else
                            {
                                Base_SimulationArrival arrival = new Base_SimulationArrival
                                {
                                    SceneryID = simulation.SceneryID,
                                    SimulationID = simulation.ID,
                                    AircraftID = aircraft.aircraft_id,
                                    AircraftLiveryID = aircraft.livery_id,
                                    RouteID = aircraft.route_id,
                                    FlightRulesID = aircraft.flight_rules_id,
                                    FlightTypeID = aircraft.flight_type_id,
                                    FlightLevelType = aircraft.flight_level_type,
                                    FlightLevel = aircraft.flight_level,
                                    Callsign = aircraft.callsign,
                                    PersonOnBoard = aircraft.person_on_board,
                                    StartPointTime = aircraft.start_time.Value,
                                    DepartureTime = aircraft.departure_time,
                                    EstimateTime = aircraft.estimate_time,
                                    Fuel = aircraft.fuel,
                                    PlayerID = entity.Base_SimulationPlayer.Where(d => d.UserID.Equals(aircraft.pilot.user_id) && d.SimulationID.Equals(simulation.ID)).Select(d => d.ID).FirstOrDefault()
                                };
                                entity.Base_SimulationArrival.Add(arrival);
                            }
                            entity.SaveChanges();
                            result.simulationID = simulation.ID;
                        }
                        result.status = ServiceResponse.OK;
                    }
                    catch (Exception ex)
                    {
                        result.status = ServiceResponse.InternalServerError;
                        Program.SendMessage("error while create simulation data, error : " + ex.Message);
                    }
                }
            }
            return result;
        }
        #endregion

        #region End Simulation Update Data

        public static StatusResponse EndSimulation(int simulationID, TimeSpan elapsedTime)
        {
            StatusResponse result = new StatusResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Simulation simulation = entity.Base_Simulation.Where(d => d.ID.Equals(simulationID)).FirstOrDefault();
                    if (simulation != null)
                    {
                        entity.Base_Simulation.Attach(simulation);
                        simulation.ElapsedTime = elapsedTime;
                        entity.SaveChanges();

                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.BadRequest;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                Program.SendMessage("Simulation Failed to Stop with error : " + ex.Message);
            }

            return result;
        }

        #endregion

    }
}
