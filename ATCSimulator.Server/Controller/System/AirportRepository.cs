﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Data;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Model.System;

namespace ATCSimulator.Server.Controller.System
{
    public class AirportRepository
    {
        #region Airport List
        public static AirportListResponse AirportList(int scenery_id)
        {
            AirportListResponse result = new AirportListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.airports = entity.Base_Airport.Where(d => d.SceneryID.Equals(scenery_id)).Select(d => new AirportDetailModel()
                    {
                        id = d.ID,
                        scenery_id = d.SceneryID,
                        airport_code = d.AirportCode,
                        airport_name = d.Name
                    }).ToList();
                    if (result.airports != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get list Airport with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Airport Appron List
        public static AirportAppronListResponse AirportAppronList(int scenery_id)
        {
            AirportAppronListResponse result = new AirportAppronListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.approns = entity.Base_Appron.Where(d=>d.SceneryID.Equals(scenery_id)).Select(d => new AirportAppronModel()
                    {
                        appron_name = d.AppronName,
                        id = d.ID,
                        scenery_id = d.SceneryID,
                        terminal_name = d.TerminalName
                    }).ToList();
                    if (result.approns != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get list Airport Appron with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Runway List
        public static RunwayListResponse RunwayList(int scenery_id)
        {
            RunwayListResponse result = new RunwayListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.runways = entity.Base_Runway.Where(d => d.SceneryID.Equals(scenery_id)).Select(d => new RunwayDetailModel()
                    {
                        id = d.ID,
                        scenery_id = d.SceneryID,
                        runway_name = d.RunwayName,
                        is_helicopter = d.IsHelicopter
                    }).ToList();
                    if (result.runways != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get list Airport with error : " + ex.Message);
            }
            return result;
        }
        #endregion
    }
}
