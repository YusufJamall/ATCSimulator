﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Helper;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Data;

namespace ATCSimulator.Server.Controller.System
{
    public class PCRepository
    {
        #region Connect PC
        public static ResponseData ConnectPC(PCContract user)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_ListPC pc = entity.Prefix_ListPC.Where(d => d.RolePC.Equals(user.role_pc)).FirstOrDefault();
                    if (pc != null)
                    {
                        if (pc.LoggedUser != null)
                        {
                            Prefix_UserProfile loggedUser = entity.Prefix_UserProfile.Where(d => d.ID.Equals(pc.LoggedUser.Value)).FirstOrDefault();
                            entity.Prefix_UserProfile.Attach(loggedUser);
                            loggedUser.isOnline = false;
                        }
                        // update current pc info //
                        pc.IPAddress = user.ip_address;
                        pc.PCName = user.pc_name;
                        pc.MacAddress = user.mac_address;
                        pc.IsOnline = false;
                        pc.IsActive = true;
                        pc.LoggedUser = null;
                        pc.RolePC = user.role_pc;
                        entity.SaveChanges();
                        result = ServiceResponse.OK;
                        Program.SendMessage("PC " + user.pc_name + " : " + user.ip_address + " is Active and Connect to Server as " + user.role_pc + " at " + DateTime.Now);
                    }
                    else
                    {
                        result = ServiceResponse.BadRequest;
                        result.description = "Role not found";
                        Program.SendMessage("PC " + user.pc_name + " could not connect");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("PC " + user.pc_name + " could not connect with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Detail PC
        public static DetailPCResponse DetailPC(int pc_id)
        {
            DetailPCResponse result = new DetailPCResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_ListPC pc = entity.Prefix_ListPC.Where(d => d.ID.Equals(pc_id)).FirstOrDefault();
                    if (pc != null)
                    {
                        result.pc = new DetailPCModel()
                        {
                            id = pc.ID,
                            ip_address = pc.IPAddress,
                            is_online = pc.IsOnline,
                            is_active = pc.IsActive,
                            logged_user = pc.LoggedUser,
                            mac_address = pc.MacAddress,
                            pc_name = pc.PCName,
                            role_pc = pc.RolePC
                        };
                        if (!string.IsNullOrEmpty(pc.RoleUserAccess))
                        {
                            result.pc.role_user_access = new List<int>();
                            List<string> _access = pc.RoleUserAccess.Split(';').ToList();
                            foreach (string s in _access)
                            {
                                result.pc.role_user_access.Add(int.Parse(s));
                            }
                        }
                        result.status = ServiceResponse.OK;
                        Program.SendMessage("Get Detail PC : " + pc_id);
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                        result.status.description = "Cannot found PC with pc id" + pc_id;
                        Program.SendMessage("Cannot found PC with pc id" + pc_id);
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("PC " + pc_id + " cant get detail with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region List PC
        public static ListPCResponse ListPC()
        {
            ListPCResponse result = new ListPCResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<Prefix_ListPC> list_pc = entity.Prefix_ListPC.ToList();
                    result.pc = new List<DetailPCModel>();
                    foreach (Prefix_ListPC p in list_pc)
                    {
                        DetailPCModel pc = new DetailPCModel()
                        {
                            id = p.ID,
                            ip_address = p.IPAddress,
                            is_online = p.IsOnline,
                            is_active = p.IsActive,
                            logged_user = p.LoggedUser,
                            mac_address = p.MacAddress,
                            pc_name = p.PCName,
                            role_pc = p.RolePC,
                            group = p.Group.HasValue ? p.Group.Value : 0
                        };
                        if (!string.IsNullOrEmpty(p.RoleUserAccess))
                        {
                            pc.role_user_access = new List<int>();
                            List<string> _access = p.RoleUserAccess.Split(';').ToList();
                            foreach (string s in _access)
                            {
                                pc.role_user_access.Add(int.Parse(s));
                            }
                        }
                        result.pc.Add(pc);
                    }
                    result.count = entity.Prefix_ListPC.Count();
                    result.status = ServiceResponse.OK;
                    Program.SendMessage("Get list pc with total : " + result.count);
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage(" cant get list pc with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Disconnect PC
        public static ResponseData DisconnectPC(string role_pc)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_ListPC pc = entity.Prefix_ListPC.Where(d => d.RolePC.ToLower().Equals(role_pc.ToLower())).FirstOrDefault();
                    if (pc != null)
                    {
                        Prefix_UserProfile _profile = entity.Prefix_UserProfile.Where(d => d.UserID.Equals(pc.LoggedUser)).FirstOrDefault();
                        if (_profile != null)
                        {
                            entity.Prefix_UserProfile.Attach(_profile);
                            _profile.isOnline = false;
                        }
                        // update current pc info //
                        entity.Prefix_ListPC.Attach(pc);
                        pc.IsOnline = false;
                        pc.IsActive = false;
                        pc.LoggedUser = null;
                        entity.SaveChanges();
                        result = ServiceResponse.OK;
                        Program.SendMessage("PC " + role_pc + " is disconnect from server");
                    }
                    else
                    {
                        result = ServiceResponse.NotFound;
                        Program.SendMessage("PC " + role_pc + " is can't disconnect from server because not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("PC " + role_pc + " is can't disconnect from server with error: " + ex.Message);
            }
            return result;
        }
        #endregion
    }
}
