﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Data;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.System
{
    public class UserRepository
    {
        #region User Login
        public static UserLoginResponse UserLogin(UserLoginContract user)
        {
            UserLoginResponse result = new UserLoginResponse();
            try
            {
                string _passwordMD5 = HelperMethod.encrypt(user.password);
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_User _user = entity.Prefix_User.Where(d => d.Username.Equals(user.username) && d.Password.Equals(_passwordMD5)).FirstOrDefault();
                    if (_user != null)
                    {
                        Prefix_ListPC using_pc = entity.Prefix_ListPC.Where(d => d.IPAddress.Equals(user.ip_address)).FirstOrDefault();
                        if (using_pc != null)
                        {
                            //check if user can access this pc
                            bool can_access = false;
                            List<string> user_access = using_pc.RoleUserAccess.Split(';').ToList();
                            foreach (string r in user_access)
                            {
                                if (_user.RoleID.ToString().Equals(r))
                                {
                                    can_access = true;
                                    break;
                                }
                            }
                            if (can_access)
                            {
                                Prefix_UserProfile profile = _user.Prefix_UserProfile.FirstOrDefault();
                                UserProfileModel user_profile = new UserProfileModel()
                                {
                                    ID = profile.ID,
                                    user_ID = profile.UserID,
                                    address = profile.Address,
                                    birthdate = profile.Birthdate.HasValue ? profile.Birthdate.Value : new DateTime(),
                                    email = profile.Email,
                                    first_name = profile.Firstname,
                                    last_name = profile.Lastname,
                                    gender = profile.Gender,
                                    phone = profile.Phone,
                                    photo = profile.Photo,
                                    role = new UserRoleModel()
                                    {
                                        role_id = _user.Prefix_UserRole.ID,
                                        role_name = _user.Prefix_UserRole.RoleName,
                                        constant_variable = _user.Prefix_UserRole.Constant
                                    },
                                    create_by = profile.CreateBy,
                                    create_date = profile.CreateDate,
                                    isStudent = profile.IsStudent,
                                    update_by = profile.UpdateBy,
                                    update_date = profile.UpdateDate,
                                    user_identity_id = profile.UserIdentity,
                                    username = _user.Username
                                };
                                if (profile.IsStudent)
                                {
                                    user_profile.student_info = new StudentInfoModel()
                                    {
                                        @class = profile.Class,
                                        class_year = profile.ClassYear.HasValue ? profile.ClassYear.Value : 0,
                                        score = profile.Score.HasValue ? profile.Score.Value : 0
                                    };
                                }
                                _user.LastLogin = DateTime.Now;
                                profile.isOnline = true;
                                using_pc.IsOnline = true;
                                using_pc.LoggedUser = _user.ID;
                                entity.SaveChanges();
                                result.status = ServiceResponse.OK;
                                Program.SendMessage("User " + user_profile.username + " Is Login and Connected at " + DateTime.Now);
                                result.user = user_profile;
                            }
                            else
                            {
                                result.status = ServiceResponse.Unauthorized;
                                Program.SendMessage("User " + user.username + " Cant Login Because his/her role cannot access pc");
                            }
                        }
                        else
                        {
                            result.status = ServiceResponse.NotFound;
                            Program.SendMessage("User " + user.username + " Cant Login Because his/her Pc is Unidentified");
                        }
                    }
                    else
                    {
                        result.status = ServiceResponse.BadRequest;
                        result.status.message = "Username and Password didn't match";
                        Program.SendMessage("User " + user.username + " Cant Login Username or Password Did not Match");
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("User " + user.username + " could not Login with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region User Remove
        public static ResponseData UserRemove(int user_id)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_User _user = entity.Prefix_User.Where(d => d.ID.Equals(user_id)).FirstOrDefault();
                    if (_user != null)
                    {
                        entity.Prefix_User.Remove(_user);
                        entity.SaveChanges();
                        result = ServiceResponse.Deleted;
                        Program.SendMessage("User " + user_id + " Success Removed");
                    }
                    else
                    {
                        result = ServiceResponse.NotFound;
                        Program.SendMessage("User " + user_id + " Can't Remove Because user not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("User ID " + user_id + " could not Remove with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region User Register
        public static ResponseData UserRegister(UserRegisterContract user)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    string username = string.Empty;
                    var checkuser = entity.Prefix_User.Where(d => d.Username.ToLower().Equals(user.username.ToLower())).FirstOrDefault();
                    if (checkuser != null)
                    {
                        result = ServiceResponse.ServiceUnavailable;
                        result.description = "Username " + user.username + " has exist please create other username";
                        Program.SendMessage("User " + user.username + " Cannot Register because username is exist");
                        return result;
                    }
                    else
                    {
                        Prefix_User _user = new Prefix_User()
                        {
                            Password = HelperMethod.encrypt(user.password),
                            RoleID = user.role_id,
                            Username = user.username,
                        };
                        entity.Prefix_User.Add(_user);
                        entity.SaveChanges();

                        Prefix_UserProfile _profile = new Prefix_UserProfile()
                        {
                            Address = user.profile.address,
                            Birthdate = user.profile.birthdate,
                            CreateBy = user.profile.create_by,
                            CreateDate = DateTime.Now,
                            Email = user.profile.email,
                            Firstname = user.profile.first_name,
                            Gender = user.profile.gender,
                            IsStudent = user.profile.isStudent,
                            Lastname = user.profile.last_name,
                            Phone = user.profile.phone,
                            Photo = user.profile.photo,
                            Rolename = user.role_name,
                            UpdateBy = user.profile.create_by,
                            UpdateDate = DateTime.Now,
                            UserIdentity = user.profile.user_identity_id,
                            isOnline = false,
                            Username = user.username,
                            UserID = _user.ID
                        };
                        if (user.profile.isStudent)
                        {
                            _profile.Class = user.profile.student_info.@class;
                            _profile.ClassYear = user.profile.student_info.class_year;
                            _profile.Score = 0;
                        }
                        entity.Prefix_UserProfile.Add(_profile);
                        entity.SaveChanges();
                        result = ServiceResponse.Created;
                        result.description = "Username " + user.username + " success registered";
                        Program.SendMessage("User " + user.username + " Success Register");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = "Username " + user.username + " Fail registered with error : " + ex.Message;
                Program.SendMessage("User " + user.username + " Fail registered with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region User Update
        public static ResponseData UserUpdate(UserUpdateContract user)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_User _user = entity.Prefix_User.Where(d => d.ID.Equals(user.user_id)).FirstOrDefault();

                    if (_user != null)
                    {
                        _user.RoleID = user.role_id;
                        Prefix_UserProfile _profile = _user.Prefix_UserProfile.FirstOrDefault();
                        _profile.Address = user.profile.address;
                        _profile.Birthdate = user.profile.birthdate;
                        _profile.CreateBy = user.profile.create_by;
                        _profile.CreateDate = DateTime.Now;
                        _profile.Email = user.profile.email;
                        _profile.Firstname = user.profile.first_name;
                        _profile.Gender = user.profile.gender;
                        _profile.IsStudent = user.profile.isStudent;
                        _profile.Lastname = user.profile.last_name;
                        _profile.Phone = user.profile.phone;
                        _profile.Photo = user.profile.photo != null ? user.profile.photo : _profile.Photo;
                        _profile.Rolename = user.role_name;
                        _profile.UpdateBy = user.profile.create_by;
                        _profile.UpdateDate = DateTime.Now;
                        _profile.UserIdentity = user.profile.user_identity_id;
                        _profile.isOnline = false;
                        if (user.profile.isStudent)
                        {
                            _profile.Class = user.profile.student_info.@class;
                            _profile.ClassYear = user.profile.student_info.class_year;
                        }
                        entity.SaveChanges();
                        result = ServiceResponse.Updated;
                        result.description = "Username " + user.username + " success update";
                        Program.SendMessage("User " + user.username + " Success update");
                    }
                    else
                    {
                        result = ServiceResponse.NotFound;
                        result.description = "Username " + user.username + " not found";
                        Program.SendMessage("User " + user.username + " not found");
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = "Username " + user.username + " can't update with error : " + ex.Message;
                Program.SendMessage("User " + user.username + " can't update with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region User List
        public static UserListResponse UserList(int page, int pageSize, int type)
        {
            UserListResponse result = new UserListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    if (type == 0)
                    {
                        result.users = entity.Prefix_UserProfile.OrderBy(x => x.ID).Skip((page - 1) * pageSize).Take(pageSize)
                            .Select(d => new UserProfileModel()
                            {
                                address = d.Address,
                                birthdate = d.Birthdate,
                                create_by = d.CreateBy,
                                create_date = d.CreateDate,
                                email = d.Email,
                                first_name = d.Firstname,
                                gender = d.Gender,
                                ID = d.ID,
                                isStudent = d.IsStudent,
                                last_name = d.Lastname,
                                phone = d.Phone,
                                photo = d.Photo,
                                role = new UserRoleModel()
                                {
                                    constant_variable = d.Prefix_User.Prefix_UserRole.Constant,
                                    role_id = d.Prefix_User.RoleID,
                                    role_name = d.Rolename
                                },
                                student_info = new StudentInfoModel()
                                {
                                    @class = d.Class,
                                    class_year = d.ClassYear,
                                    score = d.Score
                                },
                                update_by = d.UpdateBy,
                                update_date = d.UpdateDate,
                                user_ID = d.UserID,
                                user_identity_id = d.UserIdentity,
                                username = d.Username
                            }).ToList();
                        result.count = entity.Prefix_UserProfile.Count();
                        result.status = ServiceResponse.OK;
                        Program.SendMessage("Get list user with total : " + result.count);
                    }
                    else
                    {
                        result.users = entity.Prefix_UserProfile.Where(x => x.Prefix_User.RoleID.Equals(type)).OrderBy(x => x.ID).Skip((page - 1) * pageSize).Take(pageSize)
                            .Select(d => new UserProfileModel()
                            {
                                address = d.Address,
                                birthdate = d.Birthdate,
                                create_by = d.CreateBy,
                                create_date = d.CreateDate,
                                email = d.Email,
                                first_name = d.Firstname,
                                gender = d.Gender,
                                ID = d.ID,
                                isStudent = d.IsStudent,
                                last_name = d.Lastname,
                                phone = d.Phone,
                                photo = d.Photo,
                                role = new UserRoleModel()
                                {
                                    constant_variable = d.Prefix_User.Prefix_UserRole.Constant,
                                    role_id = d.Prefix_User.RoleID,
                                    role_name = d.Rolename
                                },
                                student_info = new StudentInfoModel()
                                {
                                    @class = d.Class,
                                    class_year = d.ClassYear,
                                    score = d.Score
                                },
                                update_by = d.UpdateBy,
                                update_date = d.UpdateDate,
                                user_ID = d.UserID,
                                user_identity_id = d.UserIdentity,
                                username = d.Username
                            }).ToList();
                        result.count = entity.Prefix_UserProfile.Where(x => x.Prefix_User.RoleID.Equals(type)).Count();
                        result.status = ServiceResponse.OK;
                        Program.SendMessage("Get list user sort by role id " + type + " with total : " + result.count);
                    }
                }
            }
            catch (Exception ex)
            {
                result.users = null;
                result.count = 0;
                result.status = ServiceResponse.InternalServerError;
                result.status.description = "Fail get list user with error : " + ex.Message;
                Program.SendMessage("Fail get list user with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region User Detail
        public static UserDetailResponse UserDetail(int user_id)
        {
            UserDetailResponse result = new UserDetailResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.user = entity.Prefix_UserProfile.Where(d => d.UserID.Equals(user_id)).Select(d => new UserProfileModel()
                    {
                        address = d.Address,
                        birthdate = d.Birthdate,
                        create_by = d.CreateBy,
                        create_date = d.CreateDate,
                        email = d.Email,
                        first_name = d.Firstname,
                        gender = d.Gender,
                        ID = d.ID,
                        isStudent = d.IsStudent,
                        last_name = d.Lastname,
                        phone = d.Phone,
                        photo = d.Photo,
                        role = new UserRoleModel()
                        {
                            constant_variable = d.Prefix_User.Prefix_UserRole.Constant,
                            role_id = d.Prefix_User.RoleID,
                            role_name = d.Rolename
                        },
                        student_info = new StudentInfoModel()
                        {
                            @class = d.Class,
                            class_year = d.ClassYear,
                            score = d.Score
                        },
                        update_by = d.UpdateBy,
                        update_date = d.UpdateDate,
                        user_ID = d.UserID,
                        user_identity_id = d.UserIdentity,
                        username = d.Username,
                        password = d.Prefix_User.Password
                    }).FirstOrDefault();
                    result.user.password = HelperMethod.decrypt(result.user.password);
                    result.status = ServiceResponse.OK;
                    Program.SendMessage("Get Detail user " + user_id);
                }
            }
            catch (Exception ex)
            {
                result.user = null;
                result.status = ServiceResponse.InternalServerError;
                result.status.description = "Fail get detail user with error : " + ex.Message;
                Program.SendMessage("Fail get detail user with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region User Logout
        public static ResponseData UserLogout(int user_id, string role_pc)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_UserProfile _profile = entity.Prefix_UserProfile.Where(d => d.UserID.Equals(user_id)).FirstOrDefault();
                    Prefix_ListPC using_pc = entity.Prefix_ListPC.Where(d => d.RolePC.ToLower().Equals(role_pc)).FirstOrDefault();
                    if (_profile != null)
                    {
                        entity.Prefix_UserProfile.Attach(_profile);
                        _profile.isOnline = false;
                        if (using_pc != null)
                        {
                            entity.Prefix_ListPC.Attach(using_pc);
                            using_pc.LoggedUser = null;
                            using_pc.IsOnline = false;
                        }
                        entity.SaveChanges();
                        result = ServiceResponse.OK;
                        Program.SendMessage("User " + user_id+ " is Logout");
                    }
                    else
                    {
                        result = ServiceResponse.NotFound;
                        Program.SendMessage("User " + user_id + " Cant logout because user id not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("User " + user_id + " could not logout with error : " + ex.Message);
            }
            return result;

        }
        #endregion

        #region User Remote Login
        public static UserDetailResponse UserRemoteLogin(UserRemoteContract user)
        {
            UserDetailResponse result = new UserDetailResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_User _user = entity.Prefix_User.Where(d => d.ID.Equals(user.user_id)).FirstOrDefault();
                    Prefix_ListPC using_pc = entity.Prefix_ListPC.Where(d => d.ID.Equals(user.pc_id)).FirstOrDefault();
                    if (_user != null && using_pc != null)
                    {
                        Prefix_UserProfile _profile = _user.Prefix_UserProfile.FirstOrDefault();
                        UserProfileModel user_profile = new UserProfileModel()
                        {
                            ID = _profile.ID,
                            user_ID = _profile.UserID,
                            address = _profile.Address,
                            birthdate = _profile.Birthdate,
                            email = _profile.Email,
                            first_name = _profile.Firstname,
                            last_name = _profile.Lastname,
                            gender = _profile.Gender,
                            phone = _profile.Phone,
                            photo = _profile.Photo,
                            role = new UserRoleModel()
                            {
                                role_id = _user.Prefix_UserRole.ID,
                                role_name = _user.Prefix_UserRole.RoleName,
                                constant_variable = _user.Prefix_UserRole.Constant
                            },
                            create_by = _profile.CreateBy,
                            create_date = _profile.CreateDate,
                            isStudent = _profile.IsStudent,
                            update_by = _profile.UpdateBy,
                            update_date = _profile.UpdateDate,
                            user_identity_id = _profile.UserIdentity,
                            username = _profile.Username,
                        };
                        if (_profile.IsStudent)
                        {
                            user_profile.student_info = new StudentInfoModel()
                            {
                                @class = _profile.Class,
                                class_year = _profile.ClassYear,
                                score = _profile.Score,
                            };
                        }

                        entity.Prefix_User.Attach(_user);
                        entity.Prefix_UserProfile.Attach(_profile);
                        entity.Prefix_ListPC.Attach(using_pc);
                        
                        _user.LastLogin = DateTime.Now;
                        _profile.isOnline = true;
                        using_pc.IsOnline = true;
                        using_pc.LoggedUser = _user.ID;
                        entity.SaveChanges();
                        result.status = ServiceResponse.OK;
                        Program.SendMessage("User " + user_profile.username + " Is Login and Connected at " + DateTime.Now);
                        result.user = user_profile;
                    }
                    else
                    {
                        result.status = ServiceResponse.Unauthorized;
                        Program.SendMessage("User " + user.user_id + " Cant Login Because user_id or using_pc not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("User " + user.user_id + " could not Login with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region User Simulation
        public static UserStudentSimulationResponse UserStudentSimulation()
        {
            UserStudentSimulationResponse result = new UserStudentSimulationResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.users = entity.Prefix_UserProfile.Where(d => d.IsStudent.Equals(true) && d.isOnline.Equals(false)).AsEnumerable().Select(d => new UserStudentSimulationModel()
                    {
                        first_name = d.Firstname,
                        gender = d.Gender,
                        profile_id = d.ID,
                        last_name = d.Lastname,
                        photo = d.Photo,
                        @class = d.Class,
                        class_year = d.ClassYear.HasValue ? d.ClassYear.Value : 0,
                        score = d.Score,
                        user_ID = d.UserID,
                        user_identity_id = d.UserIdentity,
                        username = d.Username
                    }).ToList();
                    result.status = ServiceResponse.OK;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("List User Student error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Get User Roles
        public static UserRolesResponse GetUserRoles()
        {
            UserRolesResponse result = new UserRolesResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.roles = entity.Prefix_UserRole.Select(d => new UserRoleModel()
                    {
                        role_id = d.ID,
                        role_name = d.RoleName,
                        constant_variable = d.Constant
                    }).ToList();
                    result.status = ServiceResponse.OK;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = "Fail get user roles with error : " + ex.Message;
                Program.SendMessage("Fail get user roles with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Reset Password

        public static ResponseData ResetPassword(int user_id)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Prefix_User _user = entity.Prefix_User.Where(d => d.ID.Equals(user_id)).FirstOrDefault();
                    if (_user != null)
                    {
                        entity.Prefix_User.Attach(_user);
                        _user.Password = HelperMethod.encrypt(_user.Prefix_UserProfile.FirstOrDefault().UserIdentity);
                        entity.SaveChanges();
                        result = ServiceResponse.Updated;
                        Program.SendMessage("User " + user_id + " Success Reset Password");
                    }
                    else
                    {
                        result = ServiceResponse.NotFound;
                        Program.SendMessage("User " + user_id + " Can't Reset Password Because user not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("User ID " + user_id + " could not Reset password with error : " + ex.Message);
            }
            return result;
        }

        #endregion
    }
}
