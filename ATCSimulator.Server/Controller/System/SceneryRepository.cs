﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Data;
using Newtonsoft.Json;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.System
{
    public class SceneryRepository
    {
        #region Scenery Add
        public static ResponseData SceneryAdd(SceneryDetailContract scenery)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Scenery _scenery = new Base_Scenery()
                    {
                        Taxi = scenery.taxi,
                        Name = scenery.name,
                        Papi = scenery.papi,
                        ApproachLight = JsonConvert.SerializeObject(scenery.approach_light),
                        RunwayLight = JsonConvert.SerializeObject(scenery.runway_light)
                    };
                    entity.Base_Scenery.Add(_scenery);
                    entity.SaveChanges();
                    result = ServiceResponse.Created;
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("Error Create Scenery with error : "+ ex.Message);
            }
            return result;
        }
        #endregion

        #region Scenery Remove
        public static ResponseData SceneryRemove(int scenery_id)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Scenery _scenery = entity.Base_Scenery.Where(d => d.ID.Equals(scenery_id)).FirstOrDefault();
                    if (_scenery != null)
                    {
                        entity.Base_Scenery.Remove(_scenery);
                        entity.SaveChanges();
                        result = ServiceResponse.Deleted;
                        Program.SendMessage("scenery " + scenery_id + " Success Removed");
                    }
                    else
                    {
                        result = ServiceResponse.NotFound;
                        Program.SendMessage("scenery " + scenery_id + " Can't Remove Because user not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("Error Remove Scenery with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Scenery Update
        public static ResponseData SceneryUpdate(SceneryUpdateContract scenery)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Scenery _scenery = entity.Base_Scenery.Where(d => d.ID.Equals(scenery.id)).FirstOrDefault();
                    if (_scenery != null)
                    {
                        _scenery.Taxi = scenery.taxi;
                        _scenery.Name = scenery.name;
                        _scenery.Papi = scenery.papi;
                        _scenery.ApproachLight = JsonConvert.SerializeObject(scenery.approach_light);
                        _scenery.RunwayLight = JsonConvert.SerializeObject(scenery.runway_light);
                        entity.SaveChanges();
                        result = ServiceResponse.Updated;
                        Program.SendMessage("scenery " + scenery.name + " Success Updated");
                    }
                    else
                    {
                        result = ServiceResponse.NotFound;
                        Program.SendMessage("scenery " + scenery.name + " Can't Update Because scenery not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("Error Update Scenery with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Scenery Detail
        public static SceneryDetailResponse SceneryDetail(int scenery_id)
        {
            SceneryDetailResponse result = new SceneryDetailResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Scenery _scenery = entity.Base_Scenery.Where(d => d.ID.Equals(scenery_id)).FirstOrDefault();
                    if (_scenery != null)
                    {
                        SceneryDetailModel data = new SceneryDetailModel()
                        {
                            id = _scenery.ID,
                            name = _scenery.Name,
                            papi = _scenery.Papi,
                            taxi = _scenery.Taxi,
                            approach_light = JsonConvert.DeserializeObject<List<string>>(_scenery.ApproachLight),
                            runway_light = JsonConvert.DeserializeObject<List<string>>(_scenery.RunwayLight)
                        };
                        result.scenery = data;
                        result.status = ServiceResponse.OK;
                        Program.SendMessage("scenery " + scenery_id + " Get Detail");
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                        Program.SendMessage("scenery " + scenery_id + " Can't Get Detail Because scenery not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get Detail Scenery with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Scenery List
        public static SceneryListResponse SceneryList()
        {
            SceneryListResponse result = new SceneryListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<SceneryListModel> _sceneries = entity.Base_Scenery.Select(d => new SceneryListModel()
                    {
                        id = d.ID,
                        name = d.Name
                    }).ToList();
                    if (_sceneries != null)
                    {
                        result.sceneries = _sceneries;
                        result.status = ServiceResponse.OK;
                        Program.SendMessage("succes Get List scenery");
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                        Program.SendMessage("Can't Get List Scenery Because scenery not found");
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get List Scenery with error : " + ex.Message);
            }
            return result;
        }
        #endregion
    }
}
