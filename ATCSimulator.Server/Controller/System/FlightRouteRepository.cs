﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Data;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Helper;
using Newtonsoft.Json;

namespace ATCSimulator.Server.Controller.System
{
    public class FlightRouteRepository
    {
        #region Flight Route List
        public static FlightRouteListResponse FlightRouteList(FlightRouteListContract request)
        {
            FlightRouteListResponse result = new FlightRouteListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    if (request.is_departure)
                    {
                        List<Waypoint_DepartureRoute> _data = new List<Waypoint_DepartureRoute>();
                         _data = entity.Waypoint_DepartureRoute.Where(d => d.SceneryID.Equals(request.scenery_id)).ToList();
                        result.flight_routes = SetFlightRouteData(_data, true);
                    }
                    else
                    {
                        List<Waypoint_ArrivalRoute> _data = new List<Waypoint_ArrivalRoute>();
                        _data = entity.Waypoint_ArrivalRoute.Where(d => d.SceneryID.Equals(request.scenery_id)).ToList();
                        result.flight_routes = SetFlightRouteData(_data,false);
                    }
                    if (result.flight_routes != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get list routes with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Flight Route Detail
        public static FlightRouteDetailResponse FlightRouteDetail(int route_id, bool is_departure)
        {
            FlightRouteDetailResponse result = new FlightRouteDetailResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    if (is_departure)
                    {
                        Waypoint_DepartureRoute _data = entity.Waypoint_DepartureRoute.Where(d => d.ID.Equals(route_id)).FirstOrDefault();
                        result.flight_route = SetFlightRouteObject(_data, true);
                    }
                    else
                    {
                        Waypoint_ArrivalRoute _data = entity.Waypoint_ArrivalRoute.Where(d => d.ID.Equals(route_id)).FirstOrDefault();
                        result.flight_route = SetFlightRouteObject(_data, false);
                    }
                    if (result.flight_route != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get list routes with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        private static List<FlightRouteDetailModel> SetFlightRouteData(object data,bool is_departure)
        {
            List<FlightRouteDetailModel> result = new List<FlightRouteDetailModel>();
            if (is_departure)
            {
                List<Waypoint_DepartureRoute> departures = (List<Waypoint_DepartureRoute>)data;
                foreach (Waypoint_DepartureRoute d in departures)
                {
                    result.Add(SetFlightRouteObject(d, true));
                }
            }
            else
            {
                List<Waypoint_ArrivalRoute> arrivals = (List<Waypoint_ArrivalRoute>)data;
                foreach (Waypoint_ArrivalRoute d in arrivals)
                {
                    result.Add(SetFlightRouteObject(d,false));
                }
            }
            return result;
        }

        private static FlightRouteDetailModel SetFlightRouteObject(object data, bool is_departure)
        {
            FlightRouteDetailModel result = new FlightRouteDetailModel();
            if (is_departure)
            {
                Waypoint_DepartureRoute d = (Waypoint_DepartureRoute)data;
                result.alternate_airport_code = d.AlternateAirportCode;
                result.alternate_airport_id = d.AlternateAirportID;
                result.alternate_airport_name = d.AlternateAirportName;
                result.origin_airport_code = d.OriginAirportCode;
                result.origin_airport_id = d.OriginAirportID;
                result.origin_airport_name = d.OriginAirportName;
                result.destination_airport_code = d.DestAirportCode;
                result.destination_airport_id = d.DestAirportID;
                result.destination_airport_name = d.DestAirportName;
                result.flight_rules = d.FlightRules;
                result.flight_rules_id = d.FlightRulesID;
                result.id = d.ID;
                result.routes_name = JsonConvert.DeserializeObject<List<string>>(d.RoutesName);
                result.runways = JsonConvert.DeserializeObject<List<string>>(d.RunwayUsed);
                result.scenery_id = d.SceneryID;
                result.total_distance = d.TotalDistance;
            }
            else
            {
                Waypoint_ArrivalRoute d = (Waypoint_ArrivalRoute)data;
                result.alternate_airport_code = d.AlternateAirportCode;
                result.alternate_airport_id = d.AlternateAirportID;
                result.alternate_airport_name = d.AlternateAirportName;
                result.origin_airport_code = d.OriginAirportCode;
                result.origin_airport_id = d.OriginAirportID;
                result.origin_airport_name = d.OriginAirportName;
                result.destination_airport_code = d.DestinationAirportCode;
                result.destination_airport_id = d.DestinationAirportID;
                result.destination_airport_name = d.DestinationAirportName;
                result.flight_rules = d.FlightRules;
                result.flight_rules_id = d.FlightRulesID;
                result.id = d.ID;
                result.routes_name = JsonConvert.DeserializeObject<List<string>>(d.RoutesName);
                result.runways = JsonConvert.DeserializeObject<List<string>>(d.RunwayUsed);
                result.scenery_id = d.SceneryID;
                result.total_distance = d.TotalDistance;
                result.waypoint_name = d.StartPointName;
            }
            return result;
        }

    }
}
