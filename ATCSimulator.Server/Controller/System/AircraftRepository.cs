﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Data;
using Newtonsoft.Json;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.System
{
    public class AircraftRepository
    {
        #region Aircraft Detail
        public static AircraftDetailResponse AircraftDetail(int aircraft_id)
        {
            AircraftDetailResponse result = new AircraftDetailResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.aircraft = entity.Base_Aircraft.Where(d=>d.ID.Equals(aircraft_id)).Select(d=>new AircraftModel()
                    {
                        fuel_consumption = d.FuelConsumption,
                        id = d.ID,
                        maximum_altitude = d.MaximumAltitude,
                        circuit_height = d.CircuitHeight,
                        maximum_person = d.MaximumPerson,
                        model = d.Model,
                        true_speed = d.TrueSpeed,
                        type = d.Type,
                        category = d.Category,
                        engine = d.Engine,
                        available_scenery = d.AvailableAtScenery,
                        notAvailable_airport = d.NotAvailableAirport
                    }).FirstOrDefault();
                    if (result.aircraft != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get detail aircraft with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Aircraft List
        public static AircraftListResponse AircraftList()
        {
            AircraftListResponse result = new AircraftListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.aircrafts = entity.Base_Aircraft.Select(d => new AircraftModel()
                    {
                        id = d.ID,
                        model = d.Model,
                        true_speed = d.TrueSpeed,
                        type = d.Type,
                        fuel_consumption = d.FuelConsumption,
                        maximum_altitude = d.MaximumAltitude,
                        circuit_height = d.CircuitHeight,
                        maximum_person = d.MaximumPerson,
                        category = d.Category,
                        engine = d.Engine,
                        available_scenery = d.AvailableAtScenery,
                        notAvailable_airport = d.NotAvailableAirport
                    }).ToList();
                    if (result.aircrafts != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get list aircraft with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Flight Rule List
        public static FlightRuleListResponse FlightRuleList()
        {
            FlightRuleListResponse result = new FlightRuleListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.flight_rules = entity.Base_FlightRules.Select(d => new FlightRuleDetailModel()
                    {
                        id = d.ID,
                        name = d.FlightRules
                    }).ToList();
                    if (result.flight_rules != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get lsit flight rule with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Flight Type List
        public static FlightTypeListResponse FlightTypeList()
        {
            FlightTypeListResponse result = new FlightTypeListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.flight_types = entity.Base_FlightType.Select(d => new FlightTypeDetailModel()
                    {
                        id = d.ID,
                        name = d.Name,
                        description = d.Description
                    }).ToList();
                    if (result.flight_types != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get list Flight Type with error : " + ex.Message);
            }
            return result;
        }
        #endregion

    }
}
