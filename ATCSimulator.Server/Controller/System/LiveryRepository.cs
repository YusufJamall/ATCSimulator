﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Data;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.System
{
    class LiveryRepository
    {
        #region Livery Detail
        public static LiveryDetailResponse LiveryDetail(int id)
        {
            LiveryDetailResponse result = new LiveryDetailResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.livery = entity.Base_AircraftLivery.Where(d => d.ID.Equals(id)).Select(d => new LiveryDetailModel()
                    {
                        aircraft_id = d.AircraftID,
                        aircraft_strips = d.AircraftStrips,
                        callsign = d.Callsign,
                        id = d.ID,
                        aircraft_model = d.AircraftModel,
                        livery = d.Livery
                    }).FirstOrDefault();
                    if (result.livery != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get detail livery with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Livery List
        public static LiveryListResponse LiveryList(int aircraft_id)
        {
            LiveryListResponse result = new LiveryListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    result.liveries = entity.Base_AircraftLivery.Where(d=>d.AircraftID.Equals(aircraft_id)).Select(d => new LiveryDetailModel()
                    {
                        aircraft_id = d.AircraftID,
                        aircraft_strips = d.AircraftStrips,
                        callsign = d.Callsign,
                        id = d.ID,
                        aircraft_model = d.AircraftModel,
                        livery = d.Livery
                    }).ToList();
                    if (result.liveries != null)
                        result.status = ServiceResponse.OK;
                    else
                        result.status = ServiceResponse.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error get lsit livery aircraft with error : " + ex.Message);
            }
            return result;
        }
        #endregion
    }
}
