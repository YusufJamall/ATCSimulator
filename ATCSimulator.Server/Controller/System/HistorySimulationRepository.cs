﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Data;
using Newtonsoft.Json;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.System
{
    public class HistorySimulationRepository
    {
        #region Simulation List
        public static SimulationListResponse SimulationList(int page, int pageSize)
        {
            SimulationListResponse result = new SimulationListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<Base_Simulation> _simulations = entity.Base_Simulation.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    if (_simulations != null)
                    {
                        result.simulations = _simulations.Select(d => new SimulationListModel()
                        {
                            id = d.ID,
                            exercise_name = d.ExerciseName,
                            scenery_id = d.SceneryID,
                            scenery_name = d.SceneryName,
                            play_time = d.PlayTime,
                            elapsed_time = d.ElapsedTime.HasValue ? d.ElapsedTime.Value : new TimeSpan(),
                            weather = d.Weather,
                            wind_direction = d.WindDirection,
                            wind_speed = d.WindSpeed,
                            temperature = d.Temperature,
                            visibility = d.Visibility,
                            time_simulation = d.TimeSimulation,
                            runway_lights = d.RunwayLights,
                            taxi = d.Taxi,
                            papi = d.Papi,
                            approach = d.Approach,
                            total_arrival = d.TotalArrival,
                            total_departure = d.TotalDeparture,
                            created_date = d.CreatedDate,
                            created_by = d.CreatedBy
                        }).ToList();
                        result.count = entity.Base_Simulation.Count();
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get List Simulation with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Delete Simulation
        public static StatusResponse RemoveSimulation(int simulationID)
        {
            StatusResponse result = new StatusResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Simulation simulation = entity.Base_Simulation.Where(d => d.ID.Equals(simulationID)).FirstOrDefault();
                    if (simulation != null)
                    {
                        entity.Base_Simulation.Attach(simulation);
                        entity.Base_Simulation.Remove(simulation);
                        entity.SaveChanges();
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.BadRequest;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Delete Simulation with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Player List
        public static PlayerListResponse PlayerList(int simulation_id)
        {
            PlayerListResponse result = new PlayerListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<Base_SimulationPlayer> _players = entity.Base_SimulationPlayer.Where(d => d.SimulationID.Equals(simulation_id)).ToList();
                    if (_players != null)
                    {
                        result.players = _players.Select(d => new PlayerListModel()
                        {
                            id = d.ID,
                            role_player = d.RolePlayer,
                            user_ID = d.UserID,
                            fullname = d.UserName,

                            pilot = entity.Base_SimulationArrival.Where(a => a.PlayerID.Equals(d.ID)).Select(a => a.Callsign)
                            .Union(entity.Base_SimulationDeparture.Where(b => b.PlayerID.Equals(d.ID)).Select(b => b.Callsign))
                            .Count() > 0 ?
                            string.Join(",", entity.Base_SimulationArrival.Where(a => a.PlayerID.Equals(d.ID)).Select(a => a.Callsign)
                            .Union(entity.Base_SimulationDeparture.Where(b => b.PlayerID.Equals(d.ID)).Select(b => b.Callsign))
                            .ToList()) : "-",

                            point = d.Point.HasValue ? d.Point.Value.ToString() : "-",
                            grade = !string.IsNullOrWhiteSpace(d.Grade) ? d.Grade : "-",
                            edited_by = !string.IsNullOrWhiteSpace(d.EditedBy) ? d.EditedBy : "-",
                            photo_image = entity.Prefix_UserProfile.Where(e => e.UserID.Equals(d.UserID)).Select(e => e.Photo).FirstOrDefault()

                        }).ToList();
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get List Player with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Aircraft History List
        public static AircraftHistoryListResponse AircraftHistoryList(int simulationId)
        {
            AircraftHistoryListResponse result = new AircraftHistoryListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Simulation _simulations = entity.Base_Simulation.Where(d => d.ID.Equals(simulationId)).FirstOrDefault();
                    if (_simulations != null)
                    {
                        result.aircrafts = _simulations.Base_SimulationArrival.Join(entity.Waypoint_ArrivalRoute, e => e.RouteID, f => f.ID, (e, f) => new { e, f }).Select(g => new AircraftHistoryData
                        {
                            callsign = g.e.Callsign,
                            is_departure = false,
                            category = g.e.Base_Aircraft.Category,
                            model = g.e.Base_Aircraft.Model,
                            engine = g.e.Base_Aircraft.Engine,
                            type = g.e.Base_Aircraft.Type,
                            true_speed = g.e.Base_Aircraft.TrueSpeed,
                            maximum_altitude = g.e.Base_Aircraft.MaximumAltitude,
                            livery = g.e.Base_AircraftLivery.Livery,
                            livery_strips = g.e.Base_AircraftLivery.AircraftStrips,
                            fuel = g.e.Fuel,
                            departure_time = g.e.DepartureTime,
                            eet = g.e.EstimateTime,
                            person = g.e.PersonOnBoard,
                            flight_level = g.e.FlightLevel,
                            flight_level_type = g.e.FlightLevelType,
                            route_id = g.e.RouteID,
                            route = g.f.RoutesName,
                            flight_rules_id = g.e.FlightRulesID,
                            flight_rules = g.e.Base_FlightRules.FlightRules,
                            origin_airport_code = g.f.AlternateAirportCode,
                            origin_airport_id = g.f.OriginAirportID,
                            origin_airport_name = g.f.OriginAirportName,
                            destination_airport_code = g.f.DestinationAirportCode,
                            destination_airport_id = g.f.DestinationAirportID,
                            destination_airport_name = g.f.DestinationAirportName,
                            alternate_airport_code = g.f.AlternateAirportCode,
                            alternate_airport_id = g.f.AlternateAirportID,
                            alternate_airport_name = g.f.AlternateAirportName,
                            start_point_name = g.f.StartPointName,
                            end_point_name = g.f.EndPointName,
                            pilotId = g.e.PlayerID
                        }).Union(_simulations.Base_SimulationDeparture.Join(entity.Waypoint_DepartureRoute, h => h.RouteID, i => i.ID, (h, i) => new { h, i }).Select(j => new AircraftHistoryData
                        {
                            callsign = j.h.Callsign,
                            is_departure = true,
                            category = j.h.Base_Aircraft.Category,
                            model = j.h.Base_Aircraft.Model,
                            engine = j.h.Base_Aircraft.Engine,
                            type = j.h.Base_Aircraft.Type,
                            true_speed = j.h.Base_Aircraft.TrueSpeed,
                            maximum_altitude = j.h.Base_Aircraft.MaximumAltitude,
                            livery = j.h.Base_AircraftLivery.Livery,
                            livery_strips = j.h.Base_AircraftLivery.AircraftStrips,
                            fuel = j.h.Fuel,
                            eet = j.h.EstimateTime,
                            person = j.h.PersonOnBoard,
                            flight_level = j.h.FlightLevel,
                            flight_level_type = j.h.FlightLevelType,
                            route_id = j.h.RouteID,
                            route = j.i.RoutesName,
                            flight_rules_id = j.h.FlightRulesID,
                            flight_rules = j.h.Base_FlightRules.FlightRules,
                            origin_airport_code = j.i.AlternateAirportCode,
                            origin_airport_id = j.i.OriginAirportID,
                            origin_airport_name = j.i.OriginAirportName,
                            destination_airport_code = j.i.DestAirportCode,
                            destination_airport_id = j.i.DestAirportID,
                            destination_airport_name = j.i.DestAirportName,
                            alternate_airport_code = j.i.AlternateAirportCode,
                            alternate_airport_id = j.i.AlternateAirportID,
                            alternate_airport_name = j.i.AlternateAirportName,
                            start_point_name = j.i.OriginAirportName,
                            end_point_name = j.i.EndPointName,
                            pilotId = j.h.PlayerID
                        }
                        )).ToList();
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get List Simulation with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Set Point
        public static StatusResponse SetPoint(PointContract point)
        {
            StatusResponse result = new StatusResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_SimulationPlayer player = entity.Base_SimulationPlayer.Where(d => d.ID.Equals(point.playerID) && d.SimulationID.Equals(point.simulationID)).FirstOrDefault();
                    if (player != null)
                    {
                        entity.Base_SimulationPlayer.Attach(player);
                        player.Point = point.point;
                        player.Grade = point.grade;
                        player.EditedBy = point.instructorName;
                        entity.SaveChanges();
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Set Point with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Get History Player
        public static HistoryPlayerResponse GetHistoryPlayer(int userID)
        {
            HistoryPlayerResponse result = new HistoryPlayerResponse();
            result.history = new List<HistoryPlayerModel>();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<Base_SimulationPlayer> player = entity.Base_SimulationPlayer.OrderByDescending(d => d.Base_Simulation.CreatedDate).Where(d => d.UserID.Equals(userID)).ToList();
                    foreach (var data in player)
                    {
                        result.history.Add(new HistoryPlayerModel
                        {
                            player = new PlayerListModel
                            {
                                id = data.ID,
                                role_player = data.RolePlayer,
                                user_ID = data.UserID,
                                fullname = data.UserName,

                                pilot = entity.Base_SimulationArrival.Where(a => a.PlayerID.Equals(data.ID)).Select(a => a.Callsign)
                                .Union(entity.Base_SimulationDeparture.Where(b => b.PlayerID.Equals(data.ID)).Select(b => b.Callsign))
                                .Count() > 0 ?
                                string.Join(",", entity.Base_SimulationArrival.Where(a => a.PlayerID.Equals(data.ID)).Select(a => a.Callsign)
                                .Union(entity.Base_SimulationDeparture.Where(b => b.PlayerID.Equals(data.ID)).Select(b => b.Callsign))
                                .ToList()) : "-",

                                point = data.Point.HasValue ? data.Point.Value.ToString() : "-",
                                grade = !string.IsNullOrWhiteSpace(data.Grade) ? data.Grade : "-",
                                edited_by = !string.IsNullOrWhiteSpace(data.EditedBy) ? data.EditedBy : "-"
                            },
                            simulation = entity.Base_Simulation.Where(d => d.ID.Equals(data.SimulationID)).Select(d => new SimulationPlayerModel
                            {
                                id = d.ID,
                                exercise_name = d.ExerciseName,
                                scenery_name = d.SceneryName,
                                created_date = d.CreatedDate
                            }).FirstOrDefault()
                        });
                    }
                    result.status = ServiceResponse.OK;
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Set Point with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Get History Instructor
        public static HistoryInstructorResponse GetHistoryInstructor(string instructorName)
        {
            HistoryInstructorResponse result = new HistoryInstructorResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<Base_Simulation> _simulations = entity.Base_Simulation.OrderByDescending(d => d.CreatedDate).Where(d => d.CreatedBy.Equals(instructorName)).ToList();
                    if (_simulations != null)
                    {
                        result.simulations = _simulations.Select(d => new SimulationListModel()
                        {
                            id = d.ID,
                            exercise_name = d.ExerciseName,
                            scenery_id = d.SceneryID,
                            scenery_name = d.SceneryName,
                            play_time = d.PlayTime,
                            elapsed_time = d.ElapsedTime.HasValue ? d.ElapsedTime.Value : new TimeSpan(),
                            weather = d.Weather,
                            wind_direction = d.WindDirection,
                            wind_speed = d.WindSpeed,
                            temperature = d.Temperature,
                            visibility = d.Visibility,
                            time_simulation = d.TimeSimulation,
                            runway_lights = d.RunwayLights,
                            taxi = d.Taxi,
                            papi = d.Papi,
                            approach = d.Approach,
                            total_arrival = d.TotalArrival,
                            total_departure = d.TotalDeparture,
                            created_date = d.CreatedDate,
                            created_by = d.CreatedBy

                        }).ToList();
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get History Instructor with error : " + ex.Message);
            }
            return result;
        }
        #endregion
    }
}
