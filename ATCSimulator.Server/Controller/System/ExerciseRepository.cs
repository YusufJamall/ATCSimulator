﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.Server.Model.System;
using ATCSimulator.Server.Contract.System;
using ATCSimulator.Server.Data;
using Newtonsoft.Json;
using ATCSimulator.Server.Helper;

namespace ATCSimulator.Server.Controller.System
{
    public class ExerciseRepository
    {
        #region Exercise Add
        public static ResponseData ExerciseAdd(ExerciseAddContract exercise)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Exercise _exercise = new Base_Exercise()
                    {
                        Approach = JsonConvert.SerializeObject(exercise.approach),
                        RunwayLights = JsonConvert.SerializeObject(exercise.runway_lights),
                        CreateBy = exercise.create_by,
                        CreateDate = DateTime.Now,
                        DescExercise = exercise.desc_exercise,
                        ExerciseName = exercise.exercise_name,
                        Papi = exercise.papi,
                        PlayTime = exercise.play_time,
                        SceneryID = exercise.scenery_id,
                        SceneryName = exercise.scenery_name,
                        Taxi = exercise.taxi,
                        Temperature = exercise.temperature,
                        TimeSimulation = exercise.time_simulation,
                        TotalArrival = exercise.aircrafts.Where(d => d.arrival != null).Count(),
                        TotalDeparture = exercise.aircrafts.Where(d => d.departure != null).Count(),
                        TrafficExercise = exercise.traffic_exercise,
                        UpdateBy = exercise.create_by,
                        UpdateDate = DateTime.Now,
                        Visibility = exercise.visibility,
                        Weather = exercise.weather.ToString(),
                        WindDirection = exercise.wind_direction,
                        WindSpeed = exercise.wind_speed
                    };
                    entity.Base_Exercise.Add(_exercise);
                    entity.SaveChanges();

                    //save aircraft exercise

                    foreach (AircraftExerciseDetailContract a in exercise.aircrafts)
                    {
                        if (a.departure != null)
                        {
                            Base_ExerciseDeparture departure = new Base_ExerciseDeparture()
                            {
                                AircraftID = a.aircraft_id,
                                AircraftLiveryID = a.aircraft_livery_id,
                                Callsign = a.callsign,
                                DepartureTime = a.departure_time,
                                EstimateTime = a.estimate_time,
                                ExerciseID = _exercise.ID,
                                FlightLevel = a.flight_level,
                                FlightLevelType = a.flight_level_type,
                                FlightRulesID = a.flight_rules_id,
                                FlightTypeID = a.flight_type_id,
                                Fuel = a.fuel,
                                ParkingID = a.departure.parking_id,
                                PersonOnBoard = a.person_on_board,
                                RouteID = a.route_id,
                                SceneryID = _exercise.SceneryID
                            };
                            entity.Base_ExerciseDeparture.Add(departure);
                        }
                        else if (a.arrival != null)
                        {
                            Base_ExerciseArrival arrival = new Base_ExerciseArrival()
                            {
                                AircraftID = a.aircraft_id,
                                AircraftLiveryID = a.aircraft_livery_id,
                                Callsign = a.callsign,
                                DepartureTime = a.departure_time,
                                EstimateTime = a.estimate_time,
                                ExerciseID = _exercise.ID,
                                FlightLevel = a.flight_level,
                                FlightLevelType = a.flight_level_type,
                                FlightRulesID = a.flight_rules_id,
                                FlightTypeID = a.flight_type_id,
                                Fuel = a.fuel,
                                StartPointTime = a.arrival.start_time,
                                PersonOnBoard = a.person_on_board,
                                RouteID = a.route_id,
                                SceneryID = _exercise.SceneryID,
                                RunwayUsed = a.arrival.runway
                            };
                            entity.Base_ExerciseArrival.Add(arrival);
                        }
                        entity.SaveChanges();
                    }//end foreach
                    result = ServiceResponse.Created;
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("Error Create Exercise with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Exercise Update
        public static ResponseData ExerciseUpdate(ExerciseUpdateContract exercise)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Exercise _exercise = entity.Base_Exercise.Where(d => d.ID.Equals(exercise.exercise_id)).FirstOrDefault();
                    if (_exercise != null)
                    {
                        _exercise.Approach = JsonConvert.SerializeObject(exercise.approach);
                        _exercise.RunwayLights = JsonConvert.SerializeObject(exercise.runway_lights);
                        _exercise.DescExercise = exercise.desc_exercise;
                        _exercise.ExerciseName = exercise.exercise_name;
                        _exercise.Papi = exercise.papi;
                        _exercise.PlayTime = exercise.play_time;
                        _exercise.SceneryID = exercise.scenery_id;
                        _exercise.SceneryName = exercise.scenery_name;
                        _exercise.Taxi = exercise.taxi;
                        _exercise.Temperature = exercise.temperature;
                        _exercise.TimeSimulation = exercise.time_simulation;
                        _exercise.TotalArrival = exercise.aircrafts.Where(d => d.arrival != null).Count();
                        _exercise.TotalDeparture = exercise.aircrafts.Where(d => d.departure != null).Count();
                        _exercise.TrafficExercise = exercise.traffic_exercise;
                        _exercise.UpdateBy = exercise.update_by;
                        _exercise.UpdateDate = DateTime.Now;
                        _exercise.Visibility = exercise.visibility;
                        _exercise.Weather = exercise.weather.ToString();
                        _exercise.WindDirection = exercise.wind_direction;
                        _exercise.WindSpeed = exercise.wind_speed;
                        entity.SaveChanges();

                        //remove aircraft exercise
                        List<Base_ExerciseArrival> list_arrival = entity.Base_ExerciseArrival.Where(d => d.ExerciseID.Equals(exercise.exercise_id)).ToList();
                        List<Base_ExerciseDeparture> list_departure = entity.Base_ExerciseDeparture.Where(d => d.ExerciseID.Equals(exercise.exercise_id)).ToList();
                        if (list_arrival != null)
                        {
                            foreach (Base_ExerciseArrival a in list_arrival)
                            {
                                entity.Base_ExerciseArrival.Remove(a);
                                entity.SaveChanges();
                            }
                        }
                        if (list_departure != null)
                        {
                            foreach (Base_ExerciseDeparture d in list_departure)
                            {
                                entity.Base_ExerciseDeparture.Remove(d);
                                entity.SaveChanges();
                            }
                        }

                        //save aircraft exercise
                        foreach (AircraftExerciseDetailContract a in exercise.aircrafts)
                        {
                            if (a.departure != null)
                            {
                                Base_ExerciseDeparture departure = new Base_ExerciseDeparture()
                                {
                                    AircraftID = a.aircraft_id,
                                    AircraftLiveryID = a.aircraft_livery_id,
                                    Callsign = a.callsign,
                                    DepartureTime = a.departure_time,
                                    EstimateTime = a.estimate_time,
                                    ExerciseID = _exercise.ID,
                                    FlightLevel = a.flight_level,
                                    FlightLevelType = a.flight_level_type,
                                    FlightRulesID = a.flight_rules_id,
                                    FlightTypeID = a.flight_type_id,
                                    Fuel = a.fuel,
                                    ParkingID = a.departure.parking_id,
                                    PersonOnBoard = a.person_on_board,
                                    RouteID = a.route_id,
                                    SceneryID = _exercise.SceneryID
                                };
                                entity.Base_ExerciseDeparture.Add(departure);
                            }
                            else if (a.arrival != null)
                            {
                                Base_ExerciseArrival arrival = new Base_ExerciseArrival()
                                {
                                    AircraftID = a.aircraft_id,
                                    AircraftLiveryID = a.aircraft_livery_id,
                                    Callsign = a.callsign,
                                    DepartureTime = a.departure_time,
                                    EstimateTime = a.estimate_time,
                                    ExerciseID = _exercise.ID,
                                    FlightLevel = a.flight_level,
                                    FlightLevelType = a.flight_level_type,
                                    FlightRulesID = a.flight_rules_id,
                                    FlightTypeID = a.flight_type_id,
                                    Fuel = a.fuel,
                                    StartPointTime = a.arrival.start_time,
                                    PersonOnBoard = a.person_on_board,
                                    RouteID = a.route_id,
                                    SceneryID = _exercise.SceneryID,
                                    RunwayUsed = a.arrival.runway
                                };
                                entity.Base_ExerciseArrival.Add(arrival);
                            }
                            entity.SaveChanges();
                        }//end foreach

                    }
                    result = ServiceResponse.Updated;
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("Error Update Exercise with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Exercise Remove
        public static ResponseData ExerciseRemove(int exercise_id)
        {
            ResponseData result = new ResponseData();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Exercise _exercise = entity.Base_Exercise.Where(d => d.ID.Equals(exercise_id)).FirstOrDefault();
                    if (_exercise != null)
                    {
                        entity.Base_Exercise.Remove(_exercise);
                        entity.SaveChanges();
                    }
                    result = ServiceResponse.Deleted;
                }
            }
            catch (Exception ex)
            {
                result = ServiceResponse.InternalServerError;
                result.description = ex.Message;
                Program.SendMessage("Error Delete Exercise with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Exercise Detail
        public static ExerciseDetailResponse ExerciseDetail(int exercise_id)
        {
            ExerciseDetailResponse result = new ExerciseDetailResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    Base_Exercise _exercise = entity.Base_Exercise.Where(d => d.ID.Equals(exercise_id)).FirstOrDefault();
                    if (_exercise != null)
                    {
                        result.exercise = new ExerciseDetailModel()
                        {
                            runway_lights = JsonConvert.DeserializeObject<Dictionary<string, bool>>(_exercise.RunwayLights),
                            approach_lights = JsonConvert.DeserializeObject<Dictionary<string, bool>>(_exercise.Approach),
                            exercise_name = _exercise.ExerciseName,
                            id = _exercise.ID,
                            desc_exercise = _exercise.DescExercise,
                            papi = _exercise.Papi,
                            play_time = _exercise.PlayTime,
                            scenery_id = _exercise.SceneryID,
                            scenery_name = _exercise.SceneryName,
                            taxi = _exercise.Taxi,
                            temperature = _exercise.Temperature,
                            time_simulation = _exercise.TimeSimulation,
                            traffic_exercise = _exercise.TrafficExercise,
                            visibility = _exercise.Visibility,
                            weather = _exercise.Weather,
                            wind_direction = _exercise.WindDirection,
                            wind_speed = _exercise.WindSpeed

                        };
                        result.exercise.aircrafts = new List<ListAircraftExerciseModel>();
                        List<Base_ExerciseArrival> arrival = _exercise.Base_ExerciseArrival.ToList();
                        List<Base_ExerciseDeparture> departure = _exercise.Base_ExerciseDeparture.ToList();

                        foreach (Base_ExerciseArrival a in arrival)
                        {
                            ListAircraftExerciseModel ac = new ListAircraftExerciseModel()
                            {
                                aircraft_id = a.AircraftID,
                                estimate_time = a.EstimateTime,
                                flight_level = a.FlightLevel,
                                flight_level_type = a.FlightLevelType,
                                flight_rules_id = a.FlightRulesID,
                                flight_type_id = a.FlightTypeID,
                                fuel = a.Fuel,
                                livery_id = a.AircraftLiveryID,
                                person_on_board = a.PersonOnBoard,
                                route_id = a.RouteID,
                                start_time = a.StartPointTime,
                                callsign = a.Callsign,
                                departure_time = a.DepartureTime,
                                destination = a.Waypoint_ArrivalRoute.DestinationAirportName,
                                flight_rule_name = a.Base_FlightRules.FlightRules,
                                is_departure = false,
                                aircraft_model = a.Base_Aircraft.Model,
                                origin = a.Waypoint_ArrivalRoute.OriginAirportName,
                                livery_name = a.Base_AircraftLivery.Livery,
                                flight_type = string.Format("{0}-{1}", a.Base_FlightType.Name, a.Base_FlightType.Description),
                                livery_strips = a.Base_AircraftLivery.AircraftStrips,
                                runway = a.RunwayUsed
                            };
                            result.exercise.aircrafts.Add(ac);
                        }
                        foreach (Base_ExerciseDeparture d in departure)
                        {
                            ListAircraftExerciseModel ac = new ListAircraftExerciseModel()
                            {
                                aircraft_id = d.AircraftID,
                                estimate_time = d.EstimateTime,
                                flight_level = d.FlightLevel,
                                flight_level_type = d.FlightLevelType,
                                flight_rules_id = d.FlightRulesID,
                                flight_type_id = d.FlightTypeID,
                                fuel = d.Fuel,
                                livery_id = d.AircraftLiveryID,
                                person_on_board = d.PersonOnBoard,
                                route_id = d.RouteID,
                                parking_id = d.ParkingID,
                                callsign = d.Callsign,
                                departure_time = d.DepartureTime,
                                destination = d.Waypoint_DepartureRoute.DestAirportName,
                                flight_rule_name = d.Base_FlightRules.FlightRules,
                                is_departure = true,
                                aircraft_model = d.Base_Aircraft.Model,
                                origin = d.Waypoint_DepartureRoute.OriginAirportName,
                                livery_name = d.Base_AircraftLivery.Livery,
                                flight_type = string.Format("{0}-{1}", d.Base_FlightType.Name, d.Base_FlightType.Description),
                                livery_strips = d.Base_AircraftLivery.AircraftStrips
                            };
                            result.exercise.aircrafts.Add(ac);
                        }
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get Detail Exercise with error : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Exercise List
        public static ExerciseListResponse ExerciseList()
        {
            ExerciseListResponse result = new ExerciseListResponse();
            try
            {
                using (ATCSimulatorDBEntities entity = new ATCSimulatorDBEntities())
                {
                    List<Base_Exercise> _exercises = entity.Base_Exercise.ToList();
                    if (_exercises != null)
                    {
                        result.exercises = _exercises.Select(d => new ExerciseListModel()
                        {
                            exercise_name = d.ExerciseName,
                            play_time = d.PlayTime,
                            scenery_id = d.SceneryID,
                            scenery_name = d.SceneryName,
                            time_simulation = d.TimeSimulation,
                            id = d.ID,
                            total_arrival = d.TotalArrival,
                            total_departure = d.TotalDeparture,
                            traffic_exercise = d.TrafficExercise,
                            weather = d.Weather
                        }).ToList();
                        result.status = ServiceResponse.OK;
                    }
                    else
                    {
                        result.status = ServiceResponse.NotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                result.status = ServiceResponse.InternalServerError;
                result.status.description = ex.Message;
                Program.SendMessage("Error Get List Exercise with error : " + ex.Message);
            }
            return result;
        }
        #endregion
    }
}
