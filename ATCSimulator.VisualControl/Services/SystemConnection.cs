﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;
using ATCSimulator.VisualControl.SystemService;
using ATCSimulator.VisualControl.Helpers;

namespace ATCSimulator.VisualControl.Services
{
    [CallbackBehavior(
      ConcurrencyMode = ConcurrencyMode.Multiple,
      UseSynchronizationContext = false)]
    public class SystemConnection : SystemServiceCallback
    {
        private SynchronizationContext _uiSyncContext = null;
        private SystemServiceClient _atcService = null;
        private UserComputerInfo user_info;
        public static SystemConnection Instance { get; private set; }

        public SystemConnection()
        {
            Instance = this;
            SetProperties();
        }

        private void SetProperties()
        {
            user_info = Helper.GetComputerInfo();
            Instance = this;
        }

        #region Connect Wrapper
        public T ConnectionWrapper<T>(Func<T> f) where T : class, new()
        {
            T returnData = new T();
            Type type = Type.GetType(returnData.ToString(), true);
            PropertyInfo prop = type.GetProperty("status");
            ResponseData status = new ResponseData();
            try
            {
                if (OpenConnection())
                {
                    returnData = f();
                }
            }
            catch (Exception ex)
            {
                status = ServiceResponses.InternalServerError;
                prop.SetValue(returnData, status, null);
            }
            return returnData;
        }
        #endregion

        #region Open Connection
        private bool OpenConnection()
        {
            try
            {
                if (_atcService == null || !_atcService.State.ToString().Equals("Opened"))
                {
                    if (user_info == null)
                        SetProperties();
                    _uiSyncContext = SynchronizationContext.Current;
                    _atcService = new SystemServiceClient(new InstanceContext(this), "TcpBinding");
                    _atcService.Open();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region PC Service

        #region Connect PC
        public StatusResponse ConnectPC()
        {
            if (OpenConnection())
            {

            }

            return ConnectionWrapper(delegate()
            {
                StatusResponse result = new StatusResponse();
                result.status = ServiceResponses.InternalServerError;
                result.status.message = "couldn't connect to server";
                result = _atcService.ConnectPC(new PCContract()
                {
                    ip_address = user_info.ip_address,
                    pc_name = user_info.pc_name,
                    mac_address = user_info.mac_address,
                    role_pc = user_info.role_pc
                });
                return result;
            });
        }
        #endregion

        #region DisconnectPC
        public static StatusResponse DisconnectPC()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.DisconnectPC(new DisconnectPCContract()
                {
                    mac_address = Instance.user_info.mac_address,
                    role_pc = Instance.user_info.role_pc
                });
            });
        }
        #endregion

        #region ShutdownRestartPC
        public static StatusResponse ShutdownRestartPC(ShutdownRestartPCContract pc)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ShutdownRestartPC(pc);
            });
        }
        #endregion

        #region Ping Service
        public static bool Ping()
        {
            try
            {
                return Instance._atcService.Ping();
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #endregion

        #region CallBack
        public bool PingClient()
        {
            return true;
        }

        #region ShutdownRestart
        public void ShutdownRestart(string command)
        {
            Helper.ExitWindows.ShutdownRestart(command, false);
        }
        #endregion

        #endregion

        #region Unused Callback
        public void RemoteLogin(UserProfileModel user)
        {
            throw new NotImplementedException();
        }

        public void PCConnect(string role_pc)
        {
            throw new NotImplementedException();
        }

        public void ConnectUpdate()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
