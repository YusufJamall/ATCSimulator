﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Configuration;
using ATCSimulator.VisualControl.Services;
using ATCSimulator.VisualControl.SystemService;

namespace ATCSimulator.VisualControl
{
    public partial class InfoForm : Form
    {
        private SystemConnection system_connection;
        ConnectToServerAsync connectAsync;
        public static InfoForm Instance { get; private set; }
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        public InfoForm()
        {
            InitializeComponent();
            Instance = this;
            SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
            this.Size = new Size { Height = 45, Width = 150 };
            this.Location = new Point { X = this.Location.X, Y = 0 };
            this.ShowInTaskbar = false;
            system_connection = new SystemConnection();
            connectAsync = new ConnectToServerAsync(Instance.ConnectToServer);

        }
        private void InfoForm_Load(object sender, EventArgs e)
        {
            if (ConnectToServer())
            {
                try
                {
                    Process.Start(ConfigurationManager.AppSettings["ExeName"].ToString());
                    Task task = new Task(PingServerAsync);
                    task.Start();
                }catch(Exception ex)
                {
                    MessageBox.Show("Wrong Configuration Setting");
                    Application.Exit();
                }

            }
            else
            {
                MessageBox.Show("Service Connection Error, application failed to run");
                Application.Exit();
            }
        }
        public void ChildFormClosed(object sender, FormClosedEventArgs e)
        {
            Exit();
        }

        private async void PingServerAsync()
        {
            if (SystemConnection.Ping())
            {
                this.SendToBack();
                await Task.Run(() => { PingServerAsync(); });
            }
            else
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.Disconnect();
                    }));
                }
                else
                {
                    Instance.Disconnect();
                }
                await Task.Run(() => { System.Threading.Thread.Sleep(1000); });

                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.Connecting();
                    }));
                }
                else
                {
                    Instance.Connecting();
                }
                await Task.Run(() =>
                {
                    System.Threading.Thread.Sleep(1000);
                    connectAsync.Invoke();
                    System.Threading.Thread.Sleep(1000);
                    PingServerAsync();
                });
            }
        }

        private void Connecting()
        {
            messageLabel.Text = "Connecting...";
            messageLabel.ForeColor = Color.Blue;
        }
        private void Disconnect()
        {
            this.BringToFront();
            messageLabel.Text = "Connection Lost";
            messageLabel.ForeColor = Color.Red;
        }
        private void Connect()
        {
            messageLabel.Text = "Connected";
            messageLabel.ForeColor = Color.Green;
        }
        private void ConnectFailed()
        {
            messageLabel.Text = "Connect Failed";
            messageLabel.ForeColor = Color.Red;
        }
        private bool ConnectToServer()
        {
            try
            {

                StatusResponse message_system = system_connection.ConnectPC();
                if (message_system.status.code == 0)
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            Instance.Connect();
                        }));
                    }
                    else
                    {
                        Instance.Connect();
                    }
                    return true;
                }
                else
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            Instance.ConnectFailed();
                        }));
                    }
                    else
                    {
                        Instance.ConnectFailed();
                    }
                    return false;
                }
            }
            catch
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.ConnectFailed();
                    }));
                }
                else
                {
                    Instance.ConnectFailed();
                }
                return false;
            }
        }
        public delegate bool ConnectToServerAsync();

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Exit();
        }

        private void Exit()
        {
            StatusResponse disconnect = SystemConnection.DisconnectPC();
            Process[] processes;
            Dictionary<string, string> item = new Dictionary<string, string>();
            processes = Process.GetProcessesByName(ConfigurationManager.AppSettings["ProcName"].ToString());
            foreach (Process proc in processes)
            {
                item.Add(proc.MainWindowTitle, proc.Id.ToString());
            }
            foreach (var proccess in item)
            {
                try
                {
                    int procID = System.Convert.ToInt32(proccess.Value);
                    Process tempProc = Process.GetProcessById(procID);
                    tempProc.CloseMainWindow();
                    tempProc.WaitForExit();
                }
                catch { }
            }
            Application.Exit();
        }
    }
}
