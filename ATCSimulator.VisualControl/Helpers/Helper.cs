﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace ATCSimulator.VisualControl.Helpers
{
    public class Helper
    {
        #region Get Computer Info
        public static UserComputerInfo GetComputerInfo()
        {
            // Get Local IP Address and PC Name
            UserComputerInfo info = new UserComputerInfo();
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            info.pc_name = host.HostName.ToString();
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    info.ip_address = ip.ToString();
                }
            }

            //Get Machine Address
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            info.mac_address = sMacAddress;
            info.role_pc = ConfigurationManager.AppSettings["PCRole"].ToString();
            return info;
        }
        #endregion

        #region Shutdown and Restart
        public static class ExitWindows
        {

            private struct LUID
            {
                public int LowPart;
                public int HighPart;
            }
            private struct LUID_AND_ATTRIBUTES
            {
                public LUID pLuid;
                public int Attributes;
            }
            private struct TOKEN_PRIVILEGES
            {
                public int PrivilegeCount;
                public LUID_AND_ATTRIBUTES Privileges;
            }

            [DllImport("advapi32.dll")]
            static extern int OpenProcessToken(IntPtr ProcessHandle,
                int DesiredAccess, out IntPtr TokenHandle);

            [DllImport("advapi32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            static extern bool AdjustTokenPrivileges(IntPtr TokenHandle,
                [MarshalAs(UnmanagedType.Bool)]bool DisableAllPrivileges,
                ref TOKEN_PRIVILEGES NewState,
                UInt32 BufferLength,
                IntPtr PreviousState,
                IntPtr ReturnLength);

            [DllImport("advapi32.dll")]
            static extern int LookupPrivilegeValue(string lpSystemName,
                string lpName, out LUID lpLuid);

            [DllImport("user32.dll", SetLastError = true)]
            static extern int ExitWindowsEx(uint uFlags, uint dwReason);

            const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
            const short SE_PRIVILEGE_ENABLED = 2;
            const short TOKEN_ADJUST_PRIVILEGES = 32;
            const short TOKEN_QUERY = 8;

            const ushort EWX_LOGOFF = 0;
            const ushort EWX_POWEROFF = 0x00000008;
            const ushort EWX_REBOOT = 0x00000002;
            const ushort EWX_RESTARTAPPS = 0x00000040;
            const ushort EWX_SHUTDOWN = 0x00000001;
            const ushort EWX_FORCE = 0x00000004;

            private static void getPrivileges()
            {
                IntPtr hToken;
                TOKEN_PRIVILEGES tkp;

                OpenProcessToken(Process.GetCurrentProcess().Handle,
                    TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, out hToken);
                tkp.PrivilegeCount = 1;
                tkp.Privileges.Attributes = SE_PRIVILEGE_ENABLED;
                LookupPrivilegeValue("", SE_SHUTDOWN_NAME,
                    out tkp.Privileges.pLuid);
                AdjustTokenPrivileges(hToken, false, ref tkp,
                    0U, IntPtr.Zero, IntPtr.Zero);
            }
            private static void Shutdown(bool force)
            {
                getPrivileges();
                ExitWindowsEx(EWX_SHUTDOWN |
                    (uint)(force ? EWX_FORCE : 0) | EWX_POWEROFF, 0);
            }

            private static void Reboot(bool force)
            {
                getPrivileges();
                ExitWindowsEx(EWX_REBOOT |
                    (uint)(force ? EWX_FORCE : 0), 0);
            }
            private static void LogOff(bool force)
            {
                getPrivileges();
                ExitWindowsEx(EWX_LOGOFF |
                    (uint)(force ? EWX_FORCE : 0), 0);
            }
            public static void ShutdownRestart(string command, bool forced)
            {
                switch (command.ToLower())
                {
                    case "off":
                        Shutdown(forced);
                        break;
                    case "restart":
                        Reboot(forced);
                        break;
                }
            }

        }
        #endregion
    }
    public class UserComputerInfo
    {
        public string pc_name { get; set; }
        public string ip_address { get; set; }
        public string mac_address { get; set; }
        public string role_pc { get; set; }
    }
}
