﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCSimulator.VisualControl.SystemService;

namespace ATCSimulator.VisualControl.Helpers
{
    class ServiceResponses
    {
        public static ResponseData OK
        {
            get
            {
                return new ResponseData
                {
                    code = 0,
                    message = "OK",
                    description = "It's all good bro, you hit the right endpoint with right credentials"
                };
            }

        }
        public static ResponseData Created
        {
            get
            {
                return new ResponseData
                {
                    code = 0,
                    message = "Created",
                    description = "New object saved"
                };
            }
        }
        public static ResponseData Updated
        {
            get
            {
                return new ResponseData
                {
                    code = 0,
                    message = "Updated",
                    description = "Update object saved"
                };
            }
        }
        public static ResponseData Deleted
        {
            get
            {
                return new ResponseData
                {
                    code = 0,
                    message = "Deleted",
                    description = "Object deleted"
                };
            }
        }

        public static ResponseData Accepted
        {
            get
            {
                return new ResponseData
                {
                    code = 0,
                    message = "Accepted",
                    description = "The request has been accepted for processing, but the processing has not been completed."
                };
            }
        }

        public static ResponseData BadRequest
        {
            get
            {
                return new ResponseData
                {
                    code = 400,
                    message = "Bad Request",
                    description = "Returns Data with the error message"
                };
            }
        }

        public static ResponseData Unauthorized
        {
            get
            {
                return new ResponseData
                {
                    code = 401,
                    message = "Unauthorized",
                    description = "Couldn't authenticate your request"
                };
            }
        }
        public static ResponseData NotFound
        {
            get
            {
                return new ResponseData
                {
                    code = 404,
                    message = "Not Found",
                    description = "No such object"
                };
            }
        }
        public static ResponseData MethodNotAllowed
        {
            get
            {
                return new ResponseData
                {
                    code = 405,
                    message = "Method Not Allowed",
                    description = "Supplied HTTP method not allowed for this endpoint"
                };
            }
        }
        public static ResponseData InternalServerError
        {
            get
            {
                return new ResponseData
                {
                    code = 500,
                    message = "Internal Server Error",
                    description = "Something went wrong"
                };
            }
        }
        public static ResponseData ServiceUnavailable
        {
            get
            {
                return new ResponseData
                {
                    code = 503,
                    message = "Service Unavailable",
                    description = "Your connection is being throttled"
                };
            }
        }
    }
}
