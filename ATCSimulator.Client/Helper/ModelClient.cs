﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.SimulationService;

namespace ATCSimulator.Client.Helper
{

    public class FlightStrip
    {
        public Bitmap image { get; set; }
        public AircraftData aircraft { get; set; }
    }
    public class FlightPlanModel
    {
        public string flight_rules { get; set; }
        public string callsign { get; set; }
        public string type_aircraft { get; set; }
        public string flight_type { get; set; }
        public double true_speed { get; set; }
        public string origin_airport_code { get; set; }
        public string origin_airport_name { get; set; }
        public string routes { get; set; }
        public string destination_airport_code { get; set; }
        public string destination_airport_name { get; set; }
        public TimeSpan eet { get; set; }
        public TimeSpan departure_time { get; set; }
        public TimeSpan fuel_board { get; set; }
        public string alternate_airport_name { get; set; }
        public int person { get; set; }
        public string strips { get; set; }
        public double altitude { get; set; }
        public string flight_level { get; set; }
        public PilotInfoModel pilot { get; set; }
    }
    public class UserComputerInfo
    {
        public string pc_name { get; set; }
        public string ip_address { get; set; }
        public string mac_address { get; set; }
        public string role_pc { get; set; }
        public string application { get; set; }
    }
    public class PilotInfoModel
    {
        public string name { get; set; }
        public string address_city { get; set; }
        public string phone { get; set; }
        public string airport_city { get; set; }
    }

    public class AircraftListGridTable
    {
        public ListAircraftExerciseModel properties { get; set; }
        public string callsign { get; set; }
        public string model { get; set; }
        public string flight { get; set; }
        public string flight_rule { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
        public string departure_time { get; set; }
    }

    #region Data Table User
    public class DataUserList
    {
        public int ID { get; set; }
        public int user_ID { get; set; }
        public Image picture { get; set; }
        public string user_identity_id { get; set; }
        public string fullname { get; set; }
        public int role_id { get; set; }
        public string role_name { get; set; }
        public string username { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public DateTime? birthday { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public bool is_student { get; set; }
        public string @class { get; set; }
        public int class_year { get; set; }
        public UserProfileModel profile { get; set; }
    }

    #endregion

    #region Data Table History
    public class DataHistoryPlayer
    {
        public int player_ID { get; set; }
        public int simulationID { get; set; }
        public DateTime dateSimulation { get; set; }
        public string exerciseName { get; set; }
        public string sceneryName { get; set; }
        public string rolePlayer { get; set; }
        public string pilot { get; set; }
        public string point { get; set; }
        public string grade { get; set; }
    }

    #endregion

}
