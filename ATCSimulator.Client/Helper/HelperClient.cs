﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using System.Management;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.InteropServices;
using ATCSimulator.Client.SimulationService;

namespace ATCSimulator.Client.Helper
{
    public class HelperClient
    {
        #region Convert
        public static double GetEET(double distance, double speed)
        {
            //speed km/h distance in km
            return (distance / speed) * 60;
        }
        public static double ConvertSpeed(string type, double speed)
        {
            double KPH2MACH = 0.00081630;
            double KPH2KNOT = 0.53996;
            double result = speed;
            //speed in kph
            if (type.Equals("N"))
                result = speed * KPH2KNOT;
            if (type.Equals("M"))
                result = speed * KPH2MACH;
            return result;

        }
        #endregion

        #region Get Data From DropDownList
        public static T GetDataDropDown<T>(Telerik.WinControls.UI.RadDropDownList item) where T : class, new()
        {
            T returnData = null;
            if (item.SelectedIndex > -1)
            {
                Telerik.WinControls.UI.RadListDataItem data = item.Items[item.SelectedIndex];
                returnData = (T)data.Value;
            }
            return returnData;
        }
        #endregion

        #region Image Handler
        public static byte[] imageToByteArray(System.Drawing.Image imgIn)
        {
            byte[] data = null;
            Bitmap imgOut = new Bitmap(imgIn);
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                imgOut.Save(ms, imgIn.RawFormat);
                data = ms.ToArray();
            }
            return data;
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            Image returnImage = null;
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(byteArrayIn))
            {
                returnImage = Bitmap.FromStream(ms);
            }
            return returnImage;
        }

        public static byte[] getResizedImage(String path, int width, int height)
        {
            Bitmap imgIn = new Bitmap(path);
            double y = imgIn.Height;
            double x = imgIn.Width;

            double factor = 1;
            if (width > 0)
            {
                factor = width / x;
            }
            else if (height > 0)
            {
                factor = height / y;
            }
            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

            // Set DPI of image (xDpi, yDpi)
            imgOut.SetResolution(72, 72);

            Graphics g = Graphics.FromImage(imgOut);
            g.Clear(Color.White);
            g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
              new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

            imgOut.Save(outStream, getImageFormat(path));
            return outStream.ToArray();
        }
        public static byte[] getResizedImage(Image image, int width, int height)
        {
            Bitmap imgIn = new Bitmap(image);
            double y = imgIn.Height;
            double x = imgIn.Width;

            double factor = 1;
            if (width > 0)
            {
                factor = width / x;
            }
            else if (height > 0)
            {
                factor = height / y;
            }
            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

            // Set DPI of image (xDpi, yDpi)
            imgOut.SetResolution(72, 72);

            Graphics g = Graphics.FromImage(imgOut);
            g.Clear(Color.White);
            g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
              new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

            imgOut.Save(outStream, ImageFormat.Jpeg);
            return outStream.ToArray();
        }
        public static ImageFormat getImageFormat(String path)
        {
            switch (Path.GetExtension(path))
            {
                case ".bmp": return ImageFormat.Bmp;
                case ".gif": return ImageFormat.Gif;
                case ".jpg": return ImageFormat.Jpeg;
                case ".png": return ImageFormat.Png;
                default: break;
            }
            return ImageFormat.Jpeg;
        }

        #endregion

        #region Get Computer Info
        public static UserComputerInfo GetComputerInfo()
        {
            // Get Local IP Address and PC Name
            UserComputerInfo info = new UserComputerInfo();
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            info.pc_name = host.HostName.ToString();
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    info.ip_address = ip.ToString();
                }
            }

            //Get Machine Address
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            info.mac_address = sMacAddress;
            info.role_pc = ConfigurationManager.AppSettings["PCRole"].ToString();
            return info;
        }
        #endregion

        #region Shutdown and Restart
        public static class ExitWindows
        {

            private struct LUID
            {
                public int LowPart;
                public int HighPart;
            }
            private struct LUID_AND_ATTRIBUTES
            {
                public LUID pLuid;
                public int Attributes;
            }
            private struct TOKEN_PRIVILEGES
            {
                public int PrivilegeCount;
                public LUID_AND_ATTRIBUTES Privileges;
            }

            [DllImport("advapi32.dll")]
            static extern int OpenProcessToken(IntPtr ProcessHandle,
                int DesiredAccess, out IntPtr TokenHandle);

            [DllImport("advapi32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            static extern bool AdjustTokenPrivileges(IntPtr TokenHandle,
                [MarshalAs(UnmanagedType.Bool)]bool DisableAllPrivileges,
                ref TOKEN_PRIVILEGES NewState,
                UInt32 BufferLength,
                IntPtr PreviousState,
                IntPtr ReturnLength);

            [DllImport("advapi32.dll")]
            static extern int LookupPrivilegeValue(string lpSystemName,
                string lpName, out LUID lpLuid);

            [DllImport("user32.dll", SetLastError = true)]
            static extern int ExitWindowsEx(uint uFlags, uint dwReason);

            const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
            const short SE_PRIVILEGE_ENABLED = 2;
            const short TOKEN_ADJUST_PRIVILEGES = 32;
            const short TOKEN_QUERY = 8;

            const ushort EWX_LOGOFF = 0;
            const ushort EWX_POWEROFF = 0x00000008;
            const ushort EWX_REBOOT = 0x00000002;
            const ushort EWX_RESTARTAPPS = 0x00000040;
            const ushort EWX_SHUTDOWN = 0x00000001;
            const ushort EWX_FORCE = 0x00000004;

            private static void getPrivileges()
            {
                IntPtr hToken;
                TOKEN_PRIVILEGES tkp;

                OpenProcessToken(Process.GetCurrentProcess().Handle,
                    TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, out hToken);
                tkp.PrivilegeCount = 1;
                tkp.Privileges.Attributes = SE_PRIVILEGE_ENABLED;
                LookupPrivilegeValue("", SE_SHUTDOWN_NAME,
                    out tkp.Privileges.pLuid);
                AdjustTokenPrivileges(hToken, false, ref tkp,
                    0U, IntPtr.Zero, IntPtr.Zero);
            }
            private static void Shutdown(bool force)
            {
                getPrivileges();
                ExitWindowsEx(EWX_SHUTDOWN |
                    (uint)(force ? EWX_FORCE : 0) | EWX_POWEROFF, 0);
            }

            private static void Reboot(bool force)
            {
                getPrivileges();
                ExitWindowsEx(EWX_REBOOT |
                    (uint)(force ? EWX_FORCE : 0), 0);
            }
            private static void LogOff(bool force)
            {
                getPrivileges();
                ExitWindowsEx(EWX_LOGOFF |
                    (uint)(force ? EWX_FORCE : 0), 0);
            }
            public static void ShutdownRestart(string command, bool forced)
            {
                switch (command.ToLower())
                {
                    case "off":
                        Shutdown(forced);
                        break;
                    case "restart":
                        Reboot(forced);
                        break;
                }
            }

        }
        #endregion

        #region Wake Function
        public class WOLClass : UdpClient
        {
            public WOLClass()
                : base()
            { }
            //this is needed to send broadcast packet
            public void SetClientToBrodcastMode()
            {
                if (this.Active)
                    this.Client.SetSocketOption(SocketOptionLevel.Socket,
                                              SocketOptionName.Broadcast, 0);
            }


            public bool WakeFunction(string MAC_ADDRESS)
            {
                bool result = false;
                try
                {
                    WOLClass client = new WOLClass();
                    client.Connect(new IPAddress(0xffffffff), 0x2fff); // port=12287 let's use this one 
                    client.SetClientToBrodcastMode();
                    //set sending bites
                    int counter = 0;
                    //buffer to be send
                    byte[] bytes = new byte[1024];   // more than enough :-)
                    //first 6 bytes should be 0xFF
                    for (int y = 0; y < 6; y++)
                        bytes[counter++] = 0xFF;
                    //now repeate MAC 16 times
                    for (int y = 0; y < 16; y++)
                    {
                        int i = 0;
                        for (int z = 0; z < 6; z++)
                        {
                            bytes[counter++] =
                                byte.Parse(MAC_ADDRESS.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
                            i += 2;
                        }
                    }

                    //now send wake up packet
                    int reterned_value = client.Send(bytes, 1024);

                    //sucess
                    result = true;
                }
                catch (Exception ex)
                {

                }
                return result;
            }

        }
        #endregion

        #region Get Image
        public static Image GetImageLivery(string livery, bool isLarge)
        {
            Image result = null;
            switch (livery.ToLower())
            {
                case "kartika airlines":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.KartikaAirlines_M:
                        global::ATCSimulator.Client.Properties.Resources.KartikaAirlines_S;
                    break;
                case "air asia":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.AirAsia_M :
                        global::ATCSimulator.Client.Properties.Resources.AirAsia_S;
                    break;
                case "batavia air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.BataviaAir_M :
                        global::ATCSimulator.Client.Properties.Resources.BataviaAir_S;
                    break;
                case "batik air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.BatikAir_M :
                        global::ATCSimulator.Client.Properties.Resources.BatikAir_S;
                    break;
                case "lion air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.LionAir_M :
                        global::ATCSimulator.Client.Properties.Resources.LionAir_S;
                    break;
                case "citilink":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.Citilink_M :
                        global::ATCSimulator.Client.Properties.Resources.Citilink_S;
                    break;
                case "merpati nusantara":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.MerpatiAir_M :
                        global::ATCSimulator.Client.Properties.Resources.MerpatiAir_S;
                    break;
                case "sriwijaya air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.SriwijayaAir_M :
                        global::ATCSimulator.Client.Properties.Resources.SriwijayaAir_S;
                    break;
                case "angkatan udara":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.TNIAU_M :
                        global::ATCSimulator.Client.Properties.Resources.TNIAU_S;
                    break;
                case "kalstar aviation":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.Kalstar_M :
                        global::ATCSimulator.Client.Properties.Resources.Kalstar_S;
                    break;
                case "garuda indonesia":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.GarudaAir_M :
                        global::ATCSimulator.Client.Properties.Resources.GarudaAir_S;
                    break;
                case "oelita air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.PelitaAir_M :
                        global::ATCSimulator.Client.Properties.Resources.PelitaAir_S;
                    break;
                case "wings air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.WingsAir_M :
                        global::ATCSimulator.Client.Properties.Resources.WingsAir_S;
                    break;
                case "sar":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.SAR_M :
                        global::ATCSimulator.Client.Properties.Resources.SAR_S;
                    break;
                case "cardig air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.CardigAir_M :
                        global::ATCSimulator.Client.Properties.Resources.CardigAir_S;
                    break;
                case "republic express":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.RepExpress_M :
                        global::ATCSimulator.Client.Properties.Resources.RepExpress_S;
                    break;
                case "tri mg air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.TriMG_M :
                        global::ATCSimulator.Client.Properties.Resources.TriMG_S;
                    break;
                case "trigana air":
                    result = isLarge ? global::ATCSimulator.Client.Properties.Resources.Trigana_M :
                        global::ATCSimulator.Client.Properties.Resources.Trigana_S;
                    break;
            }
            return result;
        }
        #endregion

        #region Weather Default
        public class WeatherDefault
        {
            public int wind_direction { get; private set; }
            public int wind_speed { get; private set; }
            public int temperature { get; private set; }
            public int visibility { get; private set; }
            public WeatherDefault(SimulationService.WeatherInfo weather)
            {
                switch(weather)
                {
                    case SimulationService.WeatherInfo.Clear:
                        this.wind_direction = 0;
                        this.wind_speed = 4;
                        this.temperature = 30;
                        this.visibility = 99999;
                        break;
                    case SimulationService.WeatherInfo.Cloudy:
                        this.wind_direction =0;
                        this.wind_speed =6;
                        this.temperature = 28;
                        this.visibility = 80000;
                        break;
                    case SimulationService.WeatherInfo.LightRain:
                        this.wind_direction = 0;
                        this.wind_speed = 8;
                        this.temperature = 24;
                        this.visibility = 40000;
                        break;
                    case SimulationService.WeatherInfo.ThunderStorm:
                        this.wind_direction = 0;
                        this.wind_speed = 12;
                        this.temperature = 20;
                        this.visibility = 10000;
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region Flight Helper
        public static AircraftData ResetFlightStatus(AircraftData aircraft)
        {
            aircraft.pilot_properties.flight.circuit_approarch.instrument_approach = false;
            aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
            aircraft.pilot_properties.flight.direct_go.directGo = false;
            aircraft.pilot_properties.flight.downwind_orbit.downwind = false;
            aircraft.pilot_properties.flight.downwind_orbit.orbit = false;
            aircraft.pilot_properties.flight.holding.continue_direction = false;
            aircraft.pilot_properties.flight.holding.continue_route = false;
            aircraft.pilot_properties.flight.holding.holding_left = false;
            aircraft.pilot_properties.flight.holding.holding_right = false;
            aircraft.pilot_properties.flight.holding.holdingAt = false;
            aircraft.pilot_properties.flight.heading.absolute_heading = false;
            aircraft.pilot_properties.flight.heading.relative_heading = false;
            aircraft.pilot_properties.flight.other.abort_takeoff = false;
            aircraft.pilot_properties.flight.other.landing = false;
            aircraft.pilot_properties.flight.other.missed_approach = false;
            aircraft.pilot_properties.flight.other.touch_go = false;
            aircraft.pilot_properties.flight.other.flypass = false;
            aircraft.pilot_properties.flight.other.return_base = false;
            return aircraft;
        }
        #endregion

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }

}
