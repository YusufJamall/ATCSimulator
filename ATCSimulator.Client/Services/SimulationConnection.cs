﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ATCSimulator.Client.SimulationService;
using System.Threading;
using ATCSimulator.Client.Helper;
using System.Reflection;

namespace ATCSimulator.Client.Services
{
    [CallbackBehavior(
       ConcurrencyMode = ConcurrencyMode.Multiple,
       UseSynchronizationContext = false)]
    public class SimulationConnection : SimulationServiceCallback
    {
        private SynchronizationContext _uiSyncContext = null;
        private SimulationServiceClient _atcService = null;
        private static Helper.UserComputerInfo user_info;
        public static bool isReady = false;
        public static bool stillPlay = false;
        public static SimulationConnection Instance { get; private set; }

        public SimulationConnection()
        {
            Instance = this;
            SetProperties();
        }

        private void SetProperties()
        {
            user_info = HelperClient.GetComputerInfo();
            Instance = this;
        }

        #region Open Connection
        private bool OpenConnection()
        {
            try
            {
                if (_atcService == null || !_atcService.State.ToString().Equals("Opened"))
                {
                    if (user_info == null)
                        SetProperties();
                    _uiSyncContext = SynchronizationContext.Current;
                    _atcService = new SimulationServiceClient(new InstanceContext(this), "TcpBinding1");
                    _atcService.Open();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        public SimulationConnectResponse Connect()
        {
            if (OpenConnection())
            {

            }
            try
            {
                SimulationConnectResponse result = new SimulationConnectResponse();
                result = _atcService.Connect(new SimulationService.UserComputerInfo()
                {
                    ip_address = user_info.ip_address,
                    pc_name = user_info.pc_name,
                    mac_address = user_info.mac_address,
                    role_pc = user_info.role_pc,
                    application = "System"
                });
                return result;
            }
            catch (Exception ex)
            {
                SimulationConnectResponse result = new SimulationConnectResponse();
                result.status = new ResponseData
                {
                    code = 500,
                    message = "Internal Server Error",
                    description = "Something went wrong, error: " + ex.Message
                };
                return result;
            }
        }
        public static StatusResponse SimulationPlay(PlaySimulationContract data)
        {
            if (Instance.OpenConnection())
            {

            }
            try
            {
                StatusResponse result = new StatusResponse();
                result = Instance._atcService.SimulationPlay(data);
                return result;
            }
            catch (Exception ex)
            {
                StatusResponse result = new StatusResponse();
                result.status = new ResponseData
                {
                    code = 500,
                    message = "Internal Server Error",
                    description = "Something went wrong, error: " + ex.Message
                };
                return result;
            }
        }
        public static void StartSimulation()
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.StartSimulation();
            }
        }
        public static void PauseSimulation(bool value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.PauseSimulation(value);
            }
        }
        public static void StopSimulation()
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.StopSimulation();
            }
        }

        public static bool ContinueSimulation()
        {
            bool result = false;
            if (Instance.OpenConnection())
            {
                result = Instance._atcService.ContinueSimulation(new SimulationService.UserComputerInfo()
                {
                    ip_address = user_info.ip_address,
                    pc_name = user_info.pc_name,
                    mac_address = user_info.mac_address,
                    role_pc = user_info.role_pc,
                    application = "System"
                });
            }
            return result;
        }

        #region Request Service

        #region Aircraft Command

        #region Ground
        public static void ChangeGroundStatus(string callsign, int status)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeGroundStatus(callsign, status);
            }

        }
        public static void ChangeFlightRoute(string callsign, FlightRouteListModel routes, string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeFlightRoute(callsign, routes, runway);
            }

        }
        public static void ChangeParkingAppron(string callsign, DataAppronModel terminal)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeParkingAppron(callsign, terminal);
            }

        }
        public static void ReadyAircraft(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ReadyAircraft(callsign);
            }

        }
        public static void Pushback(string callsign, DirectionInfo pushback)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Pushback(callsign, pushback);
            }

        }
        public static void StartEnigne(string callsign, bool engine)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.StartEnigne(callsign, engine);
            }

        }

        public static void ChangeSpeedTaxi(string callsign, GroundSpeed speed)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeSpeedTaxi(callsign, speed);
            }

        }
        public static void StartTaxi(string callsign, List<string> routes, string target, int status)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.StartTaxi(callsign, routes, target, status);
            }
        }

        public static void HoldTaxi(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.HoldTaxi(callsign);
            }

        }

        public static void ContinueTaxi(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ContinueTaxi(callsign);
            }

        }

        public static void CrossRunway(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.CrossRunway(callsign);
            }

        }

        public static void Uturn(string callsign, UturnInfo uturn)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Uturn(callsign, uturn);
            }

        }

        public static void RockingWing(string callsign, bool value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.RockingWing(callsign, value);
            }

        }

        public static void TakeOff(string callsign, VisualFlightRouteModel routes, string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.TakeOff(callsign, routes, runway);
            }

        }

        public static void RunwayHold(string callsign, VisualFlightRouteModel routes, string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.RunwayHold(callsign, routes, runway);
            }

        }

        public static void PKPPK(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.PKPPK(callsign);
            }

        }

        #endregion

        #region Flight

        public static void ActiveAircraft(string callsign, FlightRouteListModel routes)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ActiveAircraft(callsign, routes);
            }

        }
        public static void HoldingNow(string callsign, DirectionInfo direction)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.HoldingNow(callsign, direction);
            }
        }

        public static void HoldingPosition(string callsign, string position)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.HoldingPosition(callsign, position);
            }
        }

        public static void ContinueRoute(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ContinueRoute(callsign);
            }
        }

        public static void ContinueDirection(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ContinueDirection(callsign);
            }
        }
        public static void ChangeHeading(string callsign, HeadingInfo direction, float heading)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeHeading(callsign, direction, heading);
            }
        }

        public static void DirectGo(string callsign, string position, bool isILS)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.DirectGo(callsign, position, isILS);
            }
        }

        public static void ChangeRoute(string callsign, FlightRouteListModel routes)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeRoute(callsign, routes);
            }
        }

        public static void Altitude(string callsign, float altitude)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Altitude(callsign, altitude);
            }
        }

        public static void Speed(string callsign, float speed)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Speed(callsign, speed);
            }
        }

        public static void Approach(string callsign, string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Approach(callsign, runway);
            }
        }

        public static void Circuit(string callsign, string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Circuit(callsign, runway);
            }
        }

        public static void MissedApproach(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.MissedApproach(callsign);
            }
        }

        public static void TouchGo(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.TouchGo(callsign);
            }
        }

        public static void Flypass(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Flypass(callsign);
            }
        }

        public static void FlightRockingWing(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.FlightRockingWing(callsign);
            }
        }

        public static void ExtendDownwind(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ExtendDownwind(callsign);
            }
        }

        public static void Orbit(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Orbit(callsign);
            }
        }

        public static void Landing(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.Landing(callsign);
            }
        }
        public static void ILSLanding(string callsign, string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ILSLanding(callsign, runway);
            }
        }
        public static void ReturnBase(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ReturnBase(callsign);
            }
        }

        public static void AbortedTakeOff(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.AbortedTakeOff(callsign);
            }
        }

        public static void ChangeRunway(string callsign, string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeRunway(callsign, runway);
            }
        }

        public static void ChangeILSMode(string callsign, bool value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeILSMode(callsign, value);
            }
        }

        #endregion

        #endregion

        #region Instructor Command

        #region Aircraft

        public static void SelectAircraft(string callsign, string SendToTarget, string pc_sender)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.SelectAircraft(callsign, SendToTarget, pc_sender);
            }

        }

        public static void RemoveAircraft(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.RemoveAircraft(callsign);
            }

        }
        public static void LandingGearJam(string callsign)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.LandingGearJam(callsign);
            }

        }
        public static void EngineFailure(string callsign, int engine)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.EngineFailure(callsign, engine);
            }

        }
        public static void ChangeFuel(string callsign, TimeSpan fuel)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeFuel(callsign, fuel);
            }

        }
        public static void IncidentBird(string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.IncidentBird(runway);
            }

        }
        public static void IncidentAnimal(string runway)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.IncidentAnimal(runway);
            }

        }
        #endregion

        #region Environment
        public static void ChangeWeather(WeatherInfo weather)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeWeather(weather);
            }

        }
        public static void ChangeWindDirection(float value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeWindDirection(value);
            }

        }
        public static void ChangeWindSpeed(float value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeWindSpeed(value);
            }

        }

        public static void ChangeTemperature(float value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeTemperature(value);
            }

        }

        public static void ChangeVisibility(float value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeVisibility(value);
            }

        }
        public static void ChangeTimeSimulation(TimeSpan time)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.ChangeTimeSimulation(time);
            }

        }
        #endregion

        #region Light
        public static void LightRunway(string runway, bool value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.LightRunway(runway, value);
            }

        }
        public static void LightApproach(string approach, bool value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.LightApproach(approach, value);
            }

        }
        public static void LightTaxi(bool value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.LightTaxi(value);
            }

        }

        public static void LightPapi(bool value)
        {
            if (Instance.OpenConnection())
            {
                Instance._atcService.LightPapi(value);
            }

        }
        #endregion

        #endregion

        #endregion

        #region Callback
        public void StartSimulationCallback(ExerciseSimulationModel exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_route, List<DataAppronModel> aapprons)
        {
            stillPlay = true;
            UI.MainForm.PlaySimulationThread(exercise, aircrafts, flight_route, aapprons);
        }
        public void ReadySimulationCallback()
        {
            UI.Simulation.SimulationMainForm.Instance.ReadySimulationCallback();
        }
        public void UpdateTimeSimulation(TimeSpan time)
        {
            try
            {
                UI.Simulation.SimulationMainForm.Instance.UpdateTimeSimulation(time);
            }
            catch { }
        }
        public void PauseSimulationCallback(bool value)
        {
            UI.Simulation.SimulationMainForm.Instance.PauseSimulationCallback(value);
        }
        public void StopSimulationCallback()
        {
            isReady = false;
            stillPlay = false;
            UI.MainForm.StopSimulationThread();
            UI.Simulation.SimulationMainForm.Instance.StopSimulationCallback();
        }
        public void ContinueSimulationCallback(ExerciseSimulationModel exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_route, List<DataAppronModel> aapprons, int userId, bool isPaused)
        {

            if (stillPlay)
            {
                UI.MainForm.ContinueSimulationThread(exercise, aircrafts, flight_route, aapprons, isPaused, false);
                isReady = true;
            }
            else
            {
                UI.InfoForm.ContinueSimulationThread(exercise, aircrafts, flight_route, aapprons, userId, isPaused);
            }
        }

        #region Ground Command

        public void ChangeGroundStatusCallback(string callsign, int status)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeGroundStatusCallback(callsign, status);
        }
        public void ChangeFlightRouteCallback(string callsign, FlightRouteListModel routes, string runway)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeFlightRouteCallback(callsign, routes, runway);
        }

        public void ChangeParkingAppronCallback(string callsign, DataAppronModel terminal)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeParkingAppronCallback(callsign, terminal);
        }
        public void ReadyAircraftCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ReadyAircraftCallback(callsign);
        }

        public void ChangeRunwayCallback(string callsign, string runway, bool is_ground)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeRunwayCallback(callsign, runway, is_ground);
        }

        public void StartEnigneCallback(string callsign, bool engine)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.StartEngineCallback(callsign, engine);
        }

        public void PushbackCallback(string callsign, DirectionInfo pushback, string pushback_name)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.PushbackCallback(callsign, pushback, pushback_name);
        }

        public void StartTaxiCallback(string callsign, List<string> routes, int status)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.StartTaxiCallback(callsign, routes, status);
        }

        public void HoldTaxiCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.HoldTaxiCallback(callsign);
        }

        public void ContinueTaxiCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ContinueTaxiCallback(callsign);
        }

        public void ChangeSpeedTaxiCallback(string callsign, GroundSpeed speed)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeSpeedTaxiCallback(callsign, speed);
        }

        public void CrossRunwayCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.CrossRunwayCallback(callsign);
        }

        public void UturnCallback(string callsign, UturnInfo uturn)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.UturnCallback(callsign, uturn);
        }

        public void RockingWingCallback(string callsign, bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.RockingWingCallback(callsign, value);
        }

        public void TakeOffCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.TakeOffCallback(callsign);
        }

        public void RunwayHoldCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.RunwayHoldCallback(callsign);
        }
        public void AbortTakeOffCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.AbortTakeOffCallback(callsign);
        }
        #endregion

        #region Flight Command

        public void ActiveAircraftCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ActiveAircraftCallback(callsign);
        }
        public void HoldingNowCallback(string callsign, DirectionInfo direction)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.HoldingNowCallback(callsign, direction);
        }

        public void HoldingPositionCallback(string callsign, string position)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.HoldingPositionCallback(callsign, position);
        }

        public void ContinueRouteCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ContinueRouteCallback(callsign);
        }

        public void ContinueDirectionCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ContinueDirectionCallback(callsign);
        }

        public void ChangeHeadingCallback(string callsign, HeadingInfo direction, float heading)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeHeadingCallback(callsign, direction, heading);
        }

        public void ChangeRouteCallback(string callsign, FlightRouteListModel routes)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeRouteCallback(callsign, routes);
        }

        public void DirectGoCallback(string callsign, string position, bool isILS)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.DirectGoCallback(callsign, position, isILS);
        }
        public void ApproachCallback(string callsign, string runway)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ApproachCallback(callsign, runway);
        }

        public void CircuitCallback(string callsign, string runway)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.CircuitCallback(callsign, runway);
        }
        public void MissedApproachCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.MissedApproachCallback(callsign);
        }

        public void TouchGoCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.TouchGoCallback(callsign);
        }

        public void FlypassCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FlypassCallback(callsign);
        }

        public void ExtendDownwindCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ExtendDownwindCallback(callsign);
        }

        public void OrbitCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.OrbitCallback(callsign);
        }

        public void LandingCallback(string callsign, string runway)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.LandingCallback(callsign, runway);
        }
        public void AltitudeCallback(string callsign, float altitude)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.AltitudeCallback(callsign, altitude);
        }

        public void SpeedCallback(string callsign, float speed)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.SpeedCallback(callsign, speed);
        }
        public void ChangeILSModeCallback(string callsign, bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeILSModeCallback(callsign, value);
        }
        #endregion

        #region Ground Callback
        public void FinishPushback(string callsign, double heading)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishPushback(callsign, heading);
        }
        public void FinishReadyDeparture(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishReadyDeparture(callsign);
        }
        public void UpdateTaxiPosition(string callsign, string position, double heading)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.UpdateTaxiPosition(callsign, position, heading);
        }

        public void FinishTaxi(string callsign, string position, double heading, int status)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishTaxi(callsign, position, heading, status);
        }

        public void HeadingAircraft(string callsign, double heading)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.HeadingAircraft(callsign, heading);
        }

        public void RequestCrossRunway(string callsign, double heading)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.RequestCrossRunway(callsign, heading);
        }

        public void FinishUTurn(string callsign, double heading)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishUTurn(callsign, heading);
        }

        public void FinishHoldTakeOff(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishHoldTakeOff(callsign);
        }

        public void FinishTakeOff(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishTakeOff(callsign);
        }

        public void CrashAircraft(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.CrashAircraft(callsign);
        }
        public void FinishParking(AircraftData aircraft)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishParking(aircraft);
        }
        public void Backtrack(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.Backtrack(callsign);
        }
        #endregion

        #region Flight Callback
        public void CannotChangeSpeed(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.CannotChangeSpeed(callsign);
        }

        public void CannotChangeAltitude(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.CannotChangeAltitude(callsign);
        }
        public void UpdatePerformance(string callsign, float speed, float altitude)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.UpdatePerformanceCallback(callsign, speed, altitude);
        }
        public void UpdateHoldingPoint(List<string> holding)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.UpdateHoldingPoint(holding);
        }
        public void CannotHolding(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.CannotHolding(callsign);
        }
        public void HoldingAtCallback(string callsign, string position, bool isInstrument)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.HoldingAtCallback(callsign, position, isInstrument);
        }
        public void RouteUpdateCallback(string callsign, FlightRouteListModel route)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.RouteUpdateCallback(callsign, route);
        }

        public void ReadyJoinCircuit(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ReadyJoinCircuit(callsign);
        }

        public void JoinCircuitCallback(string callsign, string runway)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.JoinCircuitCallback(callsign, runway);
        }
        public void StartApproach(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.StartApproach(callsign);
        }

        public void StartGoRound(string callsign, bool isFlypass, bool isIfr)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.StartGoRound(callsign, isFlypass, isIfr);
        }

        public void FinishGoRound(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishGoRound(callsign);
        }
        public void CannotLanding(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.CannotLanding(callsign);
        }
        public void StartLanding(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.StartLanding(callsign);
        }
        public void TouchGround(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.TouchGround(callsign);
        }
        public void FinishLanding(string callsign, float heading, string from, string position)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.FinishLandingCallback(callsign, heading, from, position);
        }
        #endregion

        #region InstructorMenu
        public void RemoveAircraftCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.RemoveAircraft(callsign);
        }
        public void IncidentAnimalCallback(bool value, string runway)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.IncidentAnimal(value, runway);
        }

        public void PKPPKCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.PKPPK(callsign);
        }

        public void BirdAttackCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.BirdAttack(callsign);
        }

        public void LandingGearJamCallback(string callsign)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.LandingGearJam(callsign);
        }

        public void EngineFailureCallback(string callsign, int engine)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.EngineFailureCallback(callsign, engine);
        }

        public void ChangeFuelCallback(string callsign, TimeSpan fuel)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeFuelCallback(callsign, fuel);
        }
        public void BirdAttackValue(string callsign, bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.BirdAttackValue(callsign, value);
        }
        public void LandingGearJamValue(string callsign, bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.LandingGearJamValue(callsign, value);
        }
        public void ChangeWeatherCallback(WeatherInfo weather)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeWeatherCallback(weather);
        }

        public void ChangeWindDirectionCallback(float value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeWindDirectionCallback(value);
        }

        public void ChangeWindSpeedCallback(float value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeWindSpeedCallback(value);
        }

        public void ChangeTemperatureCallback(float value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeTemperatureCallback(value);
        }

        public void ChangeVisibilityCallback(float value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeVisibilityCallback(value);
        }

        public void ChangeTimeSimulationCallback(TimeSpan time)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ChangeTimeSimulationCallback(time);
        }
        public void TaxiLightCallback(bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.TaxiLightCallback(value);
        }

        public void PapiLightCallback(bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.PapiLightCallback(value);
        }

        public void RunwayLightCallback(string runway, bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.RunwayLightCallback(runway, value);
        }

        public void ApproachLightCallback(string runway, bool value)
        {
            if (isReady)
                UI.Simulation.SimulationMainForm.Instance.ApproachLightCallback(runway, value);
        }
        #endregion

        #endregion
    }
}
