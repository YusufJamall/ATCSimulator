﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ATCSimulator.Client.ATCService;
using ATCSimulator.Client.Helper;
using System.ServiceModel;
using System.Reflection;

namespace ATCSimulator.Client.Services
{
    [CallbackBehavior(
       ConcurrencyMode = ConcurrencyMode.Multiple,
       UseSynchronizationContext = false)]
    public class Connection : ATCServiceCallback
    {
        private SynchronizationContext _uiSyncContext = null;
        private ATCServiceClient _atcService = null;
        private UserComputerInfo user_info;
        public static Connection Instance { get; private set; }

        public Connection()
        {
            //server = server_Form;
            Instance = this;
            SetProperties();
        }
        private void SetProperties()
        {
            user_info = HelperClient.GetComputerInfo();
            Instance = this;
        }
        #region Connect Wrapper
        public T ConnectionWrapper<T>(Func<T> f) where T : class, new()
        {
            T returnData = new T();
            Type type = Type.GetType(returnData.ToString(), true);
            PropertyInfo prop = type.GetProperty("status");
            ResponseData status = new ResponseData();
            try
            {
                if (OpenConnection())
                {
                    returnData = f();
                }
                else
                {
                    status = ServiceResponses.InternalServerError;
                    prop.SetValue(returnData, status, null);
                }
            }
            catch (Exception ex)
            {
                status = ServiceResponses.InternalServerError;
                prop.SetValue(returnData, status, null);
            }
            return returnData;
        }
        #endregion

        #region Open Connection
        private bool OpenConnection()
        {
            try
            {
                if (_atcService == null || !_atcService.State.ToString().Equals("Opened"))
                {
                    if (user_info == null)
                        SetProperties();
                    _uiSyncContext = SynchronizationContext.Current;
                    _atcService = new ATCServiceClient(new InstanceContext(this), "TcpBinding");
                    _atcService.Open();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region PC Service
        #region Connect PC
        public StatusResponse ConnectPC()
        {
            return ConnectionWrapper(delegate()
            {
                StatusResponse result = new StatusResponse();
                result.status = ServiceResponses.InternalServerError;
                result.status.message = "couldn't connect to server";
                result = _atcService.ConnectPC(new ConnectPCContract()
                {
                    ip_address = user_info.ip_address,
                    pc_name = user_info.pc_name,
                    mac_address = user_info.mac_address,
                    role_pc = user_info.role_pc
                });
                return result;
            });
        }
        #endregion

        #region ListPC
        public static ListPCResponse ListPC()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ListPC();
            });
        }
        #endregion

        #endregion

        #region User Service

        #region UserLogin
        public UserLoginResponse UserLogin(string username, string password)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserLogin(new UserLoginContract()
                {
                    ip_address = Instance.user_info.ip_address,
                    password = password,
                    username = username
                });
            });
        }
        #endregion

        #region UserRemove
        public static StatusResponse UserRemove(int user_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserRemove(user_id);
            });
        }
        #endregion

        #region UserRegister
        public static StatusResponse UserRegister(UserRegisterContract user)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserRegister(user);
            });
        }
        #endregion

        #region UserUpdate
        public static StatusResponse UserUpdate(UserUpdateContract user)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserUpdate(user);
            });
        }
        #endregion

        #region UserList
        public static UserListResponse UserList(int page, int offset, int type)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserList(page, offset, type);
            });
        }
        #endregion

        #region UserDetail
        public static UserDetailResponse UserDetail(int user_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserDetail(user_id);
            });
        }
        #endregion

        #region UserLogout
        public static StatusResponse UserLogout(int user_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserLogout(user_id);
            });
        }
        #endregion

        #region UserRemoteLogin
        public static UserDetailResponse UserRemoteLogin(UserRemoteContract user)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserRemoteLogin(user);
            });
        }
        #endregion

        #region UserDetail
        public static UserStudentSimulationResponse UserStudentSimulation()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserStudentSimulation();
            });
        }
        #endregion

        #region Get User Roles
        public static UserRolesResponse GetUserRoles()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.GetUserRoles();
            });
        }
        #endregion

        #endregion

        #region Screnery Service

        #region SceneryList
        public static SceneryListResponse SceneryList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SceneryList();
            });
        }

        #endregion

        #region SceneryDetail
        public static SceneryDetailResponse SceneryDetail(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SceneryDetail(scenery_id);
            });
        }
        #endregion

        #endregion

        #region Exercise Service

        #region Exercise List
        public static ExerciseListResponse ExerciseList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseList();
            });
        }
        #endregion

        #region Exercise Add
        public static StatusResponse ExerciseAdd(ExerciseAddContract exercise)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseAdd(exercise);
            });
        }
        #endregion

        #region Exercise Update
        public static StatusResponse ExerciseUpdate(ExerciseUpdateContract exercise)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseUpdate(exercise);
            });
        }
        #endregion

        #region Exercise Detail
        public static ExerciseDetailResponse ExerciseDetail(int exercise_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseDetail(exercise_id);
            });
        }
        #endregion

        #region Exercise Remove
        public static StatusResponse ExerciseRemove(int exercise_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseRemove(exercise_id);
            });
        }
        #endregion

        #endregion

        #region Aircraft Service
        public static AircraftDetailResponse AircraftDetail(int aircraft_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AircraftDetail(aircraft_id);
            });
        }
        public static AircraftListResponse AircraftList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AircraftList();
            });
        }
        #endregion

        #region Flight Rule Service
        public static FlightRuleListResponse FlightRuleList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.FlightRuleList();
            });
        }
        #endregion

        #region Flight Type Service
        public static FlightTypeListResponse FlightTypeList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.FlightTypeList();
            });
        }
        #endregion

        #region Livery Service
        public static LiveryDetailResponse LiveryDetail(int id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.LiveryDetail(id);
            });
        }

        public static LiveryListResponse LiveryList(int aircraft_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.LiveryList(aircraft_id);
            });
        }
        #endregion

        #region Airport Service
        public static AirportAppronListResponse AirportAppronList(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AirportAppronList(scenery_id);
            });
        }

        public static AirportListResponse AirportList(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AirportList(scenery_id);
            });
        }
        #endregion

        #region Flight Route Service
        public static FlightRouteListResponse FlightRouteList(bool is_departure, int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                FlightRouteListContract request = new FlightRouteListContract()
                {
                    is_departure = is_departure,
                    scenery_id = scenery_id
                };
                return Instance._atcService.FlightRouteList(request);
            });
        }
        #endregion

        #region Simulation Process
        public static StatusResponse SimulationPlay(SimulationContract data)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SimulationPlay(data);
            });
        }
        /*
        public static StatusResponse StartEngine(string callsign, bool engine)
        {
            return Instance._atcService.StartEngine(callsign, engine);
        }
        public static StatusResponse ChangeRunwayUsed(string callsign, string runway)
        {
            return Instance._atcService.ChangeRunwayUsed(callsign, runway);
        }
        public static StatusResponse AircraftPushback(string callsign, string pushback)
        {
            return Instance._atcService.AircraftPushback(callsign, pushback);
        }
        public static StatusResponse AircraftStartTaxi(string callsign,List<string> routes_name,List<int> routes_id)
        {
            TaxiRouteContract contract = new TaxiRouteContract()
            {
                callsign = callsign,
                routes_id = routes_id.ToArray(),
                routes_name = routes_name.ToArray()
            };
            return Instance._atcService.AircraftTaxi(contract);
        }
        */
        #endregion

        #region Callback Service
        public void RemoteLogin(UserProfileModel user)
        {
            throw new NotImplementedException();
        }

        public void StartSimulation(ExerciseDetailModel exercise, List<AircraftData> aircrafts)
        {
            throw new NotImplementedException();
        }

        public void ActiveAircraft(string callsign)
        {
            throw new NotImplementedException();
        }

        public void FinishReadyAircraft(string callsign)
        {
            throw new NotImplementedException();
        }

        public void ReadyDeparture(string callsign)
        {
            throw new NotImplementedException();
        }

        public void FinishAircraftPushback(string callsign)
        {
            throw new NotImplementedException();
        }

        public void UpdatePositionTaxing(string callsign, string position)
        {
            throw new NotImplementedException();
        }

        public void FinishTaxing(string callsign)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
