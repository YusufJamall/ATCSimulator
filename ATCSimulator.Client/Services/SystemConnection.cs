﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ATCSimulator.Client.SystemService;
using System.Threading;
using ATCSimulator.Client.Helper;
using System.Reflection;

namespace ATCSimulator.Client.Services
{
    [CallbackBehavior(
      ConcurrencyMode = ConcurrencyMode.Multiple,
      UseSynchronizationContext = false)]
    public class SystemConnection : SystemServiceCallback
    {
        private SynchronizationContext _uiSyncContext = null;
        private SystemServiceClient _atcService = null;
        private ATCSimulator.Client.Helper.UserComputerInfo user_info;
        public static SystemConnection Instance { get; private set; }

        public SystemConnection()
        {
            Instance = this;
            SetProperties();
        }

        private void SetProperties()
        {
            user_info = HelperClient.GetComputerInfo();
            Instance = this;
        }

        #region Connect Wrapper
        public T ConnectionWrapper<T>(Func<T> f) where T : class, new()
        {
            T returnData = new T();
            Type type = Type.GetType(returnData.ToString(), true);
            PropertyInfo prop = type.GetProperty("status");
            ResponseData status = new ResponseData();
            try
            {
                if (OpenConnection())
                {
                    returnData = f();
                }
            }
            catch (Exception ex)
            {
                status = ServiceResponses.InternalServerError;
                prop.SetValue(returnData, status, null);
            }
            return returnData;
        }
        #endregion

        #region Open Connection
        private bool OpenConnection()
        {
            try
            {
                if (_atcService == null || !_atcService.State.ToString().Equals("Opened"))
                {
                    if (user_info == null)
                        SetProperties();
                    _uiSyncContext = SynchronizationContext.Current;
                    _atcService = new SystemServiceClient(new InstanceContext(this), "TcpBinding");
                    _atcService.Open();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region PC Service
        #region Connect PC
        public StatusResponse ConnectPC()
        {
            if (OpenConnection())
            {

            }

            return ConnectionWrapper(delegate()
            {
                StatusResponse result = new StatusResponse();
                result.status = ServiceResponses.InternalServerError;
                result.status.message = "couldn't connect to server";
                result = _atcService.ConnectPC(new PCContract()
                {
                    ip_address = user_info.ip_address,
                    pc_name = user_info.pc_name,
                    mac_address = user_info.mac_address,
                    role_pc = user_info.role_pc
                });
                return result;
            });
        }
        #endregion

        #region DetailPC
        public static DetailPCResponse DetailPC(int pc_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.DetailPC(pc_id);
            });
        }
        #endregion

        #region ListPC
        public static ListPCResponse ListPC()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ListPC();
            });
        }
        #endregion

        #region DisconnectPC
        public static StatusResponse DisconnectPC()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.DisconnectPC(new DisconnectPCContract()
                {
                    mac_address = Instance.user_info.mac_address,
                    role_pc = Instance.user_info.role_pc
                });
            });
        }
        #endregion

        #region ShutdownRestartPC
        public static StatusResponse ShutdownRestartPC(ShutdownRestartPCContract pc)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ShutdownRestartPC(pc);
            });
        }
        #endregion

        #region Ping Service
        public static bool Ping()
        {
            try
            {
                return Instance._atcService.Ping();
            }catch
            {
                return false;
            }
        }
        #endregion

        #endregion

        #region User Service

        #region UserLogin
        public static UserLoginResponse UserLogin(string username, string password)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserLogin(new UserLoginContract()
                {
                    ip_address = Instance.user_info.ip_address,
                    pc_role = Instance.user_info.role_pc,
                    password = password,
                    username = username
                });
            });
        }
        #endregion

        #region UserRemove
        public static StatusResponse UserRemove(int user_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserRemove(user_id);
            });
        }
        #endregion

        #region UserRegister
        public static StatusResponse UserRegister(UserRegisterContract user)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserRegister(user);
            });
        }
        #endregion

        #region UserUpdate
        public static StatusResponse UserUpdate(UserUpdateContract user)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserUpdate(user);
            });
        }
        #endregion

        #region UserList
        public static UserListResponse UserList(int page, int offset, int type)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserList(page, offset, type);
            });
        }
        #endregion

        #region UserDetail
        public static UserDetailResponse UserDetail(int user_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserDetail(user_id);
            });
        }
        #endregion

        #region Get Roles
        public static UserRolesResponse GetUserRoles()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.GetUserRoles();
            });
        }
        #endregion

        #region UserLogout
        public static StatusResponse UserLogout(int user_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserLogout(user_id, Instance.user_info.role_pc);
            });
        }
        #endregion

        #region UserRemoteLogin
        public static UserDetailResponse UserRemoteLogin(UserRemoteContract user)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserRemoteLogin(user);
            });
        }
        #endregion

        #region UserDetail
        public static UserStudentSimulationResponse UserStudentSimulation()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.UserStudentSimulation();
            });
        }
        #endregion

        #region Reset Password
        public static StatusResponse ResetPassword(int user_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ResetPassword(user_id);
            });
        }
        #endregion

        #endregion

        #region Scenery Service

        #region SceneryAdd
        public static StatusResponse SceneryAdd(SceneryDetailContract scenery)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SceneryAdd(scenery);
            });
        }
        #endregion

        #region SceneryRemove
        public static StatusResponse SceneryRemove(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SceneryRemove(scenery_id);
            });
        }
        #endregion

        #region SceneryUpdate
        public static StatusResponse SceneryUpdate(SceneryUpdateContract scenery)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SceneryUpdate(scenery);
            });
        }
        #endregion

        #region SceneryDetail
        public static SceneryDetailResponse SceneryDetail(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SceneryDetail(scenery_id);
            });
        }
        #endregion

        #region SceneryList
        public static SceneryListResponse SceneryList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SceneryList();
            });
        }
        #endregion

        #endregion

        #region Exercise Service

        public static StatusResponse ExerciseAdd(ExerciseAddContract exercise)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseAdd(exercise);
            });
        }
        public static StatusResponse ExerciseUpdate(ExerciseUpdateContract exercise)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseUpdate(exercise);
            });
        }
        public static ExerciseDetailResponse ExerciseDetail(int exercise_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseDetail(exercise_id);
            });
        }
        public static StatusResponse ExerciseRemove(int exercise_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseRemove(exercise_id);
            });
        }
        public static ExerciseListResponse ExerciseList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.ExerciseList();
            });
        }
        #endregion

        #region History Simulation Service
        public static StatusResponse SimulationRemove(int simulation_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SimulationRemove(simulation_id);
            });
        }
        public static SimulationListResponse SimulationList(int page, int pageSize)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SimulationList(page, pageSize);
            });
        }
        public static AircraftHistoryListResponse AircraftHistoryList(int simulationID)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AircraftHistoryList(simulationID);
            });
        }
        public static PlayerListResponse PlayerList(int simulation_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.PlayerList(simulation_id);
            });
        }
        public static StatusResponse SetPoint(PointContract point)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.SetPoint(point);
            });
        }
        public static HistoryPlayerResponse GetHistoryPlayer(int userID)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.GetHistoryPlayer(userID);
            });
        }
        public static HistoryInstructorResponse GetHistoryInstructor(string instructorName)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.GetHistoryInstructor(instructorName);
            });
        }
        #endregion

        #region Aircraft Service
        public static AircraftDetailResponse AircraftDetail(int aircraft_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AircraftDetail(aircraft_id);
            });
        }
        public static AircraftListResponse AircraftList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AircraftList();
            });
        }
        #endregion

        #region Flight Rule Service
        public static FlightRuleListResponse FlightRuleList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.FlightRuleList();
            });
        }
        #endregion

        #region Flight Type Service
        public static FlightTypeListResponse FlightTypeList()
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.FlightTypeList();
            });
        }
        #endregion

        #region Livery Service
        public static LiveryDetailResponse LiveryDetail(int id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.LiveryDetail(id);
            });
        }

        public static LiveryListResponse LiveryList(int aircraft_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.LiveryList(aircraft_id);
            });
        }
        #endregion

        #region Airport Service
        public static AirportAppronListResponse AirportAppronList(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AirportAppronList(scenery_id);
            });
        }

        public static AirportListResponse AirportList(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.AirportList(scenery_id);
            });
        }
        public static RunwayListResponse RunwayList(int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.RunwayList(scenery_id);
            });
        }
        #endregion

        #region Flight Route Service
        public static FlightRouteListResponse FlightRouteList(bool is_departure, int scenery_id)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                FlightRouteListContract request = new FlightRouteListContract()
                {
                    is_departure = is_departure,
                    scenery_id = scenery_id
                };
                return Instance._atcService.FlightRouteList(request);
            });
        }
        #endregion

        #region Flight Route Detail Service
        public static FlightRouteDetailResponse FlightRouteDetail(int route_id, bool is_departure)
        {
            return Instance.ConnectionWrapper(delegate()
            {
                return Instance._atcService.FlightRouteDetail(route_id, is_departure);
            });
        }
        #endregion


        #region CallBack
        public void RemoteLogin(UserProfileModel user)
        {
            try
            {
                UI.LoginForm.RemoteLogin(user);
            }
            catch { }
        }
        public bool PingClient()
        {
            return true;
        }

        #region ShutdownRestart
        public void ShutdownRestart(string command)
        {
            HelperClient.ExitWindows.ShutdownRestart(command, false);
        }
        #endregion

        #region PC Connect
        public void PCConnect(ATCSimulator.Client.SystemService.UserComputerInfo user)
        {
            try
            {
                UI.MainForm.ConnectCallback(user);
            }
            catch { }
        }
        #endregion

        #region Connect Update
        public void ConnectUpdate()
        {
            try
            {
                UI.MainForm.ConnectUpdateCallback();
            }
            catch { }
        }
        #endregion

        #endregion


        
    }
}
