﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.UI;
using ATCSimulator.Client.Helper;

namespace ATCSimulator.Client.UI
{
    public partial class LoginForm : Telerik.WinControls.UI.RadForm
    {
        public static LoginForm Instance { get; private set; }
        public InfoForm infoForm;
        public LoginForm(InfoForm infoForm)
        {
            InitializeComponent();
            Instance = this;
            this.infoForm = infoForm;
            this.Location = new Point { X = Convert.ToInt32(ConfigurationManager.AppSettings["X_Position"].ToString()), Y = 0 };
        }

        private void Login_Button_Click(object sender, EventArgs e)
        {
            string username = Username.Text;
            string password = Password.Text;
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                try
                {
                    UserLoginResponse user = SystemConnection.UserLogin(username, password);
                    if (user.status.code == 0)//success
                    {
                        Login(user.user);
                        ResetForm();
                    }
                    else
                    {
                        MessageBox.Show(user.status.message);
                        ResetForm();
                    }
                }
                catch
                {
                    MessageBox.Show("Service Connection Error");
                    ResetForm();
                }
            }
            else
                MessageBox.Show("Username and Password must have value!");
        }
        public void ChildFormClosed(object sender, FormClosedEventArgs e)
        {
            StatusResponse disconnect = SystemConnection.DisconnectPC();
            Application.Exit();
        }
        public static void RemoteLogin(UserProfileModel user)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    Instance.Login(user);
                }));
            }
            else
            {
                Instance.Login(user);
            }
        }
        public void Login(UserProfileModel user)
        {
            infoForm.mainForm.Init(user, this);
            infoForm.mainForm.FormClosed += new FormClosedEventHandler(ChildFormClosed);
            infoForm.mainForm.Show();
            this.Hide();
        }
        public void ContinueSimulation(UserProfileModel user, SimulationService.ExerciseSimulationModel exercise, List<SimulationService.AircraftData> aircrafts, List<SimulationService.FlightRouteListModel> flight_routes, List<SimulationService.DataAppronModel> aapprons, bool isPaused, bool isNew)
        {
            infoForm.mainForm.Init(user, this);
            infoForm.mainForm.FormClosed += new FormClosedEventHandler(ChildFormClosed);
            infoForm.mainForm.Show();
            infoForm.mainForm.ContinueSimulation(exercise, aircrafts, flight_routes, aapprons, isPaused, true);
            SimulationConnection.isReady = true;
        }
        public void FailedContinue()
        {
            MessageBox.Show("Failed to Continue");
            ResetForm();
            this.Show();
        }

        private void ResetForm()
        {
            Username.Text = "";
            Password.Text = "";
            Username.Select();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
