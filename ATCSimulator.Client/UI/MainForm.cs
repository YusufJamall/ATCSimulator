﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.UI.Simulation;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.UI.Profile;
using ATCSimulator.Client.UI.Instructor;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using System.Configuration;


namespace ATCSimulator.Client.UI
{
    public partial class MainForm : Telerik.WinControls.UI.RadForm
    {
        enum ViewMenu
        {
            None,
            Dashboard,
            Instructor,
            Profile,
            Help
        }
        Dictionary<Label, bool> header_button;
        List<Label> list_header;
        public static MainForm Instance { get; private set; }
        public InfoForm infoForm;

        DashboardForm dashboard_form;
        InstructorMainForm instructor_form;
        LoginForm login_form;
        ProfileMainForm profile_user;

        ViewMenu menu_panel = ViewMenu.None;
        string pc_role;
        private UserProfileModel user;
        public MainForm(InfoForm infoForm)
        {
            InitializeComponent();
            Instance = this;
            this.infoForm = infoForm;
            this.Location = new Point { X = Convert.ToInt32(ConfigurationManager.AppSettings["X_Position"].ToString()), Y = 0 };
            
        }
        public void Init(UserProfileModel _user, LoginForm login_form)
        {
            this.user = _user;
            this.login_form = login_form;
        }
        public void ChangeProfile(UserProfileModel _user)
        {
            this.user = _user;
            if (user.photo != null)
                user_picture.Image = HelperClient.byteArrayToImage(_user.photo);
            fullname_label.Text = _user.first_name + " " + _user.last_name;
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            ChangeProfile(user);
            pc_role = ConfigurationManager.AppSettings["PCRole"].ToString();
            role_label.Text = ConfigurationManager.AppSettings["PCRole"].ToString();
            header_button = new Dictionary<Label, bool>();

            header_button.Add(ProfileBtn, false);
            header_button.Add(HelpBtn, false);

            list_header = new List<Label>()
            {
                ProfileBtn,
                HelpBtn
            };
            profile_user = new ProfileMainForm();
            profile_user.TopLevel = false;
            main_panel.Controls.Add(profile_user);

            dashboard_form = new DashboardForm();
            dashboard_form.TopLevel = false;
            main_panel.Controls.Add(dashboard_form);

            CheckRoles(pc_role);
            ShowViewMenu(ViewMenu.Dashboard);
        }

        private void CheckRoles(string pc_role)
        {
            UserRoleModel role = user.role;
            InstructorBtn.Visible = false;
            if (pc_role.Equals("Instructor"))
            {
                list_header.Add(InstructorBtn);
                header_button.Add(InstructorBtn, false);
                instructor_form = new InstructorMainForm(user,this);
                instructor_form.TopLevel = false;
                main_panel.Controls.Add(instructor_form);
                InstructorBtn.Visible = true;
            }
        }

        #region Button Handler
        private void ShowViewMenu(ViewMenu menu)
        {
            switch (menu)
            {
                case ViewMenu.Dashboard:
                    if (menu_panel != ViewMenu.Dashboard)
                    {
                        HideViewMenu(menu_panel);
                        dashboard_form.Show();
                        dashboard_form.Dock = DockStyle.Top;
                        dashboard_form.BringToFront();
                        menu_panel = ViewMenu.Dashboard;
                    }
                    break;
                case ViewMenu.Instructor:
                    if (menu_panel != ViewMenu.Instructor)
                    {
                        HideViewMenu(menu_panel);
                        instructor_form.Show();
                        instructor_form.Init();
                        instructor_form.Dock = DockStyle.Top;
                        instructor_form.BringToFront();
                        menu_panel = ViewMenu.Instructor;
                    }
                    break;
                case ViewMenu.Profile:
                    if (menu_panel != ViewMenu.Profile)
                    {
                        HideViewMenu(menu_panel);
                        profile_user.Show();
                        profile_user.Init(user);
                        profile_user.Dock = DockStyle.Top;
                        profile_user.BringToFront();
                        menu_panel = ViewMenu.Profile;
                    }
                    break;
            }
        }
        private void HideViewMenu(ViewMenu menu)
        {
            switch (menu)
            {
                case ViewMenu.Dashboard:
                    dashboard_form.Hide();
                    dashboard_form.SendToBack();
                    break;
                case ViewMenu.Instructor:
                    instructor_form.Hide();
                    instructor_form.SendToBack();
                    break;
                case ViewMenu.Profile:
                    profile_user.Hide();
                    profile_user.SendToBack();
                    break;
            }
        }

        private void SetMainButton(Label label)
        {
            foreach (Label p in list_header)
            {
                if (p != label)
                {
                    header_button[p] = false;
                    p.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    header_button[p] = true;
                    p.ForeColor = System.Drawing.Color.DodgerBlue;
                }
            }
        }
        private void ButtonOnHover(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.ForeColor = System.Drawing.Color.DodgerBlue;
        }
        private void SignOutOnLeave(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.ForeColor = System.Drawing.Color.White;
        }

        private void SignOut(object sender, EventArgs e)
        {
            StatusResponse result = SystemConnection.UserLogout(user.ID);
            if(result.status.code == 0)
            {
                login_form.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("failed to logout");
            }
        }

        private void ButtonOnLeave(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            if (header_button[btn] == false)
                btn.ForeColor = System.Drawing.Color.White;

        }
        private void ButtonOnClick(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            SetMainButton(btn);
            switch (btn.Name)
            {
                case "InstructorBtn":
                    ShowViewMenu(ViewMenu.Instructor);
                    break;
                case "ProfileBtn":
                    ShowViewMenu(ViewMenu.Profile);
                    break;

            }
        }
        #endregion

        public static void UpdateProfile(UserProfileModel user)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    Instance.ChangeProfile(user);
                }));
            }
            else
            {
                Instance.ChangeProfile(user);
            }
        }
        public static void ConnectCallback(ATCSimulator.Client.SystemService.UserComputerInfo user)
        {
            try
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.instructor_form.ConnectPC(user);
                    }));
                }
                else
                {
                    Instance.instructor_form.ConnectPC(user);
                }
            }
            catch { }
        }
        public static void ConnectUpdateCallback()
        {
            try
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.instructor_form.ConnectUpdate();
                    }));
                }
                else
                {
                    Instance.instructor_form.ConnectUpdate();
                }
            }
            catch { }
        }

        public static void PlaySimulationThread(SimulationService.ExerciseSimulationModel exercise, List<SimulationService.AircraftData> aircrafts, List<SimulationService.FlightRouteListModel> flight_routes, List<SimulationService.DataAppronModel> aapprons)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    Instance.PlaySimulation(exercise, aircrafts, flight_routes,aapprons);
                }));
            }
            else
            {
                Instance.PlaySimulation(exercise, aircrafts, flight_routes,aapprons);
            }
        }
        public static void ContinueSimulationThread(SimulationService.ExerciseSimulationModel exercise, List<SimulationService.AircraftData> aircrafts, List<SimulationService.FlightRouteListModel> flight_routes, List<SimulationService.DataAppronModel> aapprons, bool isPaused,bool isNew)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    Instance.ContinueSimulation(exercise, aircrafts, flight_routes, aapprons,isPaused,isNew);
                }));
            }
            else
            {
                Instance.ContinueSimulation(exercise, aircrafts, flight_routes, aapprons,isPaused,isNew);
            }
        }
        public void PlaySimulation(SimulationService.ExerciseSimulationModel exercise, List<SimulationService.AircraftData> aircrafts, List<SimulationService.FlightRouteListModel> flight_routes, List<SimulationService.DataAppronModel> aapprons)
        {
            infoForm.simulationForm = new SimulationMainForm(infoForm,pc_role);
            infoForm.simulationForm.Init(exercise, aircrafts, flight_routes, aapprons, pc_role, user_picture.Image, fullname_label.Text);
            infoForm.simulationForm.Show();
            this.Hide();

            infoForm.simulationForm.PlaySimulation();
        }
        public void ContinueSimulation(SimulationService.ExerciseSimulationModel exercise, List<SimulationService.AircraftData> aircrafts, List<SimulationService.FlightRouteListModel> flight_routes, List<SimulationService.DataAppronModel> aapprons,bool isPaused, bool isNew)
        {
            infoForm.simulationForm.Init(exercise, aircrafts, flight_routes, aapprons, pc_role, user_picture.Image, fullname_label.Text);
            if (isNew)
            {
                infoForm.simulationForm.Show();
                this.Hide();
                infoForm.simulationForm.ContinueSimulation(isPaused);
            }

        }

        public static void StopSimulationThread()
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    Instance.StopSimulation();
                }));
            }
            else
            {
                Instance.StopSimulation();
            }
        }
        public void StopSimulation()
        {
            this.Show();
        }

    }
}
