﻿namespace ATCSimulator.Client.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_panel = new System.Windows.Forms.Panel();
            this.InstructorBtn = new System.Windows.Forms.Label();
            this.fullname_label = new System.Windows.Forms.Label();
            this.role_label = new System.Windows.Forms.Label();
            this.SignOutBtn = new System.Windows.Forms.Label();
            this.footnote = new System.Windows.Forms.Label();
            this.ProfileBtn = new System.Windows.Forms.Label();
            this.HelpBtn = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LineBottom = new System.Windows.Forms.PictureBox();
            this.user_picture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // main_panel
            // 
            this.main_panel.Location = new System.Drawing.Point(7, 93);
            this.main_panel.Name = "main_panel";
            this.main_panel.Size = new System.Drawing.Size(1346, 645);
            this.main_panel.TabIndex = 18;
            // 
            // InstructorBtn
            // 
            this.InstructorBtn.AutoSize = true;
            this.InstructorBtn.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.InstructorBtn.ForeColor = System.Drawing.Color.White;
            this.InstructorBtn.Location = new System.Drawing.Point(985, 60);
            this.InstructorBtn.Name = "InstructorBtn";
            this.InstructorBtn.Size = new System.Drawing.Size(111, 25);
            this.InstructorBtn.TabIndex = 16;
            this.InstructorBtn.Text = "Instructor";
            this.InstructorBtn.Click += new System.EventHandler(this.ButtonOnClick);
            this.InstructorBtn.MouseEnter += new System.EventHandler(this.ButtonOnHover);
            this.InstructorBtn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            // 
            // fullname_label
            // 
            this.fullname_label.AutoSize = true;
            this.fullname_label.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.fullname_label.ForeColor = System.Drawing.Color.DodgerBlue;
            this.fullname_label.Location = new System.Drawing.Point(91, 66);
            this.fullname_label.Name = "fullname_label";
            this.fullname_label.Size = new System.Drawing.Size(236, 22);
            this.fullname_label.TabIndex = 13;
            this.fullname_label.Text = "Muhammad Ilyasin Hafid";
            // 
            // role_label
            // 
            this.role_label.AutoSize = true;
            this.role_label.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.role_label.ForeColor = System.Drawing.Color.White;
            this.role_label.Location = new System.Drawing.Point(91, 41);
            this.role_label.Name = "role_label";
            this.role_label.Size = new System.Drawing.Size(104, 23);
            this.role_label.TabIndex = 12;
            this.role_label.Text = "Supervisor";
            // 
            // SignOutBtn
            // 
            this.SignOutBtn.AutoSize = true;
            this.SignOutBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.SignOutBtn.ForeColor = System.Drawing.Color.White;
            this.SignOutBtn.Location = new System.Drawing.Point(1281, 3);
            this.SignOutBtn.Name = "SignOutBtn";
            this.SignOutBtn.Size = new System.Drawing.Size(77, 21);
            this.SignOutBtn.TabIndex = 21;
            this.SignOutBtn.Text = "Sign Out";
            this.SignOutBtn.Click += new System.EventHandler(this.SignOut);
            this.SignOutBtn.MouseEnter += new System.EventHandler(this.ButtonOnHover);
            this.SignOutBtn.MouseLeave += new System.EventHandler(this.SignOutOnLeave);
            // 
            // footnote
            // 
            this.footnote.AutoSize = true;
            this.footnote.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.footnote.ForeColor = System.Drawing.Color.White;
            this.footnote.Location = new System.Drawing.Point(4, 746);
            this.footnote.Name = "footnote";
            this.footnote.Size = new System.Drawing.Size(330, 16);
            this.footnote.TabIndex = 0;
            this.footnote.Text = "Copyright © 2015 Media Indo Teknologi | All Rights Reserved";
            // 
            // ProfileBtn
            // 
            this.ProfileBtn.AutoSize = true;
            this.ProfileBtn.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.ProfileBtn.ForeColor = System.Drawing.Color.White;
            this.ProfileBtn.Location = new System.Drawing.Point(1173, 60);
            this.ProfileBtn.Name = "ProfileBtn";
            this.ProfileBtn.Size = new System.Drawing.Size(77, 25);
            this.ProfileBtn.TabIndex = 23;
            this.ProfileBtn.Text = "Profile";
            this.ProfileBtn.Click += new System.EventHandler(this.ButtonOnClick);
            this.ProfileBtn.MouseEnter += new System.EventHandler(this.ButtonOnHover);
            this.ProfileBtn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            // 
            // HelpBtn
            // 
            this.HelpBtn.AutoSize = true;
            this.HelpBtn.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.HelpBtn.ForeColor = System.Drawing.Color.White;
            this.HelpBtn.Location = new System.Drawing.Point(1295, 60);
            this.HelpBtn.Name = "HelpBtn";
            this.HelpBtn.Size = new System.Drawing.Size(62, 25);
            this.HelpBtn.TabIndex = 24;
            this.HelpBtn.Text = "Help";
            this.HelpBtn.Click += new System.EventHandler(this.ButtonOnClick);
            this.HelpBtn.MouseEnter += new System.EventHandler(this.ButtonOnHover);
            this.HelpBtn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox1.Location = new System.Drawing.Point(7, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1346, 4);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // LineBottom
            // 
            this.LineBottom.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.LineBottom.Location = new System.Drawing.Point(7, 740);
            this.LineBottom.Name = "LineBottom";
            this.LineBottom.Size = new System.Drawing.Size(1346, 3);
            this.LineBottom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LineBottom.TabIndex = 27;
            this.LineBottom.TabStop = false;
            // 
            // user_picture
            // 
            this.user_picture.BackColor = System.Drawing.Color.Transparent;
            this.user_picture.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            this.user_picture.Location = new System.Drawing.Point(7, 7);
            this.user_picture.Name = "user_picture";
            this.user_picture.Size = new System.Drawing.Size(80, 80);
            this.user_picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.user_picture.TabIndex = 11;
            this.user_picture.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1360, 768);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LineBottom);
            this.Controls.Add(this.HelpBtn);
            this.Controls.Add(this.ProfileBtn);
            this.Controls.Add(this.footnote);
            this.Controls.Add(this.SignOutBtn);
            this.Controls.Add(this.main_panel);
            this.Controls.Add(this.InstructorBtn);
            this.Controls.Add(this.fullname_label);
            this.Controls.Add(this.role_label);
            this.Controls.Add(this.user_picture);
            this.DoubleBuffered = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "MainMenuForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel main_panel;
        private System.Windows.Forms.Label InstructorBtn;
        private System.Windows.Forms.Label fullname_label;
        private System.Windows.Forms.Label role_label;
        private System.Windows.Forms.PictureBox user_picture;
        private System.Windows.Forms.Label SignOutBtn;
        private System.Windows.Forms.Label footnote;
        private System.Windows.Forms.Label ProfileBtn;
        private System.Windows.Forms.Label HelpBtn;
        private System.Windows.Forms.PictureBox LineBottom;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
