﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.UI.Other;
using Newtonsoft.Json;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.UI.Simulation.Pilot;
using ATCSimulator.Client.Services;

namespace ATCSimulator.Client.UI.Simulation.Controller
{
    public partial class ControllerForm : Telerik.WinControls.UI.RadForm
    {
        //ControllerFlightStripForm app_form;
        FlightPlanForm flightPlan_form;
        public static ControllerForm Instance { get; private set; }
        private FlightInfoForm flight_info_click = null;
        ExerciseSimulationModel exercise;
        private AircraftData aircraft;
        private Dictionary<string, FlightInfoForm> flight_info_list = new Dictionary<string, FlightInfoForm>();

        public ControllerForm()
        {
            InitializeComponent();
            Instance = this;
            flightPlan_form = new FlightPlanForm();
            flightPlan_form.TopLevel = false;
            flightPlan_form.HideCloseBtn();
            CommandPanel.Controls.Add(flightPlan_form);
            flightPlan_form.Location = new Point { Y = (CommandPanel.Size.Height - flightPlan_form.Size.Height) / 2, X = (CommandPanel.Size.Width - flightPlan_form.Size.Width) / 2 };
        }
        public void Init(ExerciseSimulationModel _exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_routes, List<DataAppronModel> aapprons, string pc_role, Image image, string name)
        {
            flight_info_list.Clear();
            this.exercise = _exercise;
            PCRoleLabel.Text = pc_role;
            FullnameLabel.Text = name;
            UserPicture.Image = image;
            SceneryLabel.Text = _exercise.scenery_name + " Airport";
            List<string> runways = exercise.approach_lights.Keys.ToList();

            aircrafts.OrderBy(d => d.start_time);
            int aircraft_count = 0;
            foreach (AircraftData ac in aircrafts)
            {
                FlightInfoForm a = new FlightInfoForm(this, ac);
                a.TopLevel = false;
                a.Location = new System.Drawing.Point(0, aircraft_count * 85);
                AircraftListPanel.Controls.Add(a);
                a.Show();
                a.Dock = DockStyle.None;
                a.BringToFront();
                aircraft_count++;
                a.InfoStatus(FlightInfoForm.FlightInfoStatus.ENABLE);
                flight_info_list.Add(ac.callsign, a);
            }

            // set Environtment Properties
            WeatherLabel.Text = exercise.weather.ToString();
            WindDirectionLabel.Text = exercise.wind_direction.ToString();
            WindSpeedLabel.Text = exercise.wind_speed.ToString();
            TemperatureLabel.Text = exercise.temperature.ToString();
            VisibilityLabel.Text = exercise.visibility.ToString();
            TimeLabel.Text = exercise.time_simulation.ToString(@"hh\:mm");

            Setup(_exercise.approach_lights, _exercise.runway_lights, _exercise.taxi, _exercise.papi);
            if (pc_role.ToLower().Contains("adccontroller"))
                LightsGroup.Visible = true;
        }
        private void Setup(Dictionary<string, bool> approach, Dictionary<string, bool> runways, bool taxi, bool papi)
        {

            int approach_counter = 0;
            int _app_counter = 0;
            foreach (var r in approach)
            {
                int separation = 15;
                int width = (ApproachGroup.Size.Width - separation) / (approach.Count / 2);
                int locationY = 30;
                int locationX = (width * _app_counter) + separation;
                if (approach_counter % 2 == 1)
                {
                    _app_counter++;
                    locationY = 65;
                }
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = locationX,
                    Y = locationY
                };
                CheckBox ch = CreateCheckBox("approach", r.Value, location, size, r.Key);
                ch.Click += new System.EventHandler(ApproachLight_Click);
                ApproachGroup.Controls.Add(ch);
                approach_counter++;
            }
            int runway_counter = 0;
            foreach (var r in runways)
            {
                int separation = 15;
                int width = (RunwayGroup.Size.Width - separation) / runways.Count;
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = (width * runway_counter) + separation,
                    Y = 22
                };
                CheckBox ch = CreateCheckBox("runway", r.Value, location, size, r.Key);
                ch.Click += new System.EventHandler(RunwayLight_Click);
                RunwayGroup.Controls.Add(ch);
                runway_counter++;
            }

            TaxiLight.Checked = taxi;
            PapiLight.Checked = papi;
        }
        private CheckBox CreateCheckBox(string name, bool check, Point location, Size size, string text)
        {
            CheckBox ch = new CheckBox()
            {
                Appearance = Appearance.Button,
                BackColor = System.Drawing.Color.Tomato,
                Checked = check,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Century Gothic", 12F),
                ForeColor = System.Drawing.Color.White,
                Location = location,
                Text = text,
                Name = name + text,
                Size = size,
                TextAlign = ContentAlignment.MiddleCenter
            };
            ch.FlatAppearance.BorderSize = 0;
            ch.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            return ch;
        }
        private void TaxiLight_Click(object sender, EventArgs e)
        {
            Services.SimulationConnection.LightTaxi(TaxiLight.Checked);
        }

        private void PapiLight_Click(object sender, EventArgs e)
        {
            Services.SimulationConnection.LightPapi(PapiLight.Checked);
        }
        private void RunwayLight_Click(object sender, EventArgs e)
        {
            CheckBox obj = (CheckBox)sender;
            Services.SimulationConnection.LightRunway(obj.Text, obj.Checked);
        }
        private void ApproachLight_Click(object sender, EventArgs e)
        {
            CheckBox obj = (CheckBox)sender;
            Services.SimulationConnection.LightApproach(obj.Text, obj.Checked);
        }

        public void ClickEventAircraft(AircraftData aircraft, FlightInfoForm _flight_info)
        {
            if (flight_info_click != null && flight_info_click != _flight_info)
                flight_info_click.UnClickAircraft();

            this.aircraft = aircraft;
            this.flight_info_click = _flight_info;

            if (PCRoleLabel.Text.ToLower().Contains("appcontroller"))
                AircraftInfo.Visible = true;
            AircraftLiveryImage.Image = HelperClient.GetImageLivery(aircraft.properties.livery, true); ;
            AircraftLiveryLabel.Text = aircraft.properties.livery;
            AircraftCallsignLabel.Text = aircraft.callsign;

            FlightPlanShow();
        }

        #region Show Flight Plan
        public void FlightPlanShow()
        {
            if (aircraft != null)
            {
                FlightPlanModel data = new FlightPlanModel();
                data.flight_rules = aircraft.pilot_properties.flight.flight_route.flight_rules;
                data.callsign = aircraft.callsign;
                data.type_aircraft = aircraft.properties.category;
                data.true_speed = HelperClient.ConvertSpeed("N", aircraft.properties.true_speed);
                data.flight_type = aircraft.properties.flight_type;
                data.departure_time = aircraft.departure_time;
                data.eet = aircraft.estimate_time;
                data.altitude = aircraft.properties.flight_level;
                data.flight_level = aircraft.properties.flight_level_type;
                data.routes = aircraft.pilot_properties.flight.flight_route.routes_name.Aggregate((i, j) => i + " " + j);
                data.destination_airport_code = aircraft.pilot_properties.flight.flight_route.destination_airport_code;
                data.destination_airport_name = aircraft.pilot_properties.flight.flight_route.destination_airport_name;
                data.origin_airport_code = aircraft.pilot_properties.flight.flight_route.origin_airport_code;
                data.origin_airport_name = aircraft.pilot_properties.flight.flight_route.origin_airport_name;
                data.alternate_airport_name = aircraft.pilot_properties.flight.flight_route.alternate_airport_name;
                data.fuel_board = aircraft.fuel;
                data.pilot = new PilotInfoModel()
                {
                    address_city = aircraft.user.address,
                    airport_city = aircraft.user.home_base,
                    name = aircraft.user.first_name + " " + aircraft.user.last_name,
                    phone = aircraft.user.phone
                };
                data.person = aircraft.properties.person;
                data.strips = aircraft.properties.livery_strips;
                //FlightPlanForm flight_plan = new FlightPlanForm(data);

                //flight_plan.ShowDialog();
                flightPlan_form.Init(data);
                flightPlan_form.StartPosition = FormStartPosition.CenterParent;
                flightPlan_form.Show();
            }
        }

        #endregion

        #region Callback

        #region Other Callback

        #region Remove Aircraft
        public void RemoveAircraft(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value.InfoStatus(FlightInfoForm.FlightInfoStatus.REMOVE);
                    SimulationLog.SelectionColor = Color.Blue;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " is removed at " + DateTime.Now + " \n";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = null;
                        AircraftInfo.Visible = false;
                        flight_info_click = null;
                    }
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Aircraft " + callsign + " is removed from play at " + DateTime.Now + " \n";
        }
        #endregion

        #region Incident Animal
        public void IncidentAnimal(bool value, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                ac.Value._aircraft.animal_crossing_enabled = value;
            }
            SimulationLog.SelectionColor = value ? Color.Blue : Color.Red;
            SimulationLog.SelectedText = value ? "Animal Crossing in runway " + runway + " is finish at " + DateTime.Now + " \n" : "There's Animal Crossing in runway " + runway + " at " + DateTime.Now + " \n";
        }
        #endregion

        #region PKPPK
        public void PKPPK(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pkppk = true;
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "PKPPK is arriving to aircraft " + callsign + " at " + DateTime.Now + " \n";
        }
        #endregion

        #region Bird Attack
        public void BirdAttack(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.incident_bird = true;
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Red;
            SimulationLog.SelectedText = "Aircraft " + callsign + " is on Bird Attack at " + DateTime.Now + " \n";
        }
        #endregion

        #region Landing Gear Jam
        public void LandingGearJam(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.landing_gear_jam = true;
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Red;
            SimulationLog.SelectedText = "Aircraft " + callsign + " is Landing Gear Jam at " + DateTime.Now + " \n";
        }
        #endregion

        #region Engine Failure Callback
        public void EngineFailureCallback(string callsign, int engine)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.engine_failure = engine;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.engine_failure = engine;
                        //EngineLabel.Text = "FAILURE";
                        //EngineLabel.ForeColor = Color.Red;
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Red;
            SimulationLog.SelectedText = "Aircraft " + callsign + " engine " + engine + " is failure at " + DateTime.Now + " \n";
        }
        #endregion

        #region ChangeFuelCallback
        public void ChangeFuelCallback(string callsign, TimeSpan fuel)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.fuel = fuel;
                }
                break;
            }
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Aircraft " + callsign + " fuel is increased at " + DateTime.Now + " \n";
        }
        #endregion

        #region Environtment Callback
        public void ChangeWeatherCallback(WeatherInfo weather)
        {
            exercise.weather = weather;
            WeatherLabel.Text = weather.ToString();
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Weather is changed at " + DateTime.Now + " \n";
        }
        public void ChangeWindDirection(float value)
        {
            exercise.wind_direction = Convert.ToInt32(value);
            WindDirectionLabel.Text = Convert.ToInt32(value).ToString();
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Wind Direction is changed at " + DateTime.Now + " \n";
        }
        public void ChangeWindSpeed(float value)
        {
            exercise.wind_speed = Convert.ToInt32(value);
            WindSpeedLabel.Text = Convert.ToInt32(value).ToString();
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Wind Speed is changed at " + DateTime.Now + " \n";
        }
        public void ChangeTemperature(float value)
        {
            exercise.temperature = Convert.ToInt32(value);
            TemperatureLabel.Text = Convert.ToInt32(value).ToString();
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Temperature is changed at " + DateTime.Now + " \n";
        }
        public void ChangeVisibility(float value)
        {
            exercise.visibility = Convert.ToInt32(value);
            VisibilityLabel.Text = Convert.ToInt32(value).ToString();
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Visibility is changed at " + DateTime.Now + " \n";
        }
        public void ChangeTimeSimulation(TimeSpan time)
        {
            exercise.time_simulation = time;
            TimeLabel.Text = time.ToString(@"hh\:mm");
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Time is changed at " + DateTime.Now + " \n";
        }
        #endregion

        #region Light Callback

        #region Taxi Light Callback

        public void TaxiLightCallback(bool value)
        {
            TaxiLight.Checked = value;
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Taxi light is on at " + DateTime.Now + " \n" : "Taxi light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #region Papi Light Callback

        public void PapiLightCallback(bool value)
        {
            PapiLight.Checked = value;
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Papi light is on at " + DateTime.Now + " \n" : "Papi light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #region Runway Light Callback

        public void RunwayLightCallback(string name, bool value)
        {
            CheckBox checkbox = (CheckBox)this.Controls.Find("runway" + name, true).FirstOrDefault();
            if (checkbox != null)
                checkbox.Checked = value;
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Runway " + name + " light is on at " + DateTime.Now + " \n" : "Runway " + name + " light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #region Approach Light Callback

        public void ApproachLightCallback(string name, bool value)
        {
            CheckBox checkbox = (CheckBox)this.Controls.Find("approach" + name, true).FirstOrDefault();
            if (checkbox != null)
                checkbox.Checked = value;
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Approach " + name + " light is on at " + DateTime.Now + " \n" : "Approach " + name + " light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #endregion

        #endregion

        #region System Callback
        public void UpdateTimeSimulation(TimeSpan time)
        {
            try
            {
                TimeSimulationLabel.Text = time.ToString(@"hh\:mm\:ss");
            }
            catch { }
        }
        public void ReadySimulationCallback()
        {
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation is started at " + DateTime.Now + "\n";
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation Play time is : " + exercise.play_time + "\n";
        }
        public void ContinueSimulationCallback()
        {
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation is continue at " + DateTime.Now + "\n";
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation Play time is : " + exercise.play_time + "\n";
        }
        public void PauseSimulationCallback(bool value)
        {
            if (value)
            {
                SimulationLog.SelectionColor = Color.Blue;
                SimulationLog.SelectedText = "Simulation is paused at " + DateTime.Now + " \n";
            }
            else
            {
                SimulationLog.SelectionColor = Color.Blue;
                SimulationLog.SelectedText = "Simulation is resumed at " + DateTime.Now + " \n";
            }

        }
        #endregion

        #region System Ground

        #region Change Ground Status
        public void ChangeGroundStatusCallback(string callsign, int status)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;
                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.None;
                    ac.Value._aircraft.pilot_properties.ground.taxi.routes = new List<string>();
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Route
        public void ChangeFlightRouteCallback(string callsign, FlightRouteListModel routes, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.flight_route = routes;
                    ac.Value._aircraft.pilot_properties.ground.runway.runway = runway;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Parking Bay
        public void ChangeParkingAppronCallback(string callsign, DataAppronModel terminal)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.parking.parking_id = terminal.id;
                    ac.Value._aircraft.pilot_properties.ground.parking.terminal_name = terminal.terminal_name;
                    ac.Value._aircraft.pilot_properties.ground.parking.appron_name = terminal.appron_name;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Ready Aircraft
        public void ReadyAircraftCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ready_departure.is_enabled = false;
                    ac.Value._aircraft.status = AircraftStatus.Form;
                    ac.Value.ChangeStatus(AircraftStatus.Form);
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Runway
        public void ChangeRunwayCallback(string callsign, string runway, bool is_ground)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    if (is_ground)
                        ac.Value._aircraft.pilot_properties.ground.runway.runway = runway;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        if (is_ground)
                            aircraft.pilot_properties.ground.runway.runway = runway;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Engine
        public void StartEngineCallback(string callsign, bool engine)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine = engine;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.ground.engine = engine;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Pushback
        public void PushbackCallback(string callsign, DirectionInfo pushback, string pushback_name)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.pushback.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.pushback.status = CommandStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.pushback.pushback_left = pushback.Equals(DirectionInfo.Left) ? true : false;
                    ac.Value._aircraft.pilot_properties.ground.pushback.pushback_right = pushback.Equals(DirectionInfo.Right) ? true : false;
                    ac.Value._aircraft.from_position = ac.Value._aircraft.current_position;
                    ac.Value._aircraft.current_position = pushback_name;
                    ac.Value._aircraft.status = AircraftStatus.Pushback;
                    ac.Value.ChangeStatus(AircraftStatus.Pushback);
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Taxi
        public void StartTaxiCallback(string callsign, List<string> routes, int status)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.routes = routes;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    ac.Value._aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;
                    if (ac.Value._aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
                    {
                        ac.Value._aircraft.status = AircraftStatus.Taxing;
                        ac.Value.ChangeStatus(AircraftStatus.Taxing);
                    }
                    else
                    {
                        ac.Value._aircraft.status = AircraftStatus.Parking;
                        ac.Value.ChangeStatus(AircraftStatus.Parking);
                    }
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Hold Taxi
        public void HoldTaxiCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Holding;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region ContinueTaxi
        public void ContinueTaxiCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;

                    if (ac.Value._aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
                    {
                        ac.Value._aircraft.status = AircraftStatus.Taxing;
                        ac.Value.ChangeStatus(AircraftStatus.Taxing);
                    }
                    else
                    {
                        ac.Value._aircraft.status = AircraftStatus.Parking;
                        ac.Value.ChangeStatus(AircraftStatus.Parking);
                    }

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Speed Taxi
        public void ChangeSpeedTaxiCallback(string callsign, GroundSpeed speed)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.taxi.speed = speed;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.ground.taxi.speed = speed;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Cross Runway
        public void CrossRunwayCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Uturn
        public void UturnCallback(string callsign, UturnInfo uturn)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.other.u_turn_status = uturn;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region RockingWing
        public void RockingWingCallback(string callsign, bool value)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.other.rocking_wing = value;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.ground.other.rocking_wing = value;
                    }
                    else
                    {
                        if (value)
                            ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Take Off
        public void TakeOffCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = true;
                    ac.Value._aircraft.status = AircraftStatus.TakeOff;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                    ac.Value.ChangeStatus(AircraftStatus.TakeOff);
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Runway Hold
        public void RunwayHoldCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #endregion

        #region System flight

        #region Active Aircraft
        public void ActiveAircraftCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route = true;
                    ac.Value._aircraft.pilot_properties.ready_flight.is_enabled = false;
                    ac.Value._aircraft.status = AircraftStatus.FlyingToDirection;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Flight;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Holding Now Callback
        public void HoldingNowCallback(string callsign, DirectionInfo direction)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    if (direction.Equals(DirectionInfo.Left))
                        ac.Value._aircraft.pilot_properties.flight.holding.holding_left = true;
                    else
                        ac.Value._aircraft.pilot_properties.flight.holding.holding_right = true;
                    ac.Value._aircraft.status = AircraftStatus.Holding;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Holding Position Callback
        public void HoldingPositionCallback(string callsign, string position)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.position_holding = position;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Continue Route Callback
        public void ContinueRouteCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.FlyingToRoute;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Continue Direction Callback
        public void ContinueDirectionCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.FlyingToDirection;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Heading Callback
        public void ChangeHeadingCallback(string callsign, HeadingInfo direction, float heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.FlyingToDirection;
                    if (direction.Equals(HeadingInfo.Absolute))
                    {
                        ac.Value._aircraft.pilot_properties.flight.heading.absolute_heading = true;
                        ac.Value._aircraft.pilot_properties.flight.heading.absolute_heading_value = heading;
                    }
                    else
                    {
                        ac.Value._aircraft.pilot_properties.flight.heading.relative_heading = true;
                        ac.Value._aircraft.pilot_properties.flight.heading.relative_heading_value = heading;
                    }
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Route Callback
        public void ChangeRouteCallback(string callsign, FlightRouteListModel routes)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.FlyingToRoute;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.flight_route = routes;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Direct Go Callback
        public void DirectGoCallback(string callsign, string position, bool isILS)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.FlyingToDirection;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.direct_go.directGo = true;
                    if (isILS)
                        ac.Value._aircraft.pilot_properties.flight.direct_go.positionILS = position;
                    else
                        ac.Value._aircraft.pilot_properties.flight.direct_go.position = position;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Instrument Approach Callback
        public void ApproachCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.Approach; ;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.can_changeRoute = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.change_runway.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Join Circuit Callback
        public void CircuitCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.Circuit;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.can_changeRoute = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Missed Appraoch Callback
        public void MissedApproachCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Touch Go Callback
        public void TouchGoCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Flypass Callback
        public void FlypassCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.flypass = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Extend Downwind Callback
        public void ExtendDownwindCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Orbit Callback
        public void OrbitCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Landing Callback
        public void LandingCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    if (runway.Equals(""))
                    {
                        ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                        ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                        ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                        ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                        ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    }

                    ac.Value._aircraft.pilot_properties.flight.other.landing = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landingRunway = runway;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.directGo = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Altitude Callback
        public void AltitudeCallback(string callsign, float altitude)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.altitude = altitude;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Speed Callback
        public void SpeedCallback(string callsign, float speed)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.speed = speed;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change ILS Mode Callback
        public void ChangeILSModeCallback(string callsign, bool value)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.ILSmode = value;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #endregion

        #region Visual Ground Callback

        #region Finish Ready
        public void FinishReadyDeparture(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Pushback
        public void FinishPushback(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.pushback.status = CommandStatus.Finish;
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;
                    ac.Value._aircraft.heading = heading;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Update Taxi Position
        public void UpdateTaxiPosition(string callsign, string position, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.current_position = position;
                    ac.Value._aircraft.heading = heading;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Taxi
        public void FinishTaxi(string callsign, string position, double heading, int status)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;

                    ac.Value._aircraft.heading = heading;
                    ac.Value._aircraft.current_position = position;
                    ac.Value._aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;
                    //if position can intersection show intersection

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Finish;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Heading Aircraft
        public void HeadingAircraft(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.heading = heading;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.heading = heading;
                    }
                    break;
                }
            }
        }
        #endregion

        #region RequestCrossRunway
        public void RequestCrossRunway(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = true;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.CrossRunway;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = true;

                    ac.Value._aircraft.heading = heading;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Uturn
        public void FinishUTurn(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.other.u_turn_status = UturnInfo.None;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.None;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;

                    ac.Value._aircraft.heading = heading;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Runway Hold
        public void FinishHoldTakeOff(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Take Off
        public void FinishTakeOff(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.status = AircraftStatus.TakeOff;
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Flight;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Finish;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Crash Aircraft
        public void CrashAircraft(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value.InfoStatus(FlightInfoForm.FlightInfoStatus.CRASH);
                    ac.Value._aircraft.crashed = true;
                    SimulationLog.SelectionColor = Color.Red;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " is crashed at " + DateTime.Now + " \n";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = null;
                        AircraftInfo.Visible = false;
                        flight_info_click = null;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Parking
        public void FinishParking(AircraftData newAircraftData)
        {
            foreach (var ac in flight_info_list)
            {
                if (newAircraftData.callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = newAircraftData;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Backtrack
        public void Backtrack(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #endregion

        #region Visual Flight Callback

        #region Cannot Change Speed
        public void CannotChangeSpeed(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.can_change_speed = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Cannot Change Altitude
        public void CannotChangeAltitude(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.can_change_altitude = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Update Performance
        public void UpdatePerformanceCallback(string callsign, float speed, float altitude)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.speed = speed;
                    ac.Value._aircraft.altitude = altitude;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Update Holding Point
        public void UpdateHoldingPoint(List<string> holding)
        {
            foreach (var ac in flight_info_list)
            {
                ac.Value._aircraft.pilot_properties.flight.holdingPoint = holding;
                if (ac.Value.Equals(flight_info_click))
                {
                    aircraft = ac.Value._aircraft;
                }
            }
        }
        #endregion

        #region Cannot Holding
        public void CannotHolding(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    SimulationLog.SelectionColor = Color.Red;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " Cannot Holding in " +
                        ac.Value._aircraft.pilot_properties.flight.holding.position_holding + " at " + DateTime.Now + " \n";
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.position_holding = "";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Holding At Callback
        public void HoldingAtCallback(string callsign, string position, bool isInstrument)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.Holding;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.position_holding = position;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = !isInstrument;
                    if (isInstrument)
                    {
                        ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = true;
                    }
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Route Update Callback
        public void RouteUpdateCallback(string callsign, FlightRouteListModel route)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.flight_route = route;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Ready Join Circuit
        public void ReadyJoinCircuit(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Join Circuit Callback Visual
        public void JoinCircuitCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.Circuit;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.can_changeRoute = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.change_runway.is_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Go Round
        public void FinishGoRound(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Approach
        public void StartApproach(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Go Round
        public void StartGoRound(string callsign, bool isFlypass, bool isIfr)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    if (isFlypass)
                    {
                        if (isIfr)
                        {
                            ac.Value._aircraft.status = AircraftStatus.MissedApproach;
                            ac.Value._aircraft.pilot_properties.flight.other.missed_approach = true;
                        }
                        else
                        {
                            ac.Value._aircraft.status = AircraftStatus.Flypass;
                            ac.Value._aircraft.pilot_properties.flight.other.flypass = true;
                        }
                    }
                    else
                    {
                        ac.Value._aircraft.status = AircraftStatus.TouchGo;
                        ac.Value._aircraft.pilot_properties.flight.other.touch_go = true;
                    }
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Cannot Landing
        public void CannotLanding(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.landingRunway = "";
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.directGo = true;
                    SimulationLog.SelectionColor = Color.Red;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " Cannot Landing at " + DateTime.Now + " \n";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Landing
        public void StartLanding(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.Landing;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.landing = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }

                    break;
                }
            }
        }
        #endregion

        #region Touch Ground
        public void TouchGround(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.is_departure = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.status = GroundStatusInfo.Parking;
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Landing
        public void FinishLandingCallback(string callsign, float heading,string from, string position)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.heading = heading;
                    ac.Value._aircraft.current_position = position;
                    ac.Value._aircraft.from_position = from;
                    ac.Value._aircraft.status = AircraftStatus.Parking;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                    }
                    break;
                }
            }
        }
        #endregion

        #endregion

        #endregion
    }
}
