﻿namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    partial class IncidentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IncidentCommand = new System.Windows.Forms.GroupBox();
            this.IncidentAnimal = new System.Windows.Forms.GroupBox();
            this.PKPPK = new System.Windows.Forms.Button();
            this.IncidentBird = new System.Windows.Forms.Button();
            this.IncidentCommand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // IncidentCommand
            // 
            this.IncidentCommand.Controls.Add(this.IncidentBird);
            this.IncidentCommand.Controls.Add(this.PKPPK);
            this.IncidentCommand.Controls.Add(this.IncidentAnimal);
            this.IncidentCommand.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.IncidentCommand.ForeColor = System.Drawing.Color.DodgerBlue;
            this.IncidentCommand.Location = new System.Drawing.Point(0, 0);
            this.IncidentCommand.Name = "IncidentCommand";
            this.IncidentCommand.Size = new System.Drawing.Size(378, 280);
            this.IncidentCommand.TabIndex = 111;
            this.IncidentCommand.TabStop = false;
            this.IncidentCommand.Text = "Incident";
            // 
            // IncidentAnimal
            // 
            this.IncidentAnimal.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.IncidentAnimal.ForeColor = System.Drawing.Color.White;
            this.IncidentAnimal.Location = new System.Drawing.Point(19, 148);
            this.IncidentAnimal.Name = "IncidentAnimal";
            this.IncidentAnimal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IncidentAnimal.Size = new System.Drawing.Size(334, 108);
            this.IncidentAnimal.TabIndex = 155;
            this.IncidentAnimal.TabStop = false;
            this.IncidentAnimal.Text = "Animal Crossing";
            // 
            // PKPPK
            // 
            this.PKPPK.BackColor = System.Drawing.Color.DodgerBlue;
            this.PKPPK.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.PKPPK.FlatAppearance.BorderSize = 0;
            this.PKPPK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.PKPPK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.PKPPK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PKPPK.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.PKPPK.ForeColor = System.Drawing.Color.White;
            this.PKPPK.Location = new System.Drawing.Point(188, 57);
            this.PKPPK.Name = "PKPPK";
            this.PKPPK.Size = new System.Drawing.Size(165, 63);
            this.PKPPK.TabIndex = 156;
            this.PKPPK.Text = "PKPPK";
            this.PKPPK.UseVisualStyleBackColor = false;
            this.PKPPK.Click += new System.EventHandler(this.PKPPK_Click);
            // 
            // IncidentBird
            // 
            this.IncidentBird.BackColor = System.Drawing.Color.DodgerBlue;
            this.IncidentBird.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.IncidentBird.FlatAppearance.BorderSize = 0;
            this.IncidentBird.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.IncidentBird.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.IncidentBird.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IncidentBird.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.IncidentBird.ForeColor = System.Drawing.Color.White;
            this.IncidentBird.Location = new System.Drawing.Point(17, 57);
            this.IncidentBird.Name = "IncidentBird";
            this.IncidentBird.Size = new System.Drawing.Size(165, 63);
            this.IncidentBird.TabIndex = 157;
            this.IncidentBird.Text = "Bird Attack";
            this.IncidentBird.UseVisualStyleBackColor = false;
            this.IncidentBird.Click += new System.EventHandler(this.IncidentBird_Click);
            // 
            // IncidentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(378, 280);
            this.Controls.Add(this.IncidentCommand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "IncidentForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "GroundForm";
            this.ThemeName = "ControlDefault";
            this.IncidentCommand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox IncidentCommand;
        private System.Windows.Forms.GroupBox IncidentAnimal;
        private System.Windows.Forms.Button PKPPK;
        private System.Windows.Forms.Button IncidentBird;


    }
}
