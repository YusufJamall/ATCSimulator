﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.UI.Simulation.Pilot;

namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    public partial class ActivateArrivalForm : Telerik.WinControls.UI.RadForm
    {
        public AircraftData aircraft;
        public ActivateArrivalForm()
        {
            InitializeComponent();
        }
        public void Setup(AircraftData aircraft, bool enable)
        {
            DepartureTime.Text = aircraft.departure_time.ToString("c");
            StartingTime.Text = aircraft.start_time.ToString("c");
            StartingPosition.Text = aircraft.current_position;
            ActiveAircraft.Enabled = aircraft.pilot_properties.ready_flight.is_enabled;
            ActiveAircraft.Visible = enable;
            this.aircraft = aircraft;
        }

        private void ActiveAircraft_Click(object sender, EventArgs e)
        {
            Services.SimulationConnection.ActiveAircraft(aircraft.callsign, aircraft.pilot_properties.flight.flight_route);
        }
    }
}
