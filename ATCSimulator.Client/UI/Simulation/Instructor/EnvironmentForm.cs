﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.Services;

namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    public partial class EnvironmentForm : Telerik.WinControls.UI.RadForm
    {
        public EnvironmentForm()
        {
            InitializeComponent();
            Weather.Items.Clear();
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Clear", WeatherInfo.Clear));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Cloudy", WeatherInfo.Cloudy));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Light Rain", WeatherInfo.LightRain));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Thunder Storm", WeatherInfo.ThunderStorm));
        }
        public void Setup(ExerciseSimulationModel exercise)
        {
            int _weather_index = -1;
            if (exercise.weather == WeatherInfo.Clear)
                _weather_index = 0;
            else if (exercise.weather == WeatherInfo.Cloudy)
                _weather_index = 1;
            else if (exercise.weather == WeatherInfo.LightRain)
                _weather_index = 2;
            else if (exercise.weather == WeatherInfo.ThunderStorm)
                _weather_index = 3;
            Weather.SelectedIndex = _weather_index;
            WindDirection.Value = exercise.wind_direction;
            WindSpeed.Value = exercise.wind_speed;
            Temperature.Value = exercise.temperature;
            Visibility.Value = exercise.visibility;
            EnvTime.Value = DateTime.Now.AddSeconds(exercise.time_simulation.TotalSeconds);
        }

        private void ChangeWeather_Click(object sender, EventArgs e)
        {
            if(Weather.SelectedIndex ==-1)
                return;

            WeatherInfo _weather = WeatherInfo.Clear;
            if (Weather.SelectedIndex == 1)
                _weather = WeatherInfo.Cloudy;
            else if (Weather.SelectedIndex == 2)
                _weather = WeatherInfo.LightRain;
            else if (Weather.SelectedIndex == 3)
                _weather = WeatherInfo.ThunderStorm;
            SimulationConnection.ChangeWeather(_weather);
        }

        private void ChangeWindDirection_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeWindDirection((float)WindDirection.Value);
        }

        private void ChangeWindSpeed_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeWindSpeed((float)WindSpeed.Value);
        }

        private void ChangeTemperature_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeTemperature((float)Temperature.Value);
        }

        private void ChangeVisibility_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeVisibility((float)Visibility.Value);
        }

        private void ChangeTime_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeTimeSimulation(EnvTime.Value.Value.TimeOfDay);
        }

        private void Weather_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (Weather.SelectedIndex != -1)
            {
                HelperClient.WeatherDefault weather = new HelperClient.WeatherDefault((SimulationService.WeatherInfo)Weather.SelectedItem.Value);
                if (weather != null)
                {
                    WindDirection.Value = weather.wind_direction;
                    WindSpeed.Value = weather.wind_speed;
                    Temperature.Value = weather.temperature;
                    Visibility.Value = weather.visibility;
                }
            }
        }
    }
}
