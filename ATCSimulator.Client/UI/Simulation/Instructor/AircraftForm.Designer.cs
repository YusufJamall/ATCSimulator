﻿namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    partial class AircraftForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChangeFuelBtn = new System.Windows.Forms.Button();
            this.RemoveAircraftBtn = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.TypeLabel = new System.Windows.Forms.Label();
            this.FlightPlanButton = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.AircraftCommand = new System.Windows.Forms.GroupBox();
            this.EngineFailureGroup = new System.Windows.Forms.GroupBox();
            this.EngineNumber = new Telerik.WinControls.UI.RadSpinEditor();
            this.label3 = new System.Windows.Forms.Label();
            this.EngineFailureBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.AircraftFuelHour = new Telerik.WinControls.UI.RadSpinEditor();
            this.AircraftFuelMin = new Telerik.WinControls.UI.RadSpinEditor();
            this.label10 = new System.Windows.Forms.Label();
            this.LandingGearJam = new System.Windows.Forms.Button();
            this.AircraftCommand.SuspendLayout();
            this.EngineFailureGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EngineNumber)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ChangeFuelBtn
            // 
            this.ChangeFuelBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeFuelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeFuelBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.ChangeFuelBtn.ForeColor = System.Drawing.Color.White;
            this.ChangeFuelBtn.Location = new System.Drawing.Point(164, 21);
            this.ChangeFuelBtn.Name = "ChangeFuelBtn";
            this.ChangeFuelBtn.Size = new System.Drawing.Size(183, 30);
            this.ChangeFuelBtn.TabIndex = 154;
            this.ChangeFuelBtn.Text = "Change Fuel";
            this.ChangeFuelBtn.UseVisualStyleBackColor = false;
            this.ChangeFuelBtn.Click += new System.EventHandler(this.ChangeFuelBtn_Click);
            // 
            // RemoveAircraftBtn
            // 
            this.RemoveAircraftBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.RemoveAircraftBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RemoveAircraftBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.RemoveAircraftBtn.ForeColor = System.Drawing.Color.White;
            this.RemoveAircraftBtn.Location = new System.Drawing.Point(11, 87);
            this.RemoveAircraftBtn.Name = "RemoveAircraftBtn";
            this.RemoveAircraftBtn.Size = new System.Drawing.Size(355, 30);
            this.RemoveAircraftBtn.TabIndex = 3;
            this.RemoveAircraftBtn.Text = "Remove Aircraft";
            this.RemoveAircraftBtn.UseVisualStyleBackColor = false;
            this.RemoveAircraftBtn.Click += new System.EventHandler(this.RemoveAircraftBtn_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(11, 56);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 21);
            this.label28.TabIndex = 37;
            this.label28.Text = "Flight Plan";
            // 
            // TypeLabel
            // 
            this.TypeLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.TypeLabel.ForeColor = System.Drawing.Color.Tomato;
            this.TypeLabel.Location = new System.Drawing.Point(127, 26);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(239, 25);
            this.TypeLabel.TabIndex = 35;
            this.TypeLabel.Text = "Citilink";
            this.TypeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FlightPlanButton
            // 
            this.FlightPlanButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.FlightPlanButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FlightPlanButton.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.FlightPlanButton.ForeColor = System.Drawing.Color.White;
            this.FlightPlanButton.Location = new System.Drawing.Point(272, 51);
            this.FlightPlanButton.Name = "FlightPlanButton";
            this.FlightPlanButton.Size = new System.Drawing.Size(94, 30);
            this.FlightPlanButton.TabIndex = 32;
            this.FlightPlanButton.Text = "Details";
            this.FlightPlanButton.UseVisualStyleBackColor = false;
            this.FlightPlanButton.Click += new System.EventHandler(this.FlightPlanButton_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(11, 26);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(110, 21);
            this.label24.TabIndex = 7;
            this.label24.Text = "Type Aircraft";
            // 
            // AircraftCommand
            // 
            this.AircraftCommand.Controls.Add(this.LandingGearJam);
            this.AircraftCommand.Controls.Add(this.EngineFailureGroup);
            this.AircraftCommand.Controls.Add(this.groupBox1);
            this.AircraftCommand.Controls.Add(this.label24);
            this.AircraftCommand.Controls.Add(this.FlightPlanButton);
            this.AircraftCommand.Controls.Add(this.TypeLabel);
            this.AircraftCommand.Controls.Add(this.label28);
            this.AircraftCommand.Controls.Add(this.RemoveAircraftBtn);
            this.AircraftCommand.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.AircraftCommand.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftCommand.Location = new System.Drawing.Point(0, 0);
            this.AircraftCommand.Name = "AircraftCommand";
            this.AircraftCommand.Size = new System.Drawing.Size(378, 280);
            this.AircraftCommand.TabIndex = 113;
            this.AircraftCommand.TabStop = false;
            this.AircraftCommand.Text = "Aircraft Command";
            // 
            // EngineFailureGroup
            // 
            this.EngineFailureGroup.Controls.Add(this.EngineNumber);
            this.EngineFailureGroup.Controls.Add(this.label3);
            this.EngineFailureGroup.Controls.Add(this.EngineFailureBtn);
            this.EngineFailureGroup.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.EngineFailureGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.EngineFailureGroup.Location = new System.Drawing.Point(13, 155);
            this.EngineFailureGroup.Name = "EngineFailureGroup";
            this.EngineFailureGroup.Size = new System.Drawing.Size(353, 59);
            this.EngineFailureGroup.TabIndex = 161;
            this.EngineFailureGroup.TabStop = false;
            this.EngineFailureGroup.Text = "Engine Failure";
            // 
            // EngineNumber
            // 
            this.EngineNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.EngineNumber.Location = new System.Drawing.Point(159, 26);
            this.EngineNumber.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.EngineNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.EngineNumber.Name = "EngineNumber";
            // 
            // 
            // 
            this.EngineNumber.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.EngineNumber.Size = new System.Drawing.Size(61, 24);
            this.EngineNumber.TabIndex = 160;
            this.EngineNumber.TabStop = false;
            this.EngineNumber.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.EngineNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(9, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 21);
            this.label3.TabIndex = 159;
            this.label3.Text = "Engine Number";
            // 
            // EngineFailureBtn
            // 
            this.EngineFailureBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.EngineFailureBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.EngineFailureBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.EngineFailureBtn.ForeColor = System.Drawing.Color.White;
            this.EngineFailureBtn.Location = new System.Drawing.Point(229, 22);
            this.EngineFailureBtn.Name = "EngineFailureBtn";
            this.EngineFailureBtn.Size = new System.Drawing.Size(120, 30);
            this.EngineFailureBtn.TabIndex = 158;
            this.EngineFailureBtn.Text = "Execute";
            this.EngineFailureBtn.UseVisualStyleBackColor = false;
            this.EngineFailureBtn.Click += new System.EventHandler(this.EngineFailureBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.ChangeFuelBtn);
            this.groupBox1.Controls.Add(this.AircraftFuelHour);
            this.groupBox1.Controls.Add(this.AircraftFuelMin);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.groupBox1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBox1.Location = new System.Drawing.Point(13, 214);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(353, 59);
            this.groupBox1.TabIndex = 160;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fuel";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label15.ForeColor = System.Drawing.Color.Tomato;
            this.label15.Location = new System.Drawing.Point(116, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 158;
            this.label15.Text = "HH:mm";
            // 
            // AircraftFuelHour
            // 
            this.AircraftFuelHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftFuelHour.Location = new System.Drawing.Point(11, 27);
            this.AircraftFuelHour.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.AircraftFuelHour.Name = "AircraftFuelHour";
            this.AircraftFuelHour.Size = new System.Drawing.Size(42, 24);
            this.AircraftFuelHour.TabIndex = 159;
            this.AircraftFuelHour.TabStop = false;
            this.AircraftFuelHour.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AircraftFuelMin
            // 
            this.AircraftFuelMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftFuelMin.Location = new System.Drawing.Point(68, 27);
            this.AircraftFuelMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.AircraftFuelMin.Name = "AircraftFuelMin";
            this.AircraftFuelMin.Size = new System.Drawing.Size(42, 24);
            this.AircraftFuelMin.TabIndex = 157;
            this.AircraftFuelMin.TabStop = false;
            this.AircraftFuelMin.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(54, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 20);
            this.label10.TabIndex = 156;
            this.label10.Text = ":";
            // 
            // LandingGearJam
            // 
            this.LandingGearJam.BackColor = System.Drawing.Color.DodgerBlue;
            this.LandingGearJam.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.LandingGearJam.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LandingGearJam.ForeColor = System.Drawing.Color.White;
            this.LandingGearJam.Location = new System.Drawing.Point(12, 125);
            this.LandingGearJam.Name = "LandingGearJam";
            this.LandingGearJam.Size = new System.Drawing.Size(355, 30);
            this.LandingGearJam.TabIndex = 162;
            this.LandingGearJam.Text = "Landing Gear Jam";
            this.LandingGearJam.UseVisualStyleBackColor = false;
            this.LandingGearJam.Click += new System.EventHandler(this.LandingGearJam_Click);
            // 
            // AircraftForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(378, 280);
            this.Controls.Add(this.AircraftCommand);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AircraftForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "GroundForm";
            this.ThemeName = "ControlDefault";
            this.AircraftCommand.ResumeLayout(false);
            this.AircraftCommand.PerformLayout();
            this.EngineFailureGroup.ResumeLayout(false);
            this.EngineFailureGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EngineNumber)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ChangeFuelBtn;
        private System.Windows.Forms.Button RemoveAircraftBtn;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label TypeLabel;
        private System.Windows.Forms.Button FlightPlanButton;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox AircraftCommand;
        private Telerik.WinControls.UI.RadSpinEditor AircraftFuelHour;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadSpinEditor AircraftFuelMin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox EngineFailureGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button EngineFailureBtn;
        private Telerik.WinControls.UI.RadSpinEditor EngineNumber;
        private System.Windows.Forms.Button LandingGearJam;


    }
}
