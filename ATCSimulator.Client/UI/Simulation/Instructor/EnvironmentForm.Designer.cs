﻿namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    partial class EnvironmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ChangeTime = new System.Windows.Forms.Button();
            this.ChangeVisibility = new System.Windows.Forms.Button();
            this.ChangeTemperature = new System.Windows.Forms.Button();
            this.ChangeWindSpeed = new System.Windows.Forms.Button();
            this.ChangeWindDirection = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Weather = new Telerik.WinControls.UI.RadDropDownList();
            this.ChangeWeather = new System.Windows.Forms.Button();
            this.EnvTime = new Telerik.WinControls.UI.RadTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Visibility = new Telerik.WinControls.UI.RadSpinEditor();
            this.Temperature = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindSpeed = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindDirection = new Telerik.WinControls.UI.RadSpinEditor();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ChangeTime);
            this.groupBox3.Controls.Add(this.ChangeVisibility);
            this.groupBox3.Controls.Add(this.ChangeTemperature);
            this.groupBox3.Controls.Add(this.ChangeWindSpeed);
            this.groupBox3.Controls.Add(this.ChangeWindDirection);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.EnvTime);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.Visibility);
            this.groupBox3.Controls.Add(this.Temperature);
            this.groupBox3.Controls.Add(this.WindSpeed);
            this.groupBox3.Controls.Add(this.WindDirection);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBox3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(378, 280);
            this.groupBox3.TabIndex = 114;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Environment Properties";
            // 
            // ChangeTime
            // 
            this.ChangeTime.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeTime.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeTime.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ChangeTime.ForeColor = System.Drawing.Color.White;
            this.ChangeTime.Location = new System.Drawing.Point(271, 239);
            this.ChangeTime.Name = "ChangeTime";
            this.ChangeTime.Size = new System.Drawing.Size(94, 30);
            this.ChangeTime.TabIndex = 179;
            this.ChangeTime.Text = "Change";
            this.ChangeTime.UseVisualStyleBackColor = false;
            this.ChangeTime.Click += new System.EventHandler(this.ChangeTime_Click);
            // 
            // ChangeVisibility
            // 
            this.ChangeVisibility.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeVisibility.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeVisibility.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ChangeVisibility.ForeColor = System.Drawing.Color.White;
            this.ChangeVisibility.Location = new System.Drawing.Point(271, 201);
            this.ChangeVisibility.Name = "ChangeVisibility";
            this.ChangeVisibility.Size = new System.Drawing.Size(94, 30);
            this.ChangeVisibility.TabIndex = 178;
            this.ChangeVisibility.Text = "Change";
            this.ChangeVisibility.UseVisualStyleBackColor = false;
            this.ChangeVisibility.Click += new System.EventHandler(this.ChangeVisibility_Click);
            // 
            // ChangeTemperature
            // 
            this.ChangeTemperature.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeTemperature.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeTemperature.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ChangeTemperature.ForeColor = System.Drawing.Color.White;
            this.ChangeTemperature.Location = new System.Drawing.Point(271, 164);
            this.ChangeTemperature.Name = "ChangeTemperature";
            this.ChangeTemperature.Size = new System.Drawing.Size(94, 30);
            this.ChangeTemperature.TabIndex = 177;
            this.ChangeTemperature.Text = "Change";
            this.ChangeTemperature.UseVisualStyleBackColor = false;
            this.ChangeTemperature.Click += new System.EventHandler(this.ChangeTemperature_Click);
            // 
            // ChangeWindSpeed
            // 
            this.ChangeWindSpeed.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeWindSpeed.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeWindSpeed.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ChangeWindSpeed.ForeColor = System.Drawing.Color.White;
            this.ChangeWindSpeed.Location = new System.Drawing.Point(271, 127);
            this.ChangeWindSpeed.Name = "ChangeWindSpeed";
            this.ChangeWindSpeed.Size = new System.Drawing.Size(94, 30);
            this.ChangeWindSpeed.TabIndex = 176;
            this.ChangeWindSpeed.Text = "Change";
            this.ChangeWindSpeed.UseVisualStyleBackColor = false;
            this.ChangeWindSpeed.Click += new System.EventHandler(this.ChangeWindSpeed_Click);
            // 
            // ChangeWindDirection
            // 
            this.ChangeWindDirection.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeWindDirection.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeWindDirection.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ChangeWindDirection.ForeColor = System.Drawing.Color.White;
            this.ChangeWindDirection.Location = new System.Drawing.Point(271, 90);
            this.ChangeWindDirection.Name = "ChangeWindDirection";
            this.ChangeWindDirection.Size = new System.Drawing.Size(94, 30);
            this.ChangeWindDirection.TabIndex = 175;
            this.ChangeWindDirection.Text = "Change";
            this.ChangeWindDirection.UseVisualStyleBackColor = false;
            this.ChangeWindDirection.Click += new System.EventHandler(this.ChangeWindDirection_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Weather);
            this.groupBox1.Controls.Add(this.ChangeWeather);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(6, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 56);
            this.groupBox1.TabIndex = 174;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Weather";
            // 
            // Weather
            // 
            this.Weather.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Weather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Weather.ForeColor = System.Drawing.Color.DodgerBlue;
            radListDataItem1.Text = "Clear";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Rain";
            radListDataItem2.TextWrap = true;
            this.Weather.Items.Add(radListDataItem1);
            this.Weather.Items.Add(radListDataItem2);
            this.Weather.Location = new System.Drawing.Point(6, 23);
            this.Weather.Name = "Weather";
            this.Weather.Size = new System.Drawing.Size(254, 24);
            this.Weather.TabIndex = 108;
            this.Weather.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.Weather_SelectedIndexChanged);
            // 
            // ChangeWeather
            // 
            this.ChangeWeather.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeWeather.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeWeather.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ChangeWeather.ForeColor = System.Drawing.Color.White;
            this.ChangeWeather.Location = new System.Drawing.Point(266, 20);
            this.ChangeWeather.Name = "ChangeWeather";
            this.ChangeWeather.Size = new System.Drawing.Size(94, 30);
            this.ChangeWeather.TabIndex = 173;
            this.ChangeWeather.Text = "Change";
            this.ChangeWeather.UseVisualStyleBackColor = false;
            this.ChangeWeather.Click += new System.EventHandler(this.ChangeWeather_Click);
            // 
            // EnvTime
            // 
            this.EnvTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.EnvTime.Location = new System.Drawing.Point(142, 242);
            this.EnvTime.Name = "EnvTime";
            this.EnvTime.Size = new System.Drawing.Size(122, 24);
            this.EnvTime.Step = 30;
            this.EnvTime.TabIndex = 172;
            this.EnvTime.TabStop = false;
            this.EnvTime.Text = "radTimePicker1";
            this.EnvTime.TimeTables = Telerik.WinControls.UI.TimeTables.HoursAndMinutesInOneTable;
            this.EnvTime.Value = new System.DateTime(2014, 8, 16, 11, 30, 4, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(249, 209);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 17);
            this.label12.TabIndex = 118;
            this.label12.Text = "m";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(248, 161);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 117;
            this.label11.Text = "o";
            // 
            // Visibility
            // 
            this.Visibility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Visibility.Location = new System.Drawing.Point(142, 205);
            this.Visibility.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.Visibility.Name = "Visibility";
            // 
            // 
            // 
            this.Visibility.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Visibility.Size = new System.Drawing.Size(107, 24);
            this.Visibility.TabIndex = 116;
            this.Visibility.TabStop = false;
            this.Visibility.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Visibility.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // Temperature
            // 
            this.Temperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Temperature.Location = new System.Drawing.Point(141, 168);
            this.Temperature.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Temperature.Name = "Temperature";
            // 
            // 
            // 
            this.Temperature.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Temperature.Size = new System.Drawing.Size(107, 24);
            this.Temperature.TabIndex = 115;
            this.Temperature.TabStop = false;
            this.Temperature.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Temperature.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // WindSpeed
            // 
            this.WindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindSpeed.Location = new System.Drawing.Point(142, 130);
            this.WindSpeed.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.WindSpeed.Name = "WindSpeed";
            // 
            // 
            // 
            this.WindSpeed.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindSpeed.Size = new System.Drawing.Size(107, 24);
            this.WindSpeed.TabIndex = 114;
            this.WindSpeed.TabStop = false;
            this.WindSpeed.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // WindDirection
            // 
            this.WindDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindDirection.Location = new System.Drawing.Point(142, 91);
            this.WindDirection.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.WindDirection.Name = "WindDirection";
            // 
            // 
            // 
            this.WindDirection.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindDirection.Size = new System.Drawing.Size(107, 24);
            this.WindDirection.TabIndex = 113;
            this.WindDirection.TabStop = false;
            this.WindDirection.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindDirection.Value = new decimal(new int[] {
            359,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(253, 172);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 112;
            this.label10.Text = "C";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(249, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 17);
            this.label9.TabIndex = 111;
            this.label9.Text = "kn";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(252, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 109;
            this.label8.Text = "o";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(13, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 20);
            this.label7.TabIndex = 24;
            this.label7.Text = "Time";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(12, 206);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 20);
            this.label14.TabIndex = 14;
            this.label14.Text = "Visibility";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(11, 169);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 20);
            this.label19.TabIndex = 12;
            this.label19.Text = "Temperature";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(12, 94);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(112, 20);
            this.label20.TabIndex = 9;
            this.label20.Text = "Wind Direction";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(11, 131);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "Wind Speed";
            // 
            // EnvironmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(378, 280);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EnvironmentForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "GroundForm";
            this.ThemeName = "ControlDefault";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button ChangeWeather;
        private Telerik.WinControls.UI.RadTimePicker EnvTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadSpinEditor Visibility;
        private Telerik.WinControls.UI.RadSpinEditor Temperature;
        private Telerik.WinControls.UI.RadSpinEditor WindSpeed;
        private Telerik.WinControls.UI.RadSpinEditor WindDirection;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadDropDownList Weather;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ChangeWindDirection;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button ChangeTime;
        private System.Windows.Forms.Button ChangeVisibility;
        private System.Windows.Forms.Button ChangeTemperature;
        private System.Windows.Forms.Button ChangeWindSpeed;

    }
}
