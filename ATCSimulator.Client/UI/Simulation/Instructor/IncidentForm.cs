﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.Services;

namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    public partial class IncidentForm : Telerik.WinControls.UI.RadForm
    {
        private AircraftData aircraft;
        public IncidentForm()
        {
            InitializeComponent();
        }

        public void Init(AircraftData aircraft)
        {
            this.aircraft = aircraft;
            if (aircraft.crashed)
            {
                PKPPK.Enabled = !aircraft.pkppk;
                PKPPK.BackColor = !aircraft.pkppk ? Color.DodgerBlue : Color.Tomato;
            }
            else
            {
                PKPPK.Enabled = false;
                PKPPK.BackColor = Color.DodgerBlue;
            }
            if (aircraft.incident_bird)
            {
                IncidentBird.Enabled = false;
                IncidentBird.BackColor = Color.Tomato;
            }
            else
            {
                IncidentBird.Enabled = aircraft.incident_bird_enable;
                IncidentBird.BackColor = Color.DodgerBlue;
            }
            IncidentAnimal.Enabled = aircraft.animal_crossing_enabled;


        }
        public void Setup(List<string> runways)
        {
            #region Incident Animal Crossing
            int counter = 0;
            for (int i = 0; i < runways.Count; i++)
            {
                int separation = 15;
                int width = (IncidentAnimal.Size.Width - separation) / (runways.Count / 2);
                int locationY = 30;
                int locationX = (width * counter) + separation;
                if (i % 2 == 1)
                {
                    counter++;
                    locationY = 65;
                }
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = locationX,
                    Y = locationY
                };
                Button btn = CreateButton(location, size, runways[i]);
                btn.Click += new System.EventHandler(this.IncidentAnimal_Click);
                IncidentAnimal.Controls.Add(btn);
            }
            #endregion
        }
        private Button CreateButton(Point location, Size size, string text)
        {
            Button ch = new Button()
            {
                BackColor = System.Drawing.Color.DodgerBlue,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Century Gothic", 12F),
                ForeColor = System.Drawing.Color.White,
                Location = location,
                Text = text,
                Size = size,
                TextAlign = ContentAlignment.MiddleCenter
            };
            ch.FlatAppearance.BorderSize = 0;
            return ch;
        }
        private void IncidentAnimal_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            SimulationConnection.IncidentAnimal(btn.Text);
        }

        private void PKPPK_Click(object sender, EventArgs e)
        {
            SimulationConnection.PKPPK(aircraft.callsign);
            aircraft.pkppk = true;
            Init(aircraft);
        }

        private void IncidentBird_Click(object sender, EventArgs e)
        {
            SimulationConnection.IncidentBird(aircraft.callsign);
            aircraft.incident_bird = true;
            Init(aircraft);
        }
    }
}
