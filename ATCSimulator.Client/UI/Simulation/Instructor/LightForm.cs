﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    public partial class LightForm : Telerik.WinControls.UI.RadForm
    {
        public LightForm()
        {
            InitializeComponent();
        }
        public void Setup(Dictionary<string,bool> approach,Dictionary<string,bool> runways,bool taxi,bool papi)
        {
            
            int approach_counter = 0;
            int _app_counter = 0;
            foreach (var r in approach)
            {
                int separation = 15;
                int width = (ApproachGroup.Size.Width - separation) / (approach.Count / 2);
                int locationY = 30;
                int locationX = (width * _app_counter) + separation;
                if (approach_counter % 2 == 1)
                {
                    _app_counter++;
                    locationY = 65;
                }
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = locationX,
                    Y = locationY
                };
                CheckBox ch = CreateCheckBox("approach",r.Value, location, size, r.Key);
                ch.Click += new System.EventHandler(ApproachLight_Click);
                ApproachGroup.Controls.Add(ch);
                approach_counter++;
            }
            int runway_counter = 0;
            foreach (var r in runways)
            {
                int separation = 15;
                int width = (RunwayGroup.Size.Width - separation) / runways.Count;
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = (width * runway_counter) + separation,
                    Y = 22
                };
                CheckBox ch = CreateCheckBox("runway",r.Value, location, size, r.Key);
                ch.Click += new System.EventHandler(RunwayLight_Click);
                RunwayGroup.Controls.Add(ch);
                runway_counter++;
            }

            TaxiLight.Checked = taxi;
            PapiLight.Checked = papi;
        }
        private CheckBox CreateCheckBox(string name, bool check, Point location, Size size, string text)
        {
            CheckBox ch = new CheckBox()
            {
                Appearance = Appearance.Button,
                BackColor = System.Drawing.Color.Tomato,
                Checked = check,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Century Gothic", 12F),
                ForeColor = System.Drawing.Color.White,
                Location = location,
                Text = text,
                Name = name + text,
                Size = size,
                TextAlign = ContentAlignment.MiddleCenter
            };
            ch.FlatAppearance.BorderSize = 0;
            ch.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            return ch;
        }
        private void TaxiLight_Click(object sender, EventArgs e)
        {
            Services.SimulationConnection.LightTaxi(TaxiLight.Checked);
        }

        private void PapiLight_Click(object sender, EventArgs e)
        {
            Services.SimulationConnection.LightPapi(PapiLight.Checked);
        }
        private void RunwayLight_Click(object sender,EventArgs e)
        {
            CheckBox obj = (CheckBox)sender;
            Services.SimulationConnection.LightRunway(obj.Text,obj.Checked);
        }
        private void ApproachLight_Click(object sender, EventArgs e)
        {
            CheckBox obj = (CheckBox)sender;
            Services.SimulationConnection.LightApproach(obj.Text, obj.Checked);
        }
        public void TaxiLightChangeChecked(bool value)
        {
            TaxiLight.Checked = value;
        }
        public void PapiLightChangeChecked(bool value)
        {
            PapiLight.Checked = value;
        }
        public void RunwayLightChangeChecked(string name, bool value)
        {
            CheckBox checkbox = (CheckBox)this.Controls.Find("runway" + name,true).FirstOrDefault();
            if (checkbox != null)
                checkbox.Checked = value;
        }
        public void ApproachLightChangeChecked(string name, bool value)
        {
            CheckBox checkbox = (CheckBox)this.Controls.Find("approach"+name, true).FirstOrDefault();
            if (checkbox != null)
                checkbox.Checked = value;
        }
    }
}
