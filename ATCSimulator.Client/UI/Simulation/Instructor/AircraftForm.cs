﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.UI.Other;

namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    public partial class AircraftForm : Telerik.WinControls.UI.RadForm
    {
        private AircraftData aircraft;
        public AircraftForm()
        {
            InitializeComponent();
        }

        public void Init(AircraftData aircraft)
        {
            this.aircraft = aircraft;
            TypeLabel.Text = aircraft.properties.type;
            if (aircraft.engine_failure > 0 ? true : false)
            {
                EngineFailureGroup.Enabled = false;
                EngineFailureBtn.BackColor = Color.Tomato;
            }
            else
            {
                EngineFailureGroup.Enabled = true ;
                EngineNumber.Maximum = aircraft.properties.engine;
                EngineNumber.Value = 1;
                EngineFailureBtn.BackColor = Color.DodgerBlue;
            }
            if (aircraft.landing_gear_jam)
            {
                LandingGearJam.Enabled = false;
                LandingGearJam.BackColor = Color.Tomato;
            }
            else
            {
                LandingGearJam.Enabled = aircraft.landing_gear_jam_enabled;
                LandingGearJam.BackColor = Color.DodgerBlue;
            }
            AircraftFuelHour.Value = aircraft.fuel.Hours;
            AircraftFuelMin.Value = aircraft.fuel.Minutes;
        }

        private void ChangeFuelBtn_Click(object sender, EventArgs e)
        {
            TimeSpan fuel = new TimeSpan(Convert.ToInt32(AircraftFuelHour.Value), Convert.ToInt32(AircraftFuelMin.Value), 0);
            SimulationConnection.ChangeFuel(aircraft.callsign, fuel);
        }
        private void EngineFailureBtn_Click(object sender, EventArgs e)
        {
            SimulationConnection.EngineFailure(aircraft.callsign, Convert.ToInt32(EngineNumber.Value));
        }

        private void RemoveAircraftBtn_Click(object sender, EventArgs e)
        {
            SimulationConnection.RemoveAircraft(aircraft.callsign);
        }

        private void LandingGearJam_Click(object sender, EventArgs e)
        {
            SimulationConnection.LandingGearJam(aircraft.callsign);
        }

        private void FlightPlanButton_Click(object sender, EventArgs e)
        {
            if (aircraft != null)
            {
                FlightPlanModel data = new FlightPlanModel();
                data.flight_rules = aircraft.pilot_properties.flight.flight_route.flight_rules;
                data.callsign = aircraft.callsign;
                data.type_aircraft = aircraft.properties.category;
                data.true_speed = HelperClient.ConvertSpeed("N", aircraft.properties.true_speed);
                data.flight_type = aircraft.properties.flight_type;
                data.departure_time = aircraft.departure_time;
                data.altitude = aircraft.properties.flight_level;
                data.flight_level = aircraft.properties.flight_level_type;
                data.routes = aircraft.pilot_properties.flight.flight_route.routes_name.Aggregate((i, j) => i + " " + j);
                data.destination_airport_code = aircraft.pilot_properties.flight.flight_route.destination_airport_code;
                data.destination_airport_name = aircraft.pilot_properties.flight.flight_route.destination_airport_name;
                data.origin_airport_code = aircraft.pilot_properties.flight.flight_route.origin_airport_code;
                data.origin_airport_name = aircraft.pilot_properties.flight.flight_route.origin_airport_name;
                data.alternate_airport_name = aircraft.pilot_properties.flight.flight_route.alternate_airport_name;
                data.fuel_board = aircraft.fuel;
                data.pilot = new PilotInfoModel()
                {
                    address_city = aircraft.user.address,
                    airport_city = aircraft.user.home_base,
                    name = aircraft.user.first_name + " " + aircraft.user.last_name,
                    phone = aircraft.user.phone
                };
                data.person = aircraft.properties.person;
                data.strips = aircraft.properties.livery_strips;
                FlightPlanForm flight_plan = new FlightPlanForm(data);
                flight_plan.StartPosition = FormStartPosition.CenterScreen;
                flight_plan.ShowDialog();
            }
        }

    }
}
