﻿namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    partial class LightForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LightsGroup = new System.Windows.Forms.GroupBox();
            this.ApproachGroup = new System.Windows.Forms.GroupBox();
            this.TaxiGroup = new System.Windows.Forms.GroupBox();
            this.PapiLight = new System.Windows.Forms.CheckBox();
            this.TaxiLight = new System.Windows.Forms.CheckBox();
            this.RunwayGroup = new System.Windows.Forms.GroupBox();
            this.LightsGroup.SuspendLayout();
            this.TaxiGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // LightsGroup
            // 
            this.LightsGroup.Controls.Add(this.ApproachGroup);
            this.LightsGroup.Controls.Add(this.TaxiGroup);
            this.LightsGroup.Controls.Add(this.RunwayGroup);
            this.LightsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.LightsGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LightsGroup.Location = new System.Drawing.Point(0, 0);
            this.LightsGroup.Name = "LightsGroup";
            this.LightsGroup.Size = new System.Drawing.Size(378, 280);
            this.LightsGroup.TabIndex = 157;
            this.LightsGroup.TabStop = false;
            this.LightsGroup.Text = "Lights Properties";
            // 
            // ApproachGroup
            // 
            this.ApproachGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ApproachGroup.ForeColor = System.Drawing.Color.White;
            this.ApproachGroup.Location = new System.Drawing.Point(20, 153);
            this.ApproachGroup.Name = "ApproachGroup";
            this.ApproachGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ApproachGroup.Size = new System.Drawing.Size(334, 108);
            this.ApproachGroup.TabIndex = 4;
            this.ApproachGroup.TabStop = false;
            this.ApproachGroup.Text = "Approach";
            // 
            // TaxiGroup
            // 
            this.TaxiGroup.Controls.Add(this.PapiLight);
            this.TaxiGroup.Controls.Add(this.TaxiLight);
            this.TaxiGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiGroup.ForeColor = System.Drawing.Color.White;
            this.TaxiGroup.Location = new System.Drawing.Point(19, 93);
            this.TaxiGroup.Name = "TaxiGroup";
            this.TaxiGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TaxiGroup.Size = new System.Drawing.Size(335, 60);
            this.TaxiGroup.TabIndex = 3;
            this.TaxiGroup.TabStop = false;
            this.TaxiGroup.Text = "Taxi / Papi";
            // 
            // PapiLight
            // 
            this.PapiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.PapiLight.BackColor = System.Drawing.Color.Tomato;
            this.PapiLight.FlatAppearance.BorderSize = 0;
            this.PapiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.PapiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PapiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PapiLight.ForeColor = System.Drawing.Color.White;
            this.PapiLight.Location = new System.Drawing.Point(175, 22);
            this.PapiLight.Name = "PapiLight";
            this.PapiLight.Size = new System.Drawing.Size(145, 30);
            this.PapiLight.TabIndex = 108;
            this.PapiLight.Text = "Papi";
            this.PapiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PapiLight.UseVisualStyleBackColor = false;
            this.PapiLight.Click += new System.EventHandler(this.PapiLight_Click);
            // 
            // TaxiLight
            // 
            this.TaxiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.TaxiLight.BackColor = System.Drawing.Color.Tomato;
            this.TaxiLight.FlatAppearance.BorderSize = 0;
            this.TaxiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.TaxiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaxiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiLight.ForeColor = System.Drawing.Color.White;
            this.TaxiLight.Location = new System.Drawing.Point(15, 22);
            this.TaxiLight.Name = "TaxiLight";
            this.TaxiLight.Size = new System.Drawing.Size(145, 30);
            this.TaxiLight.TabIndex = 107;
            this.TaxiLight.Text = "Taxi";
            this.TaxiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TaxiLight.UseVisualStyleBackColor = false;
            this.TaxiLight.Click += new System.EventHandler(this.TaxiLight_Click);
            // 
            // RunwayGroup
            // 
            this.RunwayGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RunwayGroup.ForeColor = System.Drawing.Color.White;
            this.RunwayGroup.Location = new System.Drawing.Point(19, 32);
            this.RunwayGroup.Name = "RunwayGroup";
            this.RunwayGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RunwayGroup.Size = new System.Drawing.Size(335, 60);
            this.RunwayGroup.TabIndex = 2;
            this.RunwayGroup.TabStop = false;
            this.RunwayGroup.Text = "Runway";
            // 
            // LightForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(378, 280);
            this.Controls.Add(this.LightsGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LightForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "GroundForm";
            this.ThemeName = "ControlDefault";
            this.LightsGroup.ResumeLayout(false);
            this.TaxiGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox LightsGroup;
        private System.Windows.Forms.GroupBox ApproachGroup;
        private System.Windows.Forms.GroupBox TaxiGroup;
        private System.Windows.Forms.CheckBox PapiLight;
        private System.Windows.Forms.CheckBox TaxiLight;
        private System.Windows.Forms.GroupBox RunwayGroup;



    }
}
