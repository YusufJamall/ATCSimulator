﻿namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    partial class ActivateArrivalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.DepartureTime = new System.Windows.Forms.Label();
            this.ActiveAircraft = new System.Windows.Forms.Button();
            this.StartingTime = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.StartingPosition = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(249, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Departure Time ";
            // 
            // DepartureTime
            // 
            this.DepartureTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.DepartureTime.ForeColor = System.Drawing.Color.DodgerBlue;
            this.DepartureTime.Location = new System.Drawing.Point(166, 182);
            this.DepartureTime.Name = "DepartureTime";
            this.DepartureTime.Size = new System.Drawing.Size(351, 35);
            this.DepartureTime.TabIndex = 2;
            this.DepartureTime.Text = "00:02:00";
            this.DepartureTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ActiveAircraft
            // 
            this.ActiveAircraft.BackColor = System.Drawing.Color.DodgerBlue;
            this.ActiveAircraft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.ActiveAircraft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.ActiveAircraft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ActiveAircraft.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ActiveAircraft.ForeColor = System.Drawing.Color.White;
            this.ActiveAircraft.Location = new System.Drawing.Point(229, 305);
            this.ActiveAircraft.Name = "ActiveAircraft";
            this.ActiveAircraft.Size = new System.Drawing.Size(227, 45);
            this.ActiveAircraft.TabIndex = 105;
            this.ActiveAircraft.Text = "Start Aircraft";
            this.ActiveAircraft.UseVisualStyleBackColor = false;
            this.ActiveAircraft.Click += new System.EventHandler(this.ActiveAircraft_Click);
            // 
            // StartingTime
            // 
            this.StartingTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.StartingTime.ForeColor = System.Drawing.Color.DodgerBlue;
            this.StartingTime.Location = new System.Drawing.Point(166, 260);
            this.StartingTime.Name = "StartingTime";
            this.StartingTime.Size = new System.Drawing.Size(351, 35);
            this.StartingTime.TabIndex = 107;
            this.StartingTime.Text = "00:02:00";
            this.StartingTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(264, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 26);
            this.label3.TabIndex = 106;
            this.label3.Text = "Starting Time";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StartingPosition
            // 
            this.StartingPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.StartingPosition.ForeColor = System.Drawing.Color.Tomato;
            this.StartingPosition.Location = new System.Drawing.Point(166, 393);
            this.StartingPosition.Name = "StartingPosition";
            this.StartingPosition.Size = new System.Drawing.Size(351, 35);
            this.StartingPosition.TabIndex = 109;
            this.StartingPosition.Text = "Prada";
            this.StartingPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(250, 362);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(171, 26);
            this.label5.TabIndex = 108;
            this.label5.Text = "Starting Position";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ActivateArrivalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(708, 700);
            this.Controls.Add(this.StartingPosition);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.StartingTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ActiveAircraft);
            this.Controls.Add(this.DepartureTime);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ActivateArrivalForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "GroundForm";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label DepartureTime;
        private System.Windows.Forms.Button ActiveAircraft;
        private System.Windows.Forms.Label StartingTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label StartingPosition;
        private System.Windows.Forms.Label label5;

    }
}

