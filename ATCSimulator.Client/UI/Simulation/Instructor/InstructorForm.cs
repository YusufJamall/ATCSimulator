﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.UI.Other;
using Newtonsoft.Json;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.UI.Simulation.Pilot;
using ATCSimulator.Client.Services;
using System.Configuration;

namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    public partial class InstructorForm : Telerik.WinControls.UI.RadForm
    {
        enum InstructorEnum
        {
            Light,
            Incident,
            Environment,
            Aircraft
        }

        GroundForm ground_form;
        FlightForm flight_form;
        DepartureWaitForm departure_wait_form;
        ActivateArrivalForm flight_wait_form;
        CrashForm crash_form;

        AircraftForm aircraft_form;
        EnvironmentForm environment_form;
        LightForm light_form;
        IncidentForm incident_form;
        public static InstructorForm Instance { get; private set; }
        private FlightInfoForm flight_info_click = null;
        private ExerciseSimulationModel exercise;
        private AircraftData aircraft;
        private Dictionary<string, FlightInfoForm> flight_info_list = new Dictionary<string, FlightInfoForm>();
        InstructorEnum instructor_menu;
        public InstructorForm()
        {
            InitializeComponent();
            ground_form = new GroundForm();
            ground_form.TopLevel = false;

            flight_form = new FlightForm();
            flight_form.TopLevel = false;

            departure_wait_form = new DepartureWaitForm();
            departure_wait_form.TopLevel = false;

            flight_wait_form = new ActivateArrivalForm();
            flight_wait_form.TopLevel = false;

            crash_form = new CrashForm();
            crash_form.TopLevel = false;

            aircraft_form = new AircraftForm();
            aircraft_form.TopLevel = false;

            environment_form = new EnvironmentForm();
            environment_form.TopLevel = false;

            light_form = new LightForm();
            light_form.TopLevel = false;

            incident_form = new IncidentForm();
            incident_form.TopLevel = false;

            Instance = this;
            CommandPanel.Controls.Add(ground_form);
            CommandPanel.Controls.Add(flight_form);
            CommandPanel.Controls.Add(departure_wait_form);
            CommandPanel.Controls.Add(flight_wait_form);
            CommandPanel.Controls.Add(crash_form);

            InstructorPanel.Controls.Add(incident_form);
            InstructorPanel.Controls.Add(light_form);
            InstructorPanel.Controls.Add(environment_form);
            InstructorPanel.Controls.Add(aircraft_form);
        }
        public void Init(ExerciseSimulationModel _exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_routes, List<DataAppronModel> aapprons, string pc_role, Image image, string name, List<string> ilsRoute)
        {
            flight_info_list.Clear();
            this.exercise = _exercise;
            PCRoleLabel.Text = pc_role;
            FullnameLabel.Text = name;
            UserPicture.Image = image;
            SceneryLabel.Text = _exercise.scenery_name + " Airport";
            List<string> runways = exercise.approach_lights.Keys.ToList();
            incident_form.Setup(runways);
            light_form.Setup(_exercise.approach_lights, _exercise.runway_lights, _exercise.taxi, _exercise.papi);
            environment_form.Setup(exercise);

            ground_form.LoadData(flight_routes, aapprons, exercise.scenery_name, exercise.scenery_id);
            flight_form.LoadData(flight_routes, runways, exercise.scenery_name, exercise.scenery_id, ilsRoute);
            aircrafts.OrderBy(d => d.start_time);
            int aircraft_count = 0;
            foreach (AircraftData ac in aircrafts)
            {
                FlightInfoForm a = new FlightInfoForm(this, ac);
                a.TopLevel = false;
                a.Location = new System.Drawing.Point(0, aircraft_count * 85);
                AircraftListPanel.Controls.Add(a);
                a.Show();
                a.Dock = DockStyle.None;
                a.BringToFront();
                aircraft_count++;
                a.InfoStatus(ac.is_enabled ? FlightInfoForm.FlightInfoStatus.ENABLE : FlightInfoForm.FlightInfoStatus.DISABLE);
                if (ac.crashed)
                    a.InfoStatus(FlightInfoForm.FlightInfoStatus.CRASH);
                if (ac.removed)
                    a.InfoStatus(FlightInfoForm.FlightInfoStatus.REMOVE);
                flight_info_list.Add(ac.callsign, a);
            }
            EnvironmentBtn.Checked = true;
        }

        public void ClickEventAircraft(AircraftData aircraft, FlightInfoForm _flight_info)
        {
            if (flight_info_click != null && flight_info_click != _flight_info)
                flight_info_click.UnClickAircraft();

            this.aircraft = aircraft;
            this.flight_info_click = _flight_info;
            //send to visual
            Helper.UserComputerInfo user_info = HelperClient.GetComputerInfo();
            SimulationConnection.SelectAircraft(aircraft.callsign, user_info.role_pc, user_info.pc_name);

            AircraftBtn.Enabled = true;
            AircraftInfo.Visible = true;
            AircraftLiveryImage.Image = HelperClient.GetImageLivery(aircraft.properties.livery, true); ;
            AircraftLiveryLabel.Text = aircraft.properties.livery;
            AircraftModelLabel.Text = aircraft.properties.model;
            AircraftCallsignLabel.Text = aircraft.callsign;
            aircraft_form.Init(aircraft);
            incident_form.Init(aircraft);

            SetCommandMenu();

        }

        private void SetCommandMenu()
        {
            HideMenu();
            if (aircraft.pilot_properties.status.Equals(PilotCommandMenu.DepartureReady))
                ShowDepartureWait();
            else if (aircraft.pilot_properties.status.Equals(PilotCommandMenu.FlightReady))
                ShowFlightWait();
            else if (aircraft.pilot_properties.status.Equals(PilotCommandMenu.Ground))
                ShowGround();
            else if (aircraft.pilot_properties.status.Equals(PilotCommandMenu.Flight))
                ShowFlight();
        }

        private void ShowDepartureWait()
        {
            if (aircraft.crashed)
            {
                crash_form.Show();
                crash_form.Dock = DockStyle.Top;
                crash_form.BringToFront();
            }
            else
            {
                departure_wait_form.Setup(aircraft);
                departure_wait_form.Show();
                departure_wait_form.Dock = DockStyle.Top;
                departure_wait_form.BringToFront();
            }
        }
        private void ShowFlightWait()
        {
            if (aircraft.crashed)
            {
                crash_form.Show();
                crash_form.Dock = DockStyle.Top;
                crash_form.BringToFront();
            }
            else
            {
                flight_wait_form.Setup(aircraft, true);
                flight_wait_form.Show();
                flight_wait_form.Dock = DockStyle.Top;
                flight_wait_form.BringToFront();
            }
        }

        private void ShowGround()
        {
            if (aircraft.crashed)
            {
                crash_form.Show();
                crash_form.Dock = DockStyle.Top;
                crash_form.BringToFront();
            }
            else
            {
                ground_form.Show();
                ground_form.Dock = DockStyle.Top;
                ground_form.BringToFront();
                ground_form.Init(aircraft);
            }
        }

        private void ShowFlight()
        {
            if (aircraft.crashed)
            {
                crash_form.Show();
                crash_form.Dock = DockStyle.Top;
                crash_form.BringToFront();
            }
            else
            {
                flight_form.Show();
                flight_form.Dock = DockStyle.Top;
                flight_form.BringToFront();
                flight_form.Init(aircraft);
            }
        }

        private void HideMenu()
        {
            crash_form.Hide();
            ground_form.Hide();
            departure_wait_form.Hide();
            flight_wait_form.Hide();
            flight_form.Hide();
        }

        #region Instructor Menu
        private void PauseSimulationBtn_CheckedChanged(object sender, EventArgs e)
        {
            SimulationConnection.PauseSimulation(PauseSimulationBtn.Checked);
        }
        public void PauseSimulation(bool value)
        {
            if (value)
            {
                PauseSimulationBtn.Text = "Resume Simulation";
                SimulationLog.SelectionColor = Color.Blue;
                SimulationLog.SelectedText = "Simulation is paused at " + DateTime.Now + " \n";
            }
            else
            {
                PauseSimulationBtn.Text = "Pause Simulation";
                SimulationLog.SelectionColor = Color.Blue;
                SimulationLog.SelectedText = "Simulation is resumed at " + DateTime.Now + " \n";
            }
        }
        private void StopSimulationBtn_Click(object sender, EventArgs e)
        {
            SimulationConnection.StopSimulation();
        }
        private void EnvironmentBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (EnvironmentBtn.Checked && instructor_menu != InstructorEnum.Environment)
            {
                instructor_menu = InstructorEnum.Environment;
                HideInstructorMenu();
                environment_form.Show();
                environment_form.Dock = DockStyle.Top;
                environment_form.BringToFront();
            }
        }
        private void IncidentBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (IncidentBtn.Checked && instructor_menu != InstructorEnum.Incident)
            {
                instructor_menu = InstructorEnum.Incident;
                HideInstructorMenu();
                incident_form.Show();
                incident_form.Dock = DockStyle.Top;
                incident_form.BringToFront();
            }
        }
        private void LightsBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (LightsBtn.Checked && instructor_menu != InstructorEnum.Light)
            {
                instructor_menu = InstructorEnum.Light;
                HideInstructorMenu();
                light_form.Show();
                light_form.Dock = DockStyle.Top;
                light_form.BringToFront();
            }
        }
        private void AircraftBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (AircraftBtn.Checked && instructor_menu != InstructorEnum.Aircraft)
            {
                instructor_menu = InstructorEnum.Aircraft;
                HideInstructorMenu();
                aircraft_form.Show();
                aircraft_form.Dock = DockStyle.Top;
                aircraft_form.BringToFront();
            }
        }
        private void HideInstructorMenu()
        {
            environment_form.Hide();
            light_form.Hide();
            incident_form.Hide();
            aircraft_form.Hide();
        }

        #endregion

        #region Callback

        #region Other Callback

        #region Remove Aircraft
        public void RemoveAircraft(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value.InfoStatus(FlightInfoForm.FlightInfoStatus.REMOVE);
                    ac.Value._aircraft.removed = true;
                    SimulationLog.SelectionColor = Color.Blue;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " is removed at " + DateTime.Now + " \n";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = null;
                        AircraftBtn.Enabled = false;
                        AircraftInfo.Visible = false;
                        incident_form.Refresh();
                        EnvironmentBtn.Checked = true;
                        flight_info_click = null;
                        HideMenu();
                    }
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Aircraft " + callsign + " is removed from play at " + DateTime.Now + " \n";
        }
        #endregion

        #region Incident Animal
        public void IncidentAnimal(bool value, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                ac.Value._aircraft.animal_crossing_enabled = value;
                if (ac.Value.Equals(flight_info_click))
                {
                    aircraft = ac.Value._aircraft;
                    incident_form.Init(aircraft);
                }
            }
            SimulationLog.SelectionColor = value ? Color.Blue : Color.Red;
            SimulationLog.SelectedText = value ? "Animal Crossing in runway " + runway + " is finish at " + DateTime.Now + " \n" : "There's Animal Crossing in runway " + runway + " at " + DateTime.Now + " \n";
        }
        #endregion

        #region PKPPK
        public void PKPPK(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pkppk = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        incident_form.Init(aircraft);
                    }
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "PKPPK is arriving to aircraft " + callsign + " at " + DateTime.Now + " \n";
        }
        #endregion

        #region Bird Attack
        public void BirdAttack(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.incident_bird = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        incident_form.Init(aircraft);
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Red;
            SimulationLog.SelectedText = "Aircraft " + callsign + " is on Bird Attack at " + DateTime.Now + " \n";
        }
        #endregion

        #region Bird Attack Value
        public void BirdAttackValue(string callsign, bool value)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.incident_bird_enable = value;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        incident_form.Init(aircraft);
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }

        }
        #endregion

        #region Landing Gear Jam Value
        public void LandingGearJamValue(string callsign, bool value)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.landing_gear_jam_enabled = value;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        incident_form.Init(aircraft);
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }

        }
        #endregion

        #region Landing Gear Jam
        public void LandingGearJam(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.landing_gear_jam = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        incident_form.Init(aircraft);
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Red;
            SimulationLog.SelectedText = "Aircraft " + callsign + " is Landing Gear Jam at " + DateTime.Now + " \n";
        }
        #endregion

        #region Engine Failure Callback
        public void EngineFailureCallback(string callsign, int engine)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.engine_failure = engine;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        aircraft_form.Init(aircraft);
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
            SimulationLog.SelectionColor = Color.Red;
            SimulationLog.SelectedText = "Aircraft " + callsign + " engine " + engine + " is failure at " + DateTime.Now + " \n";
        }
        #endregion

        #region Change Fuel Callback
        public void ChangeFuelCallback(string callsign, TimeSpan fuel)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.fuel = fuel;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        aircraft_form.Init(aircraft);
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                }
                break;
            }
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Aircraft " + callsign + " fuel is increased at " + DateTime.Now + " \n";
        }
        #endregion

        #region Environtment Callback
        public void ChangeWeatherCallback(WeatherInfo weather)
        {
            exercise.weather = weather;
            environment_form.Setup(exercise);
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Weather is changed at " + DateTime.Now + " \n";
        }
        public void ChangeWindDirection(float value)
        {
            exercise.wind_direction = Convert.ToInt32(value);
            environment_form.Setup(exercise);
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Wind Direction is changed at " + DateTime.Now + " \n";
        }
        public void ChangeWindSpeed(float value)
        {
            exercise.wind_speed = Convert.ToInt32(value);
            environment_form.Setup(exercise);
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Wind Speed is changed at " + DateTime.Now + " \n";
        }
        public void ChangeTemperature(float value)
        {
            exercise.temperature = Convert.ToInt32(value);
            environment_form.Setup(exercise);
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Temperature is changed at " + DateTime.Now + " \n";
        }
        public void ChangeVisibility(float value)
        {
            exercise.visibility = Convert.ToInt32(value);
            environment_form.Setup(exercise);
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Visibility is changed at " + DateTime.Now + " \n";
        }
        public void ChangeTimeSimulation(TimeSpan time)
        {
            exercise.time_simulation = time;
            environment_form.Setup(exercise);
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = "Time is changed at " + DateTime.Now + " \n";
        }
        #endregion

        #region Light Callback

        #region Taxi Light Callback

        public void TaxiLightCallback(bool value)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    light_form.TaxiLightChangeChecked(value);
                }));
            }
            else
            {
                light_form.TaxiLightChangeChecked(value);
            }
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Taxi light is on at " + DateTime.Now + " \n" : "Taxi light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #region Papi Light Callback

        public void PapiLightCallback(bool value)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    light_form.PapiLightChangeChecked(value);
                }));
            }
            else
            {
                light_form.PapiLightChangeChecked(value);
            }
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Papi light is on at " + DateTime.Now + " \n" : "Papi light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #region Runway Light Callback

        public void RunwayLightCallback(string name, bool value)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    light_form.RunwayLightChangeChecked(name, value);
                }));
            }
            else
            {
                light_form.RunwayLightChangeChecked(name, value);
            }
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Runway " + name + " light is on at " + DateTime.Now + " \n" : "Runway " + name + " light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #region Approach Light Callback

        public void ApproachLightCallback(string name, bool value)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    light_form.ApproachLightChangeChecked(name, value);
                }));
            }
            else
            {
                light_form.ApproachLightChangeChecked(name, value);
            }
            SimulationLog.SelectionColor = Color.Green;
            SimulationLog.SelectedText = value ? "Approach " + name + " light is on at " + DateTime.Now + " \n" : "Approach " + name + " light is off at " + DateTime.Now + " \n";
        }

        #endregion

        #endregion

        #endregion

        #region System Callback
        public void UpdateTimeSimulation(TimeSpan time)
        {
            try
            {
                TimeSimulationLabel.Text = time.ToString(@"hh\:mm\:ss");
            }
            catch { }
        }
        public void ReadySimulationCallback()
        {
            PauseSimulationBtn.Enabled = true;
            StopSimulationBtn.Enabled = true;
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation is started at " + DateTime.Now + "\n";
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation Play time is : " + exercise.play_time + "\n";
        }
        public void ContinueSimulationCallback(bool isPaused)
        {
            PauseSimulationBtn.Checked = isPaused;
            if (isPaused)
            {
                PauseSimulationBtn.Text = "Resume Simulation";
            }
            else
                PauseSimulationBtn.Text = "Pause Simulation";
            PauseSimulationBtn.Enabled = true;
            StopSimulationBtn.Enabled = true;
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation is continue at " + DateTime.Now + "\n";
            SimulationLog.SelectionColor = Color.Blue;
            SimulationLog.SelectedText = "Simulation Play time is : " + exercise.play_time + "\n";
        }
        #endregion

        #region System Ground

        #region Change Ground Status
        public void ChangeGroundStatusCallback(string callsign, int status)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;
                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.None;
                    ac.Value._aircraft.pilot_properties.ground.taxi.routes = new List<string>();
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Route
        public void ChangeFlightRouteCallback(string callsign, FlightRouteListModel routes, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.flight_route = routes;
                    ac.Value._aircraft.pilot_properties.ground.runway.runway = runway;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Parking Bay
        public void ChangeParkingAppronCallback(string callsign, DataAppronModel terminal)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.parking.parking_id = terminal.id;
                    ac.Value._aircraft.pilot_properties.ground.parking.terminal_name = terminal.terminal_name;
                    ac.Value._aircraft.pilot_properties.ground.parking.appron_name = terminal.appron_name;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Ready Aircraft
        public void ReadyAircraftCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ready_departure.is_enabled = false;
                    ac.Value._aircraft.status = AircraftStatus.Form;
                    ac.Value.ChangeStatus(AircraftStatus.Form);
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Runway
        public void ChangeRunwayCallback(string callsign, string runway, bool is_ground)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    if (is_ground)
                        ac.Value._aircraft.pilot_properties.ground.runway.runway = runway;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        if (is_ground)
                            aircraft.pilot_properties.ground.runway.runway = runway;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Engine
        public void StartEngineCallback(string callsign, bool engine)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine = engine;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.ground.engine = engine;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Pushback
        public void PushbackCallback(string callsign, DirectionInfo pushback, string pushback_name)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.pushback.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.pushback.status = CommandStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.pushback.pushback_left = pushback.Equals(DirectionInfo.Left) ? true : false;
                    ac.Value._aircraft.pilot_properties.ground.pushback.pushback_right = pushback.Equals(DirectionInfo.Right) ? true : false;
                    ac.Value._aircraft.from_position = ac.Value._aircraft.current_position;
                    ac.Value._aircraft.current_position = pushback_name;
                    ac.Value._aircraft.status = AircraftStatus.Pushback;
                    ac.Value.ChangeStatus(AircraftStatus.Pushback);
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Taxi
        public void StartTaxiCallback(string callsign, List<string> routes, int status)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.routes = routes;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    ac.Value._aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;
                    if (ac.Value._aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
                    {
                        ac.Value._aircraft.status = AircraftStatus.Taxing;
                        ac.Value.ChangeStatus(AircraftStatus.Taxing);
                    }
                    else
                    {
                        ac.Value._aircraft.status = AircraftStatus.Parking;
                        ac.Value.ChangeStatus(AircraftStatus.Parking);
                    }
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Hold Taxi
        public void HoldTaxiCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Holding;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    ac.Value._aircraft.status = AircraftStatus.Holding;
                    ac.Value.ChangeStatus(AircraftStatus.Holding);
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region ContinueTaxi
        public void ContinueTaxiCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;

                    if (ac.Value._aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
                    {
                        ac.Value._aircraft.status = AircraftStatus.Taxing;
                        ac.Value.ChangeStatus(AircraftStatus.Taxing);
                    }
                    else
                    {
                        ac.Value._aircraft.status = AircraftStatus.Parking;
                        ac.Value.ChangeStatus(AircraftStatus.Parking);
                    }

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Speed Taxi
        public void ChangeSpeedTaxiCallback(string callsign, GroundSpeed speed)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.taxi.speed = speed;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.ground.taxi.speed = speed;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Cross Runway
        public void CrossRunwayCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Uturn
        public void UturnCallback(string callsign, UturnInfo uturn)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.other.u_turn_status = uturn;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region RockingWing
        public void RockingWingCallback(string callsign, bool value)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.other.rocking_wing = value;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.ground.other.rocking_wing = value;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Take Off
        public void TakeOffCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = true;
                    ac.Value._aircraft.status = AircraftStatus.TakeOff;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                    ac.Value.ChangeStatus(AircraftStatus.TakeOff);
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Runway Hold
        public void RunwayHoldCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #endregion

        #region System flight

        #region Active Aircraft
        public void ActiveAircraftCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route = true;
                    ac.Value._aircraft.pilot_properties.ready_flight.is_enabled = false;
                    ac.Value._aircraft.status = AircraftStatus.FlyingToDirection;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Flight;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Holding Now Callback
        public void HoldingNowCallback(string callsign, DirectionInfo direction)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    if (direction.Equals(DirectionInfo.Left))
                        ac.Value._aircraft.pilot_properties.flight.holding.holding_left = true;
                    else
                        ac.Value._aircraft.pilot_properties.flight.holding.holding_right = true;
                    ac.Value._aircraft.status = AircraftStatus.Holding;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Holding Position Callback
        public void HoldingPositionCallback(string callsign, string position)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.position_holding = position;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Continue Route Callback
        public void ContinueRouteCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.FlyingToRoute;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Continue Direction Callback
        public void ContinueDirectionCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.FlyingToDirection;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Heading Callback
        public void ChangeHeadingCallback(string callsign, HeadingInfo direction, float heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.FlyingToDirection;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    if (direction.Equals(HeadingInfo.Absolute))
                    {
                        ac.Value._aircraft.pilot_properties.flight.heading.absolute_heading = true;
                        ac.Value._aircraft.pilot_properties.flight.heading.absolute_heading_value = heading;
                    }
                    else
                    {
                        ac.Value._aircraft.pilot_properties.flight.heading.relative_heading = true;
                        ac.Value._aircraft.pilot_properties.flight.heading.relative_heading_value = heading;
                    }
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change Route Callback
        public void ChangeRouteCallback(string callsign, FlightRouteListModel routes)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.FlyingToRoute;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.flight_route = routes;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Direct Go Callback
        public void DirectGoCallback(string callsign, string position, bool isILS)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.FlyingToDirection;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.direct_go.directGo = true;
                    if (isILS)
                        ac.Value._aircraft.pilot_properties.flight.direct_go.positionILS = position;
                    else
                        ac.Value._aircraft.pilot_properties.flight.direct_go.position = position;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Instrument Approach Callback
        public void ApproachCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.Approach; ;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.can_changeRoute = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.change_runway.is_enabled = false;

                    ac.Value._aircraft.from_position = "RW" + runway;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Join Circuit Callback
        public void CircuitCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    aircraft.status = AircraftStatus.Circuit;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.can_changeRoute = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;

                    ac.Value._aircraft.from_position = "RW" + runway;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Missed Appraoch Callback
        public void MissedApproachCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Touch Go Callback
        public void TouchGoCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Flypass Callback
        public void FlypassCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.flypass = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Extend Downwind Callback
        public void ExtendDownwindCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Orbit Callback
        public void OrbitCallback(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Landing Callback
        public void LandingCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    if (runway.Equals(""))
                    {
                        ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                        ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                        ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                        ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                        ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    }

                    ac.Value._aircraft.pilot_properties.flight.other.landing = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landingRunway = runway;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.directGo = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Altitude Callback
        public void AltitudeCallback(string callsign, float altitude)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.altitude = altitude;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Speed Callback
        public void SpeedCallback(string callsign, float speed)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.speed = speed;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Change ILS Mode Callback
        public void ChangeILSModeCallback(string callsign, bool value)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.ILSmode = value;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #endregion

        #region Visual Ground Callback

        #region Finish Ready
        public void FinishReadyDeparture(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Pushback
        public void FinishPushback(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.pushback.status = CommandStatus.Finish;
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;
                    ac.Value._aircraft.heading = heading;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Update Taxi Position
        public void UpdateTaxiPosition(string callsign, string position, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.from_position = ac.Value._aircraft.current_position;
                    ac.Value._aircraft.current_position = position;
                    ac.Value._aircraft.heading = heading;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Taxi
        public void FinishTaxi(string callsign, string position, double heading, int status)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;

                    ac.Value._aircraft.heading = heading;
                    ac.Value._aircraft.from_position = ac.Value._aircraft.current_position;
                    ac.Value._aircraft.current_position = position;
                    ac.Value._aircraft.pilot_properties.ground.status = (GroundStatusInfo)status;
                    //if position can intersection show intersection

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.Finish;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Heading Aircraft
        public void HeadingAircraft(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.heading = heading;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.heading = heading;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region RequestCrossRunway
        public void RequestCrossRunway(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = true;

                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.CrossRunway;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.cross_runway = true;

                    ac.Value._aircraft.heading = heading;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Uturn
        public void FinishUTurn(string callsign, double heading)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.engine_enabled = true;
                    ac.Value._aircraft.pilot_properties.ground.runway.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = false;

                    ac.Value._aircraft.pilot_properties.ground.other.u_turn_status = UturnInfo.None;
                    ac.Value._aircraft.pilot_properties.ground.other.u_turn = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.status = TaxiStatus.None;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route = false;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    ac.Value._aircraft.from_position = "";
                    ac.Value._aircraft.heading = heading;

                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Runway Hold
        public void FinishHoldTakeOff(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.None;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Take Off
        public void FinishTakeOff(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.status = AircraftStatus.TakeOff;
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Flight;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Finish;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Crash Aircraft
        public void CrashAircraft(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value.InfoStatus(FlightInfoForm.FlightInfoStatus.CRASH);
                    ac.Value._aircraft.crashed = true;
                    SimulationLog.SelectionColor = Color.Red;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " is crashed at " + DateTime.Now + " \n";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                        incident_form.Init(aircraft);
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Parking
        public void FinishParking(AircraftData newAircraftData)
        {
            foreach (var ac in flight_info_list)
            {
                if (newAircraftData.callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = newAircraftData;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Backtrack
        public void Backtrack(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.ground.intersection.is_show = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.runway_hold = true;
                    ac.Value._aircraft.pilot_properties.ground.intersection.take_off = false;
                    ac.Value._aircraft.pilot_properties.ground.intersection.status = CommandStatus.Process;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #endregion

        #region Visual Flight Callback

        #region Cannot Change Speed
        public void CannotChangeSpeed(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.can_change_speed = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Cannot Change Altitude
        public void CannotChangeAltitude(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.can_change_altitude = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Update Performance
        public void UpdatePerformanceCallback(string callsign, float speed, float altitude)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.speed = speed;
                    ac.Value._aircraft.altitude = altitude;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Update Holding Point
        public void UpdateHoldingPoint(List<string> holding)
        {
            foreach (var ac in flight_info_list)
            {
                ac.Value._aircraft.pilot_properties.flight.holdingPoint = holding;
                if (ac.Value.Equals(flight_info_click))
                {
                    aircraft = ac.Value._aircraft;
                }
            }
        }
        #endregion

        #region Cannot Holding
        public void CannotHolding(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    SimulationLog.SelectionColor = Color.Red;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " Cannot Holding in " +
                        ac.Value._aircraft.pilot_properties.flight.holding.position_holding + " at " + DateTime.Now + " \n";
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.position_holding = "";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Holding At Callback
        public void HoldingAtCallback(string callsign, string position, bool isInstrument)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.Holding;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.position_holding = position;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = !isInstrument;
                    if (isInstrument)
                    {
                        ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = true;
                        ac.Value._aircraft.pilot_properties.flight.can_changeRoute = false;
                    }
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Route Update Callback
        public void RouteUpdateCallback(string callsign, FlightRouteListModel route)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.flight_route = route;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Ready Join Circuit
        public void ReadyJoinCircuit(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Join Circuit Callback Visual
        public void JoinCircuitCallback(string callsign, string runway)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.Circuit;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit = true;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.runway = runway;
                    ac.Value._aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.can_changeRoute = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_left_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holding_right_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.holdingAt_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.holding.continue_direction_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.is_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.change_runway.is_enabled = true;

                    ac.Value._aircraft.from_position = "RW" + runway;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Go Round
        public void FinishGoRound(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Approach
        public void StartApproach(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Go Round
        public void StartGoRound(string callsign, bool isFlypass, bool isIfr)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    if (isFlypass)
                    {
                        if (isIfr)
                        {
                            ac.Value._aircraft.status = AircraftStatus.MissedApproach;
                            ac.Value._aircraft.pilot_properties.flight.other.missed_approach = true;
                        }
                        else
                        {
                            ac.Value._aircraft.status = AircraftStatus.Flypass;
                            ac.Value._aircraft.pilot_properties.flight.other.flypass = true;
                        }
                    }
                    else
                    {
                        ac.Value._aircraft.status = AircraftStatus.TouchGo;
                        ac.Value._aircraft.pilot_properties.flight.other.touch_go = true;
                    }
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Cannot Landing
        public void CannotLanding(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.pilot_properties.flight.other.landingRunway = "";
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.heading.is_enabled = true;
                    ac.Value._aircraft.pilot_properties.flight.direct_go.directGo = true;
                    SimulationLog.SelectionColor = Color.Red;
                    SimulationLog.SelectedText = "Aircraft " + callsign + " Cannot Landing at " + DateTime.Now + " \n";
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Start Landing
        public void StartLanding(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.status = AircraftStatus.Landing;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.flight.other.missed_approach_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.touch_go_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.flypass_enabled = false;
                    ac.Value._aircraft.pilot_properties.flight.other.landing = true;
                    ac.Value._aircraft.pilot_properties.flight.other.landing_enabled = false;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Touch Ground
        public void TouchGround(string callsign)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft = HelperClient.ResetFlightStatus(ac.Value._aircraft);
                    ac.Value._aircraft.is_departure = true;
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = false;
                    ac.Value._aircraft.pilot_properties.ground.status = GroundStatusInfo.Parking;
                    ac.Value._aircraft.pilot_properties.status = PilotCommandMenu.Ground;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion

        #region Finish Landing
        public void FinishLandingCallback(string callsign, float heading,string from, string position)
        {
            foreach (var ac in flight_info_list)
            {
                if (callsign.Equals(ac.Key))
                {
                    ac.Value._aircraft.heading = heading;
                    ac.Value._aircraft.current_position = position;
                    //ac.Value._aircraft.from_position = from;
                    ac.Value._aircraft.status = AircraftStatus.Parking;
                    ac.Value.ChangeStatus(ac.Value._aircraft.status);
                    ac.Value._aircraft.pilot_properties.ground.taxi.taxi_route_enabled = true;
                    if (ac.Value.Equals(flight_info_click))
                    {
                        aircraft = ac.Value._aircraft;
                        SetCommandMenu();
                    }
                    else
                    {
                        ac.Value.BlinkAircraft();
                    }
                    break;
                }
            }
        }
        #endregion


        #endregion

        #endregion
    }
}
