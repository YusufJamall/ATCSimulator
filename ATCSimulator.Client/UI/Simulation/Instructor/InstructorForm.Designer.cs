﻿namespace ATCSimulator.Client.UI.Simulation.Instructor
{
    partial class InstructorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// aClean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.AircraftListPanel = new System.Windows.Forms.Panel();
            this.CommandPanel = new System.Windows.Forms.Panel();
            this.InstructorPanel = new System.Windows.Forms.Panel();
            this.InstructorMenu = new System.Windows.Forms.GroupBox();
            this.PauseSimulationBtn = new System.Windows.Forms.CheckBox();
            this.StopSimulationBtn = new System.Windows.Forms.Button();
            this.IncidentBtn = new System.Windows.Forms.RadioButton();
            this.EnvironmentBtn = new System.Windows.Forms.RadioButton();
            this.LightsBtn = new System.Windows.Forms.RadioButton();
            this.AircraftBtn = new System.Windows.Forms.RadioButton();
            this.footnote = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TimeSimulationLabel = new System.Windows.Forms.Label();
            this.SceneryLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FullnameLabel = new System.Windows.Forms.Label();
            this.PCRoleLabel = new System.Windows.Forms.Label();
            this.AircraftInfo = new System.Windows.Forms.Panel();
            this.AircraftLiveryLabel = new System.Windows.Forms.Label();
            this.AircraftModelLabel = new System.Windows.Forms.Label();
            this.AircraftCallsignLabel = new System.Windows.Forms.Label();
            this.AircraftLiveryImage = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.UserPicture = new System.Windows.Forms.PictureBox();
            this.LineBottom = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LineBlue = new System.Windows.Forms.PictureBox();
            this.SimulationLog = new System.Windows.Forms.RichTextBox();
            this.InstructorMenu.SuspendLayout();
            this.AircraftInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftLiveryImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 22);
            this.label5.TabIndex = 9;
            this.label5.Text = "Aircraft List";
            // 
            // AircraftListPanel
            // 
            this.AircraftListPanel.AutoScroll = true;
            this.AircraftListPanel.Location = new System.Drawing.Point(6, 38);
            this.AircraftListPanel.Name = "AircraftListPanel";
            this.AircraftListPanel.Size = new System.Drawing.Size(250, 700);
            this.AircraftListPanel.TabIndex = 32;
            // 
            // CommandPanel
            // 
            this.CommandPanel.Location = new System.Drawing.Point(262, 38);
            this.CommandPanel.Name = "CommandPanel";
            this.CommandPanel.Size = new System.Drawing.Size(708, 700);
            this.CommandPanel.TabIndex = 33;
            // 
            // InstructorPanel
            // 
            this.InstructorPanel.Location = new System.Drawing.Point(975, 291);
            this.InstructorPanel.Name = "InstructorPanel";
            this.InstructorPanel.Size = new System.Drawing.Size(378, 280);
            this.InstructorPanel.TabIndex = 117;
            // 
            // InstructorMenu
            // 
            this.InstructorMenu.Controls.Add(this.PauseSimulationBtn);
            this.InstructorMenu.Controls.Add(this.StopSimulationBtn);
            this.InstructorMenu.Controls.Add(this.IncidentBtn);
            this.InstructorMenu.Controls.Add(this.EnvironmentBtn);
            this.InstructorMenu.Controls.Add(this.LightsBtn);
            this.InstructorMenu.Controls.Add(this.AircraftBtn);
            this.InstructorMenu.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.InstructorMenu.ForeColor = System.Drawing.Color.DodgerBlue;
            this.InstructorMenu.Location = new System.Drawing.Point(974, 136);
            this.InstructorMenu.Name = "InstructorMenu";
            this.InstructorMenu.Size = new System.Drawing.Size(379, 149);
            this.InstructorMenu.TabIndex = 118;
            this.InstructorMenu.TabStop = false;
            this.InstructorMenu.Text = "Instructor Menu";
            // 
            // PauseSimulationBtn
            // 
            this.PauseSimulationBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.PauseSimulationBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.PauseSimulationBtn.Enabled = false;
            this.PauseSimulationBtn.FlatAppearance.BorderSize = 0;
            this.PauseSimulationBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.PauseSimulationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PauseSimulationBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.PauseSimulationBtn.ForeColor = System.Drawing.Color.White;
            this.PauseSimulationBtn.Location = new System.Drawing.Point(12, 27);
            this.PauseSimulationBtn.Name = "PauseSimulationBtn";
            this.PauseSimulationBtn.Size = new System.Drawing.Size(175, 35);
            this.PauseSimulationBtn.TabIndex = 104;
            this.PauseSimulationBtn.Text = "Pause Simulation";
            this.PauseSimulationBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PauseSimulationBtn.UseVisualStyleBackColor = false;
            this.PauseSimulationBtn.CheckedChanged += new System.EventHandler(this.PauseSimulationBtn_CheckedChanged);
            // 
            // StopSimulationBtn
            // 
            this.StopSimulationBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.StopSimulationBtn.Enabled = false;
            this.StopSimulationBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.StopSimulationBtn.FlatAppearance.BorderSize = 0;
            this.StopSimulationBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.StopSimulationBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.StopSimulationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StopSimulationBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.StopSimulationBtn.ForeColor = System.Drawing.Color.White;
            this.StopSimulationBtn.Location = new System.Drawing.Point(193, 27);
            this.StopSimulationBtn.Name = "StopSimulationBtn";
            this.StopSimulationBtn.Size = new System.Drawing.Size(175, 35);
            this.StopSimulationBtn.TabIndex = 5;
            this.StopSimulationBtn.Text = "Stop Simulation";
            this.StopSimulationBtn.UseVisualStyleBackColor = false;
            this.StopSimulationBtn.Click += new System.EventHandler(this.StopSimulationBtn_Click);
            // 
            // IncidentBtn
            // 
            this.IncidentBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.IncidentBtn.BackColor = System.Drawing.Color.Tomato;
            this.IncidentBtn.FlatAppearance.BorderSize = 0;
            this.IncidentBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.IncidentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IncidentBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.IncidentBtn.ForeColor = System.Drawing.Color.White;
            this.IncidentBtn.Location = new System.Drawing.Point(194, 107);
            this.IncidentBtn.Name = "IncidentBtn";
            this.IncidentBtn.Size = new System.Drawing.Size(175, 35);
            this.IncidentBtn.TabIndex = 3;
            this.IncidentBtn.Text = "Incident";
            this.IncidentBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.IncidentBtn.UseVisualStyleBackColor = false;
            this.IncidentBtn.CheckedChanged += new System.EventHandler(this.IncidentBtn_CheckedChanged);
            // 
            // EnvironmentBtn
            // 
            this.EnvironmentBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.EnvironmentBtn.BackColor = System.Drawing.Color.Tomato;
            this.EnvironmentBtn.FlatAppearance.BorderSize = 0;
            this.EnvironmentBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.EnvironmentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EnvironmentBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.EnvironmentBtn.ForeColor = System.Drawing.Color.White;
            this.EnvironmentBtn.Location = new System.Drawing.Point(12, 107);
            this.EnvironmentBtn.Name = "EnvironmentBtn";
            this.EnvironmentBtn.Size = new System.Drawing.Size(175, 35);
            this.EnvironmentBtn.TabIndex = 2;
            this.EnvironmentBtn.Text = "Environment";
            this.EnvironmentBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.EnvironmentBtn.UseVisualStyleBackColor = false;
            this.EnvironmentBtn.CheckedChanged += new System.EventHandler(this.EnvironmentBtn_CheckedChanged);
            // 
            // LightsBtn
            // 
            this.LightsBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.LightsBtn.BackColor = System.Drawing.Color.Tomato;
            this.LightsBtn.FlatAppearance.BorderSize = 0;
            this.LightsBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.LightsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LightsBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LightsBtn.ForeColor = System.Drawing.Color.White;
            this.LightsBtn.Location = new System.Drawing.Point(194, 67);
            this.LightsBtn.Name = "LightsBtn";
            this.LightsBtn.Size = new System.Drawing.Size(175, 35);
            this.LightsBtn.TabIndex = 1;
            this.LightsBtn.Text = "Lights";
            this.LightsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LightsBtn.UseVisualStyleBackColor = false;
            this.LightsBtn.CheckedChanged += new System.EventHandler(this.LightsBtn_CheckedChanged);
            // 
            // AircraftBtn
            // 
            this.AircraftBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.AircraftBtn.BackColor = System.Drawing.Color.Tomato;
            this.AircraftBtn.Enabled = false;
            this.AircraftBtn.FlatAppearance.BorderSize = 0;
            this.AircraftBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.AircraftBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AircraftBtn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.AircraftBtn.ForeColor = System.Drawing.Color.White;
            this.AircraftBtn.Location = new System.Drawing.Point(12, 67);
            this.AircraftBtn.Name = "AircraftBtn";
            this.AircraftBtn.Size = new System.Drawing.Size(175, 35);
            this.AircraftBtn.TabIndex = 0;
            this.AircraftBtn.Text = "Aircraft";
            this.AircraftBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AircraftBtn.UseVisualStyleBackColor = false;
            this.AircraftBtn.CheckedChanged += new System.EventHandler(this.AircraftBtn_CheckedChanged);
            // 
            // footnote
            // 
            this.footnote.AutoSize = true;
            this.footnote.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.footnote.ForeColor = System.Drawing.Color.White;
            this.footnote.Location = new System.Drawing.Point(4, 749);
            this.footnote.Name = "footnote";
            this.footnote.Size = new System.Drawing.Size(330, 16);
            this.footnote.TabIndex = 119;
            this.footnote.Text = "Copyright © 2015 Media Indo Teknologi | All Rights Reserved";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(973, 574);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 22);
            this.label1.TabIndex = 121;
            this.label1.Text = "Simulation Log";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(819, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 21);
            this.label2.TabIndex = 122;
            this.label2.Text = "Time :";
            // 
            // TimeSimulationLabel
            // 
            this.TimeSimulationLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.TimeSimulationLabel.ForeColor = System.Drawing.Color.White;
            this.TimeSimulationLabel.Location = new System.Drawing.Point(876, 6);
            this.TimeSimulationLabel.Name = "TimeSimulationLabel";
            this.TimeSimulationLabel.Size = new System.Drawing.Size(94, 21);
            this.TimeSimulationLabel.TabIndex = 123;
            this.TimeSimulationLabel.Text = "00:00:00";
            this.TimeSimulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SceneryLabel
            // 
            this.SceneryLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.SceneryLabel.ForeColor = System.Drawing.Color.White;
            this.SceneryLabel.Location = new System.Drawing.Point(364, 6);
            this.SceneryLabel.Name = "SceneryLabel";
            this.SceneryLabel.Size = new System.Drawing.Size(290, 21);
            this.SceneryLabel.TabIndex = 125;
            this.SceneryLabel.Text = "Seahorse";
            this.SceneryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(266, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 21);
            this.label4.TabIndex = 124;
            this.label4.Text = "Scenery :";
            // 
            // FullnameLabel
            // 
            this.FullnameLabel.AutoSize = true;
            this.FullnameLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.FullnameLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.FullnameLabel.Location = new System.Drawing.Point(1042, 38);
            this.FullnameLabel.Name = "FullnameLabel";
            this.FullnameLabel.Size = new System.Drawing.Size(42, 21);
            this.FullnameLabel.TabIndex = 128;
            this.FullnameLabel.Text = "User";
            // 
            // PCRoleLabel
            // 
            this.PCRoleLabel.AutoSize = true;
            this.PCRoleLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.PCRoleLabel.ForeColor = System.Drawing.Color.White;
            this.PCRoleLabel.Location = new System.Drawing.Point(1042, 12);
            this.PCRoleLabel.Name = "PCRoleLabel";
            this.PCRoleLabel.Size = new System.Drawing.Size(44, 21);
            this.PCRoleLabel.TabIndex = 127;
            this.PCRoleLabel.Text = "Role";
            // 
            // AircraftInfo
            // 
            this.AircraftInfo.Controls.Add(this.AircraftLiveryLabel);
            this.AircraftInfo.Controls.Add(this.AircraftModelLabel);
            this.AircraftInfo.Controls.Add(this.AircraftCallsignLabel);
            this.AircraftInfo.Controls.Add(this.AircraftLiveryImage);
            this.AircraftInfo.Location = new System.Drawing.Point(973, 75);
            this.AircraftInfo.Name = "AircraftInfo";
            this.AircraftInfo.Size = new System.Drawing.Size(380, 60);
            this.AircraftInfo.TabIndex = 132;
            this.AircraftInfo.Visible = false;
            // 
            // AircraftLiveryLabel
            // 
            this.AircraftLiveryLabel.AutoSize = true;
            this.AircraftLiveryLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.AircraftLiveryLabel.ForeColor = System.Drawing.Color.White;
            this.AircraftLiveryLabel.Location = new System.Drawing.Point(61, 6);
            this.AircraftLiveryLabel.Name = "AircraftLiveryLabel";
            this.AircraftLiveryLabel.Size = new System.Drawing.Size(75, 21);
            this.AircraftLiveryLabel.TabIndex = 134;
            this.AircraftLiveryLabel.Text = "Air Asia";
            // 
            // AircraftModelLabel
            // 
            this.AircraftModelLabel.AutoSize = true;
            this.AircraftModelLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.AircraftModelLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftModelLabel.Location = new System.Drawing.Point(61, 33);
            this.AircraftModelLabel.Name = "AircraftModelLabel";
            this.AircraftModelLabel.Size = new System.Drawing.Size(86, 21);
            this.AircraftModelLabel.TabIndex = 133;
            this.AircraftModelLabel.Text = "B737-400";
            // 
            // AircraftCallsignLabel
            // 
            this.AircraftCallsignLabel.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.AircraftCallsignLabel.ForeColor = System.Drawing.Color.White;
            this.AircraftCallsignLabel.Location = new System.Drawing.Point(242, 2);
            this.AircraftCallsignLabel.Name = "AircraftCallsignLabel";
            this.AircraftCallsignLabel.Size = new System.Drawing.Size(136, 55);
            this.AircraftCallsignLabel.TabIndex = 132;
            this.AircraftCallsignLabel.Text = "AWX 2021";
            this.AircraftCallsignLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AircraftLiveryImage
            // 
            this.AircraftLiveryImage.BackColor = System.Drawing.Color.Transparent;
            this.AircraftLiveryImage.Image = global::ATCSimulator.Client.Properties.Resources.LionAir_M;
            this.AircraftLiveryImage.Location = new System.Drawing.Point(3, 2);
            this.AircraftLiveryImage.Name = "AircraftLiveryImage";
            this.AircraftLiveryImage.Size = new System.Drawing.Size(55, 55);
            this.AircraftLiveryImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.AircraftLiveryImage.TabIndex = 131;
            this.AircraftLiveryImage.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox3.Location = new System.Drawing.Point(973, 599);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(380, 3);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 130;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox2.Location = new System.Drawing.Point(973, 69);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(380, 3);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 129;
            this.pictureBox2.TabStop = false;
            // 
            // UserPicture
            // 
            this.UserPicture.BackColor = System.Drawing.Color.Transparent;
            this.UserPicture.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            this.UserPicture.Location = new System.Drawing.Point(976, 6);
            this.UserPicture.Name = "UserPicture";
            this.UserPicture.Size = new System.Drawing.Size(60, 60);
            this.UserPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.UserPicture.TabIndex = 126;
            this.UserPicture.TabStop = false;
            // 
            // LineBottom
            // 
            this.LineBottom.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.LineBottom.Location = new System.Drawing.Point(5, 743);
            this.LineBottom.Name = "LineBottom";
            this.LineBottom.Size = new System.Drawing.Size(1348, 3);
            this.LineBottom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LineBottom.TabIndex = 120;
            this.LineBottom.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.lineWhite;
            this.pictureBox1.Location = new System.Drawing.Point(257, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(4, 735);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 116;
            this.pictureBox1.TabStop = false;
            // 
            // LineBlue
            // 
            this.LineBlue.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.LineBlue.Location = new System.Drawing.Point(7, 31);
            this.LineBlue.Name = "LineBlue";
            this.LineBlue.Size = new System.Drawing.Size(963, 4);
            this.LineBlue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LineBlue.TabIndex = 115;
            this.LineBlue.TabStop = false;
            // 
            // SimulationLog
            // 
            this.SimulationLog.Font = new System.Drawing.Font("Arial", 10F);
            this.SimulationLog.Location = new System.Drawing.Point(974, 608);
            this.SimulationLog.Name = "SimulationLog";
            this.SimulationLog.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SimulationLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.SimulationLog.Size = new System.Drawing.Size(379, 129);
            this.SimulationLog.TabIndex = 133;
            this.SimulationLog.Text = "";
            // 
            // InstructorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1360, 728);
            this.Controls.Add(this.SimulationLog);
            this.Controls.Add(this.AircraftInfo);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.FullnameLabel);
            this.Controls.Add(this.SceneryLabel);
            this.Controls.Add(this.PCRoleLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.UserPicture);
            this.Controls.Add(this.TimeSimulationLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LineBottom);
            this.Controls.Add(this.footnote);
            this.Controls.Add(this.InstructorMenu);
            this.Controls.Add(this.InstructorPanel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LineBlue);
            this.Controls.Add(this.CommandPanel);
            this.Controls.Add(this.AircraftListPanel);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InstructorForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InstructorMenu";
            this.InstructorMenu.ResumeLayout(false);
            this.AircraftInfo.ResumeLayout(false);
            this.AircraftInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftLiveryImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel AircraftListPanel;
        private System.Windows.Forms.Panel CommandPanel;
        private System.Windows.Forms.PictureBox LineBlue;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel InstructorPanel;
        private System.Windows.Forms.GroupBox InstructorMenu;
        private System.Windows.Forms.RadioButton AircraftBtn;
        private System.Windows.Forms.RadioButton IncidentBtn;
        private System.Windows.Forms.RadioButton EnvironmentBtn;
        private System.Windows.Forms.RadioButton LightsBtn;
        private System.Windows.Forms.PictureBox LineBottom;
        private System.Windows.Forms.Label footnote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label TimeSimulationLabel;
        private System.Windows.Forms.Label SceneryLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label FullnameLabel;
        private System.Windows.Forms.Label PCRoleLabel;
        private System.Windows.Forms.PictureBox UserPicture;
        private System.Windows.Forms.Button StopSimulationBtn;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox AircraftLiveryImage;
        private System.Windows.Forms.Panel AircraftInfo;
        private System.Windows.Forms.Label AircraftCallsignLabel;
        private System.Windows.Forms.Label AircraftLiveryLabel;
        private System.Windows.Forms.Label AircraftModelLabel;
        private System.Windows.Forms.CheckBox PauseSimulationBtn;
        private System.Windows.Forms.RichTextBox SimulationLog;

    }
}
