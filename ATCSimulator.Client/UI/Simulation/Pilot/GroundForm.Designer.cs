﻿namespace ATCSimulator.Client.UI.Simulation.Pilot
{
    partial class GroundForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem18 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            this.GroundCommandGroup = new System.Windows.Forms.GroupBox();
            this.GroundStatusLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GroundStatusBtn = new System.Windows.Forms.Button();
            this.FlightRouteGroup = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.FlightRouteList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DestinationList = new Telerik.WinControls.UI.RadDropDownList();
            this.RunwayGroup = new System.Windows.Forms.GroupBox();
            this.RunwayList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.FlightRulesList = new Telerik.WinControls.UI.RadDropDownList();
            this.ParkingGroup = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.BayList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.TerminalList = new Telerik.WinControls.UI.RadDropDownList();
            this.Engine = new System.Windows.Forms.CheckBox();
            this.OtherCommandGroup = new System.Windows.Forms.GroupBox();
            this.RockingWing = new System.Windows.Forms.CheckBox();
            this.OneEighty = new System.Windows.Forms.Button();
            this.UTurnRight = new System.Windows.Forms.Button();
            this.UTurnLeft = new System.Windows.Forms.Button();
            this.TakeOffGroup = new System.Windows.Forms.GroupBox();
            this.AbortedTakeOff = new System.Windows.Forms.Button();
            this.TakeOff = new System.Windows.Forms.Button();
            this.RunwayHold = new System.Windows.Forms.Button();
            this.TaxiGroup = new System.Windows.Forms.GroupBox();
            this.SpeedTaxiGroup = new System.Windows.Forms.GroupBox();
            this.ChangeSpeedTaxi = new System.Windows.Forms.Button();
            this.TaxiSpeedList = new Telerik.WinControls.UI.RadDropDownList();
            this.StartTaxi = new System.Windows.Forms.Button();
            this.CrossRW = new System.Windows.Forms.Button();
            this.TaxiRoute = new System.Windows.Forms.Button();
            this.PushbackGroup = new System.Windows.Forms.GroupBox();
            this.PushbackLeft = new System.Windows.Forms.RadioButton();
            this.PushbackRight = new System.Windows.Forms.RadioButton();
            this.PBRightAlias = new System.Windows.Forms.Label();
            this.PBLeftAlias = new System.Windows.Forms.Label();
            this.PushbackStatus = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.GroundCommandGroup.SuspendLayout();
            this.FlightRouteGroup.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationList)).BeginInit();
            this.RunwayGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList)).BeginInit();
            this.ParkingGroup.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BayList)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TerminalList)).BeginInit();
            this.OtherCommandGroup.SuspendLayout();
            this.TakeOffGroup.SuspendLayout();
            this.TaxiGroup.SuspendLayout();
            this.SpeedTaxiGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaxiSpeedList)).BeginInit();
            this.PushbackGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GroundCommandGroup
            // 
            this.GroundCommandGroup.Controls.Add(this.SpeedTaxiGroup);
            this.GroundCommandGroup.Controls.Add(this.GroundStatusLbl);
            this.GroundCommandGroup.Controls.Add(this.label2);
            this.GroundCommandGroup.Controls.Add(this.GroundStatusBtn);
            this.GroundCommandGroup.Controls.Add(this.FlightRouteGroup);
            this.GroundCommandGroup.Controls.Add(this.ParkingGroup);
            this.GroundCommandGroup.Controls.Add(this.Engine);
            this.GroundCommandGroup.Controls.Add(this.OtherCommandGroup);
            this.GroundCommandGroup.Controls.Add(this.TakeOffGroup);
            this.GroundCommandGroup.Controls.Add(this.TaxiGroup);
            this.GroundCommandGroup.Controls.Add(this.PushbackGroup);
            this.GroundCommandGroup.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.GroundCommandGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.GroundCommandGroup.Location = new System.Drawing.Point(5, 0);
            this.GroundCommandGroup.Name = "GroundCommandGroup";
            this.GroundCommandGroup.Size = new System.Drawing.Size(698, 697);
            this.GroundCommandGroup.TabIndex = 17;
            this.GroundCommandGroup.TabStop = false;
            this.GroundCommandGroup.Text = "Ground Command";
            // 
            // GroundStatusLbl
            // 
            this.GroundStatusLbl.AutoSize = true;
            this.GroundStatusLbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.GroundStatusLbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.GroundStatusLbl.Location = new System.Drawing.Point(82, 90);
            this.GroundStatusLbl.Name = "GroundStatusLbl";
            this.GroundStatusLbl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GroundStatusLbl.Size = new System.Drawing.Size(105, 22);
            this.GroundStatusLbl.TabIndex = 108;
            this.GroundStatusLbl.Text = "Departure";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(9, 91);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(67, 21);
            this.label2.TabIndex = 107;
            this.label2.Text = "Status :";
            // 
            // GroundStatusBtn
            // 
            this.GroundStatusBtn.BackColor = System.Drawing.Color.Tomato;
            this.GroundStatusBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GroundStatusBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.GroundStatusBtn.ForeColor = System.Drawing.Color.White;
            this.GroundStatusBtn.Location = new System.Drawing.Point(8, 116);
            this.GroundStatusBtn.Name = "GroundStatusBtn";
            this.GroundStatusBtn.Size = new System.Drawing.Size(367, 31);
            this.GroundStatusBtn.TabIndex = 106;
            this.GroundStatusBtn.Text = "Return to Appron";
            this.GroundStatusBtn.UseVisualStyleBackColor = false;
            this.GroundStatusBtn.Click += new System.EventHandler(this.GroundStatusBtn_Click);
            // 
            // FlightRouteGroup
            // 
            this.FlightRouteGroup.Controls.Add(this.groupBox8);
            this.FlightRouteGroup.Controls.Add(this.groupBox7);
            this.FlightRouteGroup.Controls.Add(this.RunwayGroup);
            this.FlightRouteGroup.Controls.Add(this.groupBox6);
            this.FlightRouteGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.FlightRouteGroup.ForeColor = System.Drawing.Color.White;
            this.FlightRouteGroup.Location = new System.Drawing.Point(7, 163);
            this.FlightRouteGroup.Name = "FlightRouteGroup";
            this.FlightRouteGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FlightRouteGroup.Size = new System.Drawing.Size(368, 158);
            this.FlightRouteGroup.TabIndex = 105;
            this.FlightRouteGroup.TabStop = false;
            this.FlightRouteGroup.Text = "Flight Route and Rules";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.FlightRouteList);
            this.groupBox8.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox8.ForeColor = System.Drawing.Color.White;
            this.groupBox8.Location = new System.Drawing.Point(6, 79);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox8.Size = new System.Drawing.Size(263, 54);
            this.groupBox8.TabIndex = 111;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Flight Route";
            // 
            // FlightRouteList
            // 
            this.FlightRouteList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRouteList.Font = new System.Drawing.Font("Century Gothic", 10F);
            radListDataItem4.Text = "Slow";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "Medium";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "Expedite";
            radListDataItem6.TextWrap = true;
            this.FlightRouteList.Items.Add(radListDataItem4);
            this.FlightRouteList.Items.Add(radListDataItem5);
            this.FlightRouteList.Items.Add(radListDataItem6);
            this.FlightRouteList.Location = new System.Drawing.Point(6, 25);
            this.FlightRouteList.Name = "FlightRouteList";
            this.FlightRouteList.NullText = "Choose Runway";
            this.FlightRouteList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FlightRouteList.Size = new System.Drawing.Size(251, 22);
            this.FlightRouteList.TabIndex = 111;
            this.FlightRouteList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.FlightRouteList_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.DestinationList);
            this.groupBox7.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox7.ForeColor = System.Drawing.Color.White;
            this.groupBox7.Location = new System.Drawing.Point(125, 22);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox7.Size = new System.Drawing.Size(238, 54);
            this.groupBox7.TabIndex = 110;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Destination";
            // 
            // DestinationList
            // 
            this.DestinationList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DestinationList.Font = new System.Drawing.Font("Century Gothic", 10F);
            radListDataItem7.Text = "Slow";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "Medium";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "Expedite";
            radListDataItem9.TextWrap = true;
            this.DestinationList.Items.Add(radListDataItem7);
            this.DestinationList.Items.Add(radListDataItem8);
            this.DestinationList.Items.Add(radListDataItem9);
            this.DestinationList.Location = new System.Drawing.Point(6, 25);
            this.DestinationList.Name = "DestinationList";
            this.DestinationList.NullText = "Choose Runway";
            this.DestinationList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DestinationList.Size = new System.Drawing.Size(226, 22);
            this.DestinationList.TabIndex = 111;
            this.DestinationList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.DestinationList_SelectedIndexChanged);
            // 
            // RunwayGroup
            // 
            this.RunwayGroup.Controls.Add(this.RunwayList);
            this.RunwayGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.RunwayGroup.ForeColor = System.Drawing.Color.White;
            this.RunwayGroup.Location = new System.Drawing.Point(275, 77);
            this.RunwayGroup.Name = "RunwayGroup";
            this.RunwayGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RunwayGroup.Size = new System.Drawing.Size(88, 56);
            this.RunwayGroup.TabIndex = 40;
            this.RunwayGroup.TabStop = false;
            this.RunwayGroup.Text = "Runway";
            // 
            // RunwayList
            // 
            this.RunwayList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RunwayList.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.RunwayList.Location = new System.Drawing.Point(6, 27);
            this.RunwayList.Name = "RunwayList";
            this.RunwayList.NullText = "Choose Runway";
            this.RunwayList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RunwayList.Size = new System.Drawing.Size(75, 22);
            this.RunwayList.TabIndex = 41;
            this.RunwayList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RunwayList_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.FlightRulesList);
            this.groupBox6.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox6.ForeColor = System.Drawing.Color.White;
            this.groupBox6.Location = new System.Drawing.Point(8, 22);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox6.Size = new System.Drawing.Size(111, 54);
            this.groupBox6.TabIndex = 109;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Flight Rule";
            // 
            // FlightRulesList
            // 
            this.FlightRulesList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRulesList.Font = new System.Drawing.Font("Century Gothic", 10F);
            radListDataItem10.Text = "IFR";
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "VFR";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "Local";
            radListDataItem12.TextWrap = true;
            this.FlightRulesList.Items.Add(radListDataItem10);
            this.FlightRulesList.Items.Add(radListDataItem11);
            this.FlightRulesList.Items.Add(radListDataItem12);
            this.FlightRulesList.Location = new System.Drawing.Point(6, 24);
            this.FlightRulesList.Name = "FlightRulesList";
            this.FlightRulesList.NullText = "Choose Runway";
            this.FlightRulesList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FlightRulesList.Size = new System.Drawing.Size(99, 22);
            this.FlightRulesList.TabIndex = 111;
            this.FlightRulesList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.FlightRulesList_SelectedIndexChanged);
            // 
            // ParkingGroup
            // 
            this.ParkingGroup.Controls.Add(this.groupBox11);
            this.ParkingGroup.Controls.Add(this.groupBox10);
            this.ParkingGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ParkingGroup.ForeColor = System.Drawing.Color.White;
            this.ParkingGroup.Location = new System.Drawing.Point(383, 12);
            this.ParkingGroup.Name = "ParkingGroup";
            this.ParkingGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ParkingGroup.Size = new System.Drawing.Size(309, 90);
            this.ParkingGroup.TabIndex = 40;
            this.ParkingGroup.TabStop = false;
            this.ParkingGroup.Text = "Appron Parking";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.BayList);
            this.groupBox11.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox11.ForeColor = System.Drawing.Color.White;
            this.groupBox11.Location = new System.Drawing.Point(199, 22);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox11.Size = new System.Drawing.Size(104, 54);
            this.groupBox11.TabIndex = 113;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Bay";
            // 
            // BayList
            // 
            this.BayList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.BayList.Font = new System.Drawing.Font("Century Gothic", 10F);
            radListDataItem13.Text = "Slow";
            radListDataItem13.TextWrap = true;
            radListDataItem14.Text = "Medium";
            radListDataItem14.TextWrap = true;
            radListDataItem15.Text = "Expedite";
            radListDataItem15.TextWrap = true;
            this.BayList.Items.Add(radListDataItem13);
            this.BayList.Items.Add(radListDataItem14);
            this.BayList.Items.Add(radListDataItem15);
            this.BayList.Location = new System.Drawing.Point(6, 25);
            this.BayList.Name = "BayList";
            this.BayList.NullText = "Choose Runway";
            this.BayList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BayList.Size = new System.Drawing.Size(94, 22);
            this.BayList.TabIndex = 111;
            this.BayList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.BayList_SelectedIndexChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.TerminalList);
            this.groupBox10.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox10.ForeColor = System.Drawing.Color.White;
            this.groupBox10.Location = new System.Drawing.Point(6, 22);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox10.Size = new System.Drawing.Size(187, 54);
            this.groupBox10.TabIndex = 112;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Appron";
            // 
            // TerminalList
            // 
            this.TerminalList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TerminalList.Font = new System.Drawing.Font("Century Gothic", 10F);
            radListDataItem16.Text = "Slow";
            radListDataItem16.TextWrap = true;
            radListDataItem17.Text = "Medium";
            radListDataItem17.TextWrap = true;
            radListDataItem18.Text = "Expedite";
            radListDataItem18.TextWrap = true;
            this.TerminalList.Items.Add(radListDataItem16);
            this.TerminalList.Items.Add(radListDataItem17);
            this.TerminalList.Items.Add(radListDataItem18);
            this.TerminalList.Location = new System.Drawing.Point(6, 25);
            this.TerminalList.Name = "TerminalList";
            this.TerminalList.NullText = "Choose Runway";
            this.TerminalList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TerminalList.Size = new System.Drawing.Size(172, 22);
            this.TerminalList.TabIndex = 111;
            this.TerminalList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.TerminalList_SelectedIndexChanged);
            // 
            // Engine
            // 
            this.Engine.Appearance = System.Windows.Forms.Appearance.Button;
            this.Engine.BackColor = System.Drawing.Color.Tomato;
            this.Engine.FlatAppearance.BorderSize = 0;
            this.Engine.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.Engine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Engine.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.Engine.ForeColor = System.Drawing.Color.White;
            this.Engine.Location = new System.Drawing.Point(8, 38);
            this.Engine.Name = "Engine";
            this.Engine.Size = new System.Drawing.Size(367, 50);
            this.Engine.TabIndex = 103;
            this.Engine.Text = "Start Engine";
            this.Engine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Engine.UseVisualStyleBackColor = false;
            this.Engine.Click += new System.EventHandler(this.Engine_Click);
            // 
            // OtherCommandGroup
            // 
            this.OtherCommandGroup.Controls.Add(this.RockingWing);
            this.OtherCommandGroup.Controls.Add(this.OneEighty);
            this.OtherCommandGroup.Controls.Add(this.UTurnRight);
            this.OtherCommandGroup.Controls.Add(this.UTurnLeft);
            this.OtherCommandGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.OtherCommandGroup.ForeColor = System.Drawing.Color.White;
            this.OtherCommandGroup.Location = new System.Drawing.Point(7, 438);
            this.OtherCommandGroup.Name = "OtherCommandGroup";
            this.OtherCommandGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.OtherCommandGroup.Size = new System.Drawing.Size(368, 102);
            this.OtherCommandGroup.TabIndex = 7;
            this.OtherCommandGroup.TabStop = false;
            this.OtherCommandGroup.Text = "Other Command";
            // 
            // RockingWing
            // 
            this.RockingWing.Appearance = System.Windows.Forms.Appearance.Button;
            this.RockingWing.BackColor = System.Drawing.Color.Tomato;
            this.RockingWing.FlatAppearance.BorderSize = 0;
            this.RockingWing.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.RockingWing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RockingWing.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.RockingWing.ForeColor = System.Drawing.Color.White;
            this.RockingWing.Location = new System.Drawing.Point(190, 60);
            this.RockingWing.Name = "RockingWing";
            this.RockingWing.Size = new System.Drawing.Size(166, 30);
            this.RockingWing.TabIndex = 106;
            this.RockingWing.Text = "Rocking The Wing";
            this.RockingWing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.RockingWing.UseVisualStyleBackColor = false;
            this.RockingWing.Click += new System.EventHandler(this.RockingWing_Click);
            // 
            // OneEighty
            // 
            this.OneEighty.BackColor = System.Drawing.Color.DodgerBlue;
            this.OneEighty.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OneEighty.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.OneEighty.ForeColor = System.Drawing.Color.White;
            this.OneEighty.Location = new System.Drawing.Point(11, 60);
            this.OneEighty.Name = "OneEighty";
            this.OneEighty.Size = new System.Drawing.Size(169, 30);
            this.OneEighty.TabIndex = 18;
            this.OneEighty.Text = "Turn 180";
            this.OneEighty.UseVisualStyleBackColor = false;
            this.OneEighty.Click += new System.EventHandler(this.OneEighty_Click);
            // 
            // UTurnRight
            // 
            this.UTurnRight.BackColor = System.Drawing.Color.DodgerBlue;
            this.UTurnRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UTurnRight.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.UTurnRight.ForeColor = System.Drawing.Color.White;
            this.UTurnRight.Location = new System.Drawing.Point(190, 26);
            this.UTurnRight.Name = "UTurnRight";
            this.UTurnRight.Size = new System.Drawing.Size(166, 30);
            this.UTurnRight.TabIndex = 17;
            this.UTurnRight.Text = "U-Turn Right";
            this.UTurnRight.UseVisualStyleBackColor = false;
            this.UTurnRight.Click += new System.EventHandler(this.UTurnRight_Click);
            // 
            // UTurnLeft
            // 
            this.UTurnLeft.BackColor = System.Drawing.Color.DodgerBlue;
            this.UTurnLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UTurnLeft.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.UTurnLeft.ForeColor = System.Drawing.Color.White;
            this.UTurnLeft.Location = new System.Drawing.Point(11, 26);
            this.UTurnLeft.Name = "UTurnLeft";
            this.UTurnLeft.Size = new System.Drawing.Size(169, 30);
            this.UTurnLeft.TabIndex = 0;
            this.UTurnLeft.Text = "U-Turn Left";
            this.UTurnLeft.UseVisualStyleBackColor = false;
            this.UTurnLeft.Click += new System.EventHandler(this.UTurnLeft_Click);
            // 
            // TakeOffGroup
            // 
            this.TakeOffGroup.Controls.Add(this.AbortedTakeOff);
            this.TakeOffGroup.Controls.Add(this.TakeOff);
            this.TakeOffGroup.Controls.Add(this.RunwayHold);
            this.TakeOffGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.TakeOffGroup.ForeColor = System.Drawing.Color.White;
            this.TakeOffGroup.Location = new System.Drawing.Point(381, 362);
            this.TakeOffGroup.Name = "TakeOffGroup";
            this.TakeOffGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TakeOffGroup.Size = new System.Drawing.Size(310, 178);
            this.TakeOffGroup.TabIndex = 6;
            this.TakeOffGroup.TabStop = false;
            this.TakeOffGroup.Text = "Intersection";
            // 
            // AbortedTakeOff
            // 
            this.AbortedTakeOff.BackColor = System.Drawing.Color.DodgerBlue;
            this.AbortedTakeOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AbortedTakeOff.ForeColor = System.Drawing.Color.White;
            this.AbortedTakeOff.Location = new System.Drawing.Point(8, 67);
            this.AbortedTakeOff.Name = "AbortedTakeOff";
            this.AbortedTakeOff.Size = new System.Drawing.Size(293, 32);
            this.AbortedTakeOff.TabIndex = 109;
            this.AbortedTakeOff.Text = "Aborted Take Off";
            this.AbortedTakeOff.UseVisualStyleBackColor = false;
            this.AbortedTakeOff.Click += new System.EventHandler(this.AbortedTakeOff_Click);
            // 
            // TakeOff
            // 
            this.TakeOff.BackColor = System.Drawing.Color.DodgerBlue;
            this.TakeOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TakeOff.ForeColor = System.Drawing.Color.White;
            this.TakeOff.Location = new System.Drawing.Point(8, 105);
            this.TakeOff.Name = "TakeOff";
            this.TakeOff.Size = new System.Drawing.Size(292, 65);
            this.TakeOff.TabIndex = 1;
            this.TakeOff.Text = "Take Off";
            this.TakeOff.UseVisualStyleBackColor = false;
            this.TakeOff.Click += new System.EventHandler(this.TakeOff_Click);
            // 
            // RunwayHold
            // 
            this.RunwayHold.BackColor = System.Drawing.Color.DodgerBlue;
            this.RunwayHold.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RunwayHold.ForeColor = System.Drawing.Color.White;
            this.RunwayHold.Location = new System.Drawing.Point(9, 28);
            this.RunwayHold.Name = "RunwayHold";
            this.RunwayHold.Size = new System.Drawing.Size(292, 33);
            this.RunwayHold.TabIndex = 0;
            this.RunwayHold.Text = "Runway Hold";
            this.RunwayHold.UseVisualStyleBackColor = false;
            this.RunwayHold.Click += new System.EventHandler(this.RunwayHold_Click);
            // 
            // TaxiGroup
            // 
            this.TaxiGroup.Controls.Add(this.StartTaxi);
            this.TaxiGroup.Controls.Add(this.CrossRW);
            this.TaxiGroup.Controls.Add(this.TaxiRoute);
            this.TaxiGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.TaxiGroup.ForeColor = System.Drawing.Color.White;
            this.TaxiGroup.Location = new System.Drawing.Point(383, 108);
            this.TaxiGroup.Name = "TaxiGroup";
            this.TaxiGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TaxiGroup.Size = new System.Drawing.Size(310, 158);
            this.TaxiGroup.TabIndex = 2;
            this.TaxiGroup.TabStop = false;
            this.TaxiGroup.Text = "Taxi";
            // 
            // SpeedTaxiGroup
            // 
            this.SpeedTaxiGroup.Controls.Add(this.ChangeSpeedTaxi);
            this.SpeedTaxiGroup.Controls.Add(this.TaxiSpeedList);
            this.SpeedTaxiGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.SpeedTaxiGroup.ForeColor = System.Drawing.Color.White;
            this.SpeedTaxiGroup.Location = new System.Drawing.Point(485, 269);
            this.SpeedTaxiGroup.Name = "SpeedTaxiGroup";
            this.SpeedTaxiGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SpeedTaxiGroup.Size = new System.Drawing.Size(206, 89);
            this.SpeedTaxiGroup.TabIndex = 107;
            this.SpeedTaxiGroup.TabStop = false;
            this.SpeedTaxiGroup.Text = "Speed";
            // 
            // ChangeSpeedTaxi
            // 
            this.ChangeSpeedTaxi.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeSpeedTaxi.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ChangeSpeedTaxi.FlatAppearance.BorderSize = 0;
            this.ChangeSpeedTaxi.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.ChangeSpeedTaxi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.ChangeSpeedTaxi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeSpeedTaxi.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.ChangeSpeedTaxi.ForeColor = System.Drawing.Color.White;
            this.ChangeSpeedTaxi.Location = new System.Drawing.Point(6, 52);
            this.ChangeSpeedTaxi.Name = "ChangeSpeedTaxi";
            this.ChangeSpeedTaxi.Size = new System.Drawing.Size(190, 28);
            this.ChangeSpeedTaxi.TabIndex = 107;
            this.ChangeSpeedTaxi.Text = "Change";
            this.ChangeSpeedTaxi.UseVisualStyleBackColor = false;
            this.ChangeSpeedTaxi.Click += new System.EventHandler(this.ChangeSpeedTaxi_Click);
            // 
            // TaxiSpeedList
            // 
            this.TaxiSpeedList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TaxiSpeedList.Font = new System.Drawing.Font("Century Gothic", 10F);
            radListDataItem1.Text = "Slow";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Medium";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Expedite";
            radListDataItem3.TextWrap = true;
            this.TaxiSpeedList.Items.Add(radListDataItem1);
            this.TaxiSpeedList.Items.Add(radListDataItem2);
            this.TaxiSpeedList.Items.Add(radListDataItem3);
            this.TaxiSpeedList.Location = new System.Drawing.Point(6, 24);
            this.TaxiSpeedList.Name = "TaxiSpeedList";
            this.TaxiSpeedList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TaxiSpeedList.Size = new System.Drawing.Size(190, 22);
            this.TaxiSpeedList.TabIndex = 106;
            // 
            // StartTaxi
            // 
            this.StartTaxi.BackColor = System.Drawing.Color.DodgerBlue;
            this.StartTaxi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.StartTaxi.ForeColor = System.Drawing.Color.White;
            this.StartTaxi.Location = new System.Drawing.Point(7, 96);
            this.StartTaxi.Name = "StartTaxi";
            this.StartTaxi.Size = new System.Drawing.Size(292, 53);
            this.StartTaxi.TabIndex = 0;
            this.StartTaxi.Text = "Start Taxi";
            this.StartTaxi.UseVisualStyleBackColor = false;
            this.StartTaxi.Click += new System.EventHandler(this.StartTaxi_Click);
            // 
            // CrossRW
            // 
            this.CrossRW.BackColor = System.Drawing.Color.DodgerBlue;
            this.CrossRW.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.CrossRW.FlatAppearance.BorderSize = 0;
            this.CrossRW.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.CrossRW.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.CrossRW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CrossRW.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.CrossRW.ForeColor = System.Drawing.Color.White;
            this.CrossRW.Location = new System.Drawing.Point(8, 62);
            this.CrossRW.Name = "CrossRW";
            this.CrossRW.Size = new System.Drawing.Size(291, 30);
            this.CrossRW.TabIndex = 6;
            this.CrossRW.Text = "Cross Runway";
            this.CrossRW.UseVisualStyleBackColor = false;
            this.CrossRW.Click += new System.EventHandler(this.CrossRW_Click);
            // 
            // TaxiRoute
            // 
            this.TaxiRoute.BackColor = System.Drawing.Color.DodgerBlue;
            this.TaxiRoute.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.TaxiRoute.FlatAppearance.BorderSize = 0;
            this.TaxiRoute.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.TaxiRoute.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.TaxiRoute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaxiRoute.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.TaxiRoute.ForeColor = System.Drawing.Color.White;
            this.TaxiRoute.Location = new System.Drawing.Point(8, 26);
            this.TaxiRoute.Name = "TaxiRoute";
            this.TaxiRoute.Size = new System.Drawing.Size(291, 30);
            this.TaxiRoute.TabIndex = 1;
            this.TaxiRoute.Text = "Taxi Route";
            this.TaxiRoute.UseVisualStyleBackColor = false;
            this.TaxiRoute.Click += new System.EventHandler(this.TaxiRoute_Click);
            // 
            // PushbackGroup
            // 
            this.PushbackGroup.Controls.Add(this.PushbackLeft);
            this.PushbackGroup.Controls.Add(this.PushbackRight);
            this.PushbackGroup.Controls.Add(this.PBRightAlias);
            this.PushbackGroup.Controls.Add(this.PBLeftAlias);
            this.PushbackGroup.Controls.Add(this.PushbackStatus);
            this.PushbackGroup.Controls.Add(this.label19);
            this.PushbackGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.PushbackGroup.ForeColor = System.Drawing.Color.White;
            this.PushbackGroup.Location = new System.Drawing.Point(7, 330);
            this.PushbackGroup.Name = "PushbackGroup";
            this.PushbackGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PushbackGroup.Size = new System.Drawing.Size(368, 102);
            this.PushbackGroup.TabIndex = 1;
            this.PushbackGroup.TabStop = false;
            this.PushbackGroup.Text = "Pushback";
            // 
            // PushbackLeft
            // 
            this.PushbackLeft.Appearance = System.Windows.Forms.Appearance.Button;
            this.PushbackLeft.BackColor = System.Drawing.Color.Tomato;
            this.PushbackLeft.FlatAppearance.BorderSize = 0;
            this.PushbackLeft.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.PushbackLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PushbackLeft.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.PushbackLeft.ForeColor = System.Drawing.Color.White;
            this.PushbackLeft.Location = new System.Drawing.Point(7, 41);
            this.PushbackLeft.Name = "PushbackLeft";
            this.PushbackLeft.Size = new System.Drawing.Size(173, 30);
            this.PushbackLeft.TabIndex = 116;
            this.PushbackLeft.Text = "Pushback Left";
            this.PushbackLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PushbackLeft.UseVisualStyleBackColor = false;
            this.PushbackLeft.Click += new System.EventHandler(this.PushbackLeft_Click);
            // 
            // PushbackRight
            // 
            this.PushbackRight.Appearance = System.Windows.Forms.Appearance.Button;
            this.PushbackRight.BackColor = System.Drawing.Color.Tomato;
            this.PushbackRight.FlatAppearance.BorderSize = 0;
            this.PushbackRight.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.PushbackRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PushbackRight.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.PushbackRight.ForeColor = System.Drawing.Color.White;
            this.PushbackRight.Location = new System.Drawing.Point(190, 41);
            this.PushbackRight.Name = "PushbackRight";
            this.PushbackRight.Size = new System.Drawing.Size(167, 30);
            this.PushbackRight.TabIndex = 115;
            this.PushbackRight.Text = "Pushback Right";
            this.PushbackRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PushbackRight.UseVisualStyleBackColor = false;
            this.PushbackRight.Click += new System.EventHandler(this.PushbackRight_Click);
            // 
            // PBRightAlias
            // 
            this.PBRightAlias.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.PBRightAlias.ForeColor = System.Drawing.Color.Tomato;
            this.PBRightAlias.Location = new System.Drawing.Point(204, 70);
            this.PBRightAlias.Name = "PBRightAlias";
            this.PBRightAlias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PBRightAlias.Size = new System.Drawing.Size(143, 21);
            this.PBRightAlias.TabIndex = 106;
            this.PBRightAlias.Text = "Head West";
            this.PBRightAlias.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PBLeftAlias
            // 
            this.PBLeftAlias.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.PBLeftAlias.ForeColor = System.Drawing.Color.Tomato;
            this.PBLeftAlias.Location = new System.Drawing.Point(18, 71);
            this.PBLeftAlias.Name = "PBLeftAlias";
            this.PBLeftAlias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PBLeftAlias.Size = new System.Drawing.Size(147, 21);
            this.PBLeftAlias.TabIndex = 105;
            this.PBLeftAlias.Text = "Head 34L";
            this.PBLeftAlias.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PushbackStatus
            // 
            this.PushbackStatus.AutoSize = true;
            this.PushbackStatus.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.PushbackStatus.ForeColor = System.Drawing.Color.Tomato;
            this.PushbackStatus.Location = new System.Drawing.Point(71, 16);
            this.PushbackStatus.Name = "PushbackStatus";
            this.PushbackStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PushbackStatus.Size = new System.Drawing.Size(118, 21);
            this.PushbackStatus.TabIndex = 104;
            this.PushbackStatus.Text = "Pushback Left";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(8, 15);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label19.Size = new System.Drawing.Size(63, 21);
            this.label19.TabIndex = 103;
            this.label19.Text = "Status ";
            // 
            // GroundForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(708, 700);
            this.Controls.Add(this.GroundCommandGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GroundForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "GroundForm";
            this.ThemeName = "ControlDefault";
            this.GroundCommandGroup.ResumeLayout(false);
            this.GroundCommandGroup.PerformLayout();
            this.FlightRouteGroup.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationList)).EndInit();
            this.RunwayGroup.ResumeLayout(false);
            this.RunwayGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList)).EndInit();
            this.ParkingGroup.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BayList)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TerminalList)).EndInit();
            this.OtherCommandGroup.ResumeLayout(false);
            this.TakeOffGroup.ResumeLayout(false);
            this.TaxiGroup.ResumeLayout(false);
            this.SpeedTaxiGroup.ResumeLayout(false);
            this.SpeedTaxiGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaxiSpeedList)).EndInit();
            this.PushbackGroup.ResumeLayout(false);
            this.PushbackGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroundCommandGroup;
        private System.Windows.Forms.GroupBox FlightRouteGroup;
        private System.Windows.Forms.CheckBox Engine;
        private System.Windows.Forms.GroupBox RunwayGroup;
        private Telerik.WinControls.UI.RadDropDownList RunwayList;
        private System.Windows.Forms.GroupBox OtherCommandGroup;
        private System.Windows.Forms.Button UTurnLeft;
        private System.Windows.Forms.GroupBox TakeOffGroup;
        private System.Windows.Forms.Button RunwayHold;
        private System.Windows.Forms.GroupBox TaxiGroup;
        private System.Windows.Forms.Button CrossRW;
        private System.Windows.Forms.Button TaxiRoute;
        private System.Windows.Forms.Button StartTaxi;
        private System.Windows.Forms.GroupBox PushbackGroup;
        private System.Windows.Forms.Label PBRightAlias;
        private System.Windows.Forms.Label PBLeftAlias;
        private System.Windows.Forms.Label PushbackStatus;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button TakeOff;
        private Telerik.WinControls.UI.RadDropDownList TaxiSpeedList;
        private System.Windows.Forms.GroupBox ParkingGroup;
        private System.Windows.Forms.Button ChangeSpeedTaxi;
        private System.Windows.Forms.Button UTurnRight;
        private System.Windows.Forms.Button OneEighty;
        private System.Windows.Forms.RadioButton PushbackRight;
        private System.Windows.Forms.RadioButton PushbackLeft;
        private System.Windows.Forms.GroupBox SpeedTaxiGroup;
        private System.Windows.Forms.CheckBox RockingWing;
        private System.Windows.Forms.GroupBox groupBox6;
        private Telerik.WinControls.UI.RadDropDownList FlightRulesList;
        private System.Windows.Forms.GroupBox groupBox7;
        private Telerik.WinControls.UI.RadDropDownList DestinationList;
        private System.Windows.Forms.GroupBox groupBox8;
        private Telerik.WinControls.UI.RadDropDownList FlightRouteList;
        private System.Windows.Forms.GroupBox groupBox11;
        private Telerik.WinControls.UI.RadDropDownList BayList;
        private System.Windows.Forms.GroupBox groupBox10;
        private Telerik.WinControls.UI.RadDropDownList TerminalList;
        private System.Windows.Forms.Label GroundStatusLbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button GroundStatusBtn;
        private System.Windows.Forms.Button AbortedTakeOff;
    }
}
