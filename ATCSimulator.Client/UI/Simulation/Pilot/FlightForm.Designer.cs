﻿namespace ATCSimulator.Client.UI.Simulation.Pilot
{
    partial class FlightForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            this.GroundCommandGroup = new System.Windows.Forms.GroupBox();
            this.LandingRunwayGroup = new System.Windows.Forms.GroupBox();
            this.LandingRunwayList = new Telerik.WinControls.UI.RadDropDownList();
            this.RockingWing = new System.Windows.Forms.Button();
            this.DirectGoILSGroup = new System.Windows.Forms.GroupBox();
            this.DirectGoILS = new System.Windows.Forms.Button();
            this.DirectGoILSList = new Telerik.WinControls.UI.RadDropDownList();
            this.ClearToLand = new System.Windows.Forms.Button();
            this.FlightRouteGroup = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.StartPointList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.FlightRouteList = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.DestinationList = new Telerik.WinControls.UI.RadDropDownList();
            this.ChangeRoute = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.FlightRulesList = new Telerik.WinControls.UI.RadDropDownList();
            this.HoldingRoute = new System.Windows.Forms.GroupBox();
            this.ContinueDirection = new System.Windows.Forms.Button();
            this.ContinueRoute = new System.Windows.Forms.Button();
            this.Holding = new System.Windows.Forms.Button();
            this.HoldingRight = new System.Windows.Forms.Button();
            this.HoldingList = new Telerik.WinControls.UI.RadDropDownList();
            this.label3 = new System.Windows.Forms.Label();
            this.HoldingLeft = new System.Windows.Forms.Button();
            this.HeadingGroup = new System.Windows.Forms.GroupBox();
            this.HeadingRel = new System.Windows.Forms.Button();
            this.HeadingAbs = new System.Windows.Forms.Button();
            this.HeadingRelBox = new Telerik.WinControls.UI.RadSpinEditor();
            this.HeadingAbsBox = new Telerik.WinControls.UI.RadSpinEditor();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ChangeRunwayGroup = new System.Windows.Forms.GroupBox();
            this.ChangeRunwayList = new Telerik.WinControls.UI.RadDropDownList();
            this.ChangeRunway = new System.Windows.Forms.Button();
            this.DirectGoGroup = new System.Windows.Forms.GroupBox();
            this.DirectGo = new System.Windows.Forms.Button();
            this.DirectGoList = new Telerik.WinControls.UI.RadDropDownList();
            this.OtherGroup = new System.Windows.Forms.GroupBox();
            this.Flypass = new System.Windows.Forms.Button();
            this.TouchGo = new System.Windows.Forms.Button();
            this.MissedApproach = new System.Windows.Forms.Button();
            this.DownwindOrbitGroup = new System.Windows.Forms.GroupBox();
            this.Orbit = new System.Windows.Forms.Button();
            this.ExtendDownwind = new System.Windows.Forms.Button();
            this.CircuitApproachGroup = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.RunwayList = new Telerik.WinControls.UI.RadDropDownList();
            this.InstrumentApp = new System.Windows.Forms.Button();
            this.JoinCircuit = new System.Windows.Forms.Button();
            this.PerformanceGroup = new System.Windows.Forms.GroupBox();
            this.ChangeSpeed = new System.Windows.Forms.Button();
            this.ChangeAltitude = new System.Windows.Forms.Button();
            this.SpeedValue = new Telerik.WinControls.UI.RadSpinEditor();
            this.AltitudeValue = new Telerik.WinControls.UI.RadSpinEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.ReturnBase = new System.Windows.Forms.Button();
            this.AbortTakeOff = new System.Windows.Forms.Button();
            this.ILSModeBtn = new System.Windows.Forms.Button();
            this.GroundCommandGroup.SuspendLayout();
            this.LandingRunwayGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LandingRunwayList)).BeginInit();
            this.DirectGoILSGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DirectGoILSList)).BeginInit();
            this.FlightRouteGroup.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartPointList)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationList)).BeginInit();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList)).BeginInit();
            this.HoldingRoute.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HoldingList)).BeginInit();
            this.HeadingGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HeadingRelBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeadingAbsBox)).BeginInit();
            this.ChangeRunwayGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChangeRunwayList)).BeginInit();
            this.DirectGoGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DirectGoList)).BeginInit();
            this.OtherGroup.SuspendLayout();
            this.DownwindOrbitGroup.SuspendLayout();
            this.CircuitApproachGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).BeginInit();
            this.PerformanceGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AltitudeValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GroundCommandGroup
            // 
            this.GroundCommandGroup.Controls.Add(this.LandingRunwayGroup);
            this.GroundCommandGroup.Controls.Add(this.RockingWing);
            this.GroundCommandGroup.Controls.Add(this.DirectGoILSGroup);
            this.GroundCommandGroup.Controls.Add(this.ClearToLand);
            this.GroundCommandGroup.Controls.Add(this.FlightRouteGroup);
            this.GroundCommandGroup.Controls.Add(this.HoldingRoute);
            this.GroundCommandGroup.Controls.Add(this.HeadingGroup);
            this.GroundCommandGroup.Controls.Add(this.ChangeRunwayGroup);
            this.GroundCommandGroup.Controls.Add(this.DirectGoGroup);
            this.GroundCommandGroup.Controls.Add(this.OtherGroup);
            this.GroundCommandGroup.Controls.Add(this.DownwindOrbitGroup);
            this.GroundCommandGroup.Controls.Add(this.CircuitApproachGroup);
            this.GroundCommandGroup.Controls.Add(this.PerformanceGroup);
            this.GroundCommandGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.GroundCommandGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.GroundCommandGroup.Location = new System.Drawing.Point(5, 0);
            this.GroundCommandGroup.Name = "GroundCommandGroup";
            this.GroundCommandGroup.Size = new System.Drawing.Size(720, 543);
            this.GroundCommandGroup.TabIndex = 18;
            this.GroundCommandGroup.TabStop = false;
            this.GroundCommandGroup.Text = "Flight Command";
            // 
            // LandingRunwayGroup
            // 
            this.LandingRunwayGroup.Controls.Add(this.LandingRunwayList);
            this.LandingRunwayGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LandingRunwayGroup.ForeColor = System.Drawing.Color.White;
            this.LandingRunwayGroup.Location = new System.Drawing.Point(249, 413);
            this.LandingRunwayGroup.Name = "LandingRunwayGroup";
            this.LandingRunwayGroup.Size = new System.Drawing.Size(281, 63);
            this.LandingRunwayGroup.TabIndex = 154;
            this.LandingRunwayGroup.TabStop = false;
            this.LandingRunwayGroup.Text = "Landing in Runway";
            // 
            // LandingRunwayList
            // 
            this.LandingRunwayList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.LandingRunwayList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.LandingRunwayList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.LandingRunwayList.Location = new System.Drawing.Point(11, 29);
            this.LandingRunwayList.Name = "LandingRunwayList";
            this.LandingRunwayList.NullText = "Choose Runway";
            this.LandingRunwayList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.LandingRunwayList.RootElement.ControlBounds = new System.Drawing.Rectangle(11, 29, 125, 20);
            this.LandingRunwayList.RootElement.StretchVertically = true;
            this.LandingRunwayList.Size = new System.Drawing.Size(263, 22);
            this.LandingRunwayList.TabIndex = 107;
            // 
            // RockingWing
            // 
            this.RockingWing.BackColor = System.Drawing.Color.DodgerBlue;
            this.RockingWing.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RockingWing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RockingWing.ForeColor = System.Drawing.Color.White;
            this.RockingWing.Location = new System.Drawing.Point(249, 510);
            this.RockingWing.Name = "RockingWing";
            this.RockingWing.Size = new System.Drawing.Size(281, 27);
            this.RockingWing.TabIndex = 110;
            this.RockingWing.Text = "Rocking Wing";
            this.RockingWing.UseVisualStyleBackColor = false;
            this.RockingWing.Click += new System.EventHandler(this.RockingWing_Click);
            // 
            // DirectGoILSGroup
            // 
            this.DirectGoILSGroup.Controls.Add(this.DirectGoILS);
            this.DirectGoILSGroup.Controls.Add(this.DirectGoILSList);
            this.DirectGoILSGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DirectGoILSGroup.ForeColor = System.Drawing.Color.White;
            this.DirectGoILSGroup.Location = new System.Drawing.Point(7, 299);
            this.DirectGoILSGroup.Name = "DirectGoILSGroup";
            this.DirectGoILSGroup.Size = new System.Drawing.Size(236, 49);
            this.DirectGoILSGroup.TabIndex = 153;
            this.DirectGoILSGroup.TabStop = false;
            this.DirectGoILSGroup.Text = "Direct Go to";
            // 
            // DirectGoILS
            // 
            this.DirectGoILS.BackColor = System.Drawing.Color.DodgerBlue;
            this.DirectGoILS.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DirectGoILS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DirectGoILS.ForeColor = System.Drawing.Color.White;
            this.DirectGoILS.Location = new System.Drawing.Point(122, 20);
            this.DirectGoILS.Name = "DirectGoILS";
            this.DirectGoILS.Size = new System.Drawing.Size(108, 25);
            this.DirectGoILS.TabIndex = 108;
            this.DirectGoILS.Text = "Direct Go";
            this.DirectGoILS.UseVisualStyleBackColor = false;
            this.DirectGoILS.Click += new System.EventHandler(this.DirectGoILS_Click);
            // 
            // DirectGoILSList
            // 
            this.DirectGoILSList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DirectGoILSList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DirectGoILSList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.DirectGoILSList.Location = new System.Drawing.Point(11, 21);
            this.DirectGoILSList.Name = "DirectGoILSList";
            this.DirectGoILSList.NullText = "Choose Runway";
            this.DirectGoILSList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.DirectGoILSList.RootElement.ControlBounds = new System.Drawing.Rectangle(11, 21, 125, 20);
            this.DirectGoILSList.RootElement.StretchVertically = true;
            this.DirectGoILSList.Size = new System.Drawing.Size(105, 22);
            this.DirectGoILSList.TabIndex = 107;
            // 
            // ClearToLand
            // 
            this.ClearToLand.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClearToLand.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ClearToLand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ClearToLand.ForeColor = System.Drawing.Color.White;
            this.ClearToLand.Location = new System.Drawing.Point(249, 479);
            this.ClearToLand.Name = "ClearToLand";
            this.ClearToLand.Size = new System.Drawing.Size(281, 28);
            this.ClearToLand.TabIndex = 109;
            this.ClearToLand.Text = "Clear to Land";
            this.ClearToLand.UseVisualStyleBackColor = false;
            this.ClearToLand.Click += new System.EventHandler(this.ClearToLand_Click);
            // 
            // FlightRouteGroup
            // 
            this.FlightRouteGroup.Controls.Add(this.groupBox3);
            this.FlightRouteGroup.Controls.Add(this.groupBox12);
            this.FlightRouteGroup.Controls.Add(this.groupBox13);
            this.FlightRouteGroup.Controls.Add(this.ChangeRoute);
            this.FlightRouteGroup.Controls.Add(this.groupBox14);
            this.FlightRouteGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FlightRouteGroup.ForeColor = System.Drawing.Color.White;
            this.FlightRouteGroup.Location = new System.Drawing.Point(7, 167);
            this.FlightRouteGroup.Name = "FlightRouteGroup";
            this.FlightRouteGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FlightRouteGroup.Size = new System.Drawing.Size(414, 177);
            this.FlightRouteGroup.TabIndex = 155;
            this.FlightRouteGroup.TabStop = false;
            this.FlightRouteGroup.Text = "Flight Route and Rules";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.StartPointList);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(290, 79);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(118, 54);
            this.groupBox3.TabIndex = 112;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Start From";
            // 
            // StartPointList
            // 
            this.StartPointList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.StartPointList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.StartPointList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.StartPointList.Location = new System.Drawing.Point(6, 25);
            this.StartPointList.Name = "StartPointList";
            this.StartPointList.NullText = "Choose Runway";
            this.StartPointList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.StartPointList.RootElement.ControlBounds = new System.Drawing.Rectangle(6, 25, 125, 20);
            this.StartPointList.RootElement.StretchVertically = true;
            this.StartPointList.Size = new System.Drawing.Size(106, 22);
            this.StartPointList.TabIndex = 112;
            this.StartPointList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.StartPointList_SelectedIndexChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.FlightRouteList);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox12.ForeColor = System.Drawing.Color.White;
            this.groupBox12.Location = new System.Drawing.Point(6, 79);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox12.Size = new System.Drawing.Size(278, 54);
            this.groupBox12.TabIndex = 111;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Flight Route";
            // 
            // FlightRouteList
            // 
            this.FlightRouteList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.FlightRouteList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRouteList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            radListDataItem7.Text = "Slow";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "Medium";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "Expedite";
            radListDataItem9.TextWrap = true;
            this.FlightRouteList.Items.Add(radListDataItem7);
            this.FlightRouteList.Items.Add(radListDataItem8);
            this.FlightRouteList.Items.Add(radListDataItem9);
            this.FlightRouteList.Location = new System.Drawing.Point(6, 25);
            this.FlightRouteList.Name = "FlightRouteList";
            this.FlightRouteList.NullText = "Choose Runway";
            this.FlightRouteList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.FlightRouteList.RootElement.ControlBounds = new System.Drawing.Rectangle(6, 25, 125, 20);
            this.FlightRouteList.RootElement.StretchVertically = true;
            this.FlightRouteList.Size = new System.Drawing.Size(263, 22);
            this.FlightRouteList.TabIndex = 111;
            this.FlightRouteList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.FlightRouteList_SelectedIndexChanged);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.DestinationList);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox13.ForeColor = System.Drawing.Color.White;
            this.groupBox13.Location = new System.Drawing.Point(125, 22);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox13.Size = new System.Drawing.Size(283, 54);
            this.groupBox13.TabIndex = 110;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Destination";
            // 
            // DestinationList
            // 
            this.DestinationList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DestinationList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DestinationList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            radListDataItem10.Text = "Slow";
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "Medium";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "Expedite";
            radListDataItem12.TextWrap = true;
            this.DestinationList.Items.Add(radListDataItem10);
            this.DestinationList.Items.Add(radListDataItem11);
            this.DestinationList.Items.Add(radListDataItem12);
            this.DestinationList.Location = new System.Drawing.Point(6, 25);
            this.DestinationList.Name = "DestinationList";
            this.DestinationList.NullText = "Choose Runway";
            this.DestinationList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.DestinationList.RootElement.ControlBounds = new System.Drawing.Rectangle(6, 25, 125, 20);
            this.DestinationList.RootElement.StretchVertically = true;
            this.DestinationList.Size = new System.Drawing.Size(271, 22);
            this.DestinationList.TabIndex = 111;
            this.DestinationList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.DestinationList_SelectedIndexChanged);
            // 
            // ChangeRoute
            // 
            this.ChangeRoute.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeRoute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeRoute.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.ChangeRoute.ForeColor = System.Drawing.Color.White;
            this.ChangeRoute.Location = new System.Drawing.Point(6, 139);
            this.ChangeRoute.Name = "ChangeRoute";
            this.ChangeRoute.Size = new System.Drawing.Size(402, 30);
            this.ChangeRoute.TabIndex = 114;
            this.ChangeRoute.Text = "Change Route";
            this.ChangeRoute.UseVisualStyleBackColor = false;
            this.ChangeRoute.Click += new System.EventHandler(this.ChangeRoute_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.FlightRulesList);
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox14.ForeColor = System.Drawing.Color.White;
            this.groupBox14.Location = new System.Drawing.Point(8, 22);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox14.Size = new System.Drawing.Size(110, 54);
            this.groupBox14.TabIndex = 109;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Flight Rule";
            // 
            // FlightRulesList
            // 
            this.FlightRulesList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.FlightRulesList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FlightRulesList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            radListDataItem1.Text = "IFR";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "VFR";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Enabled = false;
            radListDataItem3.Text = "Local";
            radListDataItem3.TextWrap = true;
            this.FlightRulesList.Items.Add(radListDataItem1);
            this.FlightRulesList.Items.Add(radListDataItem2);
            this.FlightRulesList.Items.Add(radListDataItem3);
            this.FlightRulesList.Location = new System.Drawing.Point(6, 24);
            this.FlightRulesList.Name = "FlightRulesList";
            this.FlightRulesList.NullText = "Choose Runway";
            this.FlightRulesList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.FlightRulesList.RootElement.ControlBounds = new System.Drawing.Rectangle(6, 24, 125, 20);
            this.FlightRulesList.RootElement.StretchVertically = true;
            this.FlightRulesList.Size = new System.Drawing.Size(96, 22);
            this.FlightRulesList.TabIndex = 111;
            this.FlightRulesList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.FlightRulesList_SelectedIndexChanged);
            // 
            // HoldingRoute
            // 
            this.HoldingRoute.Controls.Add(this.ContinueDirection);
            this.HoldingRoute.Controls.Add(this.ContinueRoute);
            this.HoldingRoute.Controls.Add(this.Holding);
            this.HoldingRoute.Controls.Add(this.HoldingRight);
            this.HoldingRoute.Controls.Add(this.HoldingList);
            this.HoldingRoute.Controls.Add(this.label3);
            this.HoldingRoute.Controls.Add(this.HoldingLeft);
            this.HoldingRoute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HoldingRoute.ForeColor = System.Drawing.Color.White;
            this.HoldingRoute.Location = new System.Drawing.Point(7, 28);
            this.HoldingRoute.Name = "HoldingRoute";
            this.HoldingRoute.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HoldingRoute.Size = new System.Drawing.Size(465, 133);
            this.HoldingRoute.TabIndex = 149;
            this.HoldingRoute.TabStop = false;
            this.HoldingRoute.Text = "Holding";
            // 
            // ContinueDirection
            // 
            this.ContinueDirection.BackColor = System.Drawing.Color.DodgerBlue;
            this.ContinueDirection.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ContinueDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ContinueDirection.ForeColor = System.Drawing.Color.White;
            this.ContinueDirection.Location = new System.Drawing.Point(236, 98);
            this.ContinueDirection.Name = "ContinueDirection";
            this.ContinueDirection.Size = new System.Drawing.Size(220, 28);
            this.ContinueDirection.TabIndex = 111;
            this.ContinueDirection.Text = "Continue Direction";
            this.ContinueDirection.UseVisualStyleBackColor = false;
            this.ContinueDirection.Click += new System.EventHandler(this.ContinueDirection_Click);
            // 
            // ContinueRoute
            // 
            this.ContinueRoute.BackColor = System.Drawing.Color.DodgerBlue;
            this.ContinueRoute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ContinueRoute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ContinueRoute.ForeColor = System.Drawing.Color.White;
            this.ContinueRoute.Location = new System.Drawing.Point(9, 98);
            this.ContinueRoute.Name = "ContinueRoute";
            this.ContinueRoute.Size = new System.Drawing.Size(220, 28);
            this.ContinueRoute.TabIndex = 110;
            this.ContinueRoute.Text = "Continue Route";
            this.ContinueRoute.UseVisualStyleBackColor = false;
            this.ContinueRoute.Click += new System.EventHandler(this.ContinueRoute_Click);
            // 
            // Holding
            // 
            this.Holding.BackColor = System.Drawing.Color.DodgerBlue;
            this.Holding.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Holding.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Holding.ForeColor = System.Drawing.Color.White;
            this.Holding.Location = new System.Drawing.Point(284, 61);
            this.Holding.Name = "Holding";
            this.Holding.Size = new System.Drawing.Size(172, 28);
            this.Holding.TabIndex = 109;
            this.Holding.Text = "Holding";
            this.Holding.UseVisualStyleBackColor = false;
            this.Holding.Click += new System.EventHandler(this.Holding_Click);
            // 
            // HoldingRight
            // 
            this.HoldingRight.BackColor = System.Drawing.Color.DodgerBlue;
            this.HoldingRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HoldingRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HoldingRight.ForeColor = System.Drawing.Color.White;
            this.HoldingRight.Location = new System.Drawing.Point(236, 25);
            this.HoldingRight.Name = "HoldingRight";
            this.HoldingRight.Size = new System.Drawing.Size(220, 28);
            this.HoldingRight.TabIndex = 108;
            this.HoldingRight.Text = "Holding Now Right";
            this.HoldingRight.UseVisualStyleBackColor = false;
            this.HoldingRight.Click += new System.EventHandler(this.HoldingRight_Click);
            // 
            // HoldingList
            // 
            this.HoldingList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.HoldingList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.HoldingList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HoldingList.Location = new System.Drawing.Point(127, 61);
            this.HoldingList.Name = "HoldingList";
            this.HoldingList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.HoldingList.RootElement.ControlBounds = new System.Drawing.Rectangle(127, 61, 125, 20);
            this.HoldingList.RootElement.StretchVertically = true;
            this.HoldingList.Size = new System.Drawing.Size(148, 25);
            this.HoldingList.TabIndex = 106;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(10, 63);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.TabIndex = 107;
            this.label3.Text = "Holding At";
            // 
            // HoldingLeft
            // 
            this.HoldingLeft.BackColor = System.Drawing.Color.DodgerBlue;
            this.HoldingLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HoldingLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HoldingLeft.ForeColor = System.Drawing.Color.White;
            this.HoldingLeft.Location = new System.Drawing.Point(9, 25);
            this.HoldingLeft.Name = "HoldingLeft";
            this.HoldingLeft.Size = new System.Drawing.Size(220, 28);
            this.HoldingLeft.TabIndex = 7;
            this.HoldingLeft.Text = "Holding Now Left";
            this.HoldingLeft.UseVisualStyleBackColor = false;
            this.HoldingLeft.Click += new System.EventHandler(this.HoldingLeft_Click);
            // 
            // HeadingGroup
            // 
            this.HeadingGroup.Controls.Add(this.HeadingRel);
            this.HeadingGroup.Controls.Add(this.HeadingAbs);
            this.HeadingGroup.Controls.Add(this.HeadingRelBox);
            this.HeadingGroup.Controls.Add(this.HeadingAbsBox);
            this.HeadingGroup.Controls.Add(this.label13);
            this.HeadingGroup.Controls.Add(this.label11);
            this.HeadingGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HeadingGroup.ForeColor = System.Drawing.Color.White;
            this.HeadingGroup.Location = new System.Drawing.Point(477, 28);
            this.HeadingGroup.Name = "HeadingGroup";
            this.HeadingGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HeadingGroup.Size = new System.Drawing.Size(236, 133);
            this.HeadingGroup.TabIndex = 148;
            this.HeadingGroup.TabStop = false;
            // 
            // HeadingRel
            // 
            this.HeadingRel.BackColor = System.Drawing.Color.DodgerBlue;
            this.HeadingRel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HeadingRel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HeadingRel.ForeColor = System.Drawing.Color.White;
            this.HeadingRel.Location = new System.Drawing.Point(7, 98);
            this.HeadingRel.Name = "HeadingRel";
            this.HeadingRel.Size = new System.Drawing.Size(220, 28);
            this.HeadingRel.TabIndex = 151;
            this.HeadingRel.Text = "Change Heading";
            this.HeadingRel.UseVisualStyleBackColor = false;
            this.HeadingRel.Click += new System.EventHandler(this.HeadingRel_Click);
            // 
            // HeadingAbs
            // 
            this.HeadingAbs.BackColor = System.Drawing.Color.DodgerBlue;
            this.HeadingAbs.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HeadingAbs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HeadingAbs.ForeColor = System.Drawing.Color.White;
            this.HeadingAbs.Location = new System.Drawing.Point(7, 41);
            this.HeadingAbs.Name = "HeadingAbs";
            this.HeadingAbs.Size = new System.Drawing.Size(220, 28);
            this.HeadingAbs.TabIndex = 112;
            this.HeadingAbs.Text = "Change Heading";
            this.HeadingAbs.UseVisualStyleBackColor = false;
            this.HeadingAbs.Click += new System.EventHandler(this.HeadingAbs_Click);
            // 
            // HeadingRelBox
            // 
            this.HeadingRelBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.HeadingRelBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HeadingRelBox.Location = new System.Drawing.Point(170, 71);
            this.HeadingRelBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.HeadingRelBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.HeadingRelBox.Name = "HeadingRelBox";
            this.HeadingRelBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.HeadingRelBox.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.HeadingRelBox.RootElement.ControlBounds = new System.Drawing.Rectangle(170, 71, 100, 20);
            this.HeadingRelBox.RootElement.StretchVertically = true;
            this.HeadingRelBox.Size = new System.Drawing.Size(56, 25);
            this.HeadingRelBox.TabIndex = 152;
            this.HeadingRelBox.TabStop = false;
            this.HeadingRelBox.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.HeadingRelBox.Value = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            // 
            // HeadingAbsBox
            // 
            this.HeadingAbsBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.HeadingAbsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HeadingAbsBox.Location = new System.Drawing.Point(170, 15);
            this.HeadingAbsBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.HeadingAbsBox.Name = "HeadingAbsBox";
            this.HeadingAbsBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.HeadingAbsBox.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.HeadingAbsBox.RootElement.ControlBounds = new System.Drawing.Rectangle(170, 15, 100, 20);
            this.HeadingAbsBox.RootElement.StretchVertically = true;
            this.HeadingAbsBox.Size = new System.Drawing.Size(56, 25);
            this.HeadingAbsBox.TabIndex = 150;
            this.HeadingAbsBox.TabStop = false;
            this.HeadingAbsBox.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.HeadingAbsBox.Value = new decimal(new int[] {
            360,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(6, 76);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(130, 20);
            this.label13.TabIndex = 103;
            this.label13.Text = "Relative Heading";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(7, 17);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label11.Size = new System.Drawing.Size(136, 20);
            this.label11.TabIndex = 103;
            this.label11.Text = "Absolute Heading";
            // 
            // ChangeRunwayGroup
            // 
            this.ChangeRunwayGroup.Controls.Add(this.ChangeRunwayList);
            this.ChangeRunwayGroup.Controls.Add(this.ChangeRunway);
            this.ChangeRunwayGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ChangeRunwayGroup.ForeColor = System.Drawing.Color.White;
            this.ChangeRunwayGroup.Location = new System.Drawing.Point(536, 474);
            this.ChangeRunwayGroup.Name = "ChangeRunwayGroup";
            this.ChangeRunwayGroup.Size = new System.Drawing.Size(177, 63);
            this.ChangeRunwayGroup.TabIndex = 153;
            this.ChangeRunwayGroup.TabStop = false;
            this.ChangeRunwayGroup.Text = "Change Runway";
            // 
            // ChangeRunwayList
            // 
            this.ChangeRunwayList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ChangeRunwayList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ChangeRunwayList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.ChangeRunwayList.Location = new System.Drawing.Point(11, 29);
            this.ChangeRunwayList.Name = "ChangeRunwayList";
            this.ChangeRunwayList.NullText = "Choose Runway";
            this.ChangeRunwayList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.ChangeRunwayList.RootElement.ControlBounds = new System.Drawing.Rectangle(11, 29, 125, 20);
            this.ChangeRunwayList.RootElement.StretchVertically = true;
            this.ChangeRunwayList.Size = new System.Drawing.Size(79, 22);
            this.ChangeRunwayList.TabIndex = 107;
            // 
            // ChangeRunway
            // 
            this.ChangeRunway.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeRunway.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ChangeRunway.FlatAppearance.BorderSize = 0;
            this.ChangeRunway.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.ChangeRunway.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.ChangeRunway.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeRunway.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ChangeRunway.ForeColor = System.Drawing.Color.White;
            this.ChangeRunway.Location = new System.Drawing.Point(96, 26);
            this.ChangeRunway.Name = "ChangeRunway";
            this.ChangeRunway.Size = new System.Drawing.Size(75, 28);
            this.ChangeRunway.TabIndex = 108;
            this.ChangeRunway.Text = "Ok";
            this.ChangeRunway.UseVisualStyleBackColor = false;
            this.ChangeRunway.Click += new System.EventHandler(this.ChangeRunway_Click);
            // 
            // DirectGoGroup
            // 
            this.DirectGoGroup.Controls.Add(this.DirectGo);
            this.DirectGoGroup.Controls.Add(this.DirectGoList);
            this.DirectGoGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DirectGoGroup.ForeColor = System.Drawing.Color.White;
            this.DirectGoGroup.Location = new System.Drawing.Point(427, 295);
            this.DirectGoGroup.Name = "DirectGoGroup";
            this.DirectGoGroup.Size = new System.Drawing.Size(287, 49);
            this.DirectGoGroup.TabIndex = 152;
            this.DirectGoGroup.TabStop = false;
            this.DirectGoGroup.Text = "Direct Go to";
            // 
            // DirectGo
            // 
            this.DirectGo.BackColor = System.Drawing.Color.DodgerBlue;
            this.DirectGo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DirectGo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DirectGo.ForeColor = System.Drawing.Color.White;
            this.DirectGo.Location = new System.Drawing.Point(172, 18);
            this.DirectGo.Name = "DirectGo";
            this.DirectGo.Size = new System.Drawing.Size(109, 25);
            this.DirectGo.TabIndex = 108;
            this.DirectGo.Text = "Direct Go";
            this.DirectGo.UseVisualStyleBackColor = false;
            this.DirectGo.Click += new System.EventHandler(this.DirectGo_Click);
            // 
            // DirectGoList
            // 
            this.DirectGoList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DirectGoList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DirectGoList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.DirectGoList.Location = new System.Drawing.Point(11, 21);
            this.DirectGoList.Name = "DirectGoList";
            this.DirectGoList.NullText = "Choose Runway";
            this.DirectGoList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.DirectGoList.RootElement.ControlBounds = new System.Drawing.Rectangle(11, 21, 125, 20);
            this.DirectGoList.RootElement.StretchVertically = true;
            this.DirectGoList.Size = new System.Drawing.Size(155, 22);
            this.DirectGoList.TabIndex = 107;
            // 
            // OtherGroup
            // 
            this.OtherGroup.Controls.Add(this.Flypass);
            this.OtherGroup.Controls.Add(this.TouchGo);
            this.OtherGroup.Controls.Add(this.MissedApproach);
            this.OtherGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.OtherGroup.ForeColor = System.Drawing.Color.White;
            this.OtherGroup.Location = new System.Drawing.Point(249, 354);
            this.OtherGroup.Name = "OtherGroup";
            this.OtherGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.OtherGroup.Size = new System.Drawing.Size(281, 123);
            this.OtherGroup.TabIndex = 152;
            this.OtherGroup.TabStop = false;
            // 
            // Flypass
            // 
            this.Flypass.BackColor = System.Drawing.Color.DodgerBlue;
            this.Flypass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Flypass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Flypass.ForeColor = System.Drawing.Color.White;
            this.Flypass.Location = new System.Drawing.Point(6, 85);
            this.Flypass.Name = "Flypass";
            this.Flypass.Size = new System.Drawing.Size(268, 28);
            this.Flypass.TabIndex = 109;
            this.Flypass.Text = "Flypass / Go Round";
            this.Flypass.UseVisualStyleBackColor = false;
            this.Flypass.Click += new System.EventHandler(this.Flypass_Click);
            // 
            // TouchGo
            // 
            this.TouchGo.BackColor = System.Drawing.Color.DodgerBlue;
            this.TouchGo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TouchGo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TouchGo.ForeColor = System.Drawing.Color.White;
            this.TouchGo.Location = new System.Drawing.Point(6, 51);
            this.TouchGo.Name = "TouchGo";
            this.TouchGo.Size = new System.Drawing.Size(268, 28);
            this.TouchGo.TabIndex = 108;
            this.TouchGo.Text = "Touch and Go";
            this.TouchGo.UseVisualStyleBackColor = false;
            this.TouchGo.Click += new System.EventHandler(this.TouchGo_Click);
            // 
            // MissedApproach
            // 
            this.MissedApproach.BackColor = System.Drawing.Color.DodgerBlue;
            this.MissedApproach.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.MissedApproach.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.MissedApproach.ForeColor = System.Drawing.Color.White;
            this.MissedApproach.Location = new System.Drawing.Point(6, 17);
            this.MissedApproach.Name = "MissedApproach";
            this.MissedApproach.Size = new System.Drawing.Size(268, 28);
            this.MissedApproach.TabIndex = 7;
            this.MissedApproach.Text = "Missed Approach";
            this.MissedApproach.UseVisualStyleBackColor = false;
            this.MissedApproach.Click += new System.EventHandler(this.MissedApproach_Click);
            // 
            // DownwindOrbitGroup
            // 
            this.DownwindOrbitGroup.Controls.Add(this.Orbit);
            this.DownwindOrbitGroup.Controls.Add(this.ExtendDownwind);
            this.DownwindOrbitGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DownwindOrbitGroup.ForeColor = System.Drawing.Color.White;
            this.DownwindOrbitGroup.Location = new System.Drawing.Point(536, 354);
            this.DownwindOrbitGroup.Name = "DownwindOrbitGroup";
            this.DownwindOrbitGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DownwindOrbitGroup.Size = new System.Drawing.Size(178, 96);
            this.DownwindOrbitGroup.TabIndex = 151;
            this.DownwindOrbitGroup.TabStop = false;
            // 
            // Orbit
            // 
            this.Orbit.BackColor = System.Drawing.Color.DodgerBlue;
            this.Orbit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Orbit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Orbit.ForeColor = System.Drawing.Color.White;
            this.Orbit.Location = new System.Drawing.Point(9, 59);
            this.Orbit.Name = "Orbit";
            this.Orbit.Size = new System.Drawing.Size(163, 28);
            this.Orbit.TabIndex = 108;
            this.Orbit.Text = "Orbit";
            this.Orbit.UseVisualStyleBackColor = false;
            this.Orbit.Click += new System.EventHandler(this.Orbit_Click);
            // 
            // ExtendDownwind
            // 
            this.ExtendDownwind.BackColor = System.Drawing.Color.DodgerBlue;
            this.ExtendDownwind.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ExtendDownwind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ExtendDownwind.ForeColor = System.Drawing.Color.White;
            this.ExtendDownwind.Location = new System.Drawing.Point(9, 25);
            this.ExtendDownwind.Name = "ExtendDownwind";
            this.ExtendDownwind.Size = new System.Drawing.Size(163, 28);
            this.ExtendDownwind.TabIndex = 7;
            this.ExtendDownwind.Text = "Extend Downwind";
            this.ExtendDownwind.UseVisualStyleBackColor = false;
            this.ExtendDownwind.Click += new System.EventHandler(this.ExtendDownwind_Click);
            // 
            // CircuitApproachGroup
            // 
            this.CircuitApproachGroup.Controls.Add(this.label8);
            this.CircuitApproachGroup.Controls.Add(this.RunwayList);
            this.CircuitApproachGroup.Controls.Add(this.InstrumentApp);
            this.CircuitApproachGroup.Controls.Add(this.JoinCircuit);
            this.CircuitApproachGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CircuitApproachGroup.ForeColor = System.Drawing.Color.White;
            this.CircuitApproachGroup.Location = new System.Drawing.Point(427, 167);
            this.CircuitApproachGroup.Name = "CircuitApproachGroup";
            this.CircuitApproachGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CircuitApproachGroup.Size = new System.Drawing.Size(287, 124);
            this.CircuitApproachGroup.TabIndex = 150;
            this.CircuitApproachGroup.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(7, 20);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(66, 20);
            this.label8.TabIndex = 110;
            this.label8.Text = "Runway";
            // 
            // RunwayList
            // 
            this.RunwayList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.RunwayList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RunwayList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.RunwayList.Location = new System.Drawing.Point(86, 20);
            this.RunwayList.Name = "RunwayList";
            this.RunwayList.NullText = "Choose Runway";
            this.RunwayList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RunwayList.RootElement.ControlBounds = new System.Drawing.Rectangle(86, 20, 125, 20);
            this.RunwayList.RootElement.StretchVertically = true;
            this.RunwayList.Size = new System.Drawing.Size(94, 22);
            this.RunwayList.TabIndex = 109;
            // 
            // InstrumentApp
            // 
            this.InstrumentApp.BackColor = System.Drawing.Color.DodgerBlue;
            this.InstrumentApp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.InstrumentApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.InstrumentApp.ForeColor = System.Drawing.Color.White;
            this.InstrumentApp.Location = new System.Drawing.Point(11, 48);
            this.InstrumentApp.Name = "InstrumentApp";
            this.InstrumentApp.Size = new System.Drawing.Size(266, 28);
            this.InstrumentApp.TabIndex = 108;
            this.InstrumentApp.Text = "Instrument Approach";
            this.InstrumentApp.UseVisualStyleBackColor = false;
            this.InstrumentApp.Click += new System.EventHandler(this.InstrumentApp_Click);
            // 
            // JoinCircuit
            // 
            this.JoinCircuit.BackColor = System.Drawing.Color.DodgerBlue;
            this.JoinCircuit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.JoinCircuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.JoinCircuit.ForeColor = System.Drawing.Color.White;
            this.JoinCircuit.Location = new System.Drawing.Point(11, 82);
            this.JoinCircuit.Name = "JoinCircuit";
            this.JoinCircuit.Size = new System.Drawing.Size(266, 28);
            this.JoinCircuit.TabIndex = 7;
            this.JoinCircuit.Text = "Join Circuit";
            this.JoinCircuit.UseVisualStyleBackColor = false;
            this.JoinCircuit.Click += new System.EventHandler(this.JoinCircuit_Click);
            // 
            // PerformanceGroup
            // 
            this.PerformanceGroup.Controls.Add(this.ChangeSpeed);
            this.PerformanceGroup.Controls.Add(this.ChangeAltitude);
            this.PerformanceGroup.Controls.Add(this.SpeedValue);
            this.PerformanceGroup.Controls.Add(this.AltitudeValue);
            this.PerformanceGroup.Controls.Add(this.label1);
            this.PerformanceGroup.Controls.Add(this.label2);
            this.PerformanceGroup.Controls.Add(this.label9);
            this.PerformanceGroup.Controls.Add(this.label23);
            this.PerformanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PerformanceGroup.ForeColor = System.Drawing.Color.White;
            this.PerformanceGroup.Location = new System.Drawing.Point(6, 354);
            this.PerformanceGroup.Name = "PerformanceGroup";
            this.PerformanceGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PerformanceGroup.Size = new System.Drawing.Size(237, 183);
            this.PerformanceGroup.TabIndex = 146;
            this.PerformanceGroup.TabStop = false;
            // 
            // ChangeSpeed
            // 
            this.ChangeSpeed.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeSpeed.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ChangeSpeed.ForeColor = System.Drawing.Color.White;
            this.ChangeSpeed.Location = new System.Drawing.Point(7, 137);
            this.ChangeSpeed.Name = "ChangeSpeed";
            this.ChangeSpeed.Size = new System.Drawing.Size(223, 28);
            this.ChangeSpeed.TabIndex = 154;
            this.ChangeSpeed.Text = "Change Speed";
            this.ChangeSpeed.UseVisualStyleBackColor = false;
            this.ChangeSpeed.Click += new System.EventHandler(this.ChangeSpeed_Click);
            // 
            // ChangeAltitude
            // 
            this.ChangeAltitude.BackColor = System.Drawing.Color.DodgerBlue;
            this.ChangeAltitude.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ChangeAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ChangeAltitude.ForeColor = System.Drawing.Color.White;
            this.ChangeAltitude.Location = new System.Drawing.Point(7, 64);
            this.ChangeAltitude.Name = "ChangeAltitude";
            this.ChangeAltitude.Size = new System.Drawing.Size(223, 28);
            this.ChangeAltitude.TabIndex = 153;
            this.ChangeAltitude.Text = "Change Altitude";
            this.ChangeAltitude.UseVisualStyleBackColor = false;
            this.ChangeAltitude.Click += new System.EventHandler(this.ChangeAltitude_Click);
            // 
            // SpeedValue
            // 
            this.SpeedValue.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.SpeedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.SpeedValue.Location = new System.Drawing.Point(69, 106);
            this.SpeedValue.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.SpeedValue.Name = "SpeedValue";
            this.SpeedValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.SpeedValue.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.SpeedValue.RootElement.ControlBounds = new System.Drawing.Rectangle(102, 118, 100, 20);
            this.SpeedValue.RootElement.StretchVertically = true;
            this.SpeedValue.Size = new System.Drawing.Size(121, 25);
            this.SpeedValue.TabIndex = 152;
            this.SpeedValue.TabStop = false;
            this.SpeedValue.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.SpeedValue.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // AltitudeValue
            // 
            this.AltitudeValue.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AltitudeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AltitudeValue.Location = new System.Drawing.Point(69, 30);
            this.AltitudeValue.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.AltitudeValue.Name = "AltitudeValue";
            this.AltitudeValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.AltitudeValue.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.AltitudeValue.RootElement.ControlBounds = new System.Drawing.Rectangle(101, 16, 100, 20);
            this.AltitudeValue.RootElement.StretchVertically = true;
            this.AltitudeValue.Size = new System.Drawing.Size(121, 25);
            this.AltitudeValue.TabIndex = 151;
            this.AltitudeValue.TabStop = false;
            this.AltitudeValue.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.AltitudeValue.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label1.ForeColor = System.Drawing.Color.Tomato;
            this.label1.Location = new System.Drawing.Point(196, 113);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 144;
            this.label1.Text = "Knots";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label2.ForeColor = System.Drawing.Color.Tomato;
            this.label2.Location = new System.Drawing.Point(205, 36);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 144;
            this.label2.Text = "feet";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(7, 108);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(56, 20);
            this.label9.TabIndex = 103;
            this.label9.Text = "Speed";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(6, 32);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label23.Size = new System.Drawing.Size(63, 20);
            this.label23.TabIndex = 103;
            this.label23.Text = "Altitude";
            // 
            // ReturnBase
            // 
            this.ReturnBase.BackColor = System.Drawing.Color.DodgerBlue;
            this.ReturnBase.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ReturnBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ReturnBase.ForeColor = System.Drawing.Color.White;
            this.ReturnBase.Location = new System.Drawing.Point(248, 549);
            this.ReturnBase.Name = "ReturnBase";
            this.ReturnBase.Size = new System.Drawing.Size(236, 28);
            this.ReturnBase.TabIndex = 7;
            this.ReturnBase.Text = "Return to Base";
            this.ReturnBase.UseVisualStyleBackColor = false;
            this.ReturnBase.Click += new System.EventHandler(this.ReturnBase_Click);
            // 
            // AbortTakeOff
            // 
            this.AbortTakeOff.BackColor = System.Drawing.Color.DodgerBlue;
            this.AbortTakeOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AbortTakeOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AbortTakeOff.ForeColor = System.Drawing.Color.White;
            this.AbortTakeOff.Location = new System.Drawing.Point(489, 549);
            this.AbortTakeOff.Name = "AbortTakeOff";
            this.AbortTakeOff.Size = new System.Drawing.Size(236, 28);
            this.AbortTakeOff.TabIndex = 154;
            this.AbortTakeOff.Text = "Aborted Take Off";
            this.AbortTakeOff.UseVisualStyleBackColor = false;
            this.AbortTakeOff.Click += new System.EventHandler(this.AbortTakeOff_Click);
            // 
            // ILSModeBtn
            // 
            this.ILSModeBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.ILSModeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ILSModeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ILSModeBtn.ForeColor = System.Drawing.Color.White;
            this.ILSModeBtn.Location = new System.Drawing.Point(5, 549);
            this.ILSModeBtn.Name = "ILSModeBtn";
            this.ILSModeBtn.Size = new System.Drawing.Size(236, 28);
            this.ILSModeBtn.TabIndex = 155;
            this.ILSModeBtn.Text = "Enter ILS Mode";
            this.ILSModeBtn.UseVisualStyleBackColor = false;
            this.ILSModeBtn.Click += new System.EventHandler(this.ILSModeBtn_Click);
            // 
            // FlightForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(730, 610);
            this.Controls.Add(this.ILSModeBtn);
            this.Controls.Add(this.AbortTakeOff);
            this.Controls.Add(this.GroundCommandGroup);
            this.Controls.Add(this.ReturnBase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FlightForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FlightForm";
            this.ThemeName = "ControlDefault";
            this.GroundCommandGroup.ResumeLayout(false);
            this.LandingRunwayGroup.ResumeLayout(false);
            this.LandingRunwayGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LandingRunwayList)).EndInit();
            this.DirectGoILSGroup.ResumeLayout(false);
            this.DirectGoILSGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DirectGoILSList)).EndInit();
            this.FlightRouteGroup.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartPointList)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRouteList)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationList)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FlightRulesList)).EndInit();
            this.HoldingRoute.ResumeLayout(false);
            this.HoldingRoute.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HoldingList)).EndInit();
            this.HeadingGroup.ResumeLayout(false);
            this.HeadingGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HeadingRelBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeadingAbsBox)).EndInit();
            this.ChangeRunwayGroup.ResumeLayout(false);
            this.ChangeRunwayGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChangeRunwayList)).EndInit();
            this.DirectGoGroup.ResumeLayout(false);
            this.DirectGoGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DirectGoList)).EndInit();
            this.OtherGroup.ResumeLayout(false);
            this.DownwindOrbitGroup.ResumeLayout(false);
            this.CircuitApproachGroup.ResumeLayout(false);
            this.CircuitApproachGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).EndInit();
            this.PerformanceGroup.ResumeLayout(false);
            this.PerformanceGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AltitudeValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroundCommandGroup;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox HeadingGroup;
        private Telerik.WinControls.UI.RadSpinEditor HeadingAbsBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox PerformanceGroup;
        private System.Windows.Forms.GroupBox HoldingRoute;
        private System.Windows.Forms.Button ContinueDirection;
        private System.Windows.Forms.Button ContinueRoute;
        private System.Windows.Forms.Button Holding;
        private System.Windows.Forms.Button HoldingRight;
        private Telerik.WinControls.UI.RadDropDownList HoldingList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button HoldingLeft;
        private System.Windows.Forms.Button ReturnBase;
        private System.Windows.Forms.GroupBox CircuitApproachGroup;
        private System.Windows.Forms.Button InstrumentApp;
        private System.Windows.Forms.Button JoinCircuit;
        private Telerik.WinControls.UI.RadDropDownList DirectGoList;
        private System.Windows.Forms.GroupBox OtherGroup;
        private System.Windows.Forms.Button Flypass;
        private System.Windows.Forms.Button TouchGo;
        private System.Windows.Forms.Button MissedApproach;
        private System.Windows.Forms.GroupBox DownwindOrbitGroup;
        private System.Windows.Forms.Button Orbit;
        private System.Windows.Forms.Button ExtendDownwind;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadDropDownList RunwayList;
        private System.Windows.Forms.GroupBox DirectGoGroup;
        private System.Windows.Forms.Button ClearToLand;
        private System.Windows.Forms.GroupBox ChangeRunwayGroup;
        private Telerik.WinControls.UI.RadDropDownList ChangeRunwayList;
        private System.Windows.Forms.Button ChangeRunway;
        private System.Windows.Forms.Button RockingWing;
        private System.Windows.Forms.GroupBox FlightRouteGroup;
        private System.Windows.Forms.GroupBox groupBox12;
        private Telerik.WinControls.UI.RadDropDownList FlightRouteList;
        private System.Windows.Forms.GroupBox groupBox13;
        private Telerik.WinControls.UI.RadDropDownList DestinationList;
        private System.Windows.Forms.Button ChangeRoute;
        private System.Windows.Forms.GroupBox groupBox14;
        private Telerik.WinControls.UI.RadDropDownList FlightRulesList;
        private System.Windows.Forms.Button AbortTakeOff;
        private Telerik.WinControls.UI.RadSpinEditor SpeedValue;
        private Telerik.WinControls.UI.RadSpinEditor AltitudeValue;
        private System.Windows.Forms.Button HeadingAbs;
        private System.Windows.Forms.Button HeadingRel;
        private Telerik.WinControls.UI.RadSpinEditor HeadingRelBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button ChangeSpeed;
        private System.Windows.Forms.Button ChangeAltitude;
        private Telerik.WinControls.UI.RadDropDownList StartPointList;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button DirectGo;
        private System.Windows.Forms.Button ILSModeBtn;
        private System.Windows.Forms.GroupBox DirectGoILSGroup;
        private System.Windows.Forms.Button DirectGoILS;
        private Telerik.WinControls.UI.RadDropDownList DirectGoILSList;
        private System.Windows.Forms.GroupBox LandingRunwayGroup;
        private Telerik.WinControls.UI.RadDropDownList LandingRunwayList;
    }
}
