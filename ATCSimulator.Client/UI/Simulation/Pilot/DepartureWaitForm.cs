﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.SimulationService;

namespace ATCSimulator.Client.UI.Simulation.Pilot
{
    public partial class DepartureWaitForm : Telerik.WinControls.UI.RadForm
    {
        string callsign = "";
        public DepartureWaitForm()
        {
            InitializeComponent();
        }
        public void Setup(AircraftData aircraft)
        {
            DepartureTime.Text = aircraft.departure_time.ToString("c");
            ReadyAircraft.Enabled = aircraft.pilot_properties.ready_departure.is_enabled;
            callsign = aircraft.callsign;
        }

        private void ReadyAircraft_Click(object sender, EventArgs e)
        {
            Services.SimulationConnection.ReadyAircraft(callsign);
        }
    }
}
