﻿namespace ATCSimulator.Client.UI.Simulation.Pilot
{
    partial class DepartureWaitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.DepartureTime = new System.Windows.Forms.Label();
            this.ReadyAircraft = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(251, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Departure Time ";
            // 
            // DepartureTime
            // 
            this.DepartureTime.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.DepartureTime.ForeColor = System.Drawing.Color.DodgerBlue;
            this.DepartureTime.Location = new System.Drawing.Point(168, 269);
            this.DepartureTime.Name = "DepartureTime";
            this.DepartureTime.Size = new System.Drawing.Size(351, 35);
            this.DepartureTime.TabIndex = 2;
            this.DepartureTime.Text = "00:02:00";
            this.DepartureTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ReadyAircraft
            // 
            this.ReadyAircraft.BackColor = System.Drawing.Color.DodgerBlue;
            this.ReadyAircraft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.ReadyAircraft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.ReadyAircraft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ReadyAircraft.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.ReadyAircraft.ForeColor = System.Drawing.Color.White;
            this.ReadyAircraft.Location = new System.Drawing.Point(229, 307);
            this.ReadyAircraft.Name = "ReadyAircraft";
            this.ReadyAircraft.Size = new System.Drawing.Size(227, 45);
            this.ReadyAircraft.TabIndex = 105;
            this.ReadyAircraft.Text = "Ready Aircraft";
            this.ReadyAircraft.UseVisualStyleBackColor = false;
            this.ReadyAircraft.Click += new System.EventHandler(this.ReadyAircraft_Click);
            // 
            // DepartureWaitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(708, 700);
            this.Controls.Add(this.ReadyAircraft);
            this.Controls.Add(this.DepartureTime);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DepartureWaitForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "GroundForm";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label DepartureTime;
        private System.Windows.Forms.Button ReadyAircraft;

    }
}
