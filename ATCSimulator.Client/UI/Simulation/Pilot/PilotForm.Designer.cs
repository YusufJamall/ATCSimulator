﻿namespace ATCSimulator.Client.UI.Simulation.Pilot
{
    partial class PilotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// aClean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AircraftInfo = new System.Windows.Forms.Panel();
            this.AircraftLiveryLabel = new System.Windows.Forms.Label();
            this.AircraftModelLabel = new System.Windows.Forms.Label();
            this.AircraftCallsignLabel = new System.Windows.Forms.Label();
            this.AircraftLiveryImage = new System.Windows.Forms.PictureBox();
            this.FullnameLabel = new System.Windows.Forms.Label();
            this.SceneryLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.footnote = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PCRoleLabel = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.UserPicture = new System.Windows.Forms.PictureBox();
            this.LineBottom = new System.Windows.Forms.PictureBox();
            this.TimeSimulationLabel = new System.Windows.Forms.Label();
            this.LineBlue = new System.Windows.Forms.PictureBox();
            this.CommandPanel = new System.Windows.Forms.Panel();
            this.AircraftListPanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.TimeLabel = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.WindSpeedLabel = new System.Windows.Forms.Label();
            this.WindDirectionLabel = new System.Windows.Forms.Label();
            this.TemperatureLabel = new System.Windows.Forms.Label();
            this.VisibilityLabel = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.WeatherLabel = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.AircraftInfoGroup = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.PositionLabel = new System.Windows.Forms.Label();
            this.EngineLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.LiveryLabel = new System.Windows.Forms.Label();
            this.FlightPlanButton = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.SimulationLog = new System.Windows.Forms.RichTextBox();
            this.AircraftInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftLiveryImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBlue)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.AircraftInfoGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // AircraftInfo
            // 
            this.AircraftInfo.Controls.Add(this.AircraftLiveryLabel);
            this.AircraftInfo.Controls.Add(this.AircraftModelLabel);
            this.AircraftInfo.Controls.Add(this.AircraftCallsignLabel);
            this.AircraftInfo.Controls.Add(this.AircraftLiveryImage);
            this.AircraftInfo.Location = new System.Drawing.Point(973, 75);
            this.AircraftInfo.Name = "AircraftInfo";
            this.AircraftInfo.Size = new System.Drawing.Size(380, 60);
            this.AircraftInfo.TabIndex = 153;
            this.AircraftInfo.Visible = false;
            // 
            // AircraftLiveryLabel
            // 
            this.AircraftLiveryLabel.AutoSize = true;
            this.AircraftLiveryLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.AircraftLiveryLabel.ForeColor = System.Drawing.Color.White;
            this.AircraftLiveryLabel.Location = new System.Drawing.Point(61, 6);
            this.AircraftLiveryLabel.Name = "AircraftLiveryLabel";
            this.AircraftLiveryLabel.Size = new System.Drawing.Size(75, 21);
            this.AircraftLiveryLabel.TabIndex = 134;
            this.AircraftLiveryLabel.Text = "Air Asia";
            // 
            // AircraftModelLabel
            // 
            this.AircraftModelLabel.AutoSize = true;
            this.AircraftModelLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.AircraftModelLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftModelLabel.Location = new System.Drawing.Point(61, 33);
            this.AircraftModelLabel.Name = "AircraftModelLabel";
            this.AircraftModelLabel.Size = new System.Drawing.Size(86, 21);
            this.AircraftModelLabel.TabIndex = 133;
            this.AircraftModelLabel.Text = "B737-400";
            // 
            // AircraftCallsignLabel
            // 
            this.AircraftCallsignLabel.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.AircraftCallsignLabel.ForeColor = System.Drawing.Color.White;
            this.AircraftCallsignLabel.Location = new System.Drawing.Point(242, 2);
            this.AircraftCallsignLabel.Name = "AircraftCallsignLabel";
            this.AircraftCallsignLabel.Size = new System.Drawing.Size(136, 55);
            this.AircraftCallsignLabel.TabIndex = 132;
            this.AircraftCallsignLabel.Text = "AWX 2021";
            this.AircraftCallsignLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AircraftLiveryImage
            // 
            this.AircraftLiveryImage.BackColor = System.Drawing.Color.Transparent;
            this.AircraftLiveryImage.Image = global::ATCSimulator.Client.Properties.Resources.LionAir_M;
            this.AircraftLiveryImage.Location = new System.Drawing.Point(3, 2);
            this.AircraftLiveryImage.Name = "AircraftLiveryImage";
            this.AircraftLiveryImage.Size = new System.Drawing.Size(55, 55);
            this.AircraftLiveryImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.AircraftLiveryImage.TabIndex = 131;
            this.AircraftLiveryImage.TabStop = false;
            // 
            // FullnameLabel
            // 
            this.FullnameLabel.AutoSize = true;
            this.FullnameLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.FullnameLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.FullnameLabel.Location = new System.Drawing.Point(1042, 38);
            this.FullnameLabel.Name = "FullnameLabel";
            this.FullnameLabel.Size = new System.Drawing.Size(42, 21);
            this.FullnameLabel.TabIndex = 150;
            this.FullnameLabel.Text = "User";
            // 
            // SceneryLabel
            // 
            this.SceneryLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.SceneryLabel.ForeColor = System.Drawing.Color.White;
            this.SceneryLabel.Location = new System.Drawing.Point(364, 6);
            this.SceneryLabel.Name = "SceneryLabel";
            this.SceneryLabel.Size = new System.Drawing.Size(290, 21);
            this.SceneryLabel.TabIndex = 147;
            this.SceneryLabel.Text = "Seahorse";
            this.SceneryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(266, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 21);
            this.label4.TabIndex = 146;
            this.label4.Text = "Scenery :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(827, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 21);
            this.label2.TabIndex = 144;
            this.label2.Text = "Time :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(973, 574);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 22);
            this.label1.TabIndex = 143;
            this.label1.Text = "Simulation Log";
            // 
            // footnote
            // 
            this.footnote.AutoSize = true;
            this.footnote.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.footnote.ForeColor = System.Drawing.Color.White;
            this.footnote.Location = new System.Drawing.Point(4, 749);
            this.footnote.Name = "footnote";
            this.footnote.Size = new System.Drawing.Size(330, 16);
            this.footnote.TabIndex = 141;
            this.footnote.Text = "Copyright © 2015 Media Indo Teknologi | All Rights Reserved";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.lineWhite;
            this.pictureBox1.Location = new System.Drawing.Point(257, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(4, 735);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 138;
            this.pictureBox1.TabStop = false;
            // 
            // PCRoleLabel
            // 
            this.PCRoleLabel.AutoSize = true;
            this.PCRoleLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.PCRoleLabel.ForeColor = System.Drawing.Color.White;
            this.PCRoleLabel.Location = new System.Drawing.Point(1042, 12);
            this.PCRoleLabel.Name = "PCRoleLabel";
            this.PCRoleLabel.Size = new System.Drawing.Size(44, 21);
            this.PCRoleLabel.TabIndex = 149;
            this.PCRoleLabel.Text = "Role";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox3.Location = new System.Drawing.Point(973, 599);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(380, 3);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 152;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox2.Location = new System.Drawing.Point(973, 69);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(380, 3);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 151;
            this.pictureBox2.TabStop = false;
            // 
            // UserPicture
            // 
            this.UserPicture.BackColor = System.Drawing.Color.Transparent;
            this.UserPicture.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            this.UserPicture.Location = new System.Drawing.Point(976, 6);
            this.UserPicture.Name = "UserPicture";
            this.UserPicture.Size = new System.Drawing.Size(60, 60);
            this.UserPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.UserPicture.TabIndex = 148;
            this.UserPicture.TabStop = false;
            // 
            // LineBottom
            // 
            this.LineBottom.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.LineBottom.Location = new System.Drawing.Point(5, 743);
            this.LineBottom.Name = "LineBottom";
            this.LineBottom.Size = new System.Drawing.Size(1348, 3);
            this.LineBottom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LineBottom.TabIndex = 142;
            this.LineBottom.TabStop = false;
            // 
            // TimeSimulationLabel
            // 
            this.TimeSimulationLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.TimeSimulationLabel.ForeColor = System.Drawing.Color.White;
            this.TimeSimulationLabel.Location = new System.Drawing.Point(877, 6);
            this.TimeSimulationLabel.Name = "TimeSimulationLabel";
            this.TimeSimulationLabel.Size = new System.Drawing.Size(93, 21);
            this.TimeSimulationLabel.TabIndex = 145;
            this.TimeSimulationLabel.Text = "00:00:00";
            this.TimeSimulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LineBlue
            // 
            this.LineBlue.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.LineBlue.Location = new System.Drawing.Point(7, 31);
            this.LineBlue.Name = "LineBlue";
            this.LineBlue.Size = new System.Drawing.Size(963, 4);
            this.LineBlue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LineBlue.TabIndex = 137;
            this.LineBlue.TabStop = false;
            // 
            // CommandPanel
            // 
            this.CommandPanel.Location = new System.Drawing.Point(262, 38);
            this.CommandPanel.Name = "CommandPanel";
            this.CommandPanel.Size = new System.Drawing.Size(708, 700);
            this.CommandPanel.TabIndex = 135;
            // 
            // AircraftListPanel
            // 
            this.AircraftListPanel.AutoScroll = true;
            this.AircraftListPanel.Location = new System.Drawing.Point(6, 38);
            this.AircraftListPanel.Name = "AircraftListPanel";
            this.AircraftListPanel.Size = new System.Drawing.Size(250, 700);
            this.AircraftListPanel.TabIndex = 134;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 22);
            this.label5.TabIndex = 133;
            this.label5.Text = "Aircraft List";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.TimeLabel);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.WindSpeedLabel);
            this.groupBox6.Controls.Add(this.WindDirectionLabel);
            this.groupBox6.Controls.Add(this.TemperatureLabel);
            this.groupBox6.Controls.Add(this.VisibilityLabel);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this.WeatherLabel);
            this.groupBox6.Controls.Add(this.label49);
            this.groupBox6.Controls.Add(this.label50);
            this.groupBox6.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.groupBox6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBox6.Location = new System.Drawing.Point(977, 342);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(371, 207);
            this.groupBox6.TabIndex = 155;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Airport Environment Info";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(338, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 19);
            this.label3.TabIndex = 123;
            this.label3.Text = "m";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(336, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 16);
            this.label6.TabIndex = 122;
            this.label6.Text = "o";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(344, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 19);
            this.label7.TabIndex = 121;
            this.label7.Text = "C";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(340, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 19);
            this.label13.TabIndex = 120;
            this.label13.Text = "N";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(344, 51);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(15, 16);
            this.label36.TabIndex = 119;
            this.label36.Text = "o";
            // 
            // TimeLabel
            // 
            this.TimeLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.TimeLabel.ForeColor = System.Drawing.Color.Tomato;
            this.TimeLabel.Location = new System.Drawing.Point(175, 175);
            this.TimeLabel.Name = "TimeLabel";
            this.TimeLabel.Size = new System.Drawing.Size(190, 25);
            this.TimeLabel.TabIndex = 25;
            this.TimeLabel.Text = "00:00";
            this.TimeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(8, 175);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 21);
            this.label33.TabIndex = 24;
            this.label33.Text = "Time";
            // 
            // WindSpeedLabel
            // 
            this.WindSpeedLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.WindSpeedLabel.ForeColor = System.Drawing.Color.Tomato;
            this.WindSpeedLabel.Location = new System.Drawing.Point(193, 85);
            this.WindSpeedLabel.Name = "WindSpeedLabel";
            this.WindSpeedLabel.Size = new System.Drawing.Size(147, 25);
            this.WindSpeedLabel.TabIndex = 23;
            this.WindSpeedLabel.Text = "0";
            this.WindSpeedLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // WindDirectionLabel
            // 
            this.WindDirectionLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.WindDirectionLabel.ForeColor = System.Drawing.Color.Tomato;
            this.WindDirectionLabel.Location = new System.Drawing.Point(189, 55);
            this.WindDirectionLabel.Name = "WindDirectionLabel";
            this.WindDirectionLabel.Size = new System.Drawing.Size(151, 25);
            this.WindDirectionLabel.TabIndex = 22;
            this.WindDirectionLabel.Text = "0";
            this.WindDirectionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // TemperatureLabel
            // 
            this.TemperatureLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.TemperatureLabel.ForeColor = System.Drawing.Color.Tomato;
            this.TemperatureLabel.Location = new System.Drawing.Point(244, 115);
            this.TemperatureLabel.Name = "TemperatureLabel";
            this.TemperatureLabel.Size = new System.Drawing.Size(88, 25);
            this.TemperatureLabel.TabIndex = 21;
            this.TemperatureLabel.Text = "0";
            this.TemperatureLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // VisibilityLabel
            // 
            this.VisibilityLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.VisibilityLabel.ForeColor = System.Drawing.Color.Tomato;
            this.VisibilityLabel.Location = new System.Drawing.Point(228, 145);
            this.VisibilityLabel.Name = "VisibilityLabel";
            this.VisibilityLabel.Size = new System.Drawing.Size(112, 25);
            this.VisibilityLabel.TabIndex = 20;
            this.VisibilityLabel.Text = "0";
            this.VisibilityLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(8, 145);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 21);
            this.label43.TabIndex = 14;
            this.label43.Text = "Visibility";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(8, 115);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(112, 21);
            this.label44.TabIndex = 12;
            this.label44.Text = "Temperature";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(8, 55);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(123, 21);
            this.label45.TabIndex = 9;
            this.label45.Text = "Wind Direction";
            // 
            // WeatherLabel
            // 
            this.WeatherLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.WeatherLabel.ForeColor = System.Drawing.Color.Tomato;
            this.WeatherLabel.Location = new System.Drawing.Point(173, 25);
            this.WeatherLabel.Name = "WeatherLabel";
            this.WeatherLabel.Size = new System.Drawing.Size(190, 25);
            this.WeatherLabel.TabIndex = 8;
            this.WeatherLabel.Text = "SUNNY";
            this.WeatherLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(8, 25);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(78, 21);
            this.label49.TabIndex = 7;
            this.label49.Text = "Weather";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(8, 85);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(103, 21);
            this.label50.TabIndex = 2;
            this.label50.Text = "Wind Speed";
            // 
            // AircraftInfoGroup
            // 
            this.AircraftInfoGroup.Controls.Add(this.label18);
            this.AircraftInfoGroup.Controls.Add(this.PositionLabel);
            this.AircraftInfoGroup.Controls.Add(this.EngineLabel);
            this.AircraftInfoGroup.Controls.Add(this.label15);
            this.AircraftInfoGroup.Controls.Add(this.label28);
            this.AircraftInfoGroup.Controls.Add(this.LiveryLabel);
            this.AircraftInfoGroup.Controls.Add(this.FlightPlanButton);
            this.AircraftInfoGroup.Controls.Add(this.label24);
            this.AircraftInfoGroup.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.AircraftInfoGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftInfoGroup.Location = new System.Drawing.Point(977, 159);
            this.AircraftInfoGroup.Name = "AircraftInfoGroup";
            this.AircraftInfoGroup.Size = new System.Drawing.Size(374, 151);
            this.AircraftInfoGroup.TabIndex = 154;
            this.AircraftInfoGroup.TabStop = false;
            this.AircraftInfoGroup.Text = "Aircraft Info";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(8, 85);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 21);
            this.label18.TabIndex = 11;
            this.label18.Text = "Engine Status ";
            // 
            // PositionLabel
            // 
            this.PositionLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.PositionLabel.ForeColor = System.Drawing.Color.Tomato;
            this.PositionLabel.Location = new System.Drawing.Point(165, 55);
            this.PositionLabel.Name = "PositionLabel";
            this.PositionLabel.Size = new System.Drawing.Size(199, 21);
            this.PositionLabel.TabIndex = 10;
            this.PositionLabel.Text = "BAY International 3";
            this.PositionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // EngineLabel
            // 
            this.EngineLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.EngineLabel.ForeColor = System.Drawing.Color.Lime;
            this.EngineLabel.Location = new System.Drawing.Point(210, 85);
            this.EngineLabel.Name = "EngineLabel";
            this.EngineLabel.Size = new System.Drawing.Size(155, 21);
            this.EngineLabel.TabIndex = 12;
            this.EngineLabel.Text = "OK";
            this.EngineLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(8, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(136, 21);
            this.label15.TabIndex = 9;
            this.label15.Text = "Current Position ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(10, 117);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 21);
            this.label28.TabIndex = 37;
            this.label28.Text = "Flight Plan";
            // 
            // LiveryLabel
            // 
            this.LiveryLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LiveryLabel.ForeColor = System.Drawing.Color.Tomato;
            this.LiveryLabel.Location = new System.Drawing.Point(148, 25);
            this.LiveryLabel.Name = "LiveryLabel";
            this.LiveryLabel.Size = new System.Drawing.Size(220, 25);
            this.LiveryLabel.TabIndex = 35;
            this.LiveryLabel.Text = "Citilink";
            this.LiveryLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FlightPlanButton
            // 
            this.FlightPlanButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.FlightPlanButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FlightPlanButton.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.FlightPlanButton.ForeColor = System.Drawing.Color.White;
            this.FlightPlanButton.Location = new System.Drawing.Point(269, 113);
            this.FlightPlanButton.Name = "FlightPlanButton";
            this.FlightPlanButton.Size = new System.Drawing.Size(94, 25);
            this.FlightPlanButton.TabIndex = 32;
            this.FlightPlanButton.Text = "Details";
            this.FlightPlanButton.UseVisualStyleBackColor = false;
            this.FlightPlanButton.Click += new System.EventHandler(this.FlightPlanButton_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(8, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 21);
            this.label24.TabIndex = 7;
            this.label24.Text = "Livery";
            // 
            // SimulationLog
            // 
            this.SimulationLog.Font = new System.Drawing.Font("Arial", 10F);
            this.SimulationLog.Location = new System.Drawing.Point(974, 608);
            this.SimulationLog.Name = "SimulationLog";
            this.SimulationLog.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SimulationLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.SimulationLog.Size = new System.Drawing.Size(379, 129);
            this.SimulationLog.TabIndex = 134;
            this.SimulationLog.Text = "";
            // 
            // PilotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1360, 728);
            this.Controls.Add(this.SimulationLog);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.AircraftInfoGroup);
            this.Controls.Add(this.AircraftInfo);
            this.Controls.Add(this.FullnameLabel);
            this.Controls.Add(this.SceneryLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.footnote);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PCRoleLabel);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.UserPicture);
            this.Controls.Add(this.LineBottom);
            this.Controls.Add(this.TimeSimulationLabel);
            this.Controls.Add(this.LineBlue);
            this.Controls.Add(this.CommandPanel);
            this.Controls.Add(this.AircraftListPanel);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PilotForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PseudoPilotForm";
            this.AircraftInfo.ResumeLayout(false);
            this.AircraftInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftLiveryImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBlue)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.AircraftInfoGroup.ResumeLayout(false);
            this.AircraftInfoGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel AircraftInfo;
        private System.Windows.Forms.Label AircraftLiveryLabel;
        private System.Windows.Forms.Label AircraftModelLabel;
        private System.Windows.Forms.Label AircraftCallsignLabel;
        private System.Windows.Forms.PictureBox AircraftLiveryImage;
        private System.Windows.Forms.Label FullnameLabel;
        private System.Windows.Forms.Label SceneryLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label footnote;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label PCRoleLabel;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox UserPicture;
        private System.Windows.Forms.PictureBox LineBottom;
        private System.Windows.Forms.Label TimeSimulationLabel;
        private System.Windows.Forms.PictureBox LineBlue;
        private System.Windows.Forms.Panel CommandPanel;
        private System.Windows.Forms.Panel AircraftListPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label WindSpeedLabel;
        private System.Windows.Forms.Label WindDirectionLabel;
        private System.Windows.Forms.Label TemperatureLabel;
        private System.Windows.Forms.Label VisibilityLabel;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label WeatherLabel;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox AircraftInfoGroup;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label PositionLabel;
        private System.Windows.Forms.Label EngineLabel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label LiveryLabel;
        private System.Windows.Forms.Button FlightPlanButton;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.RichTextBox SimulationLog;


    }
}
