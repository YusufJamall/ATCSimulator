﻿using System.Linq;
using ATCSimulator.Client.UI.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SimulationService;

namespace ATCSimulator.Client.UI.Simulation.Pilot
{
    public partial class FlightForm : Telerik.WinControls.UI.RadForm
    {
        private AircraftData aircraft;
        private List<FlightRouteListModel> flight_routes;
        private List<string> runways;
        private string scenery_name;
        private int scenery_id;
        private FlightRouteListModel routes;
        public FlightForm()
        {
            InitializeComponent();
        }
        public void LoadData(List<FlightRouteListModel> flight_routes, List<string> runways, string scenery_name, int scenery_id, List<string> ilsRoute)
        {
            this.flight_routes = flight_routes;
            this.scenery_name = scenery_name;
            this.scenery_id = scenery_id;
            this.runways = runways;

            DirectGoILSGroup.Location = new System.Drawing.Point(7, 299);
            DirectGoILSList.Items.Clear();
            foreach (var routeName in ilsRoute)
            {
                DirectGoILSList.Items.Add(routeName);
            }
        }
        public void Init(AircraftData _aircraft)
        {
            this.aircraft = _aircraft;

            if (aircraft.pilot_properties.flight.ILSmode)
            {
                ILSModeBtn.Text = "Exit ILS Mode";
                ILSModeBtn.BackColor = Color.Tomato;
                HoldingRoute.Visible = false;
                FlightRouteGroup.Visible = false;
                DirectGoGroup.Visible = false;
                DirectGoILSGroup.Visible = true;
                DownwindOrbitGroup.Visible = false;
                OtherGroup.Visible = false;
                ChangeRunwayGroup.Visible = false;
                CircuitApproachGroup.Visible = false;
                LandingRunwayGroup.Visible = true;

                HeadingGroup.Location = new System.Drawing.Point(7, 164);
            }
            else
            {
                ILSModeBtn.Text = "Enter ILS Mode";
                ILSModeBtn.BackColor = Color.DodgerBlue;
                HoldingRoute.Visible = true;
                FlightRouteGroup.Visible = true;
                DirectGoGroup.Visible = true;
                DirectGoILSGroup.Visible = false;
                DownwindOrbitGroup.Visible = true;
                OtherGroup.Visible = true;
                ChangeRunwayGroup.Visible = true;
                CircuitApproachGroup.Visible = true;
                LandingRunwayGroup.Visible = false;

                HeadingGroup.Location = new System.Drawing.Point(477, 28);
            }

            #region Holding
            HoldingLeft.Enabled = aircraft.pilot_properties.flight.holding.holding_left_enabled;
            HoldingLeft.BackColor = aircraft.pilot_properties.flight.holding.holding_left ? Color.Tomato : Color.DodgerBlue;
            HoldingRight.Enabled = aircraft.pilot_properties.flight.holding.holding_right_enabled;
            HoldingRight.BackColor = aircraft.pilot_properties.flight.holding.holding_right ? Color.Tomato : Color.DodgerBlue;
            Holding.Enabled = aircraft.pilot_properties.flight.holding.holdingAt_enabled;
            Holding.BackColor = aircraft.pilot_properties.flight.holding.holdingAt ? Color.Tomato : Color.DodgerBlue;
            HoldingList.Items.Clear();
            if (aircraft.pilot_properties.flight.holdingPoint != null)
            {
                foreach (var item in aircraft.pilot_properties.flight.holdingPoint)
                {
                    foreach (var routeName in aircraft.pilot_properties.flight.flight_route.routes_name)
                    {
                        if (item.ToLower().Equals(routeName.ToLower()))
                            HoldingList.Items.Add(item);
                    }
                }
            }
            if (aircraft.pilot_properties.flight.holding.holdingAt)
                HoldingList.SelectedIndex = HoldingList.Items.IndexOf(aircraft.pilot_properties.flight.holding.position_holding);
            else
                HoldingList.SelectedIndex = -1;
            ContinueRoute.Enabled = aircraft.pilot_properties.flight.holding.continue_route_enabled;
            ContinueRoute.BackColor = aircraft.pilot_properties.flight.holding.continue_route ? Color.Tomato : Color.DodgerBlue;
            ContinueDirection.Enabled = aircraft.pilot_properties.flight.holding.continue_direction_enabled;
            ContinueDirection.BackColor = aircraft.pilot_properties.flight.holding.continue_direction ? Color.Tomato : Color.DodgerBlue;

            #endregion

            #region Heading
            HeadingGroup.Enabled = aircraft.pilot_properties.flight.heading.is_enabled;
            HeadingAbs.BackColor = aircraft.pilot_properties.flight.heading.absolute_heading ? Color.Tomato : Color.DodgerBlue;
            HeadingAbsBox.Value = Convert.ToDecimal(aircraft.pilot_properties.flight.heading.absolute_heading_value);
            HeadingRel.BackColor = aircraft.pilot_properties.flight.heading.relative_heading ? Color.Tomato : Color.DodgerBlue;
            HeadingRelBox.Value = Convert.ToDecimal(aircraft.pilot_properties.flight.heading.relative_heading_value);
            #endregion

            #region Flight Route
            FlightRouteGroup.Enabled = aircraft.pilot_properties.flight.flight_route.flight_rules.ToLower().Equals("local") ? false : aircraft.pilot_properties.flight.can_changeRoute;
            FlightRulesList.SelectedIndex = FlightRulesList.Items.IndexOf(aircraft.pilot_properties.flight.flight_route.flight_rules);
            DestinationList.SelectedIndex = DestinationList.Items.IndexOf(aircraft.pilot_properties.flight.flight_route.destination_airport_name);
            string route = aircraft.pilot_properties.flight.flight_route.routes_name.Aggregate((i, j) => i + " " + j);
            FlightRouteList.SelectedIndex = FlightRouteList.Items.IndexOf(route);
            StartPointList.SelectedIndex = StartPointList.Items.IndexOf(aircraft.pilot_properties.flight.flight_route.start_point_name);
            ChangeRunwayList.Items.Clear();
            LandingRunwayList.Items.Clear();
            foreach (var item in runways)
            {
                if (aircraft.properties.type.ToLower().Equals("helicopter"))
                {
                    ChangeRunwayList.Items.Add(item);
                    LandingRunwayList.Items.Add(item);
                }
                else
                {
                    if (!item.Contains("h"))
                    {
                        ChangeRunwayList.Items.Add(item);
                        LandingRunwayList.Items.Add(item);
                    }
                }
            }
            #endregion

            #region Circuit and Approach
            InstrumentApp.Enabled = aircraft.pilot_properties.flight.circuit_approarch.instrument_approach_enabled;
            InstrumentApp.BackColor = aircraft.pilot_properties.flight.circuit_approarch.instrument_approach ? Color.Tomato : Color.DodgerBlue;
            JoinCircuit.Enabled = aircraft.pilot_properties.flight.circuit_approarch.join_circuit_enabled;
            JoinCircuit.BackColor = aircraft.pilot_properties.flight.circuit_approarch.join_circuit ? Color.Tomato : Color.DodgerBlue;
            RunwayList.SelectedIndex = RunwayList.Items.IndexOf(aircraft.pilot_properties.flight.circuit_approarch.runway);
            #endregion

            #region Direct Go
            DirectGoGroup.Enabled = aircraft.pilot_properties.flight.direct_go.is_enabled;
            DirectGo.BackColor = aircraft.pilot_properties.flight.direct_go.directGo ? Color.Tomato : Color.DodgerBlue;
            DirectGoList.SelectedIndex = DirectGoList.Items.IndexOf(aircraft.pilot_properties.flight.direct_go.position);
            DirectGoILSGroup.Enabled = aircraft.pilot_properties.flight.direct_go.is_enabled;
            DirectGoILS.BackColor = aircraft.pilot_properties.flight.direct_go.directGo ? Color.Tomato : Color.DodgerBlue;
            DirectGoILSList.SelectedIndex = DirectGoILSList.Items.IndexOf(aircraft.pilot_properties.flight.direct_go.positionILS);
            #endregion

            #region Performance
            ChangeAltitude.Enabled = aircraft.can_change_altitude;
            ChangeSpeed.Enabled = aircraft.can_change_speed;
            SpeedValue.Maximum = Convert.ToDecimal(aircraft.properties.true_speed);
            SpeedValue.Value = Convert.ToDecimal(aircraft.speed);
            AltitudeValue.Maximum = Convert.ToDecimal(aircraft.properties.maximum_altitude);
            AltitudeValue.Value = Convert.ToDecimal(aircraft.altitude);
            #endregion

            #region Downwind and Orbit
            ExtendDownwind.Enabled = aircraft.pilot_properties.flight.downwind_orbit.downwind_enabled;
            ExtendDownwind.BackColor = aircraft.pilot_properties.flight.downwind_orbit.downwind ? Color.Tomato : Color.DodgerBlue;
            Orbit.Enabled = aircraft.pilot_properties.flight.downwind_orbit.orbit_enabled;
            Orbit.BackColor = aircraft.pilot_properties.flight.downwind_orbit.orbit ? Color.Tomato : Color.DodgerBlue;
            #endregion

            #region Change Runway
            ChangeRunwayGroup.Enabled = aircraft.pilot_properties.flight.change_runway.is_enabled;
            ChangeRunwayList.SelectedIndex = ChangeRunwayList.Items.IndexOf(aircraft.pilot_properties.flight.change_runway.runway);
            #endregion

            #region Other
            MissedApproach.Enabled = aircraft.pilot_properties.flight.other.missed_approach_enabled;
            MissedApproach.BackColor = aircraft.pilot_properties.flight.other.missed_approach ? Color.Tomato : Color.DodgerBlue;
            TouchGo.Enabled = aircraft.pilot_properties.flight.other.touch_go_enabled;
            TouchGo.BackColor = aircraft.pilot_properties.flight.other.touch_go ? Color.Tomato : Color.DodgerBlue;
            Flypass.Enabled = aircraft.pilot_properties.flight.other.flypass_enabled;
            Flypass.BackColor = aircraft.pilot_properties.flight.other.flypass ? Color.Tomato : Color.DodgerBlue;
            RockingWing.Enabled = aircraft.pilot_properties.flight.other.flight_rocking_wing_enabled;
            RockingWing.BackColor = aircraft.pilot_properties.flight.other.flight_rocking_wing ? Color.Tomato : Color.DodgerBlue;
            if (aircraft.pilot_properties.flight.ILSmode)
            {
                ClearToLand.Enabled = aircraft.pilot_properties.flight.other.landing ? false : true;
                LandingRunwayList.SelectedIndex = LandingRunwayList.Items.IndexOf(aircraft.pilot_properties.flight.other.landingRunway);
            }
            else
                ClearToLand.Enabled = aircraft.pilot_properties.flight.other.landing_enabled;
            ClearToLand.BackColor = aircraft.pilot_properties.flight.other.landing ? Color.Tomato : Color.DodgerBlue;
            ReturnBase.Enabled = aircraft.pilot_properties.flight.other.return_base_enabled;
            ReturnBase.Visible = aircraft.pilot_properties.flight.flight_route.flight_rules.ToLower().Equals("local") ? false : aircraft.pilot_properties.flight.other.return_base_isVisible;
            ReturnBase.BackColor = aircraft.pilot_properties.flight.other.return_base ? Color.Tomato : Color.DodgerBlue;
            AbortTakeOff.Enabled = aircraft.pilot_properties.flight.other.abort_takeoff_enabled;
            AbortTakeOff.Visible = aircraft.pilot_properties.flight.other.abort_takeoff_isVisible;
            AbortTakeOff.BackColor = aircraft.pilot_properties.flight.other.abort_takeoff ? Color.Tomato : Color.DodgerBlue;
            #endregion
        }

        #region Flight Route

        private void FlightRulesList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (FlightRulesList.SelectedIndex == -1)
                return;
            DestinationList.SelectedIndex = -1;
            DestinationList.Items.Clear();
            FlightRouteList.SelectedIndex = -1;
            FlightRouteList.Items.Clear();
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            List<FlightRouteListModel> _airports = flight_routes.Where(d => d.is_departure.Equals(aircraft.is_departure) && d.flight_rules.ToLower().Equals(FlightRulesList.Text.ToLower())).GroupBy(d => d.destination_airport_id).Select(d => d.First()).ToList();
            foreach (FlightRouteListModel d in _airports)
            {
                DestinationList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(d.destination_airport_name, d));
            }
        }

        private void DestinationList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if ((FlightRulesList.SelectedIndex == -1) || (DestinationList.SelectedIndex == -1))
                return;
            FlightRouteList.SelectedIndex = -1;
            FlightRouteList.Items.Clear();
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            FlightRouteListModel dest_data = ATCSimulator.Client.Helper.HelperClient.GetDataDropDown<FlightRouteListModel>(DestinationList);
            List<FlightRouteListModel> routes = flight_routes.Where(d => d.is_departure.Equals(aircraft.is_departure) && d.flight_rules_id.Equals(dest_data.flight_rules_id)
                && d.destination_airport_id.Equals(dest_data.destination_airport_id)).ToList();
            foreach (FlightRouteListModel s in routes)
            {
                string route = s.routes_name.Aggregate((i, j) => i + " " + j);
                FlightRouteList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(route, s));
            }
        }

        private void FlightRouteList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (FlightRouteList.SelectedIndex == -1)
                return;
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();
            DirectGoList.SelectedIndex = -1;
            DirectGoList.Items.Clear();
            StartPointList.SelectedIndex = -1;
            StartPointList.Items.Clear();
            routes = ATCSimulator.Client.Helper.HelperClient.GetDataDropDown<FlightRouteListModel>(FlightRouteList);
            foreach (string s in routes.runways)
            {
                if (aircraft.properties.type.ToLower().Equals("helicopter"))
                {
                    RunwayList.Items.Add(s);
                }
                else
                {
                    if (!s.Contains("h"))
                    {
                        RunwayList.Items.Add(s);
                    }
                }

            }
            bool startAdd = false;
            bool endedAdd = false;
            int count = 0;
            foreach (string a in routes.routes_name)
            {
                if (a.Equals(routes.start_point_name))
                    startAdd = true;
                if (a.Equals(routes.end_point_name))
                    count++;
                if (count.Equals(2))
                    endedAdd = true;
                if (startAdd && !endedAdd)
                {
                    DirectGoList.Items.Add(a);
                    StartPointList.Items.Add(a);
                }
                if (count > 0)
                    count++;
            }
        }
        private void StartPointList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (StartPointList.SelectedIndex == -1)
                return;
            routes.start_point_name = StartPointList.Text;
        }

        #endregion

        #region Button Handler

        private void HoldingLeft_Click(object sender, EventArgs e)
        {
            SimulationConnection.HoldingNow(aircraft.callsign, DirectionInfo.Left);
        }

        private void HoldingRight_Click(object sender, EventArgs e)
        {
            SimulationConnection.HoldingNow(aircraft.callsign, DirectionInfo.Right);
        }
        private void ContinueRoute_Click(object sender, EventArgs e)
        {
            SimulationConnection.ContinueRoute(aircraft.callsign);
        }
        private void ContinueDirection_Click(object sender, EventArgs e)
        {
            SimulationConnection.ContinueDirection(aircraft.callsign);
        }

        private void HeadingAbs_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeHeading(aircraft.callsign, HeadingInfo.Absolute, (float)HeadingAbsBox.Value);
        }

        private void HeadingRel_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeHeading(aircraft.callsign, HeadingInfo.Relative, (float)HeadingRelBox.Value);
        }

        private void MissedApproach_Click(object sender, EventArgs e)
        {
            SimulationConnection.MissedApproach(aircraft.callsign);
        }

        private void TouchGo_Click(object sender, EventArgs e)
        {
            SimulationConnection.TouchGo(aircraft.callsign);
        }

        private void Flypass_Click(object sender, EventArgs e)
        {
            SimulationConnection.Flypass(aircraft.callsign);
        }

        private void RockingWing_Click(object sender, EventArgs e)
        {
            SimulationConnection.FlightRockingWing(aircraft.callsign);
        }

        private void ClearToLand_Click(object sender, EventArgs e)
        {
            if (aircraft.pilot_properties.flight.ILSmode)
            {
                if (LandingRunwayList.SelectedIndex == -1)
                    MessageBox.Show("runway cannot be null");
                else
                    SimulationConnection.ILSLanding(aircraft.callsign, LandingRunwayList.Text);
            }
            else
            {
                SimulationConnection.Landing(aircraft.callsign);
            }
        }

        private void ReturnBase_Click(object sender, EventArgs e)
        {
            SimulationConnection.ReturnBase(aircraft.callsign);
        }

        private void AbortTakeOff_Click(object sender, EventArgs e)
        {
            SimulationConnection.AbortedTakeOff(aircraft.callsign);
        }

        private void ChangeAltitude_Click(object sender, EventArgs e)
        {
            SimulationConnection.Altitude(aircraft.callsign, (float)AltitudeValue.Value);
        }

        private void ChangeSpeed_Click(object sender, EventArgs e)
        {
            SimulationConnection.Speed(aircraft.callsign, (float)SpeedValue.Value);
        }

        private void ChangeRoute_Click(object sender, EventArgs e)
        {
            if (StartPointList.SelectedIndex != -1)
                SimulationConnection.ChangeRoute(aircraft.callsign, routes);
            else
                MessageBox.Show("start from cannot be null");

        }

        private void ExtendDownwind_Click(object sender, EventArgs e)
        {
            SimulationConnection.ExtendDownwind(aircraft.callsign);
        }

        private void Orbit_Click(object sender, EventArgs e)
        {
            SimulationConnection.Orbit(aircraft.callsign);
        }

        private void InstrumentApp_Click(object sender, EventArgs e)
        {
            if (RunwayList.SelectedIndex != -1)
                SimulationConnection.Approach(aircraft.callsign, RunwayList.Text);
            else
                MessageBox.Show("runway cannot be null");
        }

        private void JoinCircuit_Click(object sender, EventArgs e)
        {
            if (RunwayList.SelectedIndex != -1)
                SimulationConnection.Circuit(aircraft.callsign, RunwayList.Text);
            else
                MessageBox.Show("runway cannot be null");

        }

        private void ChangeRunway_Click(object sender, EventArgs e)
        {
            if (ChangeRunwayList.SelectedIndex != -1)
                SimulationConnection.ChangeRunway(aircraft.callsign, ChangeRunwayList.Text);
            else
                MessageBox.Show("runway cannot be null");

        }
        private void DirectGo_Click(object sender, EventArgs e)
        {
            if (DirectGoList.SelectedIndex != -1)
                SimulationConnection.DirectGo(aircraft.callsign, DirectGoList.Text, false);
            else
                MessageBox.Show("target cannot be null");
        }
        private void DirectGoILS_Click(object sender, EventArgs e)
        {
            if (DirectGoILSList.SelectedIndex != -1)
                SimulationConnection.DirectGo(aircraft.callsign, DirectGoILSList.Text, true);
            else
                MessageBox.Show("target cannot be null");
        }
        private void Holding_Click(object sender, EventArgs e)
        {
            if (HoldingList.SelectedIndex != -1)
                SimulationConnection.HoldingPosition(aircraft.callsign, HoldingList.Text);
            else
                MessageBox.Show("position cannot be null");
        }

        private void ILSModeBtn_Click(object sender, EventArgs e)
        {
            SimulationConnection.ChangeILSMode(aircraft.callsign, !aircraft.pilot_properties.flight.ILSmode);
        }



        #endregion



    }
}
