﻿using System.Linq;
using ATCSimulator.Client.UI.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SimulationService;

namespace ATCSimulator.Client.UI.Simulation.Pilot
{
    public partial class GroundForm : Telerik.WinControls.UI.RadForm
    {
        private AircraftData aircraft;
        private List<FlightRouteListModel> flight_routes;
        private List<DataAppronModel> approns;
        private string scenery_name;
        private int scenery_id;
        private List<string> taxi_routes;
        private bool finishInit;

        public GroundForm()
        {
            InitializeComponent();
        }
        public void LoadData(List<FlightRouteListModel> flight_routes, List<DataAppronModel> approns, string scenery_name, int scenery_id)
        {
            this.flight_routes = flight_routes;
            this.approns = approns;
            this.scenery_name = scenery_name;
            this.scenery_id = scenery_id;
        }

        public void Init(AircraftData _aircraft)
        {
            finishInit = false;
            this.aircraft = _aircraft;
            if (aircraft.properties.type.ToLower().Equals("helicopter"))
            {
                PushbackGroup.Visible = false;
                UTurnLeft.Visible = false;
                UTurnRight.Visible = false;
            }
            else
            {
                PushbackGroup.Visible = true;
                UTurnLeft.Visible = true;
                UTurnRight.Visible = true;
            }
            this.taxi_routes = _aircraft.pilot_properties.ground.taxi.routes;
            ResetForm();
            ResetProperties();

            Engine.Enabled = aircraft.pilot_properties.ground.engine_enabled;
            Engine.Checked = aircraft.pilot_properties.ground.engine;

            #region Pushback
            PushbackGroup.Enabled = aircraft.pilot_properties.ground.pushback.is_show;
            PushbackLeft.Enabled = aircraft.pilot_properties.ground.pushback.pushback_left_enabled;
            PushbackRight.Enabled = aircraft.pilot_properties.ground.pushback.pushback_right_enabled;
            PushbackLeft.Checked = aircraft.pilot_properties.ground.pushback.pushback_left;
            PushbackRight.Checked = aircraft.pilot_properties.ground.pushback.pushback_right;

            if (aircraft.pilot_properties.ground.pushback.status.Equals(CommandStatus.Process))
                PushbackStatus.Text = "Process Pushback Please Wait";
            else if (aircraft.pilot_properties.ground.pushback.status.Equals(CommandStatus.Finish))
                PushbackStatus.Text = "Finish Pushback";
            else
                PushbackStatus.Text = "-";

            PBLeftAlias.Text = aircraft.pilot_properties.ground.pushback.pushback_left_alias;
            PBRightAlias.Text = aircraft.pilot_properties.ground.pushback.pushback_right_alias;
            #endregion

            #region Flight Route
            FlightRulesList.Items.Where(d => d.Text.ToLower().Equals("local")).FirstOrDefault().Enabled = false;
            FlightRulesList.SelectedIndex = -1;

            FlightRulesList.SelectedIndex = FlightRulesList.Items.IndexOf(aircraft.pilot_properties.flight.flight_route.flight_rules);
            DestinationList.SelectedIndex = DestinationList.Items.IndexOf(aircraft.pilot_properties.flight.flight_route.destination_airport_name);
            string route = aircraft.pilot_properties.flight.flight_route.routes_name.Aggregate((i, j) => i + " " + j);
            FlightRouteList.SelectedIndex = FlightRouteList.Items.IndexOf(route);
            RunwayList.SelectedIndex = RunwayList.Items.IndexOf(aircraft.pilot_properties.ground.runway.runway);
            if (FlightRulesList.SelectedItem.Text.ToLower().Equals("local"))
            {
                FlightRulesList.Enabled = false;
                DestinationList.Enabled = false;
                FlightRouteList.Enabled = false;

            }
            else
            {
                FlightRulesList.Enabled = true;
                DestinationList.Enabled = true;
                FlightRouteList.Enabled = true;
            }
            FlightRouteGroup.Enabled = aircraft.pilot_properties.ground.runway.is_show;
            ParkingGroup.Enabled = aircraft.pilot_properties.ground.runway.is_show;
            GroundStatusBtn.Enabled = aircraft.pilot_properties.ground.runway.is_show;
            #endregion

            #region Aappron Parking
            TerminalList.Items.Clear();
            BayList.Items.Clear();
            foreach (var parking in approns.Where(d => d.scenery_id.Equals(scenery_id)).GroupBy(d => d.terminal_name).Select(d => d.First()).ToList())
            {
                TerminalList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(parking.terminal_name, parking.terminal_name));
            }
            TerminalList.SelectedIndex = TerminalList.Items.IndexOf(aircraft.pilot_properties.ground.parking.terminal_name);
            BayList.SelectedIndex = BayList.Items.IndexOf(aircraft.pilot_properties.ground.parking.appron_name);

            #endregion

            #region Taxi
            TaxiGroup.Enabled = aircraft.pilot_properties.ground.taxi.is_show;
            TaxiRoute.BackColor = aircraft.pilot_properties.ground.taxi.taxi_route == true ? System.Drawing.Color.DarkGreen : System.Drawing.Color.DodgerBlue;
            TaxiRoute.Enabled = aircraft.pilot_properties.ground.taxi.taxi_route_enabled;
            StartTaxi.Enabled = !aircraft.pilot_properties.ground.taxi.cross_runway;
            CrossRW.Enabled = aircraft.pilot_properties.ground.taxi.cross_runway;



            int taxi_speed_index = -1;
            if (aircraft.pilot_properties.ground.taxi.speed == GroundSpeed.Expedite)
                taxi_speed_index = 2;
            else if (aircraft.pilot_properties.ground.taxi.speed == GroundSpeed.Normal)
                taxi_speed_index = 1;
            else
                taxi_speed_index = 0;

            TaxiSpeedList.SelectedIndex = taxi_speed_index;

            if (aircraft.pilot_properties.ground.taxi.status == TaxiStatus.None || aircraft.pilot_properties.ground.taxi.status == TaxiStatus.Finish)
            {
                StartTaxi.Enabled = false;
                StartTaxi.Text = "Start Taxi";
                StartTaxi.BackColor = System.Drawing.Color.Tomato;
                TaxiRoute.BackColor = Color.DodgerBlue;
                UTurnLeft.Enabled = true;
                UTurnRight.Enabled = true;
                OneEighty.Enabled = true;
                SpeedTaxiGroup.Enabled = false;
            }
            else if (aircraft.pilot_properties.ground.taxi.status == TaxiStatus.Process || aircraft.pilot_properties.ground.taxi.status == TaxiStatus.Holding)
            {
                StartTaxi.Enabled = true;

                if (aircraft.pilot_properties.ground.taxi.status == TaxiStatus.Process)
                {
                    StartTaxi.Text = "Holding Taxi";
                    UTurnLeft.Enabled = false;
                    UTurnRight.Enabled = false;
                    OneEighty.Enabled = false;
                    SpeedTaxiGroup.Enabled = true;
                }
                else
                {
                    StartTaxi.Text = "Continue Taxi";
                    UTurnLeft.Enabled = true;
                    UTurnRight.Enabled = true;
                    OneEighty.Enabled = true;
                    SpeedTaxiGroup.Enabled = false;
                }

                StartTaxi.BackColor = System.Drawing.Color.DarkGreen;
                TaxiRoute.BackColor = Color.DarkGreen;
            }
            #endregion

            #region Other
            UTurnLeft.Enabled = UTurnRight.Enabled = OneEighty.Enabled = aircraft.pilot_properties.ground.other.u_turn;
            UTurnLeft.BackColor = UTurnRight.BackColor = OneEighty.BackColor = System.Drawing.Color.DodgerBlue;
            if (!aircraft.pilot_properties.ground.other.u_turn) // process 
            {
                if (aircraft.pilot_properties.ground.other.u_turn_status == UturnInfo.Turn180)
                    OneEighty.BackColor = System.Drawing.Color.DarkGreen;
                else if (aircraft.pilot_properties.ground.other.u_turn_status == UturnInfo.UTurnLeft)
                    UTurnLeft.BackColor = System.Drawing.Color.DarkGreen;
                else if (aircraft.pilot_properties.ground.other.u_turn_status == UturnInfo.UturnRight)
                    UTurnRight.BackColor = System.Drawing.Color.DarkGreen;
            }
            RockingWing.Checked = aircraft.pilot_properties.ground.other.rocking_wing;
            #endregion

            #region intersection

            TakeOffGroup.Enabled = aircraft.pilot_properties.ground.intersection.is_show;
            RunwayHold.Enabled = aircraft.pilot_properties.ground.intersection.runway_hold;
            TakeOff.Enabled = aircraft.pilot_properties.ground.intersection.take_off;
            if (aircraft.pilot_properties.ground.intersection.status == CommandStatus.None || aircraft.pilot_properties.ground.intersection.status == CommandStatus.Finish)
            {
                AbortedTakeOff.Enabled = false;
                TakeOff.Text = "Take Off";
                TakeOff.BackColor = System.Drawing.Color.DodgerBlue;
            }
            else
            {
                AbortedTakeOff.Enabled = true;
                TakeOff.Text = "Taking Off";
                TakeOff.BackColor = System.Drawing.Color.DarkGreen;
            }

            #endregion

            #region parking or departure

            if (aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
            {
                GroundStatusLbl.Text = "Departure";
                GroundStatusBtn.Text = "Return to Aappron ";
                ParkingGroup.Enabled = false;
            }
            else
            {
                GroundStatusLbl.Text = "Parking";
                GroundStatusBtn.Text = "Go Departure ";
                FlightRouteGroup.Enabled = false;
            }

            #endregion

            finishInit = true;
        }

        #region Button Handler

        #region Engine and Ground Status

        private void Engine_Click(object sender, EventArgs e)
        {
            SimulationConnection.StartEnigne(aircraft.callsign, Engine.Checked);
        }

        private void GroundStatusBtn_Click(object sender, EventArgs e)
        {
            if (aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
                SimulationConnection.ChangeGroundStatus(aircraft.callsign, (int)GroundStatusInfo.Parking);
            else
                SimulationConnection.ChangeGroundStatus(aircraft.callsign, (int)GroundStatusInfo.Departure);
        }

        #endregion

        #region Flight Route And Aapprons

        private void FlightRulesList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (FlightRulesList.SelectedIndex == -1)
                return;
            DestinationList.SelectedIndex = -1;
            DestinationList.Items.Clear();
            FlightRouteList.SelectedIndex = -1;
            FlightRouteList.Items.Clear();
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            List<FlightRouteListModel> _airports = flight_routes.Where(d => d.is_departure.Equals(aircraft.is_departure) && d.flight_rules.ToLower().Equals(FlightRulesList.Text.ToLower())).GroupBy(d => d.destination_airport_id).Select(d => d.First()).ToList();
            foreach (FlightRouteListModel d in _airports)
            {
                DestinationList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(d.destination_airport_name, d));
            }
        }

        private void DestinationList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if ((FlightRulesList.SelectedIndex == -1) || (DestinationList.SelectedIndex == -1))
                return;
            FlightRouteList.SelectedIndex = -1;
            FlightRouteList.Items.Clear();
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            FlightRouteListModel dest_data = ATCSimulator.Client.Helper.HelperClient.GetDataDropDown<FlightRouteListModel>(DestinationList);
            List<FlightRouteListModel> routes = flight_routes.Where(d => d.is_departure.Equals(aircraft.is_departure) && d.flight_rules_id.Equals(dest_data.flight_rules_id)
                && d.destination_airport_id.Equals(dest_data.destination_airport_id)).ToList();
            foreach (FlightRouteListModel s in routes)
            {
                string route = s.routes_name.Aggregate((i, j) => i + " " + j);
                FlightRouteList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(route, s));
            }
        }

        private void FlightRouteList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (FlightRouteList.SelectedIndex == -1)
                return;
            RunwayList.SelectedIndex = -1;
            RunwayList.Items.Clear();

            FlightRouteListModel routes = ATCSimulator.Client.Helper.HelperClient.GetDataDropDown<FlightRouteListModel>(FlightRouteList);

            foreach (string s in routes.runways)
            {
                if (aircraft.properties.type.ToLower().Equals("helicopter"))
                {
                    RunwayList.Items.Add(s);
                }
                else
                {
                    if (!s.ToLower().Contains("h"))
                    {
                        RunwayList.Items.Add(s);
                    }
                }

            }



        }

        private void RunwayList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (RunwayList.SelectedIndex == -1 || FlightRouteList.SelectedIndex == -1)
                return;
            if (finishInit)
            {
                FlightRouteListModel routes = ATCSimulator.Client.Helper.HelperClient.GetDataDropDown<FlightRouteListModel>(FlightRouteList);
                SimulationConnection.ChangeFlightRoute(aircraft.callsign, routes, RunwayList.Items[RunwayList.SelectedIndex].Text);
            }
        }

        private void TerminalList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (TerminalList.SelectedIndex == -1)
                return;
            BayList.SelectedIndex = -1;
            BayList.Items.Clear();
            foreach (var terminal in approns.Where(d => d.terminal_name.Equals(TerminalList.SelectedValue)).ToList())
            {
                BayList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(terminal.appron_name, terminal));
            }
        }

        private void BayList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (BayList.SelectedIndex == -1 || TerminalList.SelectedIndex == -1)
                return;
            DataAppronModel terminal = ATCSimulator.Client.Helper.HelperClient.GetDataDropDown<DataAppronModel>(BayList);
            if (aircraft.pilot_properties.ground.parking.parking_id != terminal.id)
                SimulationConnection.ChangeParkingAppron(aircraft.callsign, terminal);
        }

        #endregion

        #region Taxi Action

        private void TaxiRoute_Click(object sender, EventArgs e)
        {
            if (aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
            {
                if (RunwayList.SelectedIndex == -1 || !Engine.Checked)
                {
                    MessageBox.Show("Runway must defined or Engine must On");
                    return;
                }
            }
            else
            {
                if (BayList.SelectedIndex == -1 || !Engine.Checked)
                {
                    MessageBox.Show("Parking Bay must defined or Engine must On");
                    return;
                }
            }

            //get heading from unity
            TaxiForm taxi_panel = new TaxiForm(this);
            taxi_panel.Init(scenery_name);
            bool is_helicopter = aircraft.properties.type.ToLower().Equals("helicopter") ? true : false;

            if (aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
                taxi_panel.TaxiToRunway(aircraft.current_position.ToUpper(), aircraft.from_position.ToUpper(), aircraft.heading, "RW" + RunwayList.Items[RunwayList.SelectedIndex].Text, is_helicopter);
            else
                taxi_panel.TaxiToParking(aircraft.current_position.ToUpper(), aircraft.from_position.ToUpper(), aircraft.heading, BayList.Items[BayList.SelectedIndex].Text, is_helicopter);

            taxi_panel.StartPosition = FormStartPosition.CenterParent;
            taxi_panel.ShowDialog();
        }
        public void SetTaxiRoute(List<string> taxi_routes)
        {
            TaxiRoute.BackColor = System.Drawing.Color.DarkGreen;
            StartTaxi.Enabled = true;
            StartTaxi.BackColor = System.Drawing.Color.Tomato;
            StartTaxi.Text = "Start Taxi";
            this.taxi_routes = taxi_routes;
            aircraft.pilot_properties.ground.taxi.status = TaxiStatus.None;

        }

        private void StartTaxi_Click(object sender, EventArgs e)
        {
            if (TaxiSpeedList.SelectedIndex == -1 || taxi_routes == null)
            {
                MessageBox.Show("Taxi Speed and route must defined");
                return;
            }
            if (aircraft.pilot_properties.ground.taxi.status == TaxiStatus.None || aircraft.pilot_properties.ground.taxi.status == TaxiStatus.Finish)
            {
                string target = "";
                if (aircraft.pilot_properties.ground.status.Equals(GroundStatusInfo.Departure))
                {
                    if (!string.IsNullOrWhiteSpace(RunwayList.Text))
                        target = RunwayList.Text;
                    else
                    {
                        MessageBox.Show("Runway cannot empty");
                        return;
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(BayList.Text))
                        target = BayList.Text;
                    else
                    {
                        MessageBox.Show("Parking Bay cannot empty");
                        return;
                    }
                }
                if (!string.IsNullOrWhiteSpace(target))
                    SimulationConnection.StartTaxi(aircraft.callsign, taxi_routes, target, (int)aircraft.pilot_properties.ground.status);
            }
            else if (aircraft.pilot_properties.ground.taxi.status == TaxiStatus.Process)
            {
                SimulationConnection.HoldTaxi(aircraft.callsign);

            }
            else if (aircraft.pilot_properties.ground.taxi.status == TaxiStatus.Holding)
            {
                SimulationConnection.ContinueTaxi(aircraft.callsign);
            }
        }

        private void ChangeSpeedTaxi_Click(object sender, EventArgs e)
        {
            if (TaxiSpeedList.SelectedIndex == -1)
                return;

            GroundSpeed speed = GroundSpeed.Normal;
            if (TaxiSpeedList.SelectedIndex == 0)
                speed = GroundSpeed.Slow;
            else if (TaxiSpeedList.SelectedIndex == 2)
                speed = GroundSpeed.Expedite;

            SimulationConnection.ChangeSpeedTaxi(aircraft.callsign, speed);
        }

        private void CrossRW_Click(object sender, EventArgs e)
        {
            SimulationConnection.CrossRunway(aircraft.callsign);
        }

        #endregion

        #region Pushback

        private void PushbackLeft_Click(object sender, EventArgs e)
        {
            SimulationConnection.Pushback(aircraft.callsign, DirectionInfo.Left);
        }

        private void PushbackRight_Click(object sender, EventArgs e)
        {
            SimulationConnection.Pushback(aircraft.callsign, DirectionInfo.Right);
        }

        #endregion

        #region UTurn And Rocking Wing

        private void UTurnRight_Click(object sender, EventArgs e)
        {
            SimulationConnection.Uturn(aircraft.callsign, UturnInfo.UturnRight);
        }

        private void UTurnLeft_Click(object sender, EventArgs e)
        {
            SimulationConnection.Uturn(aircraft.callsign, UturnInfo.UTurnLeft);
        }

        private void RockingWing_Click(object sender, EventArgs e)
        {
            SimulationConnection.RockingWing(aircraft.callsign, RockingWing.Checked);
        }

        private void OneEighty_Click(object sender, EventArgs e)
        {
            SimulationConnection.Uturn(aircraft.callsign, UturnInfo.Turn180);
        }

        #endregion

        #region Intersection Action

        private void TakeOff_Click(object sender, EventArgs e)
        {
            if (!Engine.Checked)
                MessageBox.Show("Engine Must On ");
            else
            {
                VisualFlightRouteModel _routes = new VisualFlightRouteModel
                {
                    destination = aircraft.pilot_properties.flight.flight_route.destination_airport_name,
                    origin = aircraft.pilot_properties.flight.flight_route.origin_airport_name,
                    routes = aircraft.pilot_properties.flight.flight_route.routes_name,
                    flightRules = (FlightRuleInfo)Enum.Parse(typeof(FlightRuleInfo), aircraft.pilot_properties.flight.flight_route.flight_rules)
                };
                SimulationConnection.TakeOff(aircraft.callsign, _routes, RunwayList.Items[RunwayList.SelectedIndex].Text);
            }

        }

        private void RunwayHold_Click(object sender, EventArgs e)
        {
            VisualFlightRouteModel _routes = new VisualFlightRouteModel
            {
                destination = aircraft.pilot_properties.flight.flight_route.destination_airport_name,
                origin = aircraft.pilot_properties.flight.flight_route.origin_airport_name,
                routes = aircraft.pilot_properties.flight.flight_route.routes_name,
                flightRules = (FlightRuleInfo)Enum.Parse(typeof(FlightRuleInfo), aircraft.pilot_properties.flight.flight_route.flight_rules)
            };
            SimulationConnection.RunwayHold(aircraft.callsign, _routes, RunwayList.Items[RunwayList.SelectedIndex].Text);
        }

        private void AbortedTakeOff_Click(object sender, EventArgs e)
        {
            //SimulationConnection.t
        }

        #endregion

        #endregion

        #region Helper

        public void ResetForm()
        {

            FlightRouteGroup.Enabled = false;
            ParkingGroup.Enabled = false;
            GroundStatusBtn.Enabled = false;
            PushbackGroup.Enabled = false;
            TaxiGroup.Enabled = false;

        }
        public void ResetProperties()
        {
            Engine.Checked = false;

            PushbackStatus.Text = "-";
            PBLeftAlias.Text = "";
            PBRightAlias.Text = "";
            GroundStatusLbl.Text = "";


            UTurnLeft.Enabled = UTurnRight.Enabled = OneEighty.Enabled = false;
        }

        #endregion

    }
}
