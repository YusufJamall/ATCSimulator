﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.UI.Other;

namespace ATCSimulator.Client.UI.Simulation
{
    public partial class SimulationMainForm : Form
    {
        public InfoForm infoForm;
        private PausedForm paused_form;
        private Instructor.InstructorForm _instructor_form;
        private Pilot.PilotForm pilot_form;
        private Controller.ControllerForm controller_form;
        private string _pc_role;
        public static SimulationMainForm Instance { get; private set; }

        public SimulationMainForm(InfoForm infoForm, string pc_role)
        {
            InitializeComponent();
            this._pc_role = pc_role.ToLower();
            Instance = this;
            this.infoForm = infoForm;
            LoadForm();
            this.Location = new Point { X = Convert.ToInt32(ConfigurationManager.AppSettings["X_Position"].ToString()), Y = 0 };
        }
        private void LoadForm()
        {
            if (_pc_role.Contains("pseudopilot"))
            {
                pilot_form = new Pilot.PilotForm();
                pilot_form.TopLevel = false;
                SimulationPanel.Controls.Add(pilot_form);
            }
            else if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                _instructor_form = new Instructor.InstructorForm();
                _instructor_form.TopLevel = false;
                SimulationPanel.Controls.Add(_instructor_form);
            }

            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                controller_form = new Controller.ControllerForm();
                controller_form.TopLevel = false;
                SimulationPanel.Controls.Add(controller_form);
            }
        }
        public void Init(ExerciseSimulationModel exercise, List<AircraftData> aircrafts, List<FlightRouteListModel> flight_routes, List<DataAppronModel> aapprons, string pc_role, Image image, string name)
        {
            List<string> ilsroute = new List<string>();
            foreach (var item1 in flight_routes.Where(d => !d.is_departure).ToList())
            {
                bool startAdd = false;
                bool endedAdd = false;
                int count = 0;
                foreach (var item2 in item1.routes_name)
                {
                    if (item2.Equals(item1.start_point_name))
                        startAdd = true;
                    if (item2.Equals(item1.end_point_name))
                        count++;
                    if (count.Equals(2))
                        endedAdd = true;
                    if (startAdd && !endedAdd)
                    {
                        ilsroute.Add(item2);
                    }
                    if (count > 0)
                        count++;
                }
            }
            ilsroute = ilsroute.Distinct().ToList();
            ilsroute.Remove("LOCAL");
            if (_pc_role.Contains("pseudopilot"))
            {
                pilot_form.Init(exercise, aircrafts, flight_routes, aapprons, pc_role, image, name, ilsroute);
            }
            else if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                _instructor_form.Init(exercise, aircrafts, flight_routes, aapprons, pc_role, image, name, ilsroute);
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                controller_form.Init(exercise, aircrafts, flight_routes, aapprons, pc_role, image, name);
            }
        }

        #region Set Lock Screen
        public static void SetLockScreen(string command, bool value)
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    if (value)
                    {
                        Instance.Enabled = false;
                        Instance.paused_form = new PausedForm();
                        if (command.Equals("pause"))
                        {
                            Instance.paused_form.SetMessage("Simulation is Paused");
                            if (Instance.controller_form != null)
                            {
                                Instance.controller_form.PauseSimulationCallback(true);
                            }
                            else
                            {
                                Instance.pilot_form.PauseSimulationCallback(true);
                            }
                        }
                        else
                            Instance.paused_form.SetMessage("Waiting Visual Ready");
                        Instance.paused_form.StartPosition = FormStartPosition.CenterParent;
                        Instance.paused_form.Show();
                        Instance.Enabled = false;

                    }
                    else
                    {

                        Instance.Enabled = true;
                        if (Instance.paused_form != null)
                            Instance.paused_form.Hide();
                        if (command.Equals("pause"))
                        {
                            if (Instance.controller_form != null)
                            {
                                Instance.controller_form.PauseSimulationCallback(false);
                            }
                            else
                            {
                                Instance.pilot_form.PauseSimulationCallback(false);
                            }
                        }
                    }
                }));
            }
            else
            {
                if (value)
                {
                    Instance.Enabled = false;
                    Instance.paused_form = new PausedForm();
                    if (command.Equals("pause"))
                    {
                        Instance.paused_form.SetMessage("Simulation is Paused");
                        if (Instance.controller_form != null)
                        {
                            Instance.controller_form.PauseSimulationCallback(true);
                        }
                        else
                        {
                            Instance.pilot_form.PauseSimulationCallback(true);
                        }
                    }
                    else
                        Instance.paused_form.SetMessage("Waiting Visual Ready");
                    Instance.paused_form.StartPosition = FormStartPosition.CenterScreen;
                    Instance.paused_form.Show();
                }
                else
                {

                    Instance.Enabled = true;
                    if (Instance.paused_form != null)
                        Instance.paused_form.Hide();
                    if (Instance.controller_form != null)
                    {
                        Instance.controller_form.PauseSimulationCallback(false);
                    }
                    else
                    {
                        Instance.pilot_form.PauseSimulationCallback(false);
                    }
                }
            }

        }
        #endregion

        #region Play Simulation
        public void PlaySimulation()
        {
            if (_pc_role.Contains("pseudopilot"))
            {
                pilot_form.Show();
                pilot_form.Dock = DockStyle.Top;
                pilot_form.BringToFront();
            }
            else if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                _instructor_form.Show();
                _instructor_form.Dock = DockStyle.Top;
                _instructor_form.BringToFront();
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                controller_form.Show();
                controller_form.Dock = DockStyle.Top;
                controller_form.BringToFront();
            }
            // Make this Client ready to get callback
            SimulationConnection.isReady = true;

            // waiting visual to be ready
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    SetLockScreen("wait", true);
                }));
            }
            else
            {
                SetLockScreen("wait", true);
            }
        }
        #endregion

        #region Continue Simulation
        public void ContinueSimulation(bool isPaused)
        {
            if (_pc_role.Contains("pseudopilot"))
            {
                pilot_form.Show();
                pilot_form.Dock = DockStyle.Top;
                pilot_form.BringToFront();
                // visual is ready , close waiting dialog
                SetLockScreen("pause", isPaused);
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ContinueSimulationCallback();
                    }));
                }
                else
                {
                    pilot_form.ContinueSimulationCallback();
                }
            }
            else if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                _instructor_form.Show();
                _instructor_form.Dock = DockStyle.Top;
                _instructor_form.BringToFront();
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ContinueSimulationCallback(isPaused);
                    }));
                }
                else
                {
                    _instructor_form.ContinueSimulationCallback(isPaused);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                controller_form.Show();
                controller_form.Dock = DockStyle.Top;
                controller_form.BringToFront();
                SetLockScreen("pause", isPaused);
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ContinueSimulationCallback();
                    }));
                }
                else
                {
                    controller_form.ContinueSimulationCallback();
                }
            }
        }
        #endregion

        #region ButtonCallback

        #region Other Callback

        #region Remove Aircraft

        public void RemoveAircraft(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.RemoveAircraft(callsign);
                    }));
                }
                else
                {
                    _instructor_form.RemoveAircraft(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.RemoveAircraft(callsign);
                    }));
                }
                else
                {
                    pilot_form.RemoveAircraft(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.RemoveAircraft(callsign);
                    }));
                }
                else
                {
                    controller_form.RemoveAircraft(callsign);
                }
            }
        }

        #endregion

        #region Incident Animal

        public void IncidentAnimal(bool value, string runway)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.IncidentAnimal(value, runway);
                    }));
                }
                else
                {
                    _instructor_form.IncidentAnimal(value, runway);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.IncidentAnimal(value, runway);
                    }));
                }
                else
                {
                    pilot_form.IncidentAnimal(value, runway);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.IncidentAnimal(value, runway);
                    }));
                }
                else
                {
                    controller_form.IncidentAnimal(value, runway);
                }
            }
        }

        #endregion

        #region PKPPK

        public void PKPPK(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.PKPPK(callsign);
                    }));
                }
                else
                {
                    _instructor_form.PKPPK(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.PKPPK(callsign);
                    }));
                }
                else
                {
                    pilot_form.PKPPK(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.PKPPK(callsign);
                    }));
                }
                else
                {
                    controller_form.PKPPK(callsign);
                }
            }
        }

        #endregion

        #region Bird Attack

        public void BirdAttack(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.BirdAttack(callsign);
                    }));
                }
                else
                {
                    _instructor_form.BirdAttack(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.BirdAttack(callsign);
                    }));
                }
                else
                {
                    pilot_form.BirdAttack(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.BirdAttack(callsign);
                    }));
                }
                else
                {
                    controller_form.BirdAttack(callsign);
                }
            }
        }

        #endregion

        #region Bird Attack Value

        public void BirdAttackValue(string callsign, bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.BirdAttackValue(callsign, value);
                    }));
                }
                else
                {
                    _instructor_form.BirdAttackValue(callsign, value);
                }
            }

        }

        #endregion

        #region Landing Gear Jam Value

        public void LandingGearJamValue(string callsign, bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.LandingGearJamValue(callsign, value);
                    }));
                }
                else
                {
                    _instructor_form.LandingGearJamValue(callsign, value);
                }
            }

        }

        #endregion

        #region LandingGearJam

        public void LandingGearJam(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.LandingGearJam(callsign);
                    }));
                }
                else
                {
                    _instructor_form.LandingGearJam(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.LandingGearJam(callsign);
                    }));
                }
                else
                {
                    pilot_form.LandingGearJam(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.LandingGearJam(callsign);
                    }));
                }
                else
                {
                    controller_form.LandingGearJam(callsign);
                }
            }
        }

        #endregion

        #region Engine Failure

        public void EngineFailureCallback(string callsign, int engine)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.EngineFailureCallback(callsign, engine);
                    }));
                }
                else
                {
                    _instructor_form.EngineFailureCallback(callsign, engine);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.EngineFailureCallback(callsign, engine);
                    }));
                }
                else
                {
                    pilot_form.EngineFailureCallback(callsign, engine);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.EngineFailureCallback(callsign, engine);
                    }));
                }
                else
                {
                    controller_form.EngineFailureCallback(callsign, engine);
                }
            }
        }

        #endregion

        #region Change Fuel Callback

        public void ChangeFuelCallback(string callsign, TimeSpan fuel)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeFuelCallback(callsign, fuel);
                    }));
                }
                else
                {
                    _instructor_form.ChangeFuelCallback(callsign, fuel);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeFuelCallback(callsign, fuel);
                    }));
                }
                else
                {
                    pilot_form.ChangeFuelCallback(callsign, fuel);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeFuelCallback(callsign, fuel);
                    }));
                }
                else
                {
                    controller_form.ChangeFuelCallback(callsign, fuel);
                }
            }
        }

        #endregion

        #region Environment Callback

        #region Change Weather Callback

        public void ChangeWeatherCallback(WeatherInfo weather)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeWeatherCallback(weather);
                    }));
                }
                else
                {
                    _instructor_form.ChangeWeatherCallback(weather);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeWeatherCallback(weather);
                    }));
                }
                else
                {
                    pilot_form.ChangeWeatherCallback(weather);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeWeatherCallback(weather);
                    }));
                }
                else
                {
                    controller_form.ChangeWeatherCallback(weather);
                }
            }
        }

        #endregion

        #region Change Wind Direction Callback

        public void ChangeWindDirectionCallback(float value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeWindDirection(value);
                    }));
                }
                else
                {
                    _instructor_form.ChangeWindDirection(value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeWindDirection(value);
                    }));
                }
                else
                {
                    pilot_form.ChangeWindDirection(value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeWindDirection(value);
                    }));
                }
                else
                {
                    controller_form.ChangeWindDirection(value);
                }
            }
        }

        #endregion

        #region Change Wind Speed Callback

        public void ChangeWindSpeedCallback(float value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeWindSpeed(value);
                    }));
                }
                else
                {
                    _instructor_form.ChangeWindSpeed(value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeWindSpeed(value);
                    }));
                }
                else
                {
                    pilot_form.ChangeWindSpeed(value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeWindSpeed(value);
                    }));
                }
                else
                {
                    controller_form.ChangeWindSpeed(value);
                }
            }
        }

        #endregion

        #region Change Temperature Callback

        public void ChangeTemperatureCallback(float value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeTemperature(value);
                    }));
                }
                else
                {
                    _instructor_form.ChangeTemperature(value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeTemperature(value);
                    }));
                }
                else
                {
                    pilot_form.ChangeTemperature(value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeTemperature(value);
                    }));
                }
                else
                {
                    controller_form.ChangeTemperature(value);
                }
            }
        }

        #endregion

        #region Change Visibility Callback

        public void ChangeVisibilityCallback(float value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeVisibility(value);
                    }));
                }
                else
                {
                    _instructor_form.ChangeVisibility(value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeVisibility(value);
                    }));
                }
                else
                {
                    pilot_form.ChangeVisibility(value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeVisibility(value);
                    }));
                }
                else
                {
                    controller_form.ChangeVisibility(value);
                }
            }
        }

        #endregion

        #region Change Time Simulation Callback

        public void ChangeTimeSimulationCallback(TimeSpan time)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeTimeSimulation(time);
                    }));
                }
                else
                {
                    _instructor_form.ChangeTimeSimulation(time);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeTimeSimulation(time);
                    }));
                }
                else
                {
                    pilot_form.ChangeTimeSimulation(time);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeTimeSimulation(time);
                    }));
                }
                else
                {
                    controller_form.ChangeTimeSimulation(time);
                }
            }
        }

        #endregion

        #endregion

        #region Light Callback

        #region Taxi Light Callback

        public void TaxiLightCallback(bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.TaxiLightCallback(value);
                    }));
                }
                else
                {
                    _instructor_form.TaxiLightCallback(value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.TaxiLightCallback(value);
                    }));
                }
                else
                {
                    pilot_form.TaxiLightCallback(value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.TaxiLightCallback(value);
                    }));
                }
                else
                {
                    controller_form.TaxiLightCallback(value);
                }
            }
        }

        #endregion

        #region Papi Light Callback

        public void PapiLightCallback(bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.PapiLightCallback(value);
                    }));
                }
                else
                {
                    _instructor_form.PapiLightCallback(value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.PapiLightCallback(value);
                    }));
                }
                else
                {
                    pilot_form.PapiLightCallback(value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.PapiLightCallback(value);
                    }));
                }
                else
                {
                    controller_form.PapiLightCallback(value);
                }
            }
        }

        #endregion

        #region Runway Light Callback

        public void RunwayLightCallback(string name, bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.RunwayLightCallback(name, value);
                    }));
                }
                else
                {
                    _instructor_form.RunwayLightCallback(name, value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.RunwayLightCallback(name, value);
                    }));
                }
                else
                {
                    pilot_form.RunwayLightCallback(name, value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.RunwayLightCallback(name, value);
                    }));
                }
                else
                {
                    controller_form.RunwayLightCallback(name, value);
                }
            }
        }

        #endregion

        #region Approach Light Callback

        public void ApproachLightCallback(string name, bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ApproachLightCallback(name, value);
                    }));
                }
                else
                {
                    _instructor_form.ApproachLightCallback(name, value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ApproachLightCallback(name, value);
                    }));
                }
                else
                {
                    pilot_form.ApproachLightCallback(name, value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ApproachLightCallback(name, value);
                    }));
                }
                else
                {
                    controller_form.ApproachLightCallback(name, value);
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Ground Callback

        #region Change Ground Status Callback
        public void ChangeGroundStatusCallback(string callsign, int status)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeGroundStatusCallback(callsign, status);
                    }));
                }
                else
                {
                    _instructor_form.ChangeGroundStatusCallback(callsign, status);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeGroundStatusCallback(callsign, status);
                    }));
                }
                else
                {
                    pilot_form.ChangeGroundStatusCallback(callsign, status);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeGroundStatusCallback(callsign, status);
                    }));
                }
                else
                {
                    controller_form.ChangeGroundStatusCallback(callsign, status);
                }
            }
        }

        #endregion

        #region Change Route Callback
        public void ChangeFlightRouteCallback(string callsign, FlightRouteListModel routes, string runway)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeFlightRouteCallback(callsign, routes, runway);
                    }));
                }
                else
                {
                    _instructor_form.ChangeFlightRouteCallback(callsign, routes, runway);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeFlightRouteCallback(callsign, routes, runway);
                    }));
                }
                else
                {
                    pilot_form.ChangeFlightRouteCallback(callsign, routes, runway);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeFlightRouteCallback(callsign, routes, runway);
                    }));
                }
                else
                {
                    controller_form.ChangeFlightRouteCallback(callsign, routes, runway);
                }
            }

        }

        #endregion

        #region Change Parking Callback
        public void ChangeParkingAppronCallback(string callsign, DataAppronModel terminal)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeParkingAppronCallback(callsign, terminal);
                    }));
                }
                else
                {
                    _instructor_form.ChangeParkingAppronCallback(callsign, terminal);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeParkingAppronCallback(callsign, terminal);
                    }));
                }
                else
                {
                    pilot_form.ChangeParkingAppronCallback(callsign, terminal);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeParkingAppronCallback(callsign, terminal);
                    }));
                }
                else
                {
                    controller_form.ChangeParkingAppronCallback(callsign, terminal);
                }
            }
        }

        #endregion

        #region Ready Aircraft Callback
        public void ReadyAircraftCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ReadyAircraftCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.ReadyAircraftCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ReadyAircraftCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.ReadyAircraftCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ReadyAircraftCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.ReadyAircraftCallback(callsign);
                }
            }
        }

        #endregion

        #region Change Runway Callback
        public void ChangeRunwayCallback(string callsign, string runway, bool is_ground)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeRunwayCallback(callsign, runway, is_ground);
                    }));
                }
                else
                {
                    _instructor_form.ChangeRunwayCallback(callsign, runway, is_ground);
                }

            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeRunwayCallback(callsign, runway, is_ground);
                    }));
                }
                else
                {
                    pilot_form.ChangeRunwayCallback(callsign, runway, is_ground);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeRunwayCallback(callsign, runway, is_ground);
                    }));
                }
                else
                {
                    controller_form.ChangeRunwayCallback(callsign, runway, is_ground);
                }
            }
        }

        #endregion

        #region Start Engine Callback
        public void StartEngineCallback(string callsign, bool engine)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.StartEngineCallback(callsign, engine);

                    }));
                }
                else
                {
                    _instructor_form.StartEngineCallback(callsign, engine);

                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.StartEngineCallback(callsign, engine);
                    }));
                }
                else
                {
                    pilot_form.StartEngineCallback(callsign, engine);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.StartEngineCallback(callsign, engine);
                    }));
                }
                else
                {
                    controller_form.StartEngineCallback(callsign, engine);
                }
            }
        }

        #endregion

        #region Pushback Callback
        public void PushbackCallback(string callsign, DirectionInfo pushback, string pushback_name)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.PushbackCallback(callsign, pushback, pushback_name);
                    }));
                }
                else
                {
                    _instructor_form.PushbackCallback(callsign, pushback, pushback_name);
                }

            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.PushbackCallback(callsign, pushback, pushback_name);
                    }));
                }
                else
                {
                    pilot_form.PushbackCallback(callsign, pushback, pushback_name);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.PushbackCallback(callsign, pushback, pushback_name);
                    }));
                }
                else
                {
                    controller_form.PushbackCallback(callsign, pushback, pushback_name);
                }
            }
        }

        #endregion

        #region Start Taxi Callback
        public void StartTaxiCallback(string callsign, List<string> routes, int status)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.StartTaxiCallback(callsign, routes, status);
                    }));
                }
                else
                {
                    _instructor_form.StartTaxiCallback(callsign, routes, status);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.StartTaxiCallback(callsign, routes, status);
                    }));
                }
                else
                {
                    pilot_form.StartTaxiCallback(callsign, routes, status);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.StartTaxiCallback(callsign, routes, status);
                    }));
                }
                else
                {
                    controller_form.StartTaxiCallback(callsign, routes, status);
                }
            }
        }

        #endregion

        #region Hold Taxi Callback
        public void HoldTaxiCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.HoldTaxiCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.HoldTaxiCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.HoldTaxiCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.HoldTaxiCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.HoldTaxiCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.HoldTaxiCallback(callsign);
                }
            }
        }

        #endregion

        #region Continue Taxi Callback
        public void ContinueTaxiCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ContinueTaxiCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.ContinueTaxiCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ContinueTaxiCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.ContinueTaxiCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ContinueTaxiCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.ContinueTaxiCallback(callsign);
                }
            }
        }

        #endregion

        #region Change Speed Taxi Callback
        public void ChangeSpeedTaxiCallback(string callsign, GroundSpeed speed)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeSpeedTaxiCallback(callsign, speed);
                    }));
                }
                else
                {
                    _instructor_form.ChangeSpeedTaxiCallback(callsign, speed);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeSpeedTaxiCallback(callsign, speed);
                    }));
                }
                else
                {
                    pilot_form.ChangeSpeedTaxiCallback(callsign, speed);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeSpeedTaxiCallback(callsign, speed);
                    }));
                }
                else
                {
                    controller_form.ChangeSpeedTaxiCallback(callsign, speed);
                }
            }
        }

        #endregion

        #region Cross Runway Callback

        public void CrossRunwayCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.CrossRunwayCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.CrossRunwayCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.CrossRunwayCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.CrossRunwayCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.CrossRunwayCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.CrossRunwayCallback(callsign);
                }
            }
        }

        #endregion

        #region UTurn Callback

        public void UturnCallback(string callsign, UturnInfo uturn)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.UturnCallback(callsign, uturn);
                    }));
                }
                else
                {
                    _instructor_form.UturnCallback(callsign, uturn);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.UturnCallback(callsign, uturn);
                    }));
                }
                else
                {
                    pilot_form.UturnCallback(callsign, uturn);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.UturnCallback(callsign, uturn);
                    }));
                }
                else
                {
                    controller_form.UturnCallback(callsign, uturn);
                }
            }
        }

        #endregion

        #region Rocking Wing Callback

        public void RockingWingCallback(string callsign, bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.RockingWingCallback(callsign, value);
                    }));
                }
                else
                {
                    _instructor_form.RockingWingCallback(callsign, value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.RockingWingCallback(callsign, value);
                    }));
                }
                else
                {
                    pilot_form.RockingWingCallback(callsign, value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.RockingWingCallback(callsign, value);
                    }));
                }
                else
                {
                    controller_form.RockingWingCallback(callsign, value);
                }
            }
        }

        #endregion

        #region Take Off Callback

        public void TakeOffCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.TakeOffCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.TakeOffCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.TakeOffCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.TakeOffCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.TakeOffCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.TakeOffCallback(callsign);
                }
            }
        }

        #endregion

        #region Runway Hold Callback

        public void RunwayHoldCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.RunwayHoldCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.RunwayHoldCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.RunwayHoldCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.RunwayHoldCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.RunwayHoldCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.RunwayHoldCallback(callsign);
                }
            }
        }

        #endregion

        #region Abort Take Off Callback

        public void AbortTakeOffCallback(string callsign)
        {
            //if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            //{
            //    if (Instance.InvokeRequired)
            //    {
            //        Instance.Invoke(new MethodInvoker(() =>
            //        {
            //            _instructor_form.AbortTakeOffCallback(callsign);
            //        }));
            //    }
            //    else
            //    {
            //        _instructor_form.AbortTakeOffCallback(callsign);
            //    }
            //}
            //else if (_pc_role.ToLower().Contains("pseudopilot"))
            //{
            //    if (Instance.InvokeRequired)
            //    {
            //        Instance.Invoke(new MethodInvoker(() =>
            //        {
            //            pilot_form.AbortTakeOffCallback(callsign);
            //        }));
            //    }
            //    else
            //    {
            //        pilot_form.AbortTakeOffCallback(callsign);
            //    }
            //}
        }

        #endregion

        #endregion

        #region Flight Callback

        #region Ready Aircraft Callback
        public void ActiveAircraftCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ActiveAircraftCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.ActiveAircraftCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ActiveAircraftCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.ActiveAircraftCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ActiveAircraftCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.ActiveAircraftCallback(callsign);
                }
            }
        }

        #endregion

        #region Holding Now Callback
        public void HoldingNowCallback(string callsign, DirectionInfo direction)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.HoldingNowCallback(callsign, direction);
                    }));
                }
                else
                {
                    _instructor_form.HoldingNowCallback(callsign, direction);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.HoldingNowCallback(callsign, direction);
                    }));
                }
                else
                {
                    pilot_form.HoldingNowCallback(callsign, direction);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.HoldingNowCallback(callsign, direction);
                    }));
                }
                else
                {
                    controller_form.HoldingNowCallback(callsign, direction);
                }
            }
        }
        #endregion

        #region Holding Position Callback
        public void HoldingPositionCallback(string callsign, string position)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.HoldingPositionCallback(callsign, position);
                    }));
                }
                else
                {
                    _instructor_form.HoldingPositionCallback(callsign, position);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.HoldingPositionCallback(callsign, position);
                    }));
                }
                else
                {
                    pilot_form.HoldingPositionCallback(callsign, position);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.HoldingPositionCallback(callsign, position);
                    }));
                }
                else
                {
                    controller_form.HoldingPositionCallback(callsign, position);
                }
            }
        }
        #endregion

        #region Continue Route Callback
        public void ContinueRouteCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ContinueRouteCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.ContinueRouteCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ContinueRouteCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.ContinueRouteCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ContinueRouteCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.ContinueRouteCallback(callsign);
                }
            }
        }
        #endregion

        #region Continue Direction Callback
        public void ContinueDirectionCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ContinueDirectionCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.ContinueDirectionCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ContinueDirectionCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.ContinueDirectionCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ContinueDirectionCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.ContinueDirectionCallback(callsign);
                }
            }
        }
        #endregion

        #region Change Heading Callback
        public void ChangeHeadingCallback(string callsign, HeadingInfo direction, float heading)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeHeadingCallback(callsign, direction, heading);
                    }));
                }
                else
                {
                    _instructor_form.ChangeHeadingCallback(callsign, direction, heading);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeHeadingCallback(callsign, direction, heading);
                    }));
                }
                else
                {
                    pilot_form.ChangeHeadingCallback(callsign, direction, heading);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeHeadingCallback(callsign, direction, heading);
                    }));
                }
                else
                {
                    controller_form.ChangeHeadingCallback(callsign, direction, heading);
                }
            }
        }
        #endregion

        #region Change Route Callback
        public void ChangeRouteCallback(string callsign, FlightRouteListModel routes)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeRouteCallback(callsign, routes);
                    }));
                }
                else
                {
                    _instructor_form.ChangeRouteCallback(callsign, routes);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeRouteCallback(callsign, routes);
                    }));
                }
                else
                {
                    pilot_form.ChangeRouteCallback(callsign, routes);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeRouteCallback(callsign, routes);
                    }));
                }
                else
                {
                    controller_form.ChangeRouteCallback(callsign, routes);
                }
            }
        }
        #endregion

        #region Direct Go Callback
        public void DirectGoCallback(string callsign, string position, bool isILS)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.DirectGoCallback(callsign, position, isILS);
                    }));
                }
                else
                {
                    _instructor_form.DirectGoCallback(callsign, position, isILS);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.DirectGoCallback(callsign, position, isILS);
                    }));
                }
                else
                {
                    pilot_form.DirectGoCallback(callsign, position, isILS);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.DirectGoCallback(callsign, position, isILS);
                    }));
                }
                else
                {
                    controller_form.DirectGoCallback(callsign, position, isILS);
                }
            }
        }
        #endregion

        #region Instrument Approach Callback
        public void ApproachCallback(string callsign, string runway)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ApproachCallback(callsign, runway);
                    }));
                }
                else
                {
                    _instructor_form.ApproachCallback(callsign, runway);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ApproachCallback(callsign, runway);
                    }));
                }
                else
                {
                    pilot_form.ApproachCallback(callsign, runway);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ApproachCallback(callsign, runway);
                    }));
                }
                else
                {
                    controller_form.ApproachCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Join Circuit Callback
        public void CircuitCallback(string callsign, string runway)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.CircuitCallback(callsign, runway);
                    }));
                }
                else
                {
                    _instructor_form.CircuitCallback(callsign, runway);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.CircuitCallback(callsign, runway);
                    }));
                }
                else
                {
                    pilot_form.CircuitCallback(callsign, runway);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.CircuitCallback(callsign, runway);
                    }));
                }
                else
                {
                    controller_form.CircuitCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Missed Appraoch Callback
        public void MissedApproachCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.MissedApproachCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.MissedApproachCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.MissedApproachCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.MissedApproachCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.MissedApproachCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.MissedApproachCallback(callsign);
                }
            }
        }
        #endregion

        #region Touch Go Callback
        public void TouchGoCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.TouchGoCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.TouchGoCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.TouchGoCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.TouchGoCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.TouchGoCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.TouchGoCallback(callsign);
                }
            }
        }
        #endregion

        #region Flypass Callback
        public void FlypassCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FlypassCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.FlypassCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FlypassCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.FlypassCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FlypassCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.FlypassCallback(callsign);
                }
            }
        }
        #endregion

        #region Extend Downwind Callback
        public void ExtendDownwindCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ExtendDownwindCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.ExtendDownwindCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ExtendDownwindCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.ExtendDownwindCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ExtendDownwindCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.ExtendDownwindCallback(callsign);
                }
            }
        }
        #endregion

        #region Orbit Callback
        public void OrbitCallback(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.OrbitCallback(callsign);
                    }));
                }
                else
                {
                    _instructor_form.OrbitCallback(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.OrbitCallback(callsign);
                    }));
                }
                else
                {
                    pilot_form.OrbitCallback(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.OrbitCallback(callsign);
                    }));
                }
                else
                {
                    controller_form.OrbitCallback(callsign);
                }
            }
        }
        #endregion

        #region Landing Callback
        public void LandingCallback(string callsign, string runway)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.LandingCallback(callsign, runway);
                    }));
                }
                else
                {
                    _instructor_form.LandingCallback(callsign, runway);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.LandingCallback(callsign, runway);
                    }));
                }
                else
                {
                    pilot_form.LandingCallback(callsign, runway);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.LandingCallback(callsign, runway);
                    }));
                }
                else
                {
                    controller_form.LandingCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Altitude Callback
        public void AltitudeCallback(string callsign, float altitude)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.AltitudeCallback(callsign, altitude);
                    }));
                }
                else
                {
                    _instructor_form.AltitudeCallback(callsign, altitude);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.AltitudeCallback(callsign, altitude);
                    }));
                }
                else
                {
                    pilot_form.AltitudeCallback(callsign, altitude);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.AltitudeCallback(callsign, altitude);
                    }));
                }
                else
                {
                    controller_form.AltitudeCallback(callsign, altitude);
                }
            }
        }
        #endregion

        #region Speed Callback
        public void SpeedCallback(string callsign, float speed)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.SpeedCallback(callsign, speed);
                    }));
                }
                else
                {
                    _instructor_form.SpeedCallback(callsign, speed);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.SpeedCallback(callsign, speed);
                    }));
                }
                else
                {
                    pilot_form.SpeedCallback(callsign, speed);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.SpeedCallback(callsign, speed);
                    }));
                }
                else
                {
                    controller_form.SpeedCallback(callsign, speed);
                }
            }
        }
        #endregion

        #region Change ILS Mode Callback
        public void ChangeILSModeCallback(string callsign, bool value)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ChangeILSModeCallback(callsign, value);
                    }));
                }
                else
                {
                    _instructor_form.ChangeILSModeCallback(callsign, value);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ChangeILSModeCallback(callsign, value);
                    }));
                }
                else
                {
                    pilot_form.ChangeILSModeCallback(callsign, value);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ChangeILSModeCallback(callsign, value);
                    }));
                }
                else
                {
                    controller_form.ChangeILSModeCallback(callsign, value);
                }
            }
        }
        #endregion

        #endregion

        #endregion

        #region Visual Callback

        #region System Callback

        #region Ready Simulation Callback
        public void ReadySimulationCallback()
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ReadySimulationCallback();
                    }));
                }
                else
                {
                    _instructor_form.ReadySimulationCallback();
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ReadySimulationCallback();
                    }));
                }
                else
                {
                    pilot_form.ReadySimulationCallback();
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ReadySimulationCallback();
                    }));
                }
                else
                {
                    controller_form.ReadySimulationCallback();
                }
            }
            // visual is ready , close waiting dialog
            SetLockScreen("wait", false);
        }

        #endregion

        #region Update Time Simulation
        public void UpdateTimeSimulation(TimeSpan time)
        {
            try
            {
                if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            _instructor_form.UpdateTimeSimulation(time);
                        }));
                    }
                    else
                    {
                        _instructor_form.UpdateTimeSimulation(time);
                    }
                }
                else if (_pc_role.ToLower().Contains("pseudopilot"))
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            pilot_form.UpdateTimeSimulation(time);
                        }));
                    }
                    else
                    {
                        pilot_form.UpdateTimeSimulation(time);
                    }
                }
                else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            controller_form.UpdateTimeSimulation(time);
                        }));
                    }
                    else
                    {
                        controller_form.UpdateTimeSimulation(time);
                    }
                }
            }
            catch { }
        }

        #endregion

        #region Pause Simulation Callback
        public void PauseSimulationCallback(bool value)
        {
            if (_pc_role.ToLower().Equals("instructor") || _pc_role.ToLower().Equals("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.PauseSimulation(value);
                    }));
                }
                else
                {
                    _instructor_form.PauseSimulation(value);
                }
            }
            else
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        PauseSimulation(value);
                    }));
                }
                else
                {
                    PauseSimulation(value);
                }
            }
        }

        public void PauseSimulation(bool value)
        {
            SetLockScreen("pause", value);
        }

        #endregion

        #region Stop Simulation Callback
        public void StopSimulationCallback()
        {
            if (Instance.InvokeRequired)
            {
                Instance.Invoke(new MethodInvoker(() =>
                {
                    this.Hide();
                }));
            }
            else
            {
                this.Hide();
            }
        }

        #endregion

        #endregion

        #region Ground Callback

        #region Finish Ready Departure
        public void FinishReadyDeparture(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishReadyDeparture(callsign);
                    }));
                }
                else
                {
                    _instructor_form.FinishReadyDeparture(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishReadyDeparture(callsign);
                    }));
                }
                else
                {
                    pilot_form.FinishReadyDeparture(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishReadyDeparture(callsign);
                    }));
                }
                else
                {
                    controller_form.FinishReadyDeparture(callsign);
                }
            }
        }

        #endregion

        #region Finish Pushback
        public void FinishPushback(string callsign, double heading)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishPushback(callsign, heading);
                    }));
                }
                else
                {
                    _instructor_form.FinishPushback(callsign, heading);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishPushback(callsign, heading);
                    }));
                }
                else
                {
                    pilot_form.FinishPushback(callsign, heading);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishPushback(callsign, heading);
                    }));
                }
                else
                {
                    controller_form.FinishPushback(callsign, heading);
                }
            }
        }

        #endregion

        #region Update Taxi Position

        public void UpdateTaxiPosition(string callsign, string position, double heading)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.UpdateTaxiPosition(callsign, position, heading);
                    }));
                }
                else
                {
                    _instructor_form.UpdateTaxiPosition(callsign, position, heading);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.UpdateTaxiPosition(callsign, position, heading);
                    }));
                }
                else
                {
                    pilot_form.UpdateTaxiPosition(callsign, position, heading);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.UpdateTaxiPosition(callsign, position, heading);
                    }));
                }
                else
                {
                    controller_form.UpdateTaxiPosition(callsign, position, heading);
                }
            }
        }

        #endregion

        #region Finish Taxi

        public void FinishTaxi(string callsign, string position, double heading, int status)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishTaxi(callsign, position, heading, status);
                    }));
                }
                else
                {
                    _instructor_form.FinishTaxi(callsign, position, heading, status);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishTaxi(callsign, position, heading, status);
                    }));
                }
                else
                {
                    pilot_form.FinishTaxi(callsign, position, heading, status);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishTaxi(callsign, position, heading, status);
                    }));
                }
                else
                {
                    controller_form.FinishTaxi(callsign, position, heading, status);
                }
            }
        }

        #endregion

        #region Heading Aircraft

        public void HeadingAircraft(string callsign, double heading)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.HeadingAircraft(callsign, heading);
                    }));
                }
                else
                {
                    _instructor_form.HeadingAircraft(callsign, heading);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.HeadingAircraft(callsign, heading);
                    }));
                }
                else
                {
                    pilot_form.HeadingAircraft(callsign, heading);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.HeadingAircraft(callsign, heading);
                    }));
                }
                else
                {
                    controller_form.HeadingAircraft(callsign, heading);
                }
            }
        }

        #endregion

        #region Request Cross Runway

        public void RequestCrossRunway(string callsign, double heading)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.RequestCrossRunway(callsign, heading);
                    }));
                }
                else
                {
                    _instructor_form.RequestCrossRunway(callsign, heading);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.RequestCrossRunway(callsign, heading);
                    }));
                }
                else
                {
                    pilot_form.RequestCrossRunway(callsign, heading);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.RequestCrossRunway(callsign, heading);
                    }));
                }
                else
                {
                    controller_form.RequestCrossRunway(callsign, heading);
                }
            }
        }

        #endregion

        #region Finish UTurn

        public void FinishUTurn(string callsign, double heading)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishUTurn(callsign, heading);
                    }));
                }
                else
                {
                    _instructor_form.FinishUTurn(callsign, heading);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishUTurn(callsign, heading);
                    }));
                }
                else
                {
                    pilot_form.FinishUTurn(callsign, heading);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishUTurn(callsign, heading);
                    }));
                }
                else
                {
                    controller_form.FinishUTurn(callsign, heading);
                }
            }
        }

        #endregion

        #region Finish Hold Take Off

        public void FinishHoldTakeOff(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishHoldTakeOff(callsign);
                    }));
                }
                else
                {
                    _instructor_form.FinishHoldTakeOff(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishHoldTakeOff(callsign);
                    }));
                }
                else
                {
                    pilot_form.FinishHoldTakeOff(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishHoldTakeOff(callsign);
                    }));
                }
                else
                {
                    controller_form.FinishHoldTakeOff(callsign);
                }
            }
        }

        #endregion

        #region Finish Take Off

        public void FinishTakeOff(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishTakeOff(callsign);
                    }));
                }
                else
                {
                    _instructor_form.FinishTakeOff(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishTakeOff(callsign);
                    }));
                }
                else
                {
                    pilot_form.FinishTakeOff(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishTakeOff(callsign);
                    }));
                }
                else
                {
                    controller_form.FinishTakeOff(callsign);
                }
            }
        }

        #endregion

        #region Crash Aircraft

        public void CrashAircraft(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.CrashAircraft(callsign);
                    }));
                }
                else
                {
                    _instructor_form.CrashAircraft(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.CrashAircraft(callsign);
                    }));
                }
                else
                {
                    pilot_form.CrashAircraft(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.CrashAircraft(callsign);
                    }));
                }
                else
                {
                    controller_form.CrashAircraft(callsign);
                }
            }
        }

        #endregion

        #region Finish Parking

        public void FinishParking(AircraftData aircraft)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishParking(aircraft);
                    }));
                }
                else
                {
                    _instructor_form.FinishParking(aircraft);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishParking(aircraft);
                    }));
                }
                else
                {
                    pilot_form.FinishParking(aircraft);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishParking(aircraft);
                    }));
                }
                else
                {
                    controller_form.FinishParking(aircraft);
                }
            }
        }

        #endregion

        #region Backtrack

        public void Backtrack(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.Backtrack(callsign);
                    }));
                }
                else
                {
                    _instructor_form.Backtrack(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.Backtrack(callsign);
                    }));
                }
                else
                {
                    pilot_form.Backtrack(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.Backtrack(callsign);
                    }));
                }
                else
                {
                    controller_form.Backtrack(callsign);
                }
            }
        }

        #endregion

        #endregion

        #region Flight Callback

        #region Cannot Change Speed
        public void CannotChangeSpeed(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.CannotChangeSpeed(callsign);
                    }));
                }
                else
                {
                    _instructor_form.CannotHolding(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.CannotChangeSpeed(callsign);
                    }));
                }
                else
                {
                    pilot_form.CannotHolding(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.CannotChangeSpeed(callsign);
                    }));
                }
                else
                {
                    controller_form.CannotChangeSpeed(callsign);
                }
            }
        }
        #endregion

        #region Cannot Change Altitude
        public void CannotChangeAltitude(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.CannotChangeAltitude(callsign);
                    }));
                }
                else
                {
                    _instructor_form.CannotChangeAltitude(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.CannotChangeAltitude(callsign);
                    }));
                }
                else
                {
                    pilot_form.CannotHolding(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.CannotChangeAltitude(callsign);
                    }));
                }
                else
                {
                    controller_form.CannotChangeAltitude(callsign);
                }
            }
        }
        #endregion

        #region Update Performance
        public void UpdatePerformanceCallback(string callsign, float speed, float altitude)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.UpdatePerformanceCallback(callsign, speed, altitude);
                    }));
                }
                else
                {
                    _instructor_form.UpdatePerformanceCallback(callsign, speed, altitude);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.UpdatePerformanceCallback(callsign, speed, altitude);
                    }));
                }
                else
                {
                    pilot_form.UpdatePerformanceCallback(callsign, speed, altitude);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.UpdatePerformanceCallback(callsign, speed, altitude);
                    }));
                }
                else
                {
                    controller_form.UpdatePerformanceCallback(callsign, speed, altitude);
                }
            }
        }

        #endregion

        #region Update Holding Point
        public void UpdateHoldingPoint(List<string> holding)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.UpdateHoldingPoint(holding);
                    }));
                }
                else
                {
                    _instructor_form.UpdateHoldingPoint(holding);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.UpdateHoldingPoint(holding);
                    }));
                }
                else
                {
                    pilot_form.UpdateHoldingPoint(holding);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.UpdateHoldingPoint(holding);
                    }));
                }
                else
                {
                    controller_form.UpdateHoldingPoint(holding);
                }
            }
        }
        #endregion

        #region Cannot Holding
        public void CannotHolding(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.CannotHolding(callsign);
                    }));
                }
                else
                {
                    _instructor_form.CannotHolding(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.CannotHolding(callsign);
                    }));
                }
                else
                {
                    pilot_form.CannotHolding(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.CannotHolding(callsign);
                    }));
                }
                else
                {
                    controller_form.CannotHolding(callsign);
                }
            }
        }
        #endregion

        #region Holding At Callback
        public void HoldingAtCallback(string callsign, string position, bool isInstrument)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.HoldingAtCallback(callsign, position, isInstrument);
                    }));
                }
                else
                {
                    _instructor_form.HoldingAtCallback(callsign, position, isInstrument);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.HoldingAtCallback(callsign, position, isInstrument);
                    }));
                }
                else
                {
                    pilot_form.HoldingAtCallback(callsign, position, isInstrument);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.HoldingAtCallback(callsign, position, isInstrument);
                    }));
                }
                else
                {
                    controller_form.HoldingAtCallback(callsign, position, isInstrument);
                }
            }
        }
        #endregion

        #region Route Update Callback
        public void RouteUpdateCallback(string callsign, FlightRouteListModel route)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.RouteUpdateCallback(callsign, route);
                    }));
                }
                else
                {
                    _instructor_form.RouteUpdateCallback(callsign, route);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.RouteUpdateCallback(callsign, route);
                    }));
                }
                else
                {
                    pilot_form.RouteUpdateCallback(callsign, route);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.RouteUpdateCallback(callsign, route);
                    }));
                }
                else
                {
                    controller_form.RouteUpdateCallback(callsign, route);
                }
            }
        }
        #endregion

        #region Ready Join Circuit
        public void ReadyJoinCircuit(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.ReadyJoinCircuit(callsign);
                    }));
                }
                else
                {
                    _instructor_form.ReadyJoinCircuit(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.ReadyJoinCircuit(callsign);
                    }));
                }
                else
                {
                    pilot_form.ReadyJoinCircuit(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.ReadyJoinCircuit(callsign);
                    }));
                }
                else
                {
                    controller_form.ReadyJoinCircuit(callsign);
                }
            }
        }
        #endregion

        #region Join Circuit Callback Visual
        public void JoinCircuitCallback(string callsign, string runway)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.JoinCircuitCallback(callsign, runway);
                    }));
                }
                else
                {
                    _instructor_form.JoinCircuitCallback(callsign, runway);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.JoinCircuitCallback(callsign, runway);
                    }));
                }
                else
                {
                    pilot_form.JoinCircuitCallback(callsign, runway);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.JoinCircuitCallback(callsign, runway);
                    }));
                }
                else
                {
                    controller_form.JoinCircuitCallback(callsign, runway);
                }
            }
        }
        #endregion

        #region Finish Go Round
        public void FinishGoRound(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishGoRound(callsign);
                    }));
                }
                else
                {
                    _instructor_form.FinishGoRound(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishGoRound(callsign);
                    }));
                }
                else
                {
                    pilot_form.FinishGoRound(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishGoRound(callsign);
                    }));
                }
                else
                {
                    controller_form.FinishGoRound(callsign);
                }
            }
        }
        #endregion

        #region Start Approach
        public void StartApproach(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.StartApproach(callsign);
                    }));
                }
                else
                {
                    _instructor_form.StartApproach(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.StartApproach(callsign);
                    }));
                }
                else
                {
                    pilot_form.StartApproach(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.StartApproach(callsign);
                    }));
                }
                else
                {
                    controller_form.StartApproach(callsign);
                }
            }
        }
        #endregion

        #region Start Go Round
        public void StartGoRound(string callsign, bool isFlypass, bool isIfr)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.StartGoRound(callsign, isFlypass, isIfr);
                    }));
                }
                else
                {
                    _instructor_form.StartGoRound(callsign, isFlypass, isIfr);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.StartGoRound(callsign, isFlypass, isIfr);
                    }));
                }
                else
                {
                    pilot_form.StartGoRound(callsign, isFlypass, isIfr);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.StartGoRound(callsign, isFlypass, isIfr);
                    }));
                }
                else
                {
                    controller_form.StartGoRound(callsign, isFlypass, isIfr);
                }
            }
        }
        #endregion

        #region Cannot Landing
        public void CannotLanding(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.CannotLanding(callsign);
                    }));
                }
                else
                {
                    _instructor_form.CannotLanding(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.CannotLanding(callsign);
                    }));
                }
                else
                {
                    pilot_form.CannotLanding(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.CannotLanding(callsign);
                    }));
                }
                else
                {
                    controller_form.CannotLanding(callsign);
                }
            }
        }
        #endregion

        #region Start Landing
        public void StartLanding(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.StartLanding(callsign);
                    }));
                }
                else
                {
                    _instructor_form.StartLanding(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.StartLanding(callsign);
                    }));
                }
                else
                {
                    pilot_form.StartLanding(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.StartLanding(callsign);
                    }));
                }
                else
                {
                    controller_form.StartLanding(callsign);
                }
            }
        }
        #endregion

        #region Touch Ground
        public void TouchGround(string callsign)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.TouchGround(callsign);
                    }));
                }
                else
                {
                    _instructor_form.TouchGround(callsign);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.TouchGround(callsign);
                    }));
                }
                else
                {
                    pilot_form.TouchGround(callsign);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.TouchGround(callsign);
                    }));
                }
                else
                {
                    controller_form.TouchGround(callsign);
                }
            }
        }
        #endregion

        #region Finish Landing
        public void FinishLandingCallback(string callsign, float heading, string from, string position)
        {
            if (_pc_role.Equals("instructor") || _pc_role.Contains("supervisor"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        _instructor_form.FinishLandingCallback(callsign, heading, from, position);
                    }));
                }
                else
                {
                    _instructor_form.FinishLandingCallback(callsign, heading, from, position);
                }
            }
            else if (_pc_role.ToLower().Contains("pseudopilot"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        pilot_form.FinishLandingCallback(callsign, heading, from, position);
                    }));
                }
                else
                {
                    pilot_form.FinishLandingCallback(callsign, heading, from, position);
                }
            }
            else if (_pc_role.Contains("appcontroller") || _pc_role.Contains("adccontroller"))
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        controller_form.FinishLandingCallback(callsign, heading, from, position);
                    }));
                }
                else
                {
                    controller_form.FinishLandingCallback(callsign, heading, from, position);
                }
            }
        }

        #endregion

        #endregion

        #endregion


    }
}
