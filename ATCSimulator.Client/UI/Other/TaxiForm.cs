﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using ATCSimulator.Client.UI.Simulation.Pilot;
using Newtonsoft.Json;

namespace ATCSimulator.Client.UI.Other
{
    public partial class TaxiForm : Form
    {
        private const string FIND_RUNWAY = "FindRunway";
        private const string RESET_POINT = "ResetPointData";
        private const string SHOW_LEGEND = "ShowLegend";
        private const string FIND_PARKING = "FindParking";
        private const string REQUEST_ROUTES = "RequestRoutes";

        List<string> routes;
        private GroundForm pilot;
        private string send_data;
        private string function_name;
        public TaxiForm(GroundForm pilot)
        {
            InitializeComponent();
            this.pilot = pilot;
        }
        public void Init(string scenery_name)
        {
            string file_path = System.IO.Directory.GetCurrentDirectory() + "\\Asset\\" + scenery_name + "\\Pilot.swf";
            FlashContainer.Movie = file_path;
            FlashContainer.Play();
            routes = new List<string>();
        }
        public void TaxiToRunway(string current_position, string from, double heading, string runway, bool is_helicopter)
        {
            StatusTaxi.Text = "Find Intersection";
            TargetLabel.Text = runway;
            send_data = string.Format("{0}&{1}&{2}&{3}&{4}", current_position, from, heading, runway,is_helicopter);
            function_name = FIND_RUNWAY;
        }
        public void TaxiToParking(string current_position, string from, double heading, string parking_bay,bool is_helicopter)
        {
            StatusTaxi.Text = "Find Parking";
            TargetLabel.Text = parking_bay;
            send_data = string.Format("{0}&{1}&{2}&{3}&{4}", current_position, from, heading, parking_bay,is_helicopter);
            function_name = FIND_PARKING;
        }
        private void SendStringToFlash(string function_name, string sendstring)
        {
            FlashContainer.CallFunction("<invoke name=\"" + function_name + "\" returntype=\"xml\"><arguments><string>" + sendstring + "</string></arguments></invoke>");
        }
        private void FlashContainer_FlashCall(object sender, AxShockwaveFlashObjects._IShockwaveFlashEvents_FlashCallEvent e)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(e.request);
            XmlAttributeCollection attributes = document.FirstChild.Attributes;
            String command = attributes.Item(0).InnerText;
            XmlNodeList list = document.GetElementsByTagName("arguments");
            String arg = list.Item(0).InnerText;
            switch (command)
            {
                case "GetRoutes":
                    routes = JsonConvert.DeserializeObject<List<string>>(arg);
                    if (routes.Count > 0)
                    {
                        pilot.SetTaxiRoute(routes);
                        this.Close();
                    }
                    else
                        MessageBox.Show("Taxi Route must be defined at least 1 ");
                    break;
                case "OKBtn":
                    OKBtn.Enabled = bool.Parse(arg);
                    break;
                case "ReadyAccess":
                    if(!string.IsNullOrEmpty(send_data))
                        SendStringToFlash(function_name,send_data);
                    break;
            }
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ResetBtn_Click(object sender, EventArgs e)
        {
            SendStringToFlash(RESET_POINT, "");
            routes = new List<string>();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            SendStringToFlash(REQUEST_ROUTES, "");
        }

        private void LegendBtn_CheckedChanged(object sender, EventArgs e)
        {
            SendStringToFlash(SHOW_LEGEND, LegendBtn.Checked.ToString());
        }
    }
}
