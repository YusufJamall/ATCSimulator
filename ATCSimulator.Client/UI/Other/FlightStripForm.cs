﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SimulationService;

namespace ATCSimulator.Client.UI.Other
{
    public partial class FlightStripForm : Telerik.WinControls.UI.RadForm
    {
        public FlightStripForm()
        {
            InitializeComponent();
        }

        public void Init(AircraftData aircraft, string role)
        {
            // init form based on aircraft data

            // for local TWR and APP
            if (aircraft.is_departure && aircraft.pilot_properties.flight.flight_route.flight_rules.ToLower().Equals("local"))
            {
                ETA.Text = aircraft.departure_time.ToString(@"hh\:mm") + " UTC hh:mm";
                Fuel.Text = aircraft.fuel.ToString(@"hh\:mm");
                FlightType.Text = aircraft.properties.model;
                Callsign.Text = aircraft.callsign;
                destination.Visible = false;
                route.Visible = false;
                this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.estrip_departure;
                this.BackColor = Color.White;
            }
            else
            {
                Fuel.Visible = false;
                FlightType.Text = aircraft.properties.model;
                Callsign.Text = aircraft.callsign;
                destination.Text = aircraft.pilot_properties.flight.flight_route.destination_airport_code;
                route.Text = "";
                foreach (var item in aircraft.pilot_properties.flight.flight_route.routes_name)
                {
                    route.Text += item + " - ";
                }
                route.Text += " , " + aircraft.pilot_properties.flight.flight_route.alternate_airport_code;

                if (aircraft.is_departure)
                {
                    ETA.Text = aircraft.departure_time.ToString(@"hh\:mm") + " UTC hh:mm";
                    if (role.ToLower().Contains("appcontroller"))
                    {
                        this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.estrip_departure;
                        this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
                    }
                    else
                    {
                        this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.estrip_departure;
                        this.BackColor = Color.LightBlue;
                    }
                }
                else
                {
                    ETA.Text = (aircraft.start_time + aircraft.estimate_time).ToString(@"hh\:mm") + " UTC hh:mm";
                    if (role.ToLower().Contains("appcontroller"))
                    {
                        this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.estrip_arrival;
                        this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(220)))), ((int)(((byte)(130)))));
                    }
                    else
                    {
                        this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.estrip_arrival;
                        this.BackColor = Color.Yellow;
                    }
                }
            }
        }

        public Bitmap GetCapturedForm()
        {
            closeButton.Visible = false;
            Bitmap bitmap = new Bitmap(this.Width, this.Height);
            this.DrawToBitmap(bitmap, new Rectangle(0, 0, this.Width, this.Height));
            closeButton.Visible = true;
            return bitmap;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
