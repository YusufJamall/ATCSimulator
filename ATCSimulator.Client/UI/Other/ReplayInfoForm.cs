﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Newtonsoft.Json;
using ATCSimulator.Client.UI;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.Services;

namespace ATCSimulator.Client.UI.Other
{
    public partial class ReplayInfoForm : Telerik.WinControls.UI.RadForm
    {
        AircraftHistoryData aircraft;
        List<PlayerListModel> player_list;
        public ReplayInfoForm()
        {
            InitializeComponent();
        }
        public ReplayInfoForm(AircraftHistoryData aircraft, List<PlayerListModel> player_list)
        {
            InitializeComponent();
            this.aircraft = aircraft;
            this.player_list = player_list;
            Callsign.Text = aircraft.callsign;
            TypeAircraft.Text = aircraft.model;
            DepartureTime.Text = aircraft.departure_time.ToString(@"hh\:mm\:ss");
            LiveryImage.Image = HelperClient.GetImageLivery(aircraft.livery, false);
            ChangeFlightRoute();
        }
        public void ChangeFlightRoute()
        {
            Origin.Text = string.Format("{0}({1})", aircraft.origin_airport_name, aircraft.origin_airport_code);
            Destination.Text = string.Format("{0}({1})", aircraft.destination_airport_name, aircraft.destination_airport_code);
        }

        private void DetailBtn_Click(object sender, EventArgs e)
        {
            FlightPlanModel data = new FlightPlanModel();
            data.flight_rules = aircraft.flight_rules;
            data.callsign = aircraft.callsign;
            data.type_aircraft = aircraft.type;
            data.true_speed = aircraft.true_speed;
            data.origin_airport_code = aircraft.origin_airport_code;
            data.origin_airport_name = aircraft.origin_airport_name;
            data.departure_time = aircraft.departure_time;
            data.altitude = aircraft.flight_level;
            data.flight_level = aircraft.flight_level_type;
            data.origin_airport_code = aircraft.destination_airport_code;
            data.origin_airport_name = aircraft.destination_airport_name;

            data.eet = aircraft.eet;
            data.fuel_board = aircraft.fuel;
            data.alternate_airport_name = aircraft.alternate_airport_name;

            ////data.pilot = null;
            data.person = aircraft.person;
            data.strips = aircraft.livery_strips;
            foreach(var player in player_list)
            {
                if(player.id.Equals(aircraft.pilotId))
                {
                    data.pilot = new PilotInfoModel
                    {
                        name = player.fullname
                    };
                }
            }
            
            FlightPlanForm flight_plan = new FlightPlanForm(data);
            flight_plan.StartPosition = FormStartPosition.CenterScreen;
            flight_plan.ShowDialog();
        }
    }
}
