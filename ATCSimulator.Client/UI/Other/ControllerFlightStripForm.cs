﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using ATCSimulator.Client.UI.Other;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SimulationService;

namespace ATCSimulator.Client.UI.Other
{
    public partial class ControllerFlightStripForm : Telerik.WinControls.UI.RadForm
    {
        private DraggableGridView arrival;
        private DraggableGridView departure;
        private DraggableGridView local;
        private string role;

        public ControllerFlightStripForm()
        {
            InitializeComponent();
            arrival = new DraggableGridView();
            arrival.AllowAddNewRow = false;
            arrival.Dock = DockStyle.Fill;
            arrival.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            arrival.AutoSizeRows = true;
            arrival.AllowRowResize = false;
            arrival.Size = new Size { Height = 660, Width = 230 };
            arrival.ShowColumnHeaders = false;
            arrival.AutoScroll = true;
            arrival.Name = "Arrival";
            arrival.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(ViewFlightStrip);

            departure = new DraggableGridView();
            departure.AllowAddNewRow = false;
            departure.Dock = DockStyle.Fill;
            departure.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            departure.AutoSizeRows = true;
            departure.AllowRowResize = false;
            departure.Size = new Size { Height = 660, Width = 230 };
            departure.ShowColumnHeaders = false;
            departure.AutoScroll = true;
            departure.Name = "Departure";
            departure.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(ViewFlightStrip);

            local = new DraggableGridView();
            local.AllowAddNewRow = false;
            local.Dock = DockStyle.Fill;
            local.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            local.AutoSizeRows = true;
            local.AllowRowResize = false;
            local.Size = new Size { Height = 660, Width = 230 };
            local.ShowColumnHeaders = false;
            local.AutoScroll = true;
            local.Name = "Local";
            local.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(ViewFlightStrip);

            ArrivalPanel.Controls.Add(arrival);
            DeparturePanel.Controls.Add(departure);
            LocalPanel.Controls.Add(local);
        }

        public void Init(List<AircraftData> list_aircraft)
        {
            this.role = ConfigurationManager.AppSettings["PCRole"].ToString();

            if(role.ToLower().Contains("appcontroller"))
            {
                arrivalLabel.Text = "Inbound";
                departureLabel.Text = "Outbound";
            }
            else
            {
                arrivalLabel.Text = "Arrivals";
                departureLabel.Text = "Departure";
            }

            // init form based on list aircraft data
            List<FlightStrip> arrivalPic = new List<FlightStrip>();
            foreach (var arr in list_aircraft.Where(d => !d.is_departure).ToList())
            {
                // Arrival Aircraft
                FlightStripForm arrival_form = new FlightStripForm();
                arrival_form.Init(arr,role);
                arrival_form.SendToBack();
                arrival_form.Show();

                arrivalPic.Add(new FlightStrip
                {
                    image = arrival_form.GetCapturedForm(),
                    aircraft = arr
                });
                arrival_form.Close();
            }
            List<FlightStrip> departurePic = new List<FlightStrip>();
            foreach (var dep in list_aircraft.Where(d => d.is_departure && d.pilot_properties.flight.flight_route.flight_rules.ToLower() != "local").ToList())
            {
                // Arrival Aircraft
                FlightStripForm departure_form = new FlightStripForm();
                departure_form.Init(dep,role);
                departure_form.SendToBack();
                departure_form.Show();
                departurePic.Add(new FlightStrip
                {
                    image = departure_form.GetCapturedForm(),
                    aircraft = dep
                });
                departure_form.Close();
            }
            List<FlightStrip> localPic = new List<FlightStrip>();
            foreach (var loc in list_aircraft.Where(d => d.pilot_properties.flight.flight_route.flight_rules.ToLower().Equals("local")).ToList())
            {
                // Arrival Aircraft
                FlightStripForm local_form = new FlightStripForm();
                local_form.Init(loc,role);
                local_form.SendToBack();
                local_form.Show();
                localPic.Add(new FlightStrip
                {
                    image = local_form.GetCapturedForm(),
                    aircraft = loc
                });
                local_form.Close();
            }
            arrival.DataSource = CreateDataset(arrivalPic);
            arrival.DataMember = "table1";
            arrival.Columns["data"].IsVisible = false;
            departure.DataSource = CreateDataset(departurePic);
            departure.DataMember = "table1";
            departure.Columns["data"].IsVisible = false;
            local.DataSource = CreateDataset(localPic);
            local.DataMember = "table1";
            local.Columns["data"].IsVisible = false;
        }

        public DataSet CreateDataset(List<FlightStrip> data)
        {
            DataSet dataset = new DataSet();
            DataTable table = new DataTable();
            table.Columns.Add("FlightStrip", typeof(Bitmap));
            table.Columns.Add("data", typeof(AircraftData));
            foreach (FlightStrip item in data)
            {
                Bitmap result = new Bitmap(350, 80);
                using (Graphics g = Graphics.FromImage(result))
                    g.DrawImage(item.image, 0, 0, 350, 80);
                table.Rows.Add(result,item.aircraft);
            }
            dataset.Tables.Add(table);
            return dataset;
        }

        public void ViewFlightStrip(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            AircraftData aircraft = (AircraftData)e.Row.Cells["data"].Value;
            FlightStripForm form = new FlightStripForm();
            form.Init(aircraft,role);
            form.StartPosition = FormStartPosition.CenterScreen;
            form.ShowDialog();
        }
    }
}
