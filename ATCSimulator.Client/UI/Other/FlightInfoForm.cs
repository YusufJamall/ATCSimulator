﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.UI.Simulation.Instructor;
using ATCSimulator.Client.UI.Simulation.Pilot;
using ATCSimulator.Client.UI.Simulation.Controller;

namespace ATCSimulator.Client.UI.Other
{
    public partial class FlightInfoForm : Telerik.WinControls.UI.RadForm
    {
        public enum FlightInfoStatus
        {
            ENABLE,
            DISABLE,
            REMOVE,
            CRASH,
            BLINK
        }

        public FlightInfoStatus status = FlightInfoStatus.DISABLE;

        public AircraftData _aircraft;
        public Form _form;

        public FlightInfoForm(Form form, AircraftData aircraft)
        {
            InitializeComponent();
            this._aircraft = aircraft;
            this._form = form;

            Callsign.Text = _aircraft.callsign;
            TypeAircraft.Text = _aircraft.properties.model;
            DepartureTime.Text = _aircraft.departure_time.ToString("c");
            LiveryImage.Image = HelperClient.GetImageLivery(_aircraft.properties.livery, false);
            Origin.Text = string.Format("{0}({1})", _aircraft.pilot_properties.flight.flight_route.origin_airport_name, _aircraft.pilot_properties.flight.flight_route.origin_airport_code);
            Destination.Text = string.Format("{0}({1})", _aircraft.pilot_properties.flight.flight_route.destination_airport_name, _aircraft.pilot_properties.flight.flight_route.destination_airport_code);
            InfoStatusClass _status = SetStatusImageAircraft(_aircraft.status);
            StatusAircraft.Text = _status.message;
            StatusImage.Image = _status.image;
        }
        public void ChangeFlightRoute()
        {
            Origin.Text = string.Format("{0}({1})", _aircraft.pilot_properties.flight.flight_route.origin_airport_name, _aircraft.pilot_properties.flight.flight_route.origin_airport_code);
            Destination.Text = string.Format("{0}({1})", _aircraft.pilot_properties.flight.flight_route.destination_airport_name, _aircraft.pilot_properties.flight.flight_route.destination_airport_code);
        }
        public void ChangeStatus(AircraftStatus status)
        {
            _aircraft.status = status;
            InfoStatusClass _status = SetStatusImageAircraft(status);
            StatusAircraft.Text = _status.message;
            StatusImage.Image = _status.image;
        }
        private void FlightClick_Event(object sender, EventArgs e)
         {
                if (status.Equals(FlightInfoStatus.BLINK))
                    EnableAircraft();
                if (_form.Name.Equals("PilotForm"))
                {
                    ClickAircraft();
                    PilotForm pilot = (PilotForm)_form;
                    pilot.ClickEventAircraft(_aircraft, this);
                }
                else if (_form.Name.Equals("InstructorForm"))
                {
                    ClickAircraft();
                    InstructorForm instructor = (InstructorForm)_form;
                    instructor.ClickEventAircraft(_aircraft, this);
                }
                else if (_form.Name.Equals("ControllerForm"))
                {
                    ClickAircraft();
                    ControllerForm controller = (ControllerForm)_form;
                    controller.ClickEventAircraft(_aircraft, this);
                }
        }
        public void InfoStatus(FlightInfoStatus _status)
        {
            if (_status.Equals(FlightInfoStatus.ENABLE))
                EnableAircraft();
            else if (_status.Equals(FlightInfoStatus.DISABLE))
                DisableAircraft();
            else if (_status.Equals(FlightInfoStatus.REMOVE))
                RemoveAircraft();
            else if (_status.Equals(FlightInfoStatus.CRASH))
                CrashAircraft();
            else if (_status.Equals(FlightInfoStatus.BLINK))
                BlinkAircraft();
        }
        public void EnableAircraft()
        {
            status = FlightInfoStatus.ENABLE;
            InfoPanel.BackgroundImage = null;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_up;
            this.Enabled = true;
        }
        public void CrashAircraft()
        {
            status = FlightInfoStatus.CRASH;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_up;
            InfoPanel.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.crash;
        }
        public void RemoveAircraft()
        {
            status = FlightInfoStatus.REMOVE;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_up;
            InfoPanel.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.remove;
            this.Enabled = false;
        }

        public void BlinkAircraft()
        {
            status = FlightInfoStatus.BLINK;
            InfoPanel.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.blink;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_up;
        }
        public void DisableAircraft()
        {
            status = FlightInfoStatus.DISABLE;
            InfoPanel.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.disable;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_up;
            if (_form.Name.Equals("PilotForm"))
                this.Enabled = false;
            else
                this.Enabled = true;
        }
        public void ClickAircraft()
        {
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_down;
        }
        public void UnClickAircraft()
        {
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_up;
        }
        private InfoStatusClass SetStatusImageAircraft(AircraftStatus status)
        {
            InfoStatusClass result = new InfoStatusClass();
            switch (status)
            {
                case AircraftStatus.NONE:
                    result.image = global::ATCSimulator.Client.Properties.Resources.sleep;
                    result.message = "Waiting";
                    break;
                case AircraftStatus.ACC:
                    result.image = global::ATCSimulator.Client.Properties.Resources.acc;
                    result.message = "";
                    break;
                case AircraftStatus.APP:
                    result.image = global::ATCSimulator.Client.Properties.Resources.app;
                    result.message = "Set route and Flight Plan";
                    break;
                case AircraftStatus.Form:
                    result.image = global::ATCSimulator.Client.Properties.Resources.form;
                    result.message = "Wait Flight Plan";
                    break;
                case AircraftStatus.Pushback:
                    result.image = global::ATCSimulator.Client.Properties.Resources.pushback;
                    result.message = "Pusback";
                    break;
                case AircraftStatus.TakeOff:
                    result.image = global::ATCSimulator.Client.Properties.Resources.take_off;
                    result.message = "Take Off";
                    break;
                case AircraftStatus.Taxing:
                    result.image = global::ATCSimulator.Client.Properties.Resources.taxing;
                    result.message = "Taxi to Intersection";
                    break;
                case AircraftStatus.Parking:
                    result.image = global::ATCSimulator.Client.Properties.Resources.taxing;
                    result.message = "Taxi to Parking Appron";
                    break;
                case AircraftStatus.Landing:
                    result.image = global::ATCSimulator.Client.Properties.Resources.landing;
                    result.message = "Landing";
                    break;
                case AircraftStatus.FlyingToRoute:
                    result.message = "Flying to Destionation";
                    break;
                case AircraftStatus.FlyingToDirection:
                    result.message = "Flying Stay in Direction";
                    break;
                case AircraftStatus.Holding:
                    result.image = global::ATCSimulator.Client.Properties.Resources.holding;
                    result.message = "Holding";
                    break;

               
               
                case AircraftStatus.Circuit:
                    result.image = global::ATCSimulator.Client.Properties.Resources.tower;
                    break;
            }
            return result;
        }
    }
    public class InfoStatusClass
    {
        public Image image { get; set; }
        public string message { get; set; }
    }

}
