﻿namespace ATCSimulator.Client.UI.Other
{
    partial class ControllerFlightStripForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// aClean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.footnote = new System.Windows.Forms.Label();
            this.ArrivalPanel = new System.Windows.Forms.Panel();
            this.DeparturePanel = new System.Windows.Forms.Panel();
            this.arrivalLabel = new System.Windows.Forms.Label();
            this.localLabel = new System.Windows.Forms.Label();
            this.LineBottom = new System.Windows.Forms.PictureBox();
            this.departureLabel = new System.Windows.Forms.Label();
            this.LocalPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // footnote
            // 
            this.footnote.AutoSize = true;
            this.footnote.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.footnote.ForeColor = System.Drawing.Color.White;
            this.footnote.Location = new System.Drawing.Point(4, 749);
            this.footnote.Name = "footnote";
            this.footnote.Size = new System.Drawing.Size(330, 16);
            this.footnote.TabIndex = 141;
            this.footnote.Text = "Copyright © 2015 Media Indo Teknologi | All Rights Reserved";
            // 
            // ArrivalPanel
            // 
            this.ArrivalPanel.Location = new System.Drawing.Point(1, 41);
            this.ArrivalPanel.Name = "ArrivalPanel";
            this.ArrivalPanel.Size = new System.Drawing.Size(230, 660);
            this.ArrivalPanel.TabIndex = 143;
            // 
            // DeparturePanel
            // 
            this.DeparturePanel.Location = new System.Drawing.Point(239, 41);
            this.DeparturePanel.Name = "DeparturePanel";
            this.DeparturePanel.Size = new System.Drawing.Size(230, 660);
            this.DeparturePanel.TabIndex = 144;
            // 
            // arrivalLabel
            // 
            this.arrivalLabel.BackColor = System.Drawing.Color.DodgerBlue;
            this.arrivalLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.arrivalLabel.ForeColor = System.Drawing.Color.White;
            this.arrivalLabel.Location = new System.Drawing.Point(17, 9);
            this.arrivalLabel.Name = "arrivalLabel";
            this.arrivalLabel.Size = new System.Drawing.Size(190, 30);
            this.arrivalLabel.TabIndex = 145;
            this.arrivalLabel.Text = "Inbound";
            this.arrivalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // localLabel
            // 
            this.localLabel.BackColor = System.Drawing.Color.DodgerBlue;
            this.localLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.localLabel.ForeColor = System.Drawing.Color.White;
            this.localLabel.Location = new System.Drawing.Point(500, 9);
            this.localLabel.Name = "localLabel";
            this.localLabel.Size = new System.Drawing.Size(190, 30);
            this.localLabel.TabIndex = 146;
            this.localLabel.Text = "Local";
            this.localLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LineBottom
            // 
            this.LineBottom.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.LineBottom.Location = new System.Drawing.Point(5, 743);
            this.LineBottom.Name = "LineBottom";
            this.LineBottom.Size = new System.Drawing.Size(1348, 3);
            this.LineBottom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LineBottom.TabIndex = 142;
            this.LineBottom.TabStop = false;
            // 
            // departureLabel
            // 
            this.departureLabel.BackColor = System.Drawing.Color.DodgerBlue;
            this.departureLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.departureLabel.ForeColor = System.Drawing.Color.White;
            this.departureLabel.Location = new System.Drawing.Point(260, 9);
            this.departureLabel.Name = "departureLabel";
            this.departureLabel.Size = new System.Drawing.Size(190, 30);
            this.departureLabel.TabIndex = 147;
            this.departureLabel.Text = "Outbound";
            this.departureLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LocalPanel
            // 
            this.LocalPanel.Location = new System.Drawing.Point(477, 41);
            this.LocalPanel.Name = "LocalPanel";
            this.LocalPanel.Size = new System.Drawing.Size(230, 660);
            this.LocalPanel.TabIndex = 145;
            // 
            // ControllerFlightStripForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(708, 700);
            this.Controls.Add(this.LocalPanel);
            this.Controls.Add(this.departureLabel);
            this.Controls.Add(this.localLabel);
            this.Controls.Add(this.arrivalLabel);
            this.Controls.Add(this.DeparturePanel);
            this.Controls.Add(this.ArrivalPanel);
            this.Controls.Add(this.footnote);
            this.Controls.Add(this.LineBottom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ControllerFlightStripForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PseudoPilotForm";
            ((System.ComponentModel.ISupportInitialize)(this.LineBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label footnote;
        private System.Windows.Forms.PictureBox LineBottom;
        private System.Windows.Forms.Panel ArrivalPanel;
        private System.Windows.Forms.Panel DeparturePanel;
        private System.Windows.Forms.Label arrivalLabel;
        private System.Windows.Forms.Label localLabel;
        private System.Windows.Forms.Label departureLabel;
        private System.Windows.Forms.Panel LocalPanel;
        


    }
}