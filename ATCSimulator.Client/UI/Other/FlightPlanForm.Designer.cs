﻿namespace ATCSimulator.Client.UI.Other
{
    partial class FlightPlanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseButton = new System.Windows.Forms.Button();
            this.Routes = new System.Windows.Forms.Label();
            this.EETHour = new System.Windows.Forms.Label();
            this.EETMin = new System.Windows.Forms.Label();
            this.Callsign = new System.Windows.Forms.Label();
            this.AircraftType = new System.Windows.Forms.Label();
            this.TrueSpeed = new System.Windows.Forms.Label();
            this.DeparturePoint = new System.Windows.Forms.Label();
            this.DepartureTime = new System.Windows.Forms.Label();
            this.CruiseAltitude = new System.Windows.Forms.Label();
            this.Alternate = new System.Windows.Forms.Label();
            this.FuelHours = new System.Windows.Forms.Label();
            this.FuelMinutes = new System.Windows.Forms.Label();
            this.PilotInfo = new System.Windows.Forms.Label();
            this.Person = new System.Windows.Forms.Label();
            this.Strips = new System.Windows.Forms.Label();
            this.Destination = new System.Windows.Forms.Label();
            this.VFR = new System.Windows.Forms.Label();
            this.IFR = new System.Windows.Forms.Label();
            this.LOCAL = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.CloseButton.FlatAppearance.BorderSize = 0;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.CloseButton.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.CloseButton.Location = new System.Drawing.Point(326, 411);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(125, 32);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.Text = "Close Flight Plan";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // Routes
            // 
            this.Routes.BackColor = System.Drawing.Color.Transparent;
            this.Routes.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.Routes.Location = new System.Drawing.Point(6, 153);
            this.Routes.Name = "Routes";
            this.Routes.Size = new System.Drawing.Size(539, 47);
            this.Routes.TabIndex = 1;
            this.Routes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EETHour
            // 
            this.EETHour.BackColor = System.Drawing.Color.Transparent;
            this.EETHour.Font = new System.Drawing.Font("Times New Roman", 18F);
            this.EETHour.Location = new System.Drawing.Point(211, 252);
            this.EETHour.Name = "EETHour";
            this.EETHour.Size = new System.Drawing.Size(39, 33);
            this.EETHour.TabIndex = 3;
            this.EETHour.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EETMin
            // 
            this.EETMin.BackColor = System.Drawing.Color.Transparent;
            this.EETMin.Font = new System.Drawing.Font("Times New Roman", 18F);
            this.EETMin.Location = new System.Drawing.Point(259, 252);
            this.EETMin.Name = "EETMin";
            this.EETMin.Size = new System.Drawing.Size(54, 33);
            this.EETMin.TabIndex = 4;
            this.EETMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Callsign
            // 
            this.Callsign.BackColor = System.Drawing.Color.Transparent;
            this.Callsign.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.Callsign.Location = new System.Drawing.Point(67, 55);
            this.Callsign.Name = "Callsign";
            this.Callsign.Size = new System.Drawing.Size(80, 85);
            this.Callsign.TabIndex = 5;
            this.Callsign.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AircraftType
            // 
            this.AircraftType.BackColor = System.Drawing.Color.Transparent;
            this.AircraftType.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.AircraftType.Location = new System.Drawing.Point(153, 55);
            this.AircraftType.Name = "AircraftType";
            this.AircraftType.Size = new System.Drawing.Size(97, 85);
            this.AircraftType.TabIndex = 6;
            this.AircraftType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TrueSpeed
            // 
            this.TrueSpeed.BackColor = System.Drawing.Color.Transparent;
            this.TrueSpeed.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.TrueSpeed.Location = new System.Drawing.Point(256, 55);
            this.TrueSpeed.Name = "TrueSpeed";
            this.TrueSpeed.Size = new System.Drawing.Size(57, 62);
            this.TrueSpeed.TabIndex = 7;
            this.TrueSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DeparturePoint
            // 
            this.DeparturePoint.BackColor = System.Drawing.Color.Transparent;
            this.DeparturePoint.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.DeparturePoint.Location = new System.Drawing.Point(319, 55);
            this.DeparturePoint.Name = "DeparturePoint";
            this.DeparturePoint.Size = new System.Drawing.Size(69, 85);
            this.DeparturePoint.TabIndex = 8;
            this.DeparturePoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DepartureTime
            // 
            this.DepartureTime.BackColor = System.Drawing.Color.Transparent;
            this.DepartureTime.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.DepartureTime.Location = new System.Drawing.Point(393, 70);
            this.DepartureTime.Name = "DepartureTime";
            this.DepartureTime.Size = new System.Drawing.Size(46, 70);
            this.DepartureTime.TabIndex = 9;
            this.DepartureTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CruiseAltitude
            // 
            this.CruiseAltitude.BackColor = System.Drawing.Color.Transparent;
            this.CruiseAltitude.Font = new System.Drawing.Font("Times New Roman", 13F);
            this.CruiseAltitude.Location = new System.Drawing.Point(494, 55);
            this.CruiseAltitude.Name = "CruiseAltitude";
            this.CruiseAltitude.Size = new System.Drawing.Size(57, 85);
            this.CruiseAltitude.TabIndex = 10;
            this.CruiseAltitude.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Alternate
            // 
            this.Alternate.BackColor = System.Drawing.Color.Transparent;
            this.Alternate.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.Alternate.Location = new System.Drawing.Point(127, 326);
            this.Alternate.Name = "Alternate";
            this.Alternate.Size = new System.Drawing.Size(76, 47);
            this.Alternate.TabIndex = 11;
            this.Alternate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FuelHours
            // 
            this.FuelHours.BackColor = System.Drawing.Color.Transparent;
            this.FuelHours.Font = new System.Drawing.Font("Times New Roman", 20F);
            this.FuelHours.Location = new System.Drawing.Point(8, 342);
            this.FuelHours.Name = "FuelHours";
            this.FuelHours.Size = new System.Drawing.Size(49, 46);
            this.FuelHours.TabIndex = 12;
            this.FuelHours.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FuelMinutes
            // 
            this.FuelMinutes.BackColor = System.Drawing.Color.Transparent;
            this.FuelMinutes.Font = new System.Drawing.Font("Times New Roman", 20F);
            this.FuelMinutes.Location = new System.Drawing.Point(65, 343);
            this.FuelMinutes.Name = "FuelMinutes";
            this.FuelMinutes.Size = new System.Drawing.Size(57, 46);
            this.FuelMinutes.TabIndex = 13;
            this.FuelMinutes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PilotInfo
            // 
            this.PilotInfo.BackColor = System.Drawing.Color.Transparent;
            this.PilotInfo.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.PilotInfo.Location = new System.Drawing.Point(209, 317);
            this.PilotInfo.Name = "PilotInfo";
            this.PilotInfo.Size = new System.Drawing.Size(281, 33);
            this.PilotInfo.TabIndex = 14;
            this.PilotInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Person
            // 
            this.Person.BackColor = System.Drawing.Color.Transparent;
            this.Person.Font = new System.Drawing.Font("Times New Roman", 18F);
            this.Person.Location = new System.Drawing.Point(494, 326);
            this.Person.Name = "Person";
            this.Person.Size = new System.Drawing.Size(58, 63);
            this.Person.TabIndex = 15;
            this.Person.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Strips
            // 
            this.Strips.BackColor = System.Drawing.Color.Transparent;
            this.Strips.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.Strips.Location = new System.Drawing.Point(9, 415);
            this.Strips.Name = "Strips";
            this.Strips.Size = new System.Drawing.Size(186, 33);
            this.Strips.TabIndex = 16;
            this.Strips.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Destination
            // 
            this.Destination.BackColor = System.Drawing.Color.Transparent;
            this.Destination.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.Destination.Location = new System.Drawing.Point(6, 215);
            this.Destination.Name = "Destination";
            this.Destination.Size = new System.Drawing.Size(197, 73);
            this.Destination.TabIndex = 2;
            this.Destination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VFR
            // 
            this.VFR.AutoSize = true;
            this.VFR.BackColor = System.Drawing.Color.Transparent;
            this.VFR.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.VFR.Location = new System.Drawing.Point(11, 63);
            this.VFR.Name = "VFR";
            this.VFR.Size = new System.Drawing.Size(23, 21);
            this.VFR.TabIndex = 17;
            this.VFR.Text = "X";
            this.VFR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.VFR.Visible = false;
            // 
            // IFR
            // 
            this.IFR.AutoSize = true;
            this.IFR.BackColor = System.Drawing.Color.Transparent;
            this.IFR.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.IFR.Location = new System.Drawing.Point(11, 93);
            this.IFR.Name = "IFR";
            this.IFR.Size = new System.Drawing.Size(23, 21);
            this.IFR.TabIndex = 18;
            this.IFR.Text = "X";
            this.IFR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.IFR.Visible = false;
            // 
            // LOCAL
            // 
            this.LOCAL.AutoSize = true;
            this.LOCAL.BackColor = System.Drawing.Color.Transparent;
            this.LOCAL.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.LOCAL.Location = new System.Drawing.Point(11, 120);
            this.LOCAL.Name = "LOCAL";
            this.LOCAL.Size = new System.Drawing.Size(23, 21);
            this.LOCAL.TabIndex = 19;
            this.LOCAL.Text = "X";
            this.LOCAL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LOCAL.Visible = false;
            // 
            // FlightPlanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightplan;
            this.ClientSize = new System.Drawing.Size(557, 455);
            this.Controls.Add(this.LOCAL);
            this.Controls.Add(this.IFR);
            this.Controls.Add(this.VFR);
            this.Controls.Add(this.Strips);
            this.Controls.Add(this.Person);
            this.Controls.Add(this.PilotInfo);
            this.Controls.Add(this.FuelMinutes);
            this.Controls.Add(this.FuelHours);
            this.Controls.Add(this.Alternate);
            this.Controls.Add(this.CruiseAltitude);
            this.Controls.Add(this.DepartureTime);
            this.Controls.Add(this.DeparturePoint);
            this.Controls.Add(this.TrueSpeed);
            this.Controls.Add(this.AircraftType);
            this.Controls.Add(this.Callsign);
            this.Controls.Add(this.EETMin);
            this.Controls.Add(this.EETHour);
            this.Controls.Add(this.Destination);
            this.Controls.Add(this.Routes);
            this.Controls.Add(this.CloseButton);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FlightPlanForm";
            this.Text = "FlightPlanForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label Routes;
        private System.Windows.Forms.Label EETHour;
        private System.Windows.Forms.Label EETMin;
        private System.Windows.Forms.Label Callsign;
        private System.Windows.Forms.Label AircraftType;
        private System.Windows.Forms.Label TrueSpeed;
        private System.Windows.Forms.Label DeparturePoint;
        private System.Windows.Forms.Label DepartureTime;
        private System.Windows.Forms.Label CruiseAltitude;
        private System.Windows.Forms.Label Alternate;
        private System.Windows.Forms.Label FuelHours;
        private System.Windows.Forms.Label FuelMinutes;
        private System.Windows.Forms.Label PilotInfo;
        private System.Windows.Forms.Label Person;
        private System.Windows.Forms.Label Strips;
        private System.Windows.Forms.Label Destination;
        private System.Windows.Forms.Label VFR;
        private System.Windows.Forms.Label IFR;
        private System.Windows.Forms.Label LOCAL;

    }
}