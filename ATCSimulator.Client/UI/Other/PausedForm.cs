﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SystemService;
using System.Configuration;

namespace ATCSimulator.Client.UI.Other
{
    public partial class PausedForm : Form
    {
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        public PausedForm()
        {
            InitializeComponent();
            this.TopMost = true;
            this.ShowInTaskbar = false;
            Rectangle resolution = Screen.FromControl(this).Bounds;
            this.Location = new Point { X = Convert.ToInt32(ConfigurationManager.AppSettings["X_Position"].ToString()) + ((1360 - this.Width) / 2), Y = ((768 - this.Height) / 2) };
        }
        public void SetMessage(string message)
        {
            MessageLabel.Text = message;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            StatusResponse disconnect = SystemConnection.DisconnectPC();
            Application.Exit();
        }
    }
}
