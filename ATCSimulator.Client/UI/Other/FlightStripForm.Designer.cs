﻿namespace ATCSimulator.Client.UI.Other
{
    partial class FlightStripForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ETA = new System.Windows.Forms.Label();
            this.Fuel = new System.Windows.Forms.Label();
            this.FlightType = new System.Windows.Forms.Label();
            this.Callsign = new System.Windows.Forms.Label();
            this.destination = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.route = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ETA
            // 
            this.ETA.BackColor = System.Drawing.Color.Transparent;
            this.ETA.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ETA.Location = new System.Drawing.Point(1, 1);
            this.ETA.Name = "ETA";
            this.ETA.Size = new System.Drawing.Size(169, 109);
            this.ETA.TabIndex = 0;
            this.ETA.Text = "ETA / Departure Time";
            this.ETA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Fuel
            // 
            this.Fuel.BackColor = System.Drawing.Color.Transparent;
            this.Fuel.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fuel.Location = new System.Drawing.Point(209, 1);
            this.Fuel.Name = "Fuel";
            this.Fuel.Size = new System.Drawing.Size(189, 196);
            this.Fuel.TabIndex = 1;
            this.Fuel.Text = "Fuel";
            this.Fuel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FlightType
            // 
            this.FlightType.BackColor = System.Drawing.Color.Transparent;
            this.FlightType.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FlightType.Location = new System.Drawing.Point(404, 98);
            this.FlightType.Name = "FlightType";
            this.FlightType.Size = new System.Drawing.Size(185, 35);
            this.FlightType.TabIndex = 2;
            this.FlightType.Text = "Model Aircraft";
            this.FlightType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Callsign
            // 
            this.Callsign.BackColor = System.Drawing.Color.Transparent;
            this.Callsign.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Callsign.Location = new System.Drawing.Point(404, 61);
            this.Callsign.Name = "Callsign";
            this.Callsign.Size = new System.Drawing.Size(185, 37);
            this.Callsign.TabIndex = 3;
            this.Callsign.Text = "Callsign";
            this.Callsign.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // destination
            // 
            this.destination.BackColor = System.Drawing.Color.Transparent;
            this.destination.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destination.Location = new System.Drawing.Point(595, 134);
            this.destination.Name = "destination";
            this.destination.Size = new System.Drawing.Size(58, 63);
            this.destination.TabIndex = 4;
            this.destination.Text = "Destination";
            this.destination.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.closeButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.closeButton.FlatAppearance.BorderSize = 0;
            this.closeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.closeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.closeButton.ForeColor = System.Drawing.Color.White;
            this.closeButton.Location = new System.Drawing.Point(964, 1);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(34, 35);
            this.closeButton.TabIndex = 80;
            this.closeButton.Text = "x";
            this.closeButton.UseVisualStyleBackColor = false;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // route
            // 
            this.route.BackColor = System.Drawing.Color.Transparent;
            this.route.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.route.Location = new System.Drawing.Point(847, 78);
            this.route.Name = "route";
            this.route.Size = new System.Drawing.Size(151, 32);
            this.route.TabIndex = 81;
            this.route.Text = "Route - Alternate Airport";
            this.route.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FlightStripForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.estrip_departure;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1000, 200);
            this.Controls.Add(this.route);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.destination);
            this.Controls.Add(this.Callsign);
            this.Controls.Add(this.FlightType);
            this.Controls.Add(this.Fuel);
            this.Controls.Add(this.ETA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FlightStripForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FlightStripPanel";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ETA;
        private System.Windows.Forms.Label Fuel;
        private System.Windows.Forms.Label FlightType;
        private System.Windows.Forms.Label Callsign;
        private System.Windows.Forms.Label destination;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Label route;




    }
}