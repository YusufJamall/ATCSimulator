﻿namespace ATCSimulator.Client.UI.Other
{
    partial class ReplayInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InfoPanel = new System.Windows.Forms.Panel();
            this.DetailBtn = new System.Windows.Forms.Button();
            this.LiveryImage = new System.Windows.Forms.PictureBox();
            this.StatusAircraft = new System.Windows.Forms.Label();
            this.TypeAircraft = new System.Windows.Forms.Label();
            this.DepartureTime = new System.Windows.Forms.Label();
            this.Callsign = new System.Windows.Forms.Label();
            this.Destination = new System.Windows.Forms.Label();
            this.Origin = new System.Windows.Forms.Label();
            this.InfoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LiveryImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // InfoPanel
            // 
            this.InfoPanel.BackColor = System.Drawing.Color.Transparent;
            this.InfoPanel.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.enable;
            this.InfoPanel.Controls.Add(this.DetailBtn);
            this.InfoPanel.Controls.Add(this.LiveryImage);
            this.InfoPanel.Controls.Add(this.StatusAircraft);
            this.InfoPanel.Controls.Add(this.TypeAircraft);
            this.InfoPanel.Controls.Add(this.DepartureTime);
            this.InfoPanel.Controls.Add(this.Callsign);
            this.InfoPanel.Controls.Add(this.Destination);
            this.InfoPanel.Controls.Add(this.Origin);
            this.InfoPanel.Location = new System.Drawing.Point(0, 0);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Size = new System.Drawing.Size(250, 80);
            this.InfoPanel.TabIndex = 10;
            // 
            // DetailBtn
            // 
            this.DetailBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.DetailBtn.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailBtn.ForeColor = System.Drawing.Color.White;
            this.DetailBtn.Location = new System.Drawing.Point(206, 51);
            this.DetailBtn.Name = "DetailBtn";
            this.DetailBtn.Size = new System.Drawing.Size(44, 29);
            this.DetailBtn.TabIndex = 17;
            this.DetailBtn.UseVisualStyleBackColor = false;
            this.DetailBtn.Click += new System.EventHandler(this.DetailBtn_Click);
            // 
            // LiveryImage
            // 
            this.LiveryImage.Location = new System.Drawing.Point(4, 5);
            this.LiveryImage.Name = "LiveryImage";
            this.LiveryImage.Size = new System.Drawing.Size(75, 45);
            this.LiveryImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LiveryImage.TabIndex = 16;
            this.LiveryImage.TabStop = false;
            // 
            // StatusAircraft
            // 
            this.StatusAircraft.AutoSize = true;
            this.StatusAircraft.BackColor = System.Drawing.Color.Transparent;
            this.StatusAircraft.Font = new System.Drawing.Font("Century Gothic", 7F);
            this.StatusAircraft.ForeColor = System.Drawing.Color.White;
            this.StatusAircraft.Location = new System.Drawing.Point(81, 64);
            this.StatusAircraft.Name = "StatusAircraft";
            this.StatusAircraft.Size = new System.Drawing.Size(47, 15);
            this.StatusAircraft.TabIndex = 15;
            this.StatusAircraft.Text = "Take off";
            this.StatusAircraft.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.StatusAircraft.Visible = false;
            // 
            // TypeAircraft
            // 
            this.TypeAircraft.BackColor = System.Drawing.Color.Transparent;
            this.TypeAircraft.Font = new System.Drawing.Font("Century Gothic", 7F);
            this.TypeAircraft.ForeColor = System.Drawing.Color.Tomato;
            this.TypeAircraft.Location = new System.Drawing.Point(81, 51);
            this.TypeAircraft.Name = "TypeAircraft";
            this.TypeAircraft.Size = new System.Drawing.Size(66, 15);
            this.TypeAircraft.TabIndex = 14;
            this.TypeAircraft.Text = "B737-400";
            this.TypeAircraft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DepartureTime
            // 
            this.DepartureTime.BackColor = System.Drawing.Color.Transparent;
            this.DepartureTime.Font = new System.Drawing.Font("Century Gothic", 7F);
            this.DepartureTime.ForeColor = System.Drawing.Color.Tomato;
            this.DepartureTime.Location = new System.Drawing.Point(144, 51);
            this.DepartureTime.Name = "DepartureTime";
            this.DepartureTime.Size = new System.Drawing.Size(68, 15);
            this.DepartureTime.TabIndex = 13;
            this.DepartureTime.Text = "08:00";
            this.DepartureTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Callsign
            // 
            this.Callsign.AutoSize = true;
            this.Callsign.BackColor = System.Drawing.Color.Transparent;
            this.Callsign.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.Callsign.ForeColor = System.Drawing.Color.White;
            this.Callsign.Location = new System.Drawing.Point(11, 57);
            this.Callsign.Name = "Callsign";
            this.Callsign.Size = new System.Drawing.Size(63, 16);
            this.Callsign.TabIndex = 12;
            this.Callsign.Text = "AWQ 2151";
            this.Callsign.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Destination
            // 
            this.Destination.BackColor = System.Drawing.Color.Transparent;
            this.Destination.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.Destination.ForeColor = System.Drawing.Color.White;
            this.Destination.Location = new System.Drawing.Point(81, 28);
            this.Destination.Name = "Destination";
            this.Destination.Size = new System.Drawing.Size(149, 21);
            this.Destination.TabIndex = 11;
            this.Destination.Text = "Seahorse (SASH)";
            this.Destination.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Origin
            // 
            this.Origin.BackColor = System.Drawing.Color.Transparent;
            this.Origin.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.Origin.ForeColor = System.Drawing.Color.White;
            this.Origin.Location = new System.Drawing.Point(81, 7);
            this.Origin.Name = "Origin";
            this.Origin.Size = new System.Drawing.Size(151, 17);
            this.Origin.TabIndex = 10;
            this.Origin.Text = "Tigerfort (SATF)";
            this.Origin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ReplayInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.flightstrip_down;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(250, 80);
            this.Controls.Add(this.InfoPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReplayInfoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "FlightStripPanel";
            this.InfoPanel.ResumeLayout(false);
            this.InfoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LiveryImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel InfoPanel;
        private System.Windows.Forms.PictureBox LiveryImage;
        private System.Windows.Forms.Label StatusAircraft;
        private System.Windows.Forms.Label TypeAircraft;
        private System.Windows.Forms.Label DepartureTime;
        private System.Windows.Forms.Label Callsign;
        private System.Windows.Forms.Label Destination;
        private System.Windows.Forms.Label Origin;
        private System.Windows.Forms.Button DetailBtn;
    }
}
