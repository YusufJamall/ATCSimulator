﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.SystemService;

namespace ATCSimulator.Client.UI.Other
{
    public partial class FlightPlanForm : Form
    {
        public FlightPlanForm()
        {
            InitializeComponent();
        }
        public FlightPlanForm(FlightPlanModel data)
        {
            InitializeComponent();
            Init(data);
        }
        public void Init(FlightPlanModel data)
        {
            VFR.Visible = false;
            LOCAL.Visible = false;
            IFR.Visible = false;
            if (data != null)
            {
                switch (data.flight_rules.ToUpper())
                {
                    case "IFR":
                        IFR.Visible = true;
                        break;
                    case "VFR":
                        VFR.Visible = true;
                        break;
                    case "LOCAL":
                        LOCAL.Visible = true;
                        break;
                }
                Callsign.Text = data.callsign;
                string aircraft_type = data.type_aircraft;
                if (!string.IsNullOrWhiteSpace(data.flight_type))
                    aircraft_type += "/" + data.flight_type;
                AircraftType.Text = aircraft_type;
                TrueSpeed.Text = data.true_speed > 0 ?  Math.Round(data.true_speed).ToString() : "";
                DeparturePoint.Text = data.origin_airport_code;
                if (data.departure_time != null)
                {
                    int hour_time = data.departure_time.Hours;
                    int minutes = data.departure_time.Minutes;
                    string departure_time = data.departure_time.Hours > 9 ? data.departure_time.Hours.ToString() : "0" + data.departure_time.Hours.ToString();
                    departure_time += data.departure_time.Minutes > 9 ? data.departure_time.Minutes.ToString() : "0" + data.departure_time.Minutes.ToString();
                    DepartureTime.Text = departure_time;
                }
                if (data.altitude > 0 && !string.IsNullOrWhiteSpace(data.flight_level))
                    CruiseAltitude.Text = data.flight_level + data.altitude.ToString();
                if (!string.IsNullOrWhiteSpace(data.origin_airport_code) &&
                    !string.IsNullOrWhiteSpace(data.destination_airport_code) &&
                    !string.IsNullOrWhiteSpace(data.routes))
                    Routes.Text = data.origin_airport_code + " " + data.routes + " " + data.destination_airport_code;
                if (data.flight_rules.ToLower().Equals("local"))
                    Routes.Visible = false;
                else
                    Routes.Visible = true;
                if (!string.IsNullOrWhiteSpace(data.destination_airport_name))
                    Destination.Text = data.destination_airport_name.ToUpper() + " AIRPORT";
                if (data.eet != null)
                {
                    EETHour.Text = data.eet.Hours.ToString();
                    EETMin.Text = data.eet.Minutes.ToString();
                }
                if (data.fuel_board != null)
                {
                    FuelHours.Text = data.fuel_board.Hours.ToString();
                    FuelMinutes.Text = data.fuel_board.Minutes.ToString();
                }
                Alternate.Text = data.alternate_airport_name;
                if (data.pilot != null)
                    PilotInfo.Text = string.Format("{0} {1} {2} {3}", data.pilot.name, data.pilot.address_city, data.pilot.phone, data.pilot.airport_city);
                Person.Text = data.person.ToString();
                Strips.Text = data.strips;
            }
        }

        public Bitmap GetCapturedForm()
        {
            CloseButton.Visible = false;
            Bitmap bitmap = new Bitmap(this.Width, this.Height);
            this.DrawToBitmap(bitmap, new Rectangle(0, 0, this.Width, this.Height));
            CloseButton.Visible = true;
            return bitmap;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void HideCloseBtn()
        {
            CloseButton.Visible = false;
        }
    }
}