﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.UI.Instructor.History;

namespace ATCSimulator.Client.UI.Instructor
{
    public partial class UserForm : Form
    {
        private bool editMode;
        private bool imageEdit;
        UserProfileModel userSelected;
        UserProfileModel userLogin;
        private bool _isStudent;


        public UserForm(UserProfileModel user)
        {
            userLogin = user;
            InitializeComponent();
            User_List.RowTemplate.Height = ((User_List.Height - User_List.ColumnHeadersHeight) / Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["Page_Size"])) + 1;
        }

        public void Init()
        {
            GetTheRole();
            ResetForm();
            typeSelect_Box.SelectedValue = 0;
        }

        public void GetTheRole()
        {
            try
            {
                UserRolesResponse roles = SystemConnection.GetUserRoles();
                if (roles.status.code == 0)//success
                {
                    Role_Box.Items.Clear();
                    typeSelect_Box.Items.Clear();
                    typeSelect_Box.Items.Add(new Telerik.WinControls.UI.RadListDataItem("All Roles", 0));
                    foreach (var role in roles.roles)
                    {
                        Role_Box.Items.Add(new Telerik.WinControls.UI.RadListDataItem(role.role_name, role.role_id));
                        typeSelect_Box.Items.Add(new Telerik.WinControls.UI.RadListDataItem(role.role_name + " Only", role.role_id));
                    }
                }
                else
                    MessageBox.Show(roles.status.message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR" + ex);
            }

        }
        public void AddListView(int page, int type)
        {
            List<DataUserList> users = new List<DataUserList>();
            try
            {
                UserListResponse userlist = SystemConnection.UserList(page, Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["Page_Size"]), type);
                if (userlist.status.code == 0)
                {
                    foreach (var user in userlist.users)
                    {
                        users.Add(new DataUserList
                        {
                            ID = user.ID,
                            user_ID = user.user_ID,
                            picture = user.photo != null ? HelperClient.byteArrayToImage(user.photo) : null,
                            user_identity_id = user.user_identity_id,
                            fullname = user.first_name + " " + user.last_name,
                            role_id = user.role.role_id,
                            role_name = user.role.role_name,
                            username = user.username,
                            first_name = user.first_name,
                            last_name = user.last_name,
                            gender = user.gender,
                            birthday = user.birthdate,
                            email = user.email,
                            phone = user.phone,
                            address = user.address,
                            is_student = user.isStudent,
                            @class = user.student_info.@class,
                            class_year = user.student_info.class_year.HasValue ? user.student_info.class_year.Value : 0,
                            profile = user
                        });
                    }

                }
                else
                {
                    MessageBox.Show(userlist.status.message);
                }

                User_List.DataSource = users;
                User_List.Columns["ID"].Visible = false;
                User_List.Columns["user_ID"].Visible = false;
                User_List.Columns["username"].Visible = false;
                User_List.Columns["first_name"].Visible = false;
                User_List.Columns["last_name"].Visible = false;
                User_List.Columns["gender"].Visible = false;
                User_List.Columns["birthday"].Visible = false;
                User_List.Columns["email"].Visible = false;
                User_List.Columns["phone"].Visible = false;
                User_List.Columns["address"].Visible = false;
                User_List.Columns["role_id"].Visible = false;
                User_List.Columns["is_student"].Visible = false;
                User_List.Columns["class"].Visible = false;
                User_List.Columns["class_year"].Visible = false;
                User_List.Columns["profile"].Visible = false;

                User_List.Columns["picture"].HeaderText = "Picture";
                User_List.Columns["user_identity_id"].HeaderText = "User ID";
                User_List.Columns["fullname"].HeaderText = "Full Name";
                User_List.Columns["role_name"].HeaderText = "Role";

                // Paging Logic
                int pageSize = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["Page_Size"]);
                int total_page = userlist.count % pageSize > 0 ? (userlist.count / pageSize) + 1 : userlist.count / pageSize;
                int first = total_page < 1 ? 0 : page == 1 ? 1 : ((page - 1) * pageSize) + 1;
                int last = first < 1 ? 0 : first + (userlist.users.Count - 1);


                GoToPage_Box.Items.Clear();
                GoToPage_Box.SelectedIndex = -1;
                for (int i = 1; i <= total_page; i++)
                {
                    GoToPage_Box.Items.Add(new Telerik.WinControls.UI.RadListDataItem(i.ToString(), i));
                }

                Page_text.Text = page.ToString();
                Total_Page_lbl.Text = "Displaying  " + first + " - " + last + " from " + userlist.count + " result";

                // control page button enable/ disable
                // remove page if result is 0
                page = total_page < 1 ? 0 : page;
                if (page == total_page)
                {
                    if (page == 1)
                    {
                        Next_Btn.Enabled = false;
                        Prev_Btn.Enabled = false;
                    }
                    else if (total_page < 1 || page < 1)
                    {
                        Next_Btn.Enabled = false;
                        Prev_Btn.Enabled = false;
                    }
                    else
                    {
                        Next_Btn.Enabled = false;
                        Prev_Btn.Enabled = true;
                    }
                }
                else if (page == 1)
                {
                    Next_Btn.Enabled = true;
                    Prev_Btn.Enabled = false;
                }
                else
                {
                    Next_Btn.Enabled = true;
                    Prev_Btn.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR" + ex);
            }
        }


        private void GetViewMapData(DataUserList profile)
        {
            if(profile.username.ToLower().Equals(userLogin.username.ToLower()))
            {
                Role_Box.Enabled = false;
            }
            imageEdit = false;
            Image_Photo.Image = profile.picture != null ? profile.picture : global::ATCSimulator.Client.Properties.Resources.photo_img;
            ID_text.Text = profile.user_ID.ToString();
            Username.Text = profile.username;
            FirstName_text.Text = profile.first_name;
            LastName_text.Text = profile.last_name;
            UserID_Text.Text = profile.user_identity_id;
            Birthdate_User.Value = profile.birthday.HasValue ? profile.birthday.Value : DateTime.Now;
            email_text.Text = profile.email;
            phone_text.Text = profile.phone;
            address_text.Text = profile.address;

            Role_Box.SelectedValue = profile.role_id;
            if (profile.gender.ToLower() == "female")
                Gender_box.SelectedIndex = 1;
            else
                Gender_box.SelectedIndex = 0;
            if (profile.is_student)
            {
                IfStudent(true);
                Class_text.Text = profile.@class;
                ClassYear_text.Text = profile.class_year >= 0 ? profile.class_year.ToString() : "";
            }
            else
            {
                IfStudent(false);
            }
        }

        private void ResetPassword()
        {
            try
            {
                StatusResponse response = SystemConnection.ResetPassword(Convert.ToInt32(ID_text.Text));
                if (response.status.code == 0)
                {
                    MessageBox.Show("User " + Username.Text + " Password Has Changed to His Currentlly User ID");
                }
                else
                {
                    MessageBox.Show("Failed To Reset Password");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }
        private void SeeHistory()
        {
            this.Enabled = false;
            HistoryPlayerForm historyPlayerForm = new HistoryPlayerForm();
            historyPlayerForm.Init(null, this, userSelected);
            historyPlayerForm.StartPosition = FormStartPosition.CenterParent;
            historyPlayerForm.ShowDialog();
        }
        private void DeleteData()
        {
            if (Username.Text.ToLower() != userLogin.username.ToLower())
            {
                try
                {
                    StatusResponse response = SystemConnection.UserRemove(Convert.ToInt32(ID_text.Text));
                    if (response.status.code == 0)
                    {
                        MessageBox.Show("User " + Username.Text + " is Deleted");
                        ResetForm();
                        if (typeSelect_Box.SelectedValue.Equals(0))
                            AddListView(1, Convert.ToInt32(typeSelect_Box.SelectedValue));
                        else
                            typeSelect_Box.SelectedValue = 0;
                    }
                    else
                    {
                        MessageBox.Show("Failed To Delete");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error : " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("You can't delete account that currently used");
            }
        }
        private void SaveData()
        {
            try
            {
                UserRegisterContract newUser = new UserRegisterContract
                {
                    username = Username.Text,
                    password = Password.Text,
                    role_id = Convert.ToInt32(Role_Box.SelectedItem.Value),
                    role_name = Role_Box.SelectedItem.Text,
                    profile = new UserProfileContract
                    {
                        first_name = FirstName_text.Text,
                        last_name = LastName_text.Text,
                        user_identity_id = UserID_Text.Text,
                        gender = Gender_box.SelectedItem.Text,
                        birthdate = Birthdate_User.Value,
                        email = email_text.Text,
                        address = address_text.Text,
                        create_by = userLogin.username,
                        isStudent = Class_text.Visible ? true : false,
                        phone = phone_text.Text
                    }
                };
                if (imageEdit)
                    newUser.profile.photo = HelperClient.imageToByteArray(Image_Photo.Image);
                else
                    newUser.profile.photo = HelperClient.getResizedImage(Image_Photo.Image, 100, 100);
                if (newUser.profile.isStudent)
                {
                    newUser.profile.student_info = new StudentInfoContract
                    {
                        @class = Class_text.Text

                    };
                    if (!string.IsNullOrWhiteSpace(ClassYear_text.Text))
                    {
                        newUser.profile.student_info.class_year = Convert.ToInt32(ClassYear_text.Text);
                    }
                }
                StatusResponse response = SystemConnection.UserRegister(newUser);
                if (response.status.code == 0)
                {
                    MessageBox.Show("Your Data is Saved");
                    ResetForm();
                    if (typeSelect_Box.SelectedValue.Equals(0))
                        AddListView(1, Convert.ToInt32(typeSelect_Box.SelectedValue));
                    else
                        typeSelect_Box.SelectedValue = 0;
                }
                else
                {
                    MessageBox.Show("Failed To Save");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }
        private void UpdateData()
        {
            try
            {
                UserUpdateContract user = new UserUpdateContract
                {
                    user_id = Convert.ToInt32(ID_text.Text),
                    username = Username.Text,
                    role_id = Convert.ToInt32(Role_Box.SelectedItem.Value),
                    role_name = Role_Box.SelectedItem.Text,
                    profile = new UserProfileContract
                    {
                        first_name = FirstName_text.Text,
                        last_name = LastName_text.Text,
                        user_identity_id = UserID_Text.Text,
                        gender = Gender_box.SelectedItem.Text,
                        birthdate = Birthdate_User.Value,
                        email = email_text.Text,
                        address = address_text.Text,
                        create_by = userLogin.username,
                        isStudent = Class_text.Visible ? true : false,
                        phone = phone_text.Text
                    }
                };
                if (imageEdit)
                    user.profile.photo = HelperClient.imageToByteArray(Image_Photo.Image);
                if (user.profile.isStudent)
                {
                    user.profile.student_info = new StudentInfoContract
                    {
                        @class = Class_text.Text

                    };
                    if (!string.IsNullOrWhiteSpace(ClassYear_text.Text))
                    {
                        user.profile.student_info.class_year = Convert.ToInt32(ClassYear_text.Text);
                    }
                }
                StatusResponse response = SystemConnection.UserUpdate(user);
                if (response.status.code == 0)
                {
                    MessageBox.Show("Your Data is Updated");
                    // update current user profile
                    if(user.username.ToLower().Equals(userLogin.username.ToLower()))
                    {
                        UI.MainForm.UpdateProfile(new UserProfileModel
                        {
                            ID = userLogin.ID,
                            user_ID = userLogin.user_ID,
                            address = user.profile.address,
                            birthdate = user.profile.birthdate,
                            email = user.profile.email,
                            first_name = user.profile.first_name,
                            last_name = user.profile.last_name,
                            gender = user.profile.gender,
                            phone = user.profile.phone,
                            photo = user.profile.photo,
                            role = userLogin.role,
                            create_by = userLogin.create_by,
                            create_date = userLogin.create_date,
                            isStudent = user.profile.isStudent,
                            update_by = userLogin.username,
                            update_date = DateTime.Now,
                            user_identity_id = user.profile.user_identity_id,
                            username = user.username
                        });
                    }

                    ResetForm();
                    if (typeSelect_Box.SelectedValue.Equals(0))
                        AddListView(1, Convert.ToInt32(typeSelect_Box.SelectedValue));
                    else
                        typeSelect_Box.SelectedValue = 0;
                }
                else
                {
                    MessageBox.Show("Failed To Update");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }
        #region ButtonHandler

        private void UserListContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (editMode)
            {
                if (CancelOperation() == false)
                {
                    return;
                }
            }
            if (e.RowIndex != -1)
            {
                DataGridView obj = (DataGridView)sender;
                DataUserList data = (DataUserList)obj.Rows[e.RowIndex].DataBoundItem;
                userSelected = data.profile;
                EditingOperation(false);
                reset_password_btn.Visible = true;
                history_btn.Visible = true;
                Username.Enabled = false;
                Password_lbl.Visible = false;
                Password.Visible = false;

                Save_Btn.Visible = true;
                Save_Btn.Enabled = false;
                Update_Btn.Visible = false;
                Cancel_Btn.Enabled = false;
                GetViewMapData(data);
            }
        }
        private void User_List_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (editMode)
            {
                if (CancelOperation() == false)
                {
                    return;
                }
            }
            if (e.RowIndex == -1)
            {
                return;
            }
            DataGridView obj = (DataGridView)sender;
            DataUserList data = (DataUserList)obj.Rows[e.RowIndex].DataBoundItem;
            EditingOperation(true);
            reset_password_btn.Visible = true;
            history_btn.Visible = true;
            Username.Enabled = false;
            Password_lbl.Visible = false;
            Password.Visible = false;

            Save_Btn.Visible = false;
            Update_Btn.Visible = true;
            Cancel_Btn.Enabled = true;
            GetViewMapData(data);
        }
        private void ButtonOnHover(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.ForeColor = Color.White;
        }

        private void ButtonOnLeave(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.ForeColor = Color.DodgerBlue;
        }
        private void Next_Btn_MouseHover(object sender, EventArgs e)
        {
            this.Next_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_next_hover;
        }

        private void Next_Btn_MouseLeave(object sender, EventArgs e)
        {
            this.Next_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_next;
        }

        private void Prev_Btn_MouseHover(object sender, EventArgs e)
        {
            this.Prev_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_prev_hover;
        }

        private void Prev_Btn_MouseLeave(object sender, EventArgs e)
        {
            this.Prev_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_prev;
        }
        private void ButtonOnClick(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            switch (btn.Name)
            {
                case "Create_Btn":
                    if (editMode)
                    {
                        if (CancelOperation() == false)
                        {
                            break;
                        }
                    }
                    ResetForm();
                    EditingOperation(true);
                    Save_Btn.Enabled = true;
                    Update_Btn.Visible = false;
                    Cancel_Btn.Enabled = true;
                    break;
                case "Remove_Btn":
                    if (editMode)
                    {
                        if (CancelOperation() == false)
                        {
                            break;
                        }
                        ResetForm();
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(Username.Text) || string.IsNullOrWhiteSpace(ID_text.Text))
                        {
                            MessageBox.Show("You're not choosing any data to remove, please select fisrt");
                        }
                        else
                        {
                            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Are you sure to delete user " + Username.Text
                                , "Delete Confirmation", MessageBoxButtons.YesNo);
                            if (dialogResult == DialogResult.Yes)  // error is here
                            {
                                DeleteData();
                                ResetForm();
                            }
                        }
                    }
                    break;
                case "Cancel_Btn":
                    ResetForm();
                    break;
                case "Save_Btn":
                    if (ValidateData())
                    {
                        SaveData();
                    }
                    break;
                case "Update_Btn":
                    if (ValidateData())
                    {
                        UpdateData();
                    }
                    break;
            }
        }
        private void ImageOnClick(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)|*.jpg; *.jpeg; *.png; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                // resize image
                byte[] byte_photo = HelperClient.getResizedImage(open.FileName, 100, 100);
                Image photo = HelperClient.byteArrayToImage(byte_photo);
                // display image in picture box
                Image_Photo.Image = photo;
                Image_Photo.SizeMode = PictureBoxSizeMode.Zoom;
                imageEdit = true;
            }
        }

        private void ImageOnHover(object sender, EventArgs e)
        {
            label_edit_picture.Visible = true;
        }

        private void ImageOnLeave(object sender, EventArgs e)
        {
            label_edit_picture.Visible = false;
        }
        private void Role_Box_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (Role_Box.SelectedIndex > 0)
            {
                if (Role_Box.SelectedItem.Text.ToLower().Equals("student"))
                    IfStudent(true);
                else
                    IfStudent(false);
            }
            else
                IfStudent(false);

        }
        private void GoToPage_Box_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (GoToPage_Box.SelectedIndex >= 0)
                AddListView(Convert.ToInt32(GoToPage_Box.SelectedValue.ToString()), Convert.ToInt32(typeSelect_Box.SelectedValue));
        }

        private void Next_Btn_Click(object sender, EventArgs e)
        {
            AddListView(Convert.ToInt32(Page_text.Text) + 1, Convert.ToInt32(typeSelect_Box.SelectedValue));
        }

        private void Prev_Btn_Click(object sender, EventArgs e)
        {
            AddListView(Convert.ToInt32(Page_text.Text) - 1, Convert.ToInt32(typeSelect_Box.SelectedValue));
        }

        private void typeSelect_Box_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            AddListView(1, Convert.ToInt32(typeSelect_Box.SelectedValue));
        }

        private void reset_password_btn_Click(object sender, EventArgs e)
        {
            ResetPassword();
        }
        private void history_btn_Click(object sender, EventArgs e)
        {
            SeeHistory();
        }

        #endregion

        #region Helper
        public void ClearProperties()
        {
            Username.Text = "";
            Password.Text = "";
            FirstName_text.Text = "";
            LastName_text.Text = "";
            UserID_Text.Text = "";
            Gender_box.SelectedIndex = -1;
            Role_Box.SelectedIndex = -1;
            email_text.Text = "";
            Birthdate_User.Value = new DateTime();
            phone_text.Text = "";
            address_text.Text = "";
            Image_Photo.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            imageEdit = false;
            IfStudent(false);
        }
        private void EditingOperation(bool status)
        {
            Username.Enabled = status;
            Password.Enabled = status;
            FirstName_text.Enabled = status;
            LastName_text.Enabled = status;
            UserID_Text.Enabled = status;
            email_text.Enabled = status;
            phone_text.Enabled = status;
            address_text.Enabled = status;
            Birthdate_User.Enabled = status;
            Role_Box.Enabled = status;
            Gender_box.Enabled = status;
            Image_Photo.Enabled = status;
            Class_text.Enabled = status;
            ClassYear_text.Enabled = status;
            editMode = status;
            Remove_Btn.Enabled = status == true ? false : true;
            reset_password_btn.Enabled = status == true ? false : true;
            history_btn.Enabled = status == true ? false : true;
        }

        private void ResetForm()
        {
            ClearProperties();
            reset_password_btn.Visible = false;
            history_btn.Visible = false;
            Username.Enabled = true;
            Password_lbl.Visible = true;
            Password.Visible = true;
            EditingOperation(false);
            Save_Btn.Visible = true;
            Save_Btn.Enabled = false;
            Update_Btn.Visible = false;
            Cancel_Btn.Enabled = false;
            Remove_Btn.Enabled = false;
        }
        private bool CancelOperation()
        {
            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("You are in the middle of editing/inputing, " +
                    "Are you sure to cancel your operation?", "Cancel Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)  // error is here
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void IfStudent(bool value)
        {
            _isStudent = value;
            Class_lbl.Visible = value;
            ClassYear_lbl.Visible = value;
            Class_text.Visible = value;
            ClassYear_text.Visible = value;
        }

        private bool ValidateData()
        {
            if (string.IsNullOrWhiteSpace(Username.Text))
            {
                MessageBox.Show("Username cannot be empty");
                return false;
            }
            if (Password.Visible)
            {
                if (string.IsNullOrWhiteSpace(Password.Text) || Password.Text.Length < 6)
                {
                    MessageBox.Show("Password cannot be empty and must more than 6 character");
                    return false;
                }
            }
            if (Role_Box.SelectedIndex < 0)
            {
                MessageBox.Show("Role cannot be empty please choose one");
                return false;
            }
            if (string.IsNullOrWhiteSpace(UserID_Text.Text))
            {
                MessageBox.Show("User ID cannot be empty please choose one");
                return false;
            }
            if (Gender_box.SelectedIndex < 0)
            {
                MessageBox.Show("Gender cannot be empty please choose one");
                return false;
            }
            if (string.IsNullOrWhiteSpace(Birthdate_User.Text))
            {
                MessageBox.Show("Birthday Date cannot be empty please choose one");
                return false;
            }
            if (string.IsNullOrWhiteSpace(FirstName_text.Text))
            {
                MessageBox.Show("First Name Cannot be empty");
                return false;
            }
            if (_isStudent)
            {
                if (string.IsNullOrWhiteSpace(ClassYear_text.Text)) { }
                else if (!System.Text.RegularExpressions.Regex.IsMatch(ClassYear_text.Text, @"^[0-9]+$"))
                {
                    MessageBox.Show("Class Year Must be contain number only");
                    return false;
                }
            }
            return true;
        }

        #endregion

    }
}
