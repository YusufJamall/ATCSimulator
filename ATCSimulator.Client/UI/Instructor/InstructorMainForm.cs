﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATCSimulator.Client.UI.Instructor.Exercise;
using ATCSimulator.Client.UI.Instructor.Simulation;
using ATCSimulator.Client.UI.Instructor.History;
using ATCSimulator.Client.SystemService;

namespace ATCSimulator.Client.UI.Instructor
{
    public partial class InstructorMainForm : Form
    {
        enum ViewMenu
        {
            Simulation,
            Exercise,
            PCControl,
            History,
            User,
            None
        }
        private bool firstTime;
        private UserProfileModel user;
        ViewMenu menu_panel = ViewMenu.None;
        Dictionary<Label, bool> Header_Button;
        List<Label> List_Header_Button;
        PCControlForm pccontrol_panel;
        UserForm user_panel;
        ExerciseForm exercise_panel;
        SimulationForm simulation_panel;
        HistoryForm history_panel;
        MainForm mainform;


        public InstructorMainForm(UserProfileModel user, MainForm mainform)
        {
            InitializeComponent();
            this.user = user;
            firstTime = true;
            this.mainform = mainform;
        }

        public void Init()
        {
            if (firstTime)
            {
                Header_Button = new Dictionary<Label, bool>();
                Header_Button.Add(PCControlBtn, false);
                Header_Button.Add(user_btn, false);
                Header_Button.Add(exercise_btn, false);
                Header_Button.Add(simulation_btn, false);
                Header_Button.Add(history_btn, false);

                List_Header_Button = new List<Label>()
            {
                PCControlBtn,
                user_btn,
                exercise_btn,
                simulation_btn,
                history_btn
            };

                pccontrol_panel = new PCControlForm();
                user_panel = new UserForm(user);
                exercise_panel = new ExerciseForm(user.username);
                simulation_panel = new SimulationForm(user.username);
                history_panel = new HistoryForm(user.username,mainform);

                user_panel.TopLevel = false;
                pccontrol_panel.TopLevel = false;
                exercise_panel.TopLevel = false;
                simulation_panel.TopLevel = false;
                history_panel.TopLevel = false;
                firstTime = false;
            }

            ShowViewMenu(ViewMenu.PCControl);
            SetMainButton(PCControlBtn);


            supervisor_panel.Controls.Add(user_panel);
            supervisor_panel.Controls.Add(pccontrol_panel);
            supervisor_panel.Controls.Add(exercise_panel);
            supervisor_panel.Controls.Add(simulation_panel);
            supervisor_panel.Controls.Add(history_panel);
        }

        private void SetMainButton(Label label)
        {
            foreach (Label p in List_Header_Button)
            {
                if (p != label)
                {
                    Header_Button[p] = false;
                    p.ForeColor = Color.White;
                }
                else
                {
                    Header_Button[p] = true;
                    p.ForeColor = Color.DodgerBlue;
                }
            }
        }

        private void ShowViewMenu(ViewMenu menu)
        {
            switch (menu)
            {
                case ViewMenu.User:
                    if (menu_panel != ViewMenu.User)
                    {
                        HideViewMenu(menu_panel);
                        user_panel.Show();
                        user_panel.Init();
                        user_panel.Dock = DockStyle.Top;
                        user_panel.BringToFront();
                        menu_panel = ViewMenu.User;
                    }
                    break;
                case ViewMenu.PCControl:
                    if (menu_panel != ViewMenu.PCControl)
                    {
                        HideViewMenu(menu_panel);
                        pccontrol_panel.Show();
                        pccontrol_panel.Init();
                        pccontrol_panel.Dock = DockStyle.Top;
                        pccontrol_panel.BringToFront();
                        menu_panel = ViewMenu.PCControl;
                    }
                    break;
                case ViewMenu.Exercise:
                    if (menu_panel != ViewMenu.Exercise)
                    {
                        HideViewMenu(menu_panel);
                        exercise_panel.Show();
                        exercise_panel.Init();
                        exercise_panel.Dock = DockStyle.Top;
                        exercise_panel.BringToFront();
                        menu_panel = ViewMenu.Exercise;
                    }
                    break;
                case ViewMenu.Simulation:
                    if (menu_panel != ViewMenu.Simulation)
                    {
                        HideViewMenu(menu_panel);
                        simulation_panel.Show();
                        simulation_panel.Init();
                        simulation_panel.Dock = DockStyle.Top;
                        simulation_panel.BringToFront();
                        menu_panel = ViewMenu.Simulation;
                    }
                    break;
                case ViewMenu.History:
                    if (menu_panel != ViewMenu.History)
                    {
                        HideViewMenu(menu_panel);
                        history_panel.Show();
                        history_panel.Init();
                        history_panel.Dock = DockStyle.Top;
                        history_panel.BringToFront();
                        menu_panel = ViewMenu.History;
                    }
                    break;
            }
        }

        private void HideViewMenu(ViewMenu menu)
        {
            switch (menu)
            {
                case ViewMenu.User:
                    user_panel.Hide();
                    user_panel.SendToBack();
                    break;
                case ViewMenu.PCControl:
                    pccontrol_panel.Hide();
                    pccontrol_panel.SendToBack();
                    break;
                case ViewMenu.Exercise:
                    exercise_panel.Hide();
                    exercise_panel.SendToBack();
                    break;
                case ViewMenu.Simulation:
                    simulation_panel.Hide();
                    simulation_panel.SendToBack();
                    break;
                case ViewMenu.History:
                    history_panel.Hide();
                    history_panel.SendToBack();
                    break;
            }
        }

        #region Button Handler
        private void ButtonOnHover(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.ForeColor = Color.DodgerBlue;
        }

        private void ButtonOnLeave(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            if (Header_Button[btn] == false)
                btn.ForeColor = Color.White;

        }
        private void ButtonOnClick(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            SetMainButton(btn);

            switch (btn.Name)
            {
                case "user_btn":
                    ShowViewMenu(ViewMenu.User);
                    break;
                case "PCControlBtn":
                    ShowViewMenu(ViewMenu.PCControl);
                    break;
                case "exercise_btn":
                    ShowViewMenu(ViewMenu.Exercise);
                    break;
                case "simulation_btn":
                    ShowViewMenu(ViewMenu.Simulation);
                    break;
                case "history_btn":
                    ShowViewMenu(ViewMenu.History);
                    break;
            }
        }
        #endregion

        #region Callback
        public void ConnectPC(ATCSimulator.Client.SystemService.UserComputerInfo user)
        {
            try
            {
                if (menu_panel == ViewMenu.PCControl)
                {
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(() =>
                        {
                            pccontrol_panel.ConnectPC(user);
                        }));
                    }
                    else
                    {
                        pccontrol_panel.ConnectPC(user);
                    }
                }
                else if (menu_panel == ViewMenu.Simulation)
                {
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(() =>
                        {
                            simulation_panel.Init();
                        }));
                    }
                    else
                    {
                        simulation_panel.Init();
                    }
                }
            }
            catch { }
        }
        public void ConnectUpdate()
        {
            try
            {
                if (menu_panel == ViewMenu.Simulation)
                {
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(() =>
                        {
                            simulation_panel.Init();
                        }));
                    }
                    else
                    {
                        simulation_panel.Init();
                    }
                }
            }
            catch { }
        }
        #endregion
    }
}
