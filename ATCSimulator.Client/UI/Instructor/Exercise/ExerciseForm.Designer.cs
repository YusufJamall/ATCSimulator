﻿namespace ATCSimulator.Client.UI.Instructor.Exercise
{
    partial class ExerciseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EnvPropGroup = new System.Windows.Forms.GroupBox();
            this.EnvTime = new Telerik.WinControls.UI.RadTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Visibility = new Telerik.WinControls.UI.RadSpinEditor();
            this.Temperature = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindSpeed = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindDirection = new Telerik.WinControls.UI.RadSpinEditor();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Weather = new Telerik.WinControls.UI.RadDropDownList();
            this.label33 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ExerciseName = new Telerik.WinControls.UI.RadTextBox();
            this.TrafficExercise = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DescExercise = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LightsGroup = new System.Windows.Forms.GroupBox();
            this.ApproachGroup = new System.Windows.Forms.GroupBox();
            this.TaxiGroup = new System.Windows.Forms.GroupBox();
            this.PapiLight = new System.Windows.Forms.CheckBox();
            this.TaxiLight = new System.Windows.Forms.CheckBox();
            this.RunwayGroup = new System.Windows.Forms.GroupBox();
            this.SaveExerciseBtn = new System.Windows.Forms.Button();
            this.RemoveExerciseBtn = new System.Windows.Forms.Button();
            this.ClearExerciseBtn = new System.Windows.Forms.Button();
            this.AircraftPropGroup = new System.Windows.Forms.GroupBox();
            this.AddAircraftBtn = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.AircraftList = new System.Windows.Forms.DataGridView();
            this.ArrivalCount = new System.Windows.Forms.Label();
            this.DepartureCount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ExerciseTable = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.SceneryList = new Telerik.WinControls.UI.RadDropDownList();
            this.CreateExercise = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.PlayTimeHour = new Telerik.WinControls.UI.RadSpinEditor();
            this.label15 = new System.Windows.Forms.Label();
            this.PlayTimeMin = new Telerik.WinControls.UI.RadSpinEditor();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.EnvPropGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExerciseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficExercise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescExercise)).BeginInit();
            this.LightsGroup.SuspendLayout();
            this.TaxiGroup.SuspendLayout();
            this.AircraftPropGroup.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExerciseTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SceneryList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayTimeHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayTimeMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // EnvPropGroup
            // 
            this.EnvPropGroup.Controls.Add(this.EnvTime);
            this.EnvPropGroup.Controls.Add(this.label12);
            this.EnvPropGroup.Controls.Add(this.label11);
            this.EnvPropGroup.Controls.Add(this.Visibility);
            this.EnvPropGroup.Controls.Add(this.Temperature);
            this.EnvPropGroup.Controls.Add(this.WindSpeed);
            this.EnvPropGroup.Controls.Add(this.WindDirection);
            this.EnvPropGroup.Controls.Add(this.label10);
            this.EnvPropGroup.Controls.Add(this.label9);
            this.EnvPropGroup.Controls.Add(this.label8);
            this.EnvPropGroup.Controls.Add(this.Weather);
            this.EnvPropGroup.Controls.Add(this.label33);
            this.EnvPropGroup.Controls.Add(this.label43);
            this.EnvPropGroup.Controls.Add(this.label44);
            this.EnvPropGroup.Controls.Add(this.label45);
            this.EnvPropGroup.Controls.Add(this.label49);
            this.EnvPropGroup.Controls.Add(this.label50);
            this.EnvPropGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.EnvPropGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.EnvPropGroup.Location = new System.Drawing.Point(881, 372);
            this.EnvPropGroup.Name = "EnvPropGroup";
            this.EnvPropGroup.Size = new System.Drawing.Size(331, 259);
            this.EnvPropGroup.TabIndex = 84;
            this.EnvPropGroup.TabStop = false;
            this.EnvPropGroup.Text = "Environment Properties";
            // 
            // EnvTime
            // 
            this.EnvTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.EnvTime.Location = new System.Drawing.Point(199, 219);
            this.EnvTime.Name = "EnvTime";
            this.EnvTime.Size = new System.Drawing.Size(122, 24);
            this.EnvTime.Step = 30;
            this.EnvTime.TabIndex = 12;
            this.EnvTime.TabStop = false;
            this.EnvTime.Text = "radTimePicker1";
            this.EnvTime.TimeTables = Telerik.WinControls.UI.TimeTables.HoursAndMinutesInOneTable;
            this.EnvTime.Value = new System.DateTime(2014, 8, 16, 11, 30, 4, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(299, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 17);
            this.label12.TabIndex = 118;
            this.label12.Text = "m";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(299, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 117;
            this.label11.Text = "o";
            // 
            // Visibility
            // 
            this.Visibility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Visibility.Location = new System.Drawing.Point(199, 182);
            this.Visibility.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.Visibility.Name = "Visibility";
            this.Visibility.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.Visibility.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Visibility.Size = new System.Drawing.Size(100, 24);
            this.Visibility.TabIndex = 11;
            this.Visibility.TabStop = false;
            this.Visibility.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Visibility.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // Temperature
            // 
            this.Temperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Temperature.Location = new System.Drawing.Point(199, 146);
            this.Temperature.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Temperature.Name = "Temperature";
            this.Temperature.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.Temperature.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Temperature.Size = new System.Drawing.Size(100, 24);
            this.Temperature.TabIndex = 10;
            this.Temperature.TabStop = false;
            this.Temperature.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Temperature.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // WindSpeed
            // 
            this.WindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindSpeed.Location = new System.Drawing.Point(199, 111);
            this.WindSpeed.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.WindSpeed.Name = "WindSpeed";
            this.WindSpeed.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.WindSpeed.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindSpeed.Size = new System.Drawing.Size(100, 24);
            this.WindSpeed.TabIndex = 9;
            this.WindSpeed.TabStop = false;
            this.WindSpeed.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // WindDirection
            // 
            this.WindDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindDirection.Location = new System.Drawing.Point(199, 76);
            this.WindDirection.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.WindDirection.Name = "WindDirection";
            this.WindDirection.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.WindDirection.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindDirection.Size = new System.Drawing.Size(100, 24);
            this.WindDirection.TabIndex = 8;
            this.WindDirection.TabStop = false;
            this.WindDirection.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindDirection.Value = new decimal(new int[] {
            359,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(304, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 112;
            this.label10.Text = "C";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(299, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 17);
            this.label9.TabIndex = 111;
            this.label9.Text = "kn";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(299, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 109;
            this.label8.Text = "o";
            // 
            // Weather
            // 
            this.Weather.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Weather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Weather.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Weather.Location = new System.Drawing.Point(199, 37);
            this.Weather.Name = "Weather";
            this.Weather.Size = new System.Drawing.Size(124, 24);
            this.Weather.TabIndex = 7;
            this.Weather.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.Weather_SelectedIndexChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(8, 219);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(43, 20);
            this.label33.TabIndex = 24;
            this.label33.Text = "Time";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(8, 183);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(64, 20);
            this.label43.TabIndex = 14;
            this.label43.Text = "Visibility";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(8, 146);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(100, 20);
            this.label44.TabIndex = 12;
            this.label44.Text = "Temperature";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(8, 76);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(112, 20);
            this.label45.TabIndex = 9;
            this.label45.Text = "Wind Direction";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(8, 37);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(70, 20);
            this.label49.TabIndex = 7;
            this.label49.Text = "Weather";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(8, 114);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(96, 20);
            this.label50.TabIndex = 2;
            this.label50.Text = "Wind Speed";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(3, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 24);
            this.label1.TabIndex = 87;
            this.label1.Text = "Exercise List";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(874, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 20);
            this.label2.TabIndex = 90;
            this.label2.Text = "Exercise Name";
            // 
            // ExerciseName
            // 
            this.ExerciseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ExerciseName.Location = new System.Drawing.Point(878, 108);
            this.ExerciseName.Name = "ExerciseName";
            this.ExerciseName.NullText = "Exercise Name";
            this.ExerciseName.Size = new System.Drawing.Size(334, 24);
            this.ExerciseName.TabIndex = 4;
            // 
            // TrafficExercise
            // 
            this.TrafficExercise.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TrafficExercise.Location = new System.Drawing.Point(878, 160);
            this.TrafficExercise.Name = "TrafficExercise";
            this.TrafficExercise.NullText = "Traffic";
            this.TrafficExercise.Size = new System.Drawing.Size(334, 24);
            this.TrafficExercise.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(874, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 20);
            this.label3.TabIndex = 92;
            this.label3.Text = "Traffic Exercise";
            // 
            // DescExercise
            // 
            this.DescExercise.AutoSize = false;
            this.DescExercise.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DescExercise.Location = new System.Drawing.Point(878, 212);
            this.DescExercise.Multiline = true;
            this.DescExercise.Name = "DescExercise";
            this.DescExercise.NullText = "Description";
            this.DescExercise.Size = new System.Drawing.Size(334, 51);
            this.DescExercise.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(874, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 20);
            this.label4.TabIndex = 94;
            this.label4.Text = "Description Exercise";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1136, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 20);
            this.label5.TabIndex = 74;
            this.label5.Text = "Play Time";
            // 
            // LightsGroup
            // 
            this.LightsGroup.Controls.Add(this.ApproachGroup);
            this.LightsGroup.Controls.Add(this.TaxiGroup);
            this.LightsGroup.Controls.Add(this.RunwayGroup);
            this.LightsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.LightsGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LightsGroup.Location = new System.Drawing.Point(509, 372);
            this.LightsGroup.Name = "LightsGroup";
            this.LightsGroup.Size = new System.Drawing.Size(347, 259);
            this.LightsGroup.TabIndex = 96;
            this.LightsGroup.TabStop = false;
            this.LightsGroup.Text = "Lights Properties";
            // 
            // ApproachGroup
            // 
            this.ApproachGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ApproachGroup.ForeColor = System.Drawing.Color.White;
            this.ApproachGroup.Location = new System.Drawing.Point(7, 145);
            this.ApproachGroup.Name = "ApproachGroup";
            this.ApproachGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ApproachGroup.Size = new System.Drawing.Size(334, 108);
            this.ApproachGroup.TabIndex = 4;
            this.ApproachGroup.TabStop = false;
            this.ApproachGroup.Text = "Approach";
            // 
            // TaxiGroup
            // 
            this.TaxiGroup.Controls.Add(this.PapiLight);
            this.TaxiGroup.Controls.Add(this.TaxiLight);
            this.TaxiGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiGroup.ForeColor = System.Drawing.Color.White;
            this.TaxiGroup.Location = new System.Drawing.Point(6, 85);
            this.TaxiGroup.Name = "TaxiGroup";
            this.TaxiGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TaxiGroup.Size = new System.Drawing.Size(335, 60);
            this.TaxiGroup.TabIndex = 3;
            this.TaxiGroup.TabStop = false;
            this.TaxiGroup.Text = "Taxi / Papi";
            // 
            // PapiLight
            // 
            this.PapiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.PapiLight.BackColor = System.Drawing.Color.Tomato;
            this.PapiLight.Enabled = false;
            this.PapiLight.FlatAppearance.BorderSize = 0;
            this.PapiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.PapiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PapiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PapiLight.ForeColor = System.Drawing.Color.White;
            this.PapiLight.Location = new System.Drawing.Point(175, 22);
            this.PapiLight.Name = "PapiLight";
            this.PapiLight.Size = new System.Drawing.Size(145, 30);
            this.PapiLight.TabIndex = 108;
            this.PapiLight.TabStop = false;
            this.PapiLight.Text = "Papi";
            this.PapiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PapiLight.UseVisualStyleBackColor = false;
            // 
            // TaxiLight
            // 
            this.TaxiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.TaxiLight.BackColor = System.Drawing.Color.Tomato;
            this.TaxiLight.Enabled = false;
            this.TaxiLight.FlatAppearance.BorderSize = 0;
            this.TaxiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.TaxiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaxiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiLight.ForeColor = System.Drawing.Color.White;
            this.TaxiLight.Location = new System.Drawing.Point(15, 22);
            this.TaxiLight.Name = "TaxiLight";
            this.TaxiLight.Size = new System.Drawing.Size(145, 30);
            this.TaxiLight.TabIndex = 107;
            this.TaxiLight.TabStop = false;
            this.TaxiLight.Text = "Taxi";
            this.TaxiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TaxiLight.UseVisualStyleBackColor = false;
            // 
            // RunwayGroup
            // 
            this.RunwayGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RunwayGroup.ForeColor = System.Drawing.Color.White;
            this.RunwayGroup.Location = new System.Drawing.Point(6, 24);
            this.RunwayGroup.Name = "RunwayGroup";
            this.RunwayGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RunwayGroup.Size = new System.Drawing.Size(335, 60);
            this.RunwayGroup.TabIndex = 2;
            this.RunwayGroup.TabStop = false;
            this.RunwayGroup.Text = "Runway";
            // 
            // SaveExerciseBtn
            // 
            this.SaveExerciseBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.SaveExerciseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveExerciseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.SaveExerciseBtn.ForeColor = System.Drawing.Color.White;
            this.SaveExerciseBtn.Location = new System.Drawing.Point(878, 319);
            this.SaveExerciseBtn.Name = "SaveExerciseBtn";
            this.SaveExerciseBtn.Size = new System.Drawing.Size(165, 35);
            this.SaveExerciseBtn.TabIndex = 3;
            this.SaveExerciseBtn.TabStop = false;
            this.SaveExerciseBtn.Text = "Save Exercise";
            this.SaveExerciseBtn.UseVisualStyleBackColor = false;
            this.SaveExerciseBtn.Click += new System.EventHandler(this.SaveExerciseBtn_Click);
            // 
            // RemoveExerciseBtn
            // 
            this.RemoveExerciseBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.RemoveExerciseBtn.Enabled = false;
            this.RemoveExerciseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RemoveExerciseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RemoveExerciseBtn.ForeColor = System.Drawing.Color.White;
            this.RemoveExerciseBtn.Location = new System.Drawing.Point(1047, 319);
            this.RemoveExerciseBtn.Name = "RemoveExerciseBtn";
            this.RemoveExerciseBtn.Size = new System.Drawing.Size(166, 35);
            this.RemoveExerciseBtn.TabIndex = 98;
            this.RemoveExerciseBtn.TabStop = false;
            this.RemoveExerciseBtn.Text = "Remove Exercise";
            this.RemoveExerciseBtn.UseVisualStyleBackColor = false;
            this.RemoveExerciseBtn.Click += new System.EventHandler(this.RemoveExerciseBtn_Click);
            // 
            // ClearExerciseBtn
            // 
            this.ClearExerciseBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClearExerciseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ClearExerciseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ClearExerciseBtn.ForeColor = System.Drawing.Color.White;
            this.ClearExerciseBtn.Location = new System.Drawing.Point(1047, 278);
            this.ClearExerciseBtn.Name = "ClearExerciseBtn";
            this.ClearExerciseBtn.Size = new System.Drawing.Size(165, 35);
            this.ClearExerciseBtn.TabIndex = 99;
            this.ClearExerciseBtn.TabStop = false;
            this.ClearExerciseBtn.Text = "Clear Data";
            this.ClearExerciseBtn.UseVisualStyleBackColor = false;
            this.ClearExerciseBtn.Click += new System.EventHandler(this.ClearExerciseBtn_Click);
            // 
            // AircraftPropGroup
            // 
            this.AircraftPropGroup.Controls.Add(this.AddAircraftBtn);
            this.AircraftPropGroup.Controls.Add(this.groupBox7);
            this.AircraftPropGroup.Controls.Add(this.ArrivalCount);
            this.AircraftPropGroup.Controls.Add(this.DepartureCount);
            this.AircraftPropGroup.Controls.Add(this.label7);
            this.AircraftPropGroup.Controls.Add(this.label6);
            this.AircraftPropGroup.Controls.Add(this.pictureBox2);
            this.AircraftPropGroup.Controls.Add(this.pictureBox1);
            this.AircraftPropGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.AircraftPropGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftPropGroup.Location = new System.Drawing.Point(12, 372);
            this.AircraftPropGroup.Name = "AircraftPropGroup";
            this.AircraftPropGroup.Size = new System.Drawing.Size(474, 259);
            this.AircraftPropGroup.TabIndex = 97;
            this.AircraftPropGroup.TabStop = false;
            this.AircraftPropGroup.Text = "Aircraft Properties";
            // 
            // AddAircraftBtn
            // 
            this.AddAircraftBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.AddAircraftBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddAircraftBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AddAircraftBtn.ForeColor = System.Drawing.Color.White;
            this.AddAircraftBtn.Location = new System.Drawing.Point(6, 27);
            this.AddAircraftBtn.Name = "AddAircraftBtn";
            this.AddAircraftBtn.Size = new System.Drawing.Size(343, 30);
            this.AddAircraftBtn.TabIndex = 8;
            this.AddAircraftBtn.TabStop = false;
            this.AddAircraftBtn.Text = "Add Aircraft";
            this.AddAircraftBtn.UseVisualStyleBackColor = false;
            this.AddAircraftBtn.Click += new System.EventHandler(this.AddAircraftBtn_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.AircraftList);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox7.ForeColor = System.Drawing.Color.White;
            this.groupBox7.Location = new System.Drawing.Point(6, 63);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(343, 190);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Aircraft List";
            // 
            // AircraftList
            // 
            this.AircraftList.AllowUserToAddRows = false;
            this.AircraftList.AllowUserToDeleteRows = false;
            this.AircraftList.AllowUserToOrderColumns = true;
            this.AircraftList.AllowUserToResizeRows = false;
            this.AircraftList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AircraftList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.AircraftList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.AircraftList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AircraftList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.AircraftList.ColumnHeadersHeight = 28;
            this.AircraftList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.AircraftList.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AircraftList.DefaultCellStyle = dataGridViewCellStyle2;
            this.AircraftList.EnableHeadersVisualStyles = false;
            this.AircraftList.GridColor = System.Drawing.Color.Black;
            this.AircraftList.Location = new System.Drawing.Point(6, 22);
            this.AircraftList.MultiSelect = false;
            this.AircraftList.Name = "AircraftList";
            this.AircraftList.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AircraftList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.AircraftList.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.AircraftList.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.AircraftList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AircraftList.Size = new System.Drawing.Size(331, 162);
            this.AircraftList.TabIndex = 3;
            this.AircraftList.TabStop = false;
            // 
            // ArrivalCount
            // 
            this.ArrivalCount.AutoSize = true;
            this.ArrivalCount.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold);
            this.ArrivalCount.ForeColor = System.Drawing.Color.White;
            this.ArrivalCount.Location = new System.Drawing.Point(436, 174);
            this.ArrivalCount.Name = "ArrivalCount";
            this.ArrivalCount.Size = new System.Drawing.Size(31, 33);
            this.ArrivalCount.TabIndex = 6;
            this.ArrivalCount.Text = "0";
            // 
            // DepartureCount
            // 
            this.DepartureCount.AutoSize = true;
            this.DepartureCount.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold);
            this.DepartureCount.ForeColor = System.Drawing.Color.White;
            this.DepartureCount.Location = new System.Drawing.Point(436, 65);
            this.DepartureCount.Name = "DepartureCount";
            this.DepartureCount.Size = new System.Drawing.Size(31, 33);
            this.DepartureCount.TabIndex = 5;
            this.DepartureCount.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(368, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Arrival";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(355, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 20);
            this.label6.TabIndex = 3;
            this.label6.Text = "Departure";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ATCSimulator.Client.Properties.Resources.arrival;
            this.pictureBox2.Location = new System.Drawing.Point(366, 152);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 64);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.departure;
            this.pictureBox1.Location = new System.Drawing.Point(366, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ExerciseTable
            // 
            this.ExerciseTable.AllowUserToAddRows = false;
            this.ExerciseTable.AllowUserToDeleteRows = false;
            this.ExerciseTable.AllowUserToOrderColumns = true;
            this.ExerciseTable.AllowUserToResizeRows = false;
            this.ExerciseTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ExerciseTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ExerciseTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.ExerciseTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExerciseTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.ExerciseTable.ColumnHeadersHeight = 28;
            this.ExerciseTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ExerciseTable.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ExerciseTable.DefaultCellStyle = dataGridViewCellStyle6;
            this.ExerciseTable.EnableHeadersVisualStyles = false;
            this.ExerciseTable.GridColor = System.Drawing.Color.Black;
            this.ExerciseTable.Location = new System.Drawing.Point(4, 26);
            this.ExerciseTable.MultiSelect = false;
            this.ExerciseTable.Name = "ExerciseTable";
            this.ExerciseTable.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExerciseTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.ExerciseTable.RowHeadersVisible = false;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            this.ExerciseTable.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.ExerciseTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ExerciseTable.Size = new System.Drawing.Size(852, 340);
            this.ExerciseTable.TabIndex = 2;
            this.ExerciseTable.TabStop = false;
            this.ExerciseTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ExerciseTable_CellClick);
            this.ExerciseTable.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ExerciseTable_CellDoubleClick);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(874, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 20);
            this.label14.TabIndex = 100;
            this.label14.Text = "Scenery";
            // 
            // SceneryList
            // 
            this.SceneryList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.SceneryList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.SceneryList.ForeColor = System.Drawing.Color.DodgerBlue;
            this.SceneryList.Location = new System.Drawing.Point(878, 55);
            this.SceneryList.Name = "SceneryList";
            this.SceneryList.Size = new System.Drawing.Size(179, 24);
            this.SceneryList.TabIndex = 1;
            this.SceneryList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.SceneryList_SelectedIndexChanged);
            // 
            // CreateExercise
            // 
            this.CreateExercise.BackColor = System.Drawing.Color.DodgerBlue;
            this.CreateExercise.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CreateExercise.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CreateExercise.ForeColor = System.Drawing.Color.White;
            this.CreateExercise.Location = new System.Drawing.Point(878, 279);
            this.CreateExercise.Name = "CreateExercise";
            this.CreateExercise.Size = new System.Drawing.Size(165, 35);
            this.CreateExercise.TabIndex = 124;
            this.CreateExercise.TabStop = false;
            this.CreateExercise.Text = "Create Exercise";
            this.CreateExercise.UseVisualStyleBackColor = false;
            this.CreateExercise.Click += new System.EventHandler(this.CreateExercise_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label13.ForeColor = System.Drawing.Color.Tomato;
            this.label13.Location = new System.Drawing.Point(1170, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 170;
            this.label13.Text = "HH:mm";
            // 
            // PlayTimeHour
            // 
            this.PlayTimeHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PlayTimeHour.Location = new System.Drawing.Point(1067, 55);
            this.PlayTimeHour.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.PlayTimeHour.Name = "PlayTimeHour";
            this.PlayTimeHour.Size = new System.Drawing.Size(42, 24);
            this.PlayTimeHour.TabIndex = 2;
            this.PlayTimeHour.TabStop = false;
            this.PlayTimeHour.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(1110, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 20);
            this.label15.TabIndex = 167;
            this.label15.Text = ":";
            // 
            // PlayTimeMin
            // 
            this.PlayTimeMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PlayTimeMin.Location = new System.Drawing.Point(1124, 55);
            this.PlayTimeMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.PlayTimeMin.Name = "PlayTimeMin";
            this.PlayTimeMin.Size = new System.Drawing.Size(42, 24);
            this.PlayTimeMin.TabIndex = 3;
            this.PlayTimeMin.TabStop = false;
            this.PlayTimeMin.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox3.Location = new System.Drawing.Point(4, 370);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1208, 3);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 171;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox4.Location = new System.Drawing.Point(4, 21);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1208, 3);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 172;
            this.pictureBox4.TabStop = false;
            // 
            // ExerciseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1225, 645);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.PlayTimeHour);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.PlayTimeMin);
            this.Controls.Add(this.CreateExercise);
            this.Controls.Add(this.SceneryList);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.ExerciseTable);
            this.Controls.Add(this.AircraftPropGroup);
            this.Controls.Add(this.ClearExerciseBtn);
            this.Controls.Add(this.RemoveExerciseBtn);
            this.Controls.Add(this.SaveExerciseBtn);
            this.Controls.Add(this.LightsGroup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DescExercise);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TrafficExercise);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ExerciseName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.EnvPropGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ExerciseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExercisePanelForm";
            this.EnvPropGroup.ResumeLayout(false);
            this.EnvPropGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExerciseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficExercise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescExercise)).EndInit();
            this.LightsGroup.ResumeLayout(false);
            this.TaxiGroup.ResumeLayout(false);
            this.AircraftPropGroup.ResumeLayout(false);
            this.AircraftPropGroup.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AircraftList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExerciseTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SceneryList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayTimeHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayTimeMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox EnvPropGroup;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadTextBox ExerciseName;
        private Telerik.WinControls.UI.RadTextBox TrafficExercise;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadTextBox DescExercise;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox LightsGroup;
        private System.Windows.Forms.GroupBox ApproachGroup;
        private System.Windows.Forms.GroupBox TaxiGroup;
        private System.Windows.Forms.GroupBox RunwayGroup;
        private System.Windows.Forms.Button SaveExerciseBtn;
        private System.Windows.Forms.Button RemoveExerciseBtn;
        private System.Windows.Forms.Button ClearExerciseBtn;
        private System.Windows.Forms.GroupBox AircraftPropGroup;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label ArrivalCount;
        private System.Windows.Forms.Label DepartureCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button AddAircraftBtn;
        private System.Windows.Forms.GroupBox groupBox7;
        private Telerik.WinControls.UI.RadDropDownList Weather;
        private System.Windows.Forms.DataGridView ExerciseTable;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadSpinEditor Visibility;
        private Telerik.WinControls.UI.RadSpinEditor Temperature;
        private Telerik.WinControls.UI.RadSpinEditor WindSpeed;
        private Telerik.WinControls.UI.RadSpinEditor WindDirection;
        private System.Windows.Forms.Label label14;
        private Telerik.WinControls.UI.RadDropDownList SceneryList;
        private System.Windows.Forms.CheckBox PapiLight;
        private System.Windows.Forms.CheckBox TaxiLight;
        private System.Windows.Forms.Button CreateExercise;
        private System.Windows.Forms.Label label13;
        private Telerik.WinControls.UI.RadSpinEditor PlayTimeHour;
        private System.Windows.Forms.Label label15;
        private Telerik.WinControls.UI.RadSpinEditor PlayTimeMin;
        private System.Windows.Forms.DataGridView AircraftList;
        private Telerik.WinControls.UI.RadTimePicker EnvTime;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;

    }
}