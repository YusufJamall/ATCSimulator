﻿namespace ATCSimulator.Client.UI.Instructor.Exercise
{
    partial class AircraftExerciseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            this.AircraftList = new System.Windows.Forms.DataGridView();
            this.AircraftGroup = new System.Windows.Forms.GroupBox();
            this.AircraftCallsign2 = new Telerik.WinControls.UI.RadTextBox();
            this.CruiseSpeedValue = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.CruiseSpeedType = new Telerik.WinControls.UI.RadDropDownList();
            this.label27 = new System.Windows.Forms.Label();
            this.AircraftPerson = new Telerik.WinControls.UI.RadSpinEditor();
            this.label22 = new System.Windows.Forms.Label();
            this.AircraftFlightLevel = new Telerik.WinControls.UI.RadDropDownList();
            this.AircraftFuelHour = new Telerik.WinControls.UI.RadSpinEditor();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.AircraftFuelMin = new Telerik.WinControls.UI.RadSpinEditor();
            this.AircraftAltitude = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.AircraftFlightRule = new Telerik.WinControls.UI.RadDropDownList();
            this.label8 = new System.Windows.Forms.Label();
            this.AircraftTypeFlight = new Telerik.WinControls.UI.RadDropDownList();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AircraftCallsign = new Telerik.WinControls.UI.RadTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.AircraftLivery = new Telerik.WinControls.UI.RadDropDownList();
            this.label11 = new System.Windows.Forms.Label();
            this.AircraftTypeModel = new Telerik.WinControls.UI.RadDropDownList();
            this.SceneryGroup = new System.Windows.Forms.GroupBox();
            this.DeparturePanel = new System.Windows.Forms.Panel();
            this.DepartureParking = new Telerik.WinControls.UI.RadDropDownList();
            this.label21 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.eet_hour = new Telerik.WinControls.UI.RadSpinEditor();
            this.label29 = new System.Windows.Forms.Label();
            this.eet_min = new Telerik.WinControls.UI.RadSpinEditor();
            this.ArrivalPanel = new System.Windows.Forms.Panel();
            this.ArrivalStartPoint = new Telerik.WinControls.UI.RadTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.ArrivalStartHour = new Telerik.WinControls.UI.RadSpinEditor();
            this.label25 = new System.Windows.Forms.Label();
            this.ArrivalStartMinute = new Telerik.WinControls.UI.RadSpinEditor();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.AlternateAirport = new Telerik.WinControls.UI.RadTextBox();
            this.DepartureHour = new Telerik.WinControls.UI.RadSpinEditor();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.DepartureMinute = new Telerik.WinControls.UI.RadSpinEditor();
            this.label17 = new System.Windows.Forms.Label();
            this.AircraftRoute = new Telerik.WinControls.UI.RadDropDownList();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DestinationAirport = new Telerik.WinControls.UI.RadDropDownList();
            this.label7 = new System.Windows.Forms.Label();
            this.OriginAirport = new Telerik.WinControls.UI.RadDropDownList();
            this.FlightModeBox = new System.Windows.Forms.GroupBox();
            this.ArrivalCheckBox = new System.Windows.Forms.RadioButton();
            this.DepartureCheckBox = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.RemoveButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.FlightPlan = new System.Windows.Forms.Button();
            this.CreateButton = new System.Windows.Forms.Button();
            this.Line = new System.Windows.Forms.PictureBox();
            this.RunwayList = new Telerik.WinControls.UI.RadDropDownList();
            this.RunwayLbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftList)).BeginInit();
            this.AircraftGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCallsign2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruiseSpeedValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruiseSpeedType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFlightLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftAltitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFlightRule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftTypeFlight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCallsign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftLivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftTypeModel)).BeginInit();
            this.SceneryGroup.SuspendLayout();
            this.DeparturePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepartureParking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eet_hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eet_min)).BeginInit();
            this.ArrivalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalStartPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalStartHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalStartMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlternateAirport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartureHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartureMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationAirport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginAirport)).BeginInit();
            this.FlightModeBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Line)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // AircraftList
            // 
            this.AircraftList.AllowUserToAddRows = false;
            this.AircraftList.AllowUserToDeleteRows = false;
            this.AircraftList.AllowUserToOrderColumns = true;
            this.AircraftList.AllowUserToResizeRows = false;
            this.AircraftList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AircraftList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.AircraftList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.AircraftList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AircraftList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.AircraftList.ColumnHeadersHeight = 28;
            this.AircraftList.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AircraftList.DefaultCellStyle = dataGridViewCellStyle2;
            this.AircraftList.EnableHeadersVisualStyles = false;
            this.AircraftList.GridColor = System.Drawing.Color.Black;
            this.AircraftList.Location = new System.Drawing.Point(415, 30);
            this.AircraftList.MultiSelect = false;
            this.AircraftList.Name = "AircraftList";
            this.AircraftList.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AircraftList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.AircraftList.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.AircraftList.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.AircraftList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AircraftList.Size = new System.Drawing.Size(596, 389);
            this.AircraftList.TabIndex = 132;
            this.AircraftList.TabStop = false;
            this.AircraftList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AircraftList_CellClick);
            this.AircraftList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AircraftList_CellDoubleClick);
            // 
            // AircraftGroup
            // 
            this.AircraftGroup.Controls.Add(this.AircraftCallsign2);
            this.AircraftGroup.Controls.Add(this.CruiseSpeedValue);
            this.AircraftGroup.Controls.Add(this.CruiseSpeedType);
            this.AircraftGroup.Controls.Add(this.label27);
            this.AircraftGroup.Controls.Add(this.AircraftPerson);
            this.AircraftGroup.Controls.Add(this.label22);
            this.AircraftGroup.Controls.Add(this.AircraftFlightLevel);
            this.AircraftGroup.Controls.Add(this.AircraftFuelHour);
            this.AircraftGroup.Controls.Add(this.label15);
            this.AircraftGroup.Controls.Add(this.label10);
            this.AircraftGroup.Controls.Add(this.AircraftFuelMin);
            this.AircraftGroup.Controls.Add(this.AircraftAltitude);
            this.AircraftGroup.Controls.Add(this.label13);
            this.AircraftGroup.Controls.Add(this.label9);
            this.AircraftGroup.Controls.Add(this.AircraftFlightRule);
            this.AircraftGroup.Controls.Add(this.label8);
            this.AircraftGroup.Controls.Add(this.AircraftTypeFlight);
            this.AircraftGroup.Controls.Add(this.label2);
            this.AircraftGroup.Controls.Add(this.label1);
            this.AircraftGroup.Controls.Add(this.AircraftCallsign);
            this.AircraftGroup.Controls.Add(this.label14);
            this.AircraftGroup.Controls.Add(this.label12);
            this.AircraftGroup.Controls.Add(this.AircraftLivery);
            this.AircraftGroup.Controls.Add(this.label11);
            this.AircraftGroup.Controls.Add(this.AircraftTypeModel);
            this.AircraftGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.AircraftGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftGroup.Location = new System.Drawing.Point(12, 85);
            this.AircraftGroup.Name = "AircraftGroup";
            this.AircraftGroup.Size = new System.Drawing.Size(393, 334);
            this.AircraftGroup.TabIndex = 133;
            this.AircraftGroup.TabStop = false;
            this.AircraftGroup.Text = "Aircraft Properties";
            // 
            // AircraftCallsign2
            // 
            this.AircraftCallsign2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftCallsign2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftCallsign2.Location = new System.Drawing.Point(255, 97);
            this.AircraftCallsign2.MaxLength = 10;
            this.AircraftCallsign2.Name = "AircraftCallsign2";
            this.AircraftCallsign2.NullText = "ex : 0001";
            // 
            // 
            // 
            this.AircraftCallsign2.RootElement.ControlBounds = new System.Drawing.Rectangle(255, 97, 100, 20);
            this.AircraftCallsign2.RootElement.StretchVertically = true;
            this.AircraftCallsign2.Size = new System.Drawing.Size(132, 24);
            this.AircraftCallsign2.TabIndex = 3;
            // 
            // CruiseSpeedValue
            // 
            this.CruiseSpeedValue.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CruiseSpeedValue.Enabled = false;
            this.CruiseSpeedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CruiseSpeedValue.Location = new System.Drawing.Point(308, 199);
            this.CruiseSpeedValue.Mask = "n2";
            this.CruiseSpeedValue.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.CruiseSpeedValue.Name = "CruiseSpeedValue";
            this.CruiseSpeedValue.ReadOnly = true;
            this.CruiseSpeedValue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            // 
            // 
            // 
            this.CruiseSpeedValue.RootElement.ControlBounds = new System.Drawing.Rectangle(308, 199, 125, 20);
            this.CruiseSpeedValue.RootElement.Enabled = false;
            this.CruiseSpeedValue.RootElement.StretchVertically = true;
            this.CruiseSpeedValue.Size = new System.Drawing.Size(79, 22);
            this.CruiseSpeedValue.TabIndex = 8;
            this.CruiseSpeedValue.TabStop = false;
            this.CruiseSpeedValue.Text = "0.00";
            // 
            // CruiseSpeedType
            // 
            this.CruiseSpeedType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CruiseSpeedType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CruiseSpeedType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CruiseSpeedType.ForeColor = System.Drawing.Color.DodgerBlue;
            radListDataItem1.Text = "K";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "M";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "N";
            radListDataItem3.TextWrap = true;
            this.CruiseSpeedType.Items.Add(radListDataItem1);
            this.CruiseSpeedType.Items.Add(radListDataItem2);
            this.CruiseSpeedType.Items.Add(radListDataItem3);
            this.CruiseSpeedType.Location = new System.Drawing.Point(238, 199);
            this.CruiseSpeedType.Name = "CruiseSpeedType";
            // 
            // 
            // 
            this.CruiseSpeedType.RootElement.ControlBounds = new System.Drawing.Rectangle(238, 199, 125, 20);
            this.CruiseSpeedType.RootElement.StretchVertically = true;
            this.CruiseSpeedType.Size = new System.Drawing.Size(64, 22);
            this.CruiseSpeedType.TabIndex = 7;
            this.CruiseSpeedType.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CruiseSpeedType_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(6, 199);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(92, 20);
            this.label27.TabIndex = 157;
            this.label27.Text = "True Speed";
            // 
            // AircraftPerson
            // 
            this.AircraftPerson.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftPerson.Location = new System.Drawing.Point(308, 302);
            this.AircraftPerson.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.AircraftPerson.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.AircraftPerson.Name = "AircraftPerson";
            // 
            // 
            // 
            this.AircraftPerson.RootElement.ControlBounds = new System.Drawing.Rectangle(308, 302, 100, 20);
            this.AircraftPerson.RootElement.StretchVertically = true;
            this.AircraftPerson.Size = new System.Drawing.Size(79, 24);
            this.AircraftPerson.TabIndex = 12;
            this.AircraftPerson.TabStop = false;
            this.AircraftPerson.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.AircraftPerson.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label22.ForeColor = System.Drawing.Color.Tomato;
            this.label22.Location = new System.Drawing.Point(121, 138);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(108, 13);
            this.label22.TabIndex = 156;
            this.label22.Text = "ex: A 20 / (2000 feet)";
            // 
            // AircraftFlightLevel
            // 
            this.AircraftFlightLevel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftFlightLevel.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.AircraftFlightLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.AircraftFlightLevel.ForeColor = System.Drawing.Color.DodgerBlue;
            radListDataItem4.Text = "A";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "F";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "S";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "M";
            radListDataItem7.TextWrap = true;
            this.AircraftFlightLevel.Items.Add(radListDataItem4);
            this.AircraftFlightLevel.Items.Add(radListDataItem5);
            this.AircraftFlightLevel.Items.Add(radListDataItem6);
            this.AircraftFlightLevel.Items.Add(radListDataItem7);
            this.AircraftFlightLevel.Location = new System.Drawing.Point(238, 131);
            this.AircraftFlightLevel.Name = "AircraftFlightLevel";
            // 
            // 
            // 
            this.AircraftFlightLevel.RootElement.ControlBounds = new System.Drawing.Rectangle(238, 131, 125, 20);
            this.AircraftFlightLevel.RootElement.StretchVertically = true;
            this.AircraftFlightLevel.Size = new System.Drawing.Size(64, 22);
            this.AircraftFlightLevel.TabIndex = 4;
            // 
            // AircraftFuelHour
            // 
            this.AircraftFuelHour.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftFuelHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftFuelHour.Location = new System.Drawing.Point(237, 233);
            this.AircraftFuelHour.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.AircraftFuelHour.Name = "AircraftFuelHour";
            // 
            // 
            // 
            this.AircraftFuelHour.RootElement.ControlBounds = new System.Drawing.Rectangle(237, 233, 100, 20);
            this.AircraftFuelHour.RootElement.StretchVertically = true;
            this.AircraftFuelHour.Size = new System.Drawing.Size(42, 24);
            this.AircraftFuelHour.TabIndex = 9;
            this.AircraftFuelHour.TabStop = false;
            this.AircraftFuelHour.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label15.ForeColor = System.Drawing.Color.Tomato;
            this.label15.Location = new System.Drawing.Point(342, 242);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 154;
            this.label15.Text = "HH:mm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(280, 235);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 20);
            this.label10.TabIndex = 152;
            this.label10.Text = ":";
            // 
            // AircraftFuelMin
            // 
            this.AircraftFuelMin.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftFuelMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftFuelMin.Location = new System.Drawing.Point(294, 233);
            this.AircraftFuelMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.AircraftFuelMin.Name = "AircraftFuelMin";
            // 
            // 
            // 
            this.AircraftFuelMin.RootElement.ControlBounds = new System.Drawing.Rectangle(294, 233, 100, 20);
            this.AircraftFuelMin.RootElement.StretchVertically = true;
            this.AircraftFuelMin.Size = new System.Drawing.Size(42, 24);
            this.AircraftFuelMin.TabIndex = 10;
            this.AircraftFuelMin.TabStop = false;
            this.AircraftFuelMin.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AircraftAltitude
            // 
            this.AircraftAltitude.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftAltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.AircraftAltitude.Location = new System.Drawing.Point(308, 131);
            this.AircraftAltitude.Mask = "d";
            this.AircraftAltitude.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.AircraftAltitude.Name = "AircraftAltitude";
            this.AircraftAltitude.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            // 
            // 
            // 
            this.AircraftAltitude.RootElement.ControlBounds = new System.Drawing.Rectangle(308, 131, 125, 20);
            this.AircraftAltitude.RootElement.StretchVertically = true;
            this.AircraftAltitude.Size = new System.Drawing.Size(79, 22);
            this.AircraftAltitude.TabIndex = 5;
            this.AircraftAltitude.TabStop = false;
            this.AircraftAltitude.Text = "0";
            this.AircraftAltitude.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(7, 131);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 20);
            this.label13.TabIndex = 149;
            this.label13.Text = "Level";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(7, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 20);
            this.label9.TabIndex = 148;
            this.label9.Text = "Flight Rules";
            // 
            // AircraftFlightRule
            // 
            this.AircraftFlightRule.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftFlightRule.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.AircraftFlightRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftFlightRule.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftFlightRule.Location = new System.Drawing.Point(255, 166);
            this.AircraftFlightRule.Name = "AircraftFlightRule";
            // 
            // 
            // 
            this.AircraftFlightRule.RootElement.ControlBounds = new System.Drawing.Rectangle(255, 166, 125, 20);
            this.AircraftFlightRule.RootElement.StretchVertically = true;
            this.AircraftFlightRule.Size = new System.Drawing.Size(132, 24);
            this.AircraftFlightRule.TabIndex = 6;
            this.AircraftFlightRule.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.AircraftFlightRule_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(7, 268);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 20);
            this.label8.TabIndex = 146;
            this.label8.Text = "Type of Flight";
            // 
            // AircraftTypeFlight
            // 
            this.AircraftTypeFlight.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftTypeFlight.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.AircraftTypeFlight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftTypeFlight.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftTypeFlight.Location = new System.Drawing.Point(189, 268);
            this.AircraftTypeFlight.Name = "AircraftTypeFlight";
            // 
            // 
            // 
            this.AircraftTypeFlight.RootElement.ControlBounds = new System.Drawing.Rectangle(189, 268, 125, 20);
            this.AircraftTypeFlight.RootElement.StretchVertically = true;
            this.AircraftTypeFlight.Size = new System.Drawing.Size(198, 24);
            this.AircraftTypeFlight.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(8, 233);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 20);
            this.label2.TabIndex = 143;
            this.label2.Text = "Fuel on Board";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(7, 302);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 141;
            this.label1.Text = "Persons on Board";
            // 
            // AircraftCallsign
            // 
            this.AircraftCallsign.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftCallsign.Enabled = false;
            this.AircraftCallsign.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AircraftCallsign.Location = new System.Drawing.Point(189, 97);
            this.AircraftCallsign.MaxLength = 10;
            this.AircraftCallsign.Name = "AircraftCallsign";
            this.AircraftCallsign.NullText = "ex : JT";
            // 
            // 
            // 
            this.AircraftCallsign.RootElement.ControlBounds = new System.Drawing.Rectangle(189, 97, 100, 20);
            this.AircraftCallsign.RootElement.Enabled = false;
            this.AircraftCallsign.RootElement.StretchVertically = true;
            this.AircraftCallsign.Size = new System.Drawing.Size(60, 24);
            this.AircraftCallsign.TabIndex = 3;
            this.AircraftCallsign.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(7, 97);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 20);
            this.label14.TabIndex = 139;
            this.label14.Text = "Callsign";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(7, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 20);
            this.label12.TabIndex = 138;
            this.label12.Text = "Aircraft Livery";
            // 
            // AircraftLivery
            // 
            this.AircraftLivery.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftLivery.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.AircraftLivery.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.AircraftLivery.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftLivery.Location = new System.Drawing.Point(189, 63);
            this.AircraftLivery.Name = "AircraftLivery";
            // 
            // 
            // 
            this.AircraftLivery.RootElement.ControlBounds = new System.Drawing.Rectangle(189, 63, 125, 20);
            this.AircraftLivery.RootElement.StretchVertically = true;
            this.AircraftLivery.Size = new System.Drawing.Size(198, 22);
            this.AircraftLivery.TabIndex = 2;
            this.AircraftLivery.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.AircraftLivery_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(7, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 20);
            this.label11.TabIndex = 136;
            this.label11.Text = "Aircraft Type";
            // 
            // AircraftTypeModel
            // 
            this.AircraftTypeModel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftTypeModel.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.AircraftTypeModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.AircraftTypeModel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftTypeModel.Location = new System.Drawing.Point(189, 28);
            this.AircraftTypeModel.Name = "AircraftTypeModel";
            // 
            // 
            // 
            this.AircraftTypeModel.RootElement.ControlBounds = new System.Drawing.Rectangle(189, 28, 125, 20);
            this.AircraftTypeModel.RootElement.StretchVertically = true;
            this.AircraftTypeModel.Size = new System.Drawing.Size(198, 22);
            this.AircraftTypeModel.TabIndex = 1;
            this.AircraftTypeModel.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.aircraft_model_SelectedIndexChanged);
            // 
            // SceneryGroup
            // 
            this.SceneryGroup.Controls.Add(this.RunwayLbl);
            this.SceneryGroup.Controls.Add(this.RunwayList);
            this.SceneryGroup.Controls.Add(this.DeparturePanel);
            this.SceneryGroup.Controls.Add(this.label30);
            this.SceneryGroup.Controls.Add(this.eet_hour);
            this.SceneryGroup.Controls.Add(this.label29);
            this.SceneryGroup.Controls.Add(this.eet_min);
            this.SceneryGroup.Controls.Add(this.ArrivalPanel);
            this.SceneryGroup.Controls.Add(this.AlternateAirport);
            this.SceneryGroup.Controls.Add(this.DepartureHour);
            this.SceneryGroup.Controls.Add(this.label23);
            this.SceneryGroup.Controls.Add(this.label24);
            this.SceneryGroup.Controls.Add(this.DepartureMinute);
            this.SceneryGroup.Controls.Add(this.label17);
            this.SceneryGroup.Controls.Add(this.AircraftRoute);
            this.SceneryGroup.Controls.Add(this.label18);
            this.SceneryGroup.Controls.Add(this.label16);
            this.SceneryGroup.Controls.Add(this.label3);
            this.SceneryGroup.Controls.Add(this.label5);
            this.SceneryGroup.Controls.Add(this.label6);
            this.SceneryGroup.Controls.Add(this.DestinationAirport);
            this.SceneryGroup.Controls.Add(this.label7);
            this.SceneryGroup.Controls.Add(this.OriginAirport);
            this.SceneryGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.SceneryGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.SceneryGroup.Location = new System.Drawing.Point(12, 420);
            this.SceneryGroup.Name = "SceneryGroup";
            this.SceneryGroup.Size = new System.Drawing.Size(869, 137);
            this.SceneryGroup.TabIndex = 134;
            this.SceneryGroup.TabStop = false;
            // 
            // DeparturePanel
            // 
            this.DeparturePanel.Controls.Add(this.DepartureParking);
            this.DeparturePanel.Controls.Add(this.label21);
            this.DeparturePanel.Location = new System.Drawing.Point(6, 93);
            this.DeparturePanel.Name = "DeparturePanel";
            this.DeparturePanel.Size = new System.Drawing.Size(344, 33);
            this.DeparturePanel.TabIndex = 161;
            this.DeparturePanel.Visible = false;
            // 
            // DepartureParking
            // 
            this.DepartureParking.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DepartureParking.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DepartureParking.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.DepartureParking.ForeColor = System.Drawing.Color.DodgerBlue;
            this.DepartureParking.Location = new System.Drawing.Point(119, 6);
            this.DepartureParking.Name = "DepartureParking";
            // 
            // 
            // 
            this.DepartureParking.RootElement.ControlBounds = new System.Drawing.Rectangle(119, 6, 125, 20);
            this.DepartureParking.RootElement.StretchVertically = true;
            this.DepartureParking.Size = new System.Drawing.Size(216, 21);
            this.DepartureParking.TabIndex = 16;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(3, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(93, 20);
            this.label21.TabIndex = 153;
            this.label21.Text = "Parking Bay";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label30.ForeColor = System.Drawing.Color.Tomato;
            this.label30.Location = new System.Drawing.Point(824, 108);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(42, 13);
            this.label30.TabIndex = 166;
            this.label30.Text = "HH:mm";
            // 
            // eet_hour
            // 
            this.eet_hour.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.eet_hour.Enabled = false;
            this.eet_hour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.eet_hour.Location = new System.Drawing.Point(721, 101);
            this.eet_hour.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.eet_hour.Name = "eet_hour";
            // 
            // 
            // 
            this.eet_hour.RootElement.ControlBounds = new System.Drawing.Rectangle(721, 101, 100, 20);
            this.eet_hour.RootElement.Enabled = false;
            this.eet_hour.RootElement.StretchVertically = true;
            this.eet_hour.Size = new System.Drawing.Size(42, 24);
            this.eet_hour.TabIndex = 21;
            this.eet_hour.TabStop = false;
            this.eet_hour.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(764, 103);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 20);
            this.label29.TabIndex = 163;
            this.label29.Text = ":";
            // 
            // eet_min
            // 
            this.eet_min.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.eet_min.Enabled = false;
            this.eet_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.eet_min.Location = new System.Drawing.Point(778, 101);
            this.eet_min.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.eet_min.Name = "eet_min";
            // 
            // 
            // 
            this.eet_min.RootElement.ControlBounds = new System.Drawing.Rectangle(778, 101, 100, 20);
            this.eet_min.RootElement.Enabled = false;
            this.eet_min.RootElement.StretchVertically = true;
            this.eet_min.Size = new System.Drawing.Size(42, 24);
            this.eet_min.TabIndex = 22;
            this.eet_min.TabStop = false;
            this.eet_min.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ArrivalPanel
            // 
            this.ArrivalPanel.Controls.Add(this.ArrivalStartPoint);
            this.ArrivalPanel.Controls.Add(this.label28);
            this.ArrivalPanel.Controls.Add(this.ArrivalStartHour);
            this.ArrivalPanel.Controls.Add(this.label25);
            this.ArrivalPanel.Controls.Add(this.ArrivalStartMinute);
            this.ArrivalPanel.Controls.Add(this.label26);
            this.ArrivalPanel.Controls.Add(this.label19);
            this.ArrivalPanel.Controls.Add(this.label20);
            this.ArrivalPanel.Location = new System.Drawing.Point(6, 93);
            this.ArrivalPanel.Name = "ArrivalPanel";
            this.ArrivalPanel.Size = new System.Drawing.Size(600, 33);
            this.ArrivalPanel.TabIndex = 157;
            this.ArrivalPanel.Visible = false;
            // 
            // ArrivalStartPoint
            // 
            this.ArrivalStartPoint.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ArrivalStartPoint.Enabled = false;
            this.ArrivalStartPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ArrivalStartPoint.Location = new System.Drawing.Point(93, 4);
            this.ArrivalStartPoint.MaxLength = 10;
            this.ArrivalStartPoint.Name = "ArrivalStartPoint";
            this.ArrivalStartPoint.ReadOnly = true;
            // 
            // 
            // 
            this.ArrivalStartPoint.RootElement.ControlBounds = new System.Drawing.Rectangle(93, 4, 100, 20);
            this.ArrivalStartPoint.RootElement.Enabled = false;
            this.ArrivalStartPoint.RootElement.StretchVertically = true;
            this.ArrivalStartPoint.Size = new System.Drawing.Size(216, 24);
            this.ArrivalStartPoint.TabIndex = 162;
            this.ArrivalStartPoint.TabStop = false;
            this.ArrivalStartPoint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(486, 6);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 20);
            this.label28.TabIndex = 162;
            this.label28.Text = ":";
            // 
            // ArrivalStartHour
            // 
            this.ArrivalStartHour.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ArrivalStartHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ArrivalStartHour.Location = new System.Drawing.Point(443, 3);
            this.ArrivalStartHour.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.ArrivalStartHour.Name = "ArrivalStartHour";
            // 
            // 
            // 
            this.ArrivalStartHour.RootElement.ControlBounds = new System.Drawing.Rectangle(443, 3, 100, 20);
            this.ArrivalStartHour.RootElement.StretchVertically = true;
            this.ArrivalStartHour.Size = new System.Drawing.Size(42, 24);
            this.ArrivalStartHour.TabIndex = 17;
            this.ArrivalStartHour.TabStop = false;
            this.ArrivalStartHour.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label25.ForeColor = System.Drawing.Color.Tomato;
            this.label25.Location = new System.Drawing.Point(546, 17);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 13);
            this.label25.TabIndex = 162;
            this.label25.Text = "HH:mm";
            // 
            // ArrivalStartMinute
            // 
            this.ArrivalStartMinute.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ArrivalStartMinute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ArrivalStartMinute.Location = new System.Drawing.Point(500, 3);
            this.ArrivalStartMinute.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.ArrivalStartMinute.Name = "ArrivalStartMinute";
            // 
            // 
            // 
            this.ArrivalStartMinute.RootElement.ControlBounds = new System.Drawing.Rectangle(500, 3, 100, 20);
            this.ArrivalStartMinute.RootElement.StretchVertically = true;
            this.ArrivalStartMinute.Size = new System.Drawing.Size(42, 24);
            this.ArrivalStartMinute.TabIndex = 18;
            this.ArrivalStartMinute.TabStop = false;
            this.ArrivalStartMinute.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label26.ForeColor = System.Drawing.Color.Tomato;
            this.label26.Location = new System.Drawing.Point(545, 2);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 13);
            this.label26.TabIndex = 160;
            this.label26.Text = "UTC";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(2, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 20);
            this.label19.TabIndex = 153;
            this.label19.Text = "Start Point";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(315, 5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(122, 20);
            this.label20.TabIndex = 155;
            this.label20.Text = "Time Start Point";
            // 
            // AlternateAirport
            // 
            this.AlternateAirport.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AlternateAirport.Enabled = false;
            this.AlternateAirport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.AlternateAirport.Location = new System.Drawing.Point(695, 27);
            this.AlternateAirport.MaxLength = 10;
            this.AlternateAirport.Name = "AlternateAirport";
            this.AlternateAirport.ReadOnly = true;
            // 
            // 
            // 
            this.AlternateAirport.RootElement.ControlBounds = new System.Drawing.Rectangle(695, 27, 100, 20);
            this.AlternateAirport.RootElement.Enabled = false;
            this.AlternateAirport.RootElement.StretchVertically = true;
            this.AlternateAirport.Size = new System.Drawing.Size(167, 24);
            this.AlternateAirport.TabIndex = 161;
            this.AlternateAirport.TabStop = false;
            this.AlternateAirport.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // DepartureHour
            // 
            this.DepartureHour.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DepartureHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DepartureHour.Location = new System.Drawing.Point(721, 60);
            this.DepartureHour.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.DepartureHour.Name = "DepartureHour";
            // 
            // 
            // 
            this.DepartureHour.RootElement.ControlBounds = new System.Drawing.Rectangle(721, 60, 100, 20);
            this.DepartureHour.RootElement.StretchVertically = true;
            this.DepartureHour.Size = new System.Drawing.Size(42, 24);
            this.DepartureHour.TabIndex = 19;
            this.DepartureHour.TabStop = false;
            this.DepartureHour.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label23.ForeColor = System.Drawing.Color.Tomato;
            this.label23.Location = new System.Drawing.Point(824, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 13);
            this.label23.TabIndex = 158;
            this.label23.Text = "HH:mm";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(764, 62);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 20);
            this.label24.TabIndex = 156;
            this.label24.Text = ":";
            // 
            // DepartureMinute
            // 
            this.DepartureMinute.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DepartureMinute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.DepartureMinute.Location = new System.Drawing.Point(778, 60);
            this.DepartureMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.DepartureMinute.Name = "DepartureMinute";
            // 
            // 
            // 
            this.DepartureMinute.RootElement.ControlBounds = new System.Drawing.Rectangle(778, 60, 100, 20);
            this.DepartureMinute.RootElement.StretchVertically = true;
            this.DepartureMinute.Size = new System.Drawing.Size(42, 24);
            this.DepartureMinute.TabIndex = 20;
            this.DepartureMinute.TabStop = false;
            this.DepartureMinute.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(562, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(125, 20);
            this.label17.TabIndex = 150;
            this.label17.Text = "Alternate Airport";
            // 
            // AircraftRoute
            // 
            this.AircraftRoute.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.AircraftRoute.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.AircraftRoute.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.AircraftRoute.ForeColor = System.Drawing.Color.DodgerBlue;
            this.AircraftRoute.Location = new System.Drawing.Point(85, 63);
            this.AircraftRoute.Name = "AircraftRoute";
            // 
            // 
            // 
            this.AircraftRoute.RootElement.ControlBounds = new System.Drawing.Rectangle(85, 63, 125, 20);
            this.AircraftRoute.RootElement.StretchVertically = true;
            this.AircraftRoute.Size = new System.Drawing.Size(323, 22);
            this.AircraftRoute.TabIndex = 15;
            this.AircraftRoute.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.AircraftRoute_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(626, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 20);
            this.label18.TabIndex = 147;
            this.label18.Text = "Total EET";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label16.ForeColor = System.Drawing.Color.Tomato;
            this.label16.Location = new System.Drawing.Point(823, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 13);
            this.label16.TabIndex = 146;
            this.label16.Text = "UTC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(8, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 20);
            this.label3.TabIndex = 143;
            this.label3.Text = "Route";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(561, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 20);
            this.label5.TabIndex = 139;
            this.label5.Text = "Departure Time";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(282, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 20);
            this.label6.TabIndex = 138;
            this.label6.Text = "Destination";
            // 
            // DestinationAirport
            // 
            this.DestinationAirport.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DestinationAirport.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DestinationAirport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.DestinationAirport.ForeColor = System.Drawing.Color.DodgerBlue;
            this.DestinationAirport.Location = new System.Drawing.Point(388, 28);
            this.DestinationAirport.Name = "DestinationAirport";
            // 
            // 
            // 
            this.DestinationAirport.RootElement.ControlBounds = new System.Drawing.Rectangle(388, 28, 125, 20);
            this.DestinationAirport.RootElement.StretchVertically = true;
            this.DestinationAirport.Size = new System.Drawing.Size(155, 21);
            this.DestinationAirport.TabIndex = 14;
            this.DestinationAirport.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.AirportRoute_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(7, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 20);
            this.label7.TabIndex = 136;
            this.label7.Text = "Origin";
            // 
            // OriginAirport
            // 
            this.OriginAirport.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.OriginAirport.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.OriginAirport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.OriginAirport.ForeColor = System.Drawing.Color.DodgerBlue;
            this.OriginAirport.Location = new System.Drawing.Point(84, 28);
            this.OriginAirport.Name = "OriginAirport";
            // 
            // 
            // 
            this.OriginAirport.RootElement.ControlBounds = new System.Drawing.Rectangle(84, 28, 125, 20);
            this.OriginAirport.RootElement.StretchVertically = true;
            this.OriginAirport.Size = new System.Drawing.Size(155, 21);
            this.OriginAirport.TabIndex = 13;
            this.OriginAirport.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.AirportRoute_SelectedIndexChanged);
            // 
            // FlightModeBox
            // 
            this.FlightModeBox.Controls.Add(this.ArrivalCheckBox);
            this.FlightModeBox.Controls.Add(this.DepartureCheckBox);
            this.FlightModeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FlightModeBox.ForeColor = System.Drawing.Color.White;
            this.FlightModeBox.Location = new System.Drawing.Point(12, 5);
            this.FlightModeBox.Name = "FlightModeBox";
            this.FlightModeBox.Size = new System.Drawing.Size(209, 76);
            this.FlightModeBox.TabIndex = 145;
            this.FlightModeBox.TabStop = false;
            this.FlightModeBox.Text = "Flight Mode";
            // 
            // ArrivalCheckBox
            // 
            this.ArrivalCheckBox.AutoSize = true;
            this.ArrivalCheckBox.ForeColor = System.Drawing.Color.White;
            this.ArrivalCheckBox.Location = new System.Drawing.Point(129, 32);
            this.ArrivalCheckBox.Name = "ArrivalCheckBox";
            this.ArrivalCheckBox.Size = new System.Drawing.Size(70, 24);
            this.ArrivalCheckBox.TabIndex = 3;
            this.ArrivalCheckBox.Text = "Arrival";
            this.ArrivalCheckBox.UseVisualStyleBackColor = true;
            this.ArrivalCheckBox.CheckedChanged += new System.EventHandler(this.FlightModeChanged);
            // 
            // DepartureCheckBox
            // 
            this.DepartureCheckBox.AutoSize = true;
            this.DepartureCheckBox.ForeColor = System.Drawing.Color.White;
            this.DepartureCheckBox.Location = new System.Drawing.Point(12, 31);
            this.DepartureCheckBox.Name = "DepartureCheckBox";
            this.DepartureCheckBox.Size = new System.Drawing.Size(99, 24);
            this.DepartureCheckBox.TabIndex = 2;
            this.DepartureCheckBox.Text = "Departure";
            this.DepartureCheckBox.UseVisualStyleBackColor = true;
            this.DepartureCheckBox.CheckedChanged += new System.EventHandler(this.FlightModeChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(411, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 24);
            this.label4.TabIndex = 146;
            this.label4.Text = "Aircraft List";
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CloseButton.ForeColor = System.Drawing.Color.White;
            this.CloseButton.Location = new System.Drawing.Point(887, 527);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(124, 30);
            this.CloseButton.TabIndex = 151;
            this.CloseButton.TabStop = false;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // RemoveButton
            // 
            this.RemoveButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.RemoveButton.Enabled = false;
            this.RemoveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RemoveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.RemoveButton.ForeColor = System.Drawing.Color.White;
            this.RemoveButton.Location = new System.Drawing.Point(887, 495);
            this.RemoveButton.Name = "RemoveButton";
            this.RemoveButton.Size = new System.Drawing.Size(124, 30);
            this.RemoveButton.TabIndex = 150;
            this.RemoveButton.TabStop = false;
            this.RemoveButton.Text = "Remove";
            this.RemoveButton.UseVisualStyleBackColor = false;
            this.RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ClearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.ClearButton.ForeColor = System.Drawing.Color.White;
            this.ClearButton.Location = new System.Drawing.Point(887, 462);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(124, 30);
            this.ClearButton.TabIndex = 149;
            this.ClearButton.TabStop = false;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = false;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.SaveButton.ForeColor = System.Drawing.Color.White;
            this.SaveButton.Location = new System.Drawing.Point(888, 430);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(124, 30);
            this.SaveButton.TabIndex = 148;
            this.SaveButton.TabStop = false;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // FlightPlan
            // 
            this.FlightPlan.BackColor = System.Drawing.Color.DodgerBlue;
            this.FlightPlan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FlightPlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.FlightPlan.ForeColor = System.Drawing.Color.White;
            this.FlightPlan.Location = new System.Drawing.Point(227, 51);
            this.FlightPlan.Name = "FlightPlan";
            this.FlightPlan.Size = new System.Drawing.Size(178, 30);
            this.FlightPlan.TabIndex = 160;
            this.FlightPlan.TabStop = false;
            this.FlightPlan.Text = "Flight Plan";
            this.FlightPlan.UseVisualStyleBackColor = false;
            this.FlightPlan.Click += new System.EventHandler(this.FlightPlan_Click);
            // 
            // CreateButton
            // 
            this.CreateButton.BackColor = System.Drawing.Color.DodgerBlue;
            this.CreateButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CreateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CreateButton.ForeColor = System.Drawing.Color.White;
            this.CreateButton.Location = new System.Drawing.Point(227, 18);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(178, 30);
            this.CreateButton.TabIndex = 159;
            this.CreateButton.TabStop = false;
            this.CreateButton.Text = "Create Aircraft";
            this.CreateButton.UseVisualStyleBackColor = false;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // Line
            // 
            this.Line.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.Line.Location = new System.Drawing.Point(415, 21);
            this.Line.Name = "Line";
            this.Line.Size = new System.Drawing.Size(595, 4);
            this.Line.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Line.TabIndex = 161;
            this.Line.TabStop = false;
            // 
            // RunwayList
            // 
            this.RunwayList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.RunwayList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RunwayList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.RunwayList.ForeColor = System.Drawing.Color.DodgerBlue;
            this.RunwayList.Location = new System.Drawing.Point(486, 63);
            this.RunwayList.Name = "RunwayList";
            // 
            // 
            // 
            this.RunwayList.RootElement.ControlBounds = new System.Drawing.Rectangle(388, 28, 125, 20);
            this.RunwayList.RootElement.StretchVertically = true;
            this.RunwayList.Size = new System.Drawing.Size(57, 21);
            this.RunwayList.TabIndex = 167;
            // 
            // RunwayLbl
            // 
            this.RunwayLbl.AutoSize = true;
            this.RunwayLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RunwayLbl.ForeColor = System.Drawing.Color.White;
            this.RunwayLbl.Location = new System.Drawing.Point(414, 63);
            this.RunwayLbl.Name = "RunwayLbl";
            this.RunwayLbl.Size = new System.Drawing.Size(66, 20);
            this.RunwayLbl.TabIndex = 168;
            this.RunwayLbl.Text = "Runway";
            // 
            // AircraftExerciseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            this.ClientSize = new System.Drawing.Size(1024, 563);
            this.Controls.Add(this.Line);
            this.Controls.Add(this.FlightPlan);
            this.Controls.Add(this.CreateButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.RemoveButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.FlightModeBox);
            this.Controls.Add(this.SceneryGroup);
            this.Controls.Add(this.AircraftGroup);
            this.Controls.Add(this.AircraftList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AircraftExerciseForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FLight";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.AircraftList)).EndInit();
            this.AircraftGroup.ResumeLayout(false);
            this.AircraftGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCallsign2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruiseSpeedValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CruiseSpeedType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFlightLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFuelMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftAltitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftFlightRule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftTypeFlight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftCallsign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftLivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftTypeModel)).EndInit();
            this.SceneryGroup.ResumeLayout(false);
            this.SceneryGroup.PerformLayout();
            this.DeparturePanel.ResumeLayout(false);
            this.DeparturePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DepartureParking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eet_hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eet_min)).EndInit();
            this.ArrivalPanel.ResumeLayout(false);
            this.ArrivalPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalStartPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalStartHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalStartMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlternateAirport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartureHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartureMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DestinationAirport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginAirport)).EndInit();
            this.FlightModeBox.ResumeLayout(false);
            this.FlightModeBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Line)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RunwayList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView AircraftList;
        private System.Windows.Forms.GroupBox AircraftGroup;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadTextBox AircraftCallsign;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private Telerik.WinControls.UI.RadDropDownList AircraftLivery;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadDropDownList AircraftTypeModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox SceneryGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadDropDownList DestinationAirport;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadDropDownList OriginAirport;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadDropDownList AircraftTypeFlight;
        private System.Windows.Forms.Label label9;
        private Telerik.WinControls.UI.RadDropDownList AircraftFlightRule;
        private Telerik.WinControls.UI.RadMaskedEditBox AircraftAltitude;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox FlightModeBox;
        private System.Windows.Forms.RadioButton ArrivalCheckBox;
        private System.Windows.Forms.RadioButton DepartureCheckBox;
        private Telerik.WinControls.UI.RadSpinEditor AircraftFuelHour;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadSpinEditor AircraftFuelMin;
        private Telerik.WinControls.UI.RadDropDownList AircraftFlightLevel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private Telerik.WinControls.UI.RadDropDownList AircraftRoute;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button RemoveButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Panel ArrivalPanel;
        private System.Windows.Forms.Button FlightPlan;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.Label label22;
        private Telerik.WinControls.UI.RadSpinEditor AircraftPerson;
        private Telerik.WinControls.UI.RadSpinEditor DepartureHour;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Telerik.WinControls.UI.RadSpinEditor DepartureMinute;
        private Telerik.WinControls.UI.RadTextBox AlternateAirport;
        private Telerik.WinControls.UI.RadSpinEditor ArrivalStartHour;
        private System.Windows.Forms.Label label25;
        private Telerik.WinControls.UI.RadSpinEditor ArrivalStartMinute;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private Telerik.WinControls.UI.RadMaskedEditBox CruiseSpeedValue;
        private Telerik.WinControls.UI.RadDropDownList CruiseSpeedType;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private Telerik.WinControls.UI.RadSpinEditor eet_hour;
        private System.Windows.Forms.Label label29;
        private Telerik.WinControls.UI.RadSpinEditor eet_min;
        private System.Windows.Forms.Panel DeparturePanel;
        private Telerik.WinControls.UI.RadDropDownList DepartureParking;
        private System.Windows.Forms.Label label21;
        private Telerik.WinControls.UI.RadTextBox ArrivalStartPoint;
        private System.Windows.Forms.PictureBox Line;
        private Telerik.WinControls.UI.RadTextBox AircraftCallsign2;
        private System.Windows.Forms.Label RunwayLbl;
        private Telerik.WinControls.UI.RadDropDownList RunwayList;
    }
}