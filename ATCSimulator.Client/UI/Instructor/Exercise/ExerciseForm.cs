﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.Helper;

namespace ATCSimulator.Client.UI.Instructor.Exercise
{
    public partial class ExerciseForm : Form
    {
        private List<ListAircraftExerciseModel> list_aircrafts;
        private List<ExerciseListModel> list_exercises;
        private ExerciseDetailModel exercise;
        private int scenery_id = 0;
        private string scenery_name;
        private string _username;
        private bool editMode;
        public ExerciseForm(string username)
        {
            InitializeComponent();
            this._username = username;
            Weather.Items.Clear();
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Clear", SimulationService.WeatherInfo.Clear));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Cloudy", SimulationService.WeatherInfo.Cloudy));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Light Rain", SimulationService.WeatherInfo.LightRain));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Thunder Storm", SimulationService.WeatherInfo.ThunderStorm));
        }
        public void Init()
        {
            
            exercise = null; 
            list_exercises = new List<ExerciseListModel>();
            SceneryListResponse sceneries = SystemConnection.SceneryList();
            if (sceneries.status.code == 0)
            {
                SceneryList.Items.Clear();
                foreach (SceneryListModel s in sceneries.sceneries)
                {
                    SceneryList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(s.name.ToLower(), s));
                }
                ClearProperties();
                EditingOperation(false);
                SetExerciseList();
            }
            
        }

        #region Function and Procedure

        #region Set Exercise List
        public void SetExerciseList()
        {
            ExerciseListResponse exercise_data = SystemConnection.ExerciseList();
            if (exercise_data.status.code == 0)
            {
                list_exercises = exercise_data.exercises.ToList();
                ExerciseTable.DataSource = list_exercises;
                ExerciseTable.Columns["id"].Visible = false;
                ExerciseTable.Columns["scenery_id"].Visible = false;
                ExerciseTable.Columns["scenery_name"].HeaderText = "Scenery";
                ExerciseTable.Columns["exercise_name"].HeaderText = "Exercise";
                ExerciseTable.Columns["play_time"].HeaderText = "Play Time";
                ExerciseTable.Columns["time_simulation"].HeaderText = "Time On Simulation";
                ExerciseTable.Columns["traffic_exercise"].HeaderText = "Traffic";
                ExerciseTable.Columns["weather"].HeaderText = "Weather";
                ExerciseTable.Columns["total_arrival"].HeaderText = "Arrival";
                ExerciseTable.Columns["total_departure"].HeaderText = "Departure";
            }
        }
        #endregion

        #region Init Scenery
        private void InitScenery(SceneryDetailModel scenery)
        {
            TaxiLight.Checked = false;
            PapiLight.Checked = false;
            TaxiLight.Enabled = scenery.taxi;
            PapiLight.Enabled = scenery.papi;

            #region Create RunwayLight
            RunwayGroup.Controls.Clear();
            for (int i = 0; i < scenery.runway_light.Count(); i++)
            {
                int separation = 15;
                int width = (RunwayGroup.Size.Width - separation) / scenery.runway_light.Count();
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = (width * i) + separation,
                    Y = 22
                };
                RunwayGroup.Controls.Add(CreateCheckBox(false, location, size, scenery.runway_light[i]));
            }

            #endregion

            #region Create ApproachLight
            ApproachGroup.Controls.Clear();
            int counterApproach = 0;
            for (int i = 0; i < scenery.approach_light.Count(); i++)
            {
                int separation = 15;
                int width = (ApproachGroup.Size.Width - separation) / (scenery.approach_light.Count() / 2);
                int locationY = 30;
                int locationX = (width * counterApproach) + separation;
                if (i % 2 == 1)
                {
                    counterApproach++;
                    locationY = 65;
                }
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = locationX,
                    Y = locationY
                };
                ApproachGroup.Controls.Add(CreateCheckBox(false, location, size, scenery.approach_light[i]));
            }
            #endregion
        }
        #endregion

        #region CreateCheckBox
        private CheckBox CreateCheckBox(bool check, Point location, Size size, string text)
        {
            CheckBox ch = new CheckBox()
            {
                Appearance = Appearance.Button,
                BackColor = System.Drawing.Color.Tomato,
                Checked = check,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Century Gothic", 12F),
                ForeColor = System.Drawing.Color.White,
                Location = location,
                Text = text,
                Size = size,
                TextAlign = ContentAlignment.MiddleCenter,
                TabStop = false
            };
            ch.FlatAppearance.BorderSize = 0;
            ch.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            return ch;
        }
        #endregion

        #region Set AircraftList
        private void SetAircraftList()
        {
            int departure = list_aircrafts.Where(d => d.is_departure).Count();
            int arrival = list_aircrafts.Where(d => !d.is_departure).Count();
            DepartureCount.Text = departure.ToString();
            ArrivalCount.Text = arrival.ToString();
            DataTable dt = new DataTable();
            BindingSource data = new BindingSource();
            var _list_aircraft_table = list_aircrafts.Select(d => new
            {
                callsign = d.callsign,
                model = d.aircraft_model,
                flight = d.is_departure ? "Departure" : "Arrival",
                aircraft_exercise_id = d.aircraft_id
            }).ToList();

            data.DataSource = _list_aircraft_table;
            AircraftList.DataSource = data;
            AircraftList.Columns["aircraft_exercise_id"].Visible = false;
            AircraftList.Columns["callsign"].HeaderText = "Callsign";
            AircraftList.Columns["model"].HeaderText = "Type";
            AircraftList.Columns["flight"].HeaderText = "Flight";
        }
        #endregion

        #region AircraftExercise
        public void AircraftExercise(List<ListAircraftExerciseModel> aircraft_data)
        {
            this.list_aircrafts = aircraft_data;
            SetAircraftList();
        }
        #endregion

        #endregion

        #region Button Handler

        #region Scenery Change
        private void SceneryList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (editMode)
            {
                if (SceneryList.SelectedIndex == -1)
                {
                    return;
                }
                if (list_aircrafts.Count > 0)
                {
                    DialogResult change_scenery = MessageBox.Show("Are you sure want to change scenery? your aircraft data will be lost",
                        "Change Scenery",
                    MessageBoxButtons.YesNo);
                    if (change_scenery.Equals(DialogResult.No))
                    {
                        return;
                    }
                    else
                    {
                        list_aircrafts.Clear();
                        scenery_id = Convert.ToInt32(SceneryList.SelectedItem.Value);
                        scenery_name = SceneryList.SelectedItem.Text;
                    }
                }
            }
                SceneryListModel scenery = HelperClient.GetDataDropDown<SceneryListModel>(SceneryList);
                SceneryDetailResponse scenery_Data = SystemConnection.SceneryDetail(scenery.id);
                //exercise = null;
                list_aircrafts = new List<ListAircraftExerciseModel>();
                scenery_id = scenery.id;
                scenery_name = scenery.name;
                //aircraft_form.LoadData(scenery_id, scenery_name);
                InitScenery(scenery_Data.scenery);
        }
        #endregion

        #region Weather Change
        private void Weather_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (Weather.SelectedIndex != -1)
            {
                HelperClient.WeatherDefault weather = new HelperClient.WeatherDefault((SimulationService.WeatherInfo)Weather.SelectedItem.Value);
                if(weather != null)
                {
                    WindDirection.Value = weather.wind_direction;
                    WindSpeed.Value = weather.wind_speed;
                    Temperature.Value = weather.temperature;
                    Visibility.Value = weather.visibility;
                }
            }
        }
        #endregion

        #region Save Exercise
        private void SaveExerciseBtn_Click(object sender, EventArgs e)
        {
            //check data sudah terisi semua
            if (ValidateData())
            {
                try
                {
                    Dictionary<string, bool> approach = new Dictionary<string, bool>();
                    for (int i = 0; i < ApproachGroup.Controls.Count; i++)
                    {
                        CheckBox ch = (CheckBox)ApproachGroup.Controls[i];
                        approach.Add(ch.Text, ch.Checked);
                    }
                    Dictionary<string, bool> runway = new Dictionary<string, bool>();
                    for (int i = 0; i < RunwayGroup.Controls.Count; i++)
                    {
                        CheckBox ch = (CheckBox)RunwayGroup.Controls[i];
                        runway.Add(ch.Text, ch.Checked);
                    }
                    List<AircraftExerciseDetailContract> _ac = new List<AircraftExerciseDetailContract>();
                    foreach (ListAircraftExerciseModel a in list_aircrafts)
                    {
                        _ac.Add(new AircraftExerciseDetailContract()
                        {
                            aircraft_id = a.aircraft_id,
                            aircraft_livery_id = a.livery_id,
                            callsign = a.callsign,
                            departure_time = a.departure_time,
                            estimate_time = a.estimate_time,
                            flight_level = a.flight_level,
                            flight_level_type = a.flight_level_type,
                            flight_rules_id = a.flight_rules_id,
                            flight_type_id = a.flight_type_id,
                            fuel = a.fuel,
                            person_on_board = a.person_on_board,
                            route_id = a.route_id,
                            arrival = a.is_departure ? null : new AircraftExerciseArrivalContract() { start_time = a.start_time.Value , runway = a.runway},
                            departure = a.is_departure ? new AircraftExerciseDepartureContract() { parking_id = a.parking_id.Value } : null
                        });
                    }

                    ExerciseDetailContract exercise_data = new ExerciseDetailContract()
                    {
                        approach = approach,
                        desc_exercise = DescExercise.Text,
                        exercise_name = ExerciseName.Text,
                        papi = PapiLight.Checked,
                        taxi = TaxiLight.Checked,
                        play_time = new TimeSpan((int)PlayTimeHour.Value, (int)PlayTimeMin.Value, 0),
                        runway_lights = runway,
                        scenery_id = scenery_id,
                        scenery_name = scenery_name,
                        temperature = (int)Temperature.Value,
                        traffic_exercise = TrafficExercise.Text,
                        time_simulation = EnvTime.Value.Value.TimeOfDay,
                        wind_speed = (int)WindSpeed.Value,
                        visibility = (int)Visibility.Value,
                        wind_direction = (int)WindDirection.Value,
                        weather = Weather.SelectedItem.Text,
                        aircrafts = _ac
                    };
                    StatusResponse response = new StatusResponse();
                    if (exercise == null)
                    {
                        ExerciseAddContract exercise_add = new ExerciseAddContract()
                        {
                            create_by = _username,
                            approach = approach,
                            desc_exercise = DescExercise.Text,
                            exercise_name = ExerciseName.Text,
                            papi = PapiLight.Checked,
                            taxi = TaxiLight.Checked,
                            play_time = new TimeSpan((int)PlayTimeHour.Value, (int)PlayTimeMin.Value, 0),
                            runway_lights = runway,
                            scenery_id = scenery_id,
                            scenery_name = scenery_name,
                            temperature = (int)Temperature.Value,
                            traffic_exercise = TrafficExercise.Text,
                            time_simulation = EnvTime.Value.Value.TimeOfDay,
                            wind_speed = (int)WindSpeed.Value,
                            visibility = (int)Visibility.Value,
                            wind_direction = (int)WindDirection.Value,
                            weather = Weather.SelectedValue.ToString(),
                            aircrafts = _ac
                        };
                        response = SystemConnection.ExerciseAdd(exercise_add);
                    }
                    else
                    {
                        ExerciseUpdateContract exercise_update = new ExerciseUpdateContract()
                        {
                            update_by = _username,
                            approach = approach,
                            desc_exercise = DescExercise.Text,
                            exercise_name = ExerciseName.Text,
                            papi = PapiLight.Checked,
                            taxi = TaxiLight.Checked,
                            play_time = new TimeSpan((int)PlayTimeHour.Value, (int)PlayTimeMin.Value, 0),
                            runway_lights = runway,
                            scenery_id = scenery_id,
                            scenery_name = scenery_name,
                            temperature = (int)Temperature.Value,
                            traffic_exercise = TrafficExercise.Text,
                            time_simulation = EnvTime.Value.Value.TimeOfDay,
                            wind_speed = (int)WindSpeed.Value,
                            visibility = (int)Visibility.Value,
                            wind_direction = (int)WindDirection.Value,
                            weather = Weather.SelectedValue.ToString(),
                            aircrafts = _ac,
                            exercise_id = exercise.id
                        };
                        response = SystemConnection.ExerciseUpdate(exercise_update);
                    }
                    if (response != null)
                    {
                        if (response.status.code == 0)
                        {
                            MessageBox.Show("Data Succesful Save");
                            SetExerciseList();
                        }
                        else
                        {
                            MessageBox.Show("Error Save Data with error : " + response.status.message);
                        }
                    }
                    ClearProperties();
                    EditingOperation(false);
                    SetExerciseList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Save Data with error : " + ex.Message);
                }
            }
        }
        #endregion

        #region Exercise Table Click
        private void ExerciseTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            if (editMode)
            {
                if (CancelOperation() == false)
                {
                    return;
                }
            }
            EditingOperation(false);
            DataGridView obj = (DataGridView)sender;
            ExerciseListModel data = (ExerciseListModel)obj.Rows[e.RowIndex].DataBoundItem;
            SceneryList.SelectedIndex = SceneryList.Items.IndexOf(data.scenery_name.ToLower());
            ExerciseDetailResponse _exercise = SystemConnection.ExerciseDetail(data.id);

            if (_exercise.status.code == 0)
            {
                exercise = _exercise.exercise;
                list_aircrafts = _exercise.exercise.aircrafts.ToList();
                #region Properties Aircraft
                PlayTimeHour.Value = _exercise.exercise.play_time.Hours;
                PlayTimeMin.Value = _exercise.exercise.play_time.Minutes;
                ExerciseName.Text = _exercise.exercise.exercise_name;
                TrafficExercise.Text = _exercise.exercise.traffic_exercise;
                DescExercise.Text = _exercise.exercise.desc_exercise;
                Weather.Items.Where(d=> d.Value.ToString().ToLower().Equals(_exercise.exercise.weather.ToLower())).FirstOrDefault().Selected = true;
                WindDirection.Value = _exercise.exercise.wind_direction;
                WindSpeed.Value = _exercise.exercise.wind_speed;
                Temperature.Value = _exercise.exercise.temperature;
                Visibility.Value = _exercise.exercise.visibility;

                EnvTime.Value = DateTime.Now.AddSeconds(_exercise.exercise.time_simulation.TotalSeconds);
                for (int i = 0; i < ApproachGroup.Controls.Count; i++)
                {
                    CheckBox ch = (CheckBox)ApproachGroup.Controls[i];
                    foreach (var a in _exercise.exercise.approach_lights)
                    {
                        if (ch.Text.Equals(a.Key))
                        {
                            ch.Checked = a.Value;
                            break;
                        }
                    }
                }
                for (int i = 0; i < RunwayGroup.Controls.Count; i++)
                {
                    CheckBox ch = (CheckBox)RunwayGroup.Controls[i];
                    foreach (var r in _exercise.exercise.runway_lights)
                    {
                        if (ch.Text.Equals(r.Key))
                        {
                            ch.Checked = r.Value;
                            break;
                        }
                    }
                }
                TaxiLight.Checked = _exercise.exercise.taxi;
                PapiLight.Checked = _exercise.exercise.papi;
                #endregion
                RemoveExerciseBtn.Enabled = true;
                SaveExerciseBtn.Text = "Update Exercise";
                SaveExerciseBtn.Enabled = false;
                CreateExercise.Enabled = true;
                //add aprron taxi and papi di isi
                SetAircraftList();
            }
        }

        private void ExerciseTable_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            RemoveExerciseBtn.Enabled = false;
            SaveExerciseBtn.Text = "Update Exercise";
            SaveExerciseBtn.Enabled = true;
            EditingOperation(true);
        }
        #endregion

        #region AddAircraft Btn
        private void AddAircraftBtn_Click(object sender, EventArgs e)
        {
            if (scenery_id > 0)
            {
                AircraftExerciseForm aircraft_form = new AircraftExerciseForm(this, null);
                aircraft_form.LoadData(scenery_id, scenery_name);
                aircraft_form.Init(list_aircrafts);
                aircraft_form.StartPosition = FormStartPosition.CenterParent;
                this.Enabled = false;
                aircraft_form.ShowDialog();

            }
            else
            {
                MessageBox.Show("Please choose scenery first");
            }
        }
        #endregion

        #region Create Exercise
        private void CreateExercise_Click(object sender, EventArgs e)
        {
            if (editMode)
            {
                if (CancelOperation() == false)
                {
                    return;
                }
            }
            EditingOperation(true);
            exercise = null;
            ClearProperties();
            CreateExercise.Enabled = false;
            SaveExerciseBtn.Enabled = true;

        }
        #endregion

        #region ClearData
        private void ClearExerciseBtn_Click(object sender, EventArgs e)
        {
            ClearProperties();
            EditingOperation(false);
            exercise = null;
        }
        #endregion

        #region Remove Data
        private void RemoveExerciseBtn_Click(object sender, EventArgs e)
        {
            if (exercise != null)
            {
                DialogResult remove_exercise = MessageBox.Show("Are you sure want to Remove This Exercise?",
                    "Remove Exercise",
                MessageBoxButtons.YesNo);
                if (remove_exercise.Equals(DialogResult.No))
                {
                    return;
                }
                StatusResponse response = SystemConnection.ExerciseRemove(exercise.id);

                if (response.status.code == 0)
                {
                    exercise = null;
                    ClearProperties();
                    SceneryList.SelectedIndex = -1;
                    SetExerciseList();
                    MessageBox.Show("Remove Success");
                }
                else
                {
                    MessageBox.Show("Fail Remove With Error " + response.status.message);
                }
            }
        }
        #endregion

        #endregion

        #region Helper

        #region Clear Properties
        private void ClearProperties()
        {
            TaxiLight.Enabled = false;
            PapiLight.Enabled = false;
            TaxiLight.Checked = false;
            PapiLight.Checked = false;
            PlayTimeHour.Value = 0;
            PlayTimeMin.Value = 0;
            SceneryList.SelectedIndex = -1;
            ExerciseName.Clear();
            TrafficExercise.Clear();
            DescExercise.Clear();
            Weather.SelectedIndex = -1;
            WindDirection.Value = 0;
            WindSpeed.Value = 0;
            Temperature.Value = 0;
            Visibility.Value = 0;
            RunwayGroup.Controls.Clear();
            ApproachGroup.Controls.Clear();
            EnvTime.Value = DateTime.Now;
            scenery_id = 0;
            scenery_name = null;
            RemoveExerciseBtn.Enabled = false;
            SaveExerciseBtn.Enabled = false;
            SaveExerciseBtn.Text = "Save Exercise";
            CreateExercise.Enabled = true;
            list_aircrafts = new List<ListAircraftExerciseModel>();
            SetAircraftList();
        }
        #endregion

        #region Editing Operation
        private void EditingOperation(bool status)
        {
            PlayTimeHour.Enabled = status;
            PlayTimeMin.Enabled = status;
            SceneryList.Enabled = status;
            ExerciseName.Enabled = status;
            TrafficExercise.Enabled = status;
            DescExercise.Enabled = status;
            EnvPropGroup.Enabled = status;
            LightsGroup.Enabled = status;
            AircraftPropGroup.Enabled = status;
            editMode = status;
        }
        #endregion

        #region Validation Save button
        public bool ValidateData()
        {
            if (SceneryList.SelectedIndex < 0)
            {
                MessageBox.Show("Scenery Cannot Be Empty Please Choose One");
                return false;
            }
            if (((int)PlayTimeHour.Value == 0) && ((int)PlayTimeMin.Value < 10))
            {
                MessageBox.Show("Play Time Cannot Be Less than 10 min");
                return false;
            }
            if (string.IsNullOrWhiteSpace(ExerciseName.Text))
            {
                MessageBox.Show("Excercise Name Cannot Be Empty or Whitespace");
                return false;
            }

            if (list_aircrafts.Count < 1)
            {
                MessageBox.Show("Please add your aircraft for this exercise");
                return false;
            }
            if (Weather.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose default weather");
                return false;
            }
            return true;
        }
        #endregion

        #region Cancel Operation
        private bool CancelOperation()
        {
            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("You are in the middle of editing/inputing, " +
                    "Are you sure to cancel your operation?", "Cancel Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)  // error is here
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #endregion


       

       
        
    }
}


