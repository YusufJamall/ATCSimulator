﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls;
using ATCSimulator.Client.Helper;
using Newtonsoft.Json;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.UI.Other;
using ATCSimulator.Client.UI.Instructor.Simulation;

namespace ATCSimulator.Client.UI.Instructor.Exercise
{
    public partial class AircraftExerciseForm : Telerik.WinControls.UI.RadForm
    {

        private int scenery_id;
        private string scenery_name;
        private bool editMode;


        private List<AircraftModel> aircraft_list;
        private List<FlightRuleDetailModel> flight_rules;
        private AircraftModel aircraft_detail;
        private List<ListAircraftExerciseModel> list_aircraft_exercise;
        private List<FlightTypeDetailModel> flight_types;
        private List<AirportAppronModel> approns;
        private List<AirportDetailModel> airports;
        private List<FlightRouteDetailModel> departure_route;
        private List<FlightRouteDetailModel> arrival_route;
        private List<RunwayDetailModel> runways;
        private ListAircraftExerciseModel aircraft;
        private SimulationForm _simulation_form;
        private ExerciseForm _exercise_form;
        public AircraftExerciseForm(ExerciseForm exercise_form, SimulationForm simulation_form)
        {
            InitializeComponent();
            if (exercise_form != null)
            {
                this._exercise_form = exercise_form;
                CreateButton.Show();
                SaveButton.Show();
                ClearButton.Show();
                RemoveButton.Show();
            }
            if (simulation_form != null)
            {
                this._simulation_form = simulation_form;
                CreateButton.Hide();
                SaveButton.Hide();
                ClearButton.Hide();
                RemoveButton.Hide();
            }
            //this.Location = new Point { X = exercise_form.Location.X + ((exercise_form.Width - this.Width) / 2), Y = 0 };
        }

        public void LoadData(int scenery, string scenery_name)
        {
            this.scenery_id = scenery;
            this.scenery_name = scenery_name.ToUpper();
            try
            {
                AircraftListResponse _aircrafts = SystemConnection.AircraftList();
                aircraft_list = _aircrafts.aircrafts.ToList();

                FlightRuleListResponse _flight_rule = SystemConnection.FlightRuleList();
                flight_rules = _flight_rule.flight_rules.ToList();

                FlightTypeListResponse _flight_type = SystemConnection.FlightTypeList();
                flight_types = _flight_type.flight_types.ToList();

                AirportAppronListResponse _appron = SystemConnection.AirportAppronList(scenery_id);
                approns = _appron.approns.ToList();

                AirportListResponse _airports = SystemConnection.AirportList(scenery_id);
                airports = _airports.airports.ToList();

                RunwayListResponse _runways = SystemConnection.RunwayList(scenery_id);
                runways = _runways.runways.ToList();

                FlightRouteListResponse _departure_routes = SystemConnection.FlightRouteList(true, scenery_id);
                FlightRouteListResponse _arrival_routes = SystemConnection.FlightRouteList(false, scenery_id);
                departure_route = _departure_routes.flight_routes.ToList();
                arrival_route = _arrival_routes.flight_routes.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Some Data Return Null , error: " + ex.Message);
            }
        }

        #region Function and Procedure

        #region Init Form
        public void Init(List<ListAircraftExerciseModel> aircrafts_data)
        {
            this.list_aircraft_exercise = aircrafts_data;
            AircraftTypeModel.Items.Clear();
            AircraftFlightRule.Items.Clear();
            AircraftTypeFlight.Items.Clear();
            DepartureParking.Items.Clear();
            this.aircraft = null;
            foreach (AircraftModel a in aircraft_list)
            {
                if (a.available_scenery.Split(',').Where(d => d.Equals(scenery_id.ToString())).Count() > 0)
                    AircraftTypeModel.Items.Add(new Telerik.WinControls.UI.RadListDataItem(a.model, a));
            }

            foreach (FlightRuleDetailModel r in flight_rules)
            {
                AircraftFlightRule.Items.Add(new Telerik.WinControls.UI.RadListDataItem(r.name, r));
            }

            foreach (FlightTypeDetailModel t in flight_types)
            {
                string text = string.Format("{0}-{1}", t.name, t.description);
                AircraftTypeFlight.Items.Add(new Telerik.WinControls.UI.RadListDataItem(text, t));
            }

            //foreach (AirportAppronModel a in approns)
            //{
            //    string text = string.Format("{0} ({1})", a.terminal_name, a.appron_name);
            //    DepartureParking.Items.Add(new Telerik.WinControls.UI.RadListDataItem(text, a));
            //}
            DepartureCheckBox.Checked = true;
            ResetForm();
            SetAircraftList();
        }
        #endregion

        #region Aircraft List Table
        public void SetAircraftList()
        {
            DataTable dt = new DataTable();
            BindingSource data = new BindingSource();
            List<string> tes = new List<string>();

            List<AircraftListGridTable> _list_aircraft_table = list_aircraft_exercise.Select(d => new AircraftListGridTable()
            {
                properties = d,
                callsign = d.callsign,
                model = d.aircraft_model,
                flight = d.is_departure ? "Departure" : "Arrival",
                flight_rule = d.flight_rule_name,
                origin = d.origin,
                destination = d.destination,
                departure_time = d.departure_time.ToString("c")
            }).ToList();

            data.DataSource = _list_aircraft_table;
            AircraftList.DataSource = data;
            AircraftList.Columns["properties"].Visible = false;
            AircraftList.Columns["callsign"].HeaderText = "Callsign";
            AircraftList.Columns["model"].HeaderText = "Type";
            AircraftList.Columns["flight"].HeaderText = "Flight";
            AircraftList.Columns["flight_rule"].HeaderText = "Rule";
            AircraftList.Columns["origin"].HeaderText = "Departure";
            AircraftList.Columns["destination"].HeaderText = "Destination";
            AircraftList.Columns["departure_time"].HeaderText = "Depart Time";
            //ResetParking();
        }
        #endregion

        #endregion

        #region Button Handler

        #region Flight Mode Change
        private void FlightModeChanged(object sender, EventArgs e)
        {
            DeparturePanel.Visible = DepartureCheckBox.Checked;
            ArrivalPanel.Visible = ArrivalCheckBox.Checked;
            OriginAirport.Items.Clear();
            DestinationAirport.Items.Clear();
            AlternateAirport.Clear();
            AircraftRoute.Items.Clear();
            eet_hour.Value = 0;
            eet_min.Value = 0;
            AircraftFlightRule.SelectedIndex = -1;
            if (DepartureCheckBox.Checked && airports.Count > 0)
            {
                AirportDetailModel origin = airports.Where(d => d.airport_name.ToLower().Equals(scenery_name.ToLower())).FirstOrDefault();
                if (origin != null)
                {
                    OriginAirport.Items.Add(new Telerik.WinControls.UI.RadListDataItem(origin.airport_name, origin));
                    OriginAirport.SelectedIndex = 0;
                }
                if (aircraft_detail != null)
                    AircraftFlightRule.Items.Where(d => d.Text.ToLower().Equals("local")).FirstOrDefault().Enabled = aircraft_detail.type.ToLower().Equals("training") ? true : false;
                else
                    AircraftFlightRule.Items.Where(d => d.Text.ToLower().Equals("local")).FirstOrDefault().Enabled = true;
                AircraftRoute.Width = 458;
                RunwayLbl.Visible = false;
                RunwayList.Visible = false;
            }
            if (ArrivalCheckBox.Checked)
            {
                OriginAirport.Items.Clear();
                AircraftFlightRule.Items.Where(d => d.Text.ToLower().Equals("local")).FirstOrDefault().Enabled = false;
                AircraftRoute.Width = 323;
                RunwayLbl.Visible = true;
                RunwayList.Visible = true;
            }
            OriginAirport.Enabled = ArrivalCheckBox.Checked;

        }
        #endregion

        #region Aircraft Model Index Change
        private void aircraft_model_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (AircraftTypeModel.SelectedIndex == -1)
            {
                return;
            }
            AircraftFlightRule.SelectedIndex = -1;
            AircraftModel data = HelperClient.GetDataDropDown<AircraftModel>(AircraftTypeModel);
            AircraftDetailResponse _aircraft = SystemConnection.AircraftDetail(data.id);
            aircraft_detail = _aircraft.aircraft;
            if (aircraft_detail != null)
                AircraftFlightRule.Items.Where(d => d.Text.ToLower().Equals("local")).FirstOrDefault().Enabled = aircraft_detail.type.ToLower().Equals("training") ? true : false;
            DepartureParking.Items.Clear();
            RunwayList.Items.Clear();
            RunwayList.Enabled = false;
            List<string> notAvailableTerminal = !string.IsNullOrEmpty(aircraft_detail.notAvailable_airport) ? aircraft_detail.notAvailable_airport.Split(',').ToList() : new List<string>();
            foreach (AirportAppronModel a in approns)
            {
                if (notAvailableTerminal.Where(d => d.Contains(a.id.ToString())).Count() == 0)
                {
                    string text = string.Format("{0} ({1})", a.terminal_name, a.appron_name);
                    DepartureParking.Items.Add(new Telerik.WinControls.UI.RadListDataItem(text, a));
                }
            }
            ResetParking();
            ClearProperties(false);
            AircraftPerson.Maximum = aircraft_detail.maximum_person;

            LiveryListResponse liveries = SystemConnection.LiveryList(aircraft_detail.id);

            if (liveries.status.code == 0)
            {
                foreach (LiveryDetailModel a in liveries.liveries)
                {
                    AircraftLivery.Items.Add(new Telerik.WinControls.UI.RadListDataItem(a.livery, a));
                }
            }

        }
        #endregion

        #region Aircraft Livery Change
        private void AircraftLivery_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

            if (AircraftLivery.SelectedIndex == -1)
            {
                return;
            }
            LiveryDetailModel livery = HelperClient.GetDataDropDown<LiveryDetailModel>(AircraftLivery);
            AircraftCallsign.Text = livery.callsign;
        }
        #endregion

        #region Aircraft Flight Rule Index Change
        private void AircraftFlightRule_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (AircraftFlightRule.SelectedIndex == -1 || aircraft_detail == null)
            {
                return;
            }
            FlightRuleDetailModel rules = HelperClient.GetDataDropDown<FlightRuleDetailModel>(AircraftFlightRule);
            DestinationAirport.Items.Clear();
            AlternateAirport.Clear();
            RunwayList.Items.Clear();
            RunwayList.Enabled = false;
            eet_hour.Value = 0;
            eet_min.Value = 0;
            AircraftRoute.Items.Clear();
            ArrivalStartPoint.Clear();
            //get airport list
            if (DepartureCheckBox.Checked)
            {
                var test = departure_route.Where(d => d.flight_rules_id.Equals(rules.id)).GroupBy(d => d.destination_airport_id).ToList();
                List<AirportDetailModel> _airports = departure_route.Where(d => d.flight_rules_id.Equals(rules.id)).GroupBy(d => d.destination_airport_id).Select(d => d.First()).ToList().Select(d => new AirportDetailModel()
                {
                    airport_code = d.destination_airport_code,
                    airport_name = d.destination_airport_name,
                    id = d.destination_airport_id,
                    scenery_id = scenery_id
                }).ToList();
                foreach (AirportDetailModel d in _airports)
                {
                    DestinationAirport.Items.Add(new Telerik.WinControls.UI.RadListDataItem(d.airport_name, d));
                }

            }
            else
            {
                List<AirportDetailModel> origin = arrival_route.Where(d => d.flight_rules_id.Equals(rules.id)).GroupBy(d => d.origin_airport_id).Select(d => d.First()).ToList().Select(d => new AirportDetailModel()
                {
                    airport_code = d.origin_airport_code,
                    airport_name = d.origin_airport_name,
                    id = d.destination_airport_id,
                    scenery_id = scenery_id
                }).ToList();
                List<AirportDetailModel> destination = arrival_route.Where(d => d.flight_rules_id.Equals(rules.id)).GroupBy(d => d.destination_airport_id).Select(d => d.First()).ToList().Select(d => new AirportDetailModel()
                {
                    airport_code = d.destination_airport_code,
                    airport_name = d.destination_airport_name,
                    id = d.destination_airport_id,
                    scenery_id = scenery_id
                }).ToList();
                foreach (AirportDetailModel d in destination)
                {
                    DestinationAirport.Items.Add(new Telerik.WinControls.UI.RadListDataItem(d.airport_name, d));
                }
                foreach (AirportDetailModel d in origin)
                {
                    OriginAirport.Items.Add(new Telerik.WinControls.UI.RadListDataItem(d.airport_name, d));
                }
            }
        }
        #endregion

        #region Airport Route Change Index
        private void AirportRoute_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (DestinationAirport.SelectedIndex == -1 || OriginAirport.SelectedIndex == -1 || AircraftFlightRule.SelectedIndex == -1 || aircraft_detail == null)
            {
                return;
            }
            if (!AircraftFlightRule.Items.Where(d => d.Text.ToLower().Equals("local")).FirstOrDefault().Selected)
            {
                if (DestinationAirport.Text.Equals(OriginAirport.Text))
                {
                    MessageBox.Show("Destination and Origin Airport Cannot be same");
                    return;
                }
            }
            AircraftRoute.Items.Clear();

            AirportDetailModel dest_data = HelperClient.GetDataDropDown<AirportDetailModel>(DestinationAirport);
            AirportDetailModel origin_data = HelperClient.GetDataDropDown<AirportDetailModel>(OriginAirport);
            FlightRuleDetailModel rules = HelperClient.GetDataDropDown<FlightRuleDetailModel>(AircraftFlightRule);

            if (DepartureCheckBox.Checked)
            {
                List<FlightRouteDetailModel> _departure_route = departure_route.Where(d => d.destination_airport_id.Equals(dest_data.id) && d.flight_rules_id.Equals(rules.id)).ToList();
                foreach (FlightRouteDetailModel d in _departure_route)
                {
                    string route = d.routes_name.Aggregate((i, j) => i + " " + j);
                    AircraftRoute.Items.Add(new Telerik.WinControls.UI.RadListDataItem(route, d));
                }

            }
            else
            {
                List<FlightRouteDetailModel> _arrival_route = arrival_route.Where(d => d.destination_airport_id.Equals(dest_data.id) && d.flight_rules_id.Equals(rules.id)).ToList();
                foreach (FlightRouteDetailModel d in _arrival_route)
                {
                    string route = d.routes_name.Aggregate((i, j) => i + " " + j);
                    AircraftRoute.Items.Add(new Telerik.WinControls.UI.RadListDataItem(route, d));
                }
            }
        }
        #endregion

        #region Cruise Speed
        private void CruiseSpeedType_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (CruiseSpeedType.SelectedIndex == -1 || aircraft_detail == null)
            {
                return;
            }
            double value_speed = aircraft_detail.true_speed;
            double value = HelperClient.ConvertSpeed(CruiseSpeedType.Text, value_speed);
            CruiseSpeedValue.Text = value.ToString();
        }
        #endregion

        #region Aircraft Route
        private void AircraftRoute_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (AircraftRoute.SelectedIndex == -1)
            {
                return;
            }
            eet_hour.Value = 0;
            eet_min.Value = 0;
            ArrivalStartPoint.Clear();
            AlternateAirport.Clear();
            FlightRouteDetailModel routes_data = HelperClient.GetDataDropDown<FlightRouteDetailModel>(AircraftRoute);

            RunwayList.Items.Clear();
            RunwayList.Enabled = false;
            bool is_filter = !aircraft_detail.type.ToLower().Equals("helicopter");
            if (routes_data.runways.Count > 0)
            {
                RunwayList.Enabled = true;
                foreach (string item in routes_data.runways)
                {
                    if (is_filter)
                    {
                        var helicopter_runway = runways.Where(d => d.is_helicopter);
                        if (helicopter_runway.Where(d => d.runway_name.ToLower().Contains(item.ToLower())).Count() == 0)
                        {
                            RunwayList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(item, item));
                        }
                    }
                    else
                    {
                        RunwayList.Items.Add(new Telerik.WinControls.UI.RadListDataItem(item, item));
                    }

                }
            }
            else
            {
                RunwayList.Enabled = false;
            }
            AlternateAirport.Text = routes_data.alternate_airport_name;
            double dist = routes_data.total_distance;
            double speed = aircraft_detail.true_speed;
            double eet = HelperClient.GetEET(dist, speed);
            TimeSpan time_eet = TimeSpan.FromMinutes(eet);
            eet_hour.Value = time_eet.Hours;
            eet_min.Value = time_eet.Minutes;
            //tambah 30 min dari time eet
            TimeSpan fuel_board = TimeSpan.FromMinutes(eet + 30);
            AircraftFuelHour.Value = fuel_board.Hours;
            AircraftFuelMin.Value = fuel_board.Minutes;

            if (ArrivalCheckBox.Checked)
                ArrivalStartPoint.Text = routes_data.waypoint_name;


        }
        #endregion

        #region Show Flight Plan
        private void FlightPlan_Click(object sender, EventArgs e)
        {
            if (ValidateOther())
            {


                FlightPlanModel data = new FlightPlanModel();
                data.flight_rules = AircraftFlightRule.Text;
                data.callsign = AircraftCallsign.Text;
                if (aircraft_detail != null)
                {
                    data.type_aircraft = aircraft_detail.model;
                    data.true_speed = HelperClient.ConvertSpeed("N", aircraft_detail.true_speed);
                }
                data.flight_type = AircraftTypeFlight.Text;
                AirportDetailModel origin = HelperClient.GetDataDropDown<AirportDetailModel>(OriginAirport);
                if (origin != null)
                {
                    data.origin_airport_code = origin.airport_code;
                    data.origin_airport_name = origin.airport_name;
                }
                data.departure_time = new TimeSpan((int)DepartureHour.Value, (int)DepartureMinute.Value, 0);
                data.altitude = double.Parse(AircraftAltitude.Text);
                data.flight_level = AircraftFlightLevel.Text;
                data.routes = AircraftRoute.Text;

                AirportDetailModel destination = HelperClient.GetDataDropDown<AirportDetailModel>(DestinationAirport);
                if (origin != null)
                {
                    data.origin_airport_code = destination.airport_code;
                    data.origin_airport_name = destination.airport_name;
                }

                data.eet = new TimeSpan((int)eet_hour.Value, (int)eet_min.Value, 0);
                data.fuel_board = new TimeSpan((int)AircraftFuelHour.Value, (int)AircraftFuelMin.Value, 0);
                data.alternate_airport_name = AlternateAirport.Text;

                ////data.pilot = null;
                data.person = (int)AircraftPerson.Value;
                LiveryDetailModel livery = HelperClient.GetDataDropDown<LiveryDetailModel>(AircraftLivery);
                data.strips = livery != null ? livery.aircraft_strips : string.Empty;
                FlightPlanForm flight_plan = new FlightPlanForm(data);
                flight_plan.StartPosition = FormStartPosition.CenterScreen;
                flight_plan.ShowDialog();
            }
        }
        #endregion

        #region Create Aircraft
        private void CreateButton_Click(object sender, EventArgs e)
        {
            if (editMode)
            {
                if (CancelOperation() == false)
                {
                    return;
                }
            }
            aircraft = null;
            RemoveButton.Enabled = false;
            CreateButton.Enabled = false;
            SaveButton.Enabled = true;
            SaveButton.Text = "Save";
            ClearProperties(true);
            EditingOperation(true);
        }
        #endregion

        #region ClearButton
        private void ClearButton_Click(object sender, EventArgs e)
        {
            ClearProperties(true);
        }
        #endregion

        #region Save button
        private void SaveButton_Click(object sender, EventArgs e)
        {
            //check data sudah terisi semua
            if (SaveButton.Text.Equals("Save") ? ValidateData() : ValidateOther())
            {
                bool isCreate = false;
                if (aircraft == null)
                {
                    aircraft = new ListAircraftExerciseModel();
                    isCreate = true;
                }
                aircraft.callsign = AircraftCallsign.Text + " " + AircraftCallsign2.Text;
                aircraft.aircraft_model = aircraft_detail.model;
                aircraft.destination = DestinationAirport.Text.ToString();
                aircraft.departure_time = new TimeSpan((int)DepartureHour.Value, (int)DepartureMinute.Value, 0);
                aircraft.estimate_time = new TimeSpan((int)eet_hour.Value, (int)eet_min.Value, 0);
                aircraft.flight_level = int.Parse(AircraftAltitude.Text);
                aircraft.flight_level_type = AircraftFlightLevel.Text;
                FlightRuleDetailModel _flight_rule = HelperClient.GetDataDropDown<FlightRuleDetailModel>(AircraftFlightRule);
                FlightTypeDetailModel _flight_type = HelperClient.GetDataDropDown<FlightTypeDetailModel>(AircraftTypeFlight);
                aircraft.flight_rule_name = _flight_rule.name;
                aircraft.flight_rules_id = _flight_rule.id;
                aircraft.flight_type_id = _flight_type.id;
                aircraft.fuel = new TimeSpan((int)AircraftFuelHour.Value, (int)AircraftFuelMin.Value, 0);
                aircraft.aircraft_id = aircraft_detail.id;
                aircraft.origin = OriginAirport.Text.ToString();
                if (DepartureCheckBox.Checked)
                {
                    AirportAppronModel _appron = HelperClient.GetDataDropDown<AirportAppronModel>(DepartureParking);
                    aircraft.parking_id = _appron.id;
                    aircraft.is_departure = true;

                }
                else
                {
                    aircraft.start_time = new TimeSpan((int)ArrivalStartHour.Value, (int)ArrivalStartMinute.Value, 0);
                    aircraft.is_departure = false;
                    aircraft.runway = RunwayList.Text;
                }
                aircraft.is_departure = DepartureCheckBox.Checked;
                LiveryDetailModel _livery = HelperClient.GetDataDropDown<LiveryDetailModel>(AircraftLivery);
                aircraft.livery_id = _livery.id;
                aircraft.livery_name = _livery.livery;
                aircraft.person_on_board = (int)AircraftPerson.Value;
                FlightRouteDetailModel _routes = HelperClient.GetDataDropDown<FlightRouteDetailModel>(AircraftRoute);

                aircraft.route_id = _routes.id;
                if (isCreate)
                    list_aircraft_exercise.Add(aircraft);
                SetAircraftList();
                ResetForm();
            }
        }
        #endregion

        #region AIrcraft List Click

        private void AircraftList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            if (editMode)
            {
                if (CancelOperation() == false)
                {
                    return;
                }
            }
            DataGridView obj = (DataGridView)sender;
            AircraftListGridTable data = (AircraftListGridTable)obj.Rows[e.RowIndex].DataBoundItem;

            // split callsign
            string[] callsign = data.properties.callsign.Split(new char[] { ' ' });

            DepartureCheckBox.Checked = data.properties.is_departure;
            ArrivalCheckBox.Checked = !data.properties.is_departure;
            AircraftTypeModel.SelectedIndex = AircraftTypeModel.Items.IndexOf(data.properties.aircraft_model);
            AircraftLivery.SelectedIndex = AircraftLivery.Items.IndexOf(data.properties.livery_name);
            AircraftCallsign.Text = callsign[0];
            AircraftCallsign2.Text = callsign[1];
            AircraftFlightLevel.SelectedIndex = AircraftFlightLevel.Items.IndexOf(data.properties.flight_level_type);
            AircraftAltitude.Text = data.properties.flight_level.ToString();
            AircraftFlightRule.SelectedIndex = AircraftFlightRule.Items.IndexOf(data.properties.flight_rule_name);
            AircraftFuelHour.Value = data.properties.fuel.Hours;
            AircraftFuelMin.Value = data.properties.fuel.Minutes;
            FlightTypeDetailModel _flight_type = flight_types.Where(d => d.id.Equals(data.properties.flight_type_id)).FirstOrDefault();
            string flight_type = string.Format("{0}-{1}", _flight_type.name, _flight_type.description);
            AircraftTypeFlight.SelectedIndex = AircraftTypeFlight.Items.IndexOf(flight_type);
            AircraftPerson.Value = data.properties.person_on_board;
            List<string> routes = new List<string>();
            if (data.properties.is_departure)
            {
                AirportAppronModel _appron = approns.Where(d => d.id.Equals(data.properties.parking_id)).FirstOrDefault();
                string parking = string.Format("{0} ({1})", _appron.terminal_name, _appron.appron_name);
                DepartureParking.SelectedIndex = DepartureParking.Items.IndexOf(parking);
                DestinationAirport.SelectedIndex = DestinationAirport.Items.IndexOf(data.destination);
                routes = departure_route.Where(d => d.id.Equals(data.properties.route_id)).FirstOrDefault().routes_name.ToList();
            }
            else
            {
                OriginAirport.SelectedIndex = OriginAirport.Items.IndexOf(data.origin);
                ArrivalStartHour.Value = data.properties.start_time.Value.Hours;
                ArrivalStartMinute.Value = data.properties.start_time.Value.Minutes;
                DestinationAirport.SelectedIndex = DestinationAirport.Items.IndexOf(data.destination);
                routes = arrival_route.Where(d => d.id.Equals(data.properties.route_id)).FirstOrDefault().routes_name.ToList();
            }
            string route = routes.Aggregate((i, j) => i + " " + j);
            AircraftRoute.SelectedIndex = AircraftRoute.Items.IndexOf(route);
            RunwayList.SelectedIndex = RunwayList.Items.IndexOf(data.properties.runway);
            DepartureHour.Value = data.properties.departure_time.Hours;
            DepartureMinute.Value = data.properties.departure_time.Minutes;
            aircraft = data.properties;
            RemoveButton.Enabled = true;

            SaveButton.Enabled = false;
            SaveButton.Text = "Update";
            CreateButton.Enabled = true;
            EditingOperation(false);
            if (_simulation_form != null)
            {
                FlightPlan.Enabled = true;
            }
        }
        private void AircraftList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (_simulation_form == null)
            {
                SaveButton.Enabled = true;
                EditingOperation(true);

            }
        }
        #endregion

        #region Remove Button
        private void RemoveButton_Click(object sender, EventArgs e)
        {
            if (list_aircraft_exercise.Count > 0)
            {
                list_aircraft_exercise.Remove(aircraft);
                aircraft = null;
                SetAircraftList();
                ResetForm();
            }
        }
        #endregion

        #region Close Form
        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (_exercise_form != null)
            {
                _exercise_form.AircraftExercise(list_aircraft_exercise);
                _exercise_form.Enabled = true;
            }
            if (_simulation_form != null)
            {
                _simulation_form.AircraftExercise(list_aircraft_exercise);
                _simulation_form.Enabled = true;
            }
            this.Close();
        }

        #endregion


        #endregion

        #region Helper

        #region ClearProperties
        public void ClearProperties(bool clear)
        {
            AircraftCallsign.Clear();
            AircraftCallsign2.Clear();
            AircraftLivery.Items.Clear();
            AircraftFlightRule.SelectedIndex = -1;
            CruiseSpeedValue.Text = "0";
            CruiseSpeedType.SelectedIndex = 0;
            AircraftFlightLevel.SelectedIndex = 0;
            eet_hour.Value = 0;
            eet_min.Value = 0;
            if (ArrivalCheckBox.Checked)
                OriginAirport.Items.Clear();
            DestinationAirport.Items.Clear();
            AircraftRoute.Items.Clear();
            AlternateAirport.Text = string.Empty;
            if (clear)
            {
                AircraftTypeModel.SelectedIndex = -1;
                AircraftAltitude.Text = "0";
                AircraftTypeFlight.SelectedIndex = -1;
                AircraftPerson.Value = 1;
                AircraftFuelHour.Value = 0;
                AircraftFuelMin.Value = 0;
                DepartureParking.SelectedIndex = -1;
                DepartureHour.Value = 0;
                DepartureMinute.Value = 0;
                ArrivalStartHour.Value = 0;
                ArrivalStartMinute.Value = 0;
                ArrivalStartPoint.Clear();
                aircraft_detail = null;
                DepartureParking.Items.Clear();
                RunwayList.Items.Clear();
                RunwayList.Enabled = false;
            }
        }
        #endregion

        #region Editing Operation
        private void EditingOperation(bool status)
        {
            AircraftGroup.Enabled = status;
            FlightModeBox.Enabled = status;
            SceneryGroup.Enabled = status;
            FlightPlan.Enabled = status;
            ClearButton.Enabled = status;
            editMode = status;
            if (editMode)
            {
                //resetparking
                //ResetParking();
            }
        }
        #endregion

        #region Reset Form
        private void ResetForm()
        {

            ClearProperties(true);
            EditingOperation(false);
            CreateButton.Enabled = true;
            FlightPlan.Enabled = false;
            SaveButton.Enabled = false;
            SaveButton.Text = "Save";
            RemoveButton.Enabled = false;
        }
        #endregion

        #region Reset Parking
        private void ResetParking()
        {
            var parking_list = DepartureParking.Items.ToList();
            foreach (var item in parking_list)
            {
                item.Enabled = true;
            }
            foreach (ListAircraftExerciseModel p in list_aircraft_exercise)
            {
                if (p.is_departure)
                {
                    AirportAppronModel value = approns.Where(d => d.id.Equals(p.parking_id)).FirstOrDefault();
                    if (value != null)
                    {
                        var radList = DepartureParking.Items.Where(d => d.Value.Equals(value)).FirstOrDefault();
                        if (radList != null)
                            radList.Enabled = false;
                    }

                }
            }
            if (aircraft != null)
            {
                if (aircraft.is_departure)
                {
                    AirportAppronModel selected = approns.Where(d => d.id.Equals(aircraft.parking_id)).FirstOrDefault();
                    if (selected != null)
                    {
                        var radList = DepartureParking.Items.Where(d => d.Value.Equals(selected)).FirstOrDefault();
                        if (radList != null)
                            radList.Enabled = true;
                    }

                }
            }

        }
        #endregion

        #region Validation Save button
        public bool ValidateData()
        {
            if (AircraftTypeModel.SelectedIndex < 0)
            {
                MessageBox.Show("Aircraft Type Cannot Be Empty Please Choose One");
                return false;
            }
            if (AircraftLivery.SelectedIndex < 0)
            {
                MessageBox.Show("Aircraft Livery Cannot Be Empty Please Choose One");
                return false;
            }
            if (string.IsNullOrWhiteSpace(AircraftCallsign.Text))
            {
                MessageBox.Show("Callsign Cannot Be Empty or Whitespace");
                return false;
            }
            if (AircraftFlightLevel.SelectedIndex < 0)
            {
                MessageBox.Show("Level Altitude Cannot Be Empty Please Choose One");
                return false;
            }
            if (int.Parse(AircraftAltitude.Text) < 1)
            {
                MessageBox.Show("Altitude must be greater than 0");
                return false;
            }
            if (AircraftFlightRule.SelectedIndex < 0)
            {
                MessageBox.Show("Flight Rules Cannot Be Empty Please Choose One");
                return false;
            }
            if (AircraftTypeFlight.SelectedIndex < 0)
            {
                MessageBox.Show("Type of Flight Cannot Be Empty Please Choose One");
                return false;
            }
            if (((int)AircraftFuelHour.Value == 0) && ((int)AircraftFuelMin.Value < 10))
            {
                MessageBox.Show("Fuel Aircraft Cannot Be Less than 10 min");
                return false;
            }
            if (OriginAirport.SelectedIndex < 0)
            {
                MessageBox.Show("Origin Airport Cannot Be Empty Please Choose One");
                return false;
            }
            if (DestinationAirport.SelectedIndex < 0)
            {
                MessageBox.Show("Destination Airport Cannot Be Empty Please Choose One");
                return false;
            }
            if (AircraftRoute.SelectedIndex < 0)
            {
                MessageBox.Show("Route Cannot Be Empty Please Choose One");
                return false;
            }
            if (DepartureCheckBox.Checked && DepartureParking.SelectedIndex < 0)
            {
                MessageBox.Show("Parking Position Cannot Be Empty Please Choose One");
                return false;
            }
            if (ArrivalCheckBox.Checked && string.IsNullOrEmpty(ArrivalStartPoint.Text))
            {
                MessageBox.Show("Start Position Cannot Be Empty Please Choose One");
                return false;
            }
            if (RunwayList.Visible)
            {
                if (RunwayList.Enabled)
                    if (string.IsNullOrEmpty(RunwayList.Text))
                        MessageBox.Show("Runway Cannot Be Empty Please Choose One");
            }

            int number;
            if (!int.TryParse(AircraftCallsign2.Text, out number) || AircraftCallsign2.Text.Contains(" ") || AircraftCallsign2.Text.Length > 4)
            {
                MessageBox.Show("Callsign Id is number only with max 4 digit number and cannot contain space");
                return false;
            }
            else
            {
                foreach (var item in list_aircraft_exercise)
                {
                    if (item.callsign.Equals(AircraftCallsign.Text + " " + AircraftCallsign2.Text))
                    {
                        MessageBox.Show("Callsign already used, please choose different callsign");
                        return false;
                    }
                }
            }

            return true;
        }
        #endregion

        #region Validation FlightPlan
        public bool ValidateOther()
        {
            if (AircraftTypeModel.SelectedIndex < 0)
            {
                MessageBox.Show("Aircraft Type Cannot Be Empty Please Choose One");
                return false;
            }
            if (AircraftLivery.SelectedIndex < 0)
            {
                MessageBox.Show("Aircraft Livery Cannot Be Empty Please Choose One");
                return false;
            }
            if (string.IsNullOrWhiteSpace(AircraftCallsign.Text))
            {
                MessageBox.Show("Callsign Cannot Be Empty or Whitespace");
                return false;
            }
            if (AircraftFlightLevel.SelectedIndex < 0)
            {
                MessageBox.Show("Level Altitude Cannot Be Empty Please Choose One");
                return false;
            }
            if (int.Parse(AircraftAltitude.Text) < 1)
            {
                MessageBox.Show("Altitude must be greater than 0");
                return false;
            }
            if (AircraftFlightRule.SelectedIndex < 0)
            {
                MessageBox.Show("Flight Rules Cannot Be Empty Please Choose One");
                return false;
            }
            if (AircraftTypeFlight.SelectedIndex < 0)
            {
                MessageBox.Show("Type of Flight Cannot Be Empty Please Choose One");
                return false;
            }
            if (((int)AircraftFuelHour.Value == 0) && ((int)AircraftFuelMin.Value < 10))
            {
                MessageBox.Show("Fuel Aircraft Cannot Be Less than 10 min");
                return false;
            }
            if (OriginAirport.SelectedIndex < 0)
            {
                MessageBox.Show("Origin Airport Cannot Be Empty Please Choose One");
                return false;
            }
            if (DestinationAirport.SelectedIndex < 0)
            {
                MessageBox.Show("Destination Airport Cannot Be Empty Please Choose One");
                return false;
            }
            if (AircraftRoute.SelectedIndex < 0)
            {
                MessageBox.Show("Route Cannot Be Empty Please Choose One");
                return false;
            }
            if (DepartureCheckBox.Checked && DepartureParking.SelectedIndex < 0)
            {
                MessageBox.Show("Parking Position Cannot Be Empty Please Choose One");
                return false;
            }
            if (ArrivalCheckBox.Checked && string.IsNullOrEmpty(ArrivalStartPoint.Text))
            {
                MessageBox.Show("Start Position Cannot Be Empty Please Choose One");
                return false;
            }

            int number;
            if (!int.TryParse(AircraftCallsign2.Text, out number) || AircraftCallsign2.Text.Contains(" ") || AircraftCallsign2.Text.Length > 4)
            {
                MessageBox.Show("Callsign Id is number only with max 4 digit number and cannot contain space");
                return false;
            }
            else
            {
                foreach (var item in list_aircraft_exercise)
                {
                    if (item.callsign.Equals(AircraftCallsign.Text + " " + AircraftCallsign2.Text))
                    {
                        if (aircraft != null)
                        {
                            if (!item.Equals(aircraft))
                            {
                                MessageBox.Show("Callsign already used, please choose different callsign");
                                return false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Callsign already used, please choose different callsign");
                            return false;
                        }
                    }
                }
            }

            return true;
        }
        #endregion

        #region Cancel Operation
        private bool CancelOperation()
        {
            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("You are in the middle of editing/inputing, " +
                    "Are you sure to cancel your operation?", "Cancel Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)  // error is here
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #endregion



    }
}
