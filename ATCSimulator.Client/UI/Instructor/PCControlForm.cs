﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Helper;

namespace ATCSimulator.Client.UI.Instructor
{
    public partial class PCControlForm : Form
    {
        private bool is_active;
        private static List<PCmodel> list_pc = new List<PCmodel>();
        private static List<DetailPCModel> list_pc_detail = new List<DetailPCModel>();
        public PCControlForm()
        {
            InitializeComponent();
        }
        public void Init()
        {
            ListPCResponse pc = SystemConnection.ListPC();
            if (pc.status.code == 0)
            {
                if (pc.pc.Count > 0)
                {
                    // Clear List PC data
                    list_pc.Clear();
                    list_pc_detail.Clear();
                    list_pc_detail = pc.pc;

                    is_active = false;
                    enableForm_Btn.Text = "Enable PC Control";
                    ResetForm();
                    ActiveForm(is_active);
                    GenerateForm(pc.pc);
                }
            }
        }

        #region Generate Form
        public void GenerateForm(List<DetailPCModel> pc_list)
        {
            // VISUAL PC
            // jarak  : x=165, y = 110 dan inisialisasi x = 5 (jika tdk ada scroll)
            // jarak  : x=160, y = 110 dan inisialisasi x = 0 (jika ada scroll)
            // pengadaan scroll jika data pc lebih dari 15
            // location Panel : 4,20
            var Visual_PC = pc_list.Where(d => d.group.Equals(1)).OrderBy(d => d.id).ToList();
            if (Visual_PC.Count > 0)
            {
                int row = 1;
                int xVisual = Visual_PC.Count <= 15 ? 5 : 0;
                int _xVisual = Visual_PC.Count <= 15 ? 165 : 160;
                int yVisual = 0;
                foreach (var visual in Visual_PC)
                {
                    switch (row)
                    {
                        case 1:
                            Panel_Visual.Controls.Add(Create_Visual_Box(new Point { X = xVisual, Y = yVisual }, visual));
                            break;
                        case 2:
                            Panel_Visual.Controls.Add(Create_Visual_Box(new Point { X = (xVisual + _xVisual), Y = yVisual }, visual));
                            break;
                        case 3:
                            Panel_Visual.Controls.Add(Create_Visual_Box(new Point { X = (xVisual + (2 * _xVisual)), Y = yVisual }, visual));
                            break;
                    }
                    row = row < 3 ? row + 1 : 1;
                    yVisual = row == 1 ? yVisual + 110 : yVisual;
                }
            }

            // ADC CONTROLLER
            // jarak  : x=0 (tidak lebih dari 1 per baris), y = 140 dan inisialisasi x = 15 (jika tdk ada scroll)
            // jarak  : x=0 (tidak lebih dari 1 per baris), y = 140 dan inisialisasi x = 5 (jika ada scroll)
            // pengadaan scroll jika data pc lebih dari 3
            // location Panel : 4,20
            var ADC_PC = pc_list.Where(d => d.group.Equals(2)).OrderBy(d => d.id).ToList();
            if (ADC_PC.Count > 0)
            {
                int xADC = ADC_PC.Count <= 3 ? 15 : 5;
                int yADC = 0;
                foreach (var adc in ADC_PC)
                {
                    Panel_ADC.Controls.Add(Create_Other_Box(new Point { X = xADC, Y = yADC }, adc));
                    yADC += 140;
                }
            }

            // APP CONTROLLER
            // jarak  : x=0 (tidak lebih dari 1 per baris), y = 140 dan inisialisasi x = 15 (jika tdk ada scroll)
            // jarak  : x=0 (tidak lebih dari 1 per baris), y = 140 dan inisialisasi x = 5 (jika ada scroll)
            // pengadaan scroll jika data pc lebih dari 2
            // location Panel : 4,20
            var APP_PC = pc_list.Where(d => d.group.Equals(3)).OrderBy(d => d.id).ToList();
            if (APP_PC.Count > 0)
            {
                int xAPP = APP_PC.Count <= 2 ? 15 : 5;
                int yAPP = 0;
                foreach (var app in APP_PC)
                {
                    Panel_APP.Controls.Add(Create_Other_Box(new Point { X = xAPP, Y = yAPP }, app));
                    yAPP += 140;
                }
            }

            // PSEUDO PILOT
            // jarak  : x=0 (tidak lebih dari 1 per baris), y = 145 dan inisialisasi x = 20 (jika tdk ada scroll)
            // jarak  : x=0 (tidak lebih dari 1 per baris), y = 145 dan inisialisasi x = 10 (jika ada scroll)
            // pengadaan scroll jika data pc lebih dari 4
            // location Panel : 4,20
            var Pilot_PC = pc_list.Where(d => d.group.Equals(4)).OrderBy(d => d.id).ToList();
            if (Pilot_PC.Count > 0)
            {
                int xPilot = Pilot_PC.Count <= 4 ? 20 : 10;
                int yPilot = 0;
                foreach (var pilot in Pilot_PC)
                {
                    Panel_Pilot.Controls.Add(Create_Other_Box(new Point { X = xPilot, Y = yPilot }, pilot));
                    yPilot += 145;
                }
            }

            // INSTRUCTOR
            // location : 255, 23
            var Instructor_PC = pc_list.Where(d => d.group.Equals(5)).FirstOrDefault();
            if (Instructor_PC != null)
                Panel_Others.Controls.Add(Create_Other_Box(new Point { X = 255, Y = 23 }, Instructor_PC));

            // ZOOM SERVER
            // location : 20, 504
            var Zoom_PC = pc_list.Where(d => d.group.Equals(6)).FirstOrDefault();
            if (Zoom_PC != null)
                Panel_Others.Controls.Add(Create_Other_Box(new Point { X = 20, Y = 23 }, Zoom_PC));
        }

        #endregion

        #region Create Form
        private GroupBox Create_Visual_Box(Point location, DetailPCModel pc)
        {
            GroupBox groupBox = new System.Windows.Forms.GroupBox();
            Label RestartLabel = new System.Windows.Forms.Label();
            PictureBox VisualRestart = new System.Windows.Forms.PictureBox();
            Label PowerLabel = new System.Windows.Forms.Label();
            PictureBox VisualPower = new System.Windows.Forms.PictureBox();
            // 
            // groupBox
            // 
            groupBox.Controls.Add(RestartLabel);
            groupBox.Controls.Add(VisualRestart);
            groupBox.Controls.Add(PowerLabel);
            groupBox.Controls.Add(VisualPower);
            groupBox.Font = new System.Drawing.Font("Century Gothic", 11F);
            groupBox.ForeColor = System.Drawing.Color.White;
            groupBox.Location = location;
            groupBox.Name = pc.role_pc + "_Box";
            groupBox.Size = new System.Drawing.Size(158, 108);
            groupBox.Text = pc.role_pc;
            // 
            // RestartLabel
            // 
            RestartLabel.AutoSize = true;
            RestartLabel.Font = new System.Drawing.Font("Century Gothic", 10F);
            RestartLabel.ForeColor = System.Drawing.Color.White;
            RestartLabel.Location = new System.Drawing.Point(87, 85);
            RestartLabel.Name = pc.role_pc + "_Restart_Lbl";
            RestartLabel.Text = "Restart";
            // 
            // VisualRestart
            // 
            VisualRestart.Image = pc.is_active ? global::ATCSimulator.Client.Properties.Resources.restart : global::ATCSimulator.Client.Properties.Resources.restart_disable;
            VisualRestart.Location = new System.Drawing.Point(77, 21);
            VisualRestart.Name = "restart_" + pc.role_pc + "_" + pc.mac_address + "_" + pc.ip_address + "_" + pc.id;
            VisualRestart.Size = new System.Drawing.Size(75, 61);
            VisualRestart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            VisualRestart.Enabled = pc.is_active;
            VisualRestart.Click += new System.EventHandler(ButtonClick);
            // 
            // PowerLabel
            // 
            PowerLabel.AutoSize = true;
            PowerLabel.Font = new System.Drawing.Font("Century Gothic", 10F);
            PowerLabel.ForeColor = System.Drawing.Color.White;
            PowerLabel.Location = new System.Drawing.Point(12, 84);
            PowerLabel.Name = pc.role_pc + "_Power_Lbl";
            PowerLabel.Size = new System.Drawing.Size(61, 19);
            PowerLabel.Text = "Turn " + (pc.is_active ? "Off" : "On");
            // 
            // VisualPower
            // 
            VisualPower.Image = pc.is_active ? global::ATCSimulator.Client.Properties.Resources.off : global::ATCSimulator.Client.Properties.Resources.on;
            VisualPower.Location = new System.Drawing.Point(6, 21);
            VisualPower.Name = (pc.is_active ? "off_" : "on_") + pc.role_pc + "_" + pc.mac_address + "_" + pc.ip_address + "_" + pc.id;
            VisualPower.Size = new System.Drawing.Size(75, 61);
            VisualPower.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            VisualPower.Click += new System.EventHandler(ButtonClick);

            //input pc list
            list_pc.Add(new PCmodel
            {
                role_pc = pc.role_pc,
                _PowerLabel = PowerLabel,
                _RestartLabel = RestartLabel,
                _VisualRestart = VisualRestart,
                _VisualPower = VisualPower

            });

            return groupBox;
        }

        private GroupBox Create_Other_Box(Point location, DetailPCModel pc)
        {
            GroupBox groupBox = new System.Windows.Forms.GroupBox();
            Label RestartLabel = new System.Windows.Forms.Label();
            PictureBox VisualRestart = new System.Windows.Forms.PictureBox();
            Label PowerLabel = new System.Windows.Forms.Label();
            PictureBox VisualPower = new System.Windows.Forms.PictureBox();
            // 
            // groupBox
            // 
            groupBox.Controls.Add(RestartLabel);
            groupBox.Controls.Add(VisualRestart);
            groupBox.Controls.Add(PowerLabel);
            groupBox.Controls.Add(VisualPower);
            groupBox.Font = new System.Drawing.Font("Century Gothic", 11F);
            groupBox.ForeColor = System.Drawing.Color.White;
            groupBox.Location = location;
            groupBox.Name = pc.role_pc + "_Box";
            groupBox.Size = new System.Drawing.Size(180, 130);
            groupBox.Text = pc.role_pc;
            // 
            // RestartLabel
            // 
            RestartLabel.AutoSize = true;
            RestartLabel.Font = new System.Drawing.Font("Century Gothic", 10F);
            RestartLabel.ForeColor = System.Drawing.Color.White;
            RestartLabel.Location = new System.Drawing.Point(100, 100);
            RestartLabel.Size = new System.Drawing.Size(66, 21);
            RestartLabel.Name = pc.role_pc + "_Restart_Lbl";
            RestartLabel.Text = "Restart";
            // 
            // VisualRestart
            // 
            VisualRestart.Image = pc.is_active ? global::ATCSimulator.Client.Properties.Resources.restart : global::ATCSimulator.Client.Properties.Resources.restart_disable;
            VisualRestart.Location = new System.Drawing.Point(90, 26);
            VisualRestart.Name = "restart_" + pc.role_pc + "_" + pc.mac_address + "_" + pc.ip_address + "_" + pc.id;
            VisualRestart.Size = new System.Drawing.Size(85, 71);
            VisualRestart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            VisualRestart.Enabled = pc.is_active;
            VisualRestart.Click += new System.EventHandler(ButtonClick);
            // 
            // PowerLabel
            // 
            PowerLabel.AutoSize = true;
            PowerLabel.Font = new System.Drawing.Font("Century Gothic", 10F);
            PowerLabel.ForeColor = System.Drawing.Color.White;
            PowerLabel.Location = new System.Drawing.Point(18, 100);
            PowerLabel.Name = pc.role_pc + "_Power_Lbl";
            PowerLabel.Size = new System.Drawing.Size(71, 21);
            PowerLabel.Text = "Turn " + (pc.is_active ? "Off" : "On");
            // 
            // VisualPower
            // 
            VisualPower.Image = pc.is_active ? global::ATCSimulator.Client.Properties.Resources.off : global::ATCSimulator.Client.Properties.Resources.on;
            VisualPower.Location = new System.Drawing.Point(6, 26);
            VisualPower.Name = (pc.is_active ? "off_" : "on_") + pc.role_pc + "_" + pc.mac_address + "_" + pc.ip_address + "_" + pc.id;
            VisualPower.Size = new System.Drawing.Size(85, 71);
            VisualPower.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            VisualPower.Click += new System.EventHandler(ButtonClick);

            //input pc list
            list_pc.Add(new PCmodel
            {
                role_pc = pc.role_pc,
                _PowerLabel = PowerLabel,
                _RestartLabel = RestartLabel,
                _VisualRestart = VisualRestart,
                _VisualPower = VisualPower

            });

            return groupBox;
        }
        #endregion

        #region Button Handlers
        private void ButtonClick(object sender, EventArgs e)
        {


            //get all data needed => [0]:command , [1]:rolePc , [2]:mac_address , [3]:ip_address, [4]: pc_id
            PictureBox btn = (PictureBox)sender;
            Label label;
            string[] data = btn.Name.ToString().Split(new char[] { '_' });

            switch (data[0].ToLower())
            {
                case "on":
                    btn.Image = global::ATCSimulator.Client.Properties.Resources.process;
                    label = btn.Parent.Controls.OfType<Label>().Where(d => d.Name.Equals(data[1] + "_Power_Lbl")).FirstOrDefault();
                    label.Text = "Turning...";
                    HelperClient.WOLClass wol = new HelperClient.WOLClass();
                    bool status = wol.WakeFunction(data[2]);
                    if (!status)
                        MessageBox.Show("Failed Turn On Computer, Something is Wrong");
                    break;
                case "off":
                    btn.Image = global::ATCSimulator.Client.Properties.Resources.process;
                    StatusResponse shutdown = SystemConnection.ShutdownRestartPC(new ShutdownRestartPCContract
                        {
                            command = data[0],
                            role_pc = data[1],
                            mac_address = data[2],
                            ip_address = data[3],

                        });
                    if (shutdown.status.code == 0)
                    {
                        btn.Image = global::ATCSimulator.Client.Properties.Resources.on;
                        btn.Name = "on_" + data[1] + "_" + data[2] + "_" + data[3] + "_" + data[4];
                        label = btn.Parent.Controls.OfType<Label>().Where(d => d.Name.Equals(data[1] + "_Power_Lbl")).FirstOrDefault();
                        label.Text = "Turn On";
                        PictureBox _restart = btn.Parent.Controls.OfType<PictureBox>().Where(d => d.Name.Equals("restart_" + data[1] + "_" + data[2] + "_" + data[3] + "_" + data[4])).FirstOrDefault();
                        _restart.Image = global::ATCSimulator.Client.Properties.Resources.restart_disable;
                        _restart.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("Failed Turn Off Computer, Please Try Again");
                        btn.Image = global::ATCSimulator.Client.Properties.Resources.off;
                        label = btn.Parent.Controls.OfType<Label>().Where(d => d.Name.Equals(data[1] + "_Power_Lbl")).FirstOrDefault();
                        label.Text = "Turn Off";
                    }
                    break;
                case "restart":
                    btn.Image = global::ATCSimulator.Client.Properties.Resources.restart_process;
                    StatusResponse restart = SystemConnection.ShutdownRestartPC(new ShutdownRestartPCContract
                    {
                        command = data[0],
                        role_pc = data[1],
                        mac_address = data[2],
                        ip_address = data[3],
                    });
                    if (restart.status.code == 0)
                    {
                        btn.Image = global::ATCSimulator.Client.Properties.Resources.restart_disable;
                        PictureBox _power = btn.Parent.Controls.OfType<PictureBox>().Where(d => d.Name.Equals("off_" + data[1] + "_" + data[2] + "_" + data[3] + "_" + data[4])).FirstOrDefault();
                        _power.Image = global::ATCSimulator.Client.Properties.Resources.process;
                        _power.Name = "on_" + data[1] + "_" + data[2] + "_" + data[3] + "_" + data[4];
                        label = btn.Parent.Controls.OfType<Label>().Where(d => d.Name.Equals(data[1] + "_Power_Lbl")).FirstOrDefault();
                        label.Text = "Restarting";
                    }
                    else
                    {
                        MessageBox.Show("Failed Restart Computer, Please Try Again");
                        btn.Image = global::ATCSimulator.Client.Properties.Resources.restart;
                    }
                    break;
            }


        }
        private void enableForm_Btn_Click(object sender, EventArgs e)
        {
            if (is_active)
            {
                is_active = !is_active;
                ActiveForm(is_active);
                enableForm_Btn.Text = "Enable PC Control";
            }
            else
            {
                is_active = !is_active;
                ActiveForm(is_active);
                enableForm_Btn.Text = "Disable PC Control";
            }

        }
        #endregion

        #region Helper
        public void ActiveForm(bool status)
        {
            Panel_Visual.Enabled = status;
            Panel_ADC.Enabled = status;
            Panel_APP.Enabled = status;
            Panel_Pilot.Enabled = status;
            Panel_Others.Enabled = status;
        }
        public void ResetForm()
        {
            Panel_Visual.Controls.Clear();
            Panel_ADC.Controls.Clear();
            Panel_APP.Controls.Clear();
            Panel_Pilot.Controls.Clear();
            Panel_Others.Controls.Clear();
        }
        #endregion

        #region Callback Method

        public void ConnectPC(ATCSimulator.Client.SystemService.UserComputerInfo user)
        {
            try
            {
                PCmodel pc_data = list_pc.Where(d => d.role_pc.Equals(user.role_pc)).FirstOrDefault();
                DetailPCModel pc_detail = list_pc_detail.Where(d => d.role_pc.Equals(user.role_pc)).FirstOrDefault();
                pc_detail.mac_address = user.mac_address;
                pc_detail.ip_address = user.ip_address;
                pc_detail.pc_name = user.pc_name;
                pc_data._VisualPower.Name = "off_" + pc_detail.role_pc + "_" + pc_detail.mac_address + "_" + pc_detail.ip_address + "_" + pc_detail.id;
                pc_data._VisualPower.Image = global::ATCSimulator.Client.Properties.Resources.off;
                pc_data._PowerLabel.Text = "Turn Off";
                pc_data._VisualRestart.Image = global::ATCSimulator.Client.Properties.Resources.restart;
                pc_data._VisualRestart.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Class Design
        private class PCmodel
        {
            public string role_pc { get; set; }
            public Label _RestartLabel { get; set; }
            public PictureBox _VisualRestart { get; set; }
            public Label _PowerLabel { get; set; }
            public PictureBox _VisualPower { get; set; }
        }
        #endregion
    }
}


