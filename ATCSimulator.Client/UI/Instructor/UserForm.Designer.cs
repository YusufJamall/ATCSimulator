﻿namespace ATCSimulator.Client.UI.Instructor
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ID_text = new Telerik.WinControls.UI.RadTextBox();
            this.ClassYear_text = new Telerik.WinControls.UI.RadTextBox();
            this.ClassYear_lbl = new System.Windows.Forms.Label();
            this.Class_text = new Telerik.WinControls.UI.RadTextBox();
            this.Class_lbl = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.reset_password_btn = new System.Windows.Forms.Button();
            this.Remove_Btn = new System.Windows.Forms.Label();
            this.Gender_box = new Telerik.WinControls.UI.RadDropDownList();
            this.Role_Box = new Telerik.WinControls.UI.RadDropDownList();
            this.label9 = new System.Windows.Forms.Label();
            this.Save_Btn = new System.Windows.Forms.Label();
            this.User_Panel = new System.Windows.Forms.Panel();
            this.history_btn = new System.Windows.Forms.Button();
            this.Password = new Telerik.WinControls.UI.RadTextBox();
            this.Username = new Telerik.WinControls.UI.RadTextBox();
            this.Password_lbl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.address_text = new Telerik.WinControls.UI.RadTextBox();
            this.phone_text = new Telerik.WinControls.UI.RadTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Birthdate_User = new Telerik.WinControls.UI.RadDateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.UserID_Text = new Telerik.WinControls.UI.RadTextBox();
            this.email_text = new Telerik.WinControls.UI.RadTextBox();
            this.LastName_text = new Telerik.WinControls.UI.RadTextBox();
            this.FirstName_text = new Telerik.WinControls.UI.RadTextBox();
            this.label_edit_picture = new System.Windows.Forms.PictureBox();
            this.Cancel_Btn = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Update_Btn = new System.Windows.Forms.Label();
            this.Image_Photo = new System.Windows.Forms.PictureBox();
            this.User_List = new System.Windows.Forms.DataGridView();
            this.Page_text = new Telerik.WinControls.UI.RadTextBox();
            this.Total_Page_lbl = new System.Windows.Forms.Label();
            this.GoToPage_Box = new Telerik.WinControls.UI.RadDropDownList();
            this.label7 = new System.Windows.Forms.Label();
            this.Next_Btn = new System.Windows.Forms.Label();
            this.Prev_Btn = new System.Windows.Forms.Label();
            this.Create_Btn = new System.Windows.Forms.Label();
            this.typeSelect_Box = new Telerik.WinControls.UI.RadDropDownList();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ID_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClassYear_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Class_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gender_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Role_Box)).BeginInit();
            this.User_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Username)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.address_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phone_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Birthdate_User)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserID_Text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.email_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastName_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstName_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label_edit_picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.User_List)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoToPage_Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeSelect_Box)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ID_text);
            this.panel1.Controls.Add(this.ClassYear_text);
            this.panel1.Controls.Add(this.ClassYear_lbl);
            this.panel1.Controls.Add(this.Class_text);
            this.panel1.Controls.Add(this.Class_lbl);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.reset_password_btn);
            this.panel1.Controls.Add(this.Remove_Btn);
            this.panel1.Controls.Add(this.Gender_box);
            this.panel1.Controls.Add(this.Role_Box);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.Save_Btn);
            this.panel1.Controls.Add(this.User_Panel);
            this.panel1.Controls.Add(this.address_text);
            this.panel1.Controls.Add(this.phone_text);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.Birthdate_User);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.UserID_Text);
            this.panel1.Controls.Add(this.email_text);
            this.panel1.Controls.Add(this.LastName_text);
            this.panel1.Controls.Add(this.FirstName_text);
            this.panel1.Controls.Add(this.label_edit_picture);
            this.panel1.Controls.Add(this.Cancel_Btn);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Update_Btn);
            this.panel1.Controls.Add(this.Image_Photo);
            this.panel1.Location = new System.Drawing.Point(689, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 629);
            this.panel1.TabIndex = 35;
            // 
            // ID_text
            // 
            this.ID_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.ID_text.Location = new System.Drawing.Point(195, 112);
            this.ID_text.Name = "ID_text";
            this.ID_text.NullText = "ID User";
            this.ID_text.Size = new System.Drawing.Size(81, 28);
            this.ID_text.TabIndex = 81;
            this.ID_text.TabStop = false;
            this.ID_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ID_text.Visible = false;
            // 
            // ClassYear_text
            // 
            this.ClassYear_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.ClassYear_text.Location = new System.Drawing.Point(444, 547);
            this.ClassYear_text.Name = "ClassYear_text";
            this.ClassYear_text.NullText = "Year";
            this.ClassYear_text.Size = new System.Drawing.Size(75, 28);
            this.ClassYear_text.TabIndex = 13;
            this.ClassYear_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ClassYear_lbl
            // 
            this.ClassYear_lbl.AutoSize = true;
            this.ClassYear_lbl.BackColor = System.Drawing.Color.Transparent;
            this.ClassYear_lbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.ClassYear_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ClassYear_lbl.Location = new System.Drawing.Point(4, 547);
            this.ClassYear_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.ClassYear_lbl.Name = "ClassYear_lbl";
            this.ClassYear_lbl.Size = new System.Drawing.Size(102, 22);
            this.ClassYear_lbl.TabIndex = 83;
            this.ClassYear_lbl.Text = "Class Year";
            this.ClassYear_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Class_text
            // 
            this.Class_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Class_text.Location = new System.Drawing.Point(444, 507);
            this.Class_text.Name = "Class_text";
            this.Class_text.NullText = "Class";
            this.Class_text.Size = new System.Drawing.Size(75, 28);
            this.Class_text.TabIndex = 12;
            this.Class_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Class_lbl
            // 
            this.Class_lbl.AutoSize = true;
            this.Class_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Class_lbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Class_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Class_lbl.Location = new System.Drawing.Point(2, 507);
            this.Class_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Class_lbl.Name = "Class_lbl";
            this.Class_lbl.Size = new System.Drawing.Size(55, 22);
            this.Class_lbl.TabIndex = 81;
            this.Class_lbl.Text = "Class";
            this.Class_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox1.Location = new System.Drawing.Point(1, 99);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(520, 4);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 80;
            this.pictureBox1.TabStop = false;
            // 
            // reset_password_btn
            // 
            this.reset_password_btn.BackColor = System.Drawing.Color.DodgerBlue;
            this.reset_password_btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.reset_password_btn.FlatAppearance.BorderSize = 0;
            this.reset_password_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.reset_password_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.reset_password_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.reset_password_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.reset_password_btn.ForeColor = System.Drawing.Color.White;
            this.reset_password_btn.Location = new System.Drawing.Point(383, 52);
            this.reset_password_btn.Name = "reset_password_btn";
            this.reset_password_btn.Size = new System.Drawing.Size(134, 36);
            this.reset_password_btn.TabIndex = 79;
            this.reset_password_btn.Text = "Reset Password";
            this.reset_password_btn.UseVisualStyleBackColor = false;
            this.reset_password_btn.Visible = false;
            this.reset_password_btn.Click += new System.EventHandler(this.reset_password_btn_Click);
            // 
            // Remove_Btn
            // 
            this.Remove_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Remove_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remove_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Remove_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_remove;
            this.Remove_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Remove_Btn.Location = new System.Drawing.Point(358, 593);
            this.Remove_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Remove_Btn.Name = "Remove_Btn";
            this.Remove_Btn.Size = new System.Drawing.Size(166, 31);
            this.Remove_Btn.TabIndex = 67;
            this.Remove_Btn.Text = "Remove User";
            this.Remove_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Remove_Btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.Remove_Btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.Remove_Btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // Gender_box
            // 
            this.Gender_box.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Gender_box.Font = new System.Drawing.Font("Century Gothic", 14F);
            radListDataItem1.Font = new System.Drawing.Font("Century Gothic", 12F);
            radListDataItem1.Text = "Male";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Font = new System.Drawing.Font("Century Gothic", 12F);
            radListDataItem2.Text = "Female";
            radListDataItem2.TextWrap = true;
            this.Gender_box.Items.Add(radListDataItem1);
            this.Gender_box.Items.Add(radListDataItem2);
            this.Gender_box.Location = new System.Drawing.Point(367, 270);
            this.Gender_box.Name = "Gender_box";
            this.Gender_box.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Gender_box.Size = new System.Drawing.Size(152, 28);
            this.Gender_box.TabIndex = 7;
            // 
            // Role_Box
            // 
            this.Role_Box.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Role_Box.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Role_Box.Location = new System.Drawing.Point(282, 112);
            this.Role_Box.Name = "Role_Box";
            this.Role_Box.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Role_Box.Size = new System.Drawing.Size(237, 28);
            this.Role_Box.TabIndex = 3;
            this.Role_Box.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.Role_Box_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label9.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label9.Location = new System.Drawing.Point(2, 112);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 22);
            this.label9.TabIndex = 76;
            this.label9.Text = "Role";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Save_Btn
            // 
            this.Save_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Save_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Save_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_edit;
            this.Save_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Save_Btn.Location = new System.Drawing.Point(126, 593);
            this.Save_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Save_Btn.Name = "Save_Btn";
            this.Save_Btn.Size = new System.Drawing.Size(87, 31);
            this.Save_Btn.TabIndex = 74;
            this.Save_Btn.Text = "Save";
            this.Save_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Save_Btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.Save_Btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.Save_Btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // User_Panel
            // 
            this.User_Panel.Controls.Add(this.history_btn);
            this.User_Panel.Controls.Add(this.Password);
            this.User_Panel.Controls.Add(this.Username);
            this.User_Panel.Controls.Add(this.Password_lbl);
            this.User_Panel.Controls.Add(this.label8);
            this.User_Panel.Location = new System.Drawing.Point(111, 13);
            this.User_Panel.Name = "User_Panel";
            this.User_Panel.Size = new System.Drawing.Size(405, 78);
            this.User_Panel.TabIndex = 36;
            // 
            // history_btn
            // 
            this.history_btn.BackColor = System.Drawing.Color.DodgerBlue;
            this.history_btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.history_btn.FlatAppearance.BorderSize = 0;
            this.history_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.history_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.history_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.history_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.history_btn.ForeColor = System.Drawing.Color.White;
            this.history_btn.Location = new System.Drawing.Point(109, 39);
            this.history_btn.Name = "history_btn";
            this.history_btn.Size = new System.Drawing.Size(157, 36);
            this.history_btn.TabIndex = 84;
            this.history_btn.Text = "History Simulation";
            this.history_btn.UseVisualStyleBackColor = false;
            this.history_btn.Visible = false;
            this.history_btn.Click += new System.EventHandler(this.history_btn_Click);
            // 
            // Password
            // 
            this.Password.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Password.Location = new System.Drawing.Point(109, 43);
            this.Password.Name = "Password";
            this.Password.NullText = "Password";
            this.Password.PasswordChar = '*';
            this.Password.Size = new System.Drawing.Size(296, 28);
            this.Password.TabIndex = 2;
            this.Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Username
            // 
            this.Username.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Username.Location = new System.Drawing.Point(109, 3);
            this.Username.Name = "Username";
            this.Username.NullText = "Username";
            this.Username.Size = new System.Drawing.Size(296, 28);
            this.Username.TabIndex = 1;
            this.Username.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Password_lbl
            // 
            this.Password_lbl.AutoSize = true;
            this.Password_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Password_lbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Password_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Password_lbl.Location = new System.Drawing.Point(3, 43);
            this.Password_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Password_lbl.Name = "Password_lbl";
            this.Password_lbl.Size = new System.Drawing.Size(95, 22);
            this.Password_lbl.TabIndex = 79;
            this.Password_lbl.Text = "Password";
            this.Password_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label8.Location = new System.Drawing.Point(1, 3);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 22);
            this.label8.TabIndex = 78;
            this.label8.Text = "Username";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // address_text
            // 
            this.address_text.AutoSize = false;
            this.address_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.address_text.Location = new System.Drawing.Point(160, 430);
            this.address_text.Multiline = true;
            this.address_text.Name = "address_text";
            this.address_text.NullText = "address";
            this.address_text.Size = new System.Drawing.Size(359, 65);
            this.address_text.TabIndex = 11;
            this.address_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.address_text.GetChildAt(0).GetChildAt(0))).NullText = "address";
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.address_text.GetChildAt(0).GetChildAt(0))).StretchVertically = true;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.address_text.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Century Gothic", 14F);
            // 
            // phone_text
            // 
            this.phone_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.phone_text.Location = new System.Drawing.Point(301, 390);
            this.phone_text.Name = "phone_text";
            this.phone_text.NullText = "+62xxxxxxxxx";
            this.phone_text.Size = new System.Drawing.Size(218, 28);
            this.phone_text.TabIndex = 10;
            this.phone_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label11.Location = new System.Drawing.Point(2, 310);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 22);
            this.label11.TabIndex = 68;
            this.label11.Text = "Birthday Date";
            this.label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Birthdate_User
            // 
            this.Birthdate_User.CustomFormat = "dd MMMM yyyy";
            this.Birthdate_User.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Birthdate_User.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Birthdate_User.Location = new System.Drawing.Point(301, 310);
            this.Birthdate_User.Name = "Birthdate_User";
            this.Birthdate_User.NullDate = new System.DateTime(2014, 5, 18, 14, 55, 29, 0);
            this.Birthdate_User.Size = new System.Drawing.Size(218, 28);
            this.Birthdate_User.TabIndex = 8;
            this.Birthdate_User.TabStop = false;
            this.Birthdate_User.Text = "05 July 2013";
            this.Birthdate_User.Value = new System.DateTime(2013, 7, 5, 6, 50, 36, 497);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label10.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label10.Location = new System.Drawing.Point(2, 270);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 22);
            this.label10.TabIndex = 65;
            this.label10.Text = "Gender";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // UserID_Text
            // 
            this.UserID_Text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.UserID_Text.Location = new System.Drawing.Point(160, 230);
            this.UserID_Text.Name = "UserID_Text";
            this.UserID_Text.NullText = "KTP, NIM or NIK";
            this.UserID_Text.Size = new System.Drawing.Size(359, 28);
            this.UserID_Text.TabIndex = 6;
            this.UserID_Text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // email_text
            // 
            this.email_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.email_text.Location = new System.Drawing.Point(160, 350);
            this.email_text.Name = "email_text";
            this.email_text.NullText = "user@email.com";
            this.email_text.Size = new System.Drawing.Size(359, 28);
            this.email_text.TabIndex = 9;
            this.email_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LastName_text
            // 
            this.LastName_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.LastName_text.Location = new System.Drawing.Point(160, 190);
            this.LastName_text.Name = "LastName_text";
            this.LastName_text.NullText = "Last Name";
            this.LastName_text.Size = new System.Drawing.Size(359, 28);
            this.LastName_text.TabIndex = 5;
            this.LastName_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FirstName_text
            // 
            this.FirstName_text.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.FirstName_text.Location = new System.Drawing.Point(160, 150);
            this.FirstName_text.Name = "FirstName_text";
            this.FirstName_text.NullText = "First Name";
            this.FirstName_text.Size = new System.Drawing.Size(359, 28);
            this.FirstName_text.TabIndex = 4;
            this.FirstName_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_edit_picture
            // 
            this.label_edit_picture.BackColor = System.Drawing.Color.White;
            this.label_edit_picture.Image = global::ATCSimulator.Client.Properties.Resources.img_edit;
            this.label_edit_picture.Location = new System.Drawing.Point(84, 3);
            this.label_edit_picture.Name = "label_edit_picture";
            this.label_edit_picture.Size = new System.Drawing.Size(18, 17);
            this.label_edit_picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.label_edit_picture.TabIndex = 58;
            this.label_edit_picture.TabStop = false;
            this.label_edit_picture.Visible = false;
            this.label_edit_picture.Click += new System.EventHandler(this.ButtonOnClick);
            // 
            // Cancel_Btn
            // 
            this.Cancel_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Cancel_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Cancel_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_remove;
            this.Cancel_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cancel_Btn.Location = new System.Drawing.Point(250, 593);
            this.Cancel_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Cancel_Btn.Name = "Cancel_Btn";
            this.Cancel_Btn.Size = new System.Drawing.Size(108, 31);
            this.Cancel_Btn.TabIndex = 35;
            this.Cancel_Btn.Text = "Cancel";
            this.Cancel_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cancel_Btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.Cancel_Btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.Cancel_Btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(2, 426);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 22);
            this.label6.TabIndex = 42;
            this.label6.Text = "Adress";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(2, 390);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 22);
            this.label5.TabIndex = 41;
            this.label5.Text = "Phone";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(2, 350);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 22);
            this.label4.TabIndex = 40;
            this.label4.Text = "Email";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(3, 230);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 22);
            this.label3.TabIndex = 39;
            this.label3.Text = "User ID";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(2, 190);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 22);
            this.label2.TabIndex = 38;
            this.label2.Text = "Last Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(2, 150);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 22);
            this.label1.TabIndex = 37;
            this.label1.Text = "First Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Update_Btn
            // 
            this.Update_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Update_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Update_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Update_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_edit;
            this.Update_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Update_Btn.Location = new System.Drawing.Point(126, 593);
            this.Update_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Update_Btn.Name = "Update_Btn";
            this.Update_Btn.Size = new System.Drawing.Size(111, 31);
            this.Update_Btn.TabIndex = 31;
            this.Update_Btn.Text = "Update";
            this.Update_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Update_Btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.Update_Btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.Update_Btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // Image_Photo
            // 
            this.Image_Photo.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            this.Image_Photo.Location = new System.Drawing.Point(3, 3);
            this.Image_Photo.Name = "Image_Photo";
            this.Image_Photo.Size = new System.Drawing.Size(103, 95);
            this.Image_Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Image_Photo.TabIndex = 30;
            this.Image_Photo.TabStop = false;
            this.Image_Photo.Click += new System.EventHandler(this.ImageOnClick);
            this.Image_Photo.MouseLeave += new System.EventHandler(this.ImageOnLeave);
            this.Image_Photo.MouseHover += new System.EventHandler(this.ImageOnHover);
            // 
            // User_List
            // 
            this.User_List.AllowUserToAddRows = false;
            this.User_List.AllowUserToDeleteRows = false;
            this.User_List.AllowUserToOrderColumns = true;
            this.User_List.AllowUserToResizeColumns = false;
            this.User_List.AllowUserToResizeRows = false;
            this.User_List.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.User_List.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.User_List.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.User_List.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.User_List.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.User_List.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.User_List.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.User_List.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 16F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.User_List.DefaultCellStyle = dataGridViewCellStyle2;
            this.User_List.EnableHeadersVisualStyles = false;
            this.User_List.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.User_List.Location = new System.Drawing.Point(5, 40);
            this.User_List.MultiSelect = false;
            this.User_List.Name = "User_List";
            this.User_List.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.User_List.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.User_List.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.User_List.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.User_List.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.User_List.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.User_List.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.User_List.Size = new System.Drawing.Size(658, 563);
            this.User_List.TabIndex = 65;
            this.User_List.TabStop = false;
            this.User_List.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.UserListContentClick);
            this.User_List.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.User_List_CellDoubleClick);
            // 
            // Page_text
            // 
            this.Page_text.Enabled = false;
            this.Page_text.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.Page_text.Location = new System.Drawing.Point(45, 613);
            this.Page_text.Name = "Page_text";
            this.Page_text.NullText = "1";
            this.Page_text.Size = new System.Drawing.Size(29, 28);
            this.Page_text.TabIndex = 71;
            this.Page_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Total_Page_lbl
            // 
            this.Total_Page_lbl.AutoSize = true;
            this.Total_Page_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Total_Page_lbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Total_Page_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Total_Page_lbl.Location = new System.Drawing.Point(109, 619);
            this.Total_Page_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Total_Page_lbl.Name = "Total_Page_lbl";
            this.Total_Page_lbl.Size = new System.Drawing.Size(292, 22);
            this.Total_Page_lbl.TabIndex = 85;
            this.Total_Page_lbl.Text = "Displaying  100 - 100 from result";
            this.Total_Page_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // GoToPage_Box
            // 
            this.GoToPage_Box.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.GoToPage_Box.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.GoToPage_Box.Location = new System.Drawing.Point(612, 613);
            this.GoToPage_Box.Name = "GoToPage_Box";
            this.GoToPage_Box.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GoToPage_Box.Size = new System.Drawing.Size(51, 28);
            this.GoToPage_Box.TabIndex = 86;
            this.GoToPage_Box.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.GoToPage_Box_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(491, 619);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 22);
            this.label7.TabIndex = 85;
            this.label7.Text = "Go To Page";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Next_Btn
            // 
            this.Next_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Next_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Next_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_next;
            this.Next_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Next_Btn.Location = new System.Drawing.Point(77, 612);
            this.Next_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Next_Btn.Name = "Next_Btn";
            this.Next_Btn.Size = new System.Drawing.Size(34, 31);
            this.Next_Btn.TabIndex = 72;
            this.Next_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Next_Btn.Click += new System.EventHandler(this.Next_Btn_Click);
            this.Next_Btn.MouseLeave += new System.EventHandler(this.Next_Btn_MouseLeave);
            this.Next_Btn.MouseHover += new System.EventHandler(this.Next_Btn_MouseHover);
            // 
            // Prev_Btn
            // 
            this.Prev_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Prev_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prev_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Prev_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_prev;
            this.Prev_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Prev_Btn.Location = new System.Drawing.Point(9, 612);
            this.Prev_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Prev_Btn.Name = "Prev_Btn";
            this.Prev_Btn.Size = new System.Drawing.Size(34, 31);
            this.Prev_Btn.TabIndex = 68;
            this.Prev_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Prev_Btn.Click += new System.EventHandler(this.Prev_Btn_Click);
            this.Prev_Btn.MouseLeave += new System.EventHandler(this.Prev_Btn_MouseLeave);
            this.Prev_Btn.MouseHover += new System.EventHandler(this.Prev_Btn_MouseHover);
            // 
            // Create_Btn
            // 
            this.Create_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Create_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Create_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Create_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_add;
            this.Create_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Create_Btn.Location = new System.Drawing.Point(9, 1);
            this.Create_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Create_Btn.Name = "Create_Btn";
            this.Create_Btn.Size = new System.Drawing.Size(154, 31);
            this.Create_Btn.TabIndex = 66;
            this.Create_Btn.Text = "Create New";
            this.Create_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Create_Btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.Create_Btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.Create_Btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // typeSelect_Box
            // 
            this.typeSelect_Box.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.typeSelect_Box.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.typeSelect_Box.Location = new System.Drawing.Point(498, 15);
            this.typeSelect_Box.Name = "typeSelect_Box";
            this.typeSelect_Box.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.typeSelect_Box.Size = new System.Drawing.Size(165, 22);
            this.typeSelect_Box.TabIndex = 88;
            this.typeSelect_Box.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.typeSelect_Box_SelectedIndexChanged);
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1225, 645);
            this.Controls.Add(this.typeSelect_Box);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.GoToPage_Box);
            this.Controls.Add(this.Total_Page_lbl);
            this.Controls.Add(this.Next_Btn);
            this.Controls.Add(this.Page_text);
            this.Controls.Add(this.Prev_Btn);
            this.Controls.Add(this.Create_Btn);
            this.Controls.Add(this.User_List);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UserForm";
            this.Text = "List_User_Form";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ID_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClassYear_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Class_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gender_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Role_Box)).EndInit();
            this.User_Panel.ResumeLayout(false);
            this.User_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Username)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.address_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phone_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Birthdate_User)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserID_Text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.email_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastName_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstName_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label_edit_picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.User_List)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoToPage_Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeSelect_Box)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadTextBox address_text;
        private Telerik.WinControls.UI.RadTextBox phone_text;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadDateTimePicker Birthdate_User;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadTextBox UserID_Text;
        private Telerik.WinControls.UI.RadTextBox email_text;
        private Telerik.WinControls.UI.RadTextBox LastName_text;
        private Telerik.WinControls.UI.RadTextBox FirstName_text;
        private System.Windows.Forms.PictureBox label_edit_picture;
        private System.Windows.Forms.Label Cancel_Btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Update_Btn;
        private System.Windows.Forms.PictureBox Image_Photo;
        private System.Windows.Forms.Panel User_Panel;
        private Telerik.WinControls.UI.RadTextBox Password;
        private Telerik.WinControls.UI.RadTextBox Username;
        private System.Windows.Forms.Label Password_lbl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label Remove_Btn;
        private System.Windows.Forms.Label Create_Btn;
        private System.Windows.Forms.Label Save_Btn;
        private System.Windows.Forms.Label label9;
        private Telerik.WinControls.UI.RadDropDownList Role_Box;
        private Telerik.WinControls.UI.RadDropDownList Gender_box;
        private System.Windows.Forms.Button reset_password_btn;
        private System.Windows.Forms.DataGridView User_List;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadTextBox Class_text;
        private System.Windows.Forms.Label Class_lbl;
        private Telerik.WinControls.UI.RadTextBox ClassYear_text;
        private System.Windows.Forms.Label ClassYear_lbl;
        private Telerik.WinControls.UI.RadTextBox ID_text;
        private System.Windows.Forms.Label Prev_Btn;
        private Telerik.WinControls.UI.RadTextBox Page_text;
        private System.Windows.Forms.Label Next_Btn;
        private System.Windows.Forms.Label Total_Page_lbl;
        private Telerik.WinControls.UI.RadDropDownList GoToPage_Box;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadDropDownList typeSelect_Box;
        private System.Windows.Forms.Button history_btn;
    }
}