﻿namespace ATCSimulator.Client.UI.Instructor
{
    partial class InstructorMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PCControlBtn = new System.Windows.Forms.Label();
            this.exercise_btn = new System.Windows.Forms.Label();
            this.simulation_btn = new System.Windows.Forms.Label();
            this.history_btn = new System.Windows.Forms.Label();
            this.supervisor_panel = new System.Windows.Forms.Panel();
            this.user_btn = new System.Windows.Forms.Label();
            this.lineSeparation = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.lineSeparation)).BeginInit();
            this.SuspendLayout();
            // 
            // PCControlBtn
            // 
            this.PCControlBtn.AutoSize = true;
            this.PCControlBtn.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.PCControlBtn.ForeColor = System.Drawing.Color.White;
            this.PCControlBtn.Location = new System.Drawing.Point(5, 15);
            this.PCControlBtn.Name = "PCControlBtn";
            this.PCControlBtn.Size = new System.Drawing.Size(108, 22);
            this.PCControlBtn.TabIndex = 27;
            this.PCControlBtn.Text = "PC Control";
            this.PCControlBtn.Click += new System.EventHandler(this.ButtonOnClick);
            this.PCControlBtn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.PCControlBtn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // exercise_btn
            // 
            this.exercise_btn.AutoSize = true;
            this.exercise_btn.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.exercise_btn.ForeColor = System.Drawing.Color.White;
            this.exercise_btn.Location = new System.Drawing.Point(5, 62);
            this.exercise_btn.Name = "exercise_btn";
            this.exercise_btn.Size = new System.Drawing.Size(81, 22);
            this.exercise_btn.TabIndex = 28;
            this.exercise_btn.Text = "Exercise";
            this.exercise_btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.exercise_btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.exercise_btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // simulation_btn
            // 
            this.simulation_btn.AutoSize = true;
            this.simulation_btn.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.simulation_btn.ForeColor = System.Drawing.Color.White;
            this.simulation_btn.Location = new System.Drawing.Point(5, 112);
            this.simulation_btn.Name = "simulation_btn";
            this.simulation_btn.Size = new System.Drawing.Size(101, 22);
            this.simulation_btn.TabIndex = 29;
            this.simulation_btn.Text = "Simulation";
            this.simulation_btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.simulation_btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.simulation_btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // history_btn
            // 
            this.history_btn.AutoSize = true;
            this.history_btn.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.history_btn.ForeColor = System.Drawing.Color.White;
            this.history_btn.Location = new System.Drawing.Point(5, 158);
            this.history_btn.Name = "history_btn";
            this.history_btn.Size = new System.Drawing.Size(69, 22);
            this.history_btn.TabIndex = 31;
            this.history_btn.Text = "History";
            this.history_btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.history_btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.history_btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // supervisor_panel
            // 
            this.supervisor_panel.Location = new System.Drawing.Point(119, 0);
            this.supervisor_panel.Name = "supervisor_panel";
            this.supervisor_panel.Size = new System.Drawing.Size(1225, 645);
            this.supervisor_panel.TabIndex = 33;
            // 
            // user_btn
            // 
            this.user_btn.AutoSize = true;
            this.user_btn.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.user_btn.ForeColor = System.Drawing.Color.White;
            this.user_btn.Location = new System.Drawing.Point(5, 204);
            this.user_btn.Name = "user_btn";
            this.user_btn.Size = new System.Drawing.Size(47, 22);
            this.user_btn.TabIndex = 34;
            this.user_btn.Text = "User";
            this.user_btn.Click += new System.EventHandler(this.ButtonOnClick);
            this.user_btn.MouseLeave += new System.EventHandler(this.ButtonOnLeave);
            this.user_btn.MouseHover += new System.EventHandler(this.ButtonOnHover);
            // 
            // lineSeparation
            // 
            this.lineSeparation.Image = global::ATCSimulator.Client.Properties.Resources.lineWhite;
            this.lineSeparation.Location = new System.Drawing.Point(115, 0);
            this.lineSeparation.Name = "lineSeparation";
            this.lineSeparation.Size = new System.Drawing.Size(3, 645);
            this.lineSeparation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lineSeparation.TabIndex = 0;
            this.lineSeparation.TabStop = false;
            // 
            // InstructorMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1346, 645);
            this.Controls.Add(this.lineSeparation);
            this.Controls.Add(this.user_btn);
            this.Controls.Add(this.supervisor_panel);
            this.Controls.Add(this.history_btn);
            this.Controls.Add(this.simulation_btn);
            this.Controls.Add(this.exercise_btn);
            this.Controls.Add(this.PCControlBtn);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InstructorMainForm";
            this.Text = "SupervisorPanel";
            ((System.ComponentModel.ISupportInitialize)(this.lineSeparation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PCControlBtn;
        private System.Windows.Forms.Label exercise_btn;
        private System.Windows.Forms.Label simulation_btn;
        private System.Windows.Forms.Label history_btn;
        private System.Windows.Forms.Panel supervisor_panel;
        private System.Windows.Forms.Label user_btn;
        private System.Windows.Forms.PictureBox lineSeparation;
    }
}