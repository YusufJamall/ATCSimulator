﻿namespace ATCSimulator.Client.UI.Instructor
{
    partial class PCControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VisualBox = new System.Windows.Forms.GroupBox();
            this.Panel_Visual = new System.Windows.Forms.Panel();
            this.PseudoPilotBox = new System.Windows.Forms.GroupBox();
            this.Panel_Pilot = new System.Windows.Forms.Panel();
            this.ADCControllerBox = new System.Windows.Forms.GroupBox();
            this.Panel_ADC = new System.Windows.Forms.Panel();
            this.editor_btn = new System.Windows.Forms.Label();
            this.APPControllerBox = new System.Windows.Forms.GroupBox();
            this.Panel_APP = new System.Windows.Forms.Panel();
            this.Panel_Others = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.enableForm_Btn = new System.Windows.Forms.Button();
            this.VisualBox.SuspendLayout();
            this.PseudoPilotBox.SuspendLayout();
            this.ADCControllerBox.SuspendLayout();
            this.APPControllerBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // VisualBox
            // 
            this.VisualBox.Controls.Add(this.Panel_Visual);
            this.VisualBox.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.VisualBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.VisualBox.Location = new System.Drawing.Point(8, 42);
            this.VisualBox.Name = "VisualBox";
            this.VisualBox.Size = new System.Drawing.Size(506, 577);
            this.VisualBox.TabIndex = 76;
            this.VisualBox.TabStop = false;
            this.VisualBox.Text = "Visual";
            // 
            // Panel_Visual
            // 
            this.Panel_Visual.AutoScroll = true;
            this.Panel_Visual.Location = new System.Drawing.Point(4, 20);
            this.Panel_Visual.Name = "Panel_Visual";
            this.Panel_Visual.Size = new System.Drawing.Size(496, 551);
            this.Panel_Visual.TabIndex = 0;
            // 
            // PseudoPilotBox
            // 
            this.PseudoPilotBox.Controls.Add(this.Panel_Pilot);
            this.PseudoPilotBox.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.PseudoPilotBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.PseudoPilotBox.Location = new System.Drawing.Point(995, 26);
            this.PseudoPilotBox.Name = "PseudoPilotBox";
            this.PseudoPilotBox.Size = new System.Drawing.Size(227, 609);
            this.PseudoPilotBox.TabIndex = 89;
            this.PseudoPilotBox.TabStop = false;
            this.PseudoPilotBox.Text = "Pseudo Pilot";
            // 
            // Panel_Pilot
            // 
            this.Panel_Pilot.AutoScroll = true;
            this.Panel_Pilot.Location = new System.Drawing.Point(4, 20);
            this.Panel_Pilot.Name = "Panel_Pilot";
            this.Panel_Pilot.Size = new System.Drawing.Size(217, 583);
            this.Panel_Pilot.TabIndex = 102;
            // 
            // ADCControllerBox
            // 
            this.ADCControllerBox.Controls.Add(this.Panel_ADC);
            this.ADCControllerBox.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.ADCControllerBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ADCControllerBox.Location = new System.Drawing.Point(527, 26);
            this.ADCControllerBox.Name = "ADCControllerBox";
            this.ADCControllerBox.Size = new System.Drawing.Size(218, 446);
            this.ADCControllerBox.TabIndex = 90;
            this.ADCControllerBox.TabStop = false;
            this.ADCControllerBox.Text = "ADC Controller";
            // 
            // Panel_ADC
            // 
            this.Panel_ADC.AutoScroll = true;
            this.Panel_ADC.Location = new System.Drawing.Point(4, 20);
            this.Panel_ADC.Name = "Panel_ADC";
            this.Panel_ADC.Size = new System.Drawing.Size(208, 420);
            this.Panel_ADC.TabIndex = 100;
            // 
            // editor_btn
            // 
            this.editor_btn.AutoSize = true;
            this.editor_btn.Font = new System.Drawing.Font("Century Gothic", 16F);
            this.editor_btn.ForeColor = System.Drawing.Color.White;
            this.editor_btn.Location = new System.Drawing.Point(12, 10);
            this.editor_btn.Name = "editor_btn";
            this.editor_btn.Size = new System.Drawing.Size(129, 25);
            this.editor_btn.TabIndex = 96;
            this.editor_btn.Text = "PC Control";
            // 
            // APPControllerBox
            // 
            this.APPControllerBox.Controls.Add(this.Panel_APP);
            this.APPControllerBox.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.APPControllerBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.APPControllerBox.Location = new System.Drawing.Point(762, 26);
            this.APPControllerBox.Name = "APPControllerBox";
            this.APPControllerBox.Size = new System.Drawing.Size(218, 313);
            this.APPControllerBox.TabIndex = 98;
            this.APPControllerBox.TabStop = false;
            this.APPControllerBox.Text = "APP Controller";
            // 
            // Panel_APP
            // 
            this.Panel_APP.AutoScroll = true;
            this.Panel_APP.Location = new System.Drawing.Point(4, 20);
            this.Panel_APP.Name = "Panel_APP";
            this.Panel_APP.Size = new System.Drawing.Size(208, 287);
            this.Panel_APP.TabIndex = 101;
            // 
            // Panel_Others
            // 
            this.Panel_Others.Location = new System.Drawing.Point(527, 482);
            this.Panel_Others.Name = "Panel_Others";
            this.Panel_Others.Size = new System.Drawing.Size(453, 153);
            this.Panel_Others.TabIndex = 102;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox1.Location = new System.Drawing.Point(8, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(505, 4);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 99;
            this.pictureBox1.TabStop = false;
            // 
            // enableForm_Btn
            // 
            this.enableForm_Btn.BackColor = System.Drawing.Color.DodgerBlue;
            this.enableForm_Btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.enableForm_Btn.FlatAppearance.BorderSize = 0;
            this.enableForm_Btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.enableForm_Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.enableForm_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enableForm_Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.enableForm_Btn.ForeColor = System.Drawing.Color.White;
            this.enableForm_Btn.Location = new System.Drawing.Point(762, 345);
            this.enableForm_Btn.Name = "enableForm_Btn";
            this.enableForm_Btn.Size = new System.Drawing.Size(218, 36);
            this.enableForm_Btn.TabIndex = 103;
            this.enableForm_Btn.Text = "Enable PC Control";
            this.enableForm_Btn.UseVisualStyleBackColor = false;
            this.enableForm_Btn.Click += new System.EventHandler(this.enableForm_Btn_Click);
            // 
            // PCControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1225, 645);
            this.Controls.Add(this.enableForm_Btn);
            this.Controls.Add(this.Panel_Others);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.APPControllerBox);
            this.Controls.Add(this.editor_btn);
            this.Controls.Add(this.ADCControllerBox);
            this.Controls.Add(this.PseudoPilotBox);
            this.Controls.Add(this.VisualBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PCControlForm";
            this.Text = "PCControlPanelForm";
            this.VisualBox.ResumeLayout(false);
            this.PseudoPilotBox.ResumeLayout(false);
            this.ADCControllerBox.ResumeLayout(false);
            this.APPControllerBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox VisualBox;
        private System.Windows.Forms.GroupBox PseudoPilotBox;
        private System.Windows.Forms.GroupBox ADCControllerBox;
        private System.Windows.Forms.Label editor_btn;
        private System.Windows.Forms.GroupBox APPControllerBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel Panel_Visual;
        private System.Windows.Forms.Panel Panel_ADC;
        private System.Windows.Forms.Panel Panel_Pilot;
        private System.Windows.Forms.Panel Panel_APP;
        private System.Windows.Forms.Panel Panel_Others;
        private System.Windows.Forms.Button enableForm_Btn;
    }
}