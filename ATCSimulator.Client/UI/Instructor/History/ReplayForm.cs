﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
using System.Net.Sockets;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using ATCSimulator.Client.UI;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.Services;

namespace ATCSimulator.Client.UI.Instructor.History
{
    public partial class ReplayForm : Telerik.WinControls.UI.RadForm
    {
        int counter;
        string lineData;
        StreamReader file;
        UdpClient udp;
        IPEndPoint remoteEndPointVisual;
        string[] lines;
        int playSpeed = 1;
        MainForm mainform;
        SimulationListModel _simulation;
        List<PlayerListModel> _player_list;
        int statusPlay = 0;
        public ReplayForm(MainForm mainform, SimulationListModel simulation, List<PlayerListModel> player_list)
        {
            InitializeComponent();
            this.mainform = mainform;
            mediaSlider19.Maximum = 6000;
            mediaSlider19.Value = counter;
            this._simulation = simulation;
            this._player_list = player_list;
            GenerateUserForm(_player_list);
            GenerateAircraftForm(_simulation.id);
            InitScenery(_simulation.scenery_id);
            Weather.Items.Clear();
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Clear", SimulationService.WeatherInfo.Clear));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Cloudy", SimulationService.WeatherInfo.Cloudy));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Light Rain", SimulationService.WeatherInfo.LightRain));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Thunder Storm", SimulationService.WeatherInfo.ThunderStorm));
            this.Location = new Point { X = Convert.ToInt32(ConfigurationManager.AppSettings["X_Position"].ToString()), Y = 0 };
            Setup();

        }
        private void Setup()
        {
            //string filepath = ConfigurationManager.AppSettings["ReplayPath"].ToString();
            //file = new StreamReader(filepath);
            //remoteEndPointVisual = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8015);
            //udp = new UdpClient();
            //lines = File.ReadLines(filepath).ToArray();
            //mediaSlider19.Maximum = lines.Count();

            TimeSimulationLabel.Text = _simulation.elapsed_time.HasValue ? _simulation.elapsed_time.Value.ToString(@"hh\:mm\:ss") : "00:00:00";
            ChangeLightProperties(_simulation.runway_lights, _simulation.approach, _simulation.taxi, _simulation.papi);
            ChangeWeatherProperties(_simulation.weather, _simulation.wind_direction, _simulation.wind_speed, _simulation.temperature
                , _simulation.visibility, _simulation.time_simulation);
        }

        #region Procedure and Function

        #region Init Scenery
        private void InitScenery(int sceneryId)
        {
            SceneryDetailResponse scenery_Data = SystemConnection.SceneryDetail(sceneryId);
            TaxiLight.Checked = false;
            PapiLight.Checked = false;

            #region Create RunwayLight
            RunwayGroup.Controls.Clear();
            for (int i = 0; i < scenery_Data.scenery.runway_light.Count(); i++)
            {
                int separation = 15;
                int width = (RunwayGroup.Size.Width - separation) / scenery_Data.scenery.runway_light.Count();
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = (width * i) + separation,
                    Y = 22
                };
                RunwayGroup.Controls.Add(CreateCheckBox(false, location, size, scenery_Data.scenery.runway_light[i]));
            }

            #endregion

            #region Create ApproachLight
            ApproachGroup.Controls.Clear();
            int counterApproach = 0;
            for (int i = 0; i < scenery_Data.scenery.approach_light.Count(); i++)
            {
                int separation = 15;
                int width = (ApproachGroup.Size.Width - separation) / (scenery_Data.scenery.approach_light.Count() / 2);
                int locationY = 30;
                int locationX = (width * counterApproach) + separation;
                if (i % 2 == 1)
                {
                    counterApproach++;
                    locationY = 65;
                }
                System.Drawing.Size size = new System.Drawing.Size()
                {
                    Height = 30,
                    Width = width - separation
                };
                System.Drawing.Point location = new Point()
                {
                    X = locationX,
                    Y = locationY
                };
                ApproachGroup.Controls.Add(CreateCheckBox(false, location, size, scenery_Data.scenery.approach_light[i]));
            }
            #endregion
        }
        #endregion

        #region CreateCheckBox
        private CheckBox CreateCheckBox(bool check, Point location, Size size, string text)
        {
            CheckBox ch = new CheckBox()
            {
                Appearance = Appearance.Button,
                BackColor = System.Drawing.Color.Tomato,
                Checked = check,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Century Gothic", 12F),
                ForeColor = System.Drawing.Color.White,
                Location = location,
                Enabled = false,
                Text = text,
                Size = size,
                TextAlign = ContentAlignment.MiddleCenter,
                TabStop = false
            };
            ch.FlatAppearance.BorderSize = 0;
            ch.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            return ch;
        }
        #endregion

        #region Light Properties
        private void ChangeLightProperties(string _runway, string _approach, bool _taxi, bool _papi)
        {
            try
            {
                Dictionary<string, bool> runway = JsonConvert.DeserializeObject<Dictionary<string, bool>>(_runway);
                Dictionary<string, bool> approach = JsonConvert.DeserializeObject<Dictionary<string, bool>>(_approach);
                for (int i = 0; i < ApproachGroup.Controls.Count; i++)
                {
                    CheckBox ch = (CheckBox)ApproachGroup.Controls[i];
                    foreach (var a in approach)
                    {
                        if (ch.Text.Equals(a.Key))
                        {
                            ch.Checked = a.Value;
                            break;
                        }
                    }
                }

                for (int i = 0; i < RunwayGroup.Controls.Count; i++)
                {
                    CheckBox ch = (CheckBox)RunwayGroup.Controls[i];
                    foreach (var r in runway)
                    {
                        if (ch.Text.Equals(r.Key))
                        {
                            ch.Checked = r.Value;
                            break;
                        }
                    }
                }
                TaxiLight.Checked = _taxi;
                PapiLight.Checked = _papi;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region Weather Properties
        private void ChangeWeatherProperties(string weather, int windDir, int windSpeed, int temp, int visibility, TimeSpan time)
        {
            try
            {
                Weather.SelectedIndex = Weather.Items.IndexOf(weather);
                WindDirection.Value = windDir;
                WindSpeed.Value = windSpeed;
                Temperature.Value = temp;
                Visibility.Value = visibility;
                EnvTime.Value = DateTime.Now.AddSeconds(time.TotalSeconds);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        private void GenerateAircraftForm(int simulationID)
        {
            AircraftHistoryListResponse list_aircraft = SystemConnection.AircraftHistoryList(simulationID);
            if (list_aircraft.status.code == 0)
            {
                int aircraft_count = 0;
                foreach (var ac in list_aircraft.aircrafts)
                {
                    ATCSimulator.Client.UI.Other.ReplayInfoForm a = new ATCSimulator.Client.UI.Other.ReplayInfoForm(ac,_player_list);
                    a.TopLevel = false;
                    a.Location = new System.Drawing.Point(0, aircraft_count * 85);
                    AircraftListPanel.Controls.Add(a);
                    a.Show();
                    a.Dock = DockStyle.None;
                    a.BringToFront();
                    aircraft_count++;
                }
            }
        }

        private void GenerateUserForm(List<PlayerListModel> list)
        {
            int x = 6;
            int y = 0;
            foreach (var player in list)
            {
                PlayerPanel.Controls.Add(CreateUserForm(new Point { X = x, Y = y }, player));
                y = y + 55;
            }
        }
        private Panel CreateUserForm(Point location, PlayerListModel player)
        {
            Panel panelLogin = new System.Windows.Forms.Panel();
            Button buttonLogin = new System.Windows.Forms.Button();
            PictureBox pictureLogin = new System.Windows.Forms.PictureBox();
            Label labelLogin = new System.Windows.Forms.Label();
            // 
            // panelLogin
            // 
            panelLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            panelLogin.Controls.Add(buttonLogin);
            panelLogin.Controls.Add(pictureLogin);
            panelLogin.Controls.Add(labelLogin);
            panelLogin.Location = location;
            panelLogin.Name = player.role_player + "_pnl";
            panelLogin.Size = new System.Drawing.Size(284, 50);
            // 
            // buttonLogin
            // 
            buttonLogin.BackColor = System.Drawing.Color.DodgerBlue;
            buttonLogin.FlatAppearance.BorderSize = 0;
            buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            buttonLogin.Font = new System.Drawing.Font("Century Gothic", 10F);
            buttonLogin.ForeColor = System.Drawing.Color.White;
            buttonLogin.Location = new System.Drawing.Point(50, 0);
            buttonLogin.Name = player.role_player + "_btn";
            buttonLogin.Size = new System.Drawing.Size(233, 26);
            buttonLogin.TabStop = false;
            buttonLogin.Enabled = false;
            buttonLogin.Text = player.role_player;
            buttonLogin.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            pictureLogin.Image = player.photo_image != null ? HelperClient.byteArrayToImage(player.photo_image) : global::ATCSimulator.Client.Properties.Resources.photo_img;
            pictureLogin.Location = new System.Drawing.Point(0, 0);
            pictureLogin.Name = player.role_player + "_picture";
            pictureLogin.Size = new System.Drawing.Size(50, 50);
            pictureLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            pictureLogin.TabStop = false;
            // 
            // label3
            // 
            labelLogin.Font = new System.Drawing.Font("Century Gothic", 10F);
            labelLogin.ForeColor = System.Drawing.Color.White;
            labelLogin.Location = new System.Drawing.Point(53, 27);
            labelLogin.Name = player.role_player + "_lbl";
            labelLogin.Size = new System.Drawing.Size(225, 19);
            labelLogin.Text = player.fullname;
            labelLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            return panelLogin;
        }
        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter = counter + playSpeed;
            if (counter > mediaSlider19.Maximum)
                StopReplay();
            else
                mediaSlider19.Value = counter;

        }


        public void SendUDP(string message)
        {
            try
            {
                byte[] data = Encoding.ASCII.GetBytes(message);
                udp.Send(data, data.Length, remoteEndPointVisual);
            }
            catch (Exception err)
            {
                //error
            }
        }

        private void mediaSlider19_ValueChanged(object sender, EventArgs e)
        {
            int value = mediaSlider19.Value;
            counter = value;
        }

        private void mediaSlider19_Click(object sender, EventArgs e)
        {
            int value = mediaSlider19.Value;
            counter = value;
        }

        private void SpeedNormal_Click(object sender, EventArgs e)
        {
            playSpeed = 1;
            timer1.Interval = 1;
        }

        private void Speed2_Click(object sender, EventArgs e)
        {
            playSpeed = 2;
            timer1.Interval = 1;
        }

        private void Speed3_Click(object sender, EventArgs e)
        {
            playSpeed = 3;
            timer1.Interval = 1;
        }

        private void Slow2_Click(object sender, EventArgs e)
        {
            playSpeed = 1;
            timer1.Interval = 30;
        }
        private void Slow3_Click(object sender, EventArgs e)
        {
            playSpeed = 1;
            timer1.Interval = 60;
        }

        private void PlayBtn_Click(object sender, EventArgs e)
        {
            if (statusPlay.Equals(0) || statusPlay.Equals(2))
            {
                timer1.Start();
                statusPlay = 1;
            }
            else
            {
                timer1.Stop();
                statusPlay = 2;
            }
            ChangeStatusButton(statusPlay);
        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            StopReplay();
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            mainform.Enabled = true;
            mainform.Show();
            this.Close();
        }
        public void StopReplay()
        {
            timer1.Stop();
            //file.Close();
            statusPlay = 0;
            counter = 0;
            mediaSlider19.Value = counter;
            ChangeStatusButton(statusPlay);
        }
        private void ChangeStatusButton(int status)
        {
            if (status.Equals(0) || status.Equals(2))
            {
                PlayBtn.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.play;
            }
            else
            {
                PlayBtn.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.pause;
            }
        }

    }
}
