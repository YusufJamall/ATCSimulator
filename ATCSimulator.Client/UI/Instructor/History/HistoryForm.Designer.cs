﻿namespace ATCSimulator.Client.UI.Instructor.History
{
    partial class HistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EnvTime = new Telerik.WinControls.UI.RadTimePicker();
            this.SimulationTable = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.GoToPage_Box = new Telerik.WinControls.UI.RadDropDownList();
            this.Total_Page_lbl = new System.Windows.Forms.Label();
            this.Page_text = new Telerik.WinControls.UI.RadTextBox();
            this.PlayerListPanel = new System.Windows.Forms.Panel();
            this.PlayerTable = new System.Windows.Forms.DataGridView();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Next_Btn = new System.Windows.Forms.Label();
            this.Prev_Btn = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.RemoveHistoryBtn = new System.Windows.Forms.Button();
            this.ReplayBtn = new System.Windows.Forms.Button();
            this.DetailPanel = new System.Windows.Forms.Panel();
            this.HistoryBtn = new System.Windows.Forms.Button();
            this.GivePointBtn = new System.Windows.Forms.Button();
            this.ClassYearLbl = new System.Windows.Forms.Label();
            this.ClassLbl = new System.Windows.Forms.Label();
            this.UserIdLbl = new System.Windows.Forms.Label();
            this.LastNameLbl = new System.Windows.Forms.Label();
            this.FirstNameLbl = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ClassYear_lbl = new System.Windows.Forms.Label();
            this.Image_Photo = new System.Windows.Forms.PictureBox();
            this.Class_lbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SimulationTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoToPage_Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_text)).BeginInit();
            this.PlayerListPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.DetailPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).BeginInit();
            this.SuspendLayout();
            // 
            // EnvTime
            // 
            this.EnvTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.EnvTime.Location = new System.Drawing.Point(199, 219);
            this.EnvTime.Name = "EnvTime";
            this.EnvTime.Size = new System.Drawing.Size(122, 24);
            this.EnvTime.Step = 30;
            this.EnvTime.TabIndex = 12;
            this.EnvTime.TabStop = false;
            this.EnvTime.Text = "radTimePicker1";
            this.EnvTime.TimeTables = Telerik.WinControls.UI.TimeTables.HoursAndMinutesInOneTable;
            this.EnvTime.Value = new System.DateTime(2014, 8, 16, 11, 30, 4, 0);
            // 
            // SimulationTable
            // 
            this.SimulationTable.AllowUserToAddRows = false;
            this.SimulationTable.AllowUserToDeleteRows = false;
            this.SimulationTable.AllowUserToOrderColumns = true;
            this.SimulationTable.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SimulationTable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SimulationTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SimulationTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.SimulationTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SimulationTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SimulationTable.ColumnHeadersHeight = 28;
            this.SimulationTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SimulationTable.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SimulationTable.DefaultCellStyle = dataGridViewCellStyle3;
            this.SimulationTable.EnableHeadersVisualStyles = false;
            this.SimulationTable.GridColor = System.Drawing.Color.Black;
            this.SimulationTable.Location = new System.Drawing.Point(4, 69);
            this.SimulationTable.MultiSelect = false;
            this.SimulationTable.Name = "SimulationTable";
            this.SimulationTable.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SimulationTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.SimulationTable.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SimulationTable.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.SimulationTable.RowTemplate.Height = 50;
            this.SimulationTable.RowTemplate.ReadOnly = true;
            this.SimulationTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SimulationTable.Size = new System.Drawing.Size(756, 532);
            this.SimulationTable.TabIndex = 2;
            this.SimulationTable.TabStop = false;
            this.SimulationTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SimulationTable_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(3, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 24);
            this.label1.TabIndex = 87;
            this.label1.Text = "Simulation History List";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(587, 613);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 22);
            this.label7.TabIndex = 176;
            this.label7.Text = "Go To Page";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // GoToPage_Box
            // 
            this.GoToPage_Box.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.GoToPage_Box.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.GoToPage_Box.Location = new System.Drawing.Point(708, 607);
            this.GoToPage_Box.Name = "GoToPage_Box";
            this.GoToPage_Box.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GoToPage_Box.Size = new System.Drawing.Size(51, 28);
            this.GoToPage_Box.TabIndex = 178;
            this.GoToPage_Box.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.GoToPage_Box_SelectedIndexChanged);
            // 
            // Total_Page_lbl
            // 
            this.Total_Page_lbl.AutoSize = true;
            this.Total_Page_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Total_Page_lbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Total_Page_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Total_Page_lbl.Location = new System.Drawing.Point(98, 613);
            this.Total_Page_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Total_Page_lbl.Name = "Total_Page_lbl";
            this.Total_Page_lbl.Size = new System.Drawing.Size(292, 22);
            this.Total_Page_lbl.TabIndex = 177;
            this.Total_Page_lbl.Text = "Displaying  100 - 100 from result";
            this.Total_Page_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Page_text
            // 
            this.Page_text.Enabled = false;
            this.Page_text.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.Page_text.Location = new System.Drawing.Point(34, 607);
            this.Page_text.Name = "Page_text";
            this.Page_text.NullText = "1";
            this.Page_text.Size = new System.Drawing.Size(29, 28);
            this.Page_text.TabIndex = 174;
            this.Page_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PlayerListPanel
            // 
            this.PlayerListPanel.Controls.Add(this.PlayerTable);
            this.PlayerListPanel.Controls.Add(this.PrintBtn);
            this.PlayerListPanel.Controls.Add(this.label9);
            this.PlayerListPanel.Controls.Add(this.pictureBox1);
            this.PlayerListPanel.Controls.Add(this.label2);
            this.PlayerListPanel.Location = new System.Drawing.Point(763, 30);
            this.PlayerListPanel.Name = "PlayerListPanel";
            this.PlayerListPanel.Size = new System.Drawing.Size(457, 365);
            this.PlayerListPanel.TabIndex = 179;
            // 
            // PlayerTable
            // 
            this.PlayerTable.AllowUserToAddRows = false;
            this.PlayerTable.AllowUserToDeleteRows = false;
            this.PlayerTable.AllowUserToOrderColumns = true;
            this.PlayerTable.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlayerTable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.PlayerTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PlayerTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.PlayerTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlayerTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.PlayerTable.ColumnHeadersHeight = 28;
            this.PlayerTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.PlayerTable.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PlayerTable.DefaultCellStyle = dataGridViewCellStyle8;
            this.PlayerTable.EnableHeadersVisualStyles = false;
            this.PlayerTable.GridColor = System.Drawing.Color.Black;
            this.PlayerTable.Location = new System.Drawing.Point(3, 39);
            this.PlayerTable.MultiSelect = false;
            this.PlayerTable.Name = "PlayerTable";
            this.PlayerTable.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlayerTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.PlayerTable.RowHeadersVisible = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlayerTable.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.PlayerTable.RowTemplate.Height = 50;
            this.PlayerTable.RowTemplate.ReadOnly = true;
            this.PlayerTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlayerTable.Size = new System.Drawing.Size(451, 276);
            this.PlayerTable.TabIndex = 183;
            this.PlayerTable.TabStop = false;
            this.PlayerTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PlayerTable_CellClick);
            // 
            // PrintBtn
            // 
            this.PrintBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.PrintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.PrintBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PrintBtn.ForeColor = System.Drawing.Color.White;
            this.PrintBtn.Location = new System.Drawing.Point(3, 321);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(202, 35);
            this.PrintBtn.TabIndex = 201;
            this.PrintBtn.TabStop = false;
            this.PrintBtn.Text = "Print Simulation Report";
            this.PrintBtn.UseVisualStyleBackColor = false;
            this.PrintBtn.Click += new System.EventHandler(this.PrintBtn_Click);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label9.ForeColor = System.Drawing.Color.DarkOrange;
            this.label9.Location = new System.Drawing.Point(225, 318);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(229, 21);
            this.label9.TabIndex = 181;
            this.label9.Text = "*Click to See Details";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox1.Location = new System.Drawing.Point(1, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(550, 3);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 180;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(3, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 24);
            this.label2.TabIndex = 180;
            this.label2.Text = "Players on Simulation";
            // 
            // Next_Btn
            // 
            this.Next_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Next_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Next_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Next_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_next;
            this.Next_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Next_Btn.Location = new System.Drawing.Point(66, 606);
            this.Next_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Next_Btn.Name = "Next_Btn";
            this.Next_Btn.Size = new System.Drawing.Size(34, 31);
            this.Next_Btn.TabIndex = 175;
            this.Next_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Next_Btn.Click += new System.EventHandler(this.Next_Btn_Click);
            this.Next_Btn.MouseLeave += new System.EventHandler(this.Next_Btn_MouseLeave);
            this.Next_Btn.MouseHover += new System.EventHandler(this.Next_Btn_MouseHover);
            // 
            // Prev_Btn
            // 
            this.Prev_Btn.BackColor = System.Drawing.Color.Transparent;
            this.Prev_Btn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prev_Btn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Prev_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_prev;
            this.Prev_Btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Prev_Btn.Location = new System.Drawing.Point(-2, 606);
            this.Prev_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Prev_Btn.Name = "Prev_Btn";
            this.Prev_Btn.Size = new System.Drawing.Size(34, 31);
            this.Prev_Btn.TabIndex = 173;
            this.Prev_Btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Prev_Btn.Click += new System.EventHandler(this.Prev_Btn_Click);
            this.Prev_Btn.MouseLeave += new System.EventHandler(this.Prev_Btn_MouseLeave);
            this.Prev_Btn.MouseHover += new System.EventHandler(this.Prev_Btn_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox4.Location = new System.Drawing.Point(4, 23);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(1215, 3);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 172;
            this.pictureBox4.TabStop = false;
            // 
            // RemoveHistoryBtn
            // 
            this.RemoveHistoryBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.RemoveHistoryBtn.Enabled = false;
            this.RemoveHistoryBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RemoveHistoryBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RemoveHistoryBtn.ForeColor = System.Drawing.Color.White;
            this.RemoveHistoryBtn.Location = new System.Drawing.Point(173, 30);
            this.RemoveHistoryBtn.Name = "RemoveHistoryBtn";
            this.RemoveHistoryBtn.Size = new System.Drawing.Size(166, 35);
            this.RemoveHistoryBtn.TabIndex = 181;
            this.RemoveHistoryBtn.TabStop = false;
            this.RemoveHistoryBtn.Text = "Remove History";
            this.RemoveHistoryBtn.UseVisualStyleBackColor = false;
            this.RemoveHistoryBtn.Click += new System.EventHandler(this.RemoveHistoryBtn_Click);
            // 
            // ReplayBtn
            // 
            this.ReplayBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.ReplayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ReplayBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ReplayBtn.ForeColor = System.Drawing.Color.White;
            this.ReplayBtn.Location = new System.Drawing.Point(2, 30);
            this.ReplayBtn.Name = "ReplayBtn";
            this.ReplayBtn.Size = new System.Drawing.Size(165, 35);
            this.ReplayBtn.TabIndex = 180;
            this.ReplayBtn.TabStop = false;
            this.ReplayBtn.Text = "View Replay";
            this.ReplayBtn.UseVisualStyleBackColor = false;
            this.ReplayBtn.Click += new System.EventHandler(this.ReplayBtn_Click);
            // 
            // DetailPanel
            // 
            this.DetailPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DetailPanel.Controls.Add(this.HistoryBtn);
            this.DetailPanel.Controls.Add(this.GivePointBtn);
            this.DetailPanel.Controls.Add(this.ClassYearLbl);
            this.DetailPanel.Controls.Add(this.ClassLbl);
            this.DetailPanel.Controls.Add(this.UserIdLbl);
            this.DetailPanel.Controls.Add(this.LastNameLbl);
            this.DetailPanel.Controls.Add(this.FirstNameLbl);
            this.DetailPanel.Controls.Add(this.label12);
            this.DetailPanel.Controls.Add(this.label11);
            this.DetailPanel.Controls.Add(this.label10);
            this.DetailPanel.Controls.Add(this.label8);
            this.DetailPanel.Controls.Add(this.label6);
            this.DetailPanel.Controls.Add(this.ClassYear_lbl);
            this.DetailPanel.Controls.Add(this.Image_Photo);
            this.DetailPanel.Controls.Add(this.Class_lbl);
            this.DetailPanel.Controls.Add(this.label5);
            this.DetailPanel.Controls.Add(this.label3);
            this.DetailPanel.Controls.Add(this.label4);
            this.DetailPanel.Location = new System.Drawing.Point(763, 401);
            this.DetailPanel.Name = "DetailPanel";
            this.DetailPanel.Size = new System.Drawing.Size(456, 239);
            this.DetailPanel.TabIndex = 182;
            // 
            // HistoryBtn
            // 
            this.HistoryBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.HistoryBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HistoryBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.HistoryBtn.ForeColor = System.Drawing.Color.White;
            this.HistoryBtn.Location = new System.Drawing.Point(113, 200);
            this.HistoryBtn.Name = "HistoryBtn";
            this.HistoryBtn.Size = new System.Drawing.Size(165, 35);
            this.HistoryBtn.TabIndex = 199;
            this.HistoryBtn.TabStop = false;
            this.HistoryBtn.Text = "History Simulation";
            this.HistoryBtn.UseVisualStyleBackColor = false;
            this.HistoryBtn.Click += new System.EventHandler(this.HistoryBtn_Click);
            // 
            // GivePointBtn
            // 
            this.GivePointBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.GivePointBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GivePointBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.GivePointBtn.ForeColor = System.Drawing.Color.White;
            this.GivePointBtn.Location = new System.Drawing.Point(284, 200);
            this.GivePointBtn.Name = "GivePointBtn";
            this.GivePointBtn.Size = new System.Drawing.Size(165, 35);
            this.GivePointBtn.TabIndex = 198;
            this.GivePointBtn.TabStop = false;
            this.GivePointBtn.Text = "Give Point";
            this.GivePointBtn.UseVisualStyleBackColor = false;
            this.GivePointBtn.Click += new System.EventHandler(this.GivePointBtn_Click);
            // 
            // ClassYearLbl
            // 
            this.ClassYearLbl.AutoSize = true;
            this.ClassYearLbl.BackColor = System.Drawing.Color.Transparent;
            this.ClassYearLbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.ClassYearLbl.ForeColor = System.Drawing.Color.Tomato;
            this.ClassYearLbl.Location = new System.Drawing.Point(282, 143);
            this.ClassYearLbl.Margin = new System.Windows.Forms.Padding(0);
            this.ClassYearLbl.Name = "ClassYearLbl";
            this.ClassYearLbl.Size = new System.Drawing.Size(103, 22);
            this.ClassYearLbl.TabIndex = 197;
            this.ClassYearLbl.Text = "First Name";
            this.ClassYearLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ClassLbl
            // 
            this.ClassLbl.AutoSize = true;
            this.ClassLbl.BackColor = System.Drawing.Color.Transparent;
            this.ClassLbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.ClassLbl.ForeColor = System.Drawing.Color.Tomato;
            this.ClassLbl.Location = new System.Drawing.Point(282, 110);
            this.ClassLbl.Margin = new System.Windows.Forms.Padding(0);
            this.ClassLbl.Name = "ClassLbl";
            this.ClassLbl.Size = new System.Drawing.Size(103, 22);
            this.ClassLbl.TabIndex = 196;
            this.ClassLbl.Text = "First Name";
            this.ClassLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // UserIdLbl
            // 
            this.UserIdLbl.AutoSize = true;
            this.UserIdLbl.BackColor = System.Drawing.Color.Transparent;
            this.UserIdLbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.UserIdLbl.ForeColor = System.Drawing.Color.Tomato;
            this.UserIdLbl.Location = new System.Drawing.Point(282, 79);
            this.UserIdLbl.Margin = new System.Windows.Forms.Padding(0);
            this.UserIdLbl.Name = "UserIdLbl";
            this.UserIdLbl.Size = new System.Drawing.Size(103, 22);
            this.UserIdLbl.TabIndex = 195;
            this.UserIdLbl.Text = "First Name";
            this.UserIdLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // LastNameLbl
            // 
            this.LastNameLbl.AutoSize = true;
            this.LastNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.LastNameLbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.LastNameLbl.ForeColor = System.Drawing.Color.Tomato;
            this.LastNameLbl.Location = new System.Drawing.Point(282, 41);
            this.LastNameLbl.Margin = new System.Windows.Forms.Padding(0);
            this.LastNameLbl.Name = "LastNameLbl";
            this.LastNameLbl.Size = new System.Drawing.Size(103, 22);
            this.LastNameLbl.TabIndex = 194;
            this.LastNameLbl.Text = "First Name";
            this.LastNameLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // FirstNameLbl
            // 
            this.FirstNameLbl.AutoSize = true;
            this.FirstNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.FirstNameLbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.FirstNameLbl.ForeColor = System.Drawing.Color.Tomato;
            this.FirstNameLbl.Location = new System.Drawing.Point(282, 6);
            this.FirstNameLbl.Margin = new System.Windows.Forms.Padding(0);
            this.FirstNameLbl.Name = "FirstNameLbl";
            this.FirstNameLbl.Size = new System.Drawing.Size(103, 22);
            this.FirstNameLbl.TabIndex = 193;
            this.FirstNameLbl.Text = "First Name";
            this.FirstNameLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label12.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label12.Location = new System.Drawing.Point(269, 143);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 22);
            this.label12.TabIndex = 192;
            this.label12.Text = ":";
            this.label12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label11.Location = new System.Drawing.Point(269, 110);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 22);
            this.label11.TabIndex = 191;
            this.label11.Text = ":";
            this.label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label10.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label10.Location = new System.Drawing.Point(269, 78);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 22);
            this.label10.TabIndex = 190;
            this.label10.Text = ":";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label8.Location = new System.Drawing.Point(269, 41);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 22);
            this.label8.TabIndex = 189;
            this.label8.Text = ":";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(269, 6);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 22);
            this.label6.TabIndex = 188;
            this.label6.Text = ":";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ClassYear_lbl
            // 
            this.ClassYear_lbl.AutoSize = true;
            this.ClassYear_lbl.BackColor = System.Drawing.Color.Transparent;
            this.ClassYear_lbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.ClassYear_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ClassYear_lbl.Location = new System.Drawing.Point(162, 143);
            this.ClassYear_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.ClassYear_lbl.Name = "ClassYear_lbl";
            this.ClassYear_lbl.Size = new System.Drawing.Size(102, 22);
            this.ClassYear_lbl.TabIndex = 187;
            this.ClassYear_lbl.Text = "Class Year";
            this.ClassYear_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Image_Photo
            // 
            this.Image_Photo.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            this.Image_Photo.Location = new System.Drawing.Point(0, 6);
            this.Image_Photo.Name = "Image_Photo";
            this.Image_Photo.Size = new System.Drawing.Size(163, 159);
            this.Image_Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Image_Photo.TabIndex = 31;
            this.Image_Photo.TabStop = false;
            // 
            // Class_lbl
            // 
            this.Class_lbl.AutoSize = true;
            this.Class_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Class_lbl.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.Class_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Class_lbl.Location = new System.Drawing.Point(162, 110);
            this.Class_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Class_lbl.Name = "Class_lbl";
            this.Class_lbl.Size = new System.Drawing.Size(55, 22);
            this.Class_lbl.TabIndex = 186;
            this.Class_lbl.Text = "Class";
            this.Class_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(162, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 22);
            this.label5.TabIndex = 183;
            this.label5.Text = "First Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(163, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 22);
            this.label3.TabIndex = 185;
            this.label3.Text = "User ID";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(162, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 22);
            this.label4.TabIndex = 184;
            this.label4.Text = "Last Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // HistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1225, 645);
            this.Controls.Add(this.DetailPanel);
            this.Controls.Add(this.RemoveHistoryBtn);
            this.Controls.Add(this.ReplayBtn);
            this.Controls.Add(this.PlayerListPanel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.GoToPage_Box);
            this.Controls.Add(this.Total_Page_lbl);
            this.Controls.Add(this.Next_Btn);
            this.Controls.Add(this.Page_text);
            this.Controls.Add(this.Prev_Btn);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.SimulationTable);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HistoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExercisePanelForm";
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SimulationTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoToPage_Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Page_text)).EndInit();
            this.PlayerListPanel.ResumeLayout(false);
            this.PlayerListPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.DetailPanel.ResumeLayout(false);
            this.DetailPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox4;
        private Telerik.WinControls.UI.RadTimePicker EnvTime;
        private System.Windows.Forms.DataGridView SimulationTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private Telerik.WinControls.UI.RadDropDownList GoToPage_Box;
        private System.Windows.Forms.Label Total_Page_lbl;
        private System.Windows.Forms.Label Next_Btn;
        private Telerik.WinControls.UI.RadTextBox Page_text;
        private System.Windows.Forms.Label Prev_Btn;
        private System.Windows.Forms.Panel PlayerListPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button RemoveHistoryBtn;
        private System.Windows.Forms.Button ReplayBtn;
        private System.Windows.Forms.Panel DetailPanel;
        private System.Windows.Forms.PictureBox Image_Photo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label ClassYear_lbl;
        private System.Windows.Forms.Label Class_lbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label ClassYearLbl;
        private System.Windows.Forms.Label ClassLbl;
        private System.Windows.Forms.Label UserIdLbl;
        private System.Windows.Forms.Label LastNameLbl;
        private System.Windows.Forms.Label FirstNameLbl;
        private System.Windows.Forms.Button GivePointBtn;
        private System.Windows.Forms.Button HistoryBtn;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.DataGridView PlayerTable;
    }
}