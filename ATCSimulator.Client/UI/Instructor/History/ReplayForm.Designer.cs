﻿namespace ATCSimulator.Client.UI.Instructor.History
{
    partial class ReplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mediaSlider19 = new MediaSlider.MediaSlider();
            this.Speed2 = new System.Windows.Forms.Button();
            this.Speed3 = new System.Windows.Forms.Button();
            this.SpeedNormal = new System.Windows.Forms.Button();
            this.Slow2 = new System.Windows.Forms.Button();
            this.Slow3 = new System.Windows.Forms.Button();
            this.SceneryLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TimeSimulationLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CommandPanel = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SimulationLog = new System.Windows.Forms.RichTextBox();
            this.LightsGroup = new System.Windows.Forms.GroupBox();
            this.ApproachGroup = new System.Windows.Forms.GroupBox();
            this.TaxiGroup = new System.Windows.Forms.GroupBox();
            this.PapiLight = new System.Windows.Forms.CheckBox();
            this.TaxiLight = new System.Windows.Forms.CheckBox();
            this.RunwayGroup = new System.Windows.Forms.GroupBox();
            this.EnvPropGroup = new System.Windows.Forms.GroupBox();
            this.EnvTime = new Telerik.WinControls.UI.RadTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Visibility = new Telerik.WinControls.UI.RadSpinEditor();
            this.Temperature = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindSpeed = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindDirection = new Telerik.WinControls.UI.RadSpinEditor();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Weather = new Telerik.WinControls.UI.RadDropDownList();
            this.label33 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.FlightRouteGroup = new System.Windows.Forms.GroupBox();
            this.PlayBtn = new System.Windows.Forms.Button();
            this.StopBtn = new System.Windows.Forms.Button();
            this.AircraftListPanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LineBlue = new System.Windows.Forms.PictureBox();
            this.PlayerBox = new System.Windows.Forms.GroupBox();
            this.PlayerPanel = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.CommandPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.LightsGroup.SuspendLayout();
            this.TaxiGroup.SuspendLayout();
            this.EnvPropGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).BeginInit();
            this.FlightRouteGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBlue)).BeginInit();
            this.PlayerBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mediaSlider19
            // 
            this.mediaSlider19.Animated = true;
            this.mediaSlider19.AnimationSize = 0.2F;
            this.mediaSlider19.AnimationSpeed = MediaSlider.MediaSlider.AnimateSpeed.Normal;
            this.mediaSlider19.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.mediaSlider19.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.mediaSlider19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.mediaSlider19.BackGroundImage = null;
            this.mediaSlider19.ButtonAccentColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(205)))), ((int)(((byte)(229)))));
            this.mediaSlider19.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(114)))), ((int)(((byte)(166)))));
            this.mediaSlider19.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(84)))), ((int)(((byte)(152)))));
            this.mediaSlider19.ButtonCornerRadius = ((uint)(6u));
            this.mediaSlider19.ButtonSize = new System.Drawing.Size(20, 10);
            this.mediaSlider19.ButtonStyle = MediaSlider.MediaSlider.ButtonType.RoundedRectInline;
            this.mediaSlider19.ContextMenuStrip = null;
            this.mediaSlider19.LargeChange = 2;
            this.mediaSlider19.Location = new System.Drawing.Point(7, 31);
            this.mediaSlider19.Margin = new System.Windows.Forms.Padding(0);
            this.mediaSlider19.Maximum = 10;
            this.mediaSlider19.Minimum = 0;
            this.mediaSlider19.Name = "mediaSlider19";
            this.mediaSlider19.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.mediaSlider19.ShowButtonOnHover = false;
            this.mediaSlider19.Size = new System.Drawing.Size(706, 10);
            this.mediaSlider19.SliderFlyOut = MediaSlider.MediaSlider.FlyOutStyle.None;
            this.mediaSlider19.SmallChange = 1;
            this.mediaSlider19.SmoothScrolling = true;
            this.mediaSlider19.TabIndex = 14;
            this.mediaSlider19.TickColor = System.Drawing.Color.DarkGray;
            this.mediaSlider19.TickStyle = System.Windows.Forms.TickStyle.None;
            this.mediaSlider19.TickType = MediaSlider.MediaSlider.TickMode.Standard;
            this.mediaSlider19.TrackBorderColor = System.Drawing.Color.White;
            this.mediaSlider19.TrackDepth = 4;
            this.mediaSlider19.TrackFillColor = System.Drawing.Color.Transparent;
            this.mediaSlider19.TrackProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(101)))), ((int)(((byte)(188)))));
            this.mediaSlider19.TrackShadow = true;
            this.mediaSlider19.TrackShadowColor = System.Drawing.Color.DarkGray;
            this.mediaSlider19.TrackStyle = MediaSlider.MediaSlider.TrackType.Progress;
            this.mediaSlider19.Value = 1;
            this.mediaSlider19.ValueChanged += new MediaSlider.MediaSlider.ValueChangedDelegate(this.mediaSlider19_ValueChanged);
            this.mediaSlider19.Click += new System.EventHandler(this.mediaSlider19_Click);
            // 
            // Speed2
            // 
            this.Speed2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Speed2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Speed2.ForeColor = System.Drawing.Color.White;
            this.Speed2.Location = new System.Drawing.Point(241, 50);
            this.Speed2.Name = "Speed2";
            this.Speed2.Size = new System.Drawing.Size(41, 26);
            this.Speed2.TabIndex = 15;
            this.Speed2.Text = "2x";
            this.Speed2.UseVisualStyleBackColor = false;
            this.Speed2.Click += new System.EventHandler(this.Speed2_Click);
            // 
            // Speed3
            // 
            this.Speed3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Speed3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Speed3.ForeColor = System.Drawing.Color.White;
            this.Speed3.Location = new System.Drawing.Point(288, 50);
            this.Speed3.Name = "Speed3";
            this.Speed3.Size = new System.Drawing.Size(41, 26);
            this.Speed3.TabIndex = 16;
            this.Speed3.Text = "3x";
            this.Speed3.UseVisualStyleBackColor = false;
            this.Speed3.Click += new System.EventHandler(this.Speed3_Click);
            // 
            // SpeedNormal
            // 
            this.SpeedNormal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SpeedNormal.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpeedNormal.ForeColor = System.Drawing.Color.White;
            this.SpeedNormal.Location = new System.Drawing.Point(194, 50);
            this.SpeedNormal.Name = "SpeedNormal";
            this.SpeedNormal.Size = new System.Drawing.Size(41, 26);
            this.SpeedNormal.TabIndex = 17;
            this.SpeedNormal.Text = "1x";
            this.SpeedNormal.UseVisualStyleBackColor = false;
            this.SpeedNormal.Click += new System.EventHandler(this.SpeedNormal_Click);
            // 
            // Slow2
            // 
            this.Slow2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Slow2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Slow2.ForeColor = System.Drawing.Color.White;
            this.Slow2.Location = new System.Drawing.Point(147, 50);
            this.Slow2.Name = "Slow2";
            this.Slow2.Size = new System.Drawing.Size(41, 26);
            this.Slow2.TabIndex = 18;
            this.Slow2.Text = "-2x";
            this.Slow2.UseVisualStyleBackColor = false;
            this.Slow2.Click += new System.EventHandler(this.Slow2_Click);
            // 
            // Slow3
            // 
            this.Slow3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Slow3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Slow3.ForeColor = System.Drawing.Color.White;
            this.Slow3.Location = new System.Drawing.Point(100, 50);
            this.Slow3.Name = "Slow3";
            this.Slow3.Size = new System.Drawing.Size(41, 26);
            this.Slow3.TabIndex = 19;
            this.Slow3.Text = "-3x";
            this.Slow3.UseVisualStyleBackColor = false;
            this.Slow3.Click += new System.EventHandler(this.Slow3_Click);
            // 
            // SceneryLabel
            // 
            this.SceneryLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.SceneryLabel.ForeColor = System.Drawing.Color.White;
            this.SceneryLabel.Location = new System.Drawing.Point(387, 0);
            this.SceneryLabel.Name = "SceneryLabel";
            this.SceneryLabel.Size = new System.Drawing.Size(290, 21);
            this.SceneryLabel.TabIndex = 134;
            this.SceneryLabel.Text = "Seahorse";
            this.SceneryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(289, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 21);
            this.label4.TabIndex = 133;
            this.label4.Text = "Scenery :";
            // 
            // TimeSimulationLabel
            // 
            this.TimeSimulationLabel.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.TimeSimulationLabel.ForeColor = System.Drawing.Color.White;
            this.TimeSimulationLabel.Location = new System.Drawing.Point(910, 0);
            this.TimeSimulationLabel.Name = "TimeSimulationLabel";
            this.TimeSimulationLabel.Size = new System.Drawing.Size(94, 21);
            this.TimeSimulationLabel.TabIndex = 132;
            this.TimeSimulationLabel.Text = "00:00:00";
            this.TimeSimulationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(765, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 21);
            this.label2.TabIndex = 131;
            this.label2.Text = "Simulation Time :";
            // 
            // CommandPanel
            // 
            this.CommandPanel.Controls.Add(this.groupBox1);
            this.CommandPanel.Controls.Add(this.LightsGroup);
            this.CommandPanel.Controls.Add(this.EnvPropGroup);
            this.CommandPanel.Controls.Add(this.FlightRouteGroup);
            this.CommandPanel.Location = new System.Drawing.Point(281, 32);
            this.CommandPanel.Name = "CommandPanel";
            this.CommandPanel.Size = new System.Drawing.Size(735, 698);
            this.CommandPanel.TabIndex = 128;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SimulationLog);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.groupBox1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.groupBox1.Location = new System.Drawing.Point(5, 269);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(719, 323);
            this.groupBox1.TabIndex = 110;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Log";
            // 
            // SimulationLog
            // 
            this.SimulationLog.BackColor = System.Drawing.Color.Black;
            this.SimulationLog.Font = new System.Drawing.Font("Arial", 10F);
            this.SimulationLog.ForeColor = System.Drawing.Color.White;
            this.SimulationLog.Location = new System.Drawing.Point(6, 28);
            this.SimulationLog.Name = "SimulationLog";
            this.SimulationLog.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SimulationLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.SimulationLog.Size = new System.Drawing.Size(707, 289);
            this.SimulationLog.TabIndex = 134;
            this.SimulationLog.Text = "";
            // 
            // LightsGroup
            // 
            this.LightsGroup.Controls.Add(this.ApproachGroup);
            this.LightsGroup.Controls.Add(this.TaxiGroup);
            this.LightsGroup.Controls.Add(this.RunwayGroup);
            this.LightsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.LightsGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LightsGroup.Location = new System.Drawing.Point(5, 4);
            this.LightsGroup.Name = "LightsGroup";
            this.LightsGroup.Size = new System.Drawing.Size(347, 259);
            this.LightsGroup.TabIndex = 108;
            this.LightsGroup.TabStop = false;
            this.LightsGroup.Text = "Lights Properties";
            // 
            // ApproachGroup
            // 
            this.ApproachGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ApproachGroup.ForeColor = System.Drawing.Color.White;
            this.ApproachGroup.Location = new System.Drawing.Point(7, 145);
            this.ApproachGroup.Name = "ApproachGroup";
            this.ApproachGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ApproachGroup.Size = new System.Drawing.Size(334, 108);
            this.ApproachGroup.TabIndex = 4;
            this.ApproachGroup.TabStop = false;
            this.ApproachGroup.Text = "Approach";
            // 
            // TaxiGroup
            // 
            this.TaxiGroup.Controls.Add(this.PapiLight);
            this.TaxiGroup.Controls.Add(this.TaxiLight);
            this.TaxiGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiGroup.ForeColor = System.Drawing.Color.White;
            this.TaxiGroup.Location = new System.Drawing.Point(6, 85);
            this.TaxiGroup.Name = "TaxiGroup";
            this.TaxiGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TaxiGroup.Size = new System.Drawing.Size(335, 60);
            this.TaxiGroup.TabIndex = 3;
            this.TaxiGroup.TabStop = false;
            this.TaxiGroup.Text = "Taxi / Papi";
            // 
            // PapiLight
            // 
            this.PapiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.PapiLight.BackColor = System.Drawing.Color.Tomato;
            this.PapiLight.Enabled = false;
            this.PapiLight.FlatAppearance.BorderSize = 0;
            this.PapiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.PapiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PapiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PapiLight.ForeColor = System.Drawing.Color.White;
            this.PapiLight.Location = new System.Drawing.Point(175, 22);
            this.PapiLight.Name = "PapiLight";
            this.PapiLight.Size = new System.Drawing.Size(145, 30);
            this.PapiLight.TabIndex = 108;
            this.PapiLight.TabStop = false;
            this.PapiLight.Text = "Papi";
            this.PapiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PapiLight.UseVisualStyleBackColor = false;
            // 
            // TaxiLight
            // 
            this.TaxiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.TaxiLight.BackColor = System.Drawing.Color.Tomato;
            this.TaxiLight.Enabled = false;
            this.TaxiLight.FlatAppearance.BorderSize = 0;
            this.TaxiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.TaxiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaxiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiLight.ForeColor = System.Drawing.Color.White;
            this.TaxiLight.Location = new System.Drawing.Point(15, 22);
            this.TaxiLight.Name = "TaxiLight";
            this.TaxiLight.Size = new System.Drawing.Size(145, 30);
            this.TaxiLight.TabIndex = 107;
            this.TaxiLight.TabStop = false;
            this.TaxiLight.Text = "Taxi";
            this.TaxiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TaxiLight.UseVisualStyleBackColor = false;
            // 
            // RunwayGroup
            // 
            this.RunwayGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RunwayGroup.ForeColor = System.Drawing.Color.White;
            this.RunwayGroup.Location = new System.Drawing.Point(6, 24);
            this.RunwayGroup.Name = "RunwayGroup";
            this.RunwayGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RunwayGroup.Size = new System.Drawing.Size(335, 60);
            this.RunwayGroup.TabIndex = 2;
            this.RunwayGroup.TabStop = false;
            this.RunwayGroup.Text = "Runway";
            // 
            // EnvPropGroup
            // 
            this.EnvPropGroup.Controls.Add(this.EnvTime);
            this.EnvPropGroup.Controls.Add(this.label12);
            this.EnvPropGroup.Controls.Add(this.label11);
            this.EnvPropGroup.Controls.Add(this.Visibility);
            this.EnvPropGroup.Controls.Add(this.Temperature);
            this.EnvPropGroup.Controls.Add(this.WindSpeed);
            this.EnvPropGroup.Controls.Add(this.WindDirection);
            this.EnvPropGroup.Controls.Add(this.label10);
            this.EnvPropGroup.Controls.Add(this.label9);
            this.EnvPropGroup.Controls.Add(this.label8);
            this.EnvPropGroup.Controls.Add(this.Weather);
            this.EnvPropGroup.Controls.Add(this.label33);
            this.EnvPropGroup.Controls.Add(this.label43);
            this.EnvPropGroup.Controls.Add(this.label44);
            this.EnvPropGroup.Controls.Add(this.label45);
            this.EnvPropGroup.Controls.Add(this.label49);
            this.EnvPropGroup.Controls.Add(this.label50);
            this.EnvPropGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.EnvPropGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.EnvPropGroup.Location = new System.Drawing.Point(394, 4);
            this.EnvPropGroup.Name = "EnvPropGroup";
            this.EnvPropGroup.Size = new System.Drawing.Size(331, 259);
            this.EnvPropGroup.TabIndex = 107;
            this.EnvPropGroup.TabStop = false;
            this.EnvPropGroup.Text = "Environment Properties";
            // 
            // EnvTime
            // 
            this.EnvTime.Enabled = false;
            this.EnvTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.EnvTime.Location = new System.Drawing.Point(199, 219);
            this.EnvTime.Name = "EnvTime";
            this.EnvTime.Size = new System.Drawing.Size(122, 24);
            this.EnvTime.Step = 30;
            this.EnvTime.TabIndex = 12;
            this.EnvTime.TabStop = false;
            this.EnvTime.Text = "radTimePicker1";
            this.EnvTime.TimeTables = Telerik.WinControls.UI.TimeTables.HoursAndMinutesInOneTable;
            this.EnvTime.Value = new System.DateTime(2014, 8, 16, 11, 30, 4, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(299, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 17);
            this.label12.TabIndex = 118;
            this.label12.Text = "m";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(299, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 117;
            this.label11.Text = "o";
            // 
            // Visibility
            // 
            this.Visibility.Enabled = false;
            this.Visibility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Visibility.Location = new System.Drawing.Point(199, 182);
            this.Visibility.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.Visibility.Name = "Visibility";
            this.Visibility.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.Visibility.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Visibility.Size = new System.Drawing.Size(100, 24);
            this.Visibility.TabIndex = 11;
            this.Visibility.TabStop = false;
            this.Visibility.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Visibility.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // Temperature
            // 
            this.Temperature.Enabled = false;
            this.Temperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Temperature.Location = new System.Drawing.Point(199, 146);
            this.Temperature.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Temperature.Name = "Temperature";
            this.Temperature.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.Temperature.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Temperature.Size = new System.Drawing.Size(100, 24);
            this.Temperature.TabIndex = 10;
            this.Temperature.TabStop = false;
            this.Temperature.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Temperature.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // WindSpeed
            // 
            this.WindSpeed.Enabled = false;
            this.WindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindSpeed.Location = new System.Drawing.Point(199, 111);
            this.WindSpeed.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.WindSpeed.Name = "WindSpeed";
            this.WindSpeed.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.WindSpeed.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindSpeed.Size = new System.Drawing.Size(100, 24);
            this.WindSpeed.TabIndex = 9;
            this.WindSpeed.TabStop = false;
            this.WindSpeed.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // WindDirection
            // 
            this.WindDirection.Enabled = false;
            this.WindDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindDirection.Location = new System.Drawing.Point(199, 76);
            this.WindDirection.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.WindDirection.Name = "WindDirection";
            this.WindDirection.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.WindDirection.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindDirection.Size = new System.Drawing.Size(100, 24);
            this.WindDirection.TabIndex = 8;
            this.WindDirection.TabStop = false;
            this.WindDirection.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindDirection.Value = new decimal(new int[] {
            359,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(304, 150);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 112;
            this.label10.Text = "C";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(299, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 17);
            this.label9.TabIndex = 111;
            this.label9.Text = "kn";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(299, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 109;
            this.label8.Text = "o";
            // 
            // Weather
            // 
            this.Weather.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Weather.Enabled = false;
            this.Weather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Weather.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Weather.Location = new System.Drawing.Point(199, 37);
            this.Weather.Name = "Weather";
            this.Weather.Size = new System.Drawing.Size(124, 24);
            this.Weather.TabIndex = 7;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(8, 219);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(43, 20);
            this.label33.TabIndex = 24;
            this.label33.Text = "Time";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(8, 183);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(64, 20);
            this.label43.TabIndex = 14;
            this.label43.Text = "Visibility";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(8, 146);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(100, 20);
            this.label44.TabIndex = 12;
            this.label44.Text = "Temperature";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(8, 76);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(112, 20);
            this.label45.TabIndex = 9;
            this.label45.Text = "Wind Direction";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(8, 37);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(70, 20);
            this.label49.TabIndex = 7;
            this.label49.Text = "Weather";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(8, 114);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(96, 20);
            this.label50.TabIndex = 2;
            this.label50.Text = "Wind Speed";
            // 
            // FlightRouteGroup
            // 
            this.FlightRouteGroup.Controls.Add(this.PlayBtn);
            this.FlightRouteGroup.Controls.Add(this.StopBtn);
            this.FlightRouteGroup.Controls.Add(this.Slow3);
            this.FlightRouteGroup.Controls.Add(this.mediaSlider19);
            this.FlightRouteGroup.Controls.Add(this.SpeedNormal);
            this.FlightRouteGroup.Controls.Add(this.Slow2);
            this.FlightRouteGroup.Controls.Add(this.Speed2);
            this.FlightRouteGroup.Controls.Add(this.Speed3);
            this.FlightRouteGroup.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.FlightRouteGroup.ForeColor = System.Drawing.Color.White;
            this.FlightRouteGroup.Location = new System.Drawing.Point(5, 598);
            this.FlightRouteGroup.Name = "FlightRouteGroup";
            this.FlightRouteGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FlightRouteGroup.Size = new System.Drawing.Size(720, 87);
            this.FlightRouteGroup.TabIndex = 106;
            this.FlightRouteGroup.TabStop = false;
            this.FlightRouteGroup.Text = "Replay Control";
            // 
            // PlayBtn
            // 
            this.PlayBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PlayBtn.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.play;
            this.PlayBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlayBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.PlayBtn.FlatAppearance.BorderSize = 0;
            this.PlayBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.PlayBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.PlayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.PlayBtn.ForeColor = System.Drawing.Color.White;
            this.PlayBtn.Location = new System.Drawing.Point(8, 47);
            this.PlayBtn.Name = "PlayBtn";
            this.PlayBtn.Size = new System.Drawing.Size(40, 32);
            this.PlayBtn.TabIndex = 109;
            this.PlayBtn.UseVisualStyleBackColor = false;
            this.PlayBtn.Click += new System.EventHandler(this.PlayBtn_Click);
            // 
            // StopBtn
            // 
            this.StopBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StopBtn.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.stop;
            this.StopBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.StopBtn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.StopBtn.FlatAppearance.BorderSize = 0;
            this.StopBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.StopBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Tomato;
            this.StopBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StopBtn.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.StopBtn.ForeColor = System.Drawing.Color.White;
            this.StopBtn.Location = new System.Drawing.Point(54, 47);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(40, 32);
            this.StopBtn.TabIndex = 108;
            this.StopBtn.UseVisualStyleBackColor = false;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // AircraftListPanel
            // 
            this.AircraftListPanel.AutoScroll = true;
            this.AircraftListPanel.Location = new System.Drawing.Point(3, 32);
            this.AircraftListPanel.Name = "AircraftListPanel";
            this.AircraftListPanel.Size = new System.Drawing.Size(271, 698);
            this.AircraftListPanel.TabIndex = 127;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(5, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 22);
            this.label5.TabIndex = 126;
            this.label5.Text = "Aircraft List";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.lineWhite;
            this.pictureBox1.Location = new System.Drawing.Point(275, -4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(4, 733);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 130;
            this.pictureBox1.TabStop = false;
            // 
            // LineBlue
            // 
            this.LineBlue.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.LineBlue.Location = new System.Drawing.Point(6, 26);
            this.LineBlue.Name = "LineBlue";
            this.LineBlue.Size = new System.Drawing.Size(1342, 4);
            this.LineBlue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LineBlue.TabIndex = 129;
            this.LineBlue.TabStop = false;
            // 
            // PlayerBox
            // 
            this.PlayerBox.Controls.Add(this.PlayerPanel);
            this.PlayerBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.PlayerBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.PlayerBox.Location = new System.Drawing.Point(1022, 33);
            this.PlayerBox.Name = "PlayerBox";
            this.PlayerBox.Size = new System.Drawing.Size(326, 606);
            this.PlayerBox.TabIndex = 104;
            this.PlayerBox.TabStop = false;
            this.PlayerBox.Text = "Player on Simulation";
            // 
            // PlayerPanel
            // 
            this.PlayerPanel.AutoScroll = true;
            this.PlayerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PlayerPanel.Location = new System.Drawing.Point(6, 28);
            this.PlayerPanel.Name = "PlayerPanel";
            this.PlayerPanel.Size = new System.Drawing.Size(314, 572);
            this.PlayerPanel.TabIndex = 136;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ATCSimulator.Client.Properties.Resources.lineWhite;
            this.pictureBox2.Location = new System.Drawing.Point(1012, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(4, 733);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 138;
            this.pictureBox2.TabStop = false;
            // 
            // CloseBtn
            // 
            this.CloseBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.CloseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CloseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CloseBtn.ForeColor = System.Drawing.Color.White;
            this.CloseBtn.Location = new System.Drawing.Point(1022, 645);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(330, 71);
            this.CloseBtn.TabIndex = 202;
            this.CloseBtn.TabStop = false;
            this.CloseBtn.Text = "Back To Main Form";
            this.CloseBtn.UseVisualStyleBackColor = false;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // ReplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1360, 728);
            this.Controls.Add(this.PlayerBox);
            this.Controls.Add(this.CloseBtn);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.SceneryLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TimeSimulationLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LineBlue);
            this.Controls.Add(this.CommandPanel);
            this.Controls.Add(this.AircraftListPanel);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReplayForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Replay Form";
            this.CommandPanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.LightsGroup.ResumeLayout(false);
            this.TaxiGroup.ResumeLayout(false);
            this.EnvPropGroup.ResumeLayout(false);
            this.EnvPropGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).EndInit();
            this.FlightRouteGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineBlue)).EndInit();
            this.PlayerBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private MediaSlider.MediaSlider mediaSlider19;
        private System.Windows.Forms.Button Speed2;
        private System.Windows.Forms.Button Speed3;
        private System.Windows.Forms.Button SpeedNormal;
        private System.Windows.Forms.Button Slow2;
        private System.Windows.Forms.Button Slow3;
        private System.Windows.Forms.Label SceneryLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TimeSimulationLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox LineBlue;
        private System.Windows.Forms.Panel CommandPanel;
        private System.Windows.Forms.Panel AircraftListPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox FlightRouteGroup;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.GroupBox PlayerBox;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.Button PlayBtn;
        private System.Windows.Forms.Panel PlayerPanel;
        private System.Windows.Forms.GroupBox LightsGroup;
        private System.Windows.Forms.GroupBox ApproachGroup;
        private System.Windows.Forms.GroupBox TaxiGroup;
        private System.Windows.Forms.CheckBox PapiLight;
        private System.Windows.Forms.CheckBox TaxiLight;
        private System.Windows.Forms.GroupBox RunwayGroup;
        private System.Windows.Forms.GroupBox EnvPropGroup;
        private Telerik.WinControls.UI.RadTimePicker EnvTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadSpinEditor Visibility;
        private Telerik.WinControls.UI.RadSpinEditor Temperature;
        private Telerik.WinControls.UI.RadSpinEditor WindSpeed;
        private Telerik.WinControls.UI.RadSpinEditor WindDirection;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadDropDownList Weather;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox SimulationLog;

    }
}