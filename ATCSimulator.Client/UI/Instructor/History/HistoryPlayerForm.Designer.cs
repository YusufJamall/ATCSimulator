﻿namespace ATCSimulator.Client.UI.Instructor.History
{
    partial class HistoryPlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.HistoryTable = new System.Windows.Forms.DataGridView();
            this.Image_Photo = new System.Windows.Forms.PictureBox();
            this.ClassYearLbl = new System.Windows.Forms.Label();
            this.ClassLbl = new System.Windows.Forms.Label();
            this.UserIdLbl = new System.Windows.Forms.Label();
            this.LastNameLbl = new System.Windows.Forms.Label();
            this.FirstNameLbl = new System.Windows.Forms.Label();
            this.ClassYear_lbl = new System.Windows.Forms.Label();
            this.Class_lbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseBtn
            // 
            this.CloseBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.CloseBtn.FlatAppearance.BorderSize = 0;
            this.CloseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CloseBtn.ForeColor = System.Drawing.Color.White;
            this.CloseBtn.Location = new System.Drawing.Point(159, 393);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(145, 34);
            this.CloseBtn.TabIndex = 91;
            this.CloseBtn.Text = "Close";
            this.CloseBtn.UseVisualStyleBackColor = false;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // PrintBtn
            // 
            this.PrintBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.PrintBtn.FlatAppearance.BorderSize = 0;
            this.PrintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PrintBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PrintBtn.ForeColor = System.Drawing.Color.White;
            this.PrintBtn.Location = new System.Drawing.Point(7, 393);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(145, 34);
            this.PrintBtn.TabIndex = 92;
            this.PrintBtn.Text = "Print";
            this.PrintBtn.UseVisualStyleBackColor = false;
            this.PrintBtn.Click += new System.EventHandler(this.PrintBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 24);
            this.label1.TabIndex = 93;
            this.label1.Text = "Player Simulation History";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox4.Location = new System.Drawing.Point(6, 27);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(956, 3);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 173;
            this.pictureBox4.TabStop = false;
            // 
            // HistoryTable
            // 
            this.HistoryTable.AllowUserToAddRows = false;
            this.HistoryTable.AllowUserToDeleteRows = false;
            this.HistoryTable.AllowUserToOrderColumns = true;
            this.HistoryTable.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryTable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.HistoryTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.HistoryTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.HistoryTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.HistoryTable.ColumnHeadersHeight = 28;
            this.HistoryTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.HistoryTable.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HistoryTable.DefaultCellStyle = dataGridViewCellStyle3;
            this.HistoryTable.EnableHeadersVisualStyles = false;
            this.HistoryTable.GridColor = System.Drawing.Color.Black;
            this.HistoryTable.Location = new System.Drawing.Point(310, 36);
            this.HistoryTable.MultiSelect = false;
            this.HistoryTable.Name = "HistoryTable";
            this.HistoryTable.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.HistoryTable.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HistoryTable.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.HistoryTable.RowTemplate.Height = 70;
            this.HistoryTable.RowTemplate.ReadOnly = true;
            this.HistoryTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HistoryTable.Size = new System.Drawing.Size(651, 391);
            this.HistoryTable.TabIndex = 174;
            this.HistoryTable.TabStop = false;
            // 
            // Image_Photo
            // 
            this.Image_Photo.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            this.Image_Photo.Location = new System.Drawing.Point(6, 36);
            this.Image_Photo.Name = "Image_Photo";
            this.Image_Photo.Size = new System.Drawing.Size(175, 171);
            this.Image_Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Image_Photo.TabIndex = 175;
            this.Image_Photo.TabStop = false;
            // 
            // ClassYearLbl
            // 
            this.ClassYearLbl.AutoSize = true;
            this.ClassYearLbl.BackColor = System.Drawing.Color.Transparent;
            this.ClassYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.ClassYearLbl.ForeColor = System.Drawing.Color.Tomato;
            this.ClassYearLbl.Location = new System.Drawing.Point(122, 347);
            this.ClassYearLbl.Margin = new System.Windows.Forms.Padding(0);
            this.ClassYearLbl.Name = "ClassYearLbl";
            this.ClassYearLbl.Size = new System.Drawing.Size(101, 24);
            this.ClassYearLbl.TabIndex = 210;
            this.ClassYearLbl.Text = "First Name";
            this.ClassYearLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ClassLbl
            // 
            this.ClassLbl.AutoSize = true;
            this.ClassLbl.BackColor = System.Drawing.Color.Transparent;
            this.ClassLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.ClassLbl.ForeColor = System.Drawing.Color.Tomato;
            this.ClassLbl.Location = new System.Drawing.Point(122, 314);
            this.ClassLbl.Margin = new System.Windows.Forms.Padding(0);
            this.ClassLbl.Name = "ClassLbl";
            this.ClassLbl.Size = new System.Drawing.Size(101, 24);
            this.ClassLbl.TabIndex = 209;
            this.ClassLbl.Text = "First Name";
            this.ClassLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // UserIdLbl
            // 
            this.UserIdLbl.AutoSize = true;
            this.UserIdLbl.BackColor = System.Drawing.Color.Transparent;
            this.UserIdLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.UserIdLbl.ForeColor = System.Drawing.Color.Tomato;
            this.UserIdLbl.Location = new System.Drawing.Point(122, 283);
            this.UserIdLbl.Margin = new System.Windows.Forms.Padding(0);
            this.UserIdLbl.Name = "UserIdLbl";
            this.UserIdLbl.Size = new System.Drawing.Size(101, 24);
            this.UserIdLbl.TabIndex = 208;
            this.UserIdLbl.Text = "First Name";
            this.UserIdLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // LastNameLbl
            // 
            this.LastNameLbl.AutoSize = true;
            this.LastNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.LastNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.LastNameLbl.ForeColor = System.Drawing.Color.Tomato;
            this.LastNameLbl.Location = new System.Drawing.Point(122, 245);
            this.LastNameLbl.Margin = new System.Windows.Forms.Padding(0);
            this.LastNameLbl.Name = "LastNameLbl";
            this.LastNameLbl.Size = new System.Drawing.Size(101, 24);
            this.LastNameLbl.TabIndex = 207;
            this.LastNameLbl.Text = "First Name";
            this.LastNameLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // FirstNameLbl
            // 
            this.FirstNameLbl.AutoSize = true;
            this.FirstNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.FirstNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.FirstNameLbl.ForeColor = System.Drawing.Color.Tomato;
            this.FirstNameLbl.Location = new System.Drawing.Point(122, 210);
            this.FirstNameLbl.Margin = new System.Windows.Forms.Padding(0);
            this.FirstNameLbl.Name = "FirstNameLbl";
            this.FirstNameLbl.Size = new System.Drawing.Size(101, 24);
            this.FirstNameLbl.TabIndex = 206;
            this.FirstNameLbl.Text = "First Name";
            this.FirstNameLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ClassYear_lbl
            // 
            this.ClassYear_lbl.AutoSize = true;
            this.ClassYear_lbl.BackColor = System.Drawing.Color.Transparent;
            this.ClassYear_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.ClassYear_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ClassYear_lbl.Location = new System.Drawing.Point(2, 347);
            this.ClassYear_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.ClassYear_lbl.Name = "ClassYear_lbl";
            this.ClassYear_lbl.Size = new System.Drawing.Size(99, 24);
            this.ClassYear_lbl.TabIndex = 202;
            this.ClassYear_lbl.Text = "Class Year";
            this.ClassYear_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Class_lbl
            // 
            this.Class_lbl.AutoSize = true;
            this.Class_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Class_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.Class_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Class_lbl.Location = new System.Drawing.Point(2, 314);
            this.Class_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Class_lbl.Name = "Class_lbl";
            this.Class_lbl.Size = new System.Drawing.Size(55, 24);
            this.Class_lbl.TabIndex = 201;
            this.Class_lbl.Text = "Class";
            this.Class_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(2, 210);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 24);
            this.label5.TabIndex = 198;
            this.label5.Text = "First Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(3, 282);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 24);
            this.label3.TabIndex = 200;
            this.label3.Text = "User ID";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(2, 245);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 24);
            this.label4.TabIndex = 199;
            this.label4.Text = "Last Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // HistoryPlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(965, 431);
            this.Controls.Add(this.ClassYearLbl);
            this.Controls.Add(this.ClassLbl);
            this.Controls.Add(this.UserIdLbl);
            this.Controls.Add(this.LastNameLbl);
            this.Controls.Add(this.FirstNameLbl);
            this.Controls.Add(this.ClassYear_lbl);
            this.Controls.Add(this.Class_lbl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Image_Photo);
            this.Controls.Add(this.HistoryTable);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PrintBtn);
            this.Controls.Add(this.CloseBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HistoryPlayerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "List_User_Form";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.DataGridView HistoryTable;
        private System.Windows.Forms.PictureBox Image_Photo;
        private System.Windows.Forms.Label ClassYearLbl;
        private System.Windows.Forms.Label ClassLbl;
        private System.Windows.Forms.Label UserIdLbl;
        private System.Windows.Forms.Label LastNameLbl;
        private System.Windows.Forms.Label FirstNameLbl;
        private System.Windows.Forms.Label ClassYear_lbl;
        private System.Windows.Forms.Label Class_lbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}