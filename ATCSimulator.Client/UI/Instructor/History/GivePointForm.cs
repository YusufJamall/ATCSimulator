﻿using System;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.Helper;

namespace ATCSimulator.Client.UI.Instructor.History
{
    public partial class GivePointForm : Form
    {
        string username;
        HistoryForm historyForm;
        SimulationListModel simulation;
        PlayerListModel player;
        int communicationPoint;
        int comprehensivePoint;
        int initiativePoint;
        public GivePointForm(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        public void Init(HistoryForm historyForm, SimulationListModel simulation, PlayerListModel player, Image picture)
        {
            this.historyForm = historyForm;
            this.simulation = simulation;
            this.player = player;
            this.communicationPoint = 0;
            this.comprehensivePoint = 0;
            this.initiativePoint = 0;
            Image_Photo.Image = picture;
            FullNameLbl.Text = player.fullname;
            DateSimulationLbl.Text = simulation.created_date.ToString();
            PositionLbl.Text = player.role_player;
            PilotAircraftLbl.Text = player.pilot;
            SaveBtn.Enabled = false;
        }

        public void SetPoint()
        {
            int totalPoint = (communicationPoint + comprehensivePoint + initiativePoint) / 3;
            PointText.Text = totalPoint.ToString();
            if(totalPoint>= 90)
                GradeText.Text = "Excellent";
            else if(totalPoint>= 70)
                GradeText.Text = "Good";
            else if(totalPoint>= 50)
                GradeText.Text = "Sufficient";
            else if(totalPoint>= 30)
                GradeText.Text = "Deficient";
            else
                GradeText.Text = "Poor";
            SaveBtn.Enabled = true;
        }

        private void CancelBtn_Click(object sender, System.EventArgs e)
        {
            this.historyForm.Enabled = true;
            this.Close();
        }

        private void radioButton_CheckedChanged(object sender, System.EventArgs e)
        {
            RadioButton btn = (RadioButton)sender;
            if (btn.Checked)
            {
                string[] data = btn.Name.Split('_');
                int point = 0;
                switch (data[1])
                {
                    case "poor":
                        point = 20;
                        break;
                    case "deficient":
                        point = 40;
                        break;
                    case "sufficient":
                        point = 60;
                        break;
                    case "good":
                        point = 80;
                        break;
                    case "excellent":
                        point = 100;
                        break;
                    default:
                        break;
                }
                switch (data[0])
                {
                    case "communication":
                        communicationPoint = point;
                        break;
                    case "comprehensive":
                        comprehensivePoint = point;
                        break;
                    case "initiative":
                        initiativePoint = point;
                        break;
                    default:
                        break;
                }
                SetPoint();
            }
        }

        private void SaveBtn_Click(object sender, System.EventArgs e)
        {
            StatusResponse result = SystemConnection.SetPoint(new PointContract { 
            instructorName = username,
             simulationID = simulation.id,
             playerID = player.id,
             point = Convert.ToInt32(PointText.Text),
             grade = GradeText.Text
            });
            if(result.status.code == 0)
            {
                MessageBox.Show("Point Saved");
                historyForm.SetPlayerList(simulation.id);
                historyForm.Enabled = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Failed to Save Point");
            }
        }
    }
}
