﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Configuration;
using Newtonsoft.Json;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.Helper;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;

namespace ATCSimulator.Client.UI.Instructor.History
{
    public partial class HistoryPlayerForm : Form
    {
        HistoryForm historyForm;
        UserForm userForm;
        UserProfileModel player;
        List<HistoryPlayerModel> historyPlayer;
        List<SimulationListModel> historyInstructor;
        public HistoryPlayerForm()
        {
            InitializeComponent();
        }
        public void Init(HistoryForm historyForm, UserForm userForm, UserProfileModel player)
        {
            if (historyForm != null)
                this.historyForm = historyForm;
            else
                this.userForm = userForm;
            this.player = player;
            this.historyPlayer = new List<HistoryPlayerModel>();
            this.historyInstructor = new List<SimulationListModel>();
            Image_Photo.Image = HelperClient.byteArrayToImage(player.photo);
            FirstNameLbl.Text = player.first_name;
            LastNameLbl.Text = player.last_name;
            UserIdLbl.Text = player.user_identity_id;
            if (player.role.role_name.ToLower().Equals("instructor"))
            {
                ClassLbl.Text = "";
                ClassYearLbl.Text = "";
                Class_lbl.Visible = false;
                ClassYear_lbl.Visible = false;

                List<SimulationListModel> list_simulation = new List<SimulationListModel>();
                HistoryInstructorResponse simulation_data = SystemConnection.GetHistoryInstructor(player.username);
                if (simulation_data.status.code == 0)
                {
                    list_simulation = simulation_data.simulations;
                    this.historyInstructor = simulation_data.simulations;
                }
                HistoryTable.DataSource = list_simulation;
                HistoryTable.Columns["id"].Visible = false;
                HistoryTable.Columns["scenery_id"].Visible = false;
                HistoryTable.Columns["exercise_name"].HeaderText = "Exercise Name";
                HistoryTable.Columns["scenery_name"].HeaderText = "Scenery";
                HistoryTable.Columns["play_time"].HeaderText = "Play Time";
                HistoryTable.Columns["elapsed_time"].Visible = false;
                HistoryTable.Columns["weather"].HeaderText = "Weather";
                HistoryTable.Columns["wind_direction"].Visible = false;
                HistoryTable.Columns["wind_speed"].Visible = false;
                HistoryTable.Columns["temperature"].Visible = false;
                HistoryTable.Columns["visibility"].Visible = false;
                HistoryTable.Columns["time_simulation"].HeaderText = "Time On Simulation";
                HistoryTable.Columns["runway_lights"].Visible = false;
                HistoryTable.Columns["taxi"].Visible = false;
                HistoryTable.Columns["papi"].Visible = false;
                HistoryTable.Columns["approach"].Visible = false;
                HistoryTable.Columns["total_arrival"].HeaderText = "Arrival";
                HistoryTable.Columns["total_departure"].HeaderText = "Departure";
                HistoryTable.Columns["created_date"].HeaderText = "Date of Simulation";
                HistoryTable.Columns["created_by"].Visible = false;

                HistoryTable.Columns["exercise_name"].DisplayIndex = 0;
                HistoryTable.Columns["created_date"].DisplayIndex = 1;
                HistoryTable.Columns["created_date"].Width = 80;
                HistoryTable.Columns["scenery_name"].DisplayIndex = 2;
                HistoryTable.Columns["play_time"].DisplayIndex = 3;
                HistoryTable.Columns["time_simulation"].DisplayIndex = 4;
                HistoryTable.Columns["total_arrival"].DisplayIndex = 5;
                HistoryTable.Columns["total_arrival"].Width = 50;
                HistoryTable.Columns["total_departure"].DisplayIndex = 6;
                HistoryTable.Columns["total_departure"].Width = 60;
                HistoryTable.Columns["weather"].DisplayIndex = 7;

            }
            else
            {
                ClassLbl.Text = player.student_info.@class;
                ClassYearLbl.Text = player.student_info.class_year.HasValue ? player.student_info.class_year.Value.ToString() : "";
                Class_lbl.Visible = true;
                ClassYear_lbl.Visible = true;

                List<DataHistoryPlayer> table = new List<DataHistoryPlayer>();
                HistoryPlayerResponse result = SystemConnection.GetHistoryPlayer(player.user_ID);
                if (result.status.code == 0)
                {
                    this.historyPlayer = result.history;
                    foreach (var data in result.history)
                    {
                        table.Add(new DataHistoryPlayer
                        {
                            player_ID = data.player.id,
                            simulationID = data.simulation.id,
                            exerciseName = data.simulation.exercise_name,
                            dateSimulation = data.simulation.created_date,
                            sceneryName = data.simulation.scenery_name,
                            rolePlayer = data.player.role_player,
                            pilot = data.player.pilot,
                            point = data.player.point,
                            grade = data.player.grade
                        });
                    }
                }
                HistoryTable.DataSource = table;
                HistoryTable.Columns["player_ID"].Visible = false;
                HistoryTable.Columns["simulationID"].Visible = false;
                HistoryTable.Columns["exerciseName"].HeaderText = "Exercise Name";
                HistoryTable.Columns["exerciseName"].DisplayIndex = 0;
                HistoryTable.Columns["dateSimulation"].HeaderText = "Date Simulation";
                HistoryTable.Columns["dateSimulation"].DisplayIndex = 1;
                HistoryTable.Columns["dateSimulation"].Width = 70;
                HistoryTable.Columns["sceneryName"].HeaderText = "Scenery";
                HistoryTable.Columns["sceneryName"].DisplayIndex = 2;
                HistoryTable.Columns["sceneryName"].Width = 100;
                HistoryTable.Columns["rolePlayer"].HeaderText = "Position";
                HistoryTable.Columns["rolePlayer"].DisplayIndex = 3;
                HistoryTable.Columns["pilot"].HeaderText = "Pilot on Aircraft";
                HistoryTable.Columns["pilot"].DisplayIndex = 4;
                HistoryTable.Columns["point"].HeaderText = "Point";
                HistoryTable.Columns["point"].DisplayIndex = 5;
                HistoryTable.Columns["point"].Width = 50;
                HistoryTable.Columns["grade"].HeaderText = "Grade";
                HistoryTable.Columns["grade"].DisplayIndex = 6;
                HistoryTable.Columns["grade"].Width = 100;
            }
        }

        private void CloseBtn_Click(object sender, System.EventArgs e)
        {
            if (historyForm != null)
                historyForm.Enabled = true;
            else
                userForm.Enabled = true;
            this.Close();
        }

        private void PrintBtn_Click(object sender, System.EventArgs e)
        {
            try
            {
                // Create a new MigraDoc document
                Document document = new Document();
                document.Info.Title = "Simulation Report";

                //DEFINED STYLE
                // Get the predefined style Normal.
                Style style = document.Styles["Normal"];

                // Because all styles are derived from Normal, the next line changes the
                // font of the whole document. Or, more exactly, it changes the font of
                // all styles and paragraphs that do not redefine the font.

                style.Font.Name = "Verdana";
                style = document.Styles[StyleNames.Header];
                style.ParagraphFormat.AddTabStop("16cm", MigraDoc.DocumentObjectModel.TabAlignment.Right);
                style = document.Styles[StyleNames.Footer];
                style.ParagraphFormat.AddTabStop("8cm", MigraDoc.DocumentObjectModel.TabAlignment.Center);

                // Create a new style called Table based on style Normal
                style = document.Styles.AddStyle("Table", "Normal");
                style.Font.Name = "Verdana";
                style.Font.Name = "Times New Roman";
                style.Font.Size = 9;

                // Create a new style called Reference based on style Normal
                style = document.Styles.AddStyle("Reference", "Normal");
                style.ParagraphFormat.SpaceBefore = "5mm";
                style.ParagraphFormat.SpaceAfter = "5mm";
                style.ParagraphFormat.TabStops.AddTabStop("16cm", MigraDoc.DocumentObjectModel.TabAlignment.Right);

                //CREATE PAGE
                // Each MigraDoc document needs at least one section.
                Section section = document.AddSection();

                // Create footer
                Paragraph paragraph = section.Footers.Primary.AddParagraph();
                paragraph.AddText(ConfigurationManager.AppSettings["FooterForm"].ToString());
                paragraph.Format.Font.Size = 9;
                paragraph.Format.Alignment = ParagraphAlignment.Center;

                // Create the text frame for the header
                TextFrame headerFrame = new TextFrame();
                headerFrame = section.Headers.Primary.AddTextFrame();
                headerFrame.Height = "10.0cm";
                headerFrame.Width = "20.0cm";
                headerFrame.Left = ShapePosition.Left;
                headerFrame.RelativeHorizontal = RelativeHorizontal.Margin;
                headerFrame.Top = "5.0cm";
                headerFrame.RelativeVertical = RelativeVertical.Page;

                // Put data in the header
                paragraph = headerFrame.AddParagraph("First Name");
                paragraph.AddTab();
                paragraph.AddTab();
                paragraph.AddText(": " + player.first_name);
                paragraph = headerFrame.AddParagraph("Last Name");
                paragraph.AddTab();
                paragraph.AddTab();
                paragraph.AddText(": " + player.last_name);
                paragraph = headerFrame.AddParagraph("User ID");
                paragraph.AddTab();
                paragraph.AddTab();
                paragraph.AddText(": " + player.user_identity_id);
                paragraph = headerFrame.AddParagraph("Role");
                paragraph.AddTab();
                paragraph.AddTab();
                paragraph.AddTab();
                paragraph.AddText(": " + player.role.role_name.ToUpper());
                if (player.isStudent)
                {
                    paragraph = headerFrame.AddParagraph("Class / Class Year");
                    paragraph.AddTab();
                    paragraph.AddText(": " + player.student_info.@class + " / " + player.student_info.class_year);
                }

                paragraph.Format.Font.Name = "Times New Roman";
                paragraph.Format.Font.Size = 10;
                paragraph.Format.SpaceAfter = 0;

                // Add the print date field
                paragraph = section.AddParagraph();
                paragraph.Format.SpaceBefore = "5.5cm";
                paragraph.Style = "Reference";
                paragraph.AddFormattedText("History Simulation", new MigraDoc.DocumentObjectModel.Font { Bold = true, Underline = Underline.Single });


                //// Create Table
                if (player.role.role_name.ToLower().Equals("instructor"))
                    CreateTableInstrucor(section);
                else
                    CreateTablePlayer(section);

                // Create instance of PDFform class
                document.UseCmykColor = true;
                // Create a renderer for PDF that uses Unicode font encoding
                MigraDoc.Rendering.PdfDocumentRenderer pdfRenderer = new MigraDoc.Rendering.PdfDocumentRenderer(true);
                // Set the MigraDoc document
                pdfRenderer.Document = document;
                // Create the PDF document
                pdfRenderer.RenderDocument();
                // Save the PDF document...
                string filename = "Report History.pdf";
                pdfRenderer.Save(filename); ;

                ProcessStartInfo info = new ProcessStartInfo();
                info.Verb = "print";
                info.FileName = filename;
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;

                Process p = new Process();
                p.StartInfo = info;
                p.Start();
            }catch
            {
                MessageBox.Show("Print Failed, Please Try Again");
            }
        }

        public void CreateTableInstrucor(Section section)
        {
            // Create the item table
            Table table = section.AddTable();
            table.Style = "Table";
            table.Borders.Color = new MigraDoc.DocumentObjectModel.Color(0, 0, 0);
            table.Borders.Width = 0.25;
            table.Borders.Left.Width = 0.5;
            table.Borders.Right.Width = 0.5;
            table.Rows.LeftIndent = 0;
            // Before you can add a row, you must define the columns
            Column column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("1.75cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn("1.75cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Format.Font.Size = 8;
            row.Shading.Color = new MigraDoc.DocumentObjectModel.Color(204, 204, 204);
            row.Cells[0].AddParagraph("Exercise Name");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Date of Simulation");
            row.Cells[1].Format.Font.Bold = true;
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Scenery Name");
            row.Cells[2].Format.Font.Bold = true;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Play Time");
            row.Cells[3].Format.Font.Bold = true;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Time Simulation");
            row.Cells[4].Format.Font.Bold = true;
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Arrival");
            row.Cells[5].Format.Font.Bold = true;
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[6].AddParagraph("Departure");
            row.Cells[6].Format.Font.Bold = true;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[7].AddParagraph("Weather");
            row.Cells[7].Format.Font.Bold = true;
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
            table.SetEdge(0, 0, 8, 1, Edge.Box, MigraDoc.DocumentObjectModel.BorderStyle.Single, 0.75, MigraDoc.DocumentObjectModel.Color.Empty);

            // FILL Content
            foreach(var item in historyInstructor)
            {
                // Create the header of the table
                Row content = table.AddRow();
                content.HeadingFormat = false;
                content.Format.Alignment = ParagraphAlignment.Center;
                content.Format.Font.Bold = false;
                content.Format.Font.Size = 10;
                content.Shading.Color = new MigraDoc.DocumentObjectModel.Color(255, 255, 255);
                content.Cells[0].AddParagraph(item.exercise_name);
                content.Cells[0].Format.Font.Bold = false;
                content.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[1].AddParagraph(item.created_date.ToString());
                content.Cells[1].Format.Font.Bold = false;
                content.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[2].AddParagraph(item.scenery_name);
                content.Cells[2].Format.Font.Bold = false;
                content.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[3].AddParagraph(item.play_time.ToString(@"hh\:mm\:ss"));
                content.Cells[3].Format.Font.Bold = false;
                content.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[4].AddParagraph(item.time_simulation.ToString(@"hh\:mm\:ss"));
                content.Cells[4].Format.Font.Bold = false;
                content.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[5].AddParagraph(item.total_arrival.ToString());
                content.Cells[5].Format.Font.Bold = false;
                content.Cells[5].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[6].AddParagraph(item.total_departure.ToString());
                content.Cells[6].Format.Font.Bold = false;
                content.Cells[6].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[7].AddParagraph(item.weather.ToString());
                content.Cells[7].Format.Font.Bold = false;
                content.Cells[7].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[7].VerticalAlignment = VerticalAlignment.Center;
            }
            
        }

        public void CreateTablePlayer(Section section)
        {
            // Create the item table
            Table table = section.AddTable();
            table.Style = "Table";
            table.Borders.Color = new MigraDoc.DocumentObjectModel.Color(0, 0, 0);
            table.Borders.Width = 0.25;
            table.Borders.Left.Width = 0.5;
            table.Borders.Right.Width = 0.5;
            table.Rows.LeftIndent = 0;
            // Before you can add a row, you must define the columns
            Column column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("1.75cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn("1.75cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Format.Font.Size = 8;
            row.Shading.Color = new MigraDoc.DocumentObjectModel.Color(204, 204, 204);
            row.Cells[0].AddParagraph("Exercise Name");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Date of Simulation");
            row.Cells[1].Format.Font.Bold = true;
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Scenery Name");
            row.Cells[2].Format.Font.Bold = true;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Position");
            row.Cells[3].Format.Font.Bold = true;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Pilot on Aircraft");
            row.Cells[4].Format.Font.Bold = true;
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Point");
            row.Cells[5].Format.Font.Bold = true;
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[6].AddParagraph("Grade");
            row.Cells[6].Format.Font.Bold = true;
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[7].AddParagraph("Given Point By");
            row.Cells[7].Format.Font.Bold = true;
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
            table.SetEdge(0, 0, 8, 1, Edge.Box, MigraDoc.DocumentObjectModel.BorderStyle.Single, 0.75, MigraDoc.DocumentObjectModel.Color.Empty);

            // FILL Content
            foreach (var item in historyPlayer)
            {
                // Create the header of the table
                Row content = table.AddRow();
                content.HeadingFormat = false;
                content.Format.Alignment = ParagraphAlignment.Center;
                content.Format.Font.Bold = false;
                content.Format.Font.Size = 10;
                content.Shading.Color = new MigraDoc.DocumentObjectModel.Color(255, 255, 255);
                content.Cells[0].AddParagraph(item.simulation.exercise_name);
                content.Cells[0].Format.Font.Bold = false;
                content.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[1].AddParagraph(item.simulation.created_date.ToString());
                content.Cells[1].Format.Font.Bold = false;
                content.Cells[1].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[2].AddParagraph(item.simulation.scenery_name);
                content.Cells[2].Format.Font.Bold = false;
                content.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[3].AddParagraph(item.player.role_player);
                content.Cells[3].Format.Font.Bold = false;
                content.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[4].AddParagraph(item.player.pilot);
                content.Cells[4].Format.Font.Bold = false;
                content.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[5].AddParagraph(item.player.point);
                content.Cells[5].Format.Font.Bold = false;
                content.Cells[5].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[6].AddParagraph(item.player.grade);
                content.Cells[6].Format.Font.Bold = false;
                content.Cells[6].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                content.Cells[7].AddParagraph(item.player.edited_by);
                content.Cells[7].Format.Font.Bold = false;
                content.Cells[7].Format.Alignment = ParagraphAlignment.Center;
                content.Cells[7].VerticalAlignment = VerticalAlignment.Center;
            }
        }
    }
}
