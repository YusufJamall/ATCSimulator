﻿namespace ATCSimulator.Client.UI.Instructor.History
{
    partial class GivePointForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.PilotAircraftLbl = new System.Windows.Forms.Label();
            this.PositionLbl = new System.Windows.Forms.Label();
            this.DateSimulationLbl = new System.Windows.Forms.Label();
            this.FullNameLbl = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Image_Photo = new System.Windows.Forms.PictureBox();
            this.Class_lbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.communication_poor = new System.Windows.Forms.RadioButton();
            this.CommunicationPanel = new System.Windows.Forms.Panel();
            this.communication_excellent = new System.Windows.Forms.RadioButton();
            this.communication_good = new System.Windows.Forms.RadioButton();
            this.communication_sufficient = new System.Windows.Forms.RadioButton();
            this.communication_deficient = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comprehensive_excellent = new System.Windows.Forms.RadioButton();
            this.comprehensive_good = new System.Windows.Forms.RadioButton();
            this.comprehensive_sufficient = new System.Windows.Forms.RadioButton();
            this.comprehensive_deficient = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.comprehensive_poor = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.initiative_excellent = new System.Windows.Forms.RadioButton();
            this.initiative_good = new System.Windows.Forms.RadioButton();
            this.initiative_sufficient = new System.Windows.Forms.RadioButton();
            this.initiative_deficient = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.initiative_poor = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.PointText = new Telerik.WinControls.UI.RadTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.GradeText = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.CommunicationPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PointText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GradeText)).BeginInit();
            this.SuspendLayout();
            // 
            // CancelBtn
            // 
            this.CancelBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.CancelBtn.FlatAppearance.BorderSize = 0;
            this.CancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.CancelBtn.ForeColor = System.Drawing.Color.White;
            this.CancelBtn.Location = new System.Drawing.Point(478, 433);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(177, 34);
            this.CancelBtn.TabIndex = 91;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = false;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.SaveBtn.FlatAppearance.BorderSize = 0;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.SaveBtn.ForeColor = System.Drawing.Color.White;
            this.SaveBtn.Location = new System.Drawing.Point(295, 433);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(177, 34);
            this.SaveBtn.TabIndex = 92;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // PilotAircraftLbl
            // 
            this.PilotAircraftLbl.BackColor = System.Drawing.Color.Transparent;
            this.PilotAircraftLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.PilotAircraftLbl.ForeColor = System.Drawing.Color.Tomato;
            this.PilotAircraftLbl.Location = new System.Drawing.Point(353, 116);
            this.PilotAircraftLbl.Margin = new System.Windows.Forms.Padding(0);
            this.PilotAircraftLbl.Name = "PilotAircraftLbl";
            this.PilotAircraftLbl.Size = new System.Drawing.Size(293, 47);
            this.PilotAircraftLbl.TabIndex = 212;
            this.PilotAircraftLbl.Text = "First Name";
            // 
            // PositionLbl
            // 
            this.PositionLbl.AutoSize = true;
            this.PositionLbl.BackColor = System.Drawing.Color.Transparent;
            this.PositionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.PositionLbl.ForeColor = System.Drawing.Color.Tomato;
            this.PositionLbl.Location = new System.Drawing.Point(353, 85);
            this.PositionLbl.Margin = new System.Windows.Forms.Padding(0);
            this.PositionLbl.Name = "PositionLbl";
            this.PositionLbl.Size = new System.Drawing.Size(101, 24);
            this.PositionLbl.TabIndex = 211;
            this.PositionLbl.Text = "First Name";
            this.PositionLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // DateSimulationLbl
            // 
            this.DateSimulationLbl.AutoSize = true;
            this.DateSimulationLbl.BackColor = System.Drawing.Color.Transparent;
            this.DateSimulationLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.DateSimulationLbl.ForeColor = System.Drawing.Color.Tomato;
            this.DateSimulationLbl.Location = new System.Drawing.Point(353, 48);
            this.DateSimulationLbl.Margin = new System.Windows.Forms.Padding(0);
            this.DateSimulationLbl.Name = "DateSimulationLbl";
            this.DateSimulationLbl.Size = new System.Drawing.Size(101, 24);
            this.DateSimulationLbl.TabIndex = 210;
            this.DateSimulationLbl.Text = "First Name";
            this.DateSimulationLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // FullNameLbl
            // 
            this.FullNameLbl.AutoSize = true;
            this.FullNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.FullNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.FullNameLbl.ForeColor = System.Drawing.Color.Tomato;
            this.FullNameLbl.Location = new System.Drawing.Point(353, 12);
            this.FullNameLbl.Margin = new System.Windows.Forms.Padding(0);
            this.FullNameLbl.Name = "FullNameLbl";
            this.FullNameLbl.Size = new System.Drawing.Size(101, 24);
            this.FullNameLbl.TabIndex = 209;
            this.FullNameLbl.Text = "First Name";
            this.FullNameLbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label11.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label11.Location = new System.Drawing.Point(338, 116);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 24);
            this.label11.TabIndex = 207;
            this.label11.Text = ":";
            this.label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label10.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label10.Location = new System.Drawing.Point(340, 84);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 24);
            this.label10.TabIndex = 206;
            this.label10.Text = ":";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label8.Location = new System.Drawing.Point(338, 47);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 24);
            this.label8.TabIndex = 205;
            this.label8.Text = ":";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(340, 12);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 24);
            this.label6.TabIndex = 204;
            this.label6.Text = ":";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Image_Photo
            // 
            this.Image_Photo.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            this.Image_Photo.Location = new System.Drawing.Point(12, 12);
            this.Image_Photo.Name = "Image_Photo";
            this.Image_Photo.Size = new System.Drawing.Size(150, 148);
            this.Image_Photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Image_Photo.TabIndex = 198;
            this.Image_Photo.TabStop = false;
            // 
            // Class_lbl
            // 
            this.Class_lbl.AutoSize = true;
            this.Class_lbl.BackColor = System.Drawing.Color.Transparent;
            this.Class_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.Class_lbl.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Class_lbl.Location = new System.Drawing.Point(176, 116);
            this.Class_lbl.Margin = new System.Windows.Forms.Padding(0);
            this.Class_lbl.Name = "Class_lbl";
            this.Class_lbl.Size = new System.Drawing.Size(134, 24);
            this.Class_lbl.TabIndex = 202;
            this.Class_lbl.Text = "Pilot on Aircraft";
            this.Class_lbl.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(176, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 24);
            this.label5.TabIndex = 199;
            this.label5.Text = "Full Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(177, 84);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 24);
            this.label3.TabIndex = 201;
            this.label3.Text = "Posisition";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(176, 47);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 24);
            this.label4.TabIndex = 200;
            this.label4.Text = "Date Simulation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ATCSimulator.Client.Properties.Resources.LineBlue;
            this.pictureBox1.Location = new System.Drawing.Point(11, 166);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(635, 3);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 213;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(340, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 24);
            this.label1.TabIndex = 214;
            this.label1.Text = ":";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(5, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 24);
            this.label2.TabIndex = 215;
            this.label2.Text = "Communication Skill";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // communication_poor
            // 
            this.communication_poor.AutoSize = true;
            this.communication_poor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.communication_poor.ForeColor = System.Drawing.Color.DodgerBlue;
            this.communication_poor.Location = new System.Drawing.Point(40, 33);
            this.communication_poor.Name = "communication_poor";
            this.communication_poor.Size = new System.Drawing.Size(68, 28);
            this.communication_poor.TabIndex = 216;
            this.communication_poor.TabStop = true;
            this.communication_poor.Text = "Poor";
            this.communication_poor.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.communication_poor.UseVisualStyleBackColor = true;
            this.communication_poor.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // CommunicationPanel
            // 
            this.CommunicationPanel.Controls.Add(this.communication_excellent);
            this.CommunicationPanel.Controls.Add(this.communication_good);
            this.CommunicationPanel.Controls.Add(this.communication_sufficient);
            this.CommunicationPanel.Controls.Add(this.communication_deficient);
            this.CommunicationPanel.Controls.Add(this.label2);
            this.CommunicationPanel.Controls.Add(this.communication_poor);
            this.CommunicationPanel.Location = new System.Drawing.Point(13, 172);
            this.CommunicationPanel.Name = "CommunicationPanel";
            this.CommunicationPanel.Size = new System.Drawing.Size(634, 66);
            this.CommunicationPanel.TabIndex = 217;
            // 
            // communication_excellent
            // 
            this.communication_excellent.AutoSize = true;
            this.communication_excellent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.communication_excellent.ForeColor = System.Drawing.Color.DodgerBlue;
            this.communication_excellent.Location = new System.Drawing.Point(481, 33);
            this.communication_excellent.Name = "communication_excellent";
            this.communication_excellent.Size = new System.Drawing.Size(106, 28);
            this.communication_excellent.TabIndex = 220;
            this.communication_excellent.TabStop = true;
            this.communication_excellent.Text = "Excellent";
            this.communication_excellent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.communication_excellent.UseVisualStyleBackColor = true;
            this.communication_excellent.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // communication_good
            // 
            this.communication_good.AutoSize = true;
            this.communication_good.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.communication_good.ForeColor = System.Drawing.Color.DodgerBlue;
            this.communication_good.Location = new System.Drawing.Point(381, 33);
            this.communication_good.Name = "communication_good";
            this.communication_good.Size = new System.Drawing.Size(75, 28);
            this.communication_good.TabIndex = 219;
            this.communication_good.TabStop = true;
            this.communication_good.Text = "Good";
            this.communication_good.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.communication_good.UseVisualStyleBackColor = true;
            this.communication_good.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // communication_sufficient
            // 
            this.communication_sufficient.AutoSize = true;
            this.communication_sufficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.communication_sufficient.ForeColor = System.Drawing.Color.DodgerBlue;
            this.communication_sufficient.Location = new System.Drawing.Point(255, 33);
            this.communication_sufficient.Name = "communication_sufficient";
            this.communication_sufficient.Size = new System.Drawing.Size(103, 28);
            this.communication_sufficient.TabIndex = 218;
            this.communication_sufficient.TabStop = true;
            this.communication_sufficient.Text = "Sufficient";
            this.communication_sufficient.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.communication_sufficient.UseVisualStyleBackColor = true;
            this.communication_sufficient.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // communication_deficient
            // 
            this.communication_deficient.AutoSize = true;
            this.communication_deficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.communication_deficient.ForeColor = System.Drawing.Color.DodgerBlue;
            this.communication_deficient.Location = new System.Drawing.Point(129, 33);
            this.communication_deficient.Name = "communication_deficient";
            this.communication_deficient.Size = new System.Drawing.Size(100, 28);
            this.communication_deficient.TabIndex = 217;
            this.communication_deficient.TabStop = true;
            this.communication_deficient.Text = "Deficient";
            this.communication_deficient.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.communication_deficient.UseVisualStyleBackColor = true;
            this.communication_deficient.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comprehensive_excellent);
            this.panel1.Controls.Add(this.comprehensive_good);
            this.panel1.Controls.Add(this.comprehensive_sufficient);
            this.panel1.Controls.Add(this.comprehensive_deficient);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.comprehensive_poor);
            this.panel1.Location = new System.Drawing.Point(14, 244);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(634, 66);
            this.panel1.TabIndex = 221;
            // 
            // comprehensive_excellent
            // 
            this.comprehensive_excellent.AutoSize = true;
            this.comprehensive_excellent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprehensive_excellent.ForeColor = System.Drawing.Color.DodgerBlue;
            this.comprehensive_excellent.Location = new System.Drawing.Point(481, 33);
            this.comprehensive_excellent.Name = "comprehensive_excellent";
            this.comprehensive_excellent.Size = new System.Drawing.Size(106, 28);
            this.comprehensive_excellent.TabIndex = 220;
            this.comprehensive_excellent.TabStop = true;
            this.comprehensive_excellent.Text = "Excellent";
            this.comprehensive_excellent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.comprehensive_excellent.UseVisualStyleBackColor = true;
            this.comprehensive_excellent.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // comprehensive_good
            // 
            this.comprehensive_good.AutoSize = true;
            this.comprehensive_good.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprehensive_good.ForeColor = System.Drawing.Color.DodgerBlue;
            this.comprehensive_good.Location = new System.Drawing.Point(381, 33);
            this.comprehensive_good.Name = "comprehensive_good";
            this.comprehensive_good.Size = new System.Drawing.Size(75, 28);
            this.comprehensive_good.TabIndex = 219;
            this.comprehensive_good.TabStop = true;
            this.comprehensive_good.Text = "Good";
            this.comprehensive_good.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.comprehensive_good.UseVisualStyleBackColor = true;
            this.comprehensive_good.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // comprehensive_sufficient
            // 
            this.comprehensive_sufficient.AutoSize = true;
            this.comprehensive_sufficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprehensive_sufficient.ForeColor = System.Drawing.Color.DodgerBlue;
            this.comprehensive_sufficient.Location = new System.Drawing.Point(255, 33);
            this.comprehensive_sufficient.Name = "comprehensive_sufficient";
            this.comprehensive_sufficient.Size = new System.Drawing.Size(103, 28);
            this.comprehensive_sufficient.TabIndex = 218;
            this.comprehensive_sufficient.TabStop = true;
            this.comprehensive_sufficient.Text = "Sufficient";
            this.comprehensive_sufficient.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.comprehensive_sufficient.UseVisualStyleBackColor = true;
            this.comprehensive_sufficient.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // comprehensive_deficient
            // 
            this.comprehensive_deficient.AutoSize = true;
            this.comprehensive_deficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprehensive_deficient.ForeColor = System.Drawing.Color.DodgerBlue;
            this.comprehensive_deficient.Location = new System.Drawing.Point(129, 33);
            this.comprehensive_deficient.Name = "comprehensive_deficient";
            this.comprehensive_deficient.Size = new System.Drawing.Size(100, 28);
            this.comprehensive_deficient.TabIndex = 217;
            this.comprehensive_deficient.TabStop = true;
            this.comprehensive_deficient.Text = "Deficient";
            this.comprehensive_deficient.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.comprehensive_deficient.UseVisualStyleBackColor = true;
            this.comprehensive_deficient.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(5, 5);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(182, 24);
            this.label7.TabIndex = 215;
            this.label7.Text = "Comprehensive Skill";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // comprehensive_poor
            // 
            this.comprehensive_poor.AutoSize = true;
            this.comprehensive_poor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprehensive_poor.ForeColor = System.Drawing.Color.DodgerBlue;
            this.comprehensive_poor.Location = new System.Drawing.Point(40, 33);
            this.comprehensive_poor.Name = "comprehensive_poor";
            this.comprehensive_poor.Size = new System.Drawing.Size(68, 28);
            this.comprehensive_poor.TabIndex = 216;
            this.comprehensive_poor.TabStop = true;
            this.comprehensive_poor.Text = "Poor";
            this.comprehensive_poor.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.comprehensive_poor.UseVisualStyleBackColor = true;
            this.comprehensive_poor.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.initiative_excellent);
            this.panel2.Controls.Add(this.initiative_good);
            this.panel2.Controls.Add(this.initiative_sufficient);
            this.panel2.Controls.Add(this.initiative_deficient);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.initiative_poor);
            this.panel2.Location = new System.Drawing.Point(14, 316);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(634, 66);
            this.panel2.TabIndex = 221;
            // 
            // initiative_excellent
            // 
            this.initiative_excellent.AutoSize = true;
            this.initiative_excellent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initiative_excellent.ForeColor = System.Drawing.Color.DodgerBlue;
            this.initiative_excellent.Location = new System.Drawing.Point(481, 33);
            this.initiative_excellent.Name = "initiative_excellent";
            this.initiative_excellent.Size = new System.Drawing.Size(106, 28);
            this.initiative_excellent.TabIndex = 220;
            this.initiative_excellent.TabStop = true;
            this.initiative_excellent.Text = "Excellent";
            this.initiative_excellent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.initiative_excellent.UseVisualStyleBackColor = true;
            this.initiative_excellent.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // initiative_good
            // 
            this.initiative_good.AutoSize = true;
            this.initiative_good.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initiative_good.ForeColor = System.Drawing.Color.DodgerBlue;
            this.initiative_good.Location = new System.Drawing.Point(381, 33);
            this.initiative_good.Name = "initiative_good";
            this.initiative_good.Size = new System.Drawing.Size(75, 28);
            this.initiative_good.TabIndex = 219;
            this.initiative_good.TabStop = true;
            this.initiative_good.Text = "Good";
            this.initiative_good.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.initiative_good.UseVisualStyleBackColor = true;
            this.initiative_good.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // initiative_sufficient
            // 
            this.initiative_sufficient.AutoSize = true;
            this.initiative_sufficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initiative_sufficient.ForeColor = System.Drawing.Color.DodgerBlue;
            this.initiative_sufficient.Location = new System.Drawing.Point(255, 33);
            this.initiative_sufficient.Name = "initiative_sufficient";
            this.initiative_sufficient.Size = new System.Drawing.Size(103, 28);
            this.initiative_sufficient.TabIndex = 218;
            this.initiative_sufficient.TabStop = true;
            this.initiative_sufficient.Text = "Sufficient";
            this.initiative_sufficient.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.initiative_sufficient.UseVisualStyleBackColor = true;
            this.initiative_sufficient.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // initiative_deficient
            // 
            this.initiative_deficient.AutoSize = true;
            this.initiative_deficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initiative_deficient.ForeColor = System.Drawing.Color.DodgerBlue;
            this.initiative_deficient.Location = new System.Drawing.Point(129, 33);
            this.initiative_deficient.Name = "initiative_deficient";
            this.initiative_deficient.Size = new System.Drawing.Size(100, 28);
            this.initiative_deficient.TabIndex = 217;
            this.initiative_deficient.TabStop = true;
            this.initiative_deficient.Text = "Deficient";
            this.initiative_deficient.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.initiative_deficient.UseVisualStyleBackColor = true;
            this.initiative_deficient.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label9.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label9.Location = new System.Drawing.Point(5, 5);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 24);
            this.label9.TabIndex = 215;
            this.label9.Text = "Initiative Skill";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // initiative_poor
            // 
            this.initiative_poor.AutoSize = true;
            this.initiative_poor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initiative_poor.ForeColor = System.Drawing.Color.DodgerBlue;
            this.initiative_poor.Location = new System.Drawing.Point(40, 33);
            this.initiative_poor.Name = "initiative_poor";
            this.initiative_poor.Size = new System.Drawing.Size(68, 28);
            this.initiative_poor.TabIndex = 216;
            this.initiative_poor.TabStop = true;
            this.initiative_poor.Text = "Poor";
            this.initiative_poor.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.initiative_poor.UseVisualStyleBackColor = true;
            this.initiative_poor.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label12.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label12.Location = new System.Drawing.Point(18, 391);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 24);
            this.label12.TabIndex = 221;
            this.label12.Text = "Result :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label13.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label13.Location = new System.Drawing.Point(91, 391);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 24);
            this.label13.TabIndex = 222;
            this.label13.Text = "Point";
            this.label13.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // PointText
            // 
            this.PointText.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PointText.Enabled = false;
            this.PointText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.PointText.Location = new System.Drawing.Point(149, 388);
            this.PointText.Name = "PointText";
            // 
            // 
            // 
            this.PointText.RootElement.AccessibleDescription = null;
            this.PointText.RootElement.AccessibleName = null;
            this.PointText.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.PointText.RootElement.Enabled = false;
            this.PointText.RootElement.StretchVertically = true;
            this.PointText.Size = new System.Drawing.Size(59, 28);
            this.PointText.TabIndex = 223;
            this.PointText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label14.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label14.Location = new System.Drawing.Point(211, 391);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 24);
            this.label14.TabIndex = 224;
            this.label14.Text = "Grade";
            this.label14.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // GradeText
            // 
            this.GradeText.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.GradeText.Enabled = false;
            this.GradeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.GradeText.Location = new System.Drawing.Point(285, 388);
            this.GradeText.Name = "GradeText";
            // 
            // 
            // 
            this.GradeText.RootElement.AccessibleDescription = null;
            this.GradeText.RootElement.AccessibleName = null;
            this.GradeText.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.GradeText.RootElement.Enabled = false;
            this.GradeText.RootElement.StretchVertically = true;
            this.GradeText.Size = new System.Drawing.Size(362, 28);
            this.GradeText.TabIndex = 224;
            this.GradeText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GivePointForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(660, 472);
            this.Controls.Add(this.GradeText);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.PointText);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CommunicationPanel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PilotAircraftLbl);
            this.Controls.Add(this.PositionLbl);
            this.Controls.Add(this.DateSimulationLbl);
            this.Controls.Add(this.FullNameLbl);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Image_Photo);
            this.Controls.Add(this.Class_lbl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.CancelBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GivePointForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "List_User_Form";
            ((System.ComponentModel.ISupportInitialize)(this.Image_Photo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.CommunicationPanel.ResumeLayout(false);
            this.CommunicationPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PointText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GradeText)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label PilotAircraftLbl;
        private System.Windows.Forms.Label PositionLbl;
        private System.Windows.Forms.Label DateSimulationLbl;
        private System.Windows.Forms.Label FullNameLbl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox Image_Photo;
        private System.Windows.Forms.Label Class_lbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton communication_poor;
        private System.Windows.Forms.Panel CommunicationPanel;
        private System.Windows.Forms.RadioButton communication_excellent;
        private System.Windows.Forms.RadioButton communication_good;
        private System.Windows.Forms.RadioButton communication_sufficient;
        private System.Windows.Forms.RadioButton communication_deficient;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton comprehensive_excellent;
        private System.Windows.Forms.RadioButton comprehensive_good;
        private System.Windows.Forms.RadioButton comprehensive_sufficient;
        private System.Windows.Forms.RadioButton comprehensive_deficient;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton comprehensive_poor;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton initiative_excellent;
        private System.Windows.Forms.RadioButton initiative_good;
        private System.Windows.Forms.RadioButton initiative_sufficient;
        private System.Windows.Forms.RadioButton initiative_deficient;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton initiative_poor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private Telerik.WinControls.UI.RadTextBox PointText;
        private System.Windows.Forms.Label label14;
        private Telerik.WinControls.UI.RadTextBox GradeText;
    }
}