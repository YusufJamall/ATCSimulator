﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;
using Newtonsoft.Json;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.Helper;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;

namespace ATCSimulator.Client.UI.Instructor.History
{
    public partial class HistoryForm : Form
    {
        string username;
        SimulationListModel _simulation;
        List<PlayerListModel> player_list;
        PlayerListModel _player;
        UserProfileModel player;
        MainForm mainform;
        int pageSize;
        public HistoryForm(string username,MainForm mainform)
        {
            InitializeComponent();
            this.username = username;
            this.mainform = mainform;
            this.pageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["History_Page_Size"].ToString());
        }

        public void Init()
        {
            SetSimulationList(1);
        }

        #region Function And Procedure

        public void SetSimulationList(int page)
        {
            ResetForm();
            SimulationListResponse simulation_data = SystemConnection.SimulationList(page, pageSize);
            if (simulation_data.status.code == 0)
            {
                List<SimulationListModel> list_simulation = simulation_data.simulations;
                SimulationTable.DataSource = list_simulation;
                SimulationTable.Columns["id"].Visible = false;
                SimulationTable.Columns["scenery_id"].Visible = false;
                SimulationTable.Columns["scenery_name"].HeaderText = "Scenery";
                SimulationTable.Columns["exercise_name"].HeaderText = "Exercise Name";
                SimulationTable.Columns["play_time"].HeaderText = "Play Time";
                SimulationTable.Columns["play_time"].DefaultCellStyle.Format = @"hh\:mm\:ss";
                SimulationTable.Columns["elapsed_time"].HeaderText = "Elapsed Time";
                SimulationTable.Columns["elapsed_time"].DefaultCellStyle.Format = @"hh\:mm\:ss";
                SimulationTable.Columns["weather"].HeaderText = "Weather";
                SimulationTable.Columns["wind_direction"].Visible = false;
                SimulationTable.Columns["wind_speed"].Visible = false;
                SimulationTable.Columns["temperature"].Visible = false;
                SimulationTable.Columns["visibility"].Visible = false;
                SimulationTable.Columns["time_simulation"].HeaderText = "Time On Simulation";
                SimulationTable.Columns["runway_lights"].Visible = false;
                SimulationTable.Columns["taxi"].Visible = false;
                SimulationTable.Columns["papi"].Visible = false;
                SimulationTable.Columns["approach"].Visible = false;
                SimulationTable.Columns["total_arrival"].HeaderText = "Arrival";
                SimulationTable.Columns["total_departure"].HeaderText = "Departure";
                SimulationTable.Columns["created_date"].HeaderText = "Date of Simulation";
                SimulationTable.Columns["created_by"].HeaderText = "Instructor";

                SimulationTable.Columns["exercise_name"].DisplayIndex = 0;
                SimulationTable.Columns["created_date"].DisplayIndex = 1;
                SimulationTable.Columns["created_date"].Width = 80;
                SimulationTable.Columns["scenery_name"].DisplayIndex = 2;
                SimulationTable.Columns["play_time"].DisplayIndex = 3;
                SimulationTable.Columns["elapsed_time"].DisplayIndex = 4;
                SimulationTable.Columns["time_simulation"].DisplayIndex = 5;
                SimulationTable.Columns["total_arrival"].DisplayIndex = 6;
                SimulationTable.Columns["total_arrival"].Width = 50;
                SimulationTable.Columns["total_departure"].DisplayIndex = 7;
                SimulationTable.Columns["total_departure"].Width = 60;
                SimulationTable.Columns["weather"].DisplayIndex = 8;


                // Paging Logic
                int total_page = simulation_data.count % pageSize > 0 ? (simulation_data.count / pageSize) + 1 : simulation_data.count / pageSize;
                int first = total_page < 1 ? 0 : page == 1 ? 1 : ((page - 1) * pageSize) + 1;
                int last = first < 1 ? 0 : first + (simulation_data.simulations.Count - 1);


                GoToPage_Box.Items.Clear();
                GoToPage_Box.SelectedIndex = -1;
                for (int i = 1; i <= total_page; i++)
                {
                    GoToPage_Box.Items.Add(new Telerik.WinControls.UI.RadListDataItem(i.ToString(), i));
                }


                Page_text.Text = page.ToString();
                Total_Page_lbl.Text = "Displaying  " + first + " - " + last + " from " + simulation_data.count + " result";

                // control page button enable/ disable
                // remove page if result is 0
                page = total_page < 1 ? 0 : page;
                if (page == total_page)
                {
                    if (page == 1)
                    {
                        Next_Btn.Enabled = false;
                        Prev_Btn.Enabled = false;
                    }
                    else if (total_page < 1 || page < 1)
                    {
                        Next_Btn.Enabled = false;
                        Prev_Btn.Enabled = false;
                    }
                    else
                    {
                        Next_Btn.Enabled = false;
                        Prev_Btn.Enabled = true;
                    }
                }
                else if (page == 1)
                {
                    Next_Btn.Enabled = true;
                    Prev_Btn.Enabled = false;
                }
                else
                {
                    Next_Btn.Enabled = true;
                    Prev_Btn.Enabled = true;
                }
            }
        }

        #endregion

        #region Button Handler

        private void Next_Btn_MouseHover(object sender, EventArgs e)
        {
            this.Next_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_next_hover;
        }

        private void Next_Btn_MouseLeave(object sender, EventArgs e)
        {
            this.Next_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_next;
        }

        private void Prev_Btn_MouseHover(object sender, EventArgs e)
        {
            this.Prev_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_prev_hover;
        }

        private void Prev_Btn_MouseLeave(object sender, EventArgs e)
        {
            this.Prev_Btn.Image = global::ATCSimulator.Client.Properties.Resources.img_prev;
        }

        private void GoToPage_Box_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (GoToPage_Box.SelectedIndex >= 0)
                SetSimulationList(Convert.ToInt32(GoToPage_Box.SelectedValue.ToString()));
        }

        private void Next_Btn_Click(object sender, EventArgs e)
        {
            SetSimulationList(Convert.ToInt32(Page_text.Text) + 1);
        }

        private void Prev_Btn_Click(object sender, EventArgs e)
        {
            SetSimulationList(Convert.ToInt32(Page_text.Text) - 1);
        }

        #endregion

        #region Helper

        public void ResetForm()
        {
            SimulationTable.Columns.Clear();
            PlayerTable.Columns.Clear();
            DetailPanel.Visible = false;
            PrintBtn.Enabled = false;
            RemoveHistoryBtn.Enabled = false;
            ReplayBtn.Enabled = false;
            this._simulation = null;
        }

        #endregion
        public void SetPlayerList(int simulation_id)
        {
            PlayerListResponse player_data = SystemConnection.PlayerList(simulation_id);
            if (player_data.status.code == 0)
            {
                player_list = player_data.players;

                PlayerTable.DataSource = player_list;
                PlayerTable.Columns["id"].Visible = false;
                PlayerTable.Columns["role_player"].HeaderText = "Player Role";
                PlayerTable.Columns["role_player"].DisplayIndex = 0;
                PlayerTable.Columns["user_ID"].Visible = false;
                PlayerTable.Columns["fullname"].HeaderText = "Player Name";
                PlayerTable.Columns["pilot"].HeaderText = "Pilot on Aircraft";
                PlayerTable.Columns["point"].HeaderText = "Point";
                PlayerTable.Columns["point"].DisplayIndex = 5;
                PlayerTable.Columns["point"].Width = 50;
                PlayerTable.Columns["grade"].HeaderText = "Grade";
                PlayerTable.Columns["grade"].DisplayIndex = 6;
                PlayerTable.Columns["edited_by"].Visible = false;
                PlayerTable.Columns["photo_image"].Visible = false;
            }

        }

        public bool SeeDetailUser(int userID)
        {
            UserDetailResponse response = SystemConnection.UserDetail(userID);
            if (response.status.code == 0 && response.user != null)
            {
                this.player = response.user;
                Image_Photo.Image = HelperClient.byteArrayToImage(response.user.photo);
                FirstNameLbl.Text = response.user.first_name;
                LastNameLbl.Text = response.user.last_name;
                UserIdLbl.Text = response.user.user_identity_id;
                ClassLbl.Text = response.user.student_info.@class;
                ClassYearLbl.Text = response.user.student_info.class_year.HasValue ? response.user.student_info.class_year.Value.ToString() : "";
                return true;
            }
            else
                return false;
        }

        private void SimulationTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                DataGridView obj = (DataGridView)sender;
                SimulationListModel data = (SimulationListModel)obj.Rows[e.RowIndex].DataBoundItem;
                this._simulation = data;
                SetPlayerList(data.id);

                RemoveHistoryBtn.Enabled = true;
                PrintBtn.Enabled = true;
                DetailPanel.Visible = false;
                if (data.elapsed_time.Value > new TimeSpan())
                    ReplayBtn.Enabled = true;
                else
                    ReplayBtn.Enabled = false;
            }
        }

        private void PlayerTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                DataGridView obj = (DataGridView)sender;
                PlayerListModel data = (PlayerListModel)obj.Rows[e.RowIndex].DataBoundItem;
                bool found = SeeDetailUser(data.user_ID);
                this._player = data;
                if (found)
                {
                    GivePointBtn.Enabled = true;
                    HistoryBtn.Enabled = true;
                    DetailPanel.BackgroundImage = null;
                }
                else
                {
                    DetailPanel.BackgroundImage = global::ATCSimulator.Client.Properties.Resources.remove;
                    GivePointBtn.Enabled = false;
                    HistoryBtn.Enabled = false;
                    Image_Photo.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
                    FirstNameLbl.Text = "";
                    LastNameLbl.Text = "";
                    UserIdLbl.Text = "";
                    ClassLbl.Text = "";
                    ClassYearLbl.Text = "";
                }
                DetailPanel.Visible = true;
            }
        }

        private void GivePointBtn_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            GivePointForm pointForm = new GivePointForm(username);
            pointForm.Init(this, _simulation, _player, Image_Photo.Image);
            pointForm.StartPosition = FormStartPosition.CenterScreen;
            pointForm.ShowDialog();
        }

        private void HistoryBtn_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            HistoryPlayerForm historyPlayerForm = new HistoryPlayerForm();
            historyPlayerForm.Init(this, null, player);
            historyPlayerForm.StartPosition = FormStartPosition.CenterParent;
            historyPlayerForm.ShowDialog();
        }

        private void RemoveHistoryBtn_Click(object sender, EventArgs e)
        {
            StatusResponse result = SystemConnection.SimulationRemove(_simulation.id);
            if (result.status.code == 0)
            {
                MessageBox.Show("Data is Removed");
                SetSimulationList(1);
            }
            else
                MessageBox.Show("Failed to Remove Data");
        }

        private void PrintBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // Create a new MigraDoc document
                Document document = new Document();
                document.Info.Title = "Simulation Report";

                //DEFINED STYLE
                // Get the predefined style Normal.
                Style style = document.Styles["Normal"];

                // Because all styles are derived from Normal, the next line changes the
                // font of the whole document. Or, more exactly, it changes the font of
                // all styles and paragraphs that do not redefine the font.

                style.Font.Name = "Verdana";
                style = document.Styles[StyleNames.Header];
                style.ParagraphFormat.AddTabStop("16cm", MigraDoc.DocumentObjectModel.TabAlignment.Right);
                style = document.Styles[StyleNames.Footer];
                style.ParagraphFormat.AddTabStop("8cm", MigraDoc.DocumentObjectModel.TabAlignment.Center);

                // Create a new style called Table based on style Normal
                style = document.Styles.AddStyle("Table", "Normal");
                style.Font.Name = "Verdana";
                style.Font.Name = "Times New Roman";
                style.Font.Size = 9;

                // Create a new style called Reference based on style Normal
                style = document.Styles.AddStyle("Reference", "Normal");
                style.ParagraphFormat.SpaceBefore = "5mm";
                style.ParagraphFormat.SpaceAfter = "5mm";
                style.ParagraphFormat.TabStops.AddTabStop("16cm", MigraDoc.DocumentObjectModel.TabAlignment.Right);

                //CREATE PAGE
                // Each MigraDoc document needs at least one section.
                Section section = document.AddSection();

                // Create footer
                Paragraph paragraph = section.Footers.Primary.AddParagraph();
                paragraph.AddText(ConfigurationManager.AppSettings["FooterForm"].ToString());
                paragraph.Format.Font.Size = 9;
                paragraph.Format.Alignment = ParagraphAlignment.Center;



                // Create the text frame for the header
                TextFrame headerFrame = new TextFrame();
                headerFrame = section.Headers.Primary.AddTextFrame();
                headerFrame.Height = "10.0cm";
                headerFrame.Width = "20.0cm";
                headerFrame.Left = ShapePosition.Left;
                headerFrame.RelativeHorizontal = RelativeHorizontal.Margin;
                headerFrame.Top = "5.0cm";
                headerFrame.RelativeVertical = RelativeVertical.Page;

                // Put data in the header
                paragraph = headerFrame.AddParagraph("Exercise Name");
                paragraph.AddTab();
                paragraph.AddText(": " + _simulation.exercise_name);
                paragraph = headerFrame.AddParagraph("Date of Simulation");
                paragraph.AddTab();
                paragraph.AddText(": " + _simulation.created_date);
                paragraph = headerFrame.AddParagraph("Scenery Name");
                paragraph.AddTab();
                paragraph.AddText(": " + _simulation.scenery_name);
                paragraph = headerFrame.AddParagraph("Play Time");
                paragraph.AddTab();
                paragraph.AddTab();
                paragraph.AddText(": " + _simulation.play_time.ToString(@"hh\:mm\:ss"));
                paragraph = headerFrame.AddParagraph("Elapsed Time");
                paragraph.AddTab();
                paragraph.AddTab();
                paragraph.AddText(": " + (_simulation.elapsed_time.HasValue ? _simulation.elapsed_time.Value.ToString(@"hh\:mm\:ss") : new TimeSpan().ToString(@"hh\:mm\:ss")));
                paragraph.Format.Font.Name = "Times New Roman";
                paragraph.Format.Font.Size = 10;
                paragraph.Format.SpaceAfter = 0;

                // Add the print date field
                paragraph = section.AddParagraph();
                paragraph.Format.SpaceBefore = "5.5cm";
                paragraph.Style = "Reference";
                paragraph.AddFormattedText("Player On Simulation", new MigraDoc.DocumentObjectModel.Font { Bold = true, Underline = Underline.Single });
                // Create the item table
                Table table = new Table();
                table = section.AddTable();
                table.Style = "Table";
                table.Borders.Color = new MigraDoc.DocumentObjectModel.Color(0, 0, 0);
                table.Borders.Width = 0.25;
                table.Borders.Left.Width = 0.5;
                table.Borders.Right.Width = 0.5;
                table.Rows.LeftIndent = 0;
                // Before you can add a row, you must define the columns
                Column column = table.AddColumn("2.5cm");
                column.Format.Alignment = ParagraphAlignment.Center;

                column = table.AddColumn("2.5cm");
                column.Format.Alignment = ParagraphAlignment.Center;
                column = table.AddColumn("4.5cm");
                column.Format.Alignment = ParagraphAlignment.Center;

                column = table.AddColumn("1.5cm");
                column.Format.Alignment = ParagraphAlignment.Center;

                column = table.AddColumn("2.5cm");
                column.Format.Alignment = ParagraphAlignment.Center;

                column = table.AddColumn("2.5cm");
                column.Format.Alignment = ParagraphAlignment.Center;

                // Create Header
                Row header = table.AddRow();
                CreateHeader(header);
                table.SetEdge(0, 0, 6, 1, Edge.Box, MigraDoc.DocumentObjectModel.BorderStyle.Single, 0.75, MigraDoc.DocumentObjectModel.Color.Empty);

                //Fill Content
                foreach (var item in player_list)
                {
                    Row content = table.AddRow();
                    CreateContent(content, item);
                }

                // Create Sign Form
                paragraph = section.AddParagraph();
                paragraph.Format.SpaceBefore = "11cm";
                paragraph.AddDateField("........... " + DateTime.Now.ToString("dd-MM-yyyy"));
                paragraph.AddLineBreak();
                paragraph.AddText("Simulation's Intructor");
                paragraph.AddLineBreak();
                paragraph.AddLineBreak();
                paragraph.AddLineBreak();
                paragraph.AddLineBreak();
                paragraph.AddLineBreak();
                paragraph.AddLineBreak();
                paragraph.AddText("________________");
                paragraph.AddLineBreak();
                paragraph.AddText(_simulation.created_by);
                paragraph.AddTab();

                paragraph.Format.Font.Size = 10;
                paragraph.Format.Alignment = ParagraphAlignment.Right;


                // Create instance of PDFform class
                document.UseCmykColor = true;
                // Create a renderer for PDF that uses Unicode font encoding
                MigraDoc.Rendering.PdfDocumentRenderer pdfRenderer = new MigraDoc.Rendering.PdfDocumentRenderer(true);
                // Set the MigraDoc document
                pdfRenderer.Document = document;
                // Create the PDF document
                pdfRenderer.RenderDocument();
                // Save the PDF document...
                string filename = "Report Simulation.pdf";
                pdfRenderer.Save(filename); ;

                ProcessStartInfo info = new ProcessStartInfo();
                info.Verb = "print";
                info.FileName = filename;
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;

                Process p = new Process();
                p.StartInfo = info;
                p.Start();
            }catch
            {
                MessageBox.Show("Failed to Print, Please Try Again Later");
            }
        }

        public void CreateHeader(Row row)
        {
            // Create the header of the table
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Format.Font.Size = 14;
            row.Shading.Color = new MigraDoc.DocumentObjectModel.Color(204, 204, 204);
            row.Cells[0].AddParagraph("Position");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Full Name");
            row.Cells[1].Format.Font.Bold = true;
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Pilot on Aircraft");
            row.Cells[2].Format.Font.Bold = true;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Point");
            row.Cells[3].Format.Font.Bold = true;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Grade");
            row.Cells[4].Format.Font.Bold = true;
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Given Point By");
            row.Cells[5].Format.Font.Bold = true;
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
        }

        public void CreateContent(Row row, PlayerListModel player)
        {
            // Create the header of the table
            row.HeadingFormat = false;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            row.Format.Font.Size = 10;
            row.Shading.Color = new MigraDoc.DocumentObjectModel.Color(255, 255, 255);
            row.Cells[0].AddParagraph(player.role_player);
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph(player.fullname);
            row.Cells[1].Format.Font.Bold = false;
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph(player.pilot);
            row.Cells[2].Format.Font.Bold = false;
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph(player.point);
            row.Cells[3].Format.Font.Bold = false;
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph(player.grade);
            row.Cells[4].Format.Font.Bold = false;
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph(player.edited_by);
            row.Cells[5].Format.Font.Bold = false;
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
        }

        private void ReplayBtn_Click(object sender, EventArgs e)
        {
            mainform.Enabled = false;
            ReplayForm replay = new ReplayForm(mainform,_simulation,player_list);
            mainform.Hide();
            replay.Show();

        }

    }
}
