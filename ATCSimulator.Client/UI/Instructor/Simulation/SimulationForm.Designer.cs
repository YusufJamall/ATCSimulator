﻿namespace ATCSimulator.Client.UI.Instructor.Simulation
{
    partial class SimulationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PilotBox = new System.Windows.Forms.GroupBox();
            this.ControllerBox = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PlayBtn = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.ExerciseTable = new System.Windows.Forms.DataGridView();
            this.AircraftListBtn = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.AircraftListTable = new System.Windows.Forms.DataGridView();
            this.ArrivalCount = new System.Windows.Forms.Label();
            this.DepartureCount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.LightsGroup = new System.Windows.Forms.GroupBox();
            this.ApproachGroup = new System.Windows.Forms.GroupBox();
            this.TaxiGroup = new System.Windows.Forms.GroupBox();
            this.PapiLight = new System.Windows.Forms.CheckBox();
            this.TaxiLight = new System.Windows.Forms.CheckBox();
            this.RunwayGroup = new System.Windows.Forms.GroupBox();
            this.EnviromentGroup = new System.Windows.Forms.GroupBox();
            this.EnvTime = new Telerik.WinControls.UI.RadTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Visibility = new Telerik.WinControls.UI.RadSpinEditor();
            this.Temperature = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindSpeed = new Telerik.WinControls.UI.RadSpinEditor();
            this.WindDirection = new Telerik.WinControls.UI.RadSpinEditor();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Weather = new Telerik.WinControls.UI.RadDropDownList();
            this.label33 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.PlayTime = new System.Windows.Forms.Label();
            this.ExerciseName = new System.Windows.Forms.Label();
            this.SceneryName = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.AircraftPilotBtn = new System.Windows.Forms.Button();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExerciseTable)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AircraftListTable)).BeginInit();
            this.LightsGroup.SuspendLayout();
            this.TaxiGroup.SuspendLayout();
            this.EnviromentGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(12, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 22);
            this.label1.TabIndex = 86;
            this.label1.Text = "Exercise List";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.PilotBox);
            this.panel1.Controls.Add(this.ControllerBox);
            this.panel1.Location = new System.Drawing.Point(897, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(319, 340);
            this.panel1.TabIndex = 87;
            // 
            // PilotBox
            // 
            this.PilotBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.PilotBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.PilotBox.Location = new System.Drawing.Point(3, 272);
            this.PilotBox.Name = "PilotBox";
            this.PilotBox.Size = new System.Drawing.Size(296, 259);
            this.PilotBox.TabIndex = 105;
            this.PilotBox.TabStop = false;
            this.PilotBox.Text = "Pilot";
            // 
            // ControllerBox
            // 
            this.ControllerBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.ControllerBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.ControllerBox.Location = new System.Drawing.Point(3, 3);
            this.ControllerBox.Name = "ControllerBox";
            this.ControllerBox.Size = new System.Drawing.Size(296, 259);
            this.ControllerBox.TabIndex = 104;
            this.ControllerBox.TabStop = false;
            this.ControllerBox.Text = "Controller";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(1102, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 22);
            this.label2.TabIndex = 88;
            this.label2.Text = "Online User";
            // 
            // PlayBtn
            // 
            this.PlayBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.PlayBtn.Enabled = false;
            this.PlayBtn.FlatAppearance.BorderSize = 0;
            this.PlayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayBtn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.PlayBtn.ForeColor = System.Drawing.Color.White;
            this.PlayBtn.Location = new System.Drawing.Point(1091, 569);
            this.PlayBtn.Name = "PlayBtn";
            this.PlayBtn.Size = new System.Drawing.Size(122, 30);
            this.PlayBtn.TabIndex = 90;
            this.PlayBtn.Text = "Play";
            this.PlayBtn.UseVisualStyleBackColor = false;
            this.PlayBtn.Click += new System.EventHandler(this.PlayBtn_Click);
            // 
            // ClearBtn
            // 
            this.ClearBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClearBtn.FlatAppearance.BorderSize = 0;
            this.ClearBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearBtn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ClearBtn.ForeColor = System.Drawing.Color.White;
            this.ClearBtn.Location = new System.Drawing.Point(1091, 604);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(122, 30);
            this.ClearBtn.TabIndex = 93;
            this.ClearBtn.Text = "Clear Data";
            this.ClearBtn.UseVisualStyleBackColor = false;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // ExerciseTable
            // 
            this.ExerciseTable.AllowUserToAddRows = false;
            this.ExerciseTable.AllowUserToDeleteRows = false;
            this.ExerciseTable.AllowUserToOrderColumns = true;
            this.ExerciseTable.AllowUserToResizeRows = false;
            this.ExerciseTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ExerciseTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ExerciseTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.ExerciseTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Century Gothic", 12F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExerciseTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.ExerciseTable.ColumnHeadersHeight = 28;
            this.ExerciseTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ExerciseTable.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Century Gothic", 12F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(96)))), ((int)(((byte)(96)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ExerciseTable.DefaultCellStyle = dataGridViewCellStyle10;
            this.ExerciseTable.EnableHeadersVisualStyles = false;
            this.ExerciseTable.GridColor = System.Drawing.Color.Black;
            this.ExerciseTable.Location = new System.Drawing.Point(12, 31);
            this.ExerciseTable.MultiSelect = false;
            this.ExerciseTable.Name = "ExerciseTable";
            this.ExerciseTable.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Century Gothic", 12F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ExerciseTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.ExerciseTable.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Century Gothic", 12F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            this.ExerciseTable.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.ExerciseTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ExerciseTable.Size = new System.Drawing.Size(879, 340);
            this.ExerciseTable.TabIndex = 100;
            this.ExerciseTable.TabStop = false;
            this.ExerciseTable.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ExerciseTable_CellDoubleClick);
            // 
            // AircraftListBtn
            // 
            this.AircraftListBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.AircraftListBtn.Enabled = false;
            this.AircraftListBtn.FlatAppearance.BorderSize = 0;
            this.AircraftListBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AircraftListBtn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.AircraftListBtn.ForeColor = System.Drawing.Color.White;
            this.AircraftListBtn.Location = new System.Drawing.Point(1091, 494);
            this.AircraftListBtn.Name = "AircraftListBtn";
            this.AircraftListBtn.Size = new System.Drawing.Size(122, 30);
            this.AircraftListBtn.TabIndex = 101;
            this.AircraftListBtn.Text = "Aircraft List";
            this.AircraftListBtn.UseVisualStyleBackColor = false;
            this.AircraftListBtn.Click += new System.EventHandler(this.AircraftListBtn_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.AircraftListTable);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox7.ForeColor = System.Drawing.Color.White;
            this.groupBox7.Location = new System.Drawing.Point(633, 451);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(343, 190);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Aircraft List";
            // 
            // AircraftListTable
            // 
            this.AircraftListTable.AllowUserToAddRows = false;
            this.AircraftListTable.AllowUserToDeleteRows = false;
            this.AircraftListTable.AllowUserToOrderColumns = true;
            this.AircraftListTable.AllowUserToResizeRows = false;
            this.AircraftListTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AircraftListTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.AircraftListTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.AircraftListTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AircraftListTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.AircraftListTable.ColumnHeadersHeight = 28;
            this.AircraftListTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.AircraftListTable.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AircraftListTable.DefaultCellStyle = dataGridViewCellStyle14;
            this.AircraftListTable.EnableHeadersVisualStyles = false;
            this.AircraftListTable.GridColor = System.Drawing.Color.Black;
            this.AircraftListTable.Location = new System.Drawing.Point(6, 22);
            this.AircraftListTable.MultiSelect = false;
            this.AircraftListTable.Name = "AircraftListTable";
            this.AircraftListTable.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AircraftListTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.AircraftListTable.RowHeadersVisible = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            this.AircraftListTable.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.AircraftListTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AircraftListTable.Size = new System.Drawing.Size(331, 162);
            this.AircraftListTable.TabIndex = 3;
            this.AircraftListTable.TabStop = false;
            // 
            // ArrivalCount
            // 
            this.ArrivalCount.AutoSize = true;
            this.ArrivalCount.Font = new System.Drawing.Font("Arial Black", 16F, System.Drawing.FontStyle.Bold);
            this.ArrivalCount.ForeColor = System.Drawing.Color.White;
            this.ArrivalCount.Location = new System.Drawing.Point(1048, 566);
            this.ArrivalCount.Name = "ArrivalCount";
            this.ArrivalCount.Size = new System.Drawing.Size(29, 31);
            this.ArrivalCount.TabIndex = 6;
            this.ArrivalCount.Text = "0";
            // 
            // DepartureCount
            // 
            this.DepartureCount.AutoSize = true;
            this.DepartureCount.Font = new System.Drawing.Font("Arial Black", 16F, System.Drawing.FontStyle.Bold);
            this.DepartureCount.ForeColor = System.Drawing.Color.White;
            this.DepartureCount.Location = new System.Drawing.Point(1047, 478);
            this.DepartureCount.Name = "DepartureCount";
            this.DepartureCount.Size = new System.Drawing.Size(29, 31);
            this.DepartureCount.TabIndex = 5;
            this.DepartureCount.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(991, 617);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Arrival";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(978, 522);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 20);
            this.label16.TabIndex = 3;
            this.label16.Text = "Departure";
            // 
            // LightsGroup
            // 
            this.LightsGroup.Controls.Add(this.ApproachGroup);
            this.LightsGroup.Controls.Add(this.TaxiGroup);
            this.LightsGroup.Controls.Add(this.RunwayGroup);
            this.LightsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.LightsGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.LightsGroup.Location = new System.Drawing.Point(280, 383);
            this.LightsGroup.Name = "LightsGroup";
            this.LightsGroup.Size = new System.Drawing.Size(347, 259);
            this.LightsGroup.TabIndex = 103;
            this.LightsGroup.TabStop = false;
            this.LightsGroup.Text = "Lights Properties";
            // 
            // ApproachGroup
            // 
            this.ApproachGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ApproachGroup.ForeColor = System.Drawing.Color.White;
            this.ApproachGroup.Location = new System.Drawing.Point(7, 145);
            this.ApproachGroup.Name = "ApproachGroup";
            this.ApproachGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ApproachGroup.Size = new System.Drawing.Size(334, 108);
            this.ApproachGroup.TabIndex = 4;
            this.ApproachGroup.TabStop = false;
            this.ApproachGroup.Text = "Approach";
            // 
            // TaxiGroup
            // 
            this.TaxiGroup.Controls.Add(this.PapiLight);
            this.TaxiGroup.Controls.Add(this.TaxiLight);
            this.TaxiGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiGroup.ForeColor = System.Drawing.Color.White;
            this.TaxiGroup.Location = new System.Drawing.Point(6, 85);
            this.TaxiGroup.Name = "TaxiGroup";
            this.TaxiGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TaxiGroup.Size = new System.Drawing.Size(335, 60);
            this.TaxiGroup.TabIndex = 3;
            this.TaxiGroup.TabStop = false;
            this.TaxiGroup.Text = "Taxi / Papi";
            // 
            // PapiLight
            // 
            this.PapiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.PapiLight.BackColor = System.Drawing.Color.Tomato;
            this.PapiLight.FlatAppearance.BorderSize = 0;
            this.PapiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.PapiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PapiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.PapiLight.ForeColor = System.Drawing.Color.White;
            this.PapiLight.Location = new System.Drawing.Point(175, 22);
            this.PapiLight.Name = "PapiLight";
            this.PapiLight.Size = new System.Drawing.Size(145, 30);
            this.PapiLight.TabIndex = 108;
            this.PapiLight.Text = "Papi";
            this.PapiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PapiLight.UseVisualStyleBackColor = false;
            // 
            // TaxiLight
            // 
            this.TaxiLight.Appearance = System.Windows.Forms.Appearance.Button;
            this.TaxiLight.BackColor = System.Drawing.Color.Tomato;
            this.TaxiLight.FlatAppearance.BorderSize = 0;
            this.TaxiLight.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.TaxiLight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaxiLight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TaxiLight.ForeColor = System.Drawing.Color.White;
            this.TaxiLight.Location = new System.Drawing.Point(15, 22);
            this.TaxiLight.Name = "TaxiLight";
            this.TaxiLight.Size = new System.Drawing.Size(145, 30);
            this.TaxiLight.TabIndex = 107;
            this.TaxiLight.Text = "Taxi";
            this.TaxiLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TaxiLight.UseVisualStyleBackColor = false;
            // 
            // RunwayGroup
            // 
            this.RunwayGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RunwayGroup.ForeColor = System.Drawing.Color.White;
            this.RunwayGroup.Location = new System.Drawing.Point(6, 24);
            this.RunwayGroup.Name = "RunwayGroup";
            this.RunwayGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RunwayGroup.Size = new System.Drawing.Size(335, 60);
            this.RunwayGroup.TabIndex = 2;
            this.RunwayGroup.TabStop = false;
            this.RunwayGroup.Text = "Runway";
            // 
            // EnviromentGroup
            // 
            this.EnviromentGroup.Controls.Add(this.EnvTime);
            this.EnviromentGroup.Controls.Add(this.label17);
            this.EnviromentGroup.Controls.Add(this.label18);
            this.EnviromentGroup.Controls.Add(this.Visibility);
            this.EnviromentGroup.Controls.Add(this.Temperature);
            this.EnviromentGroup.Controls.Add(this.WindSpeed);
            this.EnviromentGroup.Controls.Add(this.WindDirection);
            this.EnviromentGroup.Controls.Add(this.label19);
            this.EnviromentGroup.Controls.Add(this.label20);
            this.EnviromentGroup.Controls.Add(this.label21);
            this.EnviromentGroup.Controls.Add(this.Weather);
            this.EnviromentGroup.Controls.Add(this.label33);
            this.EnviromentGroup.Controls.Add(this.label43);
            this.EnviromentGroup.Controls.Add(this.label44);
            this.EnviromentGroup.Controls.Add(this.label45);
            this.EnviromentGroup.Controls.Add(this.label49);
            this.EnviromentGroup.Controls.Add(this.label50);
            this.EnviromentGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.EnviromentGroup.ForeColor = System.Drawing.Color.DodgerBlue;
            this.EnviromentGroup.Location = new System.Drawing.Point(9, 383);
            this.EnviromentGroup.Name = "EnviromentGroup";
            this.EnviromentGroup.Size = new System.Drawing.Size(265, 259);
            this.EnviromentGroup.TabIndex = 102;
            this.EnviromentGroup.TabStop = false;
            this.EnviromentGroup.Text = "Environment Properties";
            // 
            // EnvTime
            // 
            this.EnvTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.EnvTime.Location = new System.Drawing.Point(137, 219);
            this.EnvTime.Name = "EnvTime";
            this.EnvTime.Size = new System.Drawing.Size(122, 24);
            this.EnvTime.Step = 30;
            this.EnvTime.TabIndex = 172;
            this.EnvTime.TabStop = false;
            this.EnvTime.Text = "radTimePicker1";
            this.EnvTime.TimeTables = Telerik.WinControls.UI.TimeTables.HoursAndMinutesInOneTable;
            this.EnvTime.Value = new System.DateTime(2014, 8, 16, 11, 30, 4, 0);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(237, 186);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 17);
            this.label17.TabIndex = 118;
            this.label17.Text = "m";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(237, 139);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 117;
            this.label18.Text = "o";
            // 
            // Visibility
            // 
            this.Visibility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Visibility.Location = new System.Drawing.Point(137, 182);
            this.Visibility.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.Visibility.Name = "Visibility";
            // 
            // 
            // 
            this.Visibility.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Visibility.Size = new System.Drawing.Size(100, 24);
            this.Visibility.TabIndex = 116;
            this.Visibility.TabStop = false;
            this.Visibility.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Visibility.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // Temperature
            // 
            this.Temperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Temperature.Location = new System.Drawing.Point(137, 146);
            this.Temperature.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Temperature.Name = "Temperature";
            // 
            // 
            // 
            this.Temperature.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Temperature.Size = new System.Drawing.Size(100, 24);
            this.Temperature.TabIndex = 115;
            this.Temperature.TabStop = false;
            this.Temperature.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Temperature.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // WindSpeed
            // 
            this.WindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindSpeed.Location = new System.Drawing.Point(137, 111);
            this.WindSpeed.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.WindSpeed.Name = "WindSpeed";
            // 
            // 
            // 
            this.WindSpeed.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindSpeed.Size = new System.Drawing.Size(100, 24);
            this.WindSpeed.TabIndex = 114;
            this.WindSpeed.TabStop = false;
            this.WindSpeed.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // WindDirection
            // 
            this.WindDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.WindDirection.Location = new System.Drawing.Point(137, 76);
            this.WindDirection.Maximum = new decimal(new int[] {
            359,
            0,
            0,
            0});
            this.WindDirection.Name = "WindDirection";
            // 
            // 
            // 
            this.WindDirection.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.WindDirection.Size = new System.Drawing.Size(100, 24);
            this.WindDirection.TabIndex = 113;
            this.WindDirection.TabStop = false;
            this.WindDirection.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.WindDirection.Value = new decimal(new int[] {
            359,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(242, 150);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 17);
            this.label19.TabIndex = 112;
            this.label19.Text = "C";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(237, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 17);
            this.label20.TabIndex = 111;
            this.label20.Text = "kn";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(237, 73);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 13);
            this.label21.TabIndex = 109;
            this.label21.Text = "o";
            // 
            // Weather
            // 
            this.Weather.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Weather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Weather.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Weather.Location = new System.Drawing.Point(137, 37);
            this.Weather.Name = "Weather";
            this.Weather.Size = new System.Drawing.Size(124, 24);
            this.Weather.TabIndex = 108;
            this.Weather.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.Weather_SelectedIndexChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(8, 219);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(43, 20);
            this.label33.TabIndex = 24;
            this.label33.Text = "Time";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(8, 183);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(64, 20);
            this.label43.TabIndex = 14;
            this.label43.Text = "Visibility";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(8, 146);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(100, 20);
            this.label44.TabIndex = 12;
            this.label44.Text = "Temperature";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(8, 76);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(112, 20);
            this.label45.TabIndex = 9;
            this.label45.Text = "Wind Direction";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(8, 37);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(70, 20);
            this.label49.TabIndex = 7;
            this.label49.Text = "Weather";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(8, 114);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(96, 20);
            this.label50.TabIndex = 2;
            this.label50.Text = "Wind Speed";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(635, 423);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(76, 19);
            this.label26.TabIndex = 106;
            this.label26.Text = "Scenery  :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label27.Location = new System.Drawing.Point(973, 405);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(153, 32);
            this.label27.TabIndex = 105;
            this.label27.Text = "Play Time :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(633, 395);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(121, 19);
            this.label28.TabIndex = 104;
            this.label28.Text = "Exercise Name  :";
            // 
            // PlayTime
            // 
            this.PlayTime.AutoSize = true;
            this.PlayTime.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.PlayTime.ForeColor = System.Drawing.Color.White;
            this.PlayTime.Location = new System.Drawing.Point(1132, 406);
            this.PlayTime.Name = "PlayTime";
            this.PlayTime.Size = new System.Drawing.Size(83, 32);
            this.PlayTime.TabIndex = 107;
            this.PlayTime.Text = "00:00";
            // 
            // ExerciseName
            // 
            this.ExerciseName.AutoSize = true;
            this.ExerciseName.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.ExerciseName.ForeColor = System.Drawing.Color.White;
            this.ExerciseName.Location = new System.Drawing.Point(760, 395);
            this.ExerciseName.Name = "ExerciseName";
            this.ExerciseName.Size = new System.Drawing.Size(0, 19);
            this.ExerciseName.TabIndex = 108;
            // 
            // SceneryName
            // 
            this.SceneryName.AutoSize = true;
            this.SceneryName.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.SceneryName.ForeColor = System.Drawing.Color.White;
            this.SceneryName.Location = new System.Drawing.Point(717, 423);
            this.SceneryName.Name = "SceneryName";
            this.SceneryName.Size = new System.Drawing.Size(0, 19);
            this.SceneryName.TabIndex = 109;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::ATCSimulator.Client.Properties.Resources.arrival;
            this.pictureBox6.Location = new System.Drawing.Point(988, 550);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(60, 60);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 1;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::ATCSimulator.Client.Properties.Resources.departure;
            this.pictureBox11.Location = new System.Drawing.Point(987, 460);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(60, 60);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 0;
            this.pictureBox11.TabStop = false;
            // 
            // AircraftPilotBtn
            // 
            this.AircraftPilotBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.AircraftPilotBtn.Enabled = false;
            this.AircraftPilotBtn.FlatAppearance.BorderSize = 0;
            this.AircraftPilotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AircraftPilotBtn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.AircraftPilotBtn.ForeColor = System.Drawing.Color.White;
            this.AircraftPilotBtn.Location = new System.Drawing.Point(1090, 458);
            this.AircraftPilotBtn.Name = "AircraftPilotBtn";
            this.AircraftPilotBtn.Size = new System.Drawing.Size(122, 30);
            this.AircraftPilotBtn.TabIndex = 110;
            this.AircraftPilotBtn.Text = "Aircraft Pilot";
            this.AircraftPilotBtn.UseVisualStyleBackColor = false;
            this.AircraftPilotBtn.Click += new System.EventHandler(this.AircraftPilotBtn_Click);
            // 
            // PrintBtn
            // 
            this.PrintBtn.BackColor = System.Drawing.Color.DodgerBlue;
            this.PrintBtn.Enabled = false;
            this.PrintBtn.FlatAppearance.BorderSize = 0;
            this.PrintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PrintBtn.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.PrintBtn.ForeColor = System.Drawing.Color.White;
            this.PrintBtn.Location = new System.Drawing.Point(1091, 532);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(122, 30);
            this.PrintBtn.TabIndex = 111;
            this.PrintBtn.Text = "Print";
            this.PrintBtn.UseVisualStyleBackColor = false;
            this.PrintBtn.Click += new System.EventHandler(this.PrintBtn_Click);
            // 
            // SimulationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1225, 645);
            this.Controls.Add(this.PrintBtn);
            this.Controls.Add(this.AircraftPilotBtn);
            this.Controls.Add(this.SceneryName);
            this.Controls.Add(this.ExerciseName);
            this.Controls.Add(this.PlayTime);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.ArrivalCount);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.DepartureCount);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.EnviromentGroup);
            this.Controls.Add(this.LightsGroup);
            this.Controls.Add(this.AircraftListBtn);
            this.Controls.Add(this.ExerciseTable);
            this.Controls.Add(this.ClearBtn);
            this.Controls.Add(this.PlayBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SimulationForm";
            this.Text = "v";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExerciseTable)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AircraftListTable)).EndInit();
            this.LightsGroup.ResumeLayout(false);
            this.TaxiGroup.ResumeLayout(false);
            this.EnviromentGroup.ResumeLayout(false);
            this.EnviromentGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnvTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Visibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Temperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WindDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Weather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button PlayBtn;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.DataGridView ExerciseTable;
        private System.Windows.Forms.Button AircraftListBtn;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView AircraftListTable;
        private System.Windows.Forms.Label ArrivalCount;
        private System.Windows.Forms.Label DepartureCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.GroupBox LightsGroup;
        private System.Windows.Forms.GroupBox ApproachGroup;
        private System.Windows.Forms.GroupBox TaxiGroup;
        private System.Windows.Forms.CheckBox PapiLight;
        private System.Windows.Forms.CheckBox TaxiLight;
        private System.Windows.Forms.GroupBox RunwayGroup;
        private System.Windows.Forms.GroupBox EnviromentGroup;
        private Telerik.WinControls.UI.RadTimePicker EnvTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private Telerik.WinControls.UI.RadSpinEditor Visibility;
        private Telerik.WinControls.UI.RadSpinEditor Temperature;
        private Telerik.WinControls.UI.RadSpinEditor WindSpeed;
        private Telerik.WinControls.UI.RadSpinEditor WindDirection;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private Telerik.WinControls.UI.RadDropDownList Weather;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label PlayTime;
        private System.Windows.Forms.Label ExerciseName;
        private System.Windows.Forms.Label SceneryName;
        private System.Windows.Forms.Button AircraftPilotBtn;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.GroupBox ControllerBox;
        private System.Windows.Forms.GroupBox PilotBox;

    }
}