﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Helper;

namespace ATCSimulator.Client.UI.Instructor.Simulation
{
    public partial class SimulationPilotForm : Form
    {
        private SimulationForm _simulation_form;
        private List<ListAircraftExerciseModel> list_aircrafts;
        private Dictionary<ListAircraftExerciseModel, OnlineUserModel> pilot_data;

        public SimulationPilotForm(SimulationForm simulation_form, List<ListAircraftExerciseModel> aircrafts, List<OnlineUserModel> pilots, Dictionary<ListAircraftExerciseModel, OnlineUserModel> _pilot_data)
        {
            InitializeComponent();
            this._simulation_form = simulation_form;
            this.list_aircrafts = aircrafts;
            //reset this pilot data
            pilot_data = new Dictionary<ListAircraftExerciseModel, OnlineUserModel>();
            CreateAircraft(list_aircrafts, pilots, _pilot_data);
        }
        public void CreateAircraft(List<ListAircraftExerciseModel> aircrafts, List<OnlineUserModel> pilots, Dictionary<ListAircraftExerciseModel, OnlineUserModel> _pilot_data)
        {
            for (int i = 0; i < aircrafts.Count; i++)
            {
                #region Create Callsign
                Label callsign = new Label()
                {
                    BackColor = System.Drawing.Color.Black,
                    Font = new System.Drawing.Font("Century Gothic", 10F),
                    ForeColor = System.Drawing.Color.White,
                    Location = new System.Drawing.Point(0, i*35),
                    Size = new System.Drawing.Size(148, 32),
                    Text = aircrafts[i].callsign,
                    TextAlign = System.Drawing.ContentAlignment.MiddleCenter
                };
                #endregion

                #region Create Flight
                Label flight = new Label()
                {
                    BackColor = System.Drawing.Color.Black,
                    Font = new System.Drawing.Font("Century Gothic", 10F),
                    ForeColor = System.Drawing.Color.White,
                    Location = new System.Drawing.Point(150, i * 35),
                    Size = new System.Drawing.Size(158, 32),
                    Text = aircrafts[i].is_departure?"Departure":"Arrival",
                    TextAlign = System.Drawing.ContentAlignment.MiddleCenter
                };
                #endregion

                #region Create Type
                Label type = new Label()
                {
                    BackColor = System.Drawing.Color.Black,
                    Font = new System.Drawing.Font("Century Gothic", 10F),
                    ForeColor = System.Drawing.Color.White,
                    Location = new System.Drawing.Point(310, i * 35),
                    Size = new System.Drawing.Size(138, 32),
                    Text = aircrafts[i].aircraft_model,
                    TextAlign = System.Drawing.ContentAlignment.MiddleCenter
                };
                #endregion

                #region Create Pilot
                Telerik.WinControls.UI.RadDropDownList pilot = new Telerik.WinControls.UI.RadDropDownList()
                {
                    DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList,
                    Font = new System.Drawing.Font("Century Gothic", 10F),
                    ForeColor = System.Drawing.Color.DodgerBlue,
                    Location = new System.Drawing.Point(455, (i*35)+5),
                    Name = aircrafts[i].callsign
                };
                foreach (OnlineUserModel p in pilots)
                {
                    pilot.Items.Add(new Telerik.WinControls.UI.RadListDataItem(p.user.first_name+" "+p.user.last_name,p));
                }
                pilot.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(Pilot_SelectedIndexChanged);
                pilot.Width = label4.Width - 10;

                // add recent pilot data
                foreach (var a in _pilot_data)
                {
                    if (aircrafts[i].callsign.Equals(a.Key.callsign))
                    {
                        pilot.Items.Where(d => d.Value.Equals(a.Value)).FirstOrDefault().Selected = true;
                        break;
                    }
                }

            
                #endregion

                AircraftPanel.Controls.Add(callsign);
                AircraftPanel.Controls.Add(flight);
                AircraftPanel.Controls.Add(type);
                AircraftPanel.Controls.Add(pilot);

            }
        }
        private void Pilot_SelectedIndexChanged(object sender, EventArgs e)
        {
            Telerik.WinControls.UI.RadDropDownList pilot = (Telerik.WinControls.UI.RadDropDownList)sender;

            if (pilot.SelectedIndex == -1)
            {
                return;
            }
            OnlineUserModel data = HelperClient.GetDataDropDown<OnlineUserModel>(pilot);
            foreach (ListAircraftExerciseModel a in list_aircrafts)
            {
                if (a.callsign.Equals(pilot.Name))
                {
                    pilot_data.Add(a, data);
                    break;
                }
            }
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            _simulation_form.GetPilotAircraft(pilot_data);
            _simulation_form.Enabled = true;
            this.Close();
        }
    }
}
