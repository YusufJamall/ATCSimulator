﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.SimulationService;
using ATCSimulator.Client.UI.Other;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Diagnostics;
using System.Drawing.Printing;

namespace ATCSimulator.Client.UI.Instructor.Simulation
{
    public partial class SimulationPrintForm : Form
    {
        private List<ListAircraftExerciseModel> list_aircrafts;
        private SimulationForm simulation;

        public SimulationPrintForm(SimulationForm simulation, List<ListAircraftExerciseModel> aircrafts)
        {
            InitializeComponent();
            this.simulation = simulation;
            this.list_aircrafts = aircrafts;
            CreateAircraft(list_aircrafts);
        }
        public void CreateAircraft(List<ListAircraftExerciseModel> aircrafts)
        {
            for (int i = 0; i < aircrafts.Count; i++)
            {
                #region Create Callsign
                Label callsign = new Label()
                {
                    BackColor = System.Drawing.Color.Black,
                    Font = new System.Drawing.Font("Century Gothic", 10F),
                    ForeColor = System.Drawing.Color.White,
                    Location = new System.Drawing.Point(0, i * 35),
                    Size = new System.Drawing.Size(148, 32),
                    Text = aircrafts[i].callsign,
                    TextAlign = System.Drawing.ContentAlignment.MiddleCenter
                };
                #endregion

                #region Create Flight
                Label flight = new Label()
                {
                    BackColor = System.Drawing.Color.Black,
                    Font = new System.Drawing.Font("Century Gothic", 10F),
                    ForeColor = System.Drawing.Color.White,
                    Location = new System.Drawing.Point(150, i * 35),
                    Size = new System.Drawing.Size(158, 32),
                    Text = aircrafts[i].is_departure ? "Departure" : "Arrival",
                    TextAlign = System.Drawing.ContentAlignment.MiddleCenter
                };
                #endregion

                #region Create Type
                Label type = new Label()
                {
                    BackColor = System.Drawing.Color.Black,
                    Font = new System.Drawing.Font("Century Gothic", 10F),
                    ForeColor = System.Drawing.Color.White,
                    Location = new System.Drawing.Point(310, i * 35),
                    Size = new System.Drawing.Size(138, 32),
                    Text = aircrafts[i].aircraft_model,
                    TextAlign = System.Drawing.ContentAlignment.MiddleCenter
                };
                #endregion

                #region Create Action Button
                Button print = new Button()
                {
                    BackColor = System.Drawing.Color.DodgerBlue,
                    FlatStyle = System.Windows.Forms.FlatStyle.Flat,
                    Font = new System.Drawing.Font("Century Gothic", 12F),
                    ForeColor = System.Drawing.Color.White,
                    Location = new System.Drawing.Point(450, i * 35),
                    Name = aircrafts[i].callsign,
                    Size = new System.Drawing.Size(label4.Width, 34),
                    Text = "Print",
                    UseVisualStyleBackColor = false,
                };
                print.Click += new System.EventHandler(this.PrintBtn_Click);

                #endregion

                AircraftPanel.Controls.Add(callsign);
                AircraftPanel.Controls.Add(flight);
                AircraftPanel.Controls.Add(type);
                AircraftPanel.Controls.Add(print);


            }
        }
        private void PrintBtn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            List<ListAircraftExerciseModel> aircraft = list_aircrafts.Where(d => d.callsign.Equals(btn.Name)).ToList();
            if (aircraft.Count > 0)
                CreateImage(aircraft);

        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            simulation.Enabled = true;
            this.Close();
        }

        private void PrintAllBtn_Click(object sender, EventArgs e)
        {
            CreateImage(list_aircrafts);
        }

        private void CreateImage(List<ListAircraftExerciseModel> aircrafts)
        {
            List<PDFImage> list_picture = new List<PDFImage>();
            foreach (var data in aircrafts)
            {
                FlightRouteDetailResponse result = SystemConnection.FlightRouteDetail(data.route_id, data.is_departure);
                if (result.status.code == 0)
                {
                    AircraftData aircraft = new AircraftData
                    {
                        callsign = data.callsign,
                        departure_time = data.departure_time,
                        estimate_time = data.estimate_time,
                        fuel = data.fuel,
                        is_departure = data.is_departure,
                        start_time = data.start_time.HasValue ? data.start_time.Value : new TimeSpan(),
                        pilot_properties = new PilotCommandProperties
                        {
                            flight = new PilotFlightCommand
                            {
                                flight_route = new FlightRouteListModel
                                {
                                    id = result.flight_route.id,
                                    alternate_airport_code = result.flight_route.alternate_airport_code,
                                    alternate_airport_id = result.flight_route.alternate_airport_id,
                                    alternate_airport_name = result.flight_route.alternate_airport_name,
                                    destination_airport_code = result.flight_route.destination_airport_code,
                                    destination_airport_id = result.flight_route.destination_airport_id,
                                    destination_airport_name = result.flight_route.destination_airport_name,
                                    origin_airport_code = result.flight_route.origin_airport_code,
                                    origin_airport_id = result.flight_route.origin_airport_id,
                                    origin_airport_name = result.flight_route.origin_airport_name,
                                    flight_rules = result.flight_route.flight_rules,
                                    flight_rules_id = result.flight_route.flight_rules_id,
                                    is_departure = data.is_departure,
                                    routes_name = result.flight_route.routes_name,
                                    runways = new List<string>()
                                },
                            }
                        },
                        properties = new AircraftProperties
                        {
                            model = data.aircraft_model
                        }
                    };
                    FlightStripForm flightstripApp = new FlightStripForm();
                    flightstripApp.Init(aircraft,"appcontroller");
                    flightstripApp.Show();
                    Bitmap flightstripPicApp = flightstripApp.GetCapturedForm();
                    flightstripApp.Close();
                    Bitmap pictureApp = new Bitmap(630, 115);
                    using (Graphics g = Graphics.FromImage(pictureApp))
                        g.DrawImage(flightstripPicApp, 0, 0, 630, 115);

                    FlightStripForm flightstripTwr = new FlightStripForm();
                    flightstripTwr.Init(aircraft, "twrcontroller");
                    flightstripTwr.Show();
                    Bitmap flightstripPicTwr = flightstripTwr.GetCapturedForm();
                    flightstripTwr.Close();
                    Bitmap pictureTwr = new Bitmap(630, 115);
                    using (Graphics g = Graphics.FromImage(pictureTwr))
                        g.DrawImage(flightstripPicTwr, 0, 0, 630, 115);

                    FlightPlanForm flightplan = new FlightPlanForm(new FlightPlanModel
                    {
                        callsign = data.callsign,
                        departure_time = data.departure_time,
                        eet = data.estimate_time,
                        flight_level = data.flight_level_type,
                        flight_rules = aircraft.pilot_properties.flight.flight_route.flight_rules,
                        flight_type = data.flight_type,
                        fuel_board = data.fuel,
                        person = data.person_on_board,
                        routes = aircraft.pilot_properties.flight.flight_route.routes_name.Aggregate((i, j) => i + " " + j),
                        strips = data.livery_strips,
                        true_speed = 0,
                        type_aircraft = "",
                        altitude = data.flight_level,
                        alternate_airport_name = aircraft.pilot_properties.flight.flight_route.alternate_airport_name,
                        destination_airport_code = aircraft.pilot_properties.flight.flight_route.destination_airport_code,
                        destination_airport_name = aircraft.pilot_properties.flight.flight_route.destination_airport_name,
                        origin_airport_code = aircraft.pilot_properties.flight.flight_route.origin_airport_code,
                        origin_airport_name = aircraft.pilot_properties.flight.flight_route.origin_airport_name
                    });
                    flightplan.Show();
                    Bitmap flightplanPic = flightplan.GetCapturedForm();
                    flightplan.Close();

                    list_picture.Add(new PDFImage
                    {
                        flightstripApp = pictureApp,
                        flightstripTwr = pictureTwr,
                        flightplan = flightplanPic
                    });
                }
            }
            CreatePDF(list_picture);
        }

        private void CreatePDF(List<PDFImage> data)
        {
            try
            {
                PdfDocument pdf = new PdfDocument();
                pdf.Info.Title = "Excercie Flight Plan And Strip";
                foreach (var item in data)
                {
                    PdfPage pdfPage = pdf.AddPage();
                    XGraphics graph = XGraphics.FromPdfPage(pdfPage);
                    XImage image1 = XImage.FromGdiPlusImage(item.flightplan);
                    XImage image2 = XImage.FromGdiPlusImage(item.flightstripApp);
                    XImage image3 = XImage.FromGdiPlusImage(item.flightstripTwr);
                    graph.DrawImage(image1, 100, 100);
                    graph.DrawImage(image2, 75, item.flightplan.Height + 5);
                    graph.DrawImage(image3, 75, (item.flightplan.Height + item.flightstripApp.Height));
                }
                string pdfFilename = "flightplan_flightstrip.pdf";
                pdf.Save(pdfFilename);

                ProcessStartInfo info = new ProcessStartInfo();
                info.Verb = "print";
                info.FileName = pdfFilename;
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;

                Process p = new Process();
                p.StartInfo = info;
                p.Start();
            }catch
            {
                MessageBox.Show("Print Failed, Please Try Again");
            }
        }
    }

    public class PDFImage
    {
        public Bitmap flightstripApp { get; set; }
        public Bitmap flightstripTwr { get; set; }
        public Bitmap flightplan { get; set; }
    }
}
