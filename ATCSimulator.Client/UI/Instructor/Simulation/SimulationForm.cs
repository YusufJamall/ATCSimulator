﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Helper;
using Newtonsoft.Json;
using ATCSimulator.Client.UI.Instructor.Exercise;
using ATCSimulator.Client.Services;

namespace ATCSimulator.Client.UI.Instructor.Simulation
{
    public partial class SimulationForm : Form
    {
        List<ListAircraftExerciseModel> list_aircrafts;
        List<ExerciseListModel> list_exercises;
        ExerciseDetailModel exercise;
        List<DetailPCModel> list_pc;
        string _username;
        Dictionary<Button, OnlineUserModel> online_user;
        Dictionary<ListAircraftExerciseModel, OnlineUserModel> pilot_data;

        public SimulationForm(string username)
        {
            InitializeComponent();
            this._username = username;
            Weather.Items.Clear();
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Clear", SimulationService.WeatherInfo.Clear));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Cloudy", SimulationService.WeatherInfo.Cloudy));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Light Rain", SimulationService.WeatherInfo.LightRain));
            Weather.Items.Add(new Telerik.WinControls.UI.RadListDataItem("Thunder Storm", SimulationService.WeatherInfo.ThunderStorm));
        }
        public void Init()
        {
            exercise = null;
            online_user = new Dictionary<Button, OnlineUserModel>();
            list_exercises = new List<ExerciseListModel>();
            pilot_data = new Dictionary<ListAircraftExerciseModel, OnlineUserModel>();
            ActiveForm(false);
            ResetForm();
            GenerateUserForm();
            GetUserOnline();
            SetExerciseList();
        }

        #region Function Procedure

        #region Generate Remote Login Form

        private void GenerateUserForm()
        {
            ListPCResponse _pc = SystemConnection.ListPC();
            if (_pc.status.code == 0)
            {
                list_pc = _pc.pc.Where(d => d.group.Equals(2) || d.group.Equals(3) || d.group.Equals(4)).ToList();

                // generate Controller Form
                int x = 6;
                int yController = 28;
                int yPilot = 28;

                var ADC = list_pc.Where(d => d.group.Equals(2)).ToList();
                foreach (DetailPCModel p in ADC)
                {
                    ControllerBox.Controls.Add(CreateUserForm(new Point { X = x, Y = yController }, p));
                    yController = yController + 55;
                }

                var APP = list_pc.Where(d => d.group.Equals(3)).ToList();
                foreach (DetailPCModel p in APP)
                {
                    ControllerBox.Controls.Add(CreateUserForm(new Point { X = x, Y = yController }, p));
                    yController = yController + 55;
                }

                // resize controller Box height
                ControllerBox.Height = ((APP.Count + ADC.Count) * 55) + ((APP.Count + ADC.Count) * 5) + 5;


                // generate Pilot Form
                var pilot = list_pc.Where(d => d.group.Equals(4)).ToList();
                foreach (DetailPCModel p in pilot)
                {
                    PilotBox.Controls.Add(CreateUserForm(new Point { X = x, Y = yPilot }, p));
                    yPilot = yPilot + 55;
                }

                // resize pilot Box height
                PilotBox.Height = (pilot.Count * 55) + (pilot.Count * 5) + 10;

                //set location Pilot Box
                PilotBox.Location = new Point { X = PilotBox.Location.X, Y = (ControllerBox.Location.Y + ControllerBox.Height) + 5 };
            }
        }

        #endregion

        #region Create Remote Login Form

        private Panel CreateUserForm(Point location, DetailPCModel pc)
        {
            Panel panelLogin = new System.Windows.Forms.Panel();
            Button buttonLogin = new System.Windows.Forms.Button();
            PictureBox pictureLogin = new System.Windows.Forms.PictureBox();
            Label labelLogin = new System.Windows.Forms.Label();
            // 
            // panelLogin
            // 
            panelLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            panelLogin.Controls.Add(buttonLogin);
            panelLogin.Controls.Add(pictureLogin);
            panelLogin.Controls.Add(labelLogin);
            panelLogin.Location = location;
            panelLogin.Name = pc.role_pc + "_pnl";
            panelLogin.Size = new System.Drawing.Size(284, 50);
            // 
            // buttonLogin
            // 
            buttonLogin.BackColor = System.Drawing.Color.DodgerBlue;
            buttonLogin.FlatAppearance.BorderSize = 0;
            buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            buttonLogin.Font = new System.Drawing.Font("Century Gothic", 10F);
            buttonLogin.ForeColor = System.Drawing.Color.White;
            buttonLogin.Location = new System.Drawing.Point(50, 0);
            buttonLogin.Name = pc.role_pc + "_btn";
            buttonLogin.Size = new System.Drawing.Size(233, 26);
            buttonLogin.TabStop = false;
            buttonLogin.Text = pc.role_pc;
            buttonLogin.UseVisualStyleBackColor = false;
            buttonLogin.Click += new System.EventHandler(this.UserBtnClick);
            // 
            // pictureBox1
            // 
            pictureLogin.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
            pictureLogin.Location = new System.Drawing.Point(0, 0);
            pictureLogin.Name = pc.role_pc + "_picture";
            pictureLogin.Size = new System.Drawing.Size(50, 50);
            pictureLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            pictureLogin.TabStop = false;
            // 
            // label3
            // 
            labelLogin.Font = new System.Drawing.Font("Century Gothic", 10F);
            labelLogin.ForeColor = System.Drawing.Color.White;
            labelLogin.Location = new System.Drawing.Point(53, 27);
            labelLogin.Name = pc.role_pc + "_lbl";
            labelLogin.Size = new System.Drawing.Size(225, 19);
            labelLogin.Text = "-";
            labelLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            return panelLogin;
        }
        #endregion

        #region Get User Online
        private void GetUserOnline()
        {
            if (list_pc.Count > 0)
            {

                foreach (DetailPCModel p in list_pc)
                {

                    Button btn = new System.Windows.Forms.Button();
                    PictureBox photo = new System.Windows.Forms.PictureBox();
                    Label name = new System.Windows.Forms.Label();

                    if (p.group.Equals(2) || p.group.Equals(3))
                    {
                        name = (Label)ControllerBox.Controls.Find(p.role_pc + "_lbl", true).FirstOrDefault();
                        btn = (Button)ControllerBox.Controls.Find(p.role_pc + "_btn", true).FirstOrDefault();
                        photo = (PictureBox)ControllerBox.Controls.Find(p.role_pc + "_picture", true).FirstOrDefault();
                    }
                    else if (p.group.Equals(4))
                    {
                        name = (Label)PilotBox.Controls.Find(p.role_pc + "_lbl", true).FirstOrDefault();
                        btn = (Button)PilotBox.Controls.Find(p.role_pc + "_btn", true).FirstOrDefault();
                        photo = (PictureBox)PilotBox.Controls.Find(p.role_pc + "_picture", true).FirstOrDefault();
                    }

                    if (name.Name != "" && btn.Name != "" && photo.Name != "")
                    {
                        if (p.is_active)
                        {
                            btn.BackColor = System.Drawing.Color.DodgerBlue;
                            btn.Enabled = true;
                            name.Text = "";
                            photo.Image = global::ATCSimulator.Client.Properties.Resources.photo_img;
                            if (p.is_online)
                            {
                                UserDetailResponse _user = SystemConnection.UserDetail(p.logged_user.Value);
                                online_user.Add(btn, new OnlineUserModel()
                                {
                                    pc = p,
                                    user = _user.user
                                });
                                btn.BackColor = System.Drawing.Color.ForestGreen;
                                btn.Enabled = false;
                                name.Text = _user.user.first_name + _user.user.last_name;
                                if (_user.user.photo != null)
                                    photo.Image = HelperClient.byteArrayToImage(_user.user.photo);
                            }
                        }
                        else
                        {
                            btn.BackColor = System.Drawing.Color.DimGray;
                            btn.Enabled = false;
                        }
                    }
                }
            }
        }

        #endregion

        #region Set Exercise List
        public void SetExerciseList()
        {
            ExerciseListResponse exercise_data = SystemConnection.ExerciseList();
            if (exercise_data.status.code == 0)
            {
                list_exercises = exercise_data.exercises.ToList();
                ExerciseTable.DataSource = list_exercises;
                ExerciseTable.Columns["id"].Visible = false;
                ExerciseTable.Columns["scenery_id"].Visible = false;
                ExerciseTable.Columns["scenery_name"].HeaderText = "Scenery";
                ExerciseTable.Columns["exercise_name"].HeaderText = "Exercise";
                ExerciseTable.Columns["play_time"].HeaderText = "Play Time";
                ExerciseTable.Columns["time_simulation"].HeaderText = "Time On Simulation";
                ExerciseTable.Columns["traffic_exercise"].HeaderText = "Traffic";
                ExerciseTable.Columns["weather"].HeaderText = "Weather";
                ExerciseTable.Columns["total_arrival"].HeaderText = "Arrival";
                ExerciseTable.Columns["total_departure"].HeaderText = "Departure";
            }
        }
        #endregion

        #region CreateCheckBox
        private CheckBox CreateCheckBox(bool check, Point location, Size size, string text)
        {
            CheckBox ch = new CheckBox()
            {
                Appearance = Appearance.Button,
                BackColor = System.Drawing.Color.Tomato,
                Checked = check,
                FlatStyle = FlatStyle.Flat,
                Font = new Font("Century Gothic", 12F),
                ForeColor = System.Drawing.Color.White,
                Location = location,
                Text = text,
                Size = size,
                TextAlign = ContentAlignment.MiddleCenter
            };
            ch.FlatAppearance.BorderSize = 0;
            ch.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            return ch;
        }
        #endregion

        #region Set AircraftList
        private void SetAircraftList()
        {
            int departure = list_aircrafts.Where(d => d.is_departure).Count();
            int arrival = list_aircrafts.Where(d => !d.is_departure).Count();
            DepartureCount.Text = departure.ToString();
            ArrivalCount.Text = arrival.ToString();
            DataTable dt = new DataTable();
            BindingSource data = new BindingSource();
            var _list_aircraft_table = list_aircrafts.Select(d => new
            {
                callsign = d.callsign,
                model = d.aircraft_model,
                flight = d.is_departure ? "Departure" : "Arrival",
                aircraft_exercise_id = d.aircraft_id
            }).ToList();

            data.DataSource = _list_aircraft_table;
            AircraftListTable.DataSource = data;
            AircraftListTable.Columns["aircraft_exercise_id"].Visible = false;
            AircraftListTable.Columns["callsign"].HeaderText = "Callsign";
            AircraftListTable.Columns["model"].HeaderText = "Type";
            AircraftListTable.Columns["flight"].HeaderText = "Flight";
        }
        #endregion

        #region AircraftExercise
        public void AircraftExercise(List<ListAircraftExerciseModel> aircraft_data)
        {
            this.list_aircrafts = aircraft_data;
            SetAircraftList();
        }
        #endregion

        #region Remote Login
        public void RemoteLoginUser(UserStudentSimulationModel user, DetailPCModel pc_info)
        {
            Label name = (Label)this.Controls.Find(pc_info.role_pc + "_lbl", true).FirstOrDefault();
            Button btn = (Button)this.Controls.Find(pc_info.role_pc + "_btn", true).FirstOrDefault();
            PictureBox photo = (PictureBox)this.Controls.Find(pc_info.role_pc + "_picture", true).FirstOrDefault();

            UserRemoteContract _request = new UserRemoteContract()
            {
                ip_address = pc_info.ip_address,
                pc_id = pc_info.id,
                user_id = user.user_ID,
                pc_role = pc_info.role_pc
            };
            UserDetailResponse response = SystemConnection.UserRemoteLogin(_request);
            if (response.status.code == 0)
            {

                Init();
            }
            else
            {
                MessageBox.Show("Failed to Remote Login");
            }
        }

        #endregion

        #region Get Pilot
        public void GetPilotAircraft(Dictionary<ListAircraftExerciseModel, OnlineUserModel> pilots)
        {
            pilot_data = pilots;
        }

        #endregion

        #endregion

        #region Button Handler

        #region Exercise Table Click
        private void ExerciseTable_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            DataGridView obj = (DataGridView)sender;
            ExerciseListModel data = (ExerciseListModel)obj.Rows[e.RowIndex].DataBoundItem;
            SceneryName.Text = data.scenery_name;

            ExerciseDetailResponse _exercise = SystemConnection.ExerciseDetail(data.id);
            if (_exercise.status.code == 0)
            {
                exercise = _exercise.exercise;
                list_aircrafts = _exercise.exercise.aircrafts.ToList();
                #region Properties Aircraft
                PlayTime.Text = _exercise.exercise.play_time.ToString(@"hh\:mm");
                ExerciseName.Text = _exercise.exercise.exercise_name;
                Weather.Items.Where(d => d.Value.ToString().ToLower().Equals(_exercise.exercise.weather.ToLower())).FirstOrDefault().Selected = true;
                WindDirection.Value = _exercise.exercise.wind_direction;
                WindSpeed.Value = _exercise.exercise.wind_speed;
                Temperature.Value = _exercise.exercise.temperature;
                Visibility.Value = _exercise.exercise.visibility;

                EnvTime.Value = DateTime.Now.AddSeconds(_exercise.exercise.time_simulation.TotalSeconds);
                ApproachGroup.Controls.Clear();
                int approach_counter = 0;
                int _app_counter = 0;
                foreach (var r in _exercise.exercise.approach_lights)
                {
                    int separation = 15;
                    int width = (ApproachGroup.Size.Width - separation) / (_exercise.exercise.approach_lights.Count / 2);
                    int locationY = 30;
                    int locationX = (width * _app_counter) + separation;
                    if (approach_counter % 2 == 1)
                    {
                        _app_counter++;
                        locationY = 65;
                    }
                    System.Drawing.Size size = new System.Drawing.Size()
                    {
                        Height = 30,
                        Width = width - separation
                    };
                    System.Drawing.Point location = new Point()
                    {
                        X = locationX,
                        Y = locationY
                    };
                    ApproachGroup.Controls.Add(CreateCheckBox(r.Value, location, size, r.Key));
                    approach_counter++;
                }
                RunwayGroup.Controls.Clear();
                int runway_counter = 0;
                foreach (var r in _exercise.exercise.runway_lights)
                {
                    int separation = 15;
                    int width = (RunwayGroup.Size.Width - separation) / _exercise.exercise.runway_lights.Count;
                    System.Drawing.Size size = new System.Drawing.Size()
                    {
                        Height = 30,
                        Width = width - separation
                    };
                    System.Drawing.Point location = new Point()
                    {
                        X = (width * runway_counter) + separation,
                        Y = 22
                    };
                    RunwayGroup.Controls.Add(CreateCheckBox(r.Value, location, size, r.Key));
                    runway_counter++;
                }
                TaxiLight.Checked = _exercise.exercise.taxi;
                PapiLight.Checked = _exercise.exercise.papi;
                #endregion
                ActiveForm(true);
                SetAircraftList();
                pilot_data.Clear();
            }
        }
        #endregion

        #region Play Button
        private void PlayBtn_Click(object sender, EventArgs e)
        {
            if (exercise != null)
            {
                if (list_aircrafts.Count != pilot_data.Count)
                {
                    MessageBox.Show("All Aircraft Pilot not defined");
                    return;
                }
                List<SimulationService.AircraftSimulationContract> pilots = new List<SimulationService.AircraftSimulationContract>();
                foreach (var p in pilot_data)
                {
                    SimulationService.AircraftSimulationContract ac = new SimulationService.AircraftSimulationContract()
                    {
                        aircraft_id = p.Key.aircraft_id,
                        aircraft_model = p.Key.aircraft_model,
                        callsign = p.Key.callsign,
                        departure_time = p.Key.departure_time,
                        destination = p.Key.destination,
                        estimate_time = p.Key.estimate_time,
                        flight_level = p.Key.flight_level,
                        flight_level_type = p.Key.flight_level_type,
                        flight_rule_name = p.Key.flight_rule_name,
                        flight_type_id = p.Key.flight_type_id,
                        flight_rules_id = p.Key.flight_rules_id,
                        is_departure = p.Key.is_departure,
                        fuel = p.Key.fuel,
                        livery_name = p.Key.livery_name,
                        livery_id = p.Key.livery_id,
                        origin = p.Key.origin,
                        parking_id = p.Key.parking_id,
                        route_id = p.Key.route_id,
                        person_on_board = p.Key.person_on_board,
                        start_time = p.Key.start_time,
                        flight_type = p.Key.flight_type,
                        livery_strips = p.Key.livery_strips,
                        runway = p.Key.runway,
                        pilot = new SimulationService.UserSimulationModel()
                        {
                            address = p.Value.user.address,
                            first_name = p.Value.user.first_name,
                            email = p.Value.user.email,
                            birthdate = p.Value.user.birthdate.HasValue ? p.Value.user.birthdate.Value : new DateTime(),
                            gender = p.Value.user.gender,
                            last_name = p.Value.user.last_name,
                            phone = p.Value.user.phone,
                            user_id = p.Value.user.ID,
                            user_identity_id = p.Value.user.user_identity_id,
                            pc_role = p.Value.pc.role_pc,
                            pc_name = p.Value.pc.pc_name,
                            ip_address = p.Value.pc.ip_address,
                            mac_address = p.Value.pc.mac_address,
                        }
                    };
                    pilots.Add(ac);
                }

                SimulationService.WeatherInfo _weather = SimulationService.WeatherInfo.Clear;
                _weather = (SimulationService.WeatherInfo)Weather.SelectedValue;

                // get all player
                List<SimulationService.PlayerModel> players = new List<SimulationService.PlayerModel>();
                foreach (var _online in online_user.Values)
                {
                    if (_online.pc.logged_user.HasValue)
                    {
                        players.Add(new SimulationService.PlayerModel
                        {
                            role = _online.pc.role_pc,
                            ID = _online.pc.logged_user.Value,
                            name = _online.user.first_name + " " + _online.user.last_name
                        });
                    }
                }
                SimulationService.PlaySimulationContract data = new SimulationService.PlaySimulationContract()
                {
                    exercise = new SimulationService.ExerciseSimulationModel()
                    {
                        approach_lights = exercise.approach_lights,
                        exercise_name = exercise.exercise_name,
                        id = exercise.id,
                        papi = exercise.papi,
                        play_time = exercise.play_time,
                        runway_lights = exercise.runway_lights,
                        scenery_name = exercise.scenery_name,
                        scenery_id = exercise.scenery_id,
                        taxi = exercise.taxi,
                        temperature = exercise.temperature,
                        time_simulation = exercise.time_simulation,
                        traffic_exercise = exercise.traffic_exercise,
                        visibility = exercise.visibility,
                        weather = _weather,
                        wind_direction = exercise.wind_direction,
                        wind_speed = exercise.wind_speed
                    },
                    pilots = pilots,
                    players = players,
                    InstructorName = _username
                };
                SimulationService.StatusResponse response = SimulationConnection.SimulationPlay(data);
                if (response.status.code == 0)
                {
                    SimulationConnection.StartSimulation();
                    ActiveForm(false);
                    ResetForm();

                }
                else
                {
                    MessageBox.Show("Failed to play simulation, please try again later");
                }
            }
        }

        #endregion

        #region Remote Login Button
        private void UserBtnClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string btn_name = btn.Name.Substring(0, btn.Name.Length - 4);
            foreach (DetailPCModel p in list_pc)
            {
                if (btn_name.Equals(p.role_pc))
                {
                    SimulationUserForm user_form = new SimulationUserForm(this, p);
                    user_form.StartPosition = FormStartPosition.CenterScreen;
                    user_form.Init();
                    this.Enabled = false;
                    user_form.ShowDialog();
                    break;
                }
            }
        }

        #endregion

        #region Aircraft List Button
        private void AircraftListBtn_Click(object sender, EventArgs e)
        {
            if (exercise != null)
            {
                AircraftExerciseForm aircraft_form = new AircraftExerciseForm(null, this);
                aircraft_form.LoadData(exercise.scenery_id, exercise.scenery_name);
                aircraft_form.Init(list_aircrafts);
                aircraft_form.StartPosition = FormStartPosition.CenterParent;
                this.Enabled = false;
                aircraft_form.ShowDialog();
            }
        }

        #endregion

        #region Mount Pilot Button
        private void AircraftPilotBtn_Click(object sender, EventArgs e)
        {
            List<OnlineUserModel> pilots = new List<OnlineUserModel>();
            foreach (var p in online_user)
            {
                if (p.Value.pc.role_pc.ToLower().Contains("pseudopilot"))
                {
                    pilots.Add(p.Value);
                }
            }
            SimulationPilotForm pilot = new SimulationPilotForm(this, list_aircrafts, pilots, pilot_data);
            pilot.StartPosition = FormStartPosition.CenterParent;
            this.Enabled = false;
            pilot.ShowDialog();
        }

        #endregion

        #region Print Button
        private void PrintBtn_Click(object sender, EventArgs e)
        {
            SimulationPrintForm print = new SimulationPrintForm(this, list_aircrafts);
            print.StartPosition = FormStartPosition.CenterParent;
            this.Enabled = false;
            print.ShowDialog();
        }

        #endregion

        #region Clear Button
        private void ClearBtn_Click(object sender, EventArgs e)
        {
            ActiveForm(false);
            ResetForm();
        }

        #endregion

        #region Weather
        private void Weather_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (Weather.SelectedIndex != -1)
            {
                HelperClient.WeatherDefault weather = new HelperClient.WeatherDefault((SimulationService.WeatherInfo)Weather.SelectedItem.Value);
                if (weather != null)
                {
                    WindDirection.Value = weather.wind_direction;
                    WindSpeed.Value = weather.wind_speed;
                    Temperature.Value = weather.temperature;
                    Visibility.Value = weather.visibility;
                }
            }
        }
        #endregion

        #endregion

        #region Helper

        public void ActiveForm(bool status)
        {
            AircraftPilotBtn.Enabled = status;
            AircraftListBtn.Enabled = status;
            PrintBtn.Enabled = status;
            PlayBtn.Enabled = status;
            EnviromentGroup.Enabled = status;
            LightsGroup.Enabled = status;
        }

        public void ResetForm()
        {
            RunwayGroup.Controls.Clear();
            ApproachGroup.Controls.Clear();
            TaxiLight.Checked = false;
            PapiLight.Checked = false;
            Weather.Text = "";
            WindDirection.Value = 0;
            WindSpeed.Value = 0;
            Temperature.Value = 0;
            Visibility.Value = 0;
            EnvTime.Text = "";
            BindingSource data = new BindingSource();
            AircraftListTable.DataSource = data;
            SceneryName.Text = "";
            PlayTime.Text = "00:00";
            ExerciseName.Text = "";
            DepartureCount.Text = "0";
            ArrivalCount.Text = "0";
        }
        #endregion

        

    }

    #region Class Model
    public class OnlineUserModel
    {
        public DetailPCModel pc { get; set; }
        public UserProfileModel user { get; set; }
    }
    #endregion
}
