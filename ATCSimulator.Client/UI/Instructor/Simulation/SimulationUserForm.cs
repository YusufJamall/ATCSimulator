﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ATCSimulator.Client.Helper;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;


namespace ATCSimulator.Client.UI.Instructor.Simulation
{
    public partial class SimulationUserForm : Form
    {
        List<UserStudentSimulationModel> list_user;
        SimulationForm simulation_form;
        DetailPCModel pc;
        public SimulationUserForm(SimulationForm simulation_form, DetailPCModel pc)
        {
            InitializeComponent();
            this.simulation_form = simulation_form;
            this.pc = pc;
        }
        public void Init()
        {
            UserStudentSimulationResponse data = SystemConnection.UserStudentSimulation();
            if(data.status.code ==0)
            {
                list_user = data.users.ToList();
                UserTable.DataSource = list_user;
                UserTable.Columns["profile_id"].Visible = false;
                UserTable.Columns["user_id"].Visible = false;
                UserTable.Columns["gender"].Visible = false;
                UserTable.Columns["score"].Visible = false;
                UserTable.Columns["user_identity_id"].Visible = false;
                UserTable.Columns["username"].Visible = false;
                UserTable.Columns["photo"].HeaderText = "Picture";
                UserTable.Columns["first_name"].HeaderText = "First Name";
                UserTable.Columns["last_name"].HeaderText = "Last Name";
                UserTable.Columns["class"].HeaderText = "Class";
                UserTable.Columns["class_year"].HeaderText = "Year";
                foreach (DataGridViewRow row in UserTable.Rows)
                {
                    row.Height = 20;
                }
            }
        }
        
        private void CloseBtn_Click(object sender, EventArgs e)
        {
            simulation_form.Enabled = true;
            this.Close();
        }

        private void UserTable_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            DataGridView obj = (DataGridView)sender;
            UserStudentSimulationModel data = (UserStudentSimulationModel)obj.Rows[e.RowIndex].DataBoundItem;
            simulation_form.RemoteLoginUser(data, pc);
            simulation_form.Enabled = true;
            this.Close();
        }
    }
}
