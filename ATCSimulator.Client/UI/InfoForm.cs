﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.UI;
using ATCSimulator.Client.UI.Other;
using ATCSimulator.Client.UI.Simulation;
using ATCSimulator.Client.Helper;



namespace ATCSimulator.Client.UI
{
    public partial class InfoForm : Form
    {
        private SystemConnection system_connection;
        private SimulationConnection simulation_connection;
        ConnectToServerAsync connectAsync;
        public LoginForm loginForm;
        public MainForm mainForm;
        public SimulationMainForm simulationForm;
        public PausedForm pauseForm;
        public bool isPlay;
        public static InfoForm Instance { get; private set; }
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        public InfoForm()
        {
            InitializeComponent();
            Instance = this;
            SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
            system_connection = new SystemConnection();
            simulation_connection = new SimulationConnection();
            loginForm = new LoginForm(this);
            mainForm = new MainForm(this);
            simulationForm = new SimulationMainForm(this, ConfigurationManager.AppSettings["PCRole"].ToString());
            connectAsync = new ConnectToServerAsync(Instance.ConnectToServer);
            this.Size = new Size { Height = 25, Width = 150 };
            this.Location = new Point { X = loginForm.Location.X + ( (loginForm.Width  - this.Width)/ 2 ), Y = 0 };
            this.TopMost = true;
            this.ShowInTaskbar = false;
        }
        private void InfoForm_Load(object sender, EventArgs e)
        {
            Process[] processes;
            Dictionary<string, string> item = new Dictionary<string, string>();
            processes = Process.GetProcessesByName(ConfigurationManager.AppSettings["ProcName"].ToString());
            foreach (Process proc in processes)
            {
                item.Add(proc.MainWindowTitle, proc.Id.ToString());
            }
            if (item.Count <= 1)
            {
                if (ConnectToServer())
                {
                    if (!isPlay)
                    {
                        loginForm.Show();
                    }
                    Task task = new Task(PingServerAsync);
                    task.Start();

                }
                else
                {
                    MessageBox.Show("Service Connection Error, application failed to run");
                    Application.Exit();
                }
            }
            else
            {
                MessageBox.Show("Application already running, please close application and try again");
                Application.Exit();
            }
        }
        public void ChildFormClosed(object sender, FormClosedEventArgs e)
        {
            StatusResponse disconnect = SystemConnection.DisconnectPC();
            Application.Exit();
        }

        private async void PingServerAsync()
        {
            if (SystemConnection.Ping())
            {
                await Task.Run(() => { PingServerAsync(); });
            }
            else
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.Disconnect();
                    }));
                }
                else
                {
                    Instance.Disconnect();
                }
                await Task.Run(() => { System.Threading.Thread.Sleep(1000); });

                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.Connecting();
                    }));
                }
                else
                {
                    Instance.Connecting();
                }
                await Task.Run(() =>
                {
                    System.Threading.Thread.Sleep(1000);
                    connectAsync.Invoke();
                    System.Threading.Thread.Sleep(1000);
                    PingServerAsync();
                });
            }
        }

        private void Connecting()
        {
            messageLabel.Text = "Connecting...";
            messageLabel.ForeColor = Color.Blue;
        }
        private void Disconnect()
        {
            messageLabel.Text = "Connection Lost";
            messageLabel.ForeColor = Color.Red;
            if (pauseForm == null || pauseForm.Visible.Equals(false))
            {
                pauseForm = new PausedForm();
                pauseForm.SetMessage("Please Wait");
                pauseForm.Show();
            }
            mainForm.Enabled = false;
            loginForm.Enabled = false;
            simulationForm.Enabled = false;
        }
        private void Connect()
        {
            messageLabel.Text = "Connected";
            messageLabel.ForeColor = Color.Green;
            if (pauseForm != null)
                pauseForm.Close();
            mainForm.Enabled = true;
            loginForm.Enabled = true;
            simulationForm.Enabled = true;
        }
        private void ConnectFailed()
        {
            messageLabel.Text = "Connect Failed";
            messageLabel.ForeColor = Color.Red;
        }
        private bool ConnectToServer()
        {
            try
            {

                StatusResponse message_system = system_connection.ConnectPC();
                SimulationService.SimulationConnectResponse message_simulation = simulation_connection.Connect();
                if (message_system.status.code == 0 && message_simulation.status.code == 0)
                {
                    isPlay = message_simulation.isPlay;
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            Instance.Connect();
                        }));
                    }
                    else
                    {
                        Instance.Connect();
                    }
                    if (message_simulation.isPlay)
                    {
                        bool continueResult = SimulationConnection.ContinueSimulation();
                        if (!continueResult)
                        {
                            MessageBox.Show("Simulation is playing and you're not counted as player, come back later after simulation finished");
                            Application.Exit();
                        }
                    }
                    return true;
                }
                else if (message_system.status.code == 400 || message_simulation.status.code == 400)
                {
                    MessageBox.Show("Configuration setting is not valid, application is closed");
                    Application.Exit();
                    return false;
                }
                else
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            Instance.ConnectFailed();
                        }));
                    }
                    else
                    {
                        Instance.ConnectFailed();
                    }
                    return false;
                }
            }
            catch
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.ConnectFailed();
                    }));
                }
                else
                {
                    Instance.ConnectFailed();
                }
                return false;
            }
        }
        public static void ContinueSimulationThread(ATCSimulator.Client.SimulationService.ExerciseSimulationModel exercise, List<ATCSimulator.Client.SimulationService.AircraftData> aircrafts, List<ATCSimulator.Client.SimulationService.FlightRouteListModel> flight_route, List<ATCSimulator.Client.SimulationService.DataAppronModel> aapprons, int userId, bool isPaused)
        {
            var lastLoginUserResult = SystemConnection.UserDetail(userId);
            if (lastLoginUserResult.status.code == 0)
            {
                var player = SystemConnection.UserLogin(lastLoginUserResult.user.username, lastLoginUserResult.user.password);
                if (player.status.code == 0)
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            Instance.loginForm.ContinueSimulation(player.user, exercise, aircrafts, flight_route, aapprons, isPaused, true);
                        }));
                    }
                    else
                    {
                        Instance.loginForm.ContinueSimulation(player.user, exercise, aircrafts, flight_route, aapprons, isPaused, true);
                    }
                }
                else
                {
                    if (Instance.InvokeRequired)
                    {
                        Instance.Invoke(new MethodInvoker(() =>
                        {
                            Instance.loginForm.FailedContinue();
                        }));
                    }
                    else
                    {
                        Instance.mainForm.ContinueSimulation(exercise, aircrafts, flight_route, aapprons, isPaused, true);
                    }
                }
            }
            else
            {
                if (Instance.InvokeRequired)
                {
                    Instance.Invoke(new MethodInvoker(() =>
                    {
                        Instance.loginForm.FailedContinue();
                    }));
                }
                else
                {
                    Instance.mainForm.ContinueSimulation(exercise, aircrafts, flight_route, aapprons, isPaused, true);
                }
            }
        }

        public delegate bool ConnectToServerAsync();

    }
}
