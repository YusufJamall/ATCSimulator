﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATCSimulator.Client.SystemService;
using ATCSimulator.Client.Services;
using ATCSimulator.Client.Helper;

namespace ATCSimulator.Client.UI.Profile
{
    public partial class ProfileMainForm : Form
    {
        UserProfileModel user;
        private bool imageEdit;
        public ProfileMainForm()
        {

            InitializeComponent();           
        }
        public void Init(UserProfileModel user)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["PCRole"].ToString().ToLower().Equals("instructor"))
            {
                HistoryInstructorResponse simulation_data = SystemConnection.GetHistoryInstructor(user.username);
                if (simulation_data.status.code == 0)
                {
                    List<SimulationListModel> list_simulation = simulation_data.simulations;
                    HistoryTable.DataSource = list_simulation;
                    HistoryTable.Columns["id"].Visible = false;
                    HistoryTable.Columns["scenery_id"].Visible = false;
                    HistoryTable.Columns["exercise_name"].HeaderText = "Exercise Name";
                    HistoryTable.Columns["scenery_name"].HeaderText = "Scenery";
                    HistoryTable.Columns["play_time"].HeaderText = "Play Time";
                    HistoryTable.Columns["elapsed_time"].Visible = false;
                    HistoryTable.Columns["weather"].HeaderText = "Weather";
                    HistoryTable.Columns["wind_direction"].Visible = false;
                    HistoryTable.Columns["wind_speed"].Visible = false;
                    HistoryTable.Columns["temperature"].Visible = false;
                    HistoryTable.Columns["visibility"].Visible = false;
                    HistoryTable.Columns["time_simulation"].HeaderText = "Time On Simulation";
                    HistoryTable.Columns["runway_lights"].Visible = false;
                    HistoryTable.Columns["taxi"].Visible = false;
                    HistoryTable.Columns["papi"].Visible = false;
                    HistoryTable.Columns["approach"].Visible = false;
                    HistoryTable.Columns["total_arrival"].HeaderText = "Arrival";
                    HistoryTable.Columns["total_departure"].HeaderText = "Departure";
                    HistoryTable.Columns["created_date"].HeaderText = "Date of Simulation";
                    HistoryTable.Columns["created_by"].Visible = false;

                    HistoryTable.Columns["exercise_name"].DisplayIndex = 0;
                    HistoryTable.Columns["created_date"].DisplayIndex = 1;
                    HistoryTable.Columns["created_date"].Width = 80;
                    HistoryTable.Columns["scenery_name"].DisplayIndex = 2;
                    HistoryTable.Columns["play_time"].DisplayIndex = 3;
                    HistoryTable.Columns["time_simulation"].DisplayIndex = 4;
                    HistoryTable.Columns["total_arrival"].DisplayIndex = 5;
                    HistoryTable.Columns["total_arrival"].Width = 50;
                    HistoryTable.Columns["total_departure"].DisplayIndex = 6;
                    HistoryTable.Columns["total_departure"].Width = 60;
                    HistoryTable.Columns["weather"].DisplayIndex = 7;
                }
            }
            else
            {
                List<DataHistoryPlayer> table = new List<DataHistoryPlayer>();
                HistoryPlayerResponse result = SystemConnection.GetHistoryPlayer(user.user_ID);
                if (result.status.code == 0)
                {
                    foreach (var data in result.history)
                    {
                        table.Add(new DataHistoryPlayer
                        {
                            player_ID = data.player.id,
                            simulationID = data.simulation.id,
                            dateSimulation = data.simulation.created_date,
                            sceneryName = data.simulation.scenery_name,
                            rolePlayer = data.player.role_player,
                            pilot = data.player.pilot,
                            point = data.player.point,
                            grade = data.player.grade
                        });
                    }
                }
                HistoryTable.DataSource = table;
                HistoryTable.Columns["player_ID"].Visible = false;
                HistoryTable.Columns["simulationID"].Visible = false;
                HistoryTable.Columns["dateSimulation"].HeaderText = "Date Simulation";
                HistoryTable.Columns["dateSimulation"].DisplayIndex = 0;
                HistoryTable.Columns["dateSimulation"].Width = 70;
                HistoryTable.Columns["sceneryName"].HeaderText = "Scenery";
                HistoryTable.Columns["sceneryName"].DisplayIndex = 1;
                HistoryTable.Columns["rolePlayer"].HeaderText = "Position";
                HistoryTable.Columns["rolePlayer"].DisplayIndex = 2;
                HistoryTable.Columns["pilot"].HeaderText = "Pilot on Aircraft";
                HistoryTable.Columns["pilot"].DisplayIndex = 3;
                HistoryTable.Columns["point"].HeaderText = "Point";
                HistoryTable.Columns["point"].DisplayIndex = 4;
                HistoryTable.Columns["point"].Width = 50;
                HistoryTable.Columns["grade"].HeaderText = "Grade";
                HistoryTable.Columns["grade"].DisplayIndex = 5;
                HistoryTable.Columns["grade"].Width = 100;
            }
            this.user = user;
            GetDataProfile();
            imageEdit = false;
        }

        #region Function and Procedure
        public void GetDataProfile()
        {
            Image_Photo.Image = user.photo != null ? HelperClient.byteArrayToImage(user.photo) : global::ATCSimulator.Client.Properties.Resources.photo_img;
            Username.Text = user.username;
            Role_Box.Items.Clear();
            Role_Box.Items.Add(new Telerik.WinControls.UI.RadListDataItem(user.role.role_name, user.role.role_id));
            Role_Box.SelectedValue = user.role.role_id;
            FirstName_text.Text = user.first_name;
            LastName_text.Text = user.last_name;
            UserID_Text.Text = user.user_identity_id;
            Birthdate_User.Value = user.birthdate.HasValue ? user.birthdate.Value : DateTime.Now;
            email_text.Text = user.email;
            phone_text.Text = user.phone;
            address_text.Text = user.address;

            if (user.gender.ToLower() == "female")
                Gender_box.SelectedIndex = 1;
            else
                Gender_box.SelectedIndex = 0;
            if (user.isStudent)
            {
                IfStudent(true);
                Class_text.Text = user.student_info.@class;
                ClassYear_text.Text = user.student_info.class_year >= 0 ? user.student_info.class_year.ToString() : "";
            }
            else
            {
                IfStudent(false);
            }

        }

        private void UpdateData()
        {
            try
            {
                UserUpdateContract _user = new UserUpdateContract
                {
                    user_id = Convert.ToInt32(user.user_ID),
                    username = Username.Text,
                    role_id = Convert.ToInt32(Role_Box.SelectedItem.Value),
                    role_name = Role_Box.SelectedItem.Text,
                    profile = new UserProfileContract
                    {
                        first_name = FirstName_text.Text,
                        last_name = LastName_text.Text,
                        user_identity_id = UserID_Text.Text,
                        gender = Gender_box.SelectedItem.Text,
                        birthdate = Birthdate_User.Value,
                        email = email_text.Text,
                        address = address_text.Text,
                        create_by = user.username,
                        isStudent = Class_text.Visible ? true : false,
                        phone = phone_text.Text
                    }
                };
                if (imageEdit)
                    _user.profile.photo = HelperClient.imageToByteArray(Image_Photo.Image);
                if (user.isStudent)
                {
                    _user.profile.student_info = new StudentInfoContract
                    {
                        @class = Class_text.Text

                    };
                    if (!string.IsNullOrWhiteSpace(ClassYear_text.Text))
                    {
                        _user.profile.student_info.class_year = Convert.ToInt32(ClassYear_text.Text);
                    }
                }
                StatusResponse response = SystemConnection.UserUpdate(_user);
                if (response.status.code == 0)
                {
                    MessageBox.Show("Your Data is Updated");
                    UI.MainForm.UpdateProfile(new UserProfileModel
                    {
                        ID = user.ID,
                        user_ID = user.user_ID,
                        address = _user.profile.address,
                        birthdate = _user.profile.birthdate,
                        email = _user.profile.email,
                        first_name = _user.profile.first_name,
                        last_name = _user.profile.last_name,
                        gender = _user.profile.gender,
                        phone = _user.profile.phone,
                        photo = _user.profile.photo,
                        role = user.role,
                        create_by = user.create_by,
                        create_date = user.create_date,
                        isStudent = user.isStudent,
                        update_by = user.username,
                        update_date = DateTime.Now,
                        user_identity_id = user.user_identity_id,
                        username = user.username
                    });
                }
                else
                {
                    MessageBox.Show("Failed To Update");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        #endregion

        #region Button Handler
        private void ImageOnClick(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.png; *.bmp)|*.jpg; *.jpeg; *.png; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                // resize image
                byte[] byte_photo = HelperClient.getResizedImage(open.FileName, 100, 100);
                Image photo = HelperClient.byteArrayToImage(byte_photo);
                // display image in picture box
                Image_Photo.Image = photo;
                Image_Photo.SizeMode = PictureBoxSizeMode.Zoom;
                imageEdit = true;
            }
        }
        private void ImageOnHover(object sender, EventArgs e)
        {
            label_edit_picture.Visible = true;
        }

        private void ImageOnLeave(object sender, EventArgs e)
        {
            label_edit_picture.Visible = false;
        }
        private void ButtonOnHover(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.ForeColor = Color.White;
        }

        private void ButtonOnLeave(object sender, EventArgs e)
        {
            Label btn = (Label)sender;
            btn.ForeColor = Color.DodgerBlue;
        }
        private void Save_Btn_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                UpdateData();
            }
        }
        #endregion

        #region Helper
        private void IfStudent(bool value)
        {
            Class_lbl.Visible = value;
            ClassYear_lbl.Visible = value;
            Class_text.Visible = value;
            ClassYear_text.Visible = value;
        }

        private bool ValidateData()
        {
            if (Gender_box.SelectedIndex < 0)
            {
                MessageBox.Show("Gender cannot be empty please choose one");
                return false;
            }
            if (string.IsNullOrWhiteSpace(Birthdate_User.Text))
            {
                MessageBox.Show("Birthday Date cannot be empty please choose one");
                return false;
            }
            if (string.IsNullOrWhiteSpace(FirstName_text.Text))
            {
                MessageBox.Show("First Name Cannot be empty");
                return false;
            }
            if (user.isStudent)
            {
                if (string.IsNullOrWhiteSpace(ClassYear_text.Text)) { }
                else if (!System.Text.RegularExpressions.Regex.IsMatch(ClassYear_text.Text, @"^[0-9]+$"))
                {
                    MessageBox.Show("Class Year Must be contain number only");
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}
